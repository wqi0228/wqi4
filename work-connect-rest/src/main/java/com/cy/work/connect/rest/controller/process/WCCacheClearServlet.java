/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.controller.process;

import com.cy.system.rest.client.util.SystemProjectCacheUtils;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * 快取清除
 *
 * @author brain0925_liao
 */
@Slf4j
@WebServlet(name = "/cache/clear", urlPatterns = "/cache/clear")
public class WCCacheClearServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 88817866707061854L;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        try {
            log.debug("receive clear cache signal.");
            SystemProjectCacheUtils.getInstance().doClearSystemProjectCache();
            log.debug("receive clear cache signal done.");
            response.getWriter().write("ok");
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
}
