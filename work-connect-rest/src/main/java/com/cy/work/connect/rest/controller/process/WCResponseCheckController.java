package com.cy.work.connect.rest.controller.process;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 回應檢查
 *
 * @author jimmy_chou
 */
@Api(tags = "WCResponseCheckController", description = "回應檢查")
@RestController
@RequestMapping("/monitor")
public class WCResponseCheckController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1475589487771214285L;

    @ApiOperation(value = "回應檢查")
    @RequestMapping(value = "/status/response/check", method = RequestMethod.GET)
    public ResponseEntity<String> responseCheck() {
        return new ResponseEntity<String>("ok", HttpStatus.OK);
    }
}
