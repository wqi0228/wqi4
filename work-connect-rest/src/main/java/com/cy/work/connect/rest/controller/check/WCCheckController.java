/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.controller.check;

import com.cy.work.connect.rest.logic.service.WCCheckService;
import com.cy.work.connect.rest.logic.to.StatusResultTo;
import com.cy.work.connect.rest.util.WCRestUtil;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 檢查測試
 *
 * @author kasim
 */
@Api(tags = "WCCheckController", description = "檢查測試")
@RestController
@RequestMapping("/check")
@Slf4j
public class WCCheckController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -791233172716007683L;

    @Autowired
    private WCCheckService checkService;

    /**
     * 檢查測試
     *
     * @return
     */
    @ApiOperation(value = "檢查測試")
    @RequestMapping(
        value = "/checkStatus",
        method = RequestMethod.GET,
        produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> checkStatus() {
        try {
            List<StatusResultTo> resultTos =
                Lists.newArrayList(
                    checkService.checkFindByRecipientAndLimit(),
                    checkService.checkFindByRecipient(),
                    checkService.checkFindHistoryByRecipientAndBetweenCreateDate(),
                    checkService.checkFindAllToDo());
            String result = "\n";
            boolean methodStatus = true;
            for (StatusResultTo to : resultTos) {
                result = result + "\n";
                result += to.getMessage();
                if (!to.getResult()) {
                    methodStatus = false;
                }
            }
            if (methodStatus) {
                log.debug(result);
                result = "ok";
            } else {
                log.warn(result);
                result = "方法有Exception產生，請詳log確認!!";
            }
            return new ResponseEntity<String>(
                new Gson().toJson(result), ((methodStatus) ? HttpStatus.OK : HttpStatus.SEE_OTHER));
        } catch (Exception e) {
            return WCRestUtil.createErrorResponse(e.getMessage());
        }
    }
}
