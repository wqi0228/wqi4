package com.cy.work.connect.rest.config;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.naming.NamingException;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.io.PathResource;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * rest 設定
 *
 * @author kasim
 */
@Configuration("com.cy.work.connect.rest.config")
@ComponentScan(value = {"com.cy.common", "com.cy.work.connect", "com.cy.work.backend"})
@EnableScheduling
@PropertySources({})
@Import({
    com.cy.system.rest.client.config.SystemClientSpringAppConfig.class,
    com.cy.formsigning.rest.impl.config.FormSigningRestServiceSpringConfig.class,
    com.cy.bpm.rest.client.config.RestClientConfig.class,
    com.cy.work.common.config.CommonConfig.class,
    com.cy.work.common.logic.lib.config.WorkCommonLogicLibConfig.class
})
@EnableWebMvc
public class RestConfig extends RepositoryRestMvcConfiguration {

    @Bean
    public static PropertyPlaceholderConfigurer initProp() throws NamingException, IOException {
        String configHome = System.getenv("FUSION_CONFIG_HOME");
        Path urlPath = Paths.get(configHome, "url.properties");
        Path workConnectPath = Paths.get(configHome, "work-connect.properties");
        Path workCommon = Paths.get(configHome, "work-common.properties");
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer =
            new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocations(
            new PathResource[]{
                new PathResource(urlPath), new PathResource(workCommon),
                new PathResource(workConnectPath)
            });
        // 開啟後@Value如果有預設值則會永遠使用預設值需注意...
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }
}
