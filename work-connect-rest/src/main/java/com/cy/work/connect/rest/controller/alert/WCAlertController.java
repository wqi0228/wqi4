/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.controller.alert;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cy.work.connect.rest.logic.service.WCAlertService;
import com.cy.work.connect.rest.logic.to.WCAlertTo;
import com.cy.work.connect.rest.util.WCRestUtil;
import com.google.common.collect.Maps;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * Portal 最新消息 接口
 *
 * @author kasim
 */
@Api(tags = "WCAlertController", description = "Portal 最新消息")
@RestController
@RequestMapping("/alert")
@Slf4j
public class WCAlertController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2886479616120887488L;

    @Autowired
    private WCAlertService wcAlertService;

    @Autowired
    private WCRestUtil wcRestUtil;

    /**
     * 取回最新消息 (登入頁面)
     *
     * @param recipient 登入者Sid
     * @param limit     取得數量
     * @return
     */
    @ApiOperation(value = "取回最新消息 (登入頁面)")
    @RequestMapping(value = "/findByRecipientAndLimit", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WCAlertTo>> findByRecipientAndLimit(
            @RequestParam(value = "recipient") Integer recipient,
            @RequestParam(value = "limit") Integer limit) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("recipient", recipient);
        paramMap.put("limit", limit);

        // ====================================
        // 執行
        // ====================================
        ResponseEntity<List<WCAlertTo>> responseEntity = null;
        try {
            responseEntity = new ResponseEntity<List<WCAlertTo>>(
                    this.wcAlertService.findByRecipientAndLimit(recipient, limit), HttpStatus.OK);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
            responseEntity = WCRestUtil.createErrorResponse(e.getMessage());
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return responseEntity;
    }

    /**
     * 取回最新消息 (訊息頁面)
     *
     * @param recipient 登入者Sid
     * @return
     */
    @ApiOperation(value = "取回最新消息 (訊息頁面)")
    @RequestMapping(value = "/findByRecipient", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WCAlertTo>> findByRecipient(
            @RequestParam(value = "recipient") Integer recipient) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("recipient", recipient);

        // ====================================
        // 執行
        // ====================================
        ResponseEntity<List<WCAlertTo>> responseEntity = null;
        try {
            responseEntity = new ResponseEntity<List<WCAlertTo>>(
                    this.wcAlertService.findByRecipient(recipient), HttpStatus.OK);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
            responseEntity = WCRestUtil.createErrorResponse(e.getMessage());
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return responseEntity;

    }

    /**
     * 取回歷史消息 (訊息頁面)
     *
     * @param recipient 登入者Sid
     * @param startDate 時間(起)
     * @param endDate   時間(訖)
     * @return
     */
    @ApiOperation(value = "取回歷史消息 (訊息頁面)")
    @RequestMapping(value = "/findHistoryByRecipientAndBetweenCreateDate", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WCAlertTo>> findHistoryByRecipientAndBetweenCreateDate(
            @RequestParam(value = "recipient") Integer recipient,
            @RequestParam(value = "startDate") Date startDate,
            @RequestParam(value = "endDate") Date endDate) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("recipient", recipient);
        paramMap.put("startDate", startDate);
        paramMap.put("endDate", endDate);

        // ====================================
        // 執行
        // ====================================
        ResponseEntity<List<WCAlertTo>> responseEntity = null;
        try {
            responseEntity = new ResponseEntity<List<WCAlertTo>>(
                    this.wcAlertService.findHistoryByRecipientAndBetweenCreateDate(
                            recipient,
                            startDate,
                            endDate),
                    HttpStatus.OK);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
            responseEntity = WCRestUtil.createErrorResponse(e.getMessage());
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return responseEntity;

    }

    /**
     * 讀取訊息
     *
     * @param alertSid 最新消息Sid
     * @return
     */
    @ApiOperation(value = "讀取訊息")
    @RequestMapping(value = "/readAlert", method = RequestMethod.PUT)
    public ResponseEntity<HttpStatus> readAlert(@RequestParam(value = "alertSid") String alertSid) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("alertSid", alertSid);

        // ====================================
        // 執行
        // ====================================
        ResponseEntity<HttpStatus> responseEntity = null;
        try {
            wcAlertService.readAlert(alertSid);
            responseEntity = new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
            responseEntity = WCRestUtil.createErrorResponse(e.getMessage());
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return responseEntity;

    }
}
