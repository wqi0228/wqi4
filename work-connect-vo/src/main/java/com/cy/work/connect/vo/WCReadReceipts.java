/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.connect.vo.converter.ReadReceiptConverter;
import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 可閱名單
 *
 * @author jimmy_chou
 */
@Entity
@Table(name = "wc_read_receipts")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCReadReceipts implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8974520052619517593L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wc_read_receipts_sid")
    private Long sid;

    /**
     * 主檔SID
     */
    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcsid;

    /**
     * 可閱者UserSID
     */
    @Column(name = "reader", nullable = false)
    private Integer reader;

    /**
     * 讀取回條已未讀類型
     */
    @Column(name = "readreceipt", nullable = true)
    @Convert(converter = ReadReceiptConverter.class)
    private WCReadReceiptStatus readreceipt;

    /**
     * 已讀時間
     */
    @Column(name = "readtime", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date readtime;

    /**
     * 請求待閱讀時間
     */
    @Column(name = "requiretime", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date requiretime;

    /**
     * 建立者
     */
    @Column(name = "create_usr", nullable = false)
    private Integer createUser;
}
