/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.common.vo.converter.BooleanYNConverter;
import com.cy.work.connect.vo.converter.WCAlertTypeConverter;
import com.cy.work.connect.vo.enums.WCAlertType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作聯絡單 最新消息
 *
 * @author kasim
 */
@Entity
@Table(name = "wc_alert")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCAlert implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1119830514946584974L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_alert_sid", length = 36)
    private String sid;

    /*工作聯絡單 Sid*/
    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcSid;

    /*工作聯絡單 單號*/
    @Column(name = "wc_no", nullable = false, length = 21)
    private String wcNo;

    /*型態*/
    @Convert(converter = WCAlertTypeConverter.class)
    @Column(name = "wc_alert_type", nullable = false, length = 20)
    private WCAlertType type;

    /**
     * 主題
     */
    @Column(name = "theme", nullable = false, length = 255)
    private String theme;

    /*收件人*/
    @Column(name = "recipient", nullable = false)
    private Integer recipient;

    /**
     * 閱讀時間
     */
    @Column(name = "read_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date readDate;

    /*自動更新(Y/N)*/
    @Convert(converter = BooleanYNConverter.class)
    @Column(name = "auto_update", nullable = true, length = 1)
    private Boolean autoUpdate;

    /*建立者*/
    @Column(name = "create_usr", nullable = false)
    private Integer createUser;

    /**
     * 建立時間
     */
    @Column(name = "create_dt", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createDate;

    /*追蹤 Sid*/
    @Column(name = "wc_trace_sid", nullable = true, length = 36)
    private String wcTraceSid;
}
