/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.vo.enums.TargetType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author jimmy_chou
 */
@Converter
public class TargetTypeConverter implements AttributeConverter<TargetType, String> {

    @Override
    public String convertToDatabaseColumn(TargetType type) {
        if (type != null) {
            return type.name();
        }
        return null;
    }

    @Override
    public TargetType convertToEntityAttribute(String type) {
        TargetType targetType = null;
        if (WkStringUtils.notEmpty(type)) {
            targetType = TargetType.valueOf(type);
        }
        return targetType;
    }
}
