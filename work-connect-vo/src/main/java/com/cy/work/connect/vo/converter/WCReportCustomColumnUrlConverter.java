/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自訂欄位URL Converter
 *
 * @author brain0925_liao
 */
@Slf4j
@Converter
public class WCReportCustomColumnUrlConverter
    implements AttributeConverter<WCReportCustomColumnUrlType, String> {

    @Override
    public String convertToDatabaseColumn(WCReportCustomColumnUrlType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.getVal();
    }

    @Override
    public WCReportCustomColumnUrlType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            for (WCReportCustomColumnUrlType x : WCReportCustomColumnUrlType.values()) {
                if (dbData.equals(x.getVal())) {
                    return x;
                }
            }
            return null;
        } catch (Exception e) {
            log.error("WkReportCustomColumnUrlType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
