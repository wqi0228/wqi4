/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.WCMemoStatus;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作聯絡單單據狀態Converter
 *
 * @author brain0925_liao
 */
@Slf4j
@Converter
public class WCMemoStatusConverter implements AttributeConverter<WCMemoStatus, String> {

    @Override
    public String convertToDatabaseColumn(WCMemoStatus attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WCMemoStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WCMemoStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("WCMemoStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
