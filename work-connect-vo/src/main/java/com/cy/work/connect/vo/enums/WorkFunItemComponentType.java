/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 需要額外查詢筆數的介面類型
 *
 * @author brain0925_liao
 */
public enum WorkFunItemComponentType {
    FAVORITES("收藏夾", "fun-favorites"),
    TRACE("追蹤", "fun-trace"),
    INCOME_DEPT("收部門轉發", "fun-income-forwarding-department"),
    INCOME_REPORT("收呈報", "fun-income-report"),
    INCOME_INSTRUCTION("收指示", "fun-income-instruction"),
    INCOME_MEMBER("收個人", "fun-income-personal"),
    EXEC_SEARCH("個人待處理清單", "fun-work-connect-exec-search"),
    RECEVICE_SEARCH("待領單據清單", "fun-work-connect-recevice-search"),
    SEND_SEARCH("待派工清單", "fun-work-connect-send-search"),
    CHECKED_SEARCH("確認完成清單", "fun-work-connect-checked-search"),
    FAVORITE_SEARCH("收藏", "fun-favorites"),
    WORKING_SEARCH("部門待處理清單", "fun-work-connect-working-search");

    private final String val;

    private final String componentID;

    WorkFunItemComponentType(String val, String componentID) {
        this.val = val;
        this.componentID = componentID;
    }

    public String getComponentID() {
        return componentID;
    }

    public String getVal() {
        return val;
    }
}
