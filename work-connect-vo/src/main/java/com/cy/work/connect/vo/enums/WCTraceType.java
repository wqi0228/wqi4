/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 主檔追蹤類型
 *
 * @author brain0925_liao
 */
public enum WCTraceType {

    /**
     * 結案
     */
    CLOSE("結案"),
    /**
     * 結案(終止)
     */
    CLOSE_STOP("結案(終止)"),
    /**
     * 已執行完成
     */
    EXEC_FINISH_STATUS("已完成"),
    /**
     * 執行單位狀態異動
     */
    EXECDEP_STATUS_CHANGE("執行單位狀態異動"),
    /**
     * 單據狀態異動
     */
    STATUS_SIGN_CHANGE("單據狀態異動"),
    /**
     * 加簽異動
     */
    ADD_SIGN_CHANGE("加簽人員異動"),
    /**
     * 加派異動
     */
    MODIFY_CUSTOMER_EXCEDEP("加派異動"),
    /**
     * 待執行
     */
    MODIFY_APPRVOED("需求方已核准"),
    /**
     * 執行中
     */
    MODIFY_EXEC("執行中狀態"),
    /**
     * 變更標籤
     */
    MODIFY_TAG("變更標籤"),
    /**
     * 轉入工作聯絡單
     */
    TRANS_FROM_MEMO("轉入工作聯絡單"),
    /**
     * 編輯權限調整
     */
    MODIFY_EDIT_TYPE("編輯權限調整"),
    /**
     * 主題異動
     */
    MODIFY_THEME("主題異動"),
    /**
     * 內容異動
     */
    MODIFY_CONTNET("內容異動"),
    /**
     * 備註異動
     */
    MODIFY_MEMO("備註異動"),
    /**
     * 派工
     */
    SEND_WORK("派工"),
    /**
     * 轉單
     */
    TRANS_WORK("轉單"),
    /**
     * 執行完成
     */
    EXEC_FINISH("執行完成"),
    /**
     * 領單
     */
    EXEC_RECEVICE("領單"),
    /**
     * 補充說明
     */
    REPLY("回覆"),
    /**
     * 作廢
     */
    INVAILD("作廢"),
    /**
     * 刪除附加檔案
     */
    DEL_ATTACHMENT("刪除附加檔案"),
    /**
     * 刪除工作聯絡單（草稿）
     */
    DEL_WR("刪除工作聯絡單（草稿）"),
    /**
     * 發佈提交
     */
    COMMIT("發佈提交"),
    /**
     * 新增/調整索取讀取記錄
     */
    ADD_READ_RECEIPTS("新增/調整要求簽名記錄"),
    /**
     * 新增/調整可閱名單
     */
    ADD_VIEW_PERSON("新增/調整可閱名單"),
    /**
     * 執行完成回覆
     */
    FINISH_REPLY("執行完成回覆"),
    /**
     * 符合需求回覆
     */
    CORRECT_REPLY("符合需求回覆"),
    /**
     * 指示
     */
    DIRECTIVE("指示"),
    /**
     * 執行完成回覆的回覆
     */
    FINISH_REPLY_AND_REPLY("執行完成回覆的回覆"),
    /**
     * 僅alert使用
     */
    WAIT_EXEC("待處理"),
    /**
     * 轉工作聯絡單
     */
    TRANSTOWORKCONNECT("轉工作聯絡單"),
    /**
     * 執行單位退回
     */
    EXECROLLBACK("執行單位退回"),
    /**
     * 需求單位退回
     */
    REQROLLBACK("需求單位退回"),
    /**
     * 轉工作聯絡單-轉Link
     */
    TRANSTOWORKCONNECT_LINK("轉工作聯絡單"),
    /**
     * 簽核層級異動
     */
    MODIFY_FLOWTYPE("簽核層級異動"),
    
    /**
     * 執行方簽核者-不執行
     */
    EXEC_SIGNER_STOP("執行方簽核者-不執行"),
    /**
     * 執行單位-不執行
     */
    EXEC_MEMBER_STOP("執行單位-不執行"),
    
    
    
    ;

    //    web / rest 必須同步
    private final String val;

    WCTraceType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
