/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 讀取回條已未讀類型Converter
 *
 * @author jimmy_chou
 */
@Slf4j
@Converter
public class ReadReceiptConverter implements AttributeConverter<WCReadReceiptStatus, String> {

    @Override
    public String convertToDatabaseColumn(WCReadReceiptStatus attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WCReadReceiptStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WCReadReceiptStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("WCReadReceiptStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
