package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * JSON to integer list Converter (value)
 *
 * @author jimmy_chou
 */
@Slf4j
@Converter
public class Json2IntListConverter implements AttributeConverter<List<Integer>, String> {

    /* (non-Javadoc)
     * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
     */
    @Override
    public String convertToDatabaseColumn(List<Integer> attribute) {
        if (WkStringUtils.isEmpty(attribute)) {
            return "[]";
        }

        return new Gson().toJson(attribute);
    }

    /* (non-Javadoc)
     * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.Object)
     */
    @Override
    public List<Integer> convertToEntityAttribute(String dbData) {

        String jsonStr = WkStringUtils.safeTrim(dbData);

        if (WkStringUtils.isEmpty(jsonStr)) {
            jsonStr = "[]";
        }

        try {

            return new Gson().fromJson(jsonStr, new TypeToken<List<Integer>>() {
            }.getType());

        } catch (Exception e) {
            log.error("Json2IntListConverter 轉型失敗。 dbData ：【" + dbData + "】");
            return Lists.newArrayList();
        }
    }
}
