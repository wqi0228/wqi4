/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作聯絡單單據狀態Converter
 *
 * @author brain0925_liao
 */
@Slf4j
@Converter
public class WCStatusConverter implements AttributeConverter<WCStatus, String> {

    @Override
    public String convertToDatabaseColumn(WCStatus attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WCStatus convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WCStatus.valueOf(dbData);
        } catch (Exception e) {
            log.error("WCStatus Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
