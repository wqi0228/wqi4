package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.google.common.base.Strings;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作聯絡單人員名單Converter (DB允許Null值)
 *
 * @author jimmy_chou
 */
@Converter
@Slf4j
public class UsersAllowNullConverter implements AttributeConverter<UsersTo, String> {

    @Override
    public String convertToDatabaseColumn(UsersTo attribute) {
        try {
            if (attribute == null || attribute.getUserTos() == null) {
                return null;
            }
            String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute.getUserTos());
            return json;
        } catch (Exception ex) {
            log.error("parser UsersConverter to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    @Override
    public UsersTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            UsersTo to = new UsersTo();
            List<UserTo> resultList = WkJsonUtils.getInstance()
                .fromJsonToList(dbData, UserTo.class);
            to.setUserTos(resultList);
            return to;
        } catch (Exception ex) {
            log.error("parser json to UsersConverter fail!!" + ex.getMessage(), ex);
            return null;
        }
    }
}
