/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.common.eo.AbstractEo;
import com.cy.work.connect.vo.converter.ConfigValueConverter;
import com.cy.work.connect.vo.converter.to.ConfigValueTo;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 工作聯絡單 設定群組
 *
 * @author jimmy_chou
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(
    of = {"sid"},
    callSuper = false)
@Entity
@Table(name = "wc_set_group")
public class WCSetGroup extends AbstractEo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8042494526367940033L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wc_set_group_sid")
    private Long sid;

    /**
     * 公司sid
     */
    @Column(name = "comp_sid")
    private Integer compSid;

    /**
     * 名稱
     */
    @Column(name = "group_name", nullable = false)
    private String groupName;

    /**
     * 設定值（資料庫為 JSON String）
     */
    @Convert(converter = ConfigValueConverter.class)
    @Column(name = "config_value")
    private ConfigValueTo configValue;

    /**
     * 備註
     */
    @Column(name = "memo")
    private String memo;

    /**
     * 排序序號
     */
    @Column(name = "sort_seq")
    private Integer sortSeq = Integer.MAX_VALUE;
}
