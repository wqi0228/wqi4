/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.converter.SignActionTypeConverter;
import com.cy.work.connect.vo.enums.SignActionType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 需求簽核Detail (For 歷史簽核資料)
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_manager_sign_info_detail")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCManagerSignInfoDetail implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5927818788372444947L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_manager_sign_info_detail_sid", length = 36)
    private String sid;
    /**
     * 需求方簽核Sid
     */
    @Column(name = "wc_manager_sign_info_sid", nullable = false, length = 36)
    private String wc_manager_sign_info_sid;
    /**
     * 簽核人員Sid
     */
    @Column(name = "user_sid", nullable = false)
    private Integer user_sid;
    /**
     * 簽名動作時間
     */
    @Column(name = "sign_dt", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date sign_dt;
    /**
     * 狀態
     */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
    /**
     * 動作類型
     */
    @Convert(converter = SignActionTypeConverter.class)
    @Column(name = "signActionType", nullable = true)
    private SignActionType signActionType;
}
