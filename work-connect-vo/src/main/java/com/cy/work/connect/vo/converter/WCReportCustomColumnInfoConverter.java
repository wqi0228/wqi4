/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.WCReportCustomColumnInfo;
import com.cy.work.connect.vo.converter.to.WCReportCustomColumnInfoTo;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自訂欄位Converter
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class WCReportCustomColumnInfoConverter
    implements AttributeConverter<WCReportCustomColumnInfo, String> {

    @Override
    public String convertToDatabaseColumn(WCReportCustomColumnInfo attribute) {
        if (attribute == null || attribute.getWkReportCustomColumnInfoTos() == null) {
            return "";
        }
        String json =
            WkJsonUtils.getInstance()
                .toJsonWithOutPettyJson(attribute.getWkReportCustomColumnInfoTos());
        return json;
    }

    @Override
    public WCReportCustomColumnInfo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new WCReportCustomColumnInfo();
        }
        try {
            WCReportCustomColumnInfo to = new WCReportCustomColumnInfo();
            List<WCReportCustomColumnInfoTo> resultList =
                WkJsonUtils.getInstance().fromJsonToList(dbData, WCReportCustomColumnInfoTo.class);
            //            resultList.forEach(item -> {
            //                RRecord r = new RRecord(item.getReader(), item.getReadDay(),
            // WRReadStatus.valueOf(item.getRead()));
            //                to.getRRecord().add(r);
            //            });
            if (resultList != null) {
                to.setWkReportCustomColumnInfoTos(resultList);
            }
            return to;
        } catch (IOException ex) {
            log.error("parser json to WkReportCustomColumnInfo fail!!" + ex.getMessage(), ex);
            return new WCReportCustomColumnInfo();
        }
    }
}
