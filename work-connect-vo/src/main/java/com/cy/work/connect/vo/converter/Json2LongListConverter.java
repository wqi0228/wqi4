package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * JSON to Long list Converter (value)
 *
 * @author jimmy_chou
 */
@Slf4j
@Converter
public class Json2LongListConverter implements AttributeConverter<List<Long>, String> {

    /* (non-Javadoc)
     * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
     */
    @Override
    public String convertToDatabaseColumn(List<Long> attribute) {
        if (WkStringUtils.isEmpty(attribute)) {
            return "[]";
        }

        return new Gson().toJson(attribute);
    }

    /* (non-Javadoc)
     * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.Object)
     */
    @Override
    public List<Long> convertToEntityAttribute(String dbData) {

        String jsonStr = WkStringUtils.safeTrim(dbData);

        if (WkStringUtils.isEmpty(jsonStr)) {
            jsonStr = "[]";
        }

        try {

            return new Gson().fromJson(jsonStr, new TypeToken<List<Long>>() {
            }.getType());

        } catch (Exception e) {
            log.error("Json2LongListConverter 轉型失敗。 dbData ：【" + dbData + "】");
            return Lists.newArrayList();
        }
    }
}
