/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

import java.io.Serializable;

/**
 * 底層自訂欄位使用
 *
 * @author brain0925_liao
 */
public interface Column extends Serializable {

    boolean getCanSelectItem();

    boolean getDefaultShow();

    boolean getCanModifyWidth();

    String getDefaultWidth();

    String getVal();
}
