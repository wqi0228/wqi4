/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 執行單位人員歷史變化記錄
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_exec_dep_history")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCExecDepHistory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2307024397119340005L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_exec_dep_history_sid", length = 36)
    private String sid;

    /**
     * 執行單位Sid
     */
    @Column(name = "wc_exec_dep_sid", nullable = true, length = 36)
    private String wcExecDepSid;

    /**
     * 工作聯絡單Sid
     */
    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcSid;

    /**
     * 執行人員
     */
    @Column(name = "exec_usr", nullable = false)
    private Integer exec_usr;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;
}
