package com.cy.work.connect.vo;

import com.cy.work.common.eo.AbstractEo;
import com.cy.work.connect.vo.converter.ConfigTempletTypeConverter;
import com.cy.work.connect.vo.converter.ConfigValueConverter;
import com.cy.work.connect.vo.converter.to.ConfigValueTo;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author allen
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(
    of = {"sid"},
    callSuper = false)
@Entity
@Table(name = "wc_config_templet")
public class WCConfigTemplet extends AbstractEo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 9081976197011655295L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "templet_sid")
    private Long sid;

    /**
     * 模版類別
     */
    @Convert(converter = ConfigTempletTypeConverter.class)
    @Column(name = "templet_type", nullable = false)
    private ConfigTempletType templetType;

    /**
     * 公司sid
     */
    @Column(name = "comp_sid")
    private Integer compSid;

    /**
     * 名稱
     */
    @Column(name = "templet_name", nullable = false)
    private String templetName;

    /**
     * 設定值（資料庫為 JSON String）
     */
    @Convert(converter = ConfigValueConverter.class)
    @Column(name = "config_value")
    private ConfigValueTo configValue;

    /**
     * 備註
     */
    @Column(name = "memo")
    private String memo;

    /**
     * 排序序號
     */
    @Column(name = "sort_seq")
    private Integer sortSeq = Integer.MAX_VALUE;
}
