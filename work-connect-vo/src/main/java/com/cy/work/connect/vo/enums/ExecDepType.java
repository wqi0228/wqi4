/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * @author brain0925_liao
 */
public enum ExecDepType {
    /**
     * 需要簽核
     */
    NEED_SIGN,
    /**
     * 不需簽核
     */
    NO_SIGN
}
