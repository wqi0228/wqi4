/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 自訂欄位URL 類型
 *
 * @author brain0925_liao
 */
public enum WCReportCustomColumnUrlType {
    /**
     * 待派工清單
     */
    WAIT_WORK_LIST("work-connect/search/search1.xhtml"),
    /**
     * 待領單據清單
     */
    WAIT_RECEIVE_WORK_LIST("work-connect/search/search2.xhtml"),
    /**
     * 處理清單
     */
    PROCESS_LIST("work-connect/search/search3.xhtml"),
    /**
     * 申請單位查詢
     */
    APPLICATION_DEP_INQUIRE("work-connect/search/search4.xhtml"),
    /**
     * 可閱單據查詢
     */
    READABLE_DOCS_INQUIRE("work-connect/search/search5.xhtml"),
    /**
     * 備忘錄查詢
     */
    MEMO_SEARCH("work-connect/search/search6.xhtml"),
    /**
     * 最後可編輯時間查詢
     */
    LAST_MODIFY_SEARCH("work-connect/setting/setting2.xhtml"),
    /**
     * 標籤維護查詢
     */
    TAG_SEARCH("work-connect/setting/setting3.xhtml"),
    /**
     * 確認完成清單
     */
    CHECKED_LIST("work-connect/search/search7.xhtml"),
    /**
     * 確認完成清單
     */
    ROLLBACK_LIST("work-connect/search/search8.xhtml"),
    /**
     * 待處理清單
     */
    WORKING_LIST("work-connect/search/search9.xhtml"),
    /**
     * 收藏清單
     */
    FAVORITE_LIST("work-connect/search/search10.xhtml"),
    /**
     * 收藏清單
     */
    CLOSE_LIST("work-connect/search/search11.xhtml"),
    /**
     * 工作聯絡單查詢
     */
    WORK_REPORT_SEARCH(""),
    /**
     * 單據查詢
     */
    WORK_ORDER_LIST("work-connect/search/orderList.xhtml");
    private final String val;

    WCReportCustomColumnUrlType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
