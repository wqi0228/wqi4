/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.cy.work.common.eo.AbstractEo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 類別-大類
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_category_tag")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Data
public class WCCategoryTag extends AbstractEo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7024457569307586484L;

    /**
     * key
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_category_tag_sid", length = 36)
    private String sid;

    /**
     * 大類名稱
     */
    @Column(name = "category_name", nullable = false, length = 255)
    private String categoryName;

    /**
     * 備註
     */
    @Column(name = "memo", nullable = true, length = 255)
    private String memo;

    /**
     * 序號
     */
    @Column(name = "seq", nullable = false)
    private Integer seq;

    @Column(name = "compSid", nullable = true)
    private Integer compSid;
}
