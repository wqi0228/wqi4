/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.ExecDepSidTo;
import com.cy.work.connect.vo.converter.to.ExecDepSids;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 群組資訊Converter
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ExecDepSidsConverter implements AttributeConverter<ExecDepSids, String> {

    @Override
    public String convertToDatabaseColumn(ExecDepSids attribute) {
        if (attribute == null) {
            return "";
        }
        String json = WkJsonUtils.getInstance()
            .toJsonWithOutPettyJson(attribute.getExecDepSidTos());
        return json;
    }

    @Override
    public ExecDepSids convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ExecDepSids();
        }
        try {
            ExecDepSids to = new ExecDepSids();
            List<ExecDepSidTo> resultList =
                WkJsonUtils.getInstance().fromJsonToList(dbData, ExecDepSidTo.class);
            to.setExecDepSidTos(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to ExecDepSidsConverter fail!!" + ex.getMessage(), ex);
            return new ExecDepSids();
        }
    }
}
