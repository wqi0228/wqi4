package com.cy.work.connect.vo.converter.to;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author allen
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfigValueTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4866347054090284294L;

    /**
     * 部門 SID
     */
    private List<Integer> deps;

    /**
     * 部門設定有『含以下部門』
     */
    private boolean depContainFollowing = false;

    /**
     * 被選擇部門除外
     */
    private boolean depExcept = false;

    /**
     * USER SID
     */
    private List<Integer> users;
}
