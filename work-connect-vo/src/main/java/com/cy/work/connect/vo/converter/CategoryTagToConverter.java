/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.google.common.base.Strings;
import java.io.IOException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 讀取回傳資訊Converter
 *
 * @author kasim
 */
@Converter
@Slf4j
public class CategoryTagToConverter implements AttributeConverter<CategoryTagTo, String> {

    @Override
    public String convertToDatabaseColumn(CategoryTagTo attribute) {
        if (attribute == null) {
            return "";
        }
        return WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute);
    }

    @Override
    public CategoryTagTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WkJsonUtils.getInstance().fromJson(dbData, CategoryTagTo.class);
        } catch (IOException ex) {
            log.error("parser json to CategoryTagTo fail!!" + ex.getMessage(), ex);
            return null;
        }
    }
}
