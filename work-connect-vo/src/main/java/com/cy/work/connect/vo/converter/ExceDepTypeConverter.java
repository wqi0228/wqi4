/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.ExecDepType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * BPM狀態 Converter
 *
 * @author kasim
 */
@Slf4j
@Converter
public class ExceDepTypeConverter implements AttributeConverter<ExecDepType, String> {

    @Override
    public String convertToDatabaseColumn(ExecDepType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ExecDepType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ExecDepType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ExecDepType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
