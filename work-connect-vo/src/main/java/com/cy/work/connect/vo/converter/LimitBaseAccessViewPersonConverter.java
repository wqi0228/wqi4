/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.LimitBaseAccessViewPerson;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class LimitBaseAccessViewPersonConverter
    implements AttributeConverter<LimitBaseAccessViewPerson, String> {

    @Override
    public String convertToDatabaseColumn(LimitBaseAccessViewPerson attribute) {
        if (attribute == null) {
            return "";
        }
        String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute.getUserTos());
        return json;
    }

    @Override
    public LimitBaseAccessViewPerson convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            LimitBaseAccessViewPerson to = new LimitBaseAccessViewPerson();
            List<UserTo> resultList = WkJsonUtils.getInstance()
                .fromJsonToList(dbData, UserTo.class);
            to.setUserTos(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to LimitBaseAccessViewPersonConverter fail!!" + ex.getMessage(),
                ex);
            return null;
        }
    }
}
