/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.google.common.base.Strings;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作聯絡單已讀狀況Converter
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class UserDepConverter implements AttributeConverter<UserDep, String> {

    @Override
    public String convertToDatabaseColumn(UserDep attribute) {
        try {
            if (attribute == null || attribute.getUserDepTos() == null) {
                return "";
            }

            String json = WkJsonUtils.getInstance()
                .toJsonWithOutPettyJson(attribute.getUserDepTos());
            return json;
        } catch (Exception ex) {
            log.error("parser ReadRecordConverter to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    @Override
    public UserDep convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            UserDep to = new UserDep();
            List<UserDepTo> resultList =
                WkJsonUtils.getInstance().fromJsonToList(dbData, UserDepTo.class);
            //            resultList.forEach(item -> {
            //                RRecord r = new RRecord(item.getReader(), item.getReadDay(),
            // WRReadStatus.valueOf(item.getRead()));
            //                to.getRRecord().add(r);
            //            });
            to.setUserDepTos(resultList);
            return to;
        } catch (Exception ex) {
            log.error("parser json to ReadRecord fail!!" + ex.getMessage(), ex);
            return null;
        }
    }
}
