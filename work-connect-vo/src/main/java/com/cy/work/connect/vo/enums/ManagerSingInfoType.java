/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 簽核類型
 *
 * @author brain0925_liao
 */
public enum ManagerSingInfoType {
    /**
     * 需求單位簽核
     */
    MANAGERSIGNINFO,
    /**
     * 核准單位簽核
     */
    CHECKMANAGERINFO
}
