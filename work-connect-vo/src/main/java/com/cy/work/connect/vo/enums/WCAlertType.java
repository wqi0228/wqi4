/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 訊息類型
 *
 * @author kasim
 */
public enum WCAlertType {

    /**
     * 執行回覆
     */
    EXEC_REPLY("執行回覆"),
    /**
     * 執行完成
     */
    EXEC_FINISH("執行完成"),
    /**
     * 執行完成回覆
     */
    EXEC_FINISH_REPLY("執行完成回覆");

    //    web / rest 必須同步
    private final String val;

    WCAlertType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
