/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter.to;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDep implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7806822096708335923L;

    private List<UserDepTo> userDepTos = Lists.newArrayList();

    /**
     * 解析出 DepSid
     * @param userDep
     * @return
     */
    public static Set<Integer> parserDepSids(UserDep userDep) {
        if (userDep == null || WkStringUtils.isEmpty(userDep.getUserDepTos())) {
            return Sets.newHashSet();
        }
        return userDep.getUserDepTos()
                .stream()
                .filter(userDepTo -> userDepTo != null && WkStringUtils.isNumber(userDepTo.getDepSid()))
                .map(userDepTo -> Integer.parseInt(userDepTo.getDepSid()))
                .collect(Collectors.toSet());
    }
}
