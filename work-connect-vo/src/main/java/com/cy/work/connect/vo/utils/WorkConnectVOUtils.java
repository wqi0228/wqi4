/**
 * 
 */
package com.cy.work.connect.vo.utils;

import java.util.Set;
import java.util.stream.Collectors;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.vo.converter.UserDepConverter;
import com.cy.work.connect.vo.converter.UsersConverter;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
public class WorkConnectVOUtils {

    private static final UsersConverter usersConverter = new UsersConverter();
    private static final UserDepConverter userDepConverter = new UserDepConverter();

    // ========================================================================
    // UsersTo
    // ========================================================================
    /**
     * 解析 UsersTo 資料
     * 
     * @param usersTo UsersTo
     * @return 人員 sid
     */
    public static Set<Integer> prepareUsersToSids(UsersTo usersTo) {
        if (usersTo == null
                || WkStringUtils.isEmpty(usersTo.getUserTos())) {
            return Sets.newHashSet();
        }

        return usersTo.getUserTos().stream()
                .map(UserTo::getSid)
                .filter(userSid -> userSid != null)
                .collect(Collectors.toSet());
    }

    /**
     * usersConverter.convertToDatabaseColumn
     * 
     * @param usersTo UsersTo
     * @return JSON String
     */
    public static String usersConverterFromTo(UsersTo usersTo) {
        return usersConverter.convertToDatabaseColumn(usersTo);
    }

    /**
     * usersConverter.convertToEntityAttribute
     * 
     * @param jsonStr JSON String
     * @return UsersTo
     */
    public static UsersTo usersConverterFromJson(String jsonStr) {
        return usersConverter.convertToEntityAttribute(jsonStr);
    }

    // ========================================================================
    // UserDep
    // ========================================================================
    /**
     * 解析 UserDep 資料
     * 
     * @param userDep UserDep
     * @return 部門 sid
     */
    public static Set<Integer> prepareUserDepSids(UserDep userDep) {
        if (userDep == null
                || WkStringUtils.isEmpty(userDep.getUserDepTos())) {
            return Sets.newHashSet();
        }

        return userDep.getUserDepTos().stream()
                .filter(userDepTo -> WkStringUtils.notEmpty(userDepTo.getDepSid()))
                .filter(userDepTo -> WkStringUtils.isNumber(userDepTo.getDepSid()))
                .map(userDepTo -> Integer.parseInt(userDepTo.getDepSid()))
                .collect(Collectors.toSet());
    }

    /**
     * userDepConverter.convertToEntityAttribute
     * 
     * @param jsonStr JSON string
     * @return UserDep
     */
    public static UserDep userDepConverterFromJson(String jsonStr) {
        return userDepConverter.convertToEntityAttribute(jsonStr);
    }

    /**
     * userDepConverter.convertToDatabaseColumn
     * 
     * @param userDep
     * @return JSON String
     */
    public static String userDepConverterFromTo(UserDep userDep) {
        return userDepConverter.convertToDatabaseColumn(userDep);
    }

}
