/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.WCSidTo;
import com.cy.work.connect.vo.converter.to.WCSids;
import com.google.common.base.Strings;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 備忘錄轉單Sid Controller
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class WCSidsConverter implements AttributeConverter<WCSids, String> {

    @Override
    public String convertToDatabaseColumn(WCSids attribute) {
        try {
            if (attribute == null || attribute.getWcSidTos() == null) {
                return "";
            }
            String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute.getWcSidTos());
            return json;
        } catch (Exception ex) {
            log.error("parser WcSidsConverter to json fail!!" + ex.getMessage(), ex);
        }
        return "";
    }

    @Override
    public WCSids convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            WCSids to = new WCSids();
            List<WCSidTo> resultList = WkJsonUtils.getInstance()
                .fromJsonToList(dbData, WCSidTo.class);
            to.setWcSidTos(resultList);
            return to;
        } catch (Exception ex) {
            log.error("parser json to WcSidsConverter fail!!" + ex.getMessage(), ex);
            return null;
        }
    }
}
