/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.common.eo.AbstractEo;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.connect.vo.converter.UserDepConverter;
import com.cy.work.connect.vo.converter.UsersConverter;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.utils.WorkConnectVOUtils;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 單據名稱
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_menu_tag")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Data
public class WCMenuTag extends AbstractEo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3339258670237822208L;

    /**
     * key
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_menu_tag_sid", length = 36)
    private String sid;

    @Column(name = "wc_category_tag_sid", nullable = false, length = 36)
    private String wcCategoryTagSid;

    /**
     * 可使用單位
     */
    @Column(name = "use_dep", nullable = true)
    @Convert(converter = UserDepConverter.class)
    private UserDep useDep;
    
    /**
     * 可使用單位 sid
     * @return
     */
    public Set<Integer> getUseDepSids() {
        return WorkConnectVOUtils.prepareUserDepSids(useDep);
    }

    /**
     * 可使用單位:套用模版
     */
    @Column(name = "use_dep_templet_sid", nullable = true)
    private Long useDepTempletSid;

    /**
     * 可使用單位-是否含以下
     */
    @Column(name = "isUserContainFollowing", nullable = false)
    private boolean isUserContainFollowing;

    /**
     * 可使用人員
     */
    @Column(name = "use_user", nullable = true)
    @Convert(converter = UsersConverter.class)
    private UsersTo useUser;
    /**
     * 可使用單位 sid
     * @return
     */
    public Set<Integer> getUseUserSids() {
        return WorkConnectVOUtils.prepareUsersToSids(this.useUser);
    }
    

    @Column(name = "menu_name", nullable = false, length = 255)
    private String menuName;

    @Column(name = "item_name", nullable = false, length = 255)
    private String itemName;

    /**
     * 備註
     */
    @Column(name = "memo", nullable = true, length = 255)
    private String memo;

    /**
     * 序號
     */
    @Column(name = "seq", nullable = false)
    private Integer seq;

    @Column(name = "default_theme", nullable = true, length = 255)
    /** 預設主題 */
    private String defaultTheme;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "default_content", nullable = true)
    /** 預設內容 */
    private String defaultContent;

    @Convert(converter = StringBlobConverter.class)
    @Column(name = "default_memo", nullable = true)
    /** 預設備註 */
    private String defaultMemo;

    @Column(name = "attachment_type", nullable = true)
    /** 是否為附加檔案下載類型的單據 */
    private boolean attachmentType;

    @Column(name = "attachment_path", nullable = true)
    /** 附加檔案上傳位址 */
    private String attachmentPath;

    /**
     * 強制簽核到上層主管
     */
    @Column(name = "isParentManager", nullable = false)
    private boolean isParentManager;

    /**
     * 需求方最終簽核人員
     */
    @Column(name = "req_final_apply_user_sid", nullable = true)
    private Integer reqFinalApplyUserSid;

    /**
     * 可使用單位-是否僅主管可使用
     */
    @Column(name = "is_only_for_use_dep_manager", nullable = false)
    private boolean onlyForUseDepManager;
}
