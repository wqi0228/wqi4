/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter.to;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsersTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8648827942220564891L;

    private List<UserTo> userTos = Lists.newArrayList();

    /**
     * 解析出 userSid
     * 
     * @param userDep
     * @return
     */
    public static Set<Integer> parserUserSids(UsersTo usersTo) {
        if (usersTo == null || WkStringUtils.isEmpty(usersTo.getUserTos())) {
            return Sets.newHashSet();
        }
        return usersTo.getUserTos()
                .stream()
                .filter(userTo -> userTo != null && userTo.getSid() != null)
                .map(UserTo::getSid)
                .collect(Collectors.toSet());
    }
}
