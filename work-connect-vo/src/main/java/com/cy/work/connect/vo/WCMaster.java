/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import com.cy.work.connect.vo.converter.CategoryTagToConverter;
import com.cy.work.connect.vo.converter.ReadRecordConverter;
import com.cy.work.connect.vo.converter.ReplyRecordConverter;
import com.cy.work.connect.vo.converter.WCStatusConverter;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.converter.to.ReadRecord;
import com.cy.work.connect.vo.converter.to.ReplyRecord;
import com.cy.work.connect.vo.enums.WCStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作聯絡單主檔
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_master")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class) // 控制字元移除器
public class WCMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8606954930398951415L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_sid", length = 36)
    private String sid;
    /**
     * 工作聯絡單單號
     */
    @Column(name = "wc_no", nullable = false, length = 255, unique = true)
    private String wc_no;
    /**
     * 建單者公司Sid
     */
    @Column(name = "comp_sid", nullable = false)
    private Integer comp_sid;
    /**
     * 建單者部門Sid
     */
    @Column(name = "dep_sid", nullable = false)
    private Integer dep_sid;
    /**
     * 建單者Sid
     */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /**
     * 建單時間
     */
    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;
    /**
     * 更新者Sid
     */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /**
     * 更新時間
     */
    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
    /**
     * 單據狀態
     */
    @Convert(converter = WCStatusConverter.class)
    @Column(name = "wc_status", nullable = false)
    private WCStatus wc_status;
    /**
     * 是否有附加檔案
     */
    @Column(name = "has_attachment", nullable = false)
    private boolean has_attachment;
    /**
     * 是否有追蹤
     */
    @Column(name = "has_trace", nullable = false)
    private boolean has_trace;
    /**
     * 是否有回覆
     */
    @Column(name = "has_reply", nullable = false)
    private boolean has_reply;
    /**
     * 閱讀記錄
     */
    @Column(name = "read_record")
    @Convert(converter = ReadRecordConverter.class)
    private ReadRecord read_record;
    /**
     * 主題
     */
    @Column(name = "theme", nullable = true, length = 255)
    private String theme;
    /**
     * 內容
     */
    @Column(name = "content", nullable = false, length = 255)
    private String content;
    /**
     * 內容CSS
     */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "content_css", nullable = false)
    private String content_css;
    /**
     * 備註
     */
    @Column(name = "memo", nullable = true, length = 255)
    private String memo;
    /**
     * 備註CSS
     */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "memo_css", nullable = true)
    private String memo_css;
    /**
     * 鎖定時間
     */
    @Column(name = "lock_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lock_dt;
    /**
     * 鎖定者
     */
    @Column(name = "lock_usr", nullable = true)
    private Integer lock_usr;
    /**
     * 單據名稱(中類Sid)
     */
    @Column(name = "menuTag_sid", nullable = true)
    private String menuTagSid;
    /**
     * 執行項目(小類Sid)
     */
    @Column(name = "category_tag", nullable = true)
    @Convert(converter = CategoryTagToConverter.class)
    private CategoryTagTo categoryTagTo;
    /**
     * 需求完成日
     */
    @Column(name = "require_establish_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date require_establish_dt;
    /**
     * 備忘錄Sid
     */
    @Column(name = "wc_memo_sid", nullable = true)
    private String wcMemoSid;
    /**
     * 使用者未閱讀的回覆筆數
     */
    @Column(name = "reply_not_read")
    @Convert(converter = ReplyRecordConverter.class)
    private ReplyRecord replyRead;
    /**
     * 合法區間日期
     */
    @Column(name = "legitimate_range_date")
    private String legitimateRangeDate;
    /**
     * 起訖日期
     */
    @Column(name = "start_end_date")
    private String startEndDate;
    
    /**
     * 送件時間 (WORKCOMMU-503)
     */
    @Column(name = "send_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date sendDate;
 
    /**
     * 是否已送件 (WORKCOMMU-503)
     */
    @Column(name = "is_sended", nullable = false)
    private boolean isSended;
}
