package com.cy.work.connect.vo.enums;

/**
 * 設定來源(大類、中類、小類、執行單位、模版、MENU)
 *
 * @author jimmy_chou
 */
public enum TargetType {
    /**
     * 大類(單據類別)
     */
    CATEGORY_TAG,
    /**
     * 中類(單據名稱)
     */
    MENU_TAG,
    /**
     * 小類(執行項目)
     */
    TAG,
    /**
     * 執行單位
     */
    EXEC_DEP,
    /**
     * 模版
     */
    CONFIG_TEMPLET
}
