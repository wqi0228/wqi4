/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 附加檔案
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_memo_attachment")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCMemoAttachment implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3153226841013362648L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_memo_attachment_sid", length = 36)
    private String sid;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "wc_memo_sid", nullable = true, length = 36)
    private String wcMemoSid;

    @Column(name = "wc_memo_no", nullable = true, length = 21)
    private String wcMemoNo;

    @Column(name = "file_name", nullable = false, length = 255)
    private String file_name;

    @Column(name = "description", nullable = true, length = 255)
    private String description;

    @Column(name = "department", nullable = false)
    private Integer department_sid;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr_sid;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr_sid;

    @Column(name = "update_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
}
