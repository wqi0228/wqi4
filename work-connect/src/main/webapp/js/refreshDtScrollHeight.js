//特定 class
var class_datatable_scrollable_body = " .ui-datatable-scrollable-body";

//刷新DataTableel ScrollHeight
function refreshDtScrollHeight(dtId) {
    refreshDataTableScrollHeight(dtId, $(document).height());
}

//刷新DataTableel ScrollHeight
function refreshDataTableScrollHeight(dtId, documentHeight) {
    if (!isExist(dtId)) {
        return;
    }
    var old_dtScrollHeight = getDt_scrollableHeight(dtId);
    var dataTop = getDt_scrollableTop(dtId);
    var heightBottom = getPaginatorBottom_outerHeight(dtId);
    var dtHeight = documentHeight - dataTop - heightBottom - 35;
    updateDt_scrollableHeight(dtId, dtHeight);
    var dtDataHeight = getDt_dataOuterHeight(dtId);
    var dtDataHeight = $("#" + dtId + "_data").outerHeight();
    if ((dtDataHeight > old_dtScrollHeight) && (dtHeight > dtDataHeight)) {
        updateDt_marginRightToZero(dtId);
    }
}

//判斷ID是否存在
function isExist(obj) {
    return $("#" + obj).length > 0;
}

//取得特定datatable scrollable 高度
function getDt_scrollableHeight(obj) {
    return $("#" + obj + class_datatable_scrollable_body).height();
}

//取得特定datatable scrollable 位置高度
function getDt_scrollableTop(obj) {
    return $("#" + obj + class_datatable_scrollable_body).offset().top;
}

//取得特定datatable 分頁欄位 高度
function getPaginatorBottom_outerHeight(obj) {
    if (!isExist(obj + "_paginator_bottom")) {
        return 0;
    }
    return $("#" + obj + "_paginator_bottom").outerHeight();
}

//更新特定datatable scrollable 高度
function updateDt_scrollableHeight(obj, dtHeight) {
    $("#" + obj + class_datatable_scrollable_body).height(dtHeight);
}

//取得特定datatable 內容 高度
function getDt_dataOuterHeight(obj) {
    return $("#" + obj + "_data").outerHeight();
}

//更新datatable scrollable header margin-right 格式
function updateDt_marginRightToZero(obj) {
    $("#" + obj + " .ui-datatable-scrollable-header-box").css(
        {'margin-right': '0px'});
}


