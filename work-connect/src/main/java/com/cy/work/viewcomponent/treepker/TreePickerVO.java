/**
 * 
 */
package com.cy.work.viewcomponent.treepker;

import java.io.Serializable;
import java.util.Map;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 *
 */
public class TreePickerVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1739662320651560063L;

    /**
     * 是否全選
     */
    @Getter
    @Setter
    private boolean allSelected = false;
    
    /**
     * 隱藏停用項目
     */
    @Setter
    @Getter
    private boolean hideInactiveItem = true;
    
    /**
     * 選擇時是否含以下
     */
    @Getter
    @Setter
    private boolean containFollowing = true;
    
    /**
     * 下層選取是否影響上層
     */
    @Getter
    @Setter
    private boolean propagateSelectionUp = false;

    /**
     * 
     */
    @Getter
    @Setter
    private String serachItemNameKeyword;

    /**
     * 
     */
    @Getter
    private Map<WkItem, TreeNode> treeNodeMapByItem = Maps.newHashMap();
    
    /**
     * 樹節點結構資料
     */
    @Getter
    @Setter
    private TreeNode rootNode = new DefaultTreeNode();

    /**
     * 畫面傳入選擇的節點
     */
    @Getter
    @Setter
    private TreeNode[] selectedNodes;
}
