package com.cy.work.connect.web.logic.components;

import com.cy.commons.vo.Org;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author jason_h
 */
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(
    callSuper = true,
    of = {})
@Data
public class CustomOrg extends Org {

    /**
     *
     */
    private static final long serialVersionUID = -5089794154162582279L;

    private List<CustomOrg> children;

    public CustomOrg(Integer sid) {
        super(sid);
    }

    public CustomOrg(Org org) {
        super(org);
    }
}
