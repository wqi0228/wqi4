/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search.query;

import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;

/**
 * 可閱單據查詢 查詢物件
 *
 * @author kasim
 */
@Data
@NoArgsConstructor
public class ReadableDocsInquireQuery implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4359390186750301839L;
    /**
     * 類別
     */
    private String categorySid;
    /**
     * 單據狀態
     */
    private List<String> statusList;
    /**
     * 主題
     */
    private String theme;
    /**
     * 內容
     */
    private String content;

    private String lazyContent;
    /**
     * 建立區間(起)
     */
    private Date startDate;
    /**
     * 建立區間(訖)
     */
    private Date endDate;
    /**
     * 登入者Sid
     */
    private Integer userSid;

    public void init(Integer userSid) {
        this.userSid = userSid;
        this.categorySid = null;
        this.statusList =
            Lists.newArrayList(
                    WCStatus.NEW_INSTANCE,
                    WCStatus.WAITAPPROVE,
                    WCStatus.APPROVING,
                    WCStatus.RECONSIDERATION,
                    WCStatus.EXEC,
                    WCStatus.EXEC_FINISH,
                    WCStatus.APPROVED)
                .stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
        this.theme = null;
        this.content = null;
        this.startDate = null;
        this.endDate = null;
        this.lazyContent = "";
    }

    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 前一日
     */
    public void changeDateIntervalPreDay() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusDays(1);
        this.startDate = lastDate.toDate();
        this.endDate = lastDate.toDate();
    }
}
