/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.util.pf;

import com.cy.work.connect.web.listener.DataTablePickListCallBack;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @param <T>
 * @author brain0925_liao
 */
public class DataTablePickList<T extends Object> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4397438646869929325L;

    @Getter
    @Setter
    private List<T> targetData;
    @Getter
    @Setter
    private T targetSelected;

    @Getter
    @Setter
    private List<T> sourceData;
    @Getter
    @Setter
    private T sourceSelected;
    @Getter
    @Setter
    private DataTablePickListCallBack dataTablePickListCallBack;

    public DataTablePickList(Class<T> clazz) {
        Preconditions.checkArgument(clazz != null, "clazz must not be null");
        init();
    }

    /**
     * 初始化
     */
    public void init() {
        sourceData = Lists.newArrayList();
        targetData = Lists.newArrayList();
    }

    /**
     * 增加資料
     */
    public void btnAddRow() {
        if (sourceSelected == null) {
            return;
        }
        sourceData.remove(sourceSelected);
        targetData.add(sourceSelected);
        sourceSelected = null;
        targetSelected = null;
        if (dataTablePickListCallBack != null) {
            dataTablePickListCallBack.changePickList();
        }
    }

    /**
     * 增加資料
     */
    public void btnAddAllRow() {
        if (sourceData == null || sourceData.isEmpty()) {
            return;
        }
        targetData.addAll(sourceData);
        sourceData = Lists.newArrayList();
        sourceSelected = null;
        targetSelected = null;
        if (dataTablePickListCallBack != null) {
            dataTablePickListCallBack.changePickList();
        }
    }

    /**
     * 移除資料
     */
    public void btnRemoveRow() {
        if (targetSelected == null) {
            return;
        }
        targetData.remove(targetSelected);
        sourceData.add(targetSelected);
        sourceSelected = null;
        targetSelected = null;
        if (dataTablePickListCallBack != null) {
            dataTablePickListCallBack.changePickList();
        }
    }

    /**
     * 移除資料
     */
    public void btnRemoveAllRow() {
        if (targetData == null || targetData.isEmpty()) {
            return;
        }
        sourceData.addAll(targetData);
        targetData = Lists.newArrayList();
        sourceSelected = null;
        targetSelected = null;
        if (dataTablePickListCallBack != null) {
            dataTablePickListCallBack.changePickList();
        }
    }
}
