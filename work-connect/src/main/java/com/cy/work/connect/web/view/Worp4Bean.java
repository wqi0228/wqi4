/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.PreviousAndNextCallBack;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.components.memo.MemoMaintainComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 備忘錄 - 維護
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class Worp4Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6110024442095153914L;

    @Autowired
    private DisplayController displayController;

    @Getter
    /** 備忘錄 維護主要元件 */
    private MemoMaintainComponent memoMaintainComponent;

    @Autowired
    private TableUpDownBean tableUpDownBean;

    @Getter
    /** 錯誤訊息 */
    private String errorMessage;

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /** */
        private static final long serialVersionUID = 2811727767619969548L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            displayController.update("dlg_errorMessage");
            displayController.showPfWidgetVar("dlg_errorMessage_wv");
        }
    };
    private String url = "";
    private boolean showBackHomeBtn = false;
    private final PreviousAndNextCallBack previousAndNextCallBack = new PreviousAndNextCallBack() {
        /** */
        private static final long serialVersionUID = 3945408076133046446L;

        @Override
        public void doPrevious() {
            try {
                initializeDataTable();
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("doPrevious ERROR：{}", e.getMessage());
                messageCallBack.showMessage(e.getMessage());
            } catch (Exception e) {
                log.warn("doPrevious ERROR", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }

        @Override
        public void doNext() {
            try {
                initializeDataTable();
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("doNext ERROR：{}", e.getMessage());
                messageCallBack.showMessage(e.getMessage());
            } catch (Exception e) {
                log.warn("doNext ERROR", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }
    };

    @PostConstruct
    public void init() {
        try {
            if (!WCMemoCategoryLogicComponent.getInstance().isMemoViewPermission()) {
                this.redirectByNoPermission();
            }
            this.initComponents();

            String memoSid = this.getRequestParameterMap();
            this.initData(memoSid);
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 初始化元件
     */
    private void initComponents() {
        UserViewVO userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        OrgViewVo depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        OrgViewVo compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());

        this.memoMaintainComponent = new MemoMaintainComponent(
                userViewVO,
                depViewVo,
                compViewVo,
                messageCallBack,
                previousAndNextCallBack);
    }

    /**
     * 導向無權限畫面
     */
    private void redirectByNoPermission() {
        try {
            Faces.getExternalContext().redirect("../error/illegal_read_memo.xhtml");
            Faces.getContext().responseComplete();
        } catch (Exception ex) {
            log.warn("導向讀取失敗頁面失敗..." + ex.getMessage());
        }
    }

    /**
     * 取得參數
     *
     * @return
     */
    private String getRequestParameterMap() {
        Preconditions.checkState(
                Faces.getRequestParameterMap().containsKey("memoSid"), "無效參數 無法開備忘錄!!");
        return Faces.getRequestParameterMap().get("memoSid");
    }

    /**
     * 初始化預設資料
     *
     * @param memoSid
     */
    private void initData(String memoSid) {
        boolean showPreviousBtn = !(Strings.isNullOrEmpty(
                tableUpDownBean.getSession_previous_sid()));
        tableUpDownBean.setSession_previous_sid("");
        boolean showNextBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_next_sid()));
        tableUpDownBean.setSession_next_sid("");
        showBackHomeBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_show_home()));
        tableUpDownBean.setSession_show_home("");
        url = tableUpDownBean.getWorp_path();
        tableUpDownBean.setWorp_path("");
        memoMaintainComponent.initData(memoSid, showPreviousBtn, showNextBtn, showBackHomeBtn);
    }

    public void initializeDataTable() {
        long startTime = System.currentTimeMillis();
        while (!tableUpDownBean.isSessionSettingOver()) {
            log.debug("執行上下筆,Session等待執行值,最多等待兩秒");
            try {
                TimeUnit.MICROSECONDS.sleep(50);
                if (System.currentTimeMillis() - startTime > 2000) {
                    tableUpDownBean.clearSessionSettingOver();
                    log.warn("執行上下筆,Session並未等到執行值,最多等待兩秒");
                    break;
                }
            } catch (Exception e) {
                log.warn("Sleep Error", e);
            }
        }
        tableUpDownBean.clearSessionSettingOver();
        boolean showPreviousBtn = !(Strings.isNullOrEmpty(
                tableUpDownBean.getSession_previous_sid()));
        tableUpDownBean.setSession_previous_sid("");
        boolean showNextBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_next_sid()));
        tableUpDownBean.setSession_next_sid("");
        String memoSid = "";
        if (!Strings.isNullOrEmpty(tableUpDownBean.getSession_now_sid())) {
            memoSid = tableUpDownBean.getSession_now_sid();
            tableUpDownBean.setSession_now_sid("");
        }
        if (!Strings.isNullOrEmpty(memoSid)) {
            DisplayController.getInstance()
                    .execute("replaceUrlNotNavigation('" + url + ".xhtml?memoSid=" + memoSid + "')");
        }
        memoMaintainComponent.initData(memoSid, showPreviousBtn, showNextBtn, showBackHomeBtn);
    }
}
