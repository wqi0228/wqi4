package com.cy.work.connect.web.controller;

import com.cy.work.connect.logic.manager.WCTagTempletManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCMenuTag;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author jimmy_chou
 */
@Slf4j
@Component
@FacesConverter("menuTagConverter")
public class MenuTagConverter implements Converter {

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            if ((value != null) && !"".equals(value.trim())) {
                WCMenuTag menuTag = WCTagTempletManager.getInstance()
                    .findMenuTagByWCMenuTagSid(value);
                if (menuTag != null) {
                    return new MenuTagVO(
                        menuTag.getSid(),
                        menuTag.getMenuName(),
                        WCTagTempletManager.getInstance()
                            .findCategoryTagByWCCategoryTagSid(menuTag.getWcCategoryTagSid())
                            .getCategoryName(),
                        "");
                }
            }
        } catch (Exception e) {
            log.warn("MenuTagConverter Error", e);
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof MenuTagVO) {
            return ((MenuTagVO) value).getSid();
        } else {
            return null;
        }
    }
}
