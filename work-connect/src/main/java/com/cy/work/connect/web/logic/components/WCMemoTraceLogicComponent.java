/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.manager.WCMemoTraceManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCMemoTrace;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 追蹤邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCMemoTraceLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6075121947205638830L;

    private static WCMemoTraceLogicComponent instance;
    @Autowired
    private WCMemoTraceManager wcMemoTraceManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCMasterLogicComponents wcMasterLogicComponent;
    // @Value("${work-connect.ap.url}")
    // private String webUrl;
    @Autowired
    private ViewPermissionLogic viewPermissionLogic;

    public static WCMemoTraceLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMemoTraceLogicComponent.instance = this;
    }

    /**
     * 取得追蹤介面物件List By 備忘錄Sid
     *
     * @param userSid 登入者Sid
     * @param memoSid 備忘錄Sid
     * @return
     */
    public List<WCTraceVO> getWCTraceVOsByMemoSid(
            String memoSid, UserViewVO userViewVO, OrgViewVo depViewVo) {
        try {
            List<WCTraceVO> viewTrace = Lists.newArrayList();
            List<WCMemoTrace> results = wcMemoTraceManager.getWCMemoTracesByMemoSid(memoSid);
            int index = 0;
            for (WCMemoTrace item : results) {
                User user = null;
                if (item.getUpdate_usr() != null) {
                    user = userManager.findBySid(item.getUpdate_usr());
                } else {
                    user = userManager.findBySid(item.getCreate_usr());
                }
                String dateTime = "";
                if (item.getUpdate_dt() != null) {
                    dateTime = ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt());
                } else {
                    dateTime = ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt());
                }
                String userInfo = user.getPrimaryOrg().getName() + "-" + user.getName();
                String correctDepFullName = "";
                String correctDepName = "";
                String content = this.getContent(
                        item.getWcMemoTraceType(), item.getWc_trace_content_css(), userViewVO,
                        depViewVo);
                viewTrace.add(
                        new WCTraceVO(
                                index,
                                item.getSid(),
                                item.getWcMemoTraceType(),
                                userInfo,
                                dateTime,
                                content,
                                null,
                                correctDepFullName,
                                correctDepName,
                                item.getCreate_usr().equals(userViewVO.getSid()),
                                item.getCreate_usr()));
                index++;
            }
            return viewTrace;
        } catch (Exception e) {
            log.warn("getWCTraceVOsByMemoSid Error ", e);
        }
        return Lists.newArrayList();
    }

    private String getContent(
            WCTraceType wcMemoTraceType, String contentCss, UserViewVO userViewVO,
            OrgViewVo depViewVo) {
        String content = "";
        // 特殊需求備忘錄轉工作聯絡單時,在追蹤顯示工作聯絡單單號,需即時判斷登入者是否有權限觀看工作聯絡單,故追蹤內容僅記工作聯絡單SID,來即時判斷
        if (wcMemoTraceType.equals(WCTraceType.TRANSTOWORKCONNECT_LINK)) {
            String wcSid = contentCss;
            WCMaster wcMaster = wcMasterLogicComponent.getWCMasterBySid(wcSid);
            
            if (CanViewType.ALL.equals(viewPermissionLogic.checkWorkConnectPermission(wcMaster))) {
                content = "轉至工作聯絡單[<a href=\""
                        + FusionUrlServiceUtils.getUrlByPropKey("work-connect.ap.url")
                        + "/worp/worp_full.xhtml?wrcId="
                        + wcSid
                        + "\" target=\"_blank\"  style=\"color:blue;\" title=\""
                        + wcMaster.getWc_no()
                        + "\">"
                        + wcMaster.getWc_no()
                        + "</a>]";
            } else {
                content = "轉至工作聯絡單[" + wcMaster.getWc_no() + "]";
            }
        } else {
            content = contentCss;
        }
        return content;
    }
}
