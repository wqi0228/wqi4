/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.ApplicationDepInquireColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.ApplicationDepInquireColumnVO;
import com.cy.work.connect.web.view.vo.search.ApplicationDepInquireVO;
import com.cy.work.connect.web.view.vo.search.query.ApplicationDepInquireQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 申請單位查詢 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class ApplicationDepInquireLogicComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5911315178741180857L;

    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.APPLICATION_DEP_INQUIRE;
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;

    /**
     * 取得 申請單位 查詢物件List
     *
     * @param query
     * @param userSids
     * @param sid          工作聯絡單Sid
     * @param loginUserSid
     * @return
     * @throws UserMessageException 
     */
    public List<ApplicationDepInquireVO> search(
            ApplicationDepInquireQuery query,
            String sid,
            Integer loginUserSid) throws UserMessageException {

        // ====================================
        // 查詢
        // ====================================
        // 兜組SQL
        String sql = this.bulidSqlByApplicationDepInquire(query, loginUserSid, sid);

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("申請單據查詢"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> dbRowDataMaps = Lists.newArrayList();

        try {
            dbRowDataMaps = this.jdbc.queryForList(sql);
        } catch (Exception e) {
            String message = "申請單據查詢失敗！" + e.getMessage();
            log.error(message, e);
            log.error("\r\nSQL:【\r\n"
                    + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }

        log.debug(WkCommonUtils.prepareCostMessage(startTime, "申請單據查詢，共[" + dbRowDataMaps.size() + "]筆"));
        if (WkStringUtils.isEmpty(dbRowDataMaps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 解析資料
        // ====================================
        String skypeAlertMessage = "";
        List<ApplicationDepInquireVO> resultVOs = Lists.newArrayList();
        for (Map<String, Object> dbRowDataMap : dbRowDataMaps) {
            try {
                ApplicationDepInquireVO vo = this.transToVO(dbRowDataMap);
                resultVOs.add(vo);
            } catch (Exception e) {
                String message = "申請單據查詢。資料解析失敗!" + e.getMessage();
                log.error(message, e);
                log.error("row data:\r\n" + WkJsonUtils.getInstance().toPettyJson(dbRowDataMap));
                skypeAlertMessage = message;
            }
        }

        if (WkStringUtils.notEmpty(skypeAlertMessage)) {
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(skypeAlertMessage);
        }

        // ====================================
        // 排序
        // ====================================
        return resultVOs.stream()
                .sorted(Comparator.comparing(ApplicationDepInquireVO::getCreateDt).reversed())
                .collect(Collectors.toList());
    }

    /**
     * @param rowDataMap
     * @return
     */
    private ApplicationDepInquireVO transToVO(
            Map<String, Object> rowDataMap) {
        ApplicationDepInquireVO vo = new ApplicationDepInquireVO((String) rowDataMap.get("wc_sid"));
        vo.setWcNo((String) rowDataMap.get("wc_no"));
        vo.setCreateDate(new DateTime(rowDataMap.get("create_dt")).toString("yyyy/MM/dd"));
        vo.setCreateDt((Date) rowDataMap.get("create_dt"));

        MenuTagVO menuTag = menuTagLogicComponent.findToBySid((String) rowDataMap.get("menuTag_sid"));
        if (menuTag != null) {
            vo.setMenuTagName(menuTag.getName());
            vo.setCategoryName(categoryTagLogicComponent
                    .getCategoryTagEditVOBySid(menuTag.getCategorySid())
                    .getCategoryName());
        }

        // 申請人
        vo.setApplicationUserName(WkUserUtils.findNameBySid((Integer) rowDataMap.get("create_usr")));

        if (rowDataMap.get("dep_sid") != null) {
            vo.setDepName(
                    WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeupAndDecorationStyle(
                            (Integer) rowDataMap.get("dep_sid"),
                            OrgLevel.THE_PANEL, false, "－"));
        }

        // 主題
        vo.setTheme(WkStringUtils.removeCtrlChr((String) rowDataMap.get("theme")));
        // 內容
        vo.setContent(WkStringUtils.removeCtrlChr((String) rowDataMap.get("content")));
        // 單據狀態
        vo.setStatusName(WCStatus.safeTransDescr(rowDataMap.get("wc_status") + ""));
        if (rowDataMap.get("category_tag") != null) {
            try {
                String categoryTagStr = (String) rowDataMap.get("category_tag");
                CategoryTagTo ct = WkJsonUtils.getInstance().fromJson(categoryTagStr, CategoryTagTo.class);
                ct.getTagSids().forEach(
                        item -> {
                            vo.bulidExecTag(wcTagManager.getWCTagBySid(item));
                        });
            } catch (Exception e) {
                log.warn("settingObject ERROR", e);
            }
        }
        if (rowDataMap.get("reply_not_read") != null) {
            try {
                String replyNotReadStr = (String) rowDataMap.get("reply_not_read");
                if (!Strings.isNullOrEmpty(replyNotReadStr)) {
                    List<ReplyRecordTo> resultList = WkJsonUtils.getInstance()
                            .fromJsonToList(replyNotReadStr,
                                    ReplyRecordTo.class);
                    if (resultList != null) {
                        List<ReplyRecordTo> s = resultList.stream()
                                .filter(eu -> String.valueOf(SecurityFacade.getUserSid()).equals(eu.getUser()))
                                .collect(Collectors.toList());
                        if (s != null && !s.isEmpty()) {
                            vo.setHasUnReadCount(true);
                            vo.setUnReadCount(s.get(0).getUnReadCount());
                        }
                    }
                }
            } catch (Exception e) {
                log.warn("settingObject ERROR", e);
            }
        }
        return vo;
    }

    /**
     * 建立主要 SQL
     * 
     * @param queryCondition
     * @param loginUserSid
     * @param targetWcSid
     * @return
     */
    private String bulidSqlByApplicationDepInquire(
            ApplicationDepInquireQuery queryCondition, Integer loginUserSid, String targetWcSid) {

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        String searchKeyword = WkStringUtils.safeTrim(queryCondition.getLazyContent());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 為指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(targetWcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT wc.wc_sid, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.create_dt, ");
        sql.append("       wc.menuTag_sid, ");
        sql.append("       wc.create_usr, ");
        sql.append("       wc.dep_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       wc.wc_status, ");
        sql.append("       wc.category_tag, ");
        sql.append("       wc.content, ");
        sql.append("       wc.reply_not_read ");
        sql.append("FROM   wc_master wc ");
        sql.append("WHERE  1=1 ");

        // 申請人
        List<String> cinditionSqls = Lists.newArrayList();
        cinditionSqls.add(" wc.create_usr = " + loginUserSid + " ");
        // 主管權限:可閱管理部門以下單據
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            cinditionSqls.add(" wc.dep_sid IN (" + managerOrgSids.stream().map(depSid -> depSid + "").collect(Collectors.joining(",")) + ")");
        }
        sql.append(cinditionSqls.stream().collect(Collectors.joining(" OR ", " AND (", ")")));

        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(targetWcSid)) {
                sql.append("  AND wc.wc_sid = '" + targetWcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND wc.wc_no = '" + forceWcNo + "' ");
            }
        }

        // 單據狀態
        if (!isForceSearch
                && WkStringUtils.notEmpty(queryCondition.getStatusList())) {
            sql.append(" AND wc.wc_status IN (")
                    .append(queryCondition.getStatusList().stream().collect(Collectors.joining("','", "'", "'")))
                    .append(") ");
        }

        // 單據類別
        if (!isForceSearch
                && WkStringUtils.notEmpty(queryCondition.getCategorySid())) {
            List<WCMenuTag> wcMenuTags = wcMenuTagManager.getWCMenuTagByCategorySid(queryCondition.getCategorySid(), null);
            if (WkStringUtils.notEmpty(wcMenuTags)) {
                String menuTagSids = wcMenuTags.stream()
                        .map(each -> each.getSid())
                        .collect(Collectors.joining("','", "'", "'"));
                sql.append(" AND wc.menuTag_sid in (" + menuTagSids + ") ");
            }
        }

        // 申請人
        if (!isForceSearch
                && WkStringUtils.notEmpty(queryCondition.getApplicationUserName())) {

            sql.append(" AND wc.create_usr ");

            String createUserSidsStr = WkUserUtils.findByNameLike(queryCondition.getApplicationUserName(), SecurityFacade.getCompanyId()).stream()
                    .map(each -> String.valueOf(each))
                    .collect(Collectors.joining(",", " IN (", ") "));

            if (WkStringUtils.notEmpty(createUserSidsStr)) {
                sql.append(createUserSidsStr);
            } else {
                // 找不到相似的 user name 時, 故意清空查詢結果
                sql.append(" = -999 ");
            }
        }

        // 建立區間 - 起日
        if (!isForceSearch
                && queryCondition.getStartDate() != null) {
            sql.append("AND wc.create_dt >= '")
                    .append(new DateTime(queryCondition.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        }
        // 建立區間 - 迄日
        if (!isForceSearch
                && queryCondition.getEndDate() != null) {
            sql.append("AND wc.create_dt <= '")
                    .append(new DateTime(queryCondition.getEndDate()).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }

        // ----------------------------
        // 模糊查詢
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(searchKeyword)) {
            String searchText = WkStringUtils.safeTrim(searchKeyword).toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "wc.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));

            sql.append("      ) ");
        }

        // ----------------------------
        // 排序
        // ----------------------------
        sql.append("ORDER  BY wc.create_dt");

        return sql.toString();
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public ApplicationDepInquireColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        ApplicationDepInquireColumnVO vo = new ApplicationDepInquireColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.INDEX));
            vo.setCreateDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.CREATE_DATE));
            vo.setCategoryName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.CATEGORY_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.MENUTAG_NAME));
            vo.setApplicationUserName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.APPLICATION_USER_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.THEME));
            vo.setStatusName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.STATUS_NAME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ApplicationDepInquireColumn.WC_NO));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.INDEX, columDB.getInfo()));
            vo.setCreateDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCategoryName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setApplicationUserName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.APPLICATION_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.THEME, columDB.getInfo()));
            vo.setStatusName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.STATUS_NAME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ApplicationDepInquireColumn.WC_NO, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 取得 類別(大類) 選項
     *
     * @param loginCompSid
     * @return
     */
    public List<SelectItem> getCategoryItems(Integer loginCompSid) {
        return categoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
    }

    /**
     * 取得 單據狀態 選項
     *
     * @return
     */
    public List<SelectItem> getStatusItems() {
        return Lists.newArrayList(
                WCStatus.NEW_INSTANCE,
                WCStatus.WAITAPPROVE,
                WCStatus.APPROVING,
                WCStatus.RECONSIDERATION,
                WCStatus.APPROVED,
                WCStatus.EXEC,
                WCStatus.EXEC_FINISH,
                WCStatus.CLOSE,
                WCStatus.CLOSE_STOP,
                WCStatus.INVALID)
                .stream()
                .map(each -> new SelectItem(each.name(), each.getVal()))
                .collect(Collectors.toList());
    }
}
