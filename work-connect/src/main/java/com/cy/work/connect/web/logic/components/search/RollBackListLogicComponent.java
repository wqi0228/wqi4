/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.RelationDepHelper;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.RollBackListColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.RollBackListColumnVO;
import com.cy.work.connect.web.view.vo.search.RollBackListVO;
import com.cy.work.connect.web.view.vo.search.query.CheckedListQuery;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 處理清單 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class RollBackListLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1379411868593591535L;

    private static RollBackListLogicComponent instance;
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.ROLLBACK_LIST;

    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private WCTagManager wcTagManager;

    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;

    public static RollBackListLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        RollBackListLogicComponent.instance = this;
    }

    private RollBackListVO settingObject(Map<String, Object> each) {
        String sid = (String) each.get("wc_exec_rollBack_history_sid");
        RollBackListVO rollBackListVO = new RollBackListVO(sid);
        rollBackListVO.setWcSid((String) each.get("wc_sid"));
        rollBackListVO.setWcNo((String) each.get("wc_no"));
        rollBackListVO.setRollBackDate(
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateTime.getValue(), (Date) each.get("rollBack_dt")));
        rollBackListVO.setRollBackDt((Date) each.get("rollBack_dt"));
        MenuTagVO menuTag = menuTagLogicComponent.findToBySid((String) each.get("menuTag_sid"));
        rollBackListVO.setReqDepName(WkOrgUtils.findNameBySid((Integer) each.get("reqDepSid")));
        rollBackListVO.setMenuTagName(menuTag.getName());
        rollBackListVO.setCategoryName(
                categoryTagLogicComponent
                        .getCategoryTagEditVOBySid(menuTag.getCategorySid())
                        .getCategoryName());
        rollBackListVO.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
        rollBackListVO.setContent(WkStringUtils.removeCtrlChr((String) each.get("content")));
        rollBackListVO.setRollBackUserName(WkUserUtils.findNameBySid((Integer) each.get("user_sid")));
        String wcStatusStr = (String) each.get("wc_status");
        WCStatus wcStatus = WCStatus.valueOf(wcStatusStr);
        rollBackListVO.setWcStatus(wcStatus.getVal());
        if (each.get("category_tag") != null) {
            try {
                String categoryTagStr = (String) each.get("category_tag");
                CategoryTagTo ct = WkJsonUtils.getInstance()
                        .fromJson(categoryTagStr, CategoryTagTo.class);
                ct.getTagSids()
                        .forEach(
                                item -> {
                                    rollBackListVO.bulidExecTag(wcTagManager.getWCTagBySid(item));
                                });
            } catch (Exception e) {
                log.warn("settingObject ERROR", e);
            }
        }

        return rollBackListVO;
    }

    private List<RollBackListVO> settingRollBackList(
            CheckedListQuery query,
            String wcSid) {

        // 兜組SQL
        String sql = this.bulidSqlByRollBackList(query, wcSid);

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("退件一覽表查詢"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> dbResults = jdbc.queryForList(sql);
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "退件一覽表查詢，共[" + dbResults.size() + "]筆"));
        if (WkStringUtils.isEmpty(dbResults)) {
            return Lists.newArrayList();
        }

        // 解析資料
        String skypeAlertMessage = "";
        List<RollBackListVO> resultVOs = Lists.newArrayList();
        for (Map<String, Object> dbResultMap : dbResults) {
            try {
                RollBackListVO vo = this.settingObject(dbResultMap);
                resultVOs.add(vo);
            } catch (Exception e) {
                String message = "退件一覽表查詢。資料解析失敗!" + e.getMessage();
                log.error(message, e);
                log.error("row data:\r\n" + WkJsonUtils.getInstance().toPettyJson(dbResultMap));
                skypeAlertMessage = message;
            }
        }

        if (WkStringUtils.notEmpty(skypeAlertMessage)) {
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(skypeAlertMessage);
        }

        return resultVOs;

    }

    /**
     * 取得 處理清單 查詢物件List
     *
     * @param query
     * @param depSids
     * @param wcSid   工作聯絡單Sid
     * @return
     */
    public List<RollBackListVO> search(CheckedListQuery query, String wcSid) {

        // ====================================
        // 查詢
        // ====================================
        List<RollBackListVO> res = this.settingRollBackList(query, wcSid);
        if (res == null || res.isEmpty()) {
            return Lists.newArrayList();
        }

        // ====================================
        // 以退件日期排序
        // ====================================
        return res.stream()
                .sorted(Comparator.comparing(RollBackListVO::getRollBackDt))
                .collect(Collectors.toList());
    }

    /**
     * 是否有曾經退回的權限
     * 
     * @param wcSid
     * @return
     */
    public boolean isViewPermissionByRollBack(String wcSid) {
        String sql = "SELECT count(*) FROM (" + this.bulidSqlByRollBackList(null, wcSid) + ") AS aa;";
        Long count = jdbc.queryForObject(sql, Long.class);
        return count > 0;
    }

    /**
     * 建立主要 SQL
     *
     * @param query
     * @param canViewDepSids
     * @return
     */
    public String bulidSqlByRollBackList(
            CheckedListQuery query,
            String wcSid) {

        // 防呆?
        if (query == null) {
            query = new CheckedListQuery();
            query.init();
        }

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        String searchKeyword = WkStringUtils.safeTrim(query.getLazyContent());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 為指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(wcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 取得可閱部門
        // ====================================
        // 取得登入者
        Integer loginUserSid = SecurityFacade.getUserSid();

        // 取得可閱部門
        Set<Integer> canViewDepSids = RelationDepHelper.findRelationOrgSidsForRejectExecCanView(loginUserSid);
        String canViewDepSidsCondition = "";
        if (WkStringUtils.notEmpty(canViewDepSids)) {
            canViewDepSidsCondition = canViewDepSids.stream()
                    .map(depSid -> " werh.exec_dep like '%\"" + depSid + "\"%' ")
                    .collect(Collectors.joining(" OR "));
        }

        // ====================================
        // 組SQL
        // ====================================
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        sql.append("    werh.wc_exec_rollBack_history_sid,"
                + "     wc.wc_sid ,"
                + "     wc.wc_no,wc.menuTag_sid,"
                + "     wc.theme,werh.user_sid,"
                + "     werh.rollBack_dt,"
                + "     wc.wc_status,"
                + "     wc.category_tag,"
                + "     wc.dep_sid as reqDepSid,"
                + "     wc.content "
                + " ");
        sql.append("FROM wc_exec_rollBack_history werh ");
        sql.append(" INNER JOIN wc_master wc "
                + "  ON  wc.wc_sid = werh.wc_sid ");
        sql.append("WHERE 1 = 1");

        sql.append(" AND ( ");

        // ----------------------------
        // 限制條件
        // ----------------------------
        // 退件者要看的到 (WORKCOMMU-518)
        sql.append("       werh.user_sid = " + loginUserSid + " ");

        sql.append("        OR ( ");
        // 僅領單人員可閱，檢查是否在任何一個執行單位名單 (有領單或分派後, 才會在 werh.exec_dep)
        sql.append("            EXISTS (SELECT 1  ");
        sql.append("                     FROM wc_exec_dep execDep ");
        sql.append("                    WHERE execDep.status = 0  ");
        sql.append("                      AND execDep.wc_sid = wc.wc_sid ");
        sql.append("                      AND execDep.receviceUser like  '%{\"sid\":" + loginUserSid + "}%'  ");
        sql.append("                      AND werh.exec_dep like CONCAT('%\"', execDep.dep_sid, '\"%')) ");

        // 無任何單位為非僅領單人員可閱
        sql.append("            OR ( ");
        sql.append("                NOT EXISTS (SELECT 1  ");
        sql.append("                             FROM wc_exec_dep execDep ");
        sql.append("                            WHERE execDep.status = 0 ");
        sql.append("                              AND execDep.wc_sid = wc.wc_sid  ");
        sql.append("                              AND (execDep.receviceUser IS NOT NULL AND execDep.receviceUser like '%sid%')) "); // execDep.receviceUser 中, 無出現 sid 字串
        sql.append("                AND ( " + canViewDepSidsCondition + " )");
        sql.append("               ) ");
        sql.append("          ) ");

        sql.append(" )");

        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(wcSid)) {
                sql.append("  AND wc.wc_sid = '" + wcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND wc.wc_no = '" + forceWcNo + "' ");
            }
        }

        // ----------------------------
        // 查詢條件：單據類別
        // ----------------------------
        if (!isForceSearch
                && WkStringUtils.notEmpty(query.getCategorySid())) {
            List<WCMenuTag> wcMenuTags = wcMenuTagManager.getWCMenuTagByCategorySid(query.getCategorySid(), null);
            if (WkStringUtils.notEmpty(wcMenuTags)) {
                String menuTagSids = wcMenuTags.stream()
                        .map(each -> each.getSid())
                        .collect(Collectors.joining("','", "'", "'"));
                sql.append(" AND wc.menuTag_sid in (" + menuTagSids + ") ");
            }
        }

        // ----------------------------
        // 建立區間
        // ----------------------------
        // 起日
        if (!isForceSearch
                && query.getStartDate() != null) {
            sql.append("AND wc.create_dt >= '")
                    .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        }
        // 迄日
        if (!isForceSearch
                && query.getEndDate() != null) {
            sql.append("AND wc.create_dt <= '")
                    .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }

        // ----------------------------
        // 模糊查詢
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(searchKeyword)) {
            String searchText = WkStringUtils.safeTrim(searchKeyword).toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "wc.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));

            sql.append("      ) ");
        }

        sql.append(" ORDER BY werh.rollBack_dt DESC ");
        return new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString());
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public RollBackListColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        RollBackListColumnVO vo = new RollBackListColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(RollBackListColumn.INDEX));
            vo.setRollBackDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            RollBackListColumn.ROLLBACK_DATE));
            vo.setCategoryName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            RollBackListColumn.CATEGORY_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            RollBackListColumn.MENUTAG_NAME));
            vo.setRollBackName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            RollBackListColumn.ROLLBACK_USER_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(RollBackListColumn.THEME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(RollBackListColumn.WC_NO));
            vo.setWcStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(RollBackListColumn.WC_STATUS));
            vo.setReqDep(
                    customColumnLogicComponent.createDefaultColumnDetail(RollBackListColumn.REQ_DEP));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.INDEX, columDB.getInfo()));
            vo.setRollBackDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.ROLLBACK_DATE, columDB.getInfo()));
            vo.setCategoryName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setRollBackName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.ROLLBACK_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.THEME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.WC_NO, columDB.getInfo()));
            vo.setWcStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.WC_STATUS, columDB.getInfo()));
            vo.setReqDep(
                    customColumnLogicComponent.transToColumnDetailVO(
                            RollBackListColumn.REQ_DEP, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 取得 類別(大類) 選項
     */
    public List<SelectItem> getCategoryItems(Integer loginCompSid) {
        return categoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
    }
}
