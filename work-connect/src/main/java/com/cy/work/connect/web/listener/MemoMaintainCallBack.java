/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import java.io.Serializable;

/**
 * 類別樹 元件 CallBack
 *
 * @author kasim
 */
public class MemoMaintainCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2191552284025440130L;

    public void showMessage(String message) {
    }

    public void clickEdit() {
    }

    public void clickSubmite() {
    }

    public void clickCancel() {
    }

    public void clickAtt() {
    }

    public void clickTrans() {
    }

    public void clickUp() {
    }

    public void clickDown() {
    }
}
