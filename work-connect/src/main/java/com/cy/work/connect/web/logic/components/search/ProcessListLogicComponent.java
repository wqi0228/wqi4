/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.ProcessListColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.ProcessListColumnVO;
import com.cy.work.connect.web.view.vo.search.ProcessListVO;
import com.cy.work.connect.web.view.vo.search.query.ProcessListQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 處理清單 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class ProcessListLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8674562216594270849L;

    private static ProcessListLogicComponent instance;
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.PROCESS_LIST;
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;

    public static ProcessListLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        ProcessListLogicComponent.instance = this;
    }

    public Integer count(Integer userSid) {

        ProcessListQuery query = new ProcessListQuery();
        query.init(userSid + "");

        // 組查詢SQL
        String sql = this.bulidSqlByProcessList(query, Lists.newArrayList(userSid + ""), "");

        sql = "SELECT Count(*) FROM (SELECT aa.wc_sid FROM (" + sql + ") AS aa GROUP BY aa.wc_sid) AS BB";

        try {
            return jdbc.queryForObject(sql, Integer.class);
        } catch (Exception e) {
            String message = "個人待處理清單數量計算失敗![" + e.getMessage() + "]";
            log.error(message, e);
            log.error("個人待處理清單數量計算SQL："
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
        }

        return 0;
    }

    /**
     * 取得 處理清單 查詢物件List
     *
     * @param query
     * @param canSelectUserSids
     * @param loginUserSid
     * @param sid               工作聯絡單Sid
     * @return
     */
    public List<ProcessListVO> search(
            ProcessListQuery query, List<String> canSelectUserSids, String sid, Integer loginUserSid) {

        // ====================================
        // 查詢
        // ====================================

        // 組查詢SQL
        String sql = this.bulidSqlByProcessList(query, canSelectUserSids, sid);

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("個人待處理清單"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> resultRowDataMaps = jdbc.queryForList(sql.toString());
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "個人待處理清單，共[" + resultRowDataMaps.size() + "]筆"));
        if (WkStringUtils.isEmpty(resultRowDataMaps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        Map<String, ProcessListVO> results = Maps.newHashMap();
        List<String> wcSidList = Lists.newArrayList(); // 記錄wcSid用

        for (Map<String, Object> resultRowDataMap : resultRowDataMaps) {

            String wcSid = (String) resultRowDataMap.get("wc_sid");
            wcSidList.add(wcSid);
            ProcessListVO processListVO = null;
            if (!results.containsKey(wcSid)) {
                processListVO = new ProcessListVO(wcSid);
                processListVO.setWcSid((String) resultRowDataMap.get("wc_sid"));
                processListVO.setWcNo((String) resultRowDataMap.get("wc_no"));

                processListVO.setCreateDate(new DateTime(resultRowDataMap.get("create_dt")).toString("yyyy/MM/dd"));
                processListVO.setCreateDt((Date) resultRowDataMap.get("create_dt"));
                processListVO.setReqDepName(WkOrgUtils.findNameBySid((Integer) resultRowDataMap.get("reqDepSid")));
                MenuTagVO menuTag = menuTagLogicComponent.findToBySid((String) resultRowDataMap.get("menuTag_sid"));
                processListVO.setMenuTagName(menuTag.getName());
                processListVO.setCategoryName(categoryTagLogicComponent.getCategoryTagEditVOBySid(menuTag.getCategorySid()).getCategoryName());
                processListVO.setTheme(WkStringUtils.removeCtrlChr((String) resultRowDataMap.get("theme")));
                processListVO.setContent(WkStringUtils.removeCtrlChr((String) resultRowDataMap.get("content")));
                if (resultRowDataMap.get("reply_not_read") != null) {
                    try {
                        String replyNotReadStr = (String) resultRowDataMap.get("reply_not_read");
                        if (!Strings.isNullOrEmpty(replyNotReadStr)) {
                            List<ReplyRecordTo> resultList = WkJsonUtils.getInstance().fromJsonToList(replyNotReadStr,
                                    ReplyRecordTo.class);
                            if (resultList != null) {
                                List<ReplyRecordTo> s = resultList.stream()
                                        .filter(eu -> String.valueOf(loginUserSid).equals(eu.getUser()))
                                        .collect(Collectors.toList());
                                if (s != null && !s.isEmpty()) {
                                    processListVO.setHasUnReadCount(true);
                                    processListVO.setUnReadCount(s.get(0).getUnReadCount());
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.warn("settingObject ERROR", e);
                    }
                }

            } else {
                processListVO = results.get(wcSid);
            }
            String wc_exec_dep_setting_sid = (String) resultRowDataMap.get(
                    "wc_exec_dep_setting_sid");
            if (!Strings.isNullOrEmpty(wc_exec_dep_setting_sid)) {
                WCExecDepSetting execDepSetting = execDepSettingManager.findBySidFromCache(
                        wc_exec_dep_setting_sid);
                if (execDepSetting != null) {
                    WCTag wcTag = wcTagManager.getWCTagBySid(
                            execDepSetting.getWc_tag_sid());
                    processListVO.bulidExecTag(wcTag);
                }
            }
            processListVO.bulidExecUser(userManager.findBySid((Integer) resultRowDataMap.get("exec_user_sid")));

            results.put(wcSid, processListVO);
        }

        // ====================================
        // 轉 list 並以建立日期排序
        // ====================================
        List<ProcessListVO> res = results.entrySet().stream()
                .map(each -> each.getValue())
                .sorted(Comparator.comparing(ProcessListVO::getCreateDt))
                .collect(Collectors.toList());

        return res;
    }

    private String bulidSqlByProcessList(
            ProcessListQuery queryCondition, List<String> canSelectUserSids, String targetWcSid) {

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        String searchKeyword = WkStringUtils.safeTrim(queryCondition.getLazyContent());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 為指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(targetWcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 組執行人員
        // ====================================
        // 為傳入時, 強制使其查不到
        String execUserSidCondition = " = -999 ";
        if (WkStringUtils.notEmpty(canSelectUserSids)) {
            execUserSidCondition = canSelectUserSids.stream()
                    .distinct()
                    .collect(Collectors.joining(",", "IN (", ")"));
        }

        // ====================================
        // 組SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT execDep.wc_exec_dep_sid, ");
        sql.append("       wc.wc_sid, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.create_dt, ");
        sql.append("       wc.menuTag_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       execDep.exec_user_sid, ");
        sql.append("       execDep.wc_exec_dep_setting_sid, ");
        sql.append("       wc.dep_sid AS reqDepSid, ");
        sql.append("       wc.content, ");
        sql.append("       wc.reply_not_read ");
        sql.append("FROM   wc_master wc ");
        sql.append("       INNER JOIN (SELECT execDep.wc_exec_dep_sid, ");
        sql.append("                          execDep.wc_exec_dep_setting_sid, ");
        sql.append("                          execDep.wc_sid, ");
        sql.append("                          execDep.exec_user_sid ");
        sql.append("                   FROM   wc_exec_dep execDep ");
        sql.append("                   WHERE  execDep.status = 0 ");
        // 限制執行人員
        sql.append("                          AND execDep.exec_user_sid " + execUserSidCondition + " ");
        // 執行狀態
        sql.append("                          AND execDep.exec_dep_status = '" + queryCondition.getExceStatus() + "') execDep ");
        sql.append("               ON wc.wc_sid = execDep.wc_sid ");

        sql.append("WHERE  1 = 1 ");

        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(targetWcSid)) {
                sql.append("  AND wc.wc_sid = '" + targetWcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND wc.wc_no = '" + forceWcNo + "' ");
            }
        }

        // ----------------------------
        // 單據類別
        // ----------------------------
        if (!isForceSearch
                && WkStringUtils.notEmpty(queryCondition.getCategorySid())) {
            List<WCMenuTag> wcMenuTags = wcMenuTagManager.getWCMenuTagByCategorySid(queryCondition.getCategorySid(), null);
            if (WkStringUtils.notEmpty(wcMenuTags)) {
                String menuTagSids = wcMenuTags.stream()
                        .map(each -> each.getSid())
                        .collect(Collectors.joining("','", "'", "'"));
                sql.append(" AND wc.menuTag_sid in (" + menuTagSids + ") ");
            }
        }

        // ----------------------------
        // 建立區間
        // ----------------------------
        // 起日
        if (!isForceSearch
                && queryCondition.getStartDate() != null) {
            sql.append("AND wc.create_dt >= '")
                    .append(new DateTime(queryCondition.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        }
        // 迄日
        if (!isForceSearch
                && queryCondition.getEndDate() != null) {
            sql.append("AND wc.create_dt <= '")
                    .append(new DateTime(queryCondition.getEndDate()).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }

        // ----------------------------
        // 模糊查詢
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(searchKeyword)) {
            String searchText = WkStringUtils.safeTrim(searchKeyword).toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "wc.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));

            sql.append("      ) ");
        }

        sql.append("ORDER  BY wc.create_dt ASC");

        return sql.toString();

    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public ProcessListColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        ProcessListColumnVO vo = new ProcessListColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(ProcessListColumn.INDEX));
            vo.setCreateDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ProcessListColumn.CREATE_DATE));
            vo.setCategoryName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ProcessListColumn.CATEGORY_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ProcessListColumn.MENUTAG_NAME));
            vo.setExecUserName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ProcessListColumn.EXEC_USER_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(ProcessListColumn.THEME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(ProcessListColumn.WC_NO));
            vo.setReqDep(
                    customColumnLogicComponent.createDefaultColumnDetail(ProcessListColumn.REQ_DEP));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.INDEX, columDB.getInfo()));
            vo.setCreateDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCategoryName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setExecUserName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.EXEC_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.THEME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.WC_NO, columDB.getInfo()));
            vo.setReqDep(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ProcessListColumn.REQ_DEP, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 取得 類別(大類) 選項
     *
     * @param loginCompSid
     * @return
     */
    public List<SelectItem> getCategoryItems(Integer loginCompSid) {
        return categoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
    }

    /**
     * 取得 執行人員 選項
     *
     * @param depSid
     * @param userSid
     * @param userName
     * @return
     */
    public List<SelectItem> getExecUserItems(Integer depSid, Integer userSid, String userName) {

        // ====================================
        // 取得管理單位以下所有 User (不排除停用User)
        // ====================================
        // 取得非停用管理單位
        // 排除停用單位, 避免取到停用單位下層單位的主管
        Set<Integer> avtiveManagerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(userSid).stream()
                .filter(orgSid -> WkOrgUtils.isActive(orgSid))
                .collect(Collectors.toSet());

        // 取得管理單位的 user (含以下)
        List<User> members = WkUserCache.getInstance().findUserWithManagerByOrgSids(avtiveManagerOrgSids, null);

        // ====================================
        // 列表User
        // ====================================
        Set<User> execUsers = Sets.newHashSet();

        // 加入登入者本身
        User loginUser = WkUserCache.getInstance().findBySid(userSid);
        if (loginUser != null) {
            execUsers.add(WkUserCache.getInstance().findBySid(userSid));
        }

        // 加入管理的user
        if (WkStringUtils.notEmpty(members)) {
            execUsers.addAll(members);
        }

        // ====================================
        // 轉為select item
        // ====================================
        List<SelectItem> resultItems = execUsers.stream()
                .sorted(Comparator.comparing(User::getName))
                .map(user -> new SelectItem(user.getSid() + "", prepareExecUserName(user), null, false, false))
                .collect(Collectors.toList());

        // 可選擇 user 有登入者以外其他的，才多出全部的選項
        if (resultItems.size() >= 2) {
            resultItems.add(0, new SelectItem("", "全部"));
        }

        return resultItems;

    }

    private String prepareExecUserName(User user) {
        String userName = WkUserUtils.safetyGetName(user);
        if (!WkUserUtils.isActive(user)) {
            userName += "&nbsp;&nbsp;";
            userName += WkHtmlUtils.addRedBlodClass("(停用)");
        }
        return userName;
    }

    /**
     * 取得 執行狀態 選項
     *
     * @return
     */
    public List<SelectItem> getExceStatusItems() {
        return Lists.newArrayList(WCExceDepStatus.PROCEDD, WCExceDepStatus.FINISH).stream()
                .map(each -> new SelectItem(each, each.getValue()))
                .collect(Collectors.toList());
    }
}
