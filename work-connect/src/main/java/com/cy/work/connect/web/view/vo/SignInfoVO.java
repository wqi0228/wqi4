/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.google.common.collect.Lists;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 會簽物件
 *
 * @author brain0925_liao
 */
public class SignInfoVO {

    /**
     * 欲移除會簽物件List
     */
    @Getter
    private final List<SingleManagerSignInfo> removeSingleManagerSignInfos = Lists.newArrayList();
    /**
     * 欲增加會簽使用者Sid List
     */
    @Getter
    private final List<Integer> addUserFlowSid = Lists.newArrayList();

    @Getter
    @Setter
    private Integer groupSeq;
}
