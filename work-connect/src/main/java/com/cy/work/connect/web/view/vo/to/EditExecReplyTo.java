/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.to;

import com.cy.work.connect.vo.converter.to.ExecDepRecordTo;
import java.io.Serializable;
import lombok.Data;

/**
 * 執行回覆自訂物件
 *
 * @author kasim
 */
@Data
public class EditExecReplyTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6155695820050732412L;

    private String sid;

    /**
     * 執行單位紀錄
     */
    private ExecDepRecordTo execDepRecordTo;

    /**
     * 登入資訊
     */
    private String loginInfo;

    /**
     * 回覆時間
     */
    private String updateDate;

    /**
     * 回覆內容CSS
     */
    private String contentCss;
}
