/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.hibernate.engine.jdbc.internal.BasicFormatterImpl;
import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.ReadableDocsInquireColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.ReadableDocsInquireColumnVO;
import com.cy.work.connect.web.view.vo.search.ReadableDocsInquireVO;
import com.cy.work.connect.web.view.vo.search.query.ReadableDocsInquireQuery;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 可閱單據查詢 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class ReadableDocsInquireLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8813958297326639210L;

    private static ReadableDocsInquireLogicComponent instance;
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.READABLE_DOCS_INQUIRE;
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;

    public static ReadableDocsInquireLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        ReadableDocsInquireLogicComponent.instance = this;
    }

    /**
     * 取得 可閱單據 查詢物件List
     *
     * @param query
     * @param depSids
     * @param sid               工作聯絡單Sid
     * @param readReceiptStatus
     * @return
     */
    public List<ReadableDocsInquireVO> search(
            ReadableDocsInquireQuery query,
            List<Integer> depSids,
            String sid,
            WCReadReceiptStatus readReceiptStatus) {

        // ====================================
        // 防呆
        // ====================================
        if (depSids == null || depSids.isEmpty()) {
            return Lists.newArrayList();
        }

        if (WkStringUtils.isEmpty(query.getStatusList())) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢
        // ====================================
        // 兜組SQL
        String sql = this.bulidSqlByReadableDocsInquire(query, depSids, sid, readReceiptStatus);

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("可閱單據查詢"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> resultRowDataMaps = jdbc.queryForList(sql.toString());
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "可閱單據查詢，共[" + resultRowDataMaps.size() + "]筆"));
        if (WkStringUtils.isEmpty(resultRowDataMaps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================

        List<ReadableDocsInquireVO> result = Lists.newArrayList();
        for (Map<String, Object> each : resultRowDataMaps) {
            ReadableDocsInquireVO readableDocsInquireVO = new ReadableDocsInquireVO((String) each.get("wc_sid"));
            result.add(readableDocsInquireVO);

            readableDocsInquireVO.setWcNo((String) each.get("wc_no"));
            readableDocsInquireVO.setCreateDate(
                    new DateTime(each.get("create_dt")).toString("yyyy/MM/dd"));
            readableDocsInquireVO.setCreateDt((Date) each.get("create_dt"));
            MenuTagVO menuTag = menuTagLogicComponent.findToBySid((String) each.get("menuTag_sid"));
            readableDocsInquireVO.setMenuTagName(menuTag.getName());
            readableDocsInquireVO.setCategoryName(
                    categoryTagLogicComponent
                            .getCategoryTagEditVOBySid(menuTag.getCategorySid())
                            .getCategoryName());
            readableDocsInquireVO.setApplicationUserName(WkUserUtils.findNameBySid((Integer) each.get("create_usr")));
            readableDocsInquireVO.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
            readableDocsInquireVO.setContent(WkStringUtils.removeCtrlChr((String) each.get("content")));
            WCStatus wcStatus = WCStatus.valueOf((String) each.get("wc_status"));
            if (each.get("category_tag") != null) {
                try {
                    String categoryTagStr = (String) each.get("category_tag");
                    CategoryTagTo ct = WkJsonUtils.getInstance().fromJson(categoryTagStr, CategoryTagTo.class);
                    ct.getTagSids()
                            .forEach(
                                    item -> {
                                        readableDocsInquireVO.bulidExecTag(wcTagManager.getWCTagBySid(item));
                                    });
                } catch (Exception e) {
                    log.warn("settingObject ERROR", e);
                }
            }

            readableDocsInquireVO.replaceReadStatus(WCReadReceiptStatus.UNREAD);
            if (each.get("read_record") != null) {
                try {
                    String readRecord = (String) each.get("read_record");
                    List<RRcordTo> resultLists = WkJsonUtils.getInstance().fromJsonToList(readRecord, RRcordTo.class);
                    if (resultLists.stream().anyMatch(e -> WkCommonUtils.compareByStr(SecurityFacade.getUserSid(), e.getReader() + ""))) {
                        readableDocsInquireVO.replaceReadStatus(WCReadReceiptStatus.READ);
                    }
                } catch (Exception e) {
                    log.warn("settingObject ERROR", e);
                }
            }

            readableDocsInquireVO.setStatusName(wcStatus.getVal());
        }

        return result;

    }

    /**
     * 根據已未讀狀態判斷顯示顯色(待閱讀顯示紅色其他顯示黑色)
     *
     * @param wcReadStatus 已未讀狀態
     * @return
     */
    public String transToColorCss(WCReadReceiptStatus wcReadStatus) {
        if (wcReadStatus == null || WCReadReceiptStatus.UNREAD.equals(wcReadStatus)) {
            return "color: red;";
        }
        return "";
    }

    /**
     * 建立主要 Sql
     *
     * @param query
     * @param depSids
     * @return
     */
    private String bulidSqlByReadableDocsInquire(
            ReadableDocsInquireQuery query,
            List<Integer> depSids,
            String wcSid,
            WCReadReceiptStatus readReceiptStatus) {

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        String searchKeyword = WkStringUtils.safeTrim(query.getLazyContent());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 為指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(wcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 組SQL
        // ====================================
        StringBuffer sql = new StringBuffer();

        sql.append("SELECT wc.wc_sid, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.create_dt, ");
        sql.append("       wc.menuTag_sid, ");
        sql.append("       wc.create_usr, ");
        sql.append("       wc.theme, ");
        sql.append("       wc.wc_status, ");
        sql.append("       wc.category_tag, ");
        sql.append("       wc.content, ");
        sql.append("       wc.read_record ");
        sql.append("FROM   wc_master wc ");

        sql.append("WHERE   ");

        // ----------------------------
        // 限制條件：登入者需存在於可閱清單
        // ----------------------------
        sql.append("           EXISTS (SELECT rpt.wc_sid ");
        sql.append("                       FROM wc_read_receipts rpt ");
        sql.append("                      WHERE rpt.wc_sid = wc.wc_sid ");
        sql.append("                        AND rpt.reader = " + query.getUserSid() + "  ");
        if (readReceiptStatus != null) {
            sql.append("                    AND  rpt.readreceipt = '" + readReceiptStatus.name() + "' ");
        }
        sql.append("                      ) ");

        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(wcSid)) {
                sql.append("  AND wc.wc_sid = '" + wcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND wc.wc_no = '" + forceWcNo + "' ");
            }
        }

        // ----------------------------
        // 查詢條件：單據狀態
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(query.getStatusList())) {
            sql.append(" AND wc.wc_status in (")
                    .append(
                            query.getStatusList().stream()
                                    .map(each -> each)
                                    .collect(Collectors.joining("','", "'", "'")))
                    .append(") ");
        }

        // ----------------------------
        // 查詢條件：部門
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(depSids)) {
            String depSidParam = depSids.stream().map(depSid -> String.valueOf(depSid)).collect(Collectors.joining(","));
            sql.append("  AND  wc.dep_sid IN ( " + depSidParam + ") ");
        }

        // ----------------------------
        // 查詢條件：單據類別
        // ----------------------------
        if (!isForceSearch
                && WkStringUtils.notEmpty(query.getCategorySid())) {
            List<WCMenuTag> wcMenuTags = wcMenuTagManager.getWCMenuTagByCategorySid(query.getCategorySid(), null);
            if (WkStringUtils.notEmpty(wcMenuTags)) {
                String menuTagSids = wcMenuTags.stream()
                        .map(each -> each.getSid())
                        .collect(Collectors.joining("','", "'", "'"));
                sql.append(" AND wc.menuTag_sid in (" + menuTagSids + ") ");
            }
        }

        // ----------------------------
        // 建立區間
        // ----------------------------
        // 起日
        if (!isForceSearch
                && query.getStartDate() != null) {
            sql.append("AND wc.create_dt >= '")
                    .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        }
        // 迄日
        if (!isForceSearch
                && query.getEndDate() != null) {
            sql.append("AND wc.create_dt <= '")
                    .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }

        // ----------------------------
        // 模糊查詢
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(searchKeyword)) {
            String searchText = WkStringUtils.safeTrim(searchKeyword).toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "wc.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));

            sql.append("      ) ");
        }

        sql.append(" ORDER BY wc.create_dt ASC ");

        // 格式化
        return new BasicFormatterImpl().format(sql.toString());
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public ReadableDocsInquireColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        ReadableDocsInquireColumnVO vo = new ReadableDocsInquireColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.INDEX));
            vo.setCreateDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.CREATE_DATE));
            vo.setCategoryName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.CATEGORY_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.MENUTAG_NAME));
            vo.setApplicationUserName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.APPLICATION_USER_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.THEME));
            vo.setStatusName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.STATUS_NAME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.WC_NO));
            vo.setReadStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            ReadableDocsInquireColumn.READ_STATUS));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.INDEX, columDB.getInfo()));
            vo.setCreateDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCategoryName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setApplicationUserName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.APPLICATION_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.THEME, columDB.getInfo()));
            vo.setStatusName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.STATUS_NAME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.WC_NO, columDB.getInfo()));
            vo.setReadStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            ReadableDocsInquireColumn.READ_STATUS, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 取得 類別(大類) 選項
     *
     * @param loginCompSid
     * @return
     */
    public List<SelectItem> getCategoryItems(Integer loginCompSid) {
        return categoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
    }

    /**
     * 取得 讀取狀態 選項
     *
     * @return
     */
    public List<SelectItem> getReadStatusItems() { return Lists.newArrayList(
            new SelectItem(WCReadReceiptStatus.UNREAD, "待閱讀"),
            new SelectItem(WCReadReceiptStatus.READ, "已閱讀")); }

    /**
     * 取得 單據狀態 選項
     *
     * @return
     */
    public List<SelectItem> getStatusItems() {
        return Lists.newArrayList(
                WCStatus.NEW_INSTANCE,
                WCStatus.WAITAPPROVE,
                WCStatus.APPROVING,
                WCStatus.RECONSIDERATION,
                WCStatus.APPROVED,
                WCStatus.EXEC,
                WCStatus.EXEC_FINISH,
                WCStatus.CLOSE,
                WCStatus.CLOSE_STOP)
                .stream()
                .map(each -> new SelectItem(each.name(), each.getVal()))
                .collect(Collectors.toList());
    }
}
