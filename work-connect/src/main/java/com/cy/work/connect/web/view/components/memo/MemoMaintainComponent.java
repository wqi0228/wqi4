/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components.memo;

import com.cy.commons.enums.OrgLevel;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.common.attachment.AttachmentCondition;
import com.cy.work.connect.web.common.attachment.WCMemoAttachmentCompant;
import com.cy.work.connect.web.listener.CategoryTreeCallBack;
import com.cy.work.connect.web.listener.MemoMaintainCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.PreviousAndNextCallBack;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.listener.UploadAttCallBack;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.components.CategoryTreeComponent;
import com.cy.work.connect.web.view.components.WCMemoAttachMaintainCompant;
import com.cy.work.connect.web.view.components.WCMemoTraceComponent;
import com.cy.work.connect.web.view.enumtype.TabType;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.to.MemoMasterTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;

/**
 * 備忘錄 維護主要元件
 *
 * @author kasim
 */
@Slf4j
public class MemoMaintainComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -839734546088942359L;
    /**
     * 有效分鐘
     */
    private final int effectiveMinute = 10;
    /**
     * 日期格式
     */
    private final String lockDateFormat = "yyyy-MM-dd HH:mm:ss";
    private final MemoLogicComponent memoLogicComponent;
    private final DisplayController displayController;
    private final MessageCallBack messageCallBack;
    /**
     * 訊息呼叫
     */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /** */
        private static final long serialVersionUID = 839203551654847080L;

        @Override
        public void showMessage(String message) {
            messageCallBack.showMessage(message);
        }

        @Override
        public void onNodeSelect() {
        }
    };
    /**
     * 備忘錄附件元件
     */
    @Getter
    private final WCMemoAttachMaintainCompant wcMemoAttachMaintainCompant;
    /**
     * 備忘錄追蹤元件
     */
    @Getter
    private final WCMemoTraceComponent wcMemoTraceComponent;
    @Getter
    /** 備忘錄 按鈕 */
    private final MemoBtnComponent memoBtnComponent;
    @Getter
    /** 備忘錄 表頭 */
    private final MemoHeaderComponent memoHeaderComponent;
    @Getter
    /** 備忘錄 表頭 */
    private final MemoDataComponent memoDataComponent;
    @Getter
    /** 備忘錄 鎖定資料 */
    private final MemoLockComponent memoLockComponent;
    @Getter
    /** 備忘錄 上傳附件元件 */
    private final WCMemoAttachmentCompant memoAttachemtComponent;
    /**
     * 登入者
     */
    private final UserViewVO userViewVO;
    /**
     * 登入單位
     */
    private final OrgViewVo depViewVo;
    /**
     * 登入公司
     */
    private final OrgViewVo compVieVo;
    @Getter
    private final CategoryTreeComponent categoryTreeComponent;
    private final WCCategoryTagLogicComponent categoryTagLogicComponent = WCCategoryTagLogicComponent.getInstance();
    private final PreviousAndNextCallBack previousAndNextCallBack;
    boolean showPreviousBtn = false;
    boolean showNextBtn = false;
    boolean showBackListBtn = false;
    /**
     * 當前元件操作的備忘錄Sid
     */
    private String memoSid;
    /**
     * Tab Index
     */
    @Getter
    @Setter
    private int tabIndex = 0;
    /**
     * Tab 是否顯示
     */
    @Getter
    private boolean showBottomInfoTab = false;
    /**
     * 頁簽Reload CallBack
     */
    public final TabCallBack tabCallBack = new TabCallBack() {
        /** */
        private static final long serialVersionUID = -6634235234097690934L;

        @Override
        public void reloadAttTab() {
            wcMemoAttachMaintainCompant.loadData();
        }

        @Override
        public void reloadTraceTab() {
            wcMemoTraceComponent.loadData();
        }

        @Override
        public void reloadTabView(TabType tabType) {
            loadTabIndex(tabType);
            showBottomInfoTab = (wcMemoAttachMaintainCompant.isShowAttTab()
                    || wcMemoTraceComponent.isShowTraceTab());
            displayController.update("viewPanelBottomInfoTabId");
        }
    };
    @Getter
    public final UploadAttCallBack uploadAttCallBack = new UploadAttCallBack() {
        /** */
        private static final long serialVersionUID = -3977121546991734903L;

        @Override
        public void doUploadAtt(AttachmentVO att) {
            tabCallBack.reloadAttTab();
            tabCallBack.reloadTabView(TabType.Attachment);
        }
    };
    /**
     * 維護相關事件
     */
    private final MemoMaintainCallBack memoMaintainCallBack = new MemoMaintainCallBack() {

        /** */
        private static final long serialVersionUID = -1976121530425130290L;

        @Override
        public void showMessage(String message) {
            messageCallBack.showMessage(message);
        }

        @Override
        public void clickEdit() {
            try {
                initData();
                if (!WCMemoCategoryLogicComponent.getInstance().isMemoEditPermission()) {
                    showMessage("無權限進行編輯！！");
                } else if (checkLock()) {
                    showMessage(
                            "此備忘錄已被 ["
                                    + memoLockComponent.getLockUserName()
                                    + "] 鎖定，將於 ["
                                    + memoLockComponent.getLockTime()
                                    + "] 解除鎖定");
                } else {
                    lock();
                    memoLogicComponent.lock(
                            memoSid, userViewVO.getSid(), memoLockComponent.getLockTime());
                    memoBtnComponent.editMode();
                    memoDataComponent.openEditField();
                }
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("執行備忘錄編輯按鈕動作 Error！！：{}", e.getMessage());
                showMessage("執行備忘錄編輯按鈕動作 Error！！");
            } catch (Exception e) {
                log.warn("執行備忘錄編輯按鈕動作 Error！！", e);
                showMessage("執行備忘錄編輯按鈕動作 Error！！");
            }
        }

        @Override
        public void clickSubmite() {
            try {
                if (!WCMemoCategoryLogicComponent.getInstance().isMemoEditPermission()) {
                    showMessage("無權限進行編輯！！");
                } else {
                    memoLogicComponent.update(
                            memoSid,
                            memoDataComponent.getTheme(),
                            memoDataComponent.getContent(),
                            memoDataComponent.getMemo(),
                            userViewVO.getSid());
                }
                initData();
                refreshId();
                displayController.execute("doReloadDataToUpdate();");
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("執行備忘錄更新動作 Error！！：{}", e.getMessage());
                showMessage(e.getMessage());
            } catch (Exception e) {
                log.warn("執行備忘錄更新動作 Error！！", e);
                showMessage(e.getMessage());
            }
        }

        @Override
        public void clickCancel() {
            try {
                memoLogicComponent.unLock(memoSid, userViewVO.getSid());
                initData();
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("執行備忘錄取消編輯按鈕動作 Error！！：{}", e.getMessage());
                showMessage("執行備忘錄取消編輯按鈕動作 Error！！ " + e.getMessage());
            } catch (Exception e) {
                log.warn("執行備忘錄取消編輯按鈕動作 Error！！", e);
                showMessage("執行備忘錄取消編輯按鈕動作 Error！！");
            }
        }

        @Override
        public void clickAtt() {
            try {
                MemoMasterTo obj = memoLogicComponent.findToBySid(memoSid);
                if (obj == null) {
                    log.error("無效資料 無法開啟聯絡單!! memoSid：" + memoSid);
                    Preconditions.checkState(false, "無效資料 無法開啟聯絡單!!");
                }
                AttachmentCondition ac = new AttachmentCondition(memoSid, obj.getMemoNo(),
                        true);
                ac.setUploadAttCallBack(uploadAttCallBack);
                memoAttachemtComponent.loadAttachment(ac);
                displayController.showPfWidgetVar("dlgUploadWv");
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("執行備忘錄上傳附件按鈕動作 Error！！：{}", e.getMessage());
                showMessage("執行備忘錄上傳附件按鈕動作 Error！！ " + e.getMessage());
            } catch (Exception e) {
                log.warn("執行備忘錄上傳附件按鈕動作 Error！！", e);
                showMessage("執行備忘錄上傳附件按鈕動作 Error！！");
            }
        }

        @Override
        public void clickTrans() {
            try {
                categoryTreeComponent.bulidTree(
                        categoryTagLogicComponent.getCategoryTags(compVieVo.getSid(), userViewVO.getSid()));
                displayController.update("transDlgPanel");
                displayController.showPfWidgetVar("transDlg");
            } catch (Exception e) {
                log.warn("執行備忘錄轉工作聯絡單按鈕動作 Error！！", e);
                showMessage("執行備忘錄轉工作聯絡單按鈕動作 Error！！");
            }
        }

        @Override
        public void clickUp() {
            try {
                previousAndNextCallBack.doPrevious();
            } catch (Exception e) {
                log.warn("clickUp ERROR", e);
                showMessage(e.getMessage());
            }
        }

        @Override
        public void clickDown() {
            try {
                previousAndNextCallBack.doNext();
            } catch (Exception e) {
                log.warn("clickDown ERROR", e);
                showMessage(e.getMessage());
            }
        }
    };

    public MemoMaintainComponent(
            UserViewVO userViewVO,
            OrgViewVo depViewVo,
            OrgViewVo compVieVo,
            MessageCallBack messageCallBack,
            PreviousAndNextCallBack previousAndNextCallBack) {
        this.memoLogicComponent = MemoLogicComponent.getInstance();
        this.displayController = DisplayController.getInstance();
        this.previousAndNextCallBack = previousAndNextCallBack;
        this.memoBtnComponent = new MemoBtnComponent(memoMaintainCallBack);
        this.memoHeaderComponent = new MemoHeaderComponent();
        this.memoDataComponent = new MemoDataComponent();
        this.memoLockComponent = new MemoLockComponent();
        this.messageCallBack = messageCallBack;
        this.userViewVO = userViewVO;
        this.depViewVo = depViewVo;
        this.compVieVo = compVieVo;
        this.wcMemoAttachMaintainCompant = new WCMemoAttachMaintainCompant(userViewVO.getSid(), messageCallBack, tabCallBack);
        this.wcMemoTraceComponent = new WCMemoTraceComponent();
        this.memoAttachemtComponent = new WCMemoAttachmentCompant(userViewVO.getSid(), depViewVo.getSid());
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.categoryTreeComponent.bulidTree(
                categoryTagLogicComponent.getCategoryTags(
                        compVieVo.getSid(), userViewVO.getSid()));
    }

    /**
     * 初始化當前資料
     */
    private void initData() {
        this.initData(memoSid, showPreviousBtn, showNextBtn, showBackListBtn);
    }

    /**
     * 初始化預設資料
     *
     * @param sid
     */
    public void initData(
            String sid, boolean showPreviousBtn, boolean showNextBtn, boolean showBackListBtn) {
        this.memoSid = sid;
        this.showPreviousBtn = showPreviousBtn;
        this.showNextBtn = showNextBtn;
        this.showBackListBtn = showBackListBtn;
        MemoMasterTo obj = memoLogicComponent.findToBySid(memoSid);
        if (obj == null) {
            log.warn("無效資料 無法開啟聯絡單!! memoSid：" + memoSid);
            Preconditions.checkState(false, "無效資料 無法開啟聯絡單!!");
        }
        memoLogicComponent.readWCMemoMaster(memoSid, userViewVO.getSid());
        obj = memoLogicComponent.findToBySid(memoSid);
        this.loadData(obj);

        this.displayController.update("screen_view");
    }

    /**
     * 讀取資料
     *
     * @param obj
     */
    private void loadData(MemoMasterTo obj) {
        memoBtnComponent.init(
                WCMemoCategoryLogicComponent.getInstance().isMemoEditPermission(),
                this.showPreviousBtn, this.showNextBtn,
                this.showBackListBtn);
        memoHeaderComponent.init(
                WkOrgUtils.prepareBreadcrumbsByDepName(
                        obj.getCreateDep(), OrgLevel.DIVISION_LEVEL, true, "-"),
                WkUserUtils.findNameBySid(obj.getCreateUser()),
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateTimestampss.getValue(), obj.getCreateDate()),
                obj.getMemoNo());
        memoDataComponent.init(obj.getTheme(), obj.getContentCss(), obj.getMemoCss());
        this.loadLock(obj);
        this.wcMemoAttachMaintainCompant.loadData(obj.getSid(), obj.getMemoNo());
        this.wcMemoTraceComponent.loadData(obj.getSid(), userViewVO, depViewVo);
        AttachmentCondition ac = new AttachmentCondition(obj.getSid(), obj.getMemoNo(), false);
        memoAttachemtComponent.loadAttachment(ac);
        tabCallBack.reloadTabView(null);
    }

    /**
     * 讀取鎖定資料
     *
     * @param obj
     */
    private void loadLock(MemoMasterTo obj) {
        String lockUserName = null;
        if (obj.getLockUser() != null) {
            lockUserName = WkUserUtils.findNameBySid(obj.getLockUser());
        }
        memoLockComponent.init(
                obj.getLockUser(),
                lockUserName,
                obj.getLockDate(),
                WkUserUtils.findNameBySid(obj.getUpdateUser()),
                new DateTime(obj.getUpdateDate()).toString(lockDateFormat));
    }

    /**
     * 刷新的ID
     */
    private void refreshId() {
        displayController.update("screen_view");
    }

    /**
     * 判斷是否鎖定編輯
     *
     * @return
     */
    private boolean checkLock() {
        return memoLockComponent.getLockUserSid() != null
                && memoLockComponent.getLockTime() != null
                && !userViewVO.getSid().equals(memoLockComponent.getLockUserSid())
                && new DateTime(memoLockComponent.getLockTime()).compareTo(new DateTime()) > 0;
    }

    /**
     * 鎖定
     */
    private void lock() {
        memoLockComponent.lock(userViewVO.getSid(), userViewVO.getName(), getLockTime());
        displayController.update("screen_view_lock");
        displayController.execute(
                "countDown('screen_view_lock_view_lockCount','"
                        + new DateTime(memoLockComponent.getLockTime()).toString(lockDateFormat)
                        + "','closeLock();');");
    }

    /**
     * 取得鎖定時間
     *
     * @return
     */
    private Date getLockTime() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, effectiveMinute);
        return c.getTime();
    }

    private void loadTabIndex(TabType tabType) {
        if (tabType == null) {
            return;
        }
        int attIndex = (wcMemoAttachMaintainCompant.isShowAttTab()) ? 0 : -1;
        int traceIndex = attIndex + (wcMemoTraceComponent.isShowTraceTab() ? 1 : 0);
        switch (tabType) {
        case Trace:
            tabIndex = traceIndex;
            break;
        case Attachment:
            tabIndex = attIndex;
            break;
        default:
            tabIndex = 0;
            break;
        }
        if (tabIndex < 0) {
            tabIndex = 0;
        }
    }

    public void clickToTransWorkConnect() {
        try {
            String categorySid = categoryTreeComponent.getSelectedNodeSid();
            if (Strings.isNullOrEmpty(categorySid)) {
                messageCallBack.showMessage("尚未選取資料!!");
                return;
            }
            if (memoLogicComponent.isTraned(memoSid)) {
                DisplayController.getInstance().showPfWidgetVar("confimDlgTrans");
            } else {
                doClickToTransWorkConnect();
            }
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("clickToTransWorkConnect ERROR：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("clickToTransWorkConnect ERROR", e);
        }
    }

    public void doClickToTransWorkConnect() {
        try {
            String categorySid = categoryTreeComponent.getSelectedNodeSid();
            if (Strings.isNullOrEmpty(categorySid)) {
                messageCallBack.showMessage("尚未選取資料!!");
                return;
            }
            DisplayController.getInstance()
                    .execute(
                            "window.open('"
                                    + "../worp/worp1.xhtml?categorySid="
                                    + categorySid
                                    + "&memoSid="
                                    + memoSid
                                    + "', '_blank');");
            DisplayController.getInstance().hidePfWidgetVar("confimDlgTrans");
            DisplayController.getInstance().hidePfWidgetVar("transDlg");
        } catch (Exception e) {
            log.warn("clickToTransWorkConnect ERROR", e);
        }
    }
}
