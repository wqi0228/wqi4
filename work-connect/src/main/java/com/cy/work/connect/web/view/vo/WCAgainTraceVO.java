/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.vo.enums.WCTraceType;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * 自訂物件 - 回覆的回覆
 *
 * @author kasim
 */
public class WCAgainTraceVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2923153015299532794L;

    @Getter
    /** 回覆的回覆 Sid */
    private final String sid;

    @Getter
    /*追蹤主單 Sid*/
    private final String traceSid;

    @Getter
    /** 類型 */
    private final WCTraceType type;

    @Getter
    /** 類型名稱 */
    private final String traceType;

    @Getter
    /** 使用者資訊 */
    private final String userInfo;

    @Getter
    /** 回覆時間 */
    private final String time;

    @Getter
    private final boolean showEditBtn;
    @Getter
    @Setter
    private String content;

    public WCAgainTraceVO(
        String sid,
        String traceSid,
        WCTraceType type,
        String userInfo,
        String time,
        String content,
        boolean showEditBtn) {
        this.sid = sid;
        this.traceSid = traceSid;
        this.type = type;
        if (type != null) {
            this.traceType = type.getVal();
        } else {
            this.traceType = "";
        }
        this.userInfo = userInfo;
        this.time = time;
        this.content = content;
        this.showEditBtn = showEditBtn;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WCAgainTraceVO other = (WCAgainTraceVO) obj;
        return Objects.equals(this.sid,
            other.sid);
    }
}
