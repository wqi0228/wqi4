package com.cy.work.connect.web.view;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.web.listener.PreviousAndNextCallBack;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.components.WorkReportViewComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */

@Slf4j
@Scope("view")
@ManagedBean
@Controller
public class Worp2Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3938991052357197615L;

    @Getter
    @Setter
    private String wrcId;

    /**
     * view 顯示錯誤訊息
     */
    @Getter
    private String errorMessage;

    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private WorkReportViewComponent workReportViewComponent;
    private boolean showBackHomeBtn = false;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Autowired
    private SessionTimerBean sessionTimerBean;
    private String wrcType = "";
    private String detailSID = "";
    private String url = "";
    private final PreviousAndNextCallBack previousAndNextCallBack = new PreviousAndNextCallBack() {
        /** */
        private static final long serialVersionUID = -7197404839814715516L;

        @Override
        public void doPrevious() {
            // log.info("開始進行確認上一筆");
            initializeDataTable();
        }

        @Override
        public void doNext() {
            // log.info("開始進行確認下一筆");
            initializeDataTable();
        }
    };
    @Getter
    private boolean showTitleBtn = true;
    @Getter
    private boolean isFromFormsign = false;

    @PostConstruct
    public void init() {
        try {
            this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
            this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
            this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
            this.getRequestParameterMap();
            workReportViewComponent = new WorkReportViewComponent(
                    userViewVO, depViewVo, compViewVo, previousAndNextCallBack);

            workReportViewComponent.loadMaster(wrcId, wrcType, detailSID);

            if (Faces.getRequestParameterMap().containsKey("mode")) {
                String formSigningMode = Faces.getRequestParameterMap().get("mode");
                showTitleBtn = formSigningMode.toLowerCase().contains("edit");
            }

            if (Faces.getRequestParameterMap().containsKey("isFormSignApply")) {
                String isFormSignApply = Faces.getRequestParameterMap().get("isFormSignApply");
                if (!showTitleBtn) {
                    showTitleBtn = isFormSignApply.toLowerCase().contains("true");
                }
            }

            if (Faces.getRequestParameterMap().containsKey("isFromFormsign")) {
                String isFromFormsignStr = Faces.getRequestParameterMap().get("isFromFormsign");
                isFromFormsign = isFromFormsignStr.toLowerCase().contains("true");
            }
        } catch (Exception e) {
            showMessage(e.getMessage());
        }
    }

    /**
     * 取得參數
     */
    private void getRequestParameterMap() {
        Preconditions.checkState(Faces.getRequestParameterMap().containsKey("wrcId"),
                "無效參數 無法開啟聯絡單!!");
        wrcId = Faces.getRequestParameterMap().get("wrcId");
        if (Faces.getRequestParameterMap().containsKey("wrcType")) {
            wrcType = Faces.getRequestParameterMap().get("wrcType");
        }
        if (Faces.getRequestParameterMap().containsKey("detailSID")) {
            detailSID = Faces.getRequestParameterMap().get("detailSID");
        }
    }

    public void initialize() {
        sessionTimerBean.reStartIdle();
        // Request Scope 僅view Load時執行
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        if (!Faces.getRequestParameterMap().containsKey("wrcId")) {
            return;
        }

        wrcId = Faces.getRequestParameterMap().get("wrcId");
        boolean showPreviousBtn = !(Strings.isNullOrEmpty(
                tableUpDownBean.getSession_previous_sid()));
        tableUpDownBean.setSession_previous_sid("");
        boolean showNextBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_next_sid()));
        tableUpDownBean.setSession_next_sid("");
        showBackHomeBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_show_home()));
        tableUpDownBean.setSession_show_home("");
        url = tableUpDownBean.getWorp_path();
        tableUpDownBean.setWorp_path("");
        workReportViewComponent.initialize(wrcId, showPreviousBtn, showNextBtn, showBackHomeBtn);
    }

    public void initializeDataTable() {
        long startTime = System.currentTimeMillis();
        while (!tableUpDownBean.isSessionSettingOver()) {
            log.debug("執行上下筆,Session等待執行值,最多等待兩秒");
            try {
                TimeUnit.MICROSECONDS.sleep(50);
                if (System.currentTimeMillis() - startTime > 2000) {
                    tableUpDownBean.clearSessionSettingOver();
                    log.warn("執行上下筆,Session並未等到執行值,最多等待兩秒");
                    break;
                }
            } catch (Exception e) {
                log.warn("Sleep Error", e);
            }
        }
        tableUpDownBean.clearSessionSettingOver();
        boolean showPreviousBtn = !(Strings.isNullOrEmpty(
                tableUpDownBean.getSession_previous_sid()));
        tableUpDownBean.setSession_previous_sid("");
        boolean showNextBtn = !(Strings.isNullOrEmpty(tableUpDownBean.getSession_next_sid()));
        tableUpDownBean.setSession_next_sid("");
        String wrcId = "";
        if (!Strings.isNullOrEmpty(tableUpDownBean.getSession_now_sid())) {
            wrcId = tableUpDownBean.getSession_now_sid();
            tableUpDownBean.setSession_now_sid("");
        }
        if (!Strings.isNullOrEmpty(wrcId)) {
            DisplayController.getInstance()
                    .execute("replaceUrlNotNavigation('" + url + ".xhtml?wrcId=" + wrcId + "')");
        }
        workReportViewComponent.initialize(wrcId, showPreviousBtn, showNextBtn, showBackHomeBtn);
    }

    /**
     * 顯示錯誤訊息及彈出Dialog
     *
     * @param message
     */
    private void showMessage(String message) {
        this.errorMessage = message;
        DisplayController.getInstance().update("confimDlgTemplate");
        DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
    }

}
