/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.RelationDepHelper;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCMemoCategoryManager;
import com.cy.work.connect.vo.WCMemoCategory;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.web.view.enumtype.PermissionType;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 備忘錄類別邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCMemoCategoryLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8742646672129340246L;

    private static WCMemoCategoryLogicComponent instance;
    @Autowired
    private WCMemoCategoryManager wcMemoCategoryManager;

    public static WCMemoCategoryLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMemoCategoryLogicComponent.instance = this;
    }

    /**
     * @param permissionType
     * @return
     */
    private List<WCMemoCategory> findWCMemoCategorys(PermissionType permissionType) {

        // ====================================
        // 取得 wc_memo_category 比對 use_dep, edit_dep 時，登入者對應部門
        // ====================================
        // 設定部門以下人員都可使用
        // 主要部門+直系向上（含兼職）
        Set<Integer> matchOrgSids = RelationDepHelper.findMemoMatchOrgSidsForCategoryDepSetting(SecurityFacade.getUserSid());

        // ====================================
        // 查詢所有設定
        // ====================================
        List<WCMemoCategory> memoCategorys = this.wcMemoCategoryManager.getAllWCMemoCategory();

        // ====================================
        // 過濾有權限使用的類別
        // ====================================
        return memoCategorys.stream()
                .filter(memoCategory -> this.isMemoPermission(memoCategory, matchOrgSids, permissionType))
                .collect(Collectors.toList());

    }

    /**
     * 取得該登入者部門備忘錄類別
     *
     * @param loginDepSid 登入部門Sid
     * @return
     * @throws UserMessageException
     */
    public WCMemoCategory getWCMemoCategory() throws UserMessageException {

        // ====================================
        // 查詢所有設定
        // ====================================
        List<WCMemoCategory> memoCategorys = this.findWCMemoCategorys(PermissionType.VIEW);

        // ====================================
        // 判定
        // ====================================
        if (WkStringUtils.isEmpty(memoCategorys)) {
            String message = "無權限進行備忘錄新增";
            log.warn(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        if (memoCategorys.size() > 1) {
            String message = WkMessage.EXECTION + "(備忘錄設定有誤，單位設定重複）";
            log.warn(message + "loginUserSid:[" + SecurityFacade.getUserSid() + "]");
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message + "loginUserSid:[" + SecurityFacade.getUserSid() + "]");
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }

        return memoCategorys.get(0);
    }

    public String getMemoCategorySidByLoginUser() throws UserMessageException {
        // 查詢
        return this.getWCMemoCategory().getSid();
    }

    /**
     * 根據備忘錄類別判斷登入部門是否有檢視權限
     *
     * @param loginDepSid 登入部門Sid
     * @return
     */
    public boolean isMemoViewPermission() { return this.findWCMemoCategorys(PermissionType.VIEW).size() > 0; }

    /**
     * 根據備忘錄類別判斷登入部門是否有檢視權限
     *
     * @param loginDepSid          登入部門Sid
     * @param wc_memo_category_sid 備忘錄類別Sid
     * @return
     */
    public boolean isMemoViewPermission(String wc_memo_category_sid) {
        // 設定部門以下人員都可使用
        // 主要部門+直系向上（含兼職）
        Set<Integer> matchOrgSids = RelationDepHelper.findMemoMatchOrgSidsForCategoryDepSetting(SecurityFacade.getUserSid());
        // 比對是否為同類別單位權限
        return isMemoPermission(
                wcMemoCategoryManager.getWCMemoCategory(wc_memo_category_sid),
                matchOrgSids,
                PermissionType.VIEW);
    }

    /**
     * 根據備忘錄類別判斷登入部門是否有編輯權限
     *
     * @param loginDepSid 登入部門Sid
     * @return
     */
    public boolean isMemoEditPermission() {
        return this.findWCMemoCategorys(PermissionType.EDIT).size() > 0;
    }

    /**
     * 根據備忘錄類別判斷登入部門是否有權限
     *
     * @param memoCategory 備忘錄類別物件
     * @param matchOrgSids 登入部門向上延展部門List
     * @return
     */
    private boolean isMemoPermission(
            WCMemoCategory memoCategory, Set<Integer> matchOrgSids, PermissionType permissionType) {

        // ====================================
        // 根據類別，取得設定單位
        // ====================================
        Set<Integer> settingDepSids = Sets.newHashSet();
        if (PermissionType.VIEW.equals(permissionType)) {
            settingDepSids = UserDep.parserDepSids(memoCategory.getUseDep());
        } else if (PermissionType.EDIT.equals(permissionType)) {
            settingDepSids = UserDep.parserDepSids(memoCategory.getEditDep());
        }

        // ====================================
        // 比對
        // ====================================
        for (Integer matchOrgSid : matchOrgSids) {
            if (settingDepSids.contains(matchOrgSid)) {
                return true;
            }
        }

        return false;
    }
}
