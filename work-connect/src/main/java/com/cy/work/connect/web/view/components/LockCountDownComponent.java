/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.web.logic.components.WCMasterLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;

/**
 * @author brain0925_liao
 */
@Slf4j
public class LockCountDownComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7920058591787501865L;
    /**
     * 有效分鐘
     */
    private final int effectiveMinute = 10;
    @Getter
    private String editUserName = "";
    @Getter
    private boolean showLock = false;
    private Date startEditDate;
    @Getter
    private String countDownTime = "";

    public LockCountDownComponent() {
    }

    public void loadData(
        String wc_ID, Integer loginUserSid, String editUserName, Date startEditDate) {
        this.editUserName = editUserName;
        this.startEditDate = startEditDate;
        this.showLock = true;
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MINUTE, effectiveMinute);
            Date lockTime = c.getTime();
            WCMasterLogicComponents.getInstance()
                .updateLockUserAndLockDate(wc_ID, loginUserSid, lockTime);
        } catch (Exception e) {
            log.warn("loadData", e);
        }
    }

    public void cancelLock(String wc_ID, Integer closeUserSid) {
        try {

            WCMasterLogicComponents.getInstance().closeLockUserAndLockDate(wc_ID, closeUserSid);
        } catch (Exception e) {
            log.warn("cancelLock", e);
        }
        closeLock();
    }

    public int remainingTime(Date lock_dt) {
        long nowTime = new Date().getTime();
        long lockTme = lock_dt.getTime();
        long one_sec_time = 1000;
        long diffsec = (lockTme - nowTime) / one_sec_time;
        return (int) diffsec;
    }

    public void closeLock() {
        this.showLock = false;
        this.countDownTime = "";
        this.editUserName = "";
        DisplayController.getInstance().update("userModifyPanel");
    }

    public void findEffectiveTime() {
        try {
            this.countDownTime =
                new DateTime(startEditDate).plusMinutes(effectiveMinute)
                    .toString("yyyy-MM-dd HH:mm:ss");
        } catch (Exception e) {
            log.warn("findEffectiveTime", e);
        }
    }
}
