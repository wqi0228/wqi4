/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.column.search;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Data;

/**
 * 單據清單 - dataTable 欄位資訊
 */
@Data
public class OrderListColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5610150045288267420L;
    /**
     * 一頁 X 筆
     */
    private String pageCount = "50";
    /**
     * 序
     */
    private ColumnDetailVO index;
    /**
     * 建立日期
     */
    private ColumnDetailVO createDate;
    /**
     * 類別(第一層)
     */
    private ColumnDetailVO categoryName;
    /**
     * 單據名稱(第二層)
     */
    private ColumnDetailVO menuTagName;
    /**
     * 申請人
     */
    private ColumnDetailVO applicant;
    /**
     * 主題
     */
    private ColumnDetailVO theme;
    /**
     * 單據狀態
     */
    private ColumnDetailVO statusName;
    /**
     * 單號
     */
    private ColumnDetailVO wcNo;

    public OrderListColumnVO() {
        index = new ColumnDetailVO("序", true, "30");
        createDate = new ColumnDetailVO("建立日期", true, "80");
        categoryName = new ColumnDetailVO("類別", true, "120");
        menuTagName = new ColumnDetailVO("單據名稱", true, "120");
        applicant = new ColumnDetailVO("申請人", true, "80");
        theme = new ColumnDetailVO("主題", true, "");
        statusName = new ColumnDetailVO("單據狀態", true, "70");
        wcNo = new ColumnDetailVO("單號", true, "130");
    }
}
