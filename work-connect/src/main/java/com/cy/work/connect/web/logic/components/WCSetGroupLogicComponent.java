package com.cy.work.connect.web.logic.components;

import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.logic.vo.WCGroupVo;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.view.vo.TagVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * 設定群組邏輯元件
 *
 * @author jimmy_chou
 */
@Component
public class WCSetGroupLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4964864275203445185L;

    private static WCSetGroupLogicComponent instance;

    public static WCSetGroupLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCSetGroupLogicComponent.instance = this;
    }

    /**
     * 執行存檔
     *
     * @param selectedNode  選取的設定權限項目
     * @param targetTypeMap 選取類型對應型態
     * @param selectedRow   選取的設定群組
     * @param loginCompSid  登入公司Sid
     * @param loginUserSid  登入者Sid
     * @throws java.lang.Exception
     */
    public void doUpdate(
        List<TreeNode> selectedNode,
        Map<String, TargetType> targetTypeMap,
        WCGroupVo selectedRow,
        Integer loginCompSid,
        Integer loginUserSid)
        throws Exception {
        // ====================================
        // 新舊比對
        // ====================================
        Map<TargetType, Set<String>> addPermissionMap = Maps.newHashMap();
        Map<TargetType, Set<String>> delPermissionMap = Maps.newHashMap();
        Map<TargetType, Set<String>> constPermissionMap = Maps.newHashMap();
        // 舊
        List<WCSetPermission> permissions =
            WCSetPermissionManager.getInstance()
                .findPermissionByCompSidAndGroupSid(loginCompSid, selectedRow.getSid());
        Map<TargetType, Set<String>> oldPermissionMap =
            permissions.stream()
                .collect(
                    Collectors.groupingBy(
                        WCSetPermission::getTargetType,
                        Collectors.mapping(WCSetPermission::getTargetSid, Collectors.toSet())));
        // 新
        Map<TargetType, Set<String>> newPermissionMap =
            selectedNode.stream()
                .collect(
                    Collectors.groupingBy(
                        each -> targetTypeMap.get(each.getType()),
                        Collectors.mapping(
                            each -> {
                                Object data = each.getData();
                                String sid;
                                switch (each.getType()) {
                                    case "CategoryTag":
                                        // 大類
                                    case "MenuTag":
                                        // 中類
                                    case "Tag":
                                        // 小類
                                    case "ExecDep":
                                        // 執行單位
                                        sid = ((TagVO) data).getSid();
                                        break;
                                    case "templet":
                                        // 模版
                                        sid = ((WCConfigTempletVo) data).getSid().toString();
                                        break;
                                    default:
                                        sid = "";
                                        break;
                                }
                                return sid;
                            },
                            Collectors.toSet())));

        for (TargetType target : newPermissionMap.keySet()) {
            if (oldPermissionMap.get(target) == null) {
                addPermissionMap.put(target, newPermissionMap.get(target));
            } else {
                Set<String> oldSet = oldPermissionMap.get(target);
                Set<String> newSet = newPermissionMap.get(target);

                Set<String> tempDelSet = Sets.newHashSet();
                tempDelSet.addAll(oldSet);
                tempDelSet.removeAll(newSet);
                if (!tempDelSet.isEmpty()) {
                    delPermissionMap.put(target, tempDelSet);
                }

                Set<String> tempAddSet = Sets.newHashSet();
                tempAddSet.addAll(newSet);
                tempAddSet.removeAll(oldSet);
                if (!tempAddSet.isEmpty()) {
                    addPermissionMap.put(target, tempAddSet);
                }

                Set<String> tempConstSet = Sets.newHashSet();
                tempConstSet.addAll(oldSet);
                tempConstSet.retainAll(newSet);
                if (!tempConstSet.isEmpty()) {
                    constPermissionMap.put(target, tempConstSet);
                }
            }
        }

        // 檢查是否有遺漏類型
        Set<TargetType> tempSet = Sets.newHashSet();
        tempSet.addAll(oldPermissionMap.keySet());
        if (!delPermissionMap.isEmpty()) {
            tempSet.removeAll(delPermissionMap.keySet());
        }
        if (!addPermissionMap.isEmpty()) {
            tempSet.removeAll(addPermissionMap.keySet());
        }
        if (!constPermissionMap.isEmpty()) {
            tempSet.removeAll(constPermissionMap.keySet());
        }
        if (!tempSet.isEmpty()) {
            for (TargetType target : tempSet) {
                delPermissionMap.put(target, oldPermissionMap.get(target));
            }
        }

        WCSetPermission editWCPermission = null;
        List<Long> nowPermissionList = Lists.newArrayList();
        // 新增
        if (!addPermissionMap.isEmpty()) {
            for (TargetType target : addPermissionMap.keySet()) {
                for (String sid : addPermissionMap.get(target)) {
                    // 確認目標項目是否存在
                    editWCPermission =
                        WCSetPermissionManager.getInstance()
                            .findPermissionByCompSidAndTargetSidAndTargetType(loginCompSid, sid,
                                target);
                    if (editWCPermission != null) {
                        nowPermissionList = editWCPermission.getPermissionGroups();
                        if (!nowPermissionList.contains(selectedRow.getSid())) {
                            nowPermissionList.add(selectedRow.getSid());
                        }
                    } else {
                        editWCPermission = new WCSetPermission();
                        editWCPermission.setTargetType(target);
                        editWCPermission.setTargetSid(sid);
                        nowPermissionList.add(selectedRow.getSid());
                    }
                    editWCPermission.setPermissionGroups(nowPermissionList);
                    WCSetPermissionManager.getInstance()
                        .saveSetting(editWCPermission, loginUserSid, loginCompSid);
                }
            }
        }
        // 刪除
        if (!delPermissionMap.isEmpty()) {
            for (TargetType target : delPermissionMap.keySet()) {
                for (String sid : delPermissionMap.get(target)) {
                    // 確認目標項目是否存在
                    editWCPermission =
                        WCSetPermissionManager.getInstance()
                            .findPermissionByCompSidAndTargetSidAndTargetType(loginCompSid, sid,
                                target);
                    if (editWCPermission != null) {
                        nowPermissionList = editWCPermission.getPermissionGroups();
                        nowPermissionList.remove(selectedRow.getSid());
                        editWCPermission.setPermissionGroups(nowPermissionList);
                        WCSetPermissionManager.getInstance()
                            .saveSetting(editWCPermission, loginUserSid, loginCompSid);
                    }
                }
            }
        }
    }
}
