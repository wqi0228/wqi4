package com.cy.work.connect.web.view;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.ReorderEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 設定模版維護作業 MBean
 *
 * @author allen
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Setting7Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7241903548977679539L;
    /**
     * 使用別選項 list
     */
    @Getter
    @Setter
    public List<ConfigTempletType> templetTypes = Lists.newArrayList(ConfigTempletType.values());
    /**
     * 查詢區域組件
     */
    @Getter
    @Setter
    public Setting7SearchComponent searchComponent = new Setting7SearchComponent();
    /**
     * 查詢編輯區域組件
     */
    @Getter
    @Setter
    public Setting7EditComponent editComponent = new Setting7EditComponent();
    /**
     *
     */
    @Getter
    @Setter
    public List<WCConfigTempletVo> templetList;
    /**
     *
     */
    @Getter
    @Setter
    public WCConfigTempletVo selectedRow;
    /**
     *
     */
    @Autowired
    private WCConfigTempletManager configTempletManager;
    /**
     *
     */
    @Autowired
    private DisplayController displayController;

    @PostConstruct
    public void init() {
        this.doSearch();
    }

    /**
     * 查詢
     */
    public void doSearch() {

        // ====================================
        // 查詢
        // ====================================
        this.templetList =
            this.configTempletManager.queryByCondition(
                SecurityFacade.getUserSid(),
                this.getSearchComponent().getSearchVo().getTempletType(),
                this.getSearchComponent().getSearchVo().getSelectedOrgSids(),
                Optional.ofNullable(
                        WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId()))
                    .map(Org::getSid)
                    .orElseGet(() -> null),
                true);
    }

    /**
     * 開啟模版新增視窗
     */
    public void openAddDialog() {
        // 資料初始化
        this.editComponent.init();
        // 設定為新增模式
        this.editComponent.setEditMode("A");
        // 開啟編輯視窗
        displayController.showPfWidgetVar("editDialog");
    }

    /**
     * 開啟模版編輯視窗
     */
    public void openEditDialog() {

        this.editComponent.setTempletVo(new WCConfigTempletVo());

        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (this.selectedRow == null) {
            MessagesUtils.showError("找不到所選擇的資料!");
            log.error("openEditDialog：selectedRow 為空！");
            return;
        }

        WCConfigTempletVo templetVo = this.configTempletManager.findBySid(
            this.selectedRow.getSid());

        if (templetVo == null) {
            MessagesUtils.showError("該筆資料已經被刪除, 請重新整理頁面!");
            log.warn("該筆資料已經被刪除, 請重新整理頁面!");
            return;
        }

        // ====================================
        // 初始化
        // ====================================
        // 資料初始化
        this.editComponent.init();
        // 設定為編輯模式
        this.editComponent.setEditMode("E");
        // 放入編輯資料
        this.editComponent.setTempletVo(templetVo);
        // 開啟編輯視窗
        displayController.showPfWidgetVar("editDialog");
    }

    /**
     *
     */
    public void saveTemplet() {

        if (WkStringUtils.isEmpty(this.editComponent.getTempletVo().getTempletType())) {
            MessagesUtils.showInfo("請輸入模版類別");
            return;
        }

        if (WkStringUtils.isEmpty(this.editComponent.getTempletVo().getTempletName())) {
            MessagesUtils.showInfo("請輸入名稱");
            return;
        }

        try {
            // ====================================
            // 儲存
            // ====================================
            this.configTempletManager.saveSetting(
                this.editComponent.getTempletVo(),
                SecurityFacade.getUserSid(),
                Optional.ofNullable(
                        WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId()))
                    .map(Org::getSid)
                    .orElseGet(() -> null));

            // ====================================
            // 畫面操作
            // ====================================
            MessagesUtils.showInfo("儲存成功!");
            displayController.hidePfWidgetVar("editDialog");

        } catch (Exception e) {
            String msg = "儲存時發生錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 刪除模版
     *
     * @param sid
     */
    public void delete(Long sid) {

        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (sid == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            log.error("openEditDialog：selectedRow 為空！");
            return;
        }

        WCConfigTempletVo wcConfigTempletVo = this.configTempletManager.findBySid(sid);

        if (wcConfigTempletVo == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            return;
        }

        // ====================================
        // 檢核是否有使用
        // ====================================
        // 查詢
        String inUsedMessage =
            this.configTempletManager.checkInUsed(
                wcConfigTempletVo.getTempletType(), wcConfigTempletVo.getSid());

        if (WkStringUtils.notEmpty(inUsedMessage)) {
            MessagesUtils.showWarn(
                "" + "此模版已經被使用, " + "目前無法刪除! " + "被使用項目如下:<br/><br/><br/>" + inUsedMessage);
            return;
        }

        // ====================================
        // 刪除
        // ====================================
        try {
            this.configTempletManager.deleteTemplet(wcConfigTempletVo.getSid());
            // ====================================
            // 畫面操作
            // ====================================
            MessagesUtils.showInfo("刪除成功!");

        } catch (Exception e) {
            String msg = "刪除時發生錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 異動排序
     *
     * @param event
     */
    public void sort(ReorderEvent event) {

        // ====================================
        // 更新排序序號
        // ====================================
        log.info("更新顯示排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());

        try {
            // 更新排序序號
            this.configTempletManager.updateSortSeqByListIndex(templetList);

        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 初始化部門樹 - 查詢區塊
     */
    public void initSerachDepTree() {
        this.searchComponent.setDepTree(
            this.initDepTree(this.searchComponent.getSearchVo().getSelectedOrgSids()));
    }

    /**
     * 初始化部門樹 - 編輯區塊
     */
    public void initEditDepTree() {
        this.editComponent.setDepTree(
            this.initDepTree(this.editComponent.getTempletVo().getConfigValue().getDeps()));
    }

    /**
     * 初始化部門樹
     */
    private DepTreeComponent initDepTree(List<Integer> selectedOrgSids) {

        DepTreeComponent depTree = new DepTreeComponent();

        // ====================================
        // 準備已選擇部門
        // ====================================
        List<Org> selectedOrgs = Lists.newArrayList();
        boolean defaultShowDisableDep = false;
        if (WkStringUtils.notEmpty(selectedOrgSids)) {
            for (Integer orgSid : selectedOrgSids) {
                // 依據SID 取得Org物件
                if (orgSid == null) {
                    continue;
                }
                Org org = WkOrgCache.getInstance().findBySid(orgSid);
                if (org == null) {
                    continue;
                }
                selectedOrgs.add(org);
                // 判斷是否該顯示停用部門
                if (org.getStatus().equals(Activation.INACTIVE)) {
                    defaultShowDisableDep = true;
                }
            }
        }

        // ====================================
        // 初始化樹
        // ====================================
        List<Org> allOrgs = WkOrgCache.getInstance().findAll();

        depTree.init(
            allOrgs,
            allOrgs,
            WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId()),
            selectedOrgs,
            true,
            true,
            false,
            defaultShowDisableDep);

        return depTree;
    }

    /**
     * 查詢區域組件
     *
     * @author allen
     */
    public class Setting7SearchComponent implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 1131511384697431852L;

        /**
         * 部門樹組件
         */
        @Getter
        @Setter
        private DepTreeComponent depTree;

        /**
         * 查詢條件
         */
        @Getter
        @Setter
        private Setting7SearchVo searchVo;

        /**
         * 建構子
         */
        public Setting7SearchComponent() {
            this.depTree = new DepTreeComponent();
            this.searchVo = new Setting7SearchVo();
        }

        /**
         * 部門樹設定視窗點選確定
         */
        public void depTreeConfirm() {
            List<Integer> selectedOrgSids = Lists.newArrayList();

            // 取得畫面所選的單位
            List<Org> deps = depTree.getSelectOrg();
            // 整理為 SID List
            if (WkStringUtils.notEmpty(deps)) {
                selectedOrgSids = deps.stream().map(Org::getSid).collect(Collectors.toList());
            }

            this.searchVo.selectedOrgSids = selectedOrgSids;

            DisplayController.getInstance().hidePfWidgetVar("setting7SearchDepDlg");
        }

        /**
         * Setting7 查詢欄位容器
         *
         * @author allen
         */
        public class Setting7SearchVo implements Serializable {

            /**
             *
             */
            private static final long serialVersionUID = -8577762665051384745L;

            /**
             * 使用別
             */
            @Getter
            @Setter
            private ConfigTempletType templetType;

            /**
             * 含有部門設定
             */
            @Getter
            @Setter
            private List<Integer> selectedOrgSids;

            public Setting7SearchVo() {
                this.selectedOrgSids = Lists.newArrayList();
            }
        }
    }

    /**
     * 編輯區域組件
     *
     * @author allen
     */
    public class Setting7EditComponent implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -4430576192371472009L;

        /**
         * 編輯類型 (新增/編輯)
         */
        @Getter
        @Setter
        private String editMode;

        /**
         * 部門樹組件
         */
        @Getter
        @Setter
        private DepTreeComponent depTree = new DepTreeComponent();

        /**
         * 模版資料
         */
        @Getter
        @Setter
        private WCConfigTempletVo templetVo = new WCConfigTempletVo();

        /**
         * 資料初始化
         */
        public void init() {
            this.depTree = new DepTreeComponent();
            this.templetVo = new WCConfigTempletVo();
        }

        /**
         * 部門樹設定視窗點選確定
         */
        public void depTreeConfirm() {

            List<Integer> selectedOrgSids = Lists.newArrayList();

            // 取得畫面所選的單位
            List<Org> deps = depTree.getSelectOrg();
            // 整理為 SID List
            if (WkStringUtils.notEmpty(deps)) {
                selectedOrgSids = deps.stream().map(Org::getSid).collect(Collectors.toList());
            }

            // 放到編輯資料物件欄位中
            this.templetVo.getConfigValue().setDeps(selectedOrgSids);

            DisplayController.getInstance().hidePfWidgetVar("setting7EditDepDlg");
        }
    }
}
