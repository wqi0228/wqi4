/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search.query;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;

/**
 * 處理清單 查詢物件
 *
 * @author kasim
 */
@Data
@NoArgsConstructor
public class CheckedListQuery implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2966960329011835207L;
    /**
     * 類別
     */
    private String categorySid = "";
    /**
     * 主題
     */
    private String theme = "";
    /**
     * 內容
     */
    private String content = "";
    /**
     * 模糊搜尋
     */
    private String lazyContent = "";

    private String selStatus = "";

    private List<String> selProcessStatus;
    /**
     * 建立區間(起)
     */
    private Date startDate;
    /**
     * 建立區間(訖)
     */
    private Date endDate;
    /**
     * 是否閱讀
     */
    private Boolean hasReadRecord;

    public void init() {
        this.categorySid = "";
        this.theme = "";
        this.content = "";
        this.startDate = null;
        this.endDate = null;
        this.selStatus = "";
        this.lazyContent = "";
        this.selProcessStatus = Lists.newArrayList();
        this.hasReadRecord = null;
    }

    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 前一日
     */
    public void changeDateIntervalPreDay() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusDays(1);
        this.startDate = lastDate.toDate();
        this.endDate = lastDate.toDate();
    }
}
