/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.work.connect.logic.cache.WCHomepageFavoriteCache;
import com.cy.work.connect.logic.manager.WCHomepageFavoriteManager;
import com.cy.work.connect.vo.WCHomepageFavorite;
import com.cy.work.connect.vo.converter.to.ReportPage;
import com.cy.work.connect.vo.converter.to.ReportPageTo;
import com.cy.work.connect.web.view.vo.WCHomepageFavoriteVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 報表快選區邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCHomepageFavoriteLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5411317029519096097L;

    private static WCHomepageFavoriteLogicComponents instance;
    @Autowired
    private WCHomepageFavoriteCache wcHomepageFavoriteCache;
    @Autowired
    private WCHomepageFavoriteManager wcHomepageFavoriteManager;

    public static WCHomepageFavoriteLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCHomepageFavoriteLogicComponents.instance = this;
    }

    /**
     * 儲存報表快選區設定
     *
     * @param wcHomepageFavoriteVOs 挑選紀錄報表快選區List
     * @param userSid               登入者Sid
     */
    public void saveWCHomepageFavorite(
        List<WCHomepageFavoriteVO> wcHomepageFavoriteVOs, Integer userSid) {
        WCHomepageFavorite wcHomepageFavorite = wcHomepageFavoriteCache.findByUserSid(userSid);
        if (wcHomepageFavorite == null) {
            wcHomepageFavorite = new WCHomepageFavorite();
        }
        ReportPage reportPage = new ReportPage();
        List<ReportPageTo> reportPageTos = Lists.newArrayList();
        wcHomepageFavoriteVOs.forEach(
            item -> {
                ReportPageTo rpt = new ReportPageTo();
                rpt.setPageName(item.getPageName());
                rpt.setPagePath(item.getPagePath());
                reportPageTos.add(rpt);
            });
        reportPage.setReportPageTos(reportPageTos);
        wcHomepageFavorite.setReport_page(reportPage);
        if (Strings.isNullOrEmpty(wcHomepageFavorite.getSid())) {
            wcHomepageFavorite = wcHomepageFavoriteManager.create(wcHomepageFavorite, userSid);
        } else {
            wcHomepageFavorite = wcHomepageFavoriteManager.update(wcHomepageFavorite, userSid);
        }
        wcHomepageFavoriteCache.updateCacheByUserSid(wcHomepageFavorite);
    }

    /**
     * 取得該登入者挑選的報表快選區
     *
     * @param userSid 登入者Sid
     * @return
     */
    public List<WCHomepageFavoriteVO> getWCHomepageFavoriteVOByUserSid(Integer userSid) {
        List<WCHomepageFavoriteVO> wcHomepageFavoriteVOs = Lists.newArrayList();
        try {
            WCHomepageFavorite wcHomepageFavorite = wcHomepageFavoriteCache.findByUserSid(userSid);
            if (wcHomepageFavorite != null
                && wcHomepageFavorite.getReport_page() != null
                && wcHomepageFavorite.getReport_page().getReportPageTos() != null) {
                wcHomepageFavorite
                    .getReport_page()
                    .getReportPageTos()
                    .forEach(
                        item -> {
                            WCHomepageFavoriteVO wcHomepageFavoriteVO =
                                new WCHomepageFavoriteVO(item.getPagePath(), item.getPageName());
                            wcHomepageFavoriteVOs.add(wcHomepageFavoriteVO);
                        });
            }
        } catch (Exception e) {
            log.warn("getWCHomepageFavoriteVOByUserSid", e);
        }
        return wcHomepageFavoriteVOs;
    }
}
