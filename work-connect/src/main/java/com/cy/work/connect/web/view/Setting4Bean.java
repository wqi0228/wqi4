/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.logic.SecurityCacheHelper;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.client.WorkConnectRestClient;
import com.cy.work.connect.logic.cache.WCExecManagerSignInfoCache;
import com.cy.work.connect.logic.cache.WCManagerSignInfoCache;
import com.cy.work.connect.logic.helper.ClearCacheHelper;
import com.cy.work.connect.logic.schedule.FlowDataSchedule;
import com.cy.work.connect.repository.WCMasterRepository;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 快取清除區 MBean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
public class Setting4Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7769072828623148049L;

    @Getter
    private String errorMessage;
    private final MessageCallBack messageCallBack =
        new MessageCallBack() {
            /** */
            private static final long serialVersionUID = 443205221969908626L;

            @Override
            public void showMessage(String m) {
                errorMessage = m;
                DisplayController.getInstance().update("confimDlgTemplate");
                DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
            }
        };
    @Autowired
    private WCManagerSignInfoCache wcManagerSignInfoCache;
    @Autowired
    private WCExecManagerSignInfoCache wcExecManagerSignInfoCache;
    @Autowired
    private FlowDataSchedule flowDataSchedule;
    @Autowired
    private WCMasterRepository wcMasterRepository;
    @Setter
    @Getter
    private String no;
    @Autowired
    private WorkConnectRestClient workConnectRestClient;

    @PostConstruct
    public void init() {
    }

    public void clearAllCache() {

        List<String> messages = Lists.newArrayList();
        messages.add("");

        // ====================================
        // web 快取
        // ====================================
        try {
            SecurityCacheHelper.getInstance().doClearSecurityCache();
            messages.add(ClearCacheHelper.getLatestProcessLog(true));
        } catch (Exception e) {
            String message = "執行清除 web 全部快取失敗!" + e.getMessage();
            messages.add(message);
            log.error(message, e);
        }

        // ====================================
        // REST 快取
        // ====================================
        try {
            messages.add("");
            messages.add("");
            messages.add("====================================");
            messages.add(" 開始清除【REST】所有快取");
            messages.add("====================================");
            // 呼叫 REST 執行清快取
            workConnectRestClient.clearAllCache();
            messages.add("★★★★執行清除 REST 全部快取成功★★★★");
        } catch (Exception e) {
            String message = "執行清除 REST 全部快取失敗!" + e.getMessage();
            messages.add(message);
            log.error(message, e);
        }

        MessagesUtils.showInfo(messages.stream().collect(Collectors.joining("<br/>")));
    }

    /**
     * 更新需求單位流程BPM資料及Cache
     */
    public void clearReqSignInfoCache() {
        try {
            wcManagerSignInfoCache.updateCache();
            wcManagerSignInfoCache.syncUpdateBpm();
            messageCallBack.showMessage("更新需求單位流程BPM資料及Cache");
        } catch (Exception e) {
            log.warn("clearReqSignInfoCache", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 更新執行單位流程BPM資料及Cache
     */
    public void clearExecSignInfoCache() {
        try {
            wcExecManagerSignInfoCache.updateCache();
            wcExecManagerSignInfoCache.syncUpdateBpm();
            messageCallBack.showMessage("更新執行單位流程BPM資料及Cache");
        } catch (Exception e) {
            log.warn("clearExecSignInfoCache", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void checkFlowData() {
        try {

            flowDataSchedule.checkAllData(SecurityFacade.getUserSid());
            messageCallBack.showMessage("檢測流程資料並修正");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("checkFlowData：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("checkFlowData", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void openCheckNoData() {
        this.no = "";
        DisplayController.getInstance().showPfWidgetVar("setting_dlg_wv");
    }

    public void checkNoData() {
        try {
            Preconditions.checkState(!Strings.isNullOrEmpty(no), "請輸入單號！");
            List<WCMaster> wcMasters = wcMasterRepository.findByNo(no);
            Preconditions.checkState(wcMasters != null && !wcMasters.isEmpty(), "單號無對應資料！");
            flowDataSchedule.checkOneData(wcMasters.get(0), SecurityFacade.getUserSid());
            messageCallBack.showMessage("檢測流程資料並修正");
            DisplayController.getInstance().hidePfWidgetVar("setting_dlg_wv");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("checkNoData：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("checkNoData", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }
}
