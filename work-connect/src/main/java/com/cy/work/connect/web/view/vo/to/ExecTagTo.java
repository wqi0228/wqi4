/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.to;

import java.io.Serializable;
import lombok.Data;

/**
 * @author kasim
 */
@Data
public class ExecTagTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9180297171408970057L;

    /**
     * 執行單位sid[wc_exec_dep.wc_exec_dep_sid]
     */
    private String exceDepSid;

    /**
     * 小類name
     */
    private String tagName;

    public ExecTagTo(String exceDepSid, String tagName) {
        this.exceDepSid = exceDepSid;
        this.tagName = tagName;
    }
}
