/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCReadReceiptsManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCReadReceipts;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import com.cy.work.connect.vo.enums.WCReadStatus;
import com.cy.work.connect.web.view.vo.ReadReceiptVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 索取回條邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCReadReceiptComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1289779571970949072L;

    private static WCReadReceiptComponents instance;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCReadReceiptsManager wcReadReceiptsManager;

    public static WCReadReceiptComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCReadReceiptComponents.instance = this;
    }

    /**
     * 取得索取讀取回條介面物件List By 工作聯絡單Sid
     *
     * @param wc_sid 工作聯絡單Sid
     * @return
     */
    public List<ReadReceiptVO> getReadReceiptVOByWcSid(String wc_sid) {
        List<WCReadReceipts> rpts = wcReadReceiptsManager.findByWcsid(wc_sid);
        if (rpts == null || rpts.isEmpty()) {
            return Lists.newArrayList();
        }
        List<ReadReceiptVO> readReceiptVOs = Lists.newArrayList();
        rpts.forEach(
            item -> {
                User user = userManager.findBySid(item.getReader());
                readReceiptVOs.add(
                    new ReadReceiptVO(
                        user.getName(),
                        user.getPrimaryOrg().getName(),
                        String.valueOf(item.getReader()),
                        item.getReadreceipt(),
                        ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getReadtime()),
                        ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                            item.getRequiretime()),
                        ""));
            });
        return readReceiptVOs;
    }

    public List<ReadReceiptVO> getReadInfoVOByWcSid(String wc_sid) {
        WCMaster wc = wcMasterManager.findBySid(wc_sid);
        if (wc.getRead_record() == null || wc.getRead_record().getRRecord().isEmpty()) {
            return Lists.newArrayList();
        }

        List<RRcordTo> readReceiptsMemberTos = wc.getRead_record().getRRecord();
        List<ReadReceiptVO> readReceiptVOs = Lists.newArrayList();
        Map<WCReadStatus, WCReadReceiptStatus> typeMaps = Maps.newHashMap();
        typeMaps.put(WCReadStatus.HASREAD, WCReadReceiptStatus.READ);
        typeMaps.put(WCReadStatus.WAIT_READ, WCReadReceiptStatus.READ);
        typeMaps.put(WCReadStatus.UNREAD, WCReadReceiptStatus.UNREAD);
        typeMaps.put(WCReadStatus.NEEDREAD, WCReadReceiptStatus.UNREAD);
        readReceiptsMemberTos.forEach(
            item -> {
                User user = userManager.findBySid(Integer.valueOf(item.getReader()));
                readReceiptVOs.add(
                    new ReadReceiptVO(
                        user.getName(),
                        user.getPrimaryOrg().getName(),
                        item.getReader(),
                        typeMaps.get(WCReadStatus.valueOf(item.getRead())),
                        item.getReadDay(),
                        item.getReadDay(),
                        item.getReadDay()));
            });
        return readReceiptVOs;
    }

    /**
     * 全部讀取[待閱讀] 依使用者Sid
     *
     * @param reader 使用者Sid
     */
    public void updateUNReadTimeAndReadReceiptByUserSid(Integer reader) {
        try {
            wcReadReceiptsManager.updateReadTime(reader);
        } catch (Exception e) {
            log.warn("updateUNReadTimeByUserSid Error", e);
        }
    }
}
