/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class ExecManagerInfoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 794417788324488841L;

    @Getter
    private final String depName;
    @Getter
    private final String signName;
    @Getter
    private final String roleName;
    @Getter
    private final String signTime;
    @Getter
    private final String addPersonName;
    @Getter
    private final String userSid;

    public ExecManagerInfoVO(
        String depName,
        String signName,
        String roleName,
        String signTime,
        String addPersonName,
        String userSid) {
        this.depName = depName;
        this.signName = signName;
        this.roleName = roleName;
        this.signTime = signTime;
        this.addPersonName = addPersonName;
        this.userSid = userSid;
    }
}
