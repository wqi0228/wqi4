/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.column;

import java.io.Serializable;
import lombok.Getter;

/**
 * dataTable 欄位細部資訊
 *
 * @author brain0925_liao
 */
public class ColumnDetailVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2126146733375369254L;

    @Getter
    private final String name;
    @Getter
    private final boolean show;
    @Getter
    private final String width;

    public ColumnDetailVO(String name, boolean show, String width) {
        this.name = name;
        this.show = show;
        this.width = width;
    }
}
