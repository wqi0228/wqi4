/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class UserModifyVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2077117829810444694L;

    @Getter
    private final String modifyUserName;
    @Getter
    private final String lastModifyTime;

    public UserModifyVO(String modifyUserName, String lastModifyTime) {
        this.modifyUserName = modifyUserName;
        this.lastModifyTime = lastModifyTime;
    }
}
