/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.cy.work.connect.web.common.setting.ExceStatusSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author kasim
 */
@Controller
@Scope("request")
public class ExceStatusItemBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6450495998339873987L;

    @Autowired
    private ExceStatusSetting exceStatusSetting;

    public List<SelectItem> getSearchItems() {
        return exceStatusSetting.getSearchItems();
    }
}
