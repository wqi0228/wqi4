/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.callable;

import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WorkFunItemComponentType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;

/**
 * 選單計算筆數Service
 *
 * @author brain0925_liao
 */
@Slf4j
public class HomeCallableService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6448258015716273691L;

    /**
     * 根據選單componentID取出相對應的計算筆數Callable
     *
     * @param workFunItemGroupVO 選單Group物件
     * @param componentID        componentID
     * @param loginUserSid       登入者Sid
     * @param depUserSid         登入者部門Sid
     * @return
     */
    public static HomeCallable getHomeCallableByWorkFunItemComponentType(
            WorkFunItemGroupVO workFunItemGroupVO,
            String componentID,
            Integer loginUserSid,
            Integer depUserSid) {
        // 待派工清單
        if (WorkFunItemComponentType.SEND_SEARCH.getComponentID().equals(componentID)) {
            return new SendSearchCallable(
                    workFunItemGroupVO, WorkFunItemComponentType.SEND_SEARCH, loginUserSid);
        }
        // 個人待處理清單
        else if (WorkFunItemComponentType.EXEC_SEARCH.getComponentID().equals(componentID)) {
            return new ExecSearchCallable(
                    workFunItemGroupVO, WorkFunItemComponentType.EXEC_SEARCH, loginUserSid);
        }
        // 待領單據清單
        else if (WorkFunItemComponentType.RECEVICE_SEARCH.getComponentID().equals(componentID)) {
            return new ReceviceSearchCallable(
                    workFunItemGroupVO, WorkFunItemComponentType.RECEVICE_SEARCH, loginUserSid);
        }
        // 部門待處理清單
        else if (WorkFunItemComponentType.WORKING_SEARCH.getComponentID().equals(componentID)) {
            return new WorkingSearchCallable(
                    workFunItemGroupVO, WorkFunItemComponentType.WORKING_SEARCH, loginUserSid);

        }
        return null;
    }

    /**
     * 執行計算筆數Callable並回傳result
     *
     * @param homeCallable
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<HomeCallableCondition> doHomeCallableList(List<HomeCallable> homeCallable) {
        List<HomeCallableCondition> results = Lists.newArrayList();
        if (homeCallable == null || homeCallable.isEmpty()) {
            return results;
        }
        ExecutorService pool = Executors.newFixedThreadPool(10);
        try {

            CompletionService completionPool = new ExecutorCompletionService(pool);
            homeCallable.forEach(item -> completionPool.submit(item));
            IntStream.range(0, homeCallable.size())
                    .forEach(
                            i -> {
                                try {
                                    Object result = completionPool.take().get();
                                    if (result == null) {
                                        return;
                                    }
                                    results.add((HomeCallableCondition) result);
                                } catch (InterruptedException | ExecutionException e) {
                                    log.warn("doHomeCallableList", e);
                                }
                            });
        } catch (Exception e) {
            log.warn("doHomeCallableList", e);
        } finally {
            pool.shutdown();
        }
        return results;
    }
}
