/**
 * 
 */
package com.cy.work.connect.web.view.setting10DepSettingCheck;

import lombok.Getter;

/**
 * @author allen1214_wu
 */
public enum DepSettingCheckSettingType {
    NONE("預設"), //非計算以下項目用
    EMPTY("未設定"),
    TEMPLETE("模版"),
    CUSTOMIZE("自定義");

    @Getter
    private final String descr;

    DepSettingCheckSettingType(String descr) {
        this.descr = "";
    }

}
