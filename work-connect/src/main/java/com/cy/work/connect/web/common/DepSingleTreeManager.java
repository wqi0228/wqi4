package com.cy.work.connect.web.common;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.web.util.pf.TreeNodeController;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * 單選組織樹
 *
 * @author jimmy_chou
 */
public class DepSingleTreeManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3261062883264822844L;

    private final WkOrgCache orgManager;
    private final WkStringUtils stringUtils;
    private final TreeNodeController nodeController;

    @Getter
    /** 畫面顯示的部門樹 */
    private TreeNode orgNode;
    /**
     * 預設部門樹(啟用、停用)
     */
    private TreeNode orgNodeByAll;
    /**
     * 預設部門樹(啟用)
     */
    private TreeNode orgNodeByActive;

    @Getter
    @Setter
    /** 畫面選取資料 */
    private TreeNode selectedNode;

    @Getter
    @Setter
    /** 查詢條件-部門名稱(like 部門名稱%) */
    private String orgName;

    @Getter
    @Setter
    /** 是否不需顯示停用部門 */
    private Boolean noShowDepHistory = true;

    /**
     * 預設選取部門
     */
    private Integer defaultOrg;

    public DepSingleTreeManager() {
        orgManager = WkOrgCache.getInstance();
        stringUtils = WkStringUtils.getInstance();
        nodeController = TreeNodeController.getInstance();
    }

    /**
     * 建立公司部門樹
     *
     * @param comp
     */
    public void initBuildTree(Integer comp) {
        orgNodeByActive = new DefaultTreeNode();
        orgNodeByAll = new DefaultTreeNode();
        orgNodeByActive.getChildren().add(new DefaultTreeNode(comp));
        orgNodeByAll.getChildren().add(new DefaultTreeNode(comp));
        this.buildSubTree(comp, orgManager.findAllDepSidByCompSid(comp), orgNodeByActive,
            orgNodeByAll);
    }

    /**
     * 建立子部門樹
     *
     * @param parent
     * @param allOrgs
     * @param orgNodeByActive
     * @param orgNodeByAll
     */
    private void buildSubTree(
        Integer parent, List<Integer> allOrgs, TreeNode orgNodeByActive, TreeNode orgNodeByAll) {
        List<Integer> children = this.filterChildren(parent, allOrgs);
        if (children == null || children.isEmpty()) {
            return;
        }
        children.forEach(
            each -> {
                TreeNode nodeByActive = null;
                TreeNode nodeByAll = new DefaultTreeNode(each, orgNodeByAll);
                if (orgNodeByActive != null
                    && Activation.ACTIVE.equals(
                    Optional.ofNullable(orgManager.findBySid(each))
                        .map(Org::getStatus)
                        .orElseGet(() -> null))) {
                    nodeByActive = new DefaultTreeNode(each, orgNodeByActive);
                }
                this.buildSubTree(each, allOrgs, nodeByActive, nodeByAll);
            });
    }

    public void buildTagTree(
        Integer parent, List<Integer> allOrgs, TreeNode orgNodel, List<Integer> usedOrgs) {
        List<Integer> children = this.filterChildren(parent, allOrgs);
        if (children == null || children.isEmpty()) {
            return;
        }

        children.forEach(
            each -> {
                TreeNode nodeByAll = new DefaultTreeNode(each, orgNodel);
                this.buildTagTree(each, allOrgs, nodeByAll, usedOrgs);
                usedOrgs.add(each);
            });
    }

    // 尋找最上層單位(群)
    public Org findTopParent(Org org, Integer compSid) {
        if (org.getParent() == null || compSid.equals(org.getParent().getSid())) {
            return org;
        }
        Org parentOrg = orgManager.findBySid(org.getParent().getSid());
        if (parentOrg.getParent() == null || compSid.equals(parentOrg.getParent().getSid())) {
            return parentOrg;
        }
        return this.findTopParent(parentOrg, compSid);
    }

    /**
     * 取得子組織
     *
     * @param parentOrg
     * @param allOrgs
     * @return
     */
    private List<Integer> filterChildren(Integer parentOrg, List<Integer> allOrgs) {
        List<Integer> children = Lists.newArrayList();
        allOrgs.forEach(
            each -> {
                Integer parent =
                    Optional.ofNullable(orgManager.findBySid(each))
                        .map(Org::getParent)
                        .map(SimpleOrg::getSid)
                        .orElseGet(() -> null);
                if (parent != null && parent.equals(parentOrg)) {
                    children.add(each);
                }
            });
        return children;
    }

    /**
     * 建立組織樹
     *
     * @param noShowDepHistory
     */
    public void createDepTree(Boolean noShowDepHistory) {
        this.createDepTree(null, noShowDepHistory);
    }

    /**
     * 建立組織樹
     *
     * @param defaultOrg
     * @param noShowDepHistory
     */
    public void createDepTree(Integer defaultOrg, Boolean noShowDepHistory) {
        this.selectedNode = null;
        this.noShowDepHistory = noShowDepHistory;
        this.defaultOrg = defaultOrg;
        this.changeTree();
    }

    /**
     * 組合組織樹
     */
    public void changeTree() {
        orgNode = this.getBulidDepRoot();
        this.initDepTree(orgNode);
    }

    /**
     * 依據是否停用 回傳對應資料
     *
     * @return
     */
    private TreeNode getBulidDepRoot() {
        if (noShowDepHistory) {
            return orgNodeByActive;
        } else {
            return orgNodeByAll;
        }
    }

    /**
     * 組合畫面部門組織樹
     *
     * @param depTree
     */
    private void initDepTree(TreeNode depTree) {
        nodeController.expandAll(depTree, false);
        List<TreeNode> list = Lists.newArrayList();
        nodeController.allNode(list, depTree);
        Integer selectedSid = this.redirectedSelected();
        list.forEach(
            each -> {
                if (selectedSid != null && selectedSid.equals(each.getData())) {
                    each.setSelected(true);
                    selectedNode = each;
                    nodeController.expandForChildren(each);
                } else {
                    each.setSelected(false);
                }
            });
    }

    /**
     * 重新指向 selectedNode
     *
     * @return
     */
    private Integer redirectedSelected() {
        Integer selectedSid = null;
        if (selectedNode != null && selectedNode.getData() != null) {
            selectedSid = (Integer) selectedNode.getData();
            selectedNode = null;
        } else if (defaultOrg != null) {
            selectedSid = defaultOrg;
        }
        return selectedSid;
    }

    /**
     * 依照部門名稱進行搜尋
     */
    public void searchOrgName() {
        if (Strings.isNullOrEmpty(orgName)) {
            this.changeTree();
            return;
        }
        orgNode = new DefaultTreeNode();
        this.getBulidDepRoot()
            .getChildren()
            .forEach(
                each -> {
                    TreeNode treeNode = this.filterOrgName(each);
                    if (treeNode != null) {
                        orgNode.getChildren().add(treeNode);
                    }
                });
        this.initDepTree(orgNode);
    }

    /**
     * 過濾名稱
     *
     * @param node
     * @return
     */
    private TreeNode filterOrgName(TreeNode node) {
        Integer sid = (Integer) node.getData();
        if (node.getChildCount() > 0) {
            DefaultTreeNode parent = new DefaultTreeNode();
            node.getChildren()
                .forEach(
                    each -> {
                        TreeNode treeNode = this.filterOrgName(each);
                        if (treeNode != null) {
                            parent.getChildren().add(treeNode);
                        }
                    });
            return this.checkParent(parent, sid);
        } else if (this.checkOrgName(sid)) {
            return new DefaultTreeNode(sid);
        }
        return null;
    }

    /**
     * 判斷節點名稱是否符合
     *
     * @param sid
     * @return
     */
    private Boolean checkOrgName(Integer sid) {
        return stringUtils.contains(WkOrgUtils.findNameBySid(sid), orgName);
    }

    /**
     * 判斷父節點是否有Children或者文字符合
     *
     * @param parent
     * @param sid
     * @return
     */
    private TreeNode checkParent(DefaultTreeNode parent, Integer sid) {
        if (parent.getChildCount() > 0 || this.checkOrgName(sid)) {
            parent.setData(sid);
            return parent;
        }
        return null;
    }

    /**
     * 取得選取Sid
     *
     * @return
     */
    public Integer getSelectedNodeSid() {
        if (selectedNode != null && selectedNode.getData() != null) {
            return (Integer) selectedNode.getData();
        }
        return null;
    }

    /**
     * 停用部門給予紅色字體
     *
     * @param sid
     * @return
     */
    public String getStyle(Integer sid) {
        Activation status =
            Optional.ofNullable(WkOrgCache.getInstance().findBySid(sid))
                .map(Org::getStatus)
                .orElseGet(() -> null);
        if (Activation.ACTIVE.equals(status)) {
            return "";
        }
        return "color: red";
    }
}
