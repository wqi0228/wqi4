/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class ManagerMenuVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8929818831570882724L;

    @Getter
    private final String menuSid;
    @Getter
    private final String categoryName;
    @Getter
    private final String menuName;
    @Getter
    private final String menuMemo;

    public ManagerMenuVO(String menuSid, String categoryName, String menuName, String menuMemo) {
        this.menuSid = menuSid;
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.menuMemo = menuMemo;
    }
}
