/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.bpm.rest.vo.ProcessRollBackInfo;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class FlowNodeVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2198963710277832139L;

    @Getter
    private final String roleName;
    @Getter
    private final String signName;
    @Getter
    private final String signTime;
    @Getter
    private final List<ProcessRollBackInfo> rollbackInfo;

    public FlowNodeVO(
        String roleName, String signName, String signTime, List<ProcessRollBackInfo> rollbackInfo) {
        this.roleName = roleName;
        this.signName = signName;
        this.signTime = signTime;
        this.rollbackInfo = rollbackInfo;
    }
}
