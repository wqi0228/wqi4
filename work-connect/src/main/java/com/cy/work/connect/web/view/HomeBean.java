/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WCFuncGroupBaseType;
import com.cy.work.connect.web.logic.components.CheckAuthorityComponent;
import com.cy.work.connect.web.logic.components.WCHomeLogicComponents;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 工作聯絡單 - 首頁 MBean
 *
 * @author brain0925_liao
 */
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class HomeBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7574536444826199094L;

    @Getter
    private final WCFuncGroupBaseType homeType = WCFuncGroupBaseType.HOME;
    @Getter
    private final WCFuncGroupBaseType maintainType = WCFuncGroupBaseType.MAIN_TAIN;
    @Getter
    private final WCFuncGroupBaseType searchType = WCFuncGroupBaseType.SEARCH;
    @Getter
    private final WCFuncGroupBaseType addType = WCFuncGroupBaseType.ADD;
    @Autowired
    private WCHomeLogicComponents wcHomeLogicComponents;
    @Autowired
    private DisplayController displayController;
    @Getter
    private List<WorkFunItemGroupVO> workFunItemGroups;
    @Autowired
    private WCMemoCategoryLogicComponent wcMemoCategoryLogicComponent;
    @Autowired
    private CheckAuthorityComponent checkAuthorityComponent;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    private boolean showMaintainBtn = false;

    @Getter
    @Setter
    private String menuitemId;

    private String keepType = searchType.getKey();

    private String lock = "false";

    @Getter
    /** 是否為新增模式 */
    private Boolean isCreateMode = false;

    @Getter
    /** 是否顯示備忘錄相關按鈕 */
    private boolean showMemo = false;
    /**
     * 是否顯示核決權限表
     */
    @Getter
    private boolean showCheckAuthoritySearch = false;

    @PostConstruct
    void init() {
        parseRequestParameter();

        userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());

        List<WorkFunItemGroupVO> maintain_workFunItemGroupVO = wcHomeLogicComponents.getWorkFunItemGroupByTypeAndUserPermission(
                maintainType.getKey(), 
                userViewVO.getSid(), 
                depViewVo.getSid());
        
        if (maintain_workFunItemGroupVO != null && !maintain_workFunItemGroupVO.isEmpty()) {
            maintain_workFunItemGroupVO.forEach(
                    item -> {
                        if (item.getWorkFunItemVOs() != null && !item.getWorkFunItemVOs().isEmpty()) {
                            showMaintainBtn = true;
                        }
                    });
        }
        this.showMemo = wcMemoCategoryLogicComponent.isMemoViewPermission();
        this.showCheckAuthoritySearch = checkAuthorityComponent.isCheckAuthoritySearchPermission(
                compViewVo.getSid(), depViewVo.getSid(), userViewVO.getSid());
        this.workFunItemGroups = wcHomeLogicComponents.getWorkFunItemGroupByTypeAndUserPermission(
                keepType, userViewVO.getSid(), depViewVo.getSid());
    }

    private void parseRequestParameter() {
        isCreateMode = false;
        if (Faces.getRequestParameterMap().containsKey("type")) {
            keepType = Faces.getRequestParameterMap().get("type");
        }
        if (Faces.getRequestParameterMap().containsKey("menuitemId")) {
            menuitemId = Faces.getRequestParameterMap().get("menuitemId");
        }
        if (Faces.getRequestParameterMap().containsKey("lock")) {
            lock = Faces.getRequestParameterMap().get("lock");
        }
        if (keepType.equals(WCFuncGroupBaseType.ADD.getKey())) {
            displayController.execute("changeUrlByFrame('../worp/worp_create.xhtml')");
            isCreateMode = true;
        }
    }

    public void reloadLeftMenu() throws IOException {
        parseRequestParameter();
        String url = "../home/home.xhtml?type=" + keepType + "&menuitemId=" + menuitemId + "&lock=" + lock;
        Faces.getExternalContext().redirect(url);
    }
}
