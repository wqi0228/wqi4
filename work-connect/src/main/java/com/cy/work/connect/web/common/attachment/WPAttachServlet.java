/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common.attachment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.vo.basic.BasicAttachmentService;
import com.cy.work.common.vo.basic.BasicAttachmentServlet;
import com.cy.work.connect.vo.WCAttachment;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.web.listener.PreviousAndNextCallBack;
import com.cy.work.connect.web.logic.components.WCAttachmentLogicComponents;
import com.cy.work.connect.web.logic.components.WCMasterLogicComponents;
import com.cy.work.connect.web.view.components.WorkReportViewComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.net.MediaType;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@WebServlet("/WPAttachServlet")
@Component
@Slf4j
public class WPAttachServlet extends HttpServlet implements BasicAttachmentServlet {


    // 要覆寫
    public static final String SERVLET_NAME = WPAttachServlet.class.getSimpleName();
    /**
     *
     */
    private static final long serialVersionUID = -5759060896550596282L;

    private static final String illegal_readPath = "/error/illegal_read.xhtml";
    private static final String error_readPath = "/error/error.xhtml";

    private final PreviousAndNextCallBack previousAndNextCallBack = new PreviousAndNextCallBack() {
        /** */
        private static final long serialVersionUID = 9025663858044400057L;

        @Override
        public void doPrevious() {
        }

        @Override
        public void doNext() {
        }
    };

    @SuppressWarnings("rawtypes")
    @Override
    public BasicAttachmentService getAttachService(HttpServletRequest request) {
        return WCAttachmentLogicComponents.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (SecurityFacade.getUserSid() == null || SecurityFacade.getUserSid() <= 0) {
            response.sendRedirect(request.getContextPath() + illegal_readPath);
            return;
        }

        try {
            if (request.getParameterMap().containsKey(FILE_DOWNLOAD_KEY)) {
                this.doGetFile(request, response);
            } else {
                this.doGetAttachment(request, response);
            }
        } catch (Exception e) {
            log.error(WkMessage.EXECTION + "[檔案下載錯誤] " + e.getMessage(), e);
            response.sendRedirect(request.getContextPath() + error_readPath);
        }

    }

    private void doGetAttachment(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        UserViewVO userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        OrgViewVo depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        OrgViewVo compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        
        WorkReportViewComponent workReportViewComponent = new WorkReportViewComponent(
                userViewVO, depViewVo, compViewVo, previousAndNextCallBack);
        String attachmentSid = (String) this.findAttachSid(request);
        WCAttachment wc = WCAttachmentLogicComponents.getInstance()
                .getWCAttachmentBySid(attachmentSid);
        WCMaster master = WCMasterLogicComponents.getInstance().getWCMasterBySid(wc.getWc_sid());
        boolean checkPermission = workReportViewComponent.checkViewWorkReportPermission(master);
        if (!checkPermission) {
            response.sendRedirect(request.getContextPath() + illegal_readPath);
            return;
        }

        BasicAttachmentServlet.super.doGet(request, response, super.getServletContext());
    }

    @Override
    public Object findAttachSid(HttpServletRequest request) {
        return String.valueOf(this.getAttachmentSidParameter(request));
    }

    private void doGetFile(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // ====================================
        // 取得要下載的檔案
        // ====================================
        // 取得餐數值
        String fileAlias = request.getParameter(FILE_DOWNLOAD_KEY);
        // 判斷下載類別
        FileDownloadType fileDownloadType = null;
        for (FileDownloadType currType : FileDownloadType.values()) {
            if (currType.getFileAlias().equals(fileAlias)) {
                fileDownloadType = currType;
                break;
            }
        }
        // 無此檔案時，導向無權限頁
        if (fileDownloadType == null) {
            response.sendRedirect(request.getContextPath() + illegal_readPath);
            return;
        }

        // 取得檔案
        String filePath = getServletContext().getRealPath("/") + fileDownloadType.getFilePath();
        File attachmentFile = new File(filePath);
        if (!attachmentFile.exists()) {
            log.error("檔案不存在![{}]", filePath);
            response.sendRedirect(request.getContextPath() + error_readPath);
            return;
        }

        String encodedFileName = this.encodeFileName(fileDownloadType.getOutputFileName());
        String contentDispositionPattern = this.getAttachService(request)
                .isMSIE(request)
                        ? "{0}; filename=\"{1}\""
                        : "{0}; filename=\"{1}\"; filename*=UTF-8''''\"{1}\"";
        String contentDisposition = MessageFormat.format(contentDispositionPattern, "inline", encodedFileName);

        response.setContentType(fileDownloadType.getMediaType().toString());
        response.setContentLength((int) attachmentFile.length());
        response.setHeader("Content-Disposition", contentDisposition);

        FileInputStream in = null;
        try {
            in = new FileInputStream(attachmentFile);
            OutputStream out = response.getOutputStream();
            IOUtils.copy(in, response.getOutputStream());
            out.flush();
        } finally {
            if (in != null) {
                IOUtils.closeQuietly(in);
            }
        }

    }

    private String encodeFileName(String fileName) {
        String encodedFileName = fileName;
        try {
            URI uri = new URI(null, null, fileName, null);
            encodedFileName = uri.toASCIIString();
        } catch (URISyntaxException e) {
            System.err.println(e.getMessage());
        }
        return encodedFileName;
    }

    public final static String FILE_DOWNLOAD_KEY = "fdk";

    public enum FileDownloadType {
        CHECK_AUTH_TEMPLETE(
                "核決權限表-填寫模版",
                "Op8QLy",
                MediaType.MICROSOFT_EXCEL,
                "file/check_auth_templet.xls",
                "核決權限表-填寫模版.xls"),
                ;

        @Getter
        private String descr;
        @Getter
        private String fileAlias;
        @Getter
        private MediaType mediaType;
        @Getter
        private String filePath;
        @Getter
        private String outputFileName;

        FileDownloadType(String descr, String fileAlias, MediaType mediaType, String fileName, String outputFileName) {
            this.descr = descr;
            this.fileAlias = fileAlias;
            this.mediaType = mediaType;
            this.filePath = fileName;
            this.outputFileName = outputFileName;
        }

    }

}
