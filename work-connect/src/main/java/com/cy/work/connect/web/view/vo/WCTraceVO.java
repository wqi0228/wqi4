/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.enums.WCTraceType;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class WCTraceVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6862067023084674085L;

    @Getter
    private String traceType;
    @Getter
    private WCTraceType type;
    @Getter
    private String userInfo;
    @Getter
    private String time;
    @Getter
    private UserDep correctDep;
    @Getter
    private String correctDepFullName;
    @Getter
    private String correctDepName;
    @Getter
    @Setter
    private String content;
    @Getter
    @Setter
    private String sid;
    @Getter
    private boolean showReplyBtn;
    @Getter
    private int index;
    @Getter
    private Integer createUserSid;
    @Getter
    @Setter
    private boolean showReplyEditBtn;
    @Getter
    @Setter
    private List<WCAgainTraceVO> againTrace;

    public WCTraceVO(String sid) {
        this.sid = sid;
    }

    public WCTraceVO(
        int index,
        String sid,
        WCTraceType type,
        String userInfo,
        String time,
        String content,
        UserDep correctDep,
        String correctDepFullName,
        String correctDepName,
        boolean showReplyBtn,
        Integer createUserSid) {
        this.sid = sid;
        this.type = type;
        if (type != null) {
            this.traceType = type.getVal();
        } else {
            this.traceType = "";
        }
        this.userInfo = userInfo;
        this.time = time;
        this.content = content;
        this.correctDep = correctDep;
        this.correctDepFullName = correctDepFullName;
        this.correctDepName = correctDepName;
        this.showReplyBtn = showReplyBtn;
        this.index = index;
        this.createUserSid = createUserSid;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WCTraceVO other = (WCTraceVO) obj;
        return Objects.equals(this.sid,
            other.sid);
    }
}
