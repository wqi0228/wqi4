/**
 * 
 */
package com.cy.work.connect.web.logic.components;

/**
 * @author allen1214_wu
 */
public enum CanViewType {

    /**
     * 無可閱權限
     */
    ACCESS_DENIED,
    /**
     * 可閱全部
     */
    ALL,
    /**
     * 管理者特殊可閱-遮罩模式
     */
    ADMIN_SPECIAL_MASK_MODE,
    /**
     * 管理者特殊可閱-全可閱模式
     */
    ADMIN_SPECIAL_ALL_VIEW_MODE
}
