package com.cy.work.connect.web.util.pf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import com.google.common.net.MediaType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CommonBean {

    /**
     * for 清除 client 端 js 和 css 的快取用 (更換版本號即更新)
     *
     * @return
     */
    public String getVersion() {
        try {
            return PomPropertiesLoader.getProperty("version");
        } catch (Exception e) {
            log.warn("取得系統版本別失敗![" + e.getMessage() + "]\r\n", e);
            return this.getSysTime();
        }
    }

    /**
     * for 清除 client 端 js 和 css 的快取用 (每次刷新頁面即更新)
     *
     * @return
     */
    public String getSysTime() { return System.currentTimeMillis() + ""; }

    /**
     * 輸出檔案
     * 
     * @param mediaType
     * @param filePath
     * @param outputFileName
     */
    public void exportFile(MediaType mediaType, String filePath, String outputFileName) {

        String fileName = "file";
        try {
            // 取代+號是為了處理檔名中的空白字元問題
            fileName = java.net.URLEncoder.encode(outputFileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e1) {
            MessagesUtils.showError("檔案名稱處理錯誤!");
            log.error("檔案名稱處理錯誤! fileName:[" + outputFileName + "]", e1);
            return;
        }

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext ec = facesContext.getExternalContext();

        ec.setResponseContentType(mediaType.toString());
        ec.responseReset();

        ec.setResponseHeader("Content-Disposition", "attachment;filename*=utf-8'zh_TW'" + fileName); // 等號前要加*號，第一個位置編碼，第二個位置國別，然後檔名

        OutputStream output = null;
        try {
            InputStream is = this.getClass().getClassLoader().getResourceAsStream(filePath);
            output = ec.getResponseOutputStream();
            IOUtils.copy(is, output);
            output.flush();

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(output);
            facesContext.responseComplete();
            DisplayController.getInstance().execute("hideLoad();");
        }
    }
}
