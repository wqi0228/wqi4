/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class WCLastModifySearchColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7548234676394162636L;

    @Setter
    @Getter
    private ColumnDetailVO index;
    @Setter
    @Getter
    private ColumnDetailVO createTime;
    @Setter
    @Getter
    private ColumnDetailVO department;
    @Setter
    @Getter
    private ColumnDetailVO createUser;
    @Setter
    @Getter
    private ColumnDetailVO theme;

    @Setter
    @Getter
    private ColumnDetailVO no;
    @Setter
    @Getter
    private ColumnDetailVO lastModifyTime;
    @Setter
    @Getter
    private ColumnDetailVO memo;
    @Setter
    @Getter
    private ColumnDetailVO status;
    @Setter
    @Getter
    private ColumnDetailVO editType;

    @Setter
    @Getter
    private String pageCount = "50";
}
