package com.cy.work.connect.web.view.vo;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class MenuTagEditVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3007873853597376728L;
    @Getter
    private final List<Org> orgs = Lists.newArrayList();
    @Getter
    private final List<User> users = Lists.newArrayList();
    /**
     * 部門設定模版選項
     */
    @Setter
    @Getter
    List<WCConfigTempletVo> uesDepsTempletItems;
    @Setter
    @Getter
    private String sid;
    @Setter
    @Getter
    private String name;
    @Setter
    @Getter
    private String itemName;
    @Setter
    @Getter
    private String memo;
    // @Setter
    // @Getter
    // private String reqFlowType;
    @Setter
    @Getter
    private String status;
    @Setter
    @Getter
    private String seq;
    @Setter
    @Getter
    private String defaultTheme;
    @Setter
    @Getter
    private String defaultContent;
    @Setter
    @Getter
    private String defaultMemo;
    @Setter
    @Getter
    private boolean attachmentType = false;
    @Setter
    @Getter
    private String attachmentPath;
    @Setter
    @Getter
    private boolean toParentManager;
    @Setter
    @Getter
    private boolean isUserContainFollowing;
    /**
     * 可使用單位-是否僅主管可使用
     */
    @Setter
    @Getter
    private boolean onlyForUseDepManager;
    /**
     * 需求方最終簽核人員
     */
    @Setter
    @Getter
    private Integer reqFinalApplyUserSid;

    @Setter
    @Getter
    private String reqFinalApplyUserDisplayContent;
    /**
     * 最終簽核排除項目 (issue WORKCOMMU-356)
     */
    @Setter
    @Getter
    private List<String> reqFinalApplyIgnoredWcTagSids;

    @Setter
    @Getter
    private List<SelectItem> wcTagItems;
    /**
     * 可使用單位 - 套用模版
     */
    @Setter
    @Getter
    private Long uesDepTempletSid;
    /**
     * 可使用單位顯示文字
     */
    @Setter
    @Getter
    private String uesDepsShow;
    /**
     * 可使用單位顯示文字
     */
    @Setter
    @Getter
    private String uesDepsShowToolTip;
    
    @Setter
    @Getter
    private String uesUsersShow;
    /**
     * 可使用人員顯示文字
     */
    @Setter
    @Getter
    private String uesUsersShowToolTip;

    /**
     * 中類選單設定 - 可使用部門
     */
    public void prepareUseDepsSettingContent(List<Org> selectOrgs) {

        WorkSettingOrgManager workSettingOrgManager = WorkSettingOrgManager.getInstance();

        if (this.uesDepTempletSid == null) {

            List<Integer> depSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(selectOrgs)) {
                depSids = selectOrgs.stream().map(Org::getSid).collect(Collectors.toList());
            }
            // 含以下單位先最取得最上層處理
            if (this.isUserContainFollowing) {
                depSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(depSids));
            }
            // 依組織樹排序
            depSids = WkOrgUtils.sortByOrgTree(depSids);

            // 產生顯示文字
            this.setUesDepsShowToolTip(
                workSettingOrgManager.prepareDepsShowText(depSids, this.isUserContainFollowing,
                    false));

        } else {
            for (WCConfigTempletVo wcConfigTempletVo : this.uesDepsTempletItems) {
                if ((wcConfigTempletVo.getSid() + "").equals(this.getUesDepTempletSid() + "")) {
                    // 產生顯示文字
                    this.setUesDepsShowToolTip(
                        workSettingOrgManager.prepareDepsShowText(
                            wcConfigTempletVo.getConfigValue().getDeps(),
                            wcConfigTempletVo.getConfigValue().isDepContainFollowing(),
                            wcConfigTempletVo.getConfigValue().isDepExcept()));
                }
            }
        }

        if (WkStringUtils.isEmpty(this.getUesDepsShowToolTip())) {
            this.setUesDepsShow("全單位");
            this.setUesDepsShowToolTip("全單位");
        } else {
            this.setUesDepsShow("詳細設定內容");
        }
    }
    
    public void prepareUseUsersSettingContent(List<User> selectUsers) {
        if(WkStringUtils.isEmpty(selectUsers)) {
            this.uesUsersShow = "未設定";
            this.uesUsersShowToolTip = "未設定";
            return;
        }
        
        this.uesUsersShow = selectUsers.stream()
                .map(user -> WkUserUtils.safetyGetName(user))
                .collect(Collectors.joining("、"));
        
        
        this.uesUsersShowToolTip = selectUsers.stream()
                .map(user -> WkUserUtils.prepareUserNameWithDep(user.getSid(), OrgLevel.DIVISION_LEVEL, false, "-"))
                .collect(Collectors.joining("<br/>"));
        
    }
}
