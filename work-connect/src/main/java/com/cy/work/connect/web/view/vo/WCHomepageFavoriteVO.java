/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class WCHomepageFavoriteVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7853790230202844252L;

    @Getter
    private final String pagePath;
    @Getter
    private final String pageName;

    public WCHomepageFavoriteVO(String pagePath, String pageName) {
        this.pagePath = pagePath;
        this.pageName = pageName;
    }
}
