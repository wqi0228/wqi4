/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class CategoryTagSearchVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7453967951704743849L;

    @Getter
    private final String sid;
    @Getter
    private String createTime;
    @Getter
    private String creatUser;
    @Getter
    private String tagName;
    @Getter
    private String status;
    @Getter
    private String modifyTime;
    @Getter
    private String modifyUser;
    @Getter
    private String seq;
    @Getter
    private String memo;

    public CategoryTagSearchVO(String sid) {
        this.sid = sid;
    }

    public CategoryTagSearchVO(
        String sid,
        String createTime,
        String creatUser,
        String tagName,
        String status,
        String modifyTime,
        String modifyUser,
        String seq,
        String memo) {
        this.sid = sid;
        this.createTime = createTime;
        this.creatUser = creatUser;
        this.tagName = tagName;
        this.status = status;
        this.modifyTime = modifyTime;
        this.modifyUser = modifyUser;
        this.seq = seq;
        this.memo = memo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategoryTagSearchVO other = (CategoryTagSearchVO) obj;
        return Objects.equals(this.sid,
            other.sid);
    }
}
