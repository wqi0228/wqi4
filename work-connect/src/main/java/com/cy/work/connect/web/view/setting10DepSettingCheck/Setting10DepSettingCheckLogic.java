/**
 * 
 */
package com.cy.work.connect.web.view.setting10DepSettingCheck;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCExecDepSettingCache;
import com.cy.work.connect.logic.cache.WCMenuTagCache;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.vo.view.setting11.SettingOrgTransMappingVO;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
@Service
public class Setting10DepSettingCheckLogic {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WCCategoryTagCache wCCategoryTagCache;
    @Autowired
    private WCMenuTagCache wcMenuTagCache;
    @Autowired
    private WCTagCache wcTagCache;
    @Autowired
    private WCExecDepSettingCache wcExecDepSettingCache;
    @Autowired
    private WCConfigTempletManager wcConfigTempletManager;
    @Autowired
    private WkCommonCache wkCommonCache;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 準備檢查項目資料-中類-可使用單位
     * 
     * @param compSid               公司別 sid
     * @param beforeOrgSid          異動前單位 sid
     * @param categoryTags          大類資料
     * @param configTempletMapBySid 所有模版資料
     * @param isOnlyType            僅兜組檢查項目類別 (計算筆數用)
     * @return
     */
    public List<Setting10DepSettingCheckItemVO> findCanUseDepBySecondLevel(
            Integer compSid,
            Integer beforeOrgSid,
            Integer afterOrgSid,
            List<WCCategoryTag> categoryTags,
            Map<Long, WCConfigTemplet> configTempletMapBySid,
            boolean isOnlyType) {

        List<Setting10DepSettingCheckItemVO> results = Lists.newArrayList();

        for (WCCategoryTag wcCategoryTag : categoryTags) {

            // 停用項目不顯示 by john
            if (!wcCategoryTag.isActive()) {
                continue;
            }

            // 查詢對應中類
            List<WCMenuTag> wcMenuTags = this.wcMenuTagCache.findByCategorySidAndOrderBySeq(wcCategoryTag.getSid());
            if (WkStringUtils.isEmpty(wcMenuTags)) {
                continue;
            }

            for (WCMenuTag wCMenuTag : wcMenuTags) {

                // 停用項目不顯示 by john
                if (!wCMenuTag.isActive()) {
                    continue;
                }

                // ====================================
                // 比對傳入單位是否命中
                // ====================================
                Setting10DepSettingCheckItemVO checkItemVO = this.prepareCheckItem(
                        DepSettingCheckItemType.MENUTAG_CAN_USE_DEP,
                        compSid,
                        beforeOrgSid,
                        afterOrgSid,
                        wCMenuTag.getUseDep(),
                        wCMenuTag.isUserContainFollowing(),
                        wCMenuTag.getUseDepTempletSid(),
                        configTempletMapBySid,
                        isOnlyType);

                // ====================================
                // 傳入單位命中時，加入檢查項目容器
                // ====================================
                if (checkItemVO != null) {

                    Setting10DepSettingCheckItemVO currCheckItemVO = checkItemVO;
                    // 非計數使用時，兜組顯示資訊
                    if (!isOnlyType) {
                        currCheckItemVO = new Setting10DepSettingCheckItemVO(
                                checkItemVO.getSettingType(),
                                checkItemVO.getDescr(),
                                wcCategoryTag,
                                wCMenuTag,
                                null);
                    }

                    results.add(currCheckItemVO);
                }
            }
        }
        return results;
    }

    /**
     * 準備檢查項目資料-小類-可使用單位
     * 
     * @param compSid               公司別 sid
     * @param targetOrgSid          比對單位 sid
     * @param categoryTags          大類資料
     * @param configTempletMapBySid 所有模版資料
     * @param isOnlyType            僅兜組檢查項目類別 (計算筆數用)
     * @return
     */
    public List<Setting10DepSettingCheckItemVO> findCanUseDepBythirdLevel(
            Integer compSid,
            Integer beforeOrgSid,
            Integer afterOrgSid,
            List<WCCategoryTag> categoryTags,
            Map<Long, WCConfigTemplet> configTempletMapBySid,
            boolean isOnlyType) {

        List<Setting10DepSettingCheckItemVO> results = Lists.newArrayList();

        for (WCCategoryTag wcCategoryTag : categoryTags) {

            // 停用項目不顯示 by john
            if (!wcCategoryTag.isActive()) {
                continue;
            }

            // 查詢對應中類
            List<WCMenuTag> wcMenuTags = this.wcMenuTagCache.findByCategorySidAndOrderBySeq(wcCategoryTag.getSid());
            if (WkStringUtils.isEmpty(wcMenuTags)) {
                continue;
            }

            for (WCMenuTag wCMenuTag : wcMenuTags) {

                // 停用項目不顯示 by john
                if (!wCMenuTag.isActive()) {
                    continue;
                }

                // 查詢對應小類
                List<WCTag> wcTags = this.wcTagCache.findByMenuTagSidAndOrderBySeq(wCMenuTag.getSid());
                if (WkStringUtils.isEmpty(wcMenuTags)) {
                    continue;
                }

                for (WCTag wcTag : wcTags) {

                    // 停用項目不顯示 by john
                    if (!wcTag.isActive()) {
                        continue;
                    }

                    // ====================================
                    // 比對傳入單位是否命中
                    // ====================================
                    Setting10DepSettingCheckItemVO checkItemVO = this.prepareCheckItem(
                            DepSettingCheckItemType.ITEM_CAN_VIEW_DEP,
                            compSid,
                            beforeOrgSid,
                            afterOrgSid,
                            wcTag.getCanUserDep(),
                            wcTag.isUserContainFollowing(),
                            wcTag.getCanUserDepTempletSid(),
                            configTempletMapBySid,
                            isOnlyType);

                    // ====================================
                    // 傳入單位命中時，加入檢查項目容器
                    // ====================================
                    if (checkItemVO != null) {

                        Setting10DepSettingCheckItemVO currCheckItemVO = checkItemVO;
                        // 非計數使用時，兜組顯示資訊
                        if (!isOnlyType) {
                            currCheckItemVO = new Setting10DepSettingCheckItemVO(
                                    checkItemVO.getSettingType(),
                                    checkItemVO.getDescr(),
                                    wcCategoryTag,
                                    wCMenuTag,
                                    wcTag);
                        }

                        results.add(currCheckItemVO);
                    }
                }
            }
        }

        return results;
    }

    /**
     * 準備檢查項目資料-小類-預設可閱單位
     * 
     * @param compSid               公司別 sid
     * @param targetOrgSid          比對單位 sid
     * @param categoryTags          大類資料
     * @param configTempletMapBySid 所有模版資料
     * @param isOnlyType            僅兜組檢查項目類別 (計算筆數用)
     * @return
     */
    public List<Setting10DepSettingCheckItemVO> findCanViewDepBythirdLevel(
            Integer compSid,
            Integer beforeOrgSid,
            Integer afterOrgSid,
            List<WCCategoryTag> categoryTags,
            Map<Long, WCConfigTemplet> configTempletMapBySid,
            boolean isOnlyType) {

        List<Setting10DepSettingCheckItemVO> results = Lists.newArrayList();

        for (WCCategoryTag wcCategoryTag : categoryTags) {

            // 停用項目不顯示 by john
            if (!wcCategoryTag.isActive()) {
                continue;
            }

            // 查詢對應中類
            List<WCMenuTag> wcMenuTags = this.wcMenuTagCache.findByCategorySidAndOrderBySeq(wcCategoryTag.getSid());
            if (WkStringUtils.isEmpty(wcMenuTags)) {
                continue;
            }

            for (WCMenuTag wCMenuTag : wcMenuTags) {

                // 停用項目不顯示 by john
                if (!wCMenuTag.isActive()) {
                    continue;
                }

                // 查詢對應小類
                List<WCTag> wcTags = this.wcTagCache.findByMenuTagSidAndOrderBySeq(wCMenuTag.getSid());
                if (WkStringUtils.isEmpty(wcMenuTags)) {
                    continue;
                }

                for (WCTag wcTag : wcTags) {

                    // 停用項目不顯示 by john
                    if (!wcTag.isActive()) {
                        continue;
                    }

                    // ====================================
                    // 比對傳入單位是否命中
                    // ====================================
                    Setting10DepSettingCheckItemVO checkItemVO = this.prepareCheckItem(
                            DepSettingCheckItemType.ITEM_CAN_VIEW_DEP,
                            compSid,
                            beforeOrgSid,
                            afterOrgSid,
                            wcTag.getUseDep(),
                            wcTag.isViewContainFollowing(),
                            wcTag.getUseDepTempletSid(),
                            configTempletMapBySid,
                            isOnlyType);

                    // ====================================
                    // 傳入單位命中時，加入檢查項目容器
                    // ====================================
                    if (checkItemVO != null) {
                        Setting10DepSettingCheckItemVO currCheckItemVO = checkItemVO;
                        // 非計數使用時，兜組顯示資訊
                        if (!isOnlyType) {
                            currCheckItemVO = new Setting10DepSettingCheckItemVO(
                                    checkItemVO.getSettingType(),
                                    checkItemVO.getDescr(),
                                    wcCategoryTag,
                                    wCMenuTag,
                                    wcTag);
                        }

                        results.add(currCheckItemVO);
                    }
                }
            }
        }

        return results;
    }

    /**
     * 準備檢查項目資料-執行單位
     * 
     * @param targetOrgSid 比對單位 sid
     * @param categoryTags 所有大類資料
     * @return Setting10DepSettingCheckItemVO list
     */
    public List<Setting10DepSettingCheckItemVO> findExecDep(
            Integer targetOrgSid,
            List<WCCategoryTag> categoryTags,
            boolean isCount) {

        List<Setting10DepSettingCheckItemVO> results = Lists.newArrayList();

        for (WCCategoryTag wcCategoryTag : categoryTags) {

            // 停用項目不顯示 by john
            if (!wcCategoryTag.isActive()) {
                continue;
            }

            // 查詢對應中類
            List<WCMenuTag> wcMenuTags = this.wcMenuTagCache.findByCategorySidAndOrderBySeq(wcCategoryTag.getSid());
            if (WkStringUtils.isEmpty(wcMenuTags)) {
                continue;
            }

            for (WCMenuTag wCMenuTag : wcMenuTags) {

                // 停用項目不顯示 by john
                if (!wCMenuTag.isActive()) {
                    continue;
                }

                // 查詢對應小類
                List<WCTag> wcTags = this.wcTagCache.findByMenuTagSidAndOrderBySeq(wCMenuTag.getSid());
                if (WkStringUtils.isEmpty(wcMenuTags)) {
                    continue;
                }

                for (WCTag wcTag : wcTags) {

                    // 停用項目不顯示 by john
                    if (!wcTag.isActive()) {
                        continue;
                    }

                    // 查詢對應執行單位
                    List<WCExecDepSetting> wcExecDepSettings = this.wcExecDepSettingCache.findByTagSid(wcTag.getSid());
                    if (WkStringUtils.isEmpty(wcExecDepSettings)) {
                        continue;
                    }

                    for (WCExecDepSetting wcExecDepSetting : wcExecDepSettings) {

                        // 停用單位不顯示 by john
                        if (!wcExecDepSetting.isActive()) {
                            continue;
                        }

                        if (WkCommonUtils.compareByStr(wcExecDepSetting.getExec_dep_sid(), targetOrgSid)) {
                            Setting10DepSettingCheckItemVO currCheckItemVO = null;
                            if (isCount) {
                                currCheckItemVO = new Setting10DepSettingCheckItemVO(DepSettingCheckSettingType.NONE);
                            } else {
                                currCheckItemVO = new Setting10DepSettingCheckItemVO(
                                        DepSettingCheckSettingType.NONE,
                                        "",
                                        wcCategoryTag,
                                        wCMenuTag,
                                        wcTag);
                            }

                            results.add(currCheckItemVO);
                        }

                    }
                }
            }
        }

        return results;
    }

    /**
     * 準備檢查項目
     * 
     * @param targetCheckItemType             檢查項目
     * @param compSid                         登入公司 sid
     * @param targetOrgSid                    標的單位 sid
     * @param customSettingDep                自行設定單位
     * @param customSettingIsContainFollowing 自行設定單位-是否含以下
     * @param settingTempletSid               設定模版 sid
     * @param configTempletMapBySid           所有模版資料
     * @param isOnlyType                      僅兜組檢查項目類別 (計算筆數用)
     * @return
     */
    private Setting10DepSettingCheckItemVO prepareCheckItem(
            DepSettingCheckItemType targetCheckItemType,
            Integer compSid,
            Integer beforeOrgSid,
            Integer afterOrgSid,
            UserDep customSettingDep,
            boolean customSettingIsContainFollowing,
            Long settingTempletSid,
            Map<Long, WCConfigTemplet> configTempletMapBySid,
            boolean isOnlyType) {

        // 取得模版設定
        WCConfigTemplet wCConfigTemplet = configTempletMapBySid.get(settingTempletSid);
        // 取得設定部門
        Set<Integer> settingDepSids = UserDep.parserDepSids(customSettingDep);

        // ====================================
        // 模版
        // ====================================
        if (wCConfigTemplet != null) {
            return this.prepareCheckItemByTemplete(
                    compSid,
                    beforeOrgSid,
                    afterOrgSid,
                    wCConfigTemplet,
                    isOnlyType);
        }

        // ====================================
        // 自設定
        // ====================================
        else if (WkStringUtils.notEmpty(settingDepSids)) {
            return this.prepareCheckItemByCustom(
                    compSid,
                    beforeOrgSid,
                    afterOrgSid,
                    settingDepSids,
                    customSettingIsContainFollowing,
                    isOnlyType);

        }

        // ====================================
        // 未設定
        // ====================================
        else {
            switch (targetCheckItemType) {
            case MENUTAG_CAN_USE_DEP:
                return new Setting10DepSettingCheckItemVO(
                        DepSettingCheckSettingType.EMPTY,
                        "全單位適用");
            case ITEM_USE_DEP:
                // 小可使用部門通常未設定，檢查未設定資料無意義
            case ITEM_CAN_VIEW_DEP:
                // 小類可閱部門，未設定就代表無任何單位命中，故無需顯示
            default:
                break;
            }
        }

        return null;
    }

    /**
     * 準備檢查項目 for 模版
     * 
     * @param compSid
     * @param beforeOrgSid
     * @param afterOrgSid
     * @param wCConfigTemplet
     * @param isOnlyType
     * @return
     */
    private Setting10DepSettingCheckItemVO prepareCheckItemByTemplete(
            Integer compSid,
            Integer beforeOrgSid,
            Integer afterOrgSid,
            WCConfigTemplet wCConfigTemplet,
            boolean isOnlyType) {

        DepSettingCheckSettingType settingType = DepSettingCheckSettingType.TEMPLETE;

        // ====================================
        // 取得模版設定部門
        // ====================================
        List<Integer> allCanUseDepSids = this.prepareTempletDepSids(wCConfigTemplet, compSid);

        // ====================================
        // 以下情況無需處理
        // ====================================
        // 和轉換前單位無關
        if (!allCanUseDepSids.contains(beforeOrgSid)) {
            return null;
        }
        // 轉換後單位已經包含在模版設定裡面了
        if (allCanUseDepSids.contains(afterOrgSid)) {
            return null;
        }

        // ====================================
        // 資料命中，組VO
        // ====================================
        // 僅計算筆數用，不組說明文字
        if (isOnlyType) {
            return new Setting10DepSettingCheckItemVO(settingType);
        }

        // 顯示內容
        String descr = WCConfigTempletManager.getInstance()
                .getPettyTempletName(
                        wCConfigTemplet.getTempletName(),
                        wCConfigTemplet.getConfigValue(), true, true);

        return new Setting10DepSettingCheckItemVO(
                settingType,
                descr);
    }

    private Setting10DepSettingCheckItemVO prepareCheckItemByCustom(
            Integer compSid,
            Integer beforeOrgSid,
            Integer afterOrgSid,
            Set<Integer> settingDepSids,
            boolean isContainFollowing,
            boolean isOnlyType) {

        DepSettingCheckSettingType settingType = DepSettingCheckSettingType.CUSTOMIZE;

        // ====================================
        // 準備可使用部門設定值
        // ====================================
        // 手動設定的可使用部門
        // 先複製一份設定值
        Set<Integer> manualSettingDepSids = settingDepSids.stream().collect(Collectors.toSet());

        // 全部可使用部門 (加上含以下)
        Set<Integer> allCanUseDepSids = Sets.newHashSet(manualSettingDepSids);
        if (isContainFollowing) {
            // 算出含以下單位 (包含本身)
            Set<Integer> allChildSids = WkOrgCache.getInstance().findAllChildSids(manualSettingDepSids);
            if(WkStringUtils.notEmpty(allChildSids)) {
                allCanUseDepSids.addAll(allChildSids);
            }
        }

        // ====================================
        // 此項目設定不包含轉換前單位 (此項目和此次異動無關)
        // ====================================
        if (!allCanUseDepSids.contains(beforeOrgSid)) {
            return null;
        }

        // ====================================
        // 排除條件
        // ====================================
        // 非以下兩種狀況時，排除
        // 1.『轉換前單位』不是手動定義
        // 2.『轉換後單位』已經被包含在可使用單位
        if (!manualSettingDepSids.contains(beforeOrgSid)
                && allCanUseDepSids.contains(afterOrgSid)) {
            return null;
        }

        // ====================================
        // 資料命中，組VO
        // ====================================

        // 僅計算筆數用，不組說明文字
        if (isOnlyType) {
            return new Setting10DepSettingCheckItemVO(settingType);
        }

        // 將設定值收束為最上層
        List<Integer> topDepSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(settingDepSids));

        // 往上一直找到符合設定值的父單位
        Integer topParentSid = beforeOrgSid;
        Org currOrg = WkOrgCache.getInstance().findBySid(beforeOrgSid);
        while (currOrg != null) {
            if (topDepSids.contains(currOrg.getSid())) {
                topParentSid = currOrg.getSid();
                break;
            }

            if (currOrg.getParent() != null) {
                currOrg = WkOrgCache.getInstance().findBySid(currOrg.getParent().getSid());
            } else {
                currOrg = null;
            }
        }

        String descr = WkOrgUtils.findNameBySid(topParentSid);
        // 停用單位加上刪除線
        if (!WkOrgUtils.isActive(topParentSid)) {
            descr = WkHtmlUtils.addStrikethroughStyle(WkOrgUtils.findNameBySid(topParentSid), "停用");
        }

        if (!topDepSids.contains(beforeOrgSid)) {
            descr += "【<span class='WS1-1-3'>含以下</span>】";
        }

        return new Setting10DepSettingCheckItemVO(
                DepSettingCheckSettingType.CUSTOMIZE,
                descr);
    }

    /**
     * 快取名稱
     */
    private static final String prepareTempletDepSidsCacheName = Setting10DepSettingCheckLogic.class.getSimpleName() + "_prepareTempletDepSidsCacheName";

    /**
     * 快取逾時時間 (5秒)
     */
    private static final long prepareTempletDepSidsCachOverdueMillisecond = 5 * 1000;

    /**
     * 運算模版設定部門
     * 
     * @param templet      模版資料
     * @param loginCompSid 登入公司別 sid
     * @return
     */
    private List<Integer> prepareTempletDepSids(
            WCConfigTemplet templet,
            Integer loginCompSid) {

        // ====================================
        // 快取 key
        // ====================================
        List<String> cacheKeys = Lists.newArrayList(
                templet.getSid() + "",
                loginCompSid + "");

        // ====================================
        // 由快取取得
        // ====================================
        List<Integer> depSids = this.wkCommonCache.getCache(
                prepareTempletDepSidsCacheName,
                cacheKeys,
                prepareTempletDepSidsCachOverdueMillisecond);

        if (depSids != null) {
            return depSids;
        }

        // ====================================
        // 若不存在時，進行運算
        // ====================================
        // 取得模版設定部門
        depSids = this.wcConfigTempletManager.prepareTempletDepSids(
                templet, loginCompSid, true);

        // 塞快取
        this.wkCommonCache.putCache(
                prepareTempletDepSidsCacheName,
                cacheKeys,
                depSids);

        return depSids;

    }

    /**
     * 準備筆數資訊
     * 
     * @param orgTransMappingVOs
     * @throws UserMessageException
     */
    public void prepareCountInfo(List<SettingOrgTransMappingVO> orgTransMappingVOs) throws UserMessageException {

        if (WkStringUtils.isEmpty(orgTransMappingVOs)) {
            return;
        }

        // ====================================
        // 公司 sid
        // ====================================
        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
        if (comp == null) {
            throw new UserMessageException(WkMessage.SESSION_TIMEOUT, InfomationLevel.WARN);
        }

        // ====================================
        // 取得所有部門設定模版
        // ====================================
        // 查詢
        List<WCConfigTemplet> wcConfigTemplets = this.wcConfigTempletManager.findAll();
        // 索引
        Map<Long, WCConfigTemplet> configTempletMapBySid = wcConfigTemplets.stream()
                .collect(Collectors.toMap(
                        WCConfigTemplet::getSid, each -> each));

        // ====================================
        // 所有大類
        // ====================================
        List<WCCategoryTag> categoryTags = this.wCCategoryTagCache.findAllAndSortBySeq(
                SecurityFacade.getCompanyId());

        // ====================================
        // 逐筆處理
        // ====================================
        List<Setting10DepSettingCheckItemVO> checkItems = Lists.newArrayList();

        for (SettingOrgTransMappingVO settingOrgTransMappingVO : orgTransMappingVOs) {

            // ====================================
            // 中類-可使用單位
            // ====================================
            // 取得相關資料 (有被設定的)
            checkItems = this.findCanUseDepBySecondLevel(
                    comp.getSid(),
                    settingOrgTransMappingVO.getBeforeOrgSid(),
                    settingOrgTransMappingVO.getAfterOrgSid(),
                    categoryTags,
                    configTempletMapBySid,
                    true);

            // 兜組顯示資訊
            settingOrgTransMappingVO.getDataMap().put(
                    DepSettingCheckItemType.MENUTAG_CAN_USE_DEP.name(),
                    this.prepareItmeCountShowInfo(checkItems));

            // ====================================
            // 中類-可使用單位
            // ====================================
            // 取得相關資料 (有被設定的)
            checkItems = this.findCanUseDepBythirdLevel(
                    comp.getSid(),
                    settingOrgTransMappingVO.getBeforeOrgSid(),
                    settingOrgTransMappingVO.getAfterOrgSid(),
                    categoryTags,
                    configTempletMapBySid,
                    true);

            // 兜組顯示資訊
            settingOrgTransMappingVO.getDataMap().put(
                    DepSettingCheckItemType.ITEM_USE_DEP.name(),
                    this.prepareItmeCountShowInfo(checkItems));

            // ====================================
            // 中類-可使用單位
            // ====================================
            // 取得相關資料 (有被設定的)
            checkItems = this.findCanUseDepBythirdLevel(
                    comp.getSid(),
                    settingOrgTransMappingVO.getBeforeOrgSid(),
                    settingOrgTransMappingVO.getAfterOrgSid(),
                    categoryTags,
                    configTempletMapBySid,
                    true);

            // 兜組顯示資訊
            settingOrgTransMappingVO.getDataMap().put(
                    DepSettingCheckItemType.ITEM_USE_DEP.name(),
                    this.prepareItmeCountShowInfo(checkItems));

            // ====================================
            // 中類-預設可閱單位
            // ====================================
            // 取得相關資料 (有被設定的)
            checkItems = this.findCanViewDepBythirdLevel(
                    comp.getSid(),
                    settingOrgTransMappingVO.getBeforeOrgSid(),
                    settingOrgTransMappingVO.getAfterOrgSid(),
                    categoryTags,
                    configTempletMapBySid,
                    true);

            // 兜組顯示資訊
            settingOrgTransMappingVO.getDataMap().put(
                    DepSettingCheckItemType.ITEM_CAN_VIEW_DEP.name(),
                    this.prepareItmeCountShowInfo(checkItems));

            // ====================================
            // 執行單位
            // ====================================
            checkItems = this.findExecDep(
                    settingOrgTransMappingVO.getBeforeOrgSid(),
                    categoryTags,
                    true);

            // 兜組顯示資訊
            settingOrgTransMappingVO.getDataMap().put(
                    DepSettingCheckItemType.EXEC_DEP.name(),
                    this.prepareItmeCountShowInfo(checkItems));

        }

    }

    private String prepareItmeCountShowInfo(
            List<Setting10DepSettingCheckItemVO> checkItems) {

        // 計算『自行設定』筆數
        Long countCustom = checkItems.stream()
                .filter(checkItem -> DepSettingCheckSettingType.CUSTOMIZE.equals(checkItem.getSettingType()))
                .count();

        // 計算『模版設定』筆數
        Long countTemplete = checkItems.stream()
                .filter(checkItem -> DepSettingCheckSettingType.TEMPLETE.equals(checkItem.getSettingType()))
                .count();

        // 計算『未設定』筆數
        Long countEmpty = checkItems.stream()
                .filter(checkItem -> DepSettingCheckSettingType.EMPTY.equals(checkItem.getSettingType()))
                .count();

        // 計算『被設定』筆數 (執行單位用)
        Long countNone = checkItems.stream()
                .filter(checkItem -> DepSettingCheckSettingType.NONE.equals(checkItem.getSettingType()))
                .count();

        String resultContent = "";

        if (countCustom != null && countCustom > 0) {
            resultContent += ""
                    + "<div style='display:flex; align-self:stretch' class='WS1-1-2b'>"
                    + "自行：" + Integer.parseInt(countCustom + "")
                    + "</div>";
        }

        if (countTemplete != null && countTemplete > 0) {
            resultContent += ""
                    + "<div style='display:flex; align-self:stretch' class='WS1-1-3b'>"
                    + "模版：" + Integer.parseInt(countTemplete + "")
                    + "</div>";

        }

        if (countEmpty != null && countEmpty > 0) {
            resultContent += ""
                    + "<div style='display:flex; align-self:stretch' class='WS1-1-4b'>"
                    + "全單位：" + Integer.parseInt(countEmpty + "")
                    + "</div>";
        }

        if (countNone != null && countNone > 0) {
            resultContent += ""
                    + "<div style='display:flex; align-self:stretch' class='WS1-1-2b'>"
                    + "被設定：" + Integer.parseInt(countNone + "")
                    + "</div>";
        }

        return resultContent;
    }

}
