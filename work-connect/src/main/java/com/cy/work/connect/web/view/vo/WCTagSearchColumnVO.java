/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class WCTagSearchColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 574208076758912362L;

    @Setter
    @Getter
    private ColumnDetailVO index;
    @Setter
    @Getter
    private ColumnDetailVO createTime;
    @Setter
    @Getter
    private ColumnDetailVO createUser;
    @Setter
    @Getter
    private ColumnDetailVO categoryName;

    @Setter
    @Getter
    private ColumnDetailVO tag;

    @Setter
    @Getter
    private ColumnDetailVO editMenu;

    @Setter
    @Getter
    private ColumnDetailVO status;
    @Setter
    @Getter
    private ColumnDetailVO modifyTime;
    @Setter
    @Getter
    private ColumnDetailVO seq;
    @Setter
    @Getter
    private String pageCount = "50";
}
