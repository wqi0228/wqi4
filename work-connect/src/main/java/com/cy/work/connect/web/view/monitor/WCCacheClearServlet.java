/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.monitor;

import com.cy.work.connect.logic.helper.ClearCacheHelper;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * @author brain0925_liao
 */
@Slf4j
@WebServlet(name = "/cache/clear", urlPatterns = "/cache/clear")
public class WCCacheClearServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -7209234242229244020L;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(
            this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        try {
            log.debug("receive clear cache signal.");
            ClearCacheHelper.process();
            log.debug("receive clear cache signal done.");
            response.getWriter().write("ok");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
