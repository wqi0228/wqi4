/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.logic.vo.ExecReplyTo;
import com.cy.work.connect.web.listener.ExecReplyCallBack;
import com.cy.work.connect.web.logic.components.WCExecReplyLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.to.EditExecReplyTo;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 執行回覆
 *
 * @author kasim
 */
@Slf4j
public class ExecReplyComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9093185733052511664L;

    private final WCExecReplyLogicComponent execReplyLogicComponent;
    private final DisplayController displayController;

    private final ExecReplyCallBack execReplyCallBack;

    /**
     * 登入者
     */
    private final Integer userSid;
    /**
     * 登入單位
     */
    private final Integer depSid;
    /**
     * 聯絡單sid
     */
    private String wcSid;

    @Getter
    /** 是否顯示頁籤 */
    private boolean showTab = false;

    @Getter
    /** 列表資料 - 執行回覆 */
    private List<ExecReplyTo> execReplyTos;

    @Getter
    /** 編輯模式 */
    private boolean editMode = false;

    @Getter
    @Setter
    /** 視窗 - 操作物件 */
    private EditExecReplyTo editTo = new EditExecReplyTo();
    /**
     * [wc_exec_dep].[wc_exec_dep_sid]
     */
    private List<String> exceDepSid;

    public ExecReplyComponent(Integer depSid, Integer userSid,
            ExecReplyCallBack execReplyCallBack) {
        this.execReplyLogicComponent = WCExecReplyLogicComponent.getInstance();
        this.displayController = DisplayController.getInstance();
        this.execReplyCallBack = execReplyCallBack;
        this.depSid = depSid;
        this.userSid = userSid;
    }

    /**
     * 讀取資料
     *
     * @param sid
     * @param wcNo
     */
    public void loadData(String sid) {
        this.wcSid = sid;
        this.loadData();
    }

    /**
     * 讀取資料
     */
    public void loadData() {
        this.execReplyTos = execReplyLogicComponent.findExecReplyToByWcSid(userSid, wcSid);
        this.showTab = execReplyTos != null && !execReplyTos.isEmpty();
    }

    /**
     * 開啟執行回覆(新增)
     */
    public void loadExecReplyByNew() {
        try {
            this.editMode = false;
            this.editTo = execReplyLogicComponent.createTo(depSid, userSid);
            this.exceDepSid = execReplyLogicComponent.getExecReplyExecDeps(wcSid, userSid);
            if (exceDepSid.isEmpty()) {
                log.warn("無執行單位選項資料，請確認!! wcSid：" + wcSid + "，userSid：" + userSid + "。");
                execReplyCallBack.showMessage("無執行單位選項資料，請確認!!");
                return;
            }
            displayController.update("dlgExecReply_view_id");
            displayController.showPfWidgetVar("dlgExecReply_wv");
        } catch (Exception e) {
            log.warn("開啟 執行回覆失敗!!", e);
            execReplyCallBack.showMessage("開啟 執行回覆失敗!!");
        }
    }
}
