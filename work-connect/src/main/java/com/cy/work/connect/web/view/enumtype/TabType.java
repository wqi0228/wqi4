/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.enumtype;

/**
 * @author brain0925_liao
 */
public enum TabType {
    /**
     * 追蹤
     */
    Trace,
    /**
     * 補充說明
     */
    Reply,
    /**
     * 附件
     */
    Attachment,
    /**
     * 閱讀記錄
     */
    ReadReceipt,
    /**
     * 簽核進度
     */
    BpmFlow,
    /**
     * 執行回覆
     */
    ExecReply,
    /**
     * 執行資訊
     */
    ExecInfo,
    /**
     * 可閱人員
     */
    ViewPerson
}
