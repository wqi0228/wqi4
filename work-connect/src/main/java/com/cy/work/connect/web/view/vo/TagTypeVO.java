package com.cy.work.connect.web.view.vo;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jimmy_chou
 */
public class TagTypeVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4541519087878547866L;

    @Getter
    private String tagType;

    @Getter
    private List<SelectItem> categoryItems;

    @Getter
    @Setter
    private List<String> selCategoryItems;

    @Getter
    @Setter
    private Boolean checkAll;

    public TagTypeVO() {
    }

    /**
     * 建構式 (預設無勾選)
     *
     * @param tagType
     * @param categoryItems
     * @param sids
     */
    public TagTypeVO(String tagType, List<SelectItem> categoryItems, List<String> sids) {
        this.tagType = tagType;
        this.categoryItems = categoryItems;
        if (sids == null) {
            this.selCategoryItems = Lists.newArrayList();
        } else {
            this.selCategoryItems =
                categoryItems.stream()
                    .filter(each -> sids.contains((String) each.getValue()))
                    .map(each -> String.valueOf(each.getValue()))
                    .collect(Collectors.toList());
        }
        this.checkAll = this.categoryItems.size() == this.selCategoryItems.size();
    }

    /**
     * 建構式 (預設有勾選)
     *
     * @param tagType
     * @param categoryItems
     * @param checkAll
     */
    public TagTypeVO(String tagType, List<SelectItem> categoryItems, Boolean checkAll) {
        this.tagType = tagType;
        this.categoryItems = categoryItems;
        this.selCategoryItems =
            categoryItems.stream()
                .map(each -> String.valueOf(each.getValue()))
                .collect(Collectors.toList());
        this.checkAll = checkAll;
    }

    /**
     * Tag分類是否全選
     */
    public void changeCheckBox() {
        if (this.checkAll) {
            this.selCategoryItems.removeAll(
                this.categoryItems.stream()
                    .map(each -> String.valueOf(each.getValue()))
                    .collect(Collectors.toList()));
            this.selCategoryItems.addAll(
                this.categoryItems.stream()
                    .map(each -> String.valueOf(each.getValue()))
                    .collect(Collectors.toList()));
            for (SelectItem item : this.categoryItems) {
                item.setEscape(true);
            }
        } else {
            this.selCategoryItems.removeAll(
                this.categoryItems.stream()
                    .map(each -> String.valueOf(each.getValue()))
                    .collect(Collectors.toList()));
            for (SelectItem item : this.categoryItems) {
                item.setEscape(false);
            }
        }
    }

    /**
     * Tag分類項目異動
     */
    public void changeItems() {
        this.checkAll = this.selCategoryItems.size() == this.categoryItems.size();
    }
}
