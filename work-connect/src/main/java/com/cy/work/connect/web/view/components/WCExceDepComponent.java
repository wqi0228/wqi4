/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.helper.ExecDepLogic;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.logic.components.WCExceDepLogicComponent;
import com.cy.work.connect.web.util.pf.DataTablePickList;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.enumtype.TabType;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WCExceDepComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7214730020929440158L;
    private final Integer loginUserSid;
    private final TabCallBack workReportTabCallBack;
    private List<OrgViewVo> allDeps;
    private Map<Integer, Integer> orgSortNumberMapByDepSid = WkOrgCache.getInstance().findOrgSortNumberMapByDepSid();
    /**
     * DataTablePickList 物件
     */
    @Getter
    @Setter
    private DataTablePickList<OrgViewVo> dataPickList;
    private String wc_ID;
    @Getter
    @Setter
    private String filterName;
    @Getter
    @Setter
    private boolean searchParentDep = false;

    public WCExceDepComponent(
            Integer loginUserSid, TabCallBack workReportTabCallBack) {
        this.dataPickList = new DataTablePickList<>(OrgViewVo.class);
        this.loginUserSid = loginUserSid;
        this.workReportTabCallBack = workReportTabCallBack;
        this.allDeps = Lists.newArrayList();
    }

    public void loadData(String wc_ID) {
        this.wc_ID = wc_ID;

        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        Org companyOrg = WkOrgCache.getInstance()
                .findBySid(loginUser.getPrimaryOrg().getCompanySid());

        List<Org> allDepOrgs = WkOrgCache.getInstance().findAllDepByCompSid(companyOrg.getSid());

        this.allDeps = WkCommonUtils.safeStream(allDepOrgs)
                .filter(org -> WkOrgUtils.isActive(org))
                .map(org -> this.transToOrgViewVo(org))
                .sorted(Comparator.comparing(org -> this.orgSortNumberMapByDepSid.get(org.getSid())))
                .collect(Collectors.toList());

    }

    private OrgViewVo transToOrgViewVo(Org org) {
        return new OrgViewVo(
                org.getSid(),
                org.getName(),
                WkOrgUtils.prepareBreadcrumbsByDepName(org.getSid(), OrgLevel.DIVISION_LEVEL, true, "-"),
                org.getCompany().getName());
    }

    public void openCustomDepDialog() {

        this.filterName = "";
        this.searchParentDep = false;

        try {

            List<OrgViewVo> target = WkCommonUtils.safeStream(WCExceDepLogicComponent.getInstance().getCustomerAddExceDeps(wc_ID))
                    .map(org -> this.transToOrgViewVo(org))
                    .collect(Collectors.toList());
            target = WkCommonUtils.safeStream(target)
                    .sorted(Comparator.comparing(org -> orgSortNumberMapByDepSid.get(org.getSid())))
                    .collect(Collectors.toList());

            dataPickList.setTargetData(target);

            dataPickList.setSourceData(removeTargetView(this.allDeps, target));

            DisplayController.getInstance().update("excedep");
            DisplayController.getInstance().showPfWidgetVar("excedeptUnitDlg");
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void doFliterName() {
        if (Strings.isNullOrEmpty(filterName)) {
            dataPickList.setSourceData(removeTargetView(allDeps, dataPickList.getTargetData()));
        } else {
            if (searchParentDep) {
                dataPickList.setSourceData(
                        removeTargetView(
                                allDeps.stream()
                                        .filter(each -> each.getName().toLowerCase()
                                                .contains(filterName.toLowerCase()))
                                        .collect(Collectors.toList()),
                                dataPickList.getTargetData()));
            } else {
                dataPickList.setSourceData(
                        removeTargetView(
                                allDeps.stream()
                                        .filter(
                                                each -> each.getDepName().toLowerCase()
                                                        .contains(filterName.toLowerCase()))
                                        .collect(Collectors.toList()),
                                dataPickList.getTargetData()));
            }
        }
        dataPickList.setSourceData(
                WkCommonUtils.safeStream(dataPickList.getSourceData())
                        .sorted(Comparator.comparing(org -> orgSortNumberMapByDepSid.get(org.getSid())))
                        .collect(Collectors.toList()));

    }

    private List<OrgViewVo> removeTargetView(List<OrgViewVo> source, List<OrgViewVo> target) {
        List<OrgViewVo> filterSource = Lists.newArrayList();
        source.forEach(
                item -> {
                    if (!target.contains(item)) {
                        filterSource.add(item);
                    }
                });
        return filterSource;
    }

    public void doSelectOrg() {
        try {
            Set<Integer> selectedDepSids = WkCommonUtils.safeStream(this.dataPickList.getTargetData())
                    .map(item -> item.getSid())
                    .collect(Collectors.toSet());

            ExecDepLogic.getInstance().saveCustomerExceDeps(wc_ID, selectedDepSids, loginUserSid);

            workReportTabCallBack.reloadWCMasterData();
            workReportTabCallBack.reloadBpmTab();
            workReportTabCallBack.reloadTraceTab();
            workReportTabCallBack.reloadTabView(TabType.Trace);

            DisplayController.getInstance().hidePfWidgetVar("excedeptUnitDlg");
            DisplayController.getInstance().execute("doReloadDataToUpdate('" + wc_ID + "')");

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
            return;
        }

    }
}
