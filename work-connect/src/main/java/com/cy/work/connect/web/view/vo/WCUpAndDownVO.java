/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
@SuppressWarnings("rawtypes")
public abstract class WCUpAndDownVO<T extends WCUpAndDownVO> {

    /**
     * 報表主要key
     */
    @Setter
    @Getter
    private String upAndDownVO_key;
    /**
     * 主檔key-
     */
    @Setter
    @Getter
    private String wc_Key;

    public WCUpAndDownVO() {
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.upAndDownVO_key);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WCUpAndDownVO other = (WCUpAndDownVO) obj;
        return Objects.equals(this.upAndDownVO_key,
            other.upAndDownVO_key);
    }

    public abstract void replaceValue(T obj);
}
