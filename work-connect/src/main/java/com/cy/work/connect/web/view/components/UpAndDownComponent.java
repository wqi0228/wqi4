/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.web.view.vo.WCUpAndDownVO;
import java.io.Serializable;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@SuppressWarnings("rawtypes")
@Component
public class UpAndDownComponent<T extends WCUpAndDownVO> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1792563739696736115L;

    //    public void btnOpenUrl(List<T> datas, T selData, TableUpDownBean tableUpDownBean, String
    // upAndDownVO_key) {
    //        settintSelFavoriteSearchVO(datas, selData, upAndDownVO_key);
    //        settingPreviousAndNext(datas, selData, tableUpDownBean);
    //        tableUpDownBean.setWorp_path("worp_full");
    //    }
    //
    //    private void settintSelFavoriteSearchVO(List<T> datas, T selData, String upAndDownVO_key) {
    //        List<T> filterTemp = datas.stream()
    //                .filter(each -> each.getUpAndDownVO_key().equals(upAndDownVO_key))
    //                .collect(Collectors.toList());
    //
    //        int index = datas.indexOf(filterTemp.get(0));
    //        if (index > 0) {
    //            selData = datas.get(index);
    //        } else {
    //            selData = datas.get(0);
    //        }
    //    }
    //
    //    public void doUp(List<T> datas, T selData, TableUpDownBean tableUpDownBean, List<T>
    // tempWRUpAndDownVOs, String tempSelSid) {
    //        try {
    //            if (!datas.isEmpty()) {
    //                toUp(datas, selData, tempWRUpAndDownVOs, tempSelSid);
    //                settingPreviousAndNext(datas, selData, tableUpDownBean);
    //                tableUpDownBean.setSession_now_sid(selData.getWr_Key());
    //            }
    //        } catch (Exception e) {
    //            log.error("doUp", e);
    //        } finally {
    //            tableUpDownBean.setSessionSettingOver();
    //        }
    //    }
    //
    //    public void doDown(List<T> datas, T selData, TableUpDownBean tableUpDownBean, List<T>
    // tempWRUpAndDownVOs, String tempSelSid) {
    //        try {
    //            if (!datas.isEmpty()) {
    //                toDown(datas, selData, tempWRUpAndDownVOs, tempSelSid);
    //                settingPreviousAndNext(datas, selData, tableUpDownBean);
    //                tableUpDownBean.setSession_now_sid(selData.getWr_Key());
    //            }
    //        } catch (Exception e) {
    //            log.error("doUp", e);
    //        } finally {
    //            tableUpDownBean.setSessionSettingOver();
    //        }
    //    }
    //
    //    public void updateListByWR_Key(List<T> datas, T selData, List<T> resultList, String wr_Key,
    // List<T> tempWRUpAndDownVOs, String tempSelSid) {
    //        if (resultList == null || resultList.isEmpty()) {
    //            tempWRUpAndDownVOs = Lists.newArrayList();
    //            if (datas != null && !datas.isEmpty()) {
    //                datas.forEach(item -> {
    //                    tempWRUpAndDownVOs.add(item);
    //                });
    //                if (selData != null) {
    //                    tempSelSid = selData.getUpAndDownVO_key();
    //                }
    //                List<T> sel = datas.stream()
    //                        .filter(each -> each.getWr_Key().equals(wr_Key))
    //                        .collect(Collectors.toList());
    //                if (sel != null && !sel.isEmpty()) {
    //                    sel.forEach(item -> {
    //                        datas.remove(item);
    //                    });
    //                }
    //            }
    //        } else {
    //            resultList.forEach(newItem -> {
    //                int index = datas.indexOf(newItem);
    //                if (index >= 0) {
    //                    datas.get(index).replaceValue(newItem);
    //                } else {
    //                    datas.add(newItem);
    //                }
    //                if (tempWRUpAndDownVOs != null && !tempWRUpAndDownVOs.isEmpty()) {
    //                    int indext = tempWRUpAndDownVOs.indexOf(newItem);
    //                    if (indext >= 0) {
    //                        tempWRUpAndDownVOs.get(indext).replaceValue(newItem);
    //                    } else {
    //                        tempWRUpAndDownVOs.add(newItem);
    //                    }
    //                }
    //            });
    //        }
    //    }
    //
    //    private void settingPreviousAndNext(List<T> datas, T selData, TableUpDownBean
    // tableUpDownBean) {
    //        int index = datas.indexOf(selData);
    //        if (index <= 0) {
    //            tableUpDownBean.setSession_previous_sid("");
    //        } else {
    //            tableUpDownBean.setSession_previous_sid(datas.get(index - 1).getWr_Key());
    //        }
    //        if (index == datas.size() - 1) {
    //            tableUpDownBean.setSession_next_sid("");
    //        } else {
    //            tableUpDownBean.setSession_next_sid(datas.get(index + 1).getWr_Key());
    //        }
    //    }
    //
    //    private void toDown(List<T> datas, T selData,List<T> tempWRUpAndDownVOs,String tempSelSid) {
    //        try {
    //            if (tempWRUpAndDownVOs != null && !tempWRUpAndDownVOs.isEmpty() &&
    // !Strings.isNullOrEmpty(tempSelSid)) {
    //                List<T> filterTemp = tempWRUpAndDownVOs.stream()
    //                        .filter(each -> each.getUpAndDownVO_key().equals(tempSelSid))
    //                        .collect(Collectors.toList());
    //                int index = tempWRUpAndDownVOs.indexOf(filterTemp.get(0));
    //                if (index > 0) {
    //                    selData = tempWRUpAndDownVOs.get(index + 1);
    //                } else {
    //                    selData = datas.get(datas.size() - 1);
    //                }
    //                tempWRUpAndDownVOs = null;
    //                tempSelSid = "";
    //            } else {
    //                int index = datas.indexOf(selData);
    //                if (index >= 0) {
    //                    selData = datas.get(index + 1);
    //                } else {
    //                    selData = datas.get(datas.size() - 1);
    //                }
    //            }
    //        } catch (Exception e) {
    //            log.error("toDown", e);
    //            tempWRUpAndDownVOs = null;
    //            tempSelSid = "";
    //            if (datas.size() > 0) {
    //                selData = datas.get(datas.size() - 1);
    //            }
    //        }
    //    }
    //
    //    private void toUp(List<T> datas, T selData, List<T> tempWRUpAndDownVOs, String tempSelSid) {
    //        try {
    //            if (tempWRUpAndDownVOs != null && !tempWRUpAndDownVOs.isEmpty() &&
    // !Strings.isNullOrEmpty(tempSelSid)) {
    //                List<T> filterTemp = tempWRUpAndDownVOs.stream()
    //                        .filter(each -> each.getUpAndDownVO_key().equals(tempSelSid))
    //                        .collect(Collectors.toList());
    //                int index = tempWRUpAndDownVOs.indexOf(filterTemp.get(0));
    //                if (index > 0) {
    //                    selData = tempWRUpAndDownVOs.get(index - 1);
    //                } else {
    //                    selData = datas.get(0);
    //                }
    //                tempWRUpAndDownVOs = null;
    //                tempSelSid = "";
    //            } else {
    //                int index = datas.indexOf(selData);
    //                if (index > 0) {
    //                    selData = datas.get(index - 1);
    //                } else {
    //                    selData = datas.get(0);
    //                }
    //            }
    //        } catch (Exception e) {
    //            log.error("toUp", e);
    //            tempSelSid = "";
    //            tempWRUpAndDownVOs = null;
    //            if (!datas.isEmpty()) {
    //                selData = datas.get(0);
    //            }
    //        }
    //    }

}
