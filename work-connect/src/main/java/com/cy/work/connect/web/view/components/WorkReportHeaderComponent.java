/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.view.vo.ExecInfoVO;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author brain0925_liao
 */
@NoArgsConstructor
@Data
public class WorkReportHeaderComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1559864552876493084L;
    /**
     * 部門
     */
    private String departmentName;
    /**
     * 建立人員
     */
    private String createUserName;
    /**
     * 建立日期
     */
    private String createTime;
    // System.err.println("***!@#$1234");
    private String createTimeTitle = "建立日期";
    /**
     * 標籤ID
     */
    private String tagID;
    /**
     * 單號
     */
    private String workProjectNo;
    /**
     * 單據SID
     */
    private String workProjectSID;
    /**
     * 狀態
     */
    private String status;
    /**
     * 是否顯示回覆已讀取按鈕
     */
    private boolean showReadReceiptBtn = false;
    /**
     * 是否可點選回覆已讀取按鈕
     */
    private boolean readReceiptBtnAble = false;
    /**
     * 是否可顯示執行資訊
     */
    private boolean showExecStatus = false;
    /**
     * 執行資訊List
     */
    private List<ExecInfoVO> execInfoVOs;

    public WorkReportHeaderComponent(String departmentName, String createUserName) {
        this.departmentName = departmentName;
        this.createUserName = createUserName;
        this.createTime =
            ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), new Date());
    }
}
