/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import java.io.Serializable;

/**
 * BPM 按鈕監聽CallBack
 *
 * @author kasim
 */
public class BpmtCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1475029556276552838L;

    public void showMessage(String message) {
    }

    public void reloadBPMTab() {
    }

    public void reloadExecBPMTab() {
    }
}
