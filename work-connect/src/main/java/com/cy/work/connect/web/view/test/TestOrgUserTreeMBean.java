package com.cy.work.connect.web.view.test;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.web.view.components.qstree.OrgUserTreeMBean;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
public class TestOrgUserTreeMBean {

    /**
     * 使用者資料快取
     */
    @Autowired
    private WkUserCache wkUserCache;

    /**
     *
     */
    @Getter
    @Setter
    private OrgUserTreeMBean orgUserTree = new OrgUserTreeMBean();

    @Getter
    private List<User> selectUsers;
    /**
     * 實做供元件呼叫取得已選清單的方法, 會將此份名單帶入右邊的【已選擇人員】清單
     */
    Function<Map<String, Object>, List<?>> getSelectedDataList =
        (condition) -> {

            // 將現行已選擇的 user 轉為元件需要的資料型態 (sid list)
            if (WkStringUtils.notEmpty(this.selectUsers)) {
                return this.selectUsers.stream().map(o -> o.getSid()).collect(Collectors.toList());
            }

            return Lists.newArrayList();
        };

    /**
     *
     */
    @PostConstruct
    public void init() {
        log.info("TestOrgUserTreeMBean @PostConstruct");

        selectUsers = Lists.newArrayList();
    }

    /**
     * 點選開啟組織人員選擇樹
     */
    public void openOrgUserTree() {
        // log.info("openOrgUserTree");

        Map<String, Object> condition = Maps.newHashMap();
        condition.put("category_sid", Long.parseLong("1"));

        this.orgUserTree = new OrgUserTreeMBean();

        this.orgUserTree.init(
            Optional.ofNullable(WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId()))
                .map(Org::getSid)
                .orElseGet(() -> null),
            SecurityFacade.getPrimaryOrgSid(),
            getSelectedDataList,
            condition);
    }

    /**
     * 更新畫面的已選擇人員清單
     */
    public void reNewSelectedUsers() {

        // 取得元件中選取的 user 清單
        List<Integer> userSids = this.orgUserTree.getSelectedDataList();

        if (WkStringUtils.isEmpty(userSids)) {
            this.selectUsers = Lists.newArrayList();
            return;
        }

        this.selectUsers =
            userSids.stream()
                .map(userSid -> wkUserCache.findBySid(userSid))
                .collect(Collectors.toList());
    }
}
