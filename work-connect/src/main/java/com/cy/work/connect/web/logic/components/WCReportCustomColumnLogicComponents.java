/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.work.connect.logic.manager.WCReportCustomColumnManager;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.CustomColumnDetailTo;
import com.cy.work.connect.vo.converter.to.WCReportCustomColumnInfo;
import com.cy.work.connect.vo.converter.to.WCReportCustomColumnInfoTo;
import com.cy.work.connect.vo.enums.Column;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCTagSearchColumn;
import com.cy.work.connect.vo.enums.WCWorkReportSearchColumn;
import com.cy.work.connect.web.view.vo.CustomCulumnVO;
import com.cy.work.connect.web.view.vo.WCTagSearchColumnVO;
import com.cy.work.connect.web.view.vo.WCWorkReportSearchColumnVO;
import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 自訂欄位邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCReportCustomColumnLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7590784798353771712L;

    private static WCReportCustomColumnLogicComponents instance;
    @Autowired
    private WCReportCustomColumnManager wkReportCustomColumnManager;

    public static WCReportCustomColumnLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCReportCustomColumnLogicComponents.instance = this;
    }

    /**
     * 設定顯示欄位
     *
     * @param wcReportCustomColumnInfo    資料庫蘭位資料
     * @param wcColumns                   該報表欄位資訊
     * @param selectCustomCulumns         挑選顯示自訂欄位
     * @param wcReportCustomColumnUrlType 報表類型
     * @return
     */
    private List<WCReportCustomColumnInfoTo> settingShowColumn(
        WCReportCustomColumnInfo wcReportCustomColumnInfo,
        List<Column> wcColumns,
        List<CustomCulumnVO> selectCustomCulumns,
        WCReportCustomColumnUrlType wcReportCustomColumnUrlType) {
        List<WCReportCustomColumnInfoTo> wkReportCustomColumnInfoTos = Lists.newArrayList();
        if (wcReportCustomColumnInfo != null
            && wcReportCustomColumnInfo.getWkReportCustomColumnInfoTos() != null) {
            wcReportCustomColumnInfo
                .getWkReportCustomColumnInfoTos()
                .forEach(
                    item -> {
                        // 需檢測DB欄位是否與目前程式欄位一致
                        List<Column> column =
                            wcColumns.stream()
                                .filter(each -> each.getVal().equals(item.getName()))
                                .collect(Collectors.toList());
                        if (column == null || column.size() == 0) {
                            log.warn(
                                "問題欄位 : WkReportCustomColumnUrlType - "
                                    + wcReportCustomColumnUrlType.getVal()
                                    + " ColumnName"
                                    + item.getName());
                            return;
                        }

                        Column itemWCColumn = column.get(0);
                        List<CustomCulumnVO> selCutom =
                            selectCustomCulumns.stream()
                                .filter(each -> each.getName().equals(itemWCColumn.getVal()))
                                .collect(Collectors.toList());
                        // 檢測該次挑選的欄位,是否為可移除欄位,若不可移除,依舊需加入
                        if (selCutom == null || selCutom.size() == 0) {
                            if (!itemWCColumn.getCanSelectItem()) {
                                wkReportCustomColumnInfoTos.add(item);
                            }
                        } else {
                            wkReportCustomColumnInfoTos.add(item);
                        }
                    });
        }
        wcColumns.forEach(
            item -> {
                List<WCReportCustomColumnInfoTo> exists =
                    wkReportCustomColumnInfoTos.stream()
                        .filter(each -> each.getName().equals(item.getVal()))
                        .collect(Collectors.toList());
                if (exists != null && exists.size() > 0) {
                    return;
                }
                if (!item.getCanSelectItem()) {
                    WCReportCustomColumnInfoTo wt = new WCReportCustomColumnInfoTo();
                    wt.setName(item.getVal());
                    wt.setWidth(item.getDefaultWidth());
                    wkReportCustomColumnInfoTos.add(wt);
                } else {
                    List<CustomCulumnVO> selCutom =
                        selectCustomCulumns.stream()
                            .filter(each -> each.getName().equals(item.getVal()))
                            .collect(Collectors.toList());
                    if (selCutom != null && selCutom.size() > 0) {
                        WCReportCustomColumnInfoTo wt = new WCReportCustomColumnInfoTo();
                        wt.setName(item.getVal());
                        wt.setWidth(item.getDefaultWidth());
                        wkReportCustomColumnInfoTos.add(wt);
                    }
                }
            });
        return wkReportCustomColumnInfoTos;
    }

    /**
     * 設定欄位寬度
     *
     * @param wcReportCustomColumnInfo    資料庫蘭位資料
     * @param wcColumns                   該報表欄位資訊
     * @param customColumnDetailTo        Li
     * @param wcReportCustomColumnUrlType 報表類型
     * @return
     */
    private List<WCReportCustomColumnInfoTo> settingWidthColumn(
        WCReportCustomColumnInfo wcReportCustomColumnInfo,
        List<Column> wcColumns,
        List<CustomColumnDetailTo> customColumnDetailTo,
        WCReportCustomColumnUrlType wcReportCustomColumnUrlType) {
        List<WCReportCustomColumnInfoTo> wcReportCustomColumnInfoTos = Lists.newArrayList();
        // 檢測欄位是否有變更過,若有變更過,僅變更存在的欄位寬度
        if (wcReportCustomColumnInfo != null
            && wcReportCustomColumnInfo.getWkReportCustomColumnInfoTos() != null) {
            wcReportCustomColumnInfo
                .getWkReportCustomColumnInfoTos()
                .forEach(
                    item -> {
                        List<Column> column =
                            wcColumns.stream()
                                .filter(each -> each.getVal().equals(item.getName()))
                                .collect(Collectors.toList());
                        if (column == null || column.size() == 0) {
                            log.warn(
                                "問題欄位 : WCReportCustomColumnUrlType - "
                                    + wcReportCustomColumnUrlType.getVal()
                                    + " ColumnName"
                                    + item.getName());
                            return;
                        }
                        Column itemWCColumn = column.get(0);
                        List<CustomColumnDetailTo> selCutom =
                            customColumnDetailTo.stream()
                                .filter(each -> each.getName().equals(itemWCColumn.getVal()))
                                .collect(Collectors.toList());
                        // 若介面與DB無找到符合,不進行變動存進DB
                        if (selCutom == null || selCutom.size() == 0) {
                            wcReportCustomColumnInfoTos.add(item);
                            // 若發現該欄位不可變動寬度,直接存進DB
                        } else if (!itemWCColumn.getCanModifyWidth()) {
                            wcReportCustomColumnInfoTos.add(item);
                        } else {
                            item.setWidth(selCutom.get(0).getWidth());
                            wcReportCustomColumnInfoTos.add(item);
                        }
                    });
        } else {
            wcColumns.forEach(
                item -> {
                    if (!item.getDefaultShow()) {
                        return;
                    }
                    if (!item.getCanModifyWidth()) {
                        WCReportCustomColumnInfoTo wt = new WCReportCustomColumnInfoTo();
                        wt.setName(item.getVal());
                        wt.setWidth(item.getDefaultWidth());
                        wcReportCustomColumnInfoTos.add(wt);
                    } else {
                        List<CustomColumnDetailTo> selCutom =
                            customColumnDetailTo.stream()
                                .filter(each -> each.getName().equals(item.getVal()))
                                .collect(Collectors.toList());
                        if (selCutom != null && selCutom.size() > 0) {
                            WCReportCustomColumnInfoTo wt = new WCReportCustomColumnInfoTo();
                            wt.setName(item.getVal());
                            wt.setWidth(selCutom.get(0).getWidth());
                            wcReportCustomColumnInfoTos.add(wt);
                        }
                    }
                });
        }
        return wcReportCustomColumnInfoTos;
    }

    /**
     * 儲存欄位設定
     *
     * @param userSid                     登入者Sid
     * @param wcColumns
     * @param selectCustomCulumns
     * @param pageCount
     * @param wcReportCustomColumnUrlType
     */
    public void saveWCColumnShowSetting(
        Integer userSid,
        List<Column> wcColumns,
        List<CustomCulumnVO> selectCustomCulumns,
        String pageCount,
        WCReportCustomColumnUrlType wcReportCustomColumnUrlType) {
        WCReportCustomColumn columDB =
            wkReportCustomColumnManager.getWCReportCustomColumnByUrlAndUserSid(
                wcReportCustomColumnUrlType, userSid);

        if (columDB == null) {
            columDB = new WCReportCustomColumn();
            columDB.setUrl(wcReportCustomColumnUrlType);
        }
        columDB.setPagecnt(Integer.valueOf(pageCount));
        WCReportCustomColumnInfo wci = new WCReportCustomColumnInfo();
        wci.setWkReportCustomColumnInfoTos(
            settingShowColumn(
                columDB.getInfo(), wcColumns, selectCustomCulumns, wcReportCustomColumnUrlType));
        columDB.setInfo(wci);
        wkReportCustomColumnManager.save(columDB, userSid);
    }

    /**
     * 建立欄位介面物件
     *
     * @param wcColumn
     * @return
     */
    private ColumnDetailVO createDefaultColumnDetail(Column wcColumn) {
        return new ColumnDetailVO(
            wcColumn.getVal(), wcColumn.getDefaultShow(),
            transToStyleCss(wcColumn.getDefaultWidth()));
    }

    public WCTagSearchColumnVO getWCTagSearchColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB =
            wkReportCustomColumnManager.getWCReportCustomColumnByUrlAndUserSid(
                WCReportCustomColumnUrlType.TAG_SEARCH, userSid);
        WCTagSearchColumnVO vo = new WCTagSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WCTagSearchColumn.INDEX));
            vo.setCreateTime(createDefaultColumnDetail(WCTagSearchColumn.CREATE_TIME));
            vo.setCreateUser(createDefaultColumnDetail(WCTagSearchColumn.CREATE_USER));
            vo.setTag(createDefaultColumnDetail(WCTagSearchColumn.TAG));
            vo.setStatus(createDefaultColumnDetail(WCTagSearchColumn.STATUS));
            vo.setModifyTime(createDefaultColumnDetail(WCTagSearchColumn.MODIFY_TIME));
            vo.setEditMenu(createDefaultColumnDetail(WCTagSearchColumn.MENUTAG));
            vo.setSeq(createDefaultColumnDetail(WCTagSearchColumn.SEQ));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWCColumnDetailVO(WCTagSearchColumn.INDEX, columDB.getInfo()));
            vo.setCreateTime(
                transToWCColumnDetailVO(WCTagSearchColumn.CREATE_TIME, columDB.getInfo()));
            vo.setCreateUser(
                transToWCColumnDetailVO(WCTagSearchColumn.CREATE_USER, columDB.getInfo()));
            vo.setTag(transToWCColumnDetailVO(WCTagSearchColumn.TAG, columDB.getInfo()));
            vo.setStatus(transToWCColumnDetailVO(WCTagSearchColumn.STATUS, columDB.getInfo()));
            vo.setModifyTime(
                transToWCColumnDetailVO(WCTagSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setEditMenu(transToWCColumnDetailVO(WCTagSearchColumn.MENUTAG, columDB.getInfo()));
            vo.setSeq(transToWCColumnDetailVO(WCTagSearchColumn.SEQ, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * * 取得欄位介面物件(工作聯絡單報表)
     *
     * @param userSid
     * @return
     */
    public WCWorkReportSearchColumnVO getWCWorkReportSearchColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB =
            wkReportCustomColumnManager.getWCReportCustomColumnByUrlAndUserSid(
                WCReportCustomColumnUrlType.WORK_REPORT_SEARCH, userSid);
        WCWorkReportSearchColumnVO vo = new WCWorkReportSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(createDefaultColumnDetail(WCWorkReportSearchColumn.INDEX));
            vo.setFowardDep(createDefaultColumnDetail(WCWorkReportSearchColumn.FOWARDDEP));
            vo.setFowardMember(createDefaultColumnDetail(WCWorkReportSearchColumn.FOWARDMEMBER));
            vo.setTrace(createDefaultColumnDetail(WCWorkReportSearchColumn.TRACE));
            vo.setConnect(createDefaultColumnDetail(WCWorkReportSearchColumn.CONNECT));
            vo.setCreateTime(createDefaultColumnDetail(WCWorkReportSearchColumn.CREATE_TIME));
            vo.setTag(createDefaultColumnDetail(WCWorkReportSearchColumn.TAG));
            vo.setDepartment(createDefaultColumnDetail(WCWorkReportSearchColumn.DEPARTMENT));
            vo.setCreateUser(createDefaultColumnDetail(WCWorkReportSearchColumn.CREATE_USER));
            vo.setTheme(createDefaultColumnDetail(WCWorkReportSearchColumn.THEME));
            vo.setStatus(createDefaultColumnDetail(WCWorkReportSearchColumn.STATUS));
            vo.setNo(createDefaultColumnDetail(WCWorkReportSearchColumn.NO));
            vo.setModifyTime(createDefaultColumnDetail(WCWorkReportSearchColumn.MODIFY_TIME));
            vo.setReaded(createDefaultColumnDetail(WCWorkReportSearchColumn.READED));
            vo.setReadRecipt(createDefaultColumnDetail(WCWorkReportSearchColumn.READRECIPT));
            vo.setReadRecipted(
                createDefaultColumnDetail(WCWorkReportSearchColumn.READRECIPT_SENDED));
            vo.setExecDepartment(
                createDefaultColumnDetail(WCWorkReportSearchColumn.EXECDEPARTMENT));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(transToWCColumnDetailVO(WCWorkReportSearchColumn.INDEX, columDB.getInfo()));

            vo.setFowardDep(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.FOWARDDEP, columDB.getInfo()));
            vo.setFowardMember(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.FOWARDMEMBER, columDB.getInfo()));
            vo.setTrace(transToWCColumnDetailVO(WCWorkReportSearchColumn.TRACE, columDB.getInfo()));
            vo.setConnect(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.CONNECT, columDB.getInfo()));

            vo.setCreateTime(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.CREATE_TIME, columDB.getInfo()));

            vo.setTag(transToWCColumnDetailVO(WCWorkReportSearchColumn.TAG, columDB.getInfo()));
            vo.setDepartment(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.DEPARTMENT, columDB.getInfo()));
            vo.setCreateUser(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.CREATE_USER, columDB.getInfo()));
            vo.setTheme(transToWCColumnDetailVO(WCWorkReportSearchColumn.THEME, columDB.getInfo()));

            vo.setStatus(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.STATUS, columDB.getInfo()));
            vo.setNo(transToWCColumnDetailVO(WCWorkReportSearchColumn.NO, columDB.getInfo()));

            vo.setModifyTime(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.MODIFY_TIME, columDB.getInfo()));
            vo.setReaded(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.READED, columDB.getInfo()));
            vo.setReadRecipt(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.READRECIPT, columDB.getInfo()));
            vo.setReadRecipted(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.READRECIPT_SENDED,
                    columDB.getInfo()));
            vo.setExecDepartment(
                transToWCColumnDetailVO(WCWorkReportSearchColumn.EXECDEPARTMENT,
                    columDB.getInfo()));
        }
        return vo;
    }

    public void saveWCColumnWidthSetting(
        WCReportCustomColumnUrlType wcReportCustomColumnUrlType,
        Integer userSid,
        List<CustomColumnDetailTo> customColumnDetailTo,
        List<Column> wcColumns,
        String pageCount) {
        WCReportCustomColumn columDB =
            wkReportCustomColumnManager.getWCReportCustomColumnByUrlAndUserSid(
                wcReportCustomColumnUrlType, userSid);
        if (columDB == null) {
            columDB = new WCReportCustomColumn();
            columDB.setUrl(wcReportCustomColumnUrlType);
            columDB.setPagecnt(Integer.valueOf(pageCount));
        }
        WCReportCustomColumnInfo wci = new WCReportCustomColumnInfo();
        wci.setWkReportCustomColumnInfoTos(
            settingWidthColumn(
                columDB.getInfo(), wcColumns, customColumnDetailTo, wcReportCustomColumnUrlType));
        columDB.setInfo(wci);
        wkReportCustomColumnManager.save(columDB, userSid);
    }

    public List<CustomCulumnVO> getSelectWCColumn(
        WCReportCustomColumnUrlType wkReportCustomColumnUrlType, Integer userSid) {
        WCReportCustomColumn draftColumDB =
            wkReportCustomColumnManager.getWCReportCustomColumnByUrlAndUserSid(
                wkReportCustomColumnUrlType, userSid);
        if (draftColumDB == null
            || draftColumDB.getInfo() == null
            || draftColumDB.getInfo().getWkReportCustomColumnInfoTos() == null
            || draftColumDB.getInfo().getWkReportCustomColumnInfoTos().size() == 0) {
            return null;
        }
        List<CustomCulumnVO> selectColumns = Lists.newArrayList();
        draftColumDB
            .getInfo()
            .getWkReportCustomColumnInfoTos()
            .forEach(
                item -> {
                    CustomCulumnVO cv = new CustomCulumnVO(true, item.getName(), item.getWidth());
                    selectColumns.add(cv);
                });
        return selectColumns;
    }

    private String transToStyleCss(String width) {
        if (!Strings.isNullOrEmpty(width)) {
            return "width: " + width + "px";
        }
        return "";
    }

    public ColumnDetailVO transToWCColumnDetailVO(
        Column wcColumn, WCReportCustomColumnInfo wkReportCustomColumnInfo) {
        if (wkReportCustomColumnInfo != null
            && wkReportCustomColumnInfo.getWkReportCustomColumnInfoTos() != null
            && wkReportCustomColumnInfo.getWkReportCustomColumnInfoTos().size() > 0) {
            // 搜尋資料庫是否有該欄位記錄長度資料
            List<WCReportCustomColumnInfoTo> wkReportCustomColumnInfoTo =
                wkReportCustomColumnInfo.getWkReportCustomColumnInfoTos().stream()
                    .filter(each -> each.getName().equals(wcColumn.getVal()))
                    .collect(Collectors.toList());
            if (wkReportCustomColumnInfoTo != null && wkReportCustomColumnInfoTo.size() > 0) {
                // 若資料庫有值代表,有紀錄需顯示,還需確認改欄位是否可改寬度,若不可改寬度,需顯示預設
                return new ColumnDetailVO(
                    wcColumn.getVal(),
                    true,
                    (wcColumn.getCanModifyWidth())
                        ? transToStyleCss(wkReportCustomColumnInfoTo.get(0).getWidth())
                        : transToStyleCss(wcColumn.getDefaultWidth()));
            } else if (wcColumn.getCanSelectItem()) {
                // 若該欄位是可被選擇是否不顯示,在資料庫欄位未發現,代表使用者有變更將其變為不顯示
                return new ColumnDetailVO(wcColumn.getVal(), false, "");
            } else {
                // 若在資料庫欄位未發現,但該欄位是不可被選擇不顯示的,代表須顯示,寬度為預設
                return new ColumnDetailVO(
                    wcColumn.getVal(), true, transToStyleCss(wcColumn.getDefaultWidth()));
            }
        } else {
            return new ColumnDetailVO(
                wcColumn.getVal(),
                wcColumn.getDefaultShow(),
                transToStyleCss(wcColumn.getDefaultWidth()));
        }
    }
}
