package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.WCCategoryTagManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.CategoryTagVO;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.view.vo.CategoryTagEditVO;
import com.cy.work.connect.web.view.vo.CategoryTagSearchVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WCCategoryTagLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -650082079644146776L;

    private static WCCategoryTagLogicComponent instance;
    @Autowired
    private WCCategoryTagManager wcCategoryTagManager;

    public static WCCategoryTagLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCCategoryTagLogicComponent.instance = this;
    }

    public List<Org> findDistinctCompSidByActiveStatus() {
        return wcCategoryTagManager.findDistinctCompSidByActiveStatus().stream()
            .map(each -> WkOrgCache.getInstance().findBySid(each))
            .collect(Collectors.toList());
    }

    /**
     * 取得類別樹-所需大類中類
     *
     * @param loginCompSid     登入公司Sid
     * @param loginUserSid     登入者Sid
     * @return
     */
    public List<CategoryTagVO> getCategoryTags(
        Integer loginCompSid, Integer loginUserSid) {
        return wcCategoryTagManager.getCategoryTags(loginCompSid, loginUserSid);
    }

    public void saveCategoryTag(
        String sid,
        // String seq,
        String name,
        String status,
        String memo,
        Integer loginUserSid,
        Integer loginCompSid) {

        if (Strings.isNullOrEmpty(name)) {
            Preconditions.checkState(false, "類別為必要輸入欄位，請確認！！");
        }
        if (Strings.isNullOrEmpty(status)) {
            Preconditions.checkState(false, "狀態為必要輸入欄位，請確認！！");
        }
        WCCategoryTag wcCategoryTag = null;
        if (Strings.isNullOrEmpty(sid)) {
            wcCategoryTag = new WCCategoryTag();
        } else {
            wcCategoryTag = wcCategoryTagManager.getWCCategoryTagBySid(sid);
        }
        for (WCCategoryTag wt : this.findAll(Activation.ACTIVE, loginCompSid)) {
            if (!wt.getSid().equals(sid)) {
                if (wt.getCategoryName().equals(name)) {
                    Preconditions.checkState(false, "該名稱已存在，請確認！！");
                }
            }
        }
        wcCategoryTag.setCompSid(loginCompSid);
        wcCategoryTag.setMemo(memo);
        wcCategoryTag.setStatus(Activation.valueOf(status));
        wcCategoryTag.setCategoryName(name);
        if (Strings.isNullOrEmpty(sid)) {
            wcCategoryTag = wcCategoryTagManager.createWCCategoryTag(wcCategoryTag, loginUserSid);
        } else {
            wcCategoryTag = wcCategoryTagManager.updateWCCategoryTag(wcCategoryTag, loginUserSid);
        }
    }

    public CategoryTagEditVO getCategoryTagEditVOBySid(String sid) {
        WCCategoryTag wcCategoryTag = wcCategoryTagManager.getWCCategoryTagBySid(sid);
        CategoryTagEditVO ct = new CategoryTagEditVO();
        ct.setCategoryName(wcCategoryTag.getCategoryName());
        ct.setMemo(wcCategoryTag.getMemo());
        ct.setSeq(String.valueOf(wcCategoryTag.getSeq()));
        ct.setStatus(wcCategoryTag.getStatus().name());
        ct.setSid(wcCategoryTag.getSid());
        return ct;
    }

    public List<CategoryTagSearchVO> getCategoryTagSearch(
        String statusID, 
        String tagName, 
        String tagID, 
        Integer loginCompSid,
        boolean hideInactiveCategory) {
        List<CategoryTagSearchVO> tagSearchs = Lists.newArrayList();
        
        //處理不顯示停用
        Activation status = null;
        if(hideInactiveCategory) {
            status = Activation.ACTIVE;
        }
        
        
        this.findAll(status, loginCompSid)
            .forEach(
                item -> {
                    if (!Strings.isNullOrEmpty(statusID) && !statusID.equals(
                        item.getStatus().name())) {
                        return;
                    }
                    if (!Strings.isNullOrEmpty(tagName)
                        && !item.getCategoryName().toLowerCase().contains(tagName.toLowerCase())) {
                        return;
                    }

                    if (!Strings.isNullOrEmpty(tagID) && !item.getSid().equals(tagID)) {
                        return;
                    }

                    String statusStr = (Activation.ACTIVE.equals(item.getStatus())) ? "正常" : "停用";
                    CategoryTagSearchVO categoryTagSearchVO =
                        new CategoryTagSearchVO(
                            item.getSid(),
                            ToolsDate.transDateToString(
                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                item.getCreateDate()),
                            WkUserUtils.findNameBySid(item.getCreateUser()),
                            item.getCategoryName(),
                            statusStr,
                            ToolsDate.transDateToString(
                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                item.getUpdateDate()),
                            WkUserUtils.findNameBySid(item.getUpdateUser()),
                            String.valueOf(item.getSeq()),
                            item.getMemo());
                    tagSearchs.add(categoryTagSearchVO);
                });
        return tagSearchs;
    }

    /**
     * 取得類別BySid
     *
     * @param sid 大類(類別)Sid
     * @return
     */
    public WCCategoryTag getWCCategoryTagBySid(String sid) {
        if (sid == null) {
            return null;
        }
        return wcCategoryTagManager.getWCCategoryTagBySid(sid);
    }

    /**
     * 取得類別By啟用狀態 & 登入公司Sid
     *
     * @param status       啟用狀態
     * @param loginCompSid 登入公司Sid
     * @return
     */
    public List<WCCategoryTag> findAll(Activation status, Integer loginCompSid) {
        return wcCategoryTagManager.getWCCategoryTags(status, loginCompSid);
    }

    /**
     * 依據傳入順序排序
     *
     * @param tagSearchVOs 大類(類別)搜尋物件清單
     */
    public void sort(List<CategoryTagSearchVO> tagSearchVOs) {
        if (WkStringUtils.isEmpty(tagSearchVOs)) {
            return;
        }

        List<String> sids =
            tagSearchVOs.stream().map(CategoryTagSearchVO::getSid).collect(Collectors.toList());

        this.wcCategoryTagManager.updateSortSeqByListIndex(sids);
    }
}
