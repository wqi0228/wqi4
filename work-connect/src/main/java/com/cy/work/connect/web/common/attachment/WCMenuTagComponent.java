package com.cy.work.connect.web.common.attachment;

import com.cy.work.connect.logic.utils.FileNameValidator;
import com.google.common.collect.Maps;
import java.io.ByteArrayInputStream;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

@Slf4j
public class WCMenuTagComponent {

    @Getter
    private final Map<String, ByteArrayInputStream> attachments = Maps.newHashMap();
    @Getter
    private final Map<String, Boolean> finalUploadFiles = Maps.newHashMap();
    @Getter
    private final StringBuffer uploadMessages = new StringBuffer();
    @Getter
    @Setter
    private boolean checked;

    public void init() {
        attachments.clear();
        uploadMessages.delete(0, uploadMessages.length());
    }

    public void validateFiles(FileUploadEvent event) {
        UploadedFile uploadedFile = event.getFile();
        try {
            if (validateByFileNameAndResponse(uploadedFile.getFileName())) {
                attachments.put(
                    uploadedFile.getFileName(),
                    new ByteArrayInputStream(uploadedFile.getContent()));
                finalUploadFiles.put(uploadedFile.getFileName(), true);
                addMessage(uploadedFile.getFileName(), "OK");
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }

    public Integer getFileSizeLimit() {
        return 50;
    }

    public boolean validateByFileNameAndResponse(String fileName) {
        FileNameValidator fileNameValidator = new FileNameValidator(fileName);
        if (!fileNameValidator.isAcceptable()) {
            addMessage(fileName, "不接受，" + fileNameValidator.getValidateMessage());
            return false;
        }
        if (attachments.get(fileName) != null) {
            addMessage(fileName, "上傳失敗，已重複檔名，請重新命名");
            return false;
        }
        return true;
    }

    private void addMessage(String fileName, String detail) {
        if (uploadMessages.length() > 0) {
            uploadMessages.append("\n");
        }
        uploadMessages.append("\"");
        uploadMessages.append(fileName);
        uploadMessages.append("\": ");
        uploadMessages.append(detail);
    }

    public void changeCheckbox(String key) {
        //        if(finalUploadFiles.get(key)){
        //            finalUploadFiles.put(key, true);
        //        } else {
        //            finalUploadFiles.put(key, false);
        //        }
    }
}
