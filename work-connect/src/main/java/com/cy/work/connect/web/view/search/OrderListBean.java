package com.cy.work.connect.web.view.search;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.repository.result.OrderListResult;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.OrderListColumn;
import com.cy.work.connect.web.common.ExportExcelComponent;
import com.cy.work.connect.web.common.clause.OrderListQueryClause;
import com.cy.work.connect.web.common.clause.OrderListViewControl;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.TableUpDownListener;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.TableUpDownBean;
import com.cy.work.connect.web.view.components.CustomColumnComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkReportSidTo;
import com.cy.work.connect.web.view.vo.column.search.OrderListColumnVO;
import com.cy.work.connect.web.view.vo.search.helper.OrderListHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Scope("view")
@ManagedBean
@Controller
public class OrderListBean implements Serializable, TableUpDownListener {

    /**
     *
     */
    private static final long serialVersionUID = 2512669996256065060L;

    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private OrderListHelper orderListHelper;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Autowired
    private ExportExcelComponent exportExcelComponent;

    @Getter
    private OrderListQueryClause queryClause;
    @Getter
    private OrderListViewControl control;
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /** */
        private static final long serialVersionUID = -9123679411509761790L;

        @Override
        public void showMessage(String m) {
            control.setErrorMsg(m);
            DisplayController.getInstance().update("confimDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
        }
    };

    @Getter
    /** dataTable filter items */
    private List<SelectItem> menuTagNameItems;

    @Getter
    /** dataTable filter items */
    private List<SelectItem> categoryTagNameItems;

    @Getter
    /** dataTable filter items */
    private List<SelectItem> statusNameItems;

    @Getter
    /** 類別選項 */
    private List<SelectItem> categoryItems;

    @Getter
    /** 單據狀態 選項 */
    private List<SelectItem> statusItems;

    @Getter
    @Setter
    /** 登入使用者資訊 */
    private UserViewVO userViewVO;

    @Getter
    private List<OrderListResult> allVos;
    @Getter
    private List<OrderListResult> tempVOs;
    @Getter
    @Setter
    private List<OrderListResult> fillterVos;
    /**
     * 暫存資料
     */
    private String tempSelSid;

    @Getter
    @Setter
    private OrderListResult selVo;
    @Getter
    private CustomColumnComponent customColumn;

    @Getter
    /** dataTable 欄位資訊 */
    private OrderListColumnVO columnVO;

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /** */
        private static final long serialVersionUID = -3428353502424725572L;

        @Override
        public void reload() {
            columnVO = orderListHelper.getOrderListColumnVO(userViewVO.getSid());
            DisplayController.getInstance().update(control.getDataTableID());
        }
    };

    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;

    @PostConstruct
    public void init() {
        initUserViewVO();
        initOptions();
        columnVO = orderListHelper.getOrderListColumnVO(SecurityFacade.getUserSid());
        control = new OrderListViewControl();
        queryClause = new OrderListQueryClause();
        queryClause.init();
        customColumn = new CustomColumnComponent(
                WCReportCustomColumnUrlType.WORK_ORDER_LIST,
                Arrays.asList(OrderListColumn.values()),
                columnVO.getPageCount(),
                userViewVO.getSid(),
                messageCallBack,
                dataTableReLoadCallBack);

        query();
    }

    private void initOptions() {
        categoryItems = categoryTagLogicComponent.findAll(Activation.ACTIVE, compViewVo.getSid()).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
        statusItems = Lists.newArrayList(
                WCStatus.NEW_INSTANCE,
                WCStatus.WAITAPPROVE,
                WCStatus.APPROVING,
                WCStatus.RECONSIDERATION,
                WCStatus.APPROVED,
                WCStatus.EXEC,
                WCStatus.EXEC_FINISH,
                WCStatus.CLOSE,
                WCStatus.CLOSE_STOP,
                WCStatus.INVALID)
                .stream()
                .map(each -> new SelectItem(each.name(), each.getVal()))
                .collect(Collectors.toList());
    }

    private void initUserViewVO() {
        this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());

        User userModel = userManager.findBySid(SecurityFacade.getUserSid());
        Org dep = orgManager.findBySid(userModel.getPrimaryOrg().getSid());
        List<Org> managedDepts = orgManager.findManagerWithChildOrgSids(SecurityFacade.getUserSid()).stream()
                .map(each -> orgManager.findBySid(each))
                .collect(Collectors.toList());
        List<Org> parentOrgs = orgManager.findAllParent(dep.getSid());
        List<Org> tempOrgs = Lists.newArrayList();
        tempOrgs.add(dep);
        tempOrgs.addAll(parentOrgs);
        tempOrgs.addAll(
                orgManager.findManagerOrgs(SecurityFacade.getUserSid()).stream()
                        .flatMap(
                                each -> {
                                    return Sets.newHashSet(
                                            WorkSettingOrgManager.getInstance()
                                                    .getAllParentAndChildAndOneself(each))
                                            .stream();
                                })
                        .collect(Collectors.toSet()));
        List<Integer> orgSids = tempOrgs.stream().map(Org::getSid).distinct().collect(Collectors.toList());
        userViewVO.getExecRelatedDeps().addAll(orgSids);

        if (managedDepts != null && !managedDepts.isEmpty()) {
            userViewVO.setManager(true);
            userViewVO
                    .getManagedDeps()
                    .addAll(managedDepts.stream().map(Org::getSid).collect(Collectors.toSet()));
        }
    }

    // 清除
    public void clear() {
        queryClause.clear();
        query();
    }


    // 搜尋
    public List<OrderListResult> query() {
        try {
            if (queryClause.getDateIntervalClause().getStartDate() != null
                    && queryClause.getDateIntervalClause().getEndDate() != null) {
                if (queryClause
                        .getDateIntervalClause()
                        .getStartDate()
                        .after(queryClause.getDateIntervalClause().getEndDate())) {
                    messageCallBack.showMessage("建立區間起始日期不可晚於結束日期");
                }
            }
            allVos = searchView();
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("query：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("query", e);
            messageCallBack.showMessage(e.getMessage());
        }
        return allVos;
    }


    /**
     * 刷新畫面動作
     *
     * @param sid
     * @return
     */
    private List<OrderListResult> searchView() {
        List<OrderListResult> tempAllVos = orderListHelper.queryByCondition(
                queryClause, SecurityFacade.getUserSid(), null);

        this.menuTagNameItems = tempAllVos.stream()
                .map(OrderListResult::getMenuTagName)
                .collect(Collectors.toSet())
                .stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());

        this.categoryTagNameItems = tempAllVos.stream()
                .map(OrderListResult::getCategoryName)
                .collect(Collectors.toSet())
                .stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());

        this.statusNameItems = tempAllVos.stream()
                .map(OrderListResult::getStatusName)
                .distinct()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());

        return tempAllVos;
    }

    /**
     * 載入滿版
     */
    public void showDetail(String sid) {
        control.setShowFrame(true);
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            control.setIframeUrl(control.getUrl() + selVo.getWcSid());
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.warn("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟分頁
     *
     * @param sid
     */
    public void openWebTab(String sid) {
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(control.getDataTableID());
        } catch (Exception e) {
            log.warn("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 變更 selVo
     */
    private void settintSelVo(String sid) {
        OrderListResult sel = new OrderListResult(sid);
        int index = allVos.indexOf(sel);
        if (index > 0) {
            selVo = allVos.get(index);
        } else {
            selVo = allVos.get(0);
        }
    }

    /**
     * 更新dtScrollHeight
     */
    public void updateDtScrollHeight() {
        String height = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("dtScrollHeight");
        if (!Strings.isNullOrEmpty(height)) {
            control.setDtScrollHeight(height);
        }
    }

    /**
     * 回到列表查詢
     */
    public void closeIframe() {
        control.setShowFrame(false);
        try {
            control.setIframeUrl("");
        } catch (Exception e) {
            log.warn("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行上一筆
     */
    @Override
    public void openerByBtnUp() {
        try {
            if (!allVos.isEmpty()) {
                this.toUp();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
            }
            DisplayController.getInstance().update(control.getDataTableID());
        } catch (Exception e) {
            log.warn("工作聯絡單進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行上一筆
     */
    private void toUp() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index > 0) {
                    selVo = tempVOs.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index > 0) {
                    selVo = allVos.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
            }
        } catch (Exception e) {
            log.warn("toUp", e);
            this.clearTemp();
            if (!allVos.isEmpty()) {
                selVo = allVos.get(0);
            }
        }
    }

    /**
     * 執行下一筆
     */
    @Override
    public void openerByBtnDown() {
        try {
            if (!allVos.isEmpty()) {
                this.toDown();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
            }
            DisplayController.getInstance().update(control.getDataTableID());
        } catch (Exception e) {
            log.warn("工作聯絡單查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行下一筆
     */
    private void toDown() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index >= 0) {
                    selVo = tempVOs.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index >= 0) {
                    selVo = allVos.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
            }
        } catch (Exception e) {
            log.warn("toDown", e);
            this.clearTemp();
            if (allVos.size() > 0) {
                selVo = allVos.get(allVos.size() - 1);
            }
        }
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNextAndReaded() {
        this.settingPreviousAndNext();
        tableUpDownBean.setSession_now_sid(selVo.getWcSid());
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNext() {
        int index = allVos.indexOf(selVo);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(allVos.get(index - 1).getWcSid());
        }
        if (index == allVos.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(allVos.get(index + 1).getWcSid());
        }
    }

    /**
     * 判斷是否有暫存資訊
     */
    private boolean checkTempVOs() {
        return tempVOs != null && !tempVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid);
    }

    /**
     * 清除暫存
     */
    private void clearTemp() {
        tempSelSid = "";
        tempVOs = null;
    }

    /**
     * 取得暫存索引
     */
    private int getTempIndex() { return tempVOs.indexOf(new OrderListResult(tempSelSid)); }

    public void reloadDataByUpdate() {
        this.doReloadInfo();
    }

    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    @Override
    public void openerByFavorite() {
    }

    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.warn("doReloadInfo", ex);
        }
    }

    private void updateInfoToDataTable(String sid) {
        try {
            List<OrderListResult> result = orderListHelper.queryByCondition(queryClause, SecurityFacade.getUserSid(),
                    sid);
            if (result == null || result.isEmpty()) {
                tempVOs = Lists.newArrayList();
                if (allVos != null && !allVos.isEmpty()) {
                    tempVOs.addAll(allVos);
                    allVos.remove(new OrderListResult(sid));
                    tempSelSid = sid;
                }
            } else {
                OrderListResult updateDeail = result.get(0);
                allVos.forEach(
                        item -> {
                            if (item.getWcSid().equals(updateDeail.getWcSid())) {
                                item.replaceValue(updateDeail);
                            }
                        });
            }
            DisplayController.getInstance().update(control.getDataTableID());
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("updateInfoToDataTable Error：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("updateInfoToDataTable Error", e);
        }
    }

    /**
     * 執行匯出前置
     */
    public void hideColumnContent() {
        control.setDisplayButton(false);
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcel(
                    document,
                    queryClause.getDateIntervalClause().getStartDate(),
                    queryClause.getDateIntervalClause().getEndDate(),
                    "單據清單",
                    true);
        } catch (Exception e) {
            log.warn("exportExcel", e);
        } finally {
            control.setDisplayButton(true);
        }
    }
}
