package com.cy.work.connect.web.common;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkWebPageUtils;
import java.io.Serializable;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 提供 common-vo 畫面相關顯示
 *
 * @author jimmy_chou
 */
@Controller
public class CommonVoBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2129374428986954990L;

    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WkWebPageUtils webPageUtils;

    /**
     * 停用部門給予紅色字體
     *
     * @param sid
     * @return
     */
    public String getRowStyleClassByOrg(Integer sid) {
        Activation status =
            Optional.ofNullable(orgManager.findBySid(sid)).map(Org::getStatus)
                .orElseGet(() -> null);
        if (Activation.ACTIVE.equals(status)) {
            return "";
        }
        return "redColor";
    }

    /**
     * 停用部門給予紅色字體
     *
     * @param dep
     * @return
     */
    public String getRowStyleClassByOrg(Org dep) {
        if (dep == null) {
            return "";
        }
        if (Activation.ACTIVE.equals(dep.getStatus())) {
            return "";
        }
        return "redColor";
    }

    /**
     * 取得Org Name
     *
     * @param sid
     * @return
     */
    public String getNameByOrg(Integer sid) {
        return WkOrgUtils.findNameBySid(sid);
    }

    /**
     * 取得Org Name
     *
     * @param dep
     * @return
     */
    public String getNameByOrgObj(Org dep) {
        if (dep == null) {
            return "";
        }
        return dep.getName();
    }

    /**
     * 取代使用者複製貼上的標籤格式
     *
     * @param value
     * @return
     */
    public String addWrapTag(String value) {
        return webPageUtils.addWrapTag(value);
    }
}
