package com.cy.work.connect.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkOrgTransMappingUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCTransManager;
import com.cy.work.connect.logic.vo.view.setting11.SettingOrgTransPageVO;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.components.Setting11ExecDepComponent;
import com.cy.work.connect.web.view.components.Setting11TransDepComponent;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 組織異動轉換管理 MBean
 *
 * @author jimmy_chou
 */
@Slf4j
@NoArgsConstructor
@Controller
@Scope("view")
@ManagedBean
public class Setting11Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2354224821585616989L;

    @Getter
    @Setter
    private SettingOrgTransPageVO pageVO;
    
    /**
     * 生效日下拉選項
     */
    @Getter
    private List<SelectItem> allEffectiveDateItems;

    /**
     * 是否為有效頁面
     */
    private Boolean isActiveView = Boolean.FALSE;

    @Autowired
    private SessionTimerBean sessionTimerBean;
    @Autowired
    private DisplayController displayController;
    @Autowired
    private transient WCTransManager transManager;

    /**
     * 執行單位轉檔 component
     */
    @Getter
    @Setter
    private Setting11ExecDepComponent execDepComponent;

    /**
     * 指派單位轉檔 component
     */
    @Getter
    @Setter
    private Setting11TransDepComponent transDepComponent;

    /**
     * view 顯示錯誤訊息
     */
    @Getter
    private String errorMessage;

    @PostConstruct
    public void init() {
        // ====================================
        // 本頁VO初始化
        // ====================================
        this.pageVO = new SettingOrgTransPageVO();
        
        // 準備生效日列表生效日
        this.prepareSelectItem();
        // 查詢設定資料中，最大的生效日
        if (WkStringUtils.notEmpty(this.allEffectiveDateItems)) {
            // 取日期最近的一筆
            this.pageVO.setQryEffectiveDate((Long) this.allEffectiveDateItems.get(0).getValue());
        }

        // ====================================
        // Component 初始化
        // ====================================
        this.execDepComponent = new Setting11ExecDepComponent();
        this.transDepComponent = new Setting11TransDepComponent();
        // ====================================
        // 查詢
        // ====================================
        this.btnSearchWorkVerify();
    }
    
    public void prepareSelectItem() {
        this.allEffectiveDateItems = Lists.newArrayList();

        // 查詢所有的生效日
        List<Date> allEffectiveDate =  WkOrgTransMappingUtils.getInstance().findOrgTransMappingEffectiveDates(SecurityFacade.getCompanyId());
        if (WkStringUtils.isEmpty(allEffectiveDate)) {
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (Date date : allEffectiveDate) {
            this.allEffectiveDateItems.add(new SelectItem(date.getTime(), sdf.format(date)));
        }
    }

    /**
     * 有效頁面
     */
    public void viewAction() {
        this.isActiveView = Boolean.TRUE;
    }

    /**
     * 事件後觸發
     */
    public void preRenderView() {
        if (!this.isActiveView) {
            this.displayController.showPfWidgetVar("dlgIdleSession");
            return;
        }
        this.sessionTimerBean.reStartIdle();
    }

    /**
     * 查詢轉換單位設定資料
     */
    public void btnSearchWorkVerify() {

        try {
            // ====================================
            // 查詢轉換單位設定資料
            // ====================================
            this.transManager.btnSearchWorkVerify(this.pageVO, SecurityFacade.getCompanyId());

            this.displayController.execute("removeElmClass('spec_RemoveCss', 'ui-panelgrid')");

        } catch (Exception e) {
            String errMsg = "查詢時發生錯誤：" + e.getMessage();
            log.error(errMsg, e);
            MessagesUtils.showError(errMsg);
        }
    }
}
