/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.config;

import com.cy.work.common.vo.converter.InstanceStatusConverter;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author brain0925_liao
 */
@Slf4j
@Configuration
@EnableJpaRepositories(value = { "com.cy.work.connect.repository", "com.cy.work.backend.repository" })
@EnableTransactionManagement
public class JpaConfiguration {

    @Autowired
    private WCProperties properties;

    @Bean(name = ConnectConstants.CONNECT_JDBC_TEMPLATE)
    public JdbcTemplate reqJdbcTemplate() {
        JdbcTemplate template = new JdbcTemplate();
        template.setDataSource(this.sqlDataSource());
        return template;
    }

    @Bean
    public DataSource sqlDataSource() {
        HikariConfig tgHikariConfig = new HikariConfig();
        try {
            tgHikariConfig.setDriverClassName(properties.getClassName());
            tgHikariConfig.setJdbcUrl(properties.getDbUrl());
            tgHikariConfig.setUsername(properties.getDbUserName());
            tgHikariConfig.setPassword(properties.getDbPassword());
            tgHikariConfig.setMaximumPoolSize(properties.getMaxPoolSize());
            tgHikariConfig.setMinimumIdle(properties.getMinimumIdle());
            tgHikariConfig.setIdleTimeout(properties.getIdleTimeout());
            tgHikariConfig.setMaxLifetime(properties.getMaxLifetime());
            tgHikariConfig.addDataSourceProperty(
                    "dataSource.cachePrepStmts", properties.getCachePrepStmts());
            tgHikariConfig.addDataSourceProperty(
                    "dataSource.useServerPrepStmts", properties.getUseServerPrepStmts());
            tgHikariConfig.addDataSourceProperty(
                    "dataSource.prepStmtCacheSize", properties.getPrepStmtCacheSize());
            tgHikariConfig.addDataSourceProperty(
                    "dataSource.prepStmtCacheSqlLimit", properties.getPrepStmtCacheSqlLimit());
            tgHikariConfig.setConnectionTestQuery("SELECT 1");
            tgHikariConfig.setPoolName("springHikariCP");
            tgHikariConfig.addDataSourceProperty(
                    "dataSource.logger", "com.mysql.jdbc.log.StandardLogger");
            tgHikariConfig.addDataSourceProperty("dataSource.logSlowQueries", true);
            tgHikariConfig.addDataSourceProperty("dataSource.dumpQueriesOnException", true);
            // dataSource.logger=com.mysql.jdbc.log.StandardLogger
            // dataSource.logSlowQueries=true
            // dataSource.dumpQueriesOnException=true
            log.info("dataSource connection is valid");
        } catch (Exception e) {
            log.error("dataSource connection is fail", e);
        }
        log.info("dataSource has startup" + tgHikariConfig);
        return new HikariDataSource(tgHikariConfig);

        // ComboPooledDataSource dataSource = new ComboPooledDataSource();
        // try {
        // dataSource.setDriverClass(properties.getClassName());
        // dataSource.setJdbcUrl(properties.getDbUrl());
        // dataSource.setUser(properties.getDbUserName());
        // dataSource.setPassword(properties.getDbPassword());
        // dataSource.setMaxPoolSize(properties.getMaxPoolSize());
        // dataSource.setMinPoolSize(properties.getMinPoolSize());
        // dataSource.setMaxStatements(properties.getMaxStatements());
        // dataSource.setTestConnectionOnCheckin(true);
        // dataSource.setTestConnectionOnCheckout(true);
        // log.info("dataSource connection is valid");
        // } catch (Exception e) {
        // log.error("dataSource connection is fail", e);
        // }
        // return dataSource;
    }

    @Bean
    public Map<String, Object> jpaProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put("hibernate.dialect", properties.getDialect());
        if (properties.isShow_sql()) {
            props.put("hibernate.show_sql", true);
        }

        if (properties.isFormat_sql()) {
            props.put("hibernate.format_sql", true);
        }

        return props;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setPackagesToScan(
                "com.cy.work.connect.vo",
                "com.cy.commons.vo",
                "com.cy.system.vo",
                "com.cy.work.backend.vo");

        lef.setDataSource(sqlDataSource());
        lef.setJpaDialect(new HibernateJpaDialect());
        lef.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        lef.setJpaPropertyMap(this.jpaProperties());
        return lef;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory().getObject());
    }

    @Bean
    public InstanceStatusConverter instanceStatusConverter() {
        return new InstanceStatusConverter();
    }
}
