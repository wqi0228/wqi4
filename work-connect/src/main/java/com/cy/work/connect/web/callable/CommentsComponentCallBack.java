/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.callable;

import java.io.Serializable;

/**
 * @author brain0925_liao
 */
public class CommentsComponentCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2115543146987255645L;

    /**
     * 訊息
     */
    public void showMessage(String message) {
    }

    /**
     * 執行 確定 按鈕
     */
    public void submit() {
    }

    /**
     * 執行 取消 按鈕
     */
    public void cancel() {
    }
}
