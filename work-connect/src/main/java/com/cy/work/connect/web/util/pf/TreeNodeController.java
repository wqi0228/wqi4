package com.cy.work.connect.web.util.pf;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.web.util.SpringContextHolder;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 提供 TreeNode 相關方法
 *
 * @author jimmy_chou
 */
@Slf4j
@Component
public class TreeNodeController implements Serializable, InitializingBean {

    /**
     *
     */
    private static final long serialVersionUID = 1348588507631476110L;

    private static TreeNodeController instance;

    @Autowired
    private WorkSettingOrgManager orgManager;

    public static TreeNodeController getInstance() {
        return instance;
    }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(TreeNodeController instance) {
        TreeNodeController.instance = instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
        if (this.orgManager == null) {
            this.orgManager = SpringContextHolder.getBean(WorkSettingOrgManager.class);
        }
    }

    /**
     * 將所有樹狀節點加入list中
     *
     * @param list
     * @param root
     */
    public void allNode(List<TreeNode> list, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren()
            .forEach(
                each -> {
                    list.add(each);
                    this.allNode(list, each);
                });
    }

    /**
     * 展開該節點下的所有節點
     *
     * @param root
     * @param isExpand : 設定為 true 則展開全部，反之則收起全部
     */
    public void expandAll(TreeNode root, boolean isExpand) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren()
            .forEach(
                each -> {
                    each.setExpanded(isExpand);
                    this.expandAll(each, isExpand);
                });
    }

    /**
     * 展開該子節點上層的父節點，若該父節點還有父節點也展開，直到根節點。
     *
     * @param children
     */
    public void expandForChildren(TreeNode children) {
        if (children.getParent() != null) {
            children.getParent().setExpanded(true);
            this.expandForChildren(children.getParent());
        }
    }

    /**
     * 傳回第一筆符合資料值的 TreeNode
     *
     * @param treeNode
     * @param targetNodeData
     * @return
     */
    public TreeNode getNodeByData(TreeNode treeNode, Object targetNodeData) {

        if (treeNode == null) {
            return null;
        }

        // ====================================
        // 比對此節點是否需選取
        // ====================================
        // 取得傳入節點資料
        Object treeNodeData = treeNode.getData();
        // 比對目標節點資料是否和傳入節點等值
        if ((treeNodeData == null && treeNode.getData() == null)
            || (treeNodeData != null && treeNodeData.equals(targetNodeData))) {
            return treeNode;
        }

        // ====================================
        // 遞迴比對子節點
        // ====================================
        // 無子節點不處理
        if (treeNode.getChildren() == null) {
            return null;
        }
        // 遞迴處理每個子節點
        for (TreeNode childTreeNode : treeNode.getChildren()) {
            TreeNode resultNode = getNodeByData(childTreeNode, targetNodeData);
            if (resultNode != null) {
                return resultNode;
            }
        }

        return null;
    }

    /**
     * 依據傳入值選取節點
     *
     * @param treeNode
     * @param targetNodeData
     */
    public void selectNode(TreeNode treeNode, Object targetNodeData) {

        if (treeNode == null) {
            return;
        }

        // ====================================
        // 比對此節點是否需選取
        // ====================================
        // 取得傳入節點資料
        Object treeNodeData = treeNode.getData();
        // 比對目標節點資料是否和傳入節點等值
        if ((treeNodeData == null && treeNode.getData() == null)
            || (treeNodeData != null && treeNodeData.equals(targetNodeData))) {
            treeNode.setSelected(true);
        }

        // ====================================
        // 遞迴處理子節點
        // ====================================
        // 無子節點不處理
        if (treeNode.getChildren() == null) {
            return;
        }
        // 遞迴處理每個子節點
        for (TreeNode childTreeNode : treeNode.getChildren()) {
            selectNode(childTreeNode, targetNodeData);
        }
    }

    /**
     * 取得單位樹
     *
     * @param rootOrgSid
     * @return
     */
    public TreeNode getOrgTree(Integer rootOrgSid) {

        // 根節點為公司別
        TreeNode rootOrgNode = new DefaultTreeNode(rootOrgSid);

        // 遞迴建立子單位樹
        this.RecursiveBuildOrgTree(rootOrgNode, orgManager.findAllSid());

        return rootOrgNode;
    }

    /**
     * 遞迴建立子單位樹
     *
     * @param orgNode    父節點
     * @param allOrgSids 所有單位SID
     */
    private void RecursiveBuildOrgTree(TreeNode orgNode, List<Integer> allOrgSids) {

        if (orgNode == null) {
            return;
        }

        // ====================================
        // 取得傳入單位 的子部門 （僅下一層）
        // ====================================
        // 父節點單位SID
        Integer orgSid = (Integer) orgNode.getData();
        // 從所有單位List中, 取出父單位的下一層子單位
        List<Integer> childOrgSids =
            allOrgSids.stream()
                .filter(eachOrgSid -> orgManager.compareParent(eachOrgSid, orgSid))
                .collect(Collectors.toList());

        if (childOrgSids == null || childOrgSids.isEmpty()) {
            return;
        }

        // ====================================
        // 建立子部門 tree
        // ====================================
        // 將子部門, 依附於父節點
        childOrgSids.forEach(
            eachChildOrgSid -> {

                // 建立此單位節點
                TreeNode currOrgNode = new DefaultTreeNode(eachChildOrgSid, orgNode);

                // 遞迴繼續處理此子部門的下一層部門
                this.RecursiveBuildOrgTree(currOrgNode, allOrgSids);
            });
    }

    /**
     * 將不存在列表中的單位刪除
     *
     * @param rootOrgTreeNode
     * @param targetOrgSids
     * @return
     */
    public TreeNode OrgTreefilter(TreeNode rootOrgTreeNode, List<Integer> targetOrgSids) {

        if (targetOrgSids == null) {
            targetOrgSids = Lists.newArrayList();
        }

        try {

            // 將TreeNode 轉為 list
            List<TreeNode> allNodelist = Lists.newArrayList();
            this.allNode(allNodelist, rootOrgTreeNode);

            // ====================================
            //
            // ====================================
            // 檢查：所有單位節點
            for (TreeNode orgNode : allNodelist) {

                // -------------------
                // 防呆
                // -------------------
                if (orgNode == null || orgNode.getData() == null) {
                    continue;
                }

                // -------------------
                // 過濾單位節點
                // -------------------
                // 檢核：直系單位節點，無任何一個名稱有符合的關鍵字
                if (!isInOrgList(orgNode, targetOrgSids)) {

                    // 無關鍵字時,從父類別的子項清單移除
                    TreeNode parentOrgNode = orgNode.getParent();
                    if (parentOrgNode != null && WkStringUtils.notEmpty(
                        parentOrgNode.getChildren())) {
                        parentOrgNode.getChildren().remove(orgNode);
                    }

                    continue;
                }

                // -------------------
                // 展開節點
                // -------------------
                this.expandForChildren(orgNode);
            }

            return rootOrgTreeNode;

        } catch (Exception e) {
            log.error("建置組織樹失敗!!", e);
            throw e;
        }
    }

    private boolean isInOrgList(TreeNode orgNode, List<Integer> orgSids) {

        if (orgNode == null || orgNode.getData() == null) {
            return false;
        }

        Integer currOrgSids = (Integer) orgNode.getData();

        // 檢核此節點是否包含在要保留的部門中
        if (orgSids.contains(currOrgSids)) {
            return true;
        }

        // 檢核子節點
        if (WkStringUtils.notEmpty(orgNode.getChildren())) {
            for (TreeNode childOrgNode : orgNode.getChildren()) {
                // 遞迴檢查子系
                if (this.isInOrgList(childOrgNode, orgSids)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param orgNode
     * @param targetNode
     */
    public void cloneTreeNode(TreeNode orgNode, TreeNode targetNode) {
        if (orgNode.getChildCount() == 0) {
            return;
        }
        for (TreeNode orgChildrenNode : orgNode.getChildren()) {
            TreeNode targetChildrenNode = new DefaultTreeNode(orgChildrenNode.getData(),
                targetNode);
            this.cloneTreeNode(orgChildrenNode, targetChildrenNode);
        }
    }
}
