/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.util.pf;

import java.io.Serializable;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.omnifaces.util.Faces;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class DisplayController implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2250653696738495858L;

    private static DisplayController instance;

    public static DisplayController getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        DisplayController.instance = this;
    }

    /**
     * 顯現某個具有 widgetVar 的 PF 元件 (PF5.0)
     *
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void showPfWidgetVar(String widgetVar) {
        // PrimeFaces.current().executeScript("PF('" + widgetVar + "').show()");
        PrimeFaces.current().executeScript("showPF('" + widgetVar + "');");
    }

    /**
     * 隱藏某個具有 widgetVar 的 PF 元件 (PF5.0)
     *
     * @param widgetVar : PF 元件 widgetVar 名稱
     */
    public void hidePfWidgetVar(String widgetVar) {
        // PrimeFaces.current().executeScript("PF('" + widgetVar + "').hide()");
        PrimeFaces.current().executeScript("hidePF('" + widgetVar + "');");
    }

    /**
     * update javascript from beans.
     *
     * @param id : PF 元件 ID
     * @author steve_chen
     */
    public void update(String id) {
        PrimeFaces.current().ajax().update(id);
    }

    /**
     * 清除某個具有 widgetVar 及 filter 功能的 PF 元件的 filter
     *
     * @param widgetVar
     */
    public void clearFilter(String widgetVar) {
        PrimeFaces.current().executeScript("PF('" + widgetVar + "').clearFilters();");
    }

    /**
     * 執行某個具有 widgetVar 及 filter 功能的 PF 元件的 filter
     *
     * @param widgetVar
     */
    public void doFilter(String widgetVar) {
        PrimeFaces.current().executeScript("PF('" + widgetVar + "').filter();");
    }

    /**
     * Execute javascript from beans.
     *
     * @param script : 要執行的 javascript
     */
    public void execute(String script) {
        PrimeFaces.current().executeScript(script);
    }

    /**
     * reset datatable 一併清除datatable filter
     *
     * @param tableId
     */
    public void resetDataTable(String tableId) {
        DataTable table = (DataTable) Faces.getViewRoot().findComponent("formTemplate:" + tableId);
        if (table != null) {
            // reset table state
            ValueExpression ve = table.getValueExpression("sortBy");
            if (ve != null) {
                table.setValueExpression("sortBy", null);
            }

            ve = table.getValueExpression("filterBy");
            if (ve != null) {
                table.setValueExpression("filterBy", null);
            }

            ve = table.getValueExpression("filteredValue");
            if (ve != null) {
                table.setValueExpression("filteredValue", null);
            }

            table.reset();
        }
    }

    /**
     * 取得FacesContext
     *
     * @return
     */
    public FacesContext getContext() { return FacesContext.getCurrentInstance(); }

    /**
     * 取得元件
     *
     * @param key
     * @return
     */
    public UIComponent findComponent(String key) {
        UIComponent uIComponent = this.getContext().getViewRoot().findComponent(key);
        return uIComponent;
    }
}
