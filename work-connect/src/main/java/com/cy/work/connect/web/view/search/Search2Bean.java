package com.cy.work.connect.web.view.search;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.omnifaces.util.Faces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.FilterMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.WaitReceiveLogic;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.WaitWorkReceiveListColumn;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.TableUpDownListener;
import com.cy.work.connect.web.logic.components.search.WaitWorkReceiveListLogicComponent;
import com.cy.work.connect.web.util.pf.ConfirmCallbackDialogController;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.TableUpDownBean;
import com.cy.work.connect.web.view.components.CustomColumnComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkReportSidTo;
import com.cy.work.connect.web.view.vo.column.search.WaitWorkReceiveListColumnVO;
import com.cy.work.connect.web.view.vo.search.WaitWorkReceiveListVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 待領單據清單
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search2Bean implements Serializable, TableUpDownListener {

    /**
     *
     */
    private static final long serialVersionUID = 4597595422370721940L;

    @Getter
    /** dataTable Id */
    private final String dataTableID = "dataTableWorkReport";

    @Getter
    /** dataTable widgetVar */
    private final String dataTableWv = "dataTableWorkReportWv";

    @Autowired
    private ConfirmCallbackDialogController confirmCallbackDialogController;
    @Autowired
    private WaitReceiveLogic waitReceiveLogic;

    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Autowired
    private WaitWorkReceiveListLogicComponent searchLogicComponent;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private WkOrgCache wkOrgCache;
    @Autowired
    private WCMasterManager wcMasterManager;

    @Getter
    /** 登入者 */
    private UserViewVO userViewVO;

    @Getter
    /** 登入單位 */
    private OrgViewVo depViewVo;

    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;
    /**
     * 可查詢單位
     */
    private List<Integer> depSids;

    @Getter
    /** view 顯示錯誤訊息 */
    private String errorMessage;

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /** */
        private static final long serialVersionUID = -7124854849920326853L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confimDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
        }
    };

    @Setter
    @Getter
    /** 是否版面切換(顯示Frame畫面) */
    private boolean showFrame = false;

    @Getter
    /** dataTable 欄位資訊 */
    private WaitWorkReceiveListColumnVO columnVO;

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /** */
        private static final long serialVersionUID = -6568198607487809548L;

        @Override
        public void reload() {
            columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        }
    };

    @Setter
    @Getter
    /** 列表資料 */
    private List<WaitWorkReceiveListVO> allVos;

    private List<WaitWorkReceiveListVO> filterdDatas;

    @Setter
    @Getter
    /** 列表選取資料 */
    private List<WaitWorkReceiveListVO> selVos;

    /**
     * 暫存資料
     */
    private List<WaitWorkReceiveListVO> tempVOs;
    /**
     * 暫存資料
     */
    private String tempSelSid;

    @Getter
    /** frame 連結路徑 */
    private String iframeUrl = "";

    @Getter
    /** 單據名稱 選項 */
    private List<SelectItem> menuTagItems;

    @Getter
    private boolean disableBatchRecevice = true;
    @Getter
    private CustomColumnComponent customColumn;

    @PostConstruct
    public void init() {
        this.initLogin();
        this.initComponents();
        filterdDatas = Lists.newArrayList();
        selVos = Lists.newArrayList();
        this.doSearch();
    }

    /**
     * 初始化 登入者資訊
     */
    private void initLogin() {
        this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        try {
            depSids = wkOrgCache.findAllParent(depViewVo.getSid()).stream()
                    .map(each -> each.getSid())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.warn("initLogin ERROR", e);
        }
        depSids.add(depViewVo.getSid());
    }

    /**
     * 初始化 元件
     */
    private void initComponents() {
        columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(
                WCReportCustomColumnUrlType.WAIT_RECEIVE_WORK_LIST,
                Arrays.asList(WaitWorkReceiveListColumn.values()),
                columnVO.getPageCount(),
                userViewVO.getSid(),
                messageCallBack,
                dataTableReLoadCallBack);
    }

    /**
     * 執行查詢
     */
    public void doSearch() {
        try {
            allVos = this.search("");
            if (allVos.size() > 0) {
                DisplayController.getInstance()
                        .execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        } catch (Exception e) {
            log.warn("doSearch", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行查詢
     *
     * @param sid
     * @return
     */
    private List<WaitWorkReceiveListVO> search(String sid) {
        // 查詢待領單據
        List<WaitWorkReceiveListVO> results = searchLogicComponent.search(userViewVO.getSid(), sid);
        // 收單據名稱
        this.menuTagItems = results.stream()
                .map(each -> each.getMenuTagName())
                .distinct()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        return results;
    }

    /**
     * 回到列表查詢
     */
    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.warn("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行上一筆
     */
    @Override
    public void openerByBtnUp() {
        try {
            if (!allVos.isEmpty()) {
                this.toUp();
                if (selVos != null) {
                    this.settingPreviousAndNextAndReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("工作聯絡單進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行上一筆
     */
    private void toUp() {
    }

    /**
     * 執行下一筆
     */
    @Override
    public void openerByBtnDown() {
    }

    /**
     * 執行下一筆
     */
    @SuppressWarnings("unused")
    private void toDown() {
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNextAndReaded() {
    }

    /**
     * 傳遞上下筆資訊
     */
    @SuppressWarnings("unused")
    private void settingPreviousAndNext() {
    }

    /**
     * 判斷是否有暫存資訊
     */
    @SuppressWarnings("unused")
    private boolean checkTempVOs() {
        return tempVOs != null && !tempVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid);
    }

    /**
     * 變更 selVo
     */
    @SuppressWarnings("unused")
    private void settintSelVo(String sid) {
    }

    /**
     * ?????
     */
    public void reloadDataByUpdate() {
        this.doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByFavorite() {
    }

    /**
     * ?????
     */
    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.warn("doReloadInfo", ex);
        }
        // try {
        // allVos = this.search("");
        // DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        // } catch (Exception e) {
        // log.error("doReloadInfo", e);
        // messageCallBack.showMessage(e.getMessage());
        // }
    }

    /**
     * ?????
     *
     * @param sid
     */
    private void updateInfoToDataTable(String sid) {
        try {
            List<WaitWorkReceiveListVO> result = search(sid);
            if (result == null || result.isEmpty()) {
                tempVOs = Lists.newArrayList();
                if (allVos != null && !allVos.isEmpty()) {
                    allVos.forEach(
                            item -> {
                                tempVOs.add(item);
                            });
                    allVos.remove(new WaitWorkReceiveListVO(sid));
                    tempSelSid = sid;
                }
            } else {
                WaitWorkReceiveListVO updateDeail = result.get(0);
                allVos.forEach(
                        item -> {
                            if (item.getWcSid().equals(updateDeail.getWcSid())) {
                                item.replaceValue(updateDeail);
                            }
                        });
            }
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        } catch (Exception e) {
            log.warn("updateInfoToDataTable Error", e);
        }
    }

    /**
     * 清除暫存
     */
    @SuppressWarnings("unused")
    private void clearTemp() {
        tempSelSid = "";
        tempVOs = null;
    }

    /**
     * 取得暫存索引
     */
    @SuppressWarnings("unused")
    private int getTempIndex() { return tempVOs.indexOf(new WaitWorkReceiveListVO(tempSelSid)); }

    public void doSelectRow() {
        disableBatchRecevice = selVos == null || selVos.isEmpty();
    }

    public Map<String, FilterMeta> onFilter(AjaxBehaviorEvent event) {
        DataTable table = (DataTable) event.getSource();
        Map<String, FilterMeta> filters = table.getFilterBy();
        try {
            selVos.clear();
            filterdDatas.clear();
            disableBatchRecevice = true;
            allVos.forEach(
                    item -> {
                        if (filters.get("requireDepName") != null) {
                            String filterValue = String.valueOf(filters.get("requireDepName"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getRequireDepName().toLowerCase()
                                            .contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        if (filters.get("requireUserName") != null) {
                            String filterValue = String.valueOf(filters.get("requireUserName"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getRequireUserName().toLowerCase()
                                            .contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        if (filters.get("menuTagName") != null) {
                            String filterValue = String.valueOf(filters.get("menuTagName"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getMenuTagName().equalsIgnoreCase(filterValue)) {
                                return;
                            }
                        }
                        if (filters.get("theme") != null) {
                            String filterValue = String.valueOf(filters.get("theme"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getTheme().toLowerCase().contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        if (filters.get("wcNo") != null) {
                            String filterValue = String.valueOf(filters.get("wcNo"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getWcNo().toLowerCase().contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        filterdDatas.add(item);
                    });
        } catch (Exception e) {
            log.warn("onFilter ERROR", e);
        }
        return filters;
    }

    // ========================================================================
    // 領單
    // ========================================================================
    // 選擇要領單的 wc sid
    private String selectedWcSid = "";

    /**
     * 開啟領單確認視窗
     * 
     * @param wcSid
     */
    public void openReceiveDialog(String wcSid) {
        // 記錄選擇的單據
        this.selectedWcSid = wcSid;
        // 跳出確認視窗
        this.confirmCallbackDialogController.showConfimDialog(
                "此單據的執行人員將變更為您,請確認!",
                Lists.newArrayList(),
                () -> confirmReceive());

    }

    /**
     * 確認領單
     */
    public void confirmReceive() {

        // ====================================
        // 執行領單
        // ====================================
        try {
            this.waitReceiveLogic.doReceive(this.selectedWcSid, SecurityFacade.getUserSid());
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "(" + e.getMessage() + ")";
            log.error(message, e);
            MessagesUtils.showError(message);
            return;
        }
        
        // ====================================
        // 畫面控制重整畫面
        // ====================================
        this.onReload();

        // ====================================
        // 畫面控制 - 執行換頁
        // ====================================
        DisplayController.getInstance()
                .execute("openInNewTab('../worp/worp_full.xhtml?wrcId=" + this.selectedWcSid + "');");
    }

    // ========================================================================
    // 批次領單
    // ========================================================================
    /**
     * 批次領單選擇的項目
     */
    private List<String> selectedWcSids = Lists.newArrayList();;

    /**
     * 開啟批次領單確認視窗
     * 
     * @param wcSid
     */
    public void openBatchReceiveDialog() {

        if (WkStringUtils.isEmpty(this.selVos)) {
            MessagesUtils.showWarn("請挑選領單單據!");
            return;
        }

        // 收集選擇的單據
        this.selectedWcSids = this.selVos.stream()
                .map(WaitWorkReceiveListVO::getWcSid)
                .collect(Collectors.toList());

        // 跳出確認視窗
        this.confirmCallbackDialogController.showConfimDialog(
                "共選擇" + this.selectedWcSids.size() + "單據，單據的執行人員將變更為您，請確認!",
                Lists.newArrayList(),
                () -> confirmBatchReceive());

    }

    /**
     * 確認執行批次領單
     */
    public void confirmBatchReceive() {

        // ====================================
        // 執行領單
        // ====================================
        // 收集執行成功訊息
        List<String> seccessMessages = Lists.newArrayList();
        // 收集執行失敗訊息
        List<String> failMessages = Lists.newArrayList();

        // 逐筆處理
        for (String wcSid : this.selectedWcSids) {

            // 查詢主單
            WCMaster wcMaster = this.wcMasterManager.findBySid(wcSid);
            if (wcMaster == null) {
                failMessages.add("[" + wcSid + "] 找不到單據資料，無法領單");
                continue;
            }

            // 執行領單
            try {
                this.waitReceiveLogic.doReceive(wcSid, SecurityFacade.getUserSid());
                seccessMessages.add("[" + wcMaster.getWc_no() + "] " + wcMaster.getTheme());
            } catch (UserMessageException e) {
                failMessages.add("[" + wcMaster.getWc_no() + "] " + e.getMessage());
                continue;
            } catch (Exception e) {
                String message = WkMessage.PROCESS_FAILED + "(" + e.getMessage() + ")";
                log.error(message, e);
                failMessages.add("[" + wcMaster.getWc_no() + "] " + message);
                continue;
            }

        }

        // ====================================
        // 處理結果訊息
        // ====================================
        String message = "";
        if (WkStringUtils.notEmpty(seccessMessages)) {
            message += "下列單據已完成領單：<br/>";
            message += String.join("<br/>", seccessMessages);
        }

        if (WkStringUtils.notEmpty(failMessages)) {
            if (WkStringUtils.notEmpty(message)) {
                message += "<br/><br/>";
            }
            message += WkHtmlUtils.addRedBlodClass("以下單據執行失敗：<br/>" + String.join("<br/>", failMessages));
        }

        log.info("\r\n執行批次領單\r\n" + WkJsoupUtils.getInstance().htmlToText(message));
        MessagesUtils.showInfo(message);

        // ====================================
        // 畫面控制重整畫面
        // ====================================
        this.onReload();
    }

    public void onReload() {
        try {
            this.allVos = search("");
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        } catch (Exception e) {
            messageCallBack.showMessage(e.getMessage());
            log.warn("onReload ERROR", e);
        }
    }

}
