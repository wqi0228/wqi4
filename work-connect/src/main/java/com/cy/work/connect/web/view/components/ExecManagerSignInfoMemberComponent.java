/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.model.DualListModel;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.web.listener.SignInfoMemberCallBack;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.logic.components.WCOrgLogic;
import com.cy.work.connect.web.logic.components.WCUserLogic;
import com.cy.work.connect.web.util.SpringContextHolder;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class ExecManagerSignInfoMemberComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5710110928798644568L;
    private final Integer loginUserSid;
    private final TabCallBack workReportTabCallBack;
    @Getter
    private final boolean showReadReceiptTab = false;
    @Getter
    private final SignInfoMemberCallBack signInfoMemberCallBack;
    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;
    private LinkedHashSet<UserViewVO> leftViewVO;
    private String wc_Id;
    private String wc_no;
    @Getter
    @Setter
    private String filterNameValue;
    private Integer compSid;
    // private Integer createUser;
    private LinkedHashSet<UserViewVO> allCompUser;

    public ExecManagerSignInfoMemberComponent(
        Integer loginUserSid,
        TabCallBack workReportTabCallBack,
        SignInfoMemberCallBack signInfoMemberCallBack) {
        this.loginUserSid = loginUserSid;
        this.workReportTabCallBack = workReportTabCallBack;
        this.signInfoMemberCallBack = signInfoMemberCallBack;
        pickUsers = new DualListModel<>();
    }

    public void loadWCID(String wc_Id, String wc_no) {
        this.wc_Id = wc_Id;
        this.wc_no = wc_no;
    }

    public void loadUserPicker(String wc_Id) {
        this.filterNameValue = "";
        this.wc_Id = wc_Id;
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        this.compSid = user.getPrimaryOrg().getCompanySid();
        allCompUser = Sets.newLinkedHashSet();
        try {
            WkOrgCache.getInstance().findAllDepByCompSid(this.compSid).stream()
                .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList())
                .forEach(
                    item -> {
                        allCompUser.addAll(
                            WCUserLogic.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.warn(" WkOrgCache.getInstance().findAllDepByCompSid", e);
        }

        Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        List<Org> allChildOrg =
            WkOrgCache.getInstance().findAllChild(baseOrg.getSid()).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());
        leftViewVO = Sets.newLinkedHashSet();
        leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(baseOrg.getSid()));

        allChildOrg.forEach(
            item -> {
                leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });

        List<UserViewVO> rightViewVO = Lists.newArrayList();
        List<SingleManagerSignInfo> counterSignInfos =
            WCExecManagerSignInfoManager.getInstance().getCounterSignInfos(wc_Id);
        counterSignInfos.forEach(
            item -> {
                User tempUser = WkUserCache.getInstance()
                    .findBySid(Integer.valueOf(item.getUserSid()));
                Org dep = WkOrgCache.getInstance().findBySid(tempUser.getPrimaryOrg().getSid());
                UserViewVO uv =
                    new UserViewVO(
                        tempUser.getSid(),
                        tempUser.getName(),
                        WkOrgUtils.prepareBreadcrumbsByDepName(
                            dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-"));
                if (!item.getStatus().equals(BpmStatus.NEW_INSTANCE)) {
                    uv.setMoveAble(false);
                }
                rightViewVO.add(uv);
            });
        pickUsers.setSource(removeTargetUserView(Lists.newArrayList(leftViewVO), rightViewVO));
        pickUsers.setTarget(rightViewVO);
    }

    public void loadSelectDepartment(List<Org> departments) {
        settingTargetSigned();
        if (departments == null || departments.size() == 0) {
            Preconditions.checkState(false, "請挑選部門!!");
            return;
        }
        // leftViewVO.clear();

        List<UserViewVO> selectDepUserView = Lists.newArrayList();
        allCompUser.forEach(
            item -> {
                User u = WkUserCache.getInstance().findBySid(item.getSid());
                Org org = WkOrgCache.getInstance().findBySid(u.getPrimaryOrg().getSid());
                if (departments.contains(org)) {
                    selectDepUserView.add(item);
                }
            });
        pickUsers.setSource(removeTargetUserView(selectDepUserView, pickUsers.getTarget()));
    }

    public void filterName() {
        settingTargetSigned();
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(
                removeTargetUserView(Lists.newArrayList(allCompUser), pickUsers.getTarget()));
        } else {
            pickUsers.setSource(
                removeTargetUserView(
                    allCompUser.stream()
                        .filter(
                            each -> each.getName().toLowerCase()
                                .contains(filterNameValue.toLowerCase()))
                        .collect(Collectors.toList()),
                    pickUsers.getTarget()));
        }
        DisplayController.getInstance().update("execCounterSignPanel");
    }

    private void settingTargetSigned() {
        List<SingleManagerSignInfo> counterSignInfos =
            WCExecManagerSignInfoManager.getInstance().getCounterSignInfos(wc_Id);
        // 過濾出已簽核人員
        List<SingleManagerSignInfo> counterSignInfosSigned =
            counterSignInfos.stream()
                .filter(each -> !each.getStatus().equals(BpmStatus.NEW_INSTANCE))
                .collect(Collectors.toList());
        if (pickUsers.getTarget() != null && pickUsers.getTarget().size() > 0) {
            pickUsers
                .getTarget()
                .forEach(
                    item -> {
                        List<SingleManagerSignInfo> sameSingleManagerSignInfoTo =
                            counterSignInfosSigned.stream()
                                .filter(each -> each.getUserSid().equals(item.getSid()))
                                .collect(Collectors.toList());
                        if (sameSingleManagerSignInfoTo != null
                            && sameSingleManagerSignInfoTo.size() > 0) {
                            item.setMoveAble(false);
                        }
                    });
        }
    }

    public void saveReqCounterSignUser() {
        try {
            List<SingleManagerSignInfo> oldCounterSignInfos =
                WCExecManagerSignInfoManager.getInstance().getCounterSignInfos(wc_Id);
            List<SingleManagerSignInfo> canRemoveOld = Lists.newArrayList();
            oldCounterSignInfos.forEach(
                item -> {
                    List<UserViewVO> canRemove =
                        allCompUser.stream()
                            .filter(each -> each.getSid().equals(item.getUserSid()))
                            .collect(Collectors.toList());
                    if (canRemove != null && !canRemove.isEmpty()) {
                        canRemoveOld.add(item);
                    }
                });

            List<UserViewVO> canSelectPerson =
                pickUsers.getTarget().stream()
                    .filter(each -> allCompUser.contains(each))
                    .collect(Collectors.toList());

            if ((canRemoveOld == null || canRemoveOld.isEmpty())
                && (canSelectPerson == null || canSelectPerson.isEmpty())) {
                Preconditions.checkState(false, "尚未挑選任何資訊,請確認！");
            }

            // 過濾出已簽核人員
            List<SingleManagerSignInfo> counterSignInfosSigned =
                canRemoveOld.stream()
                    .filter(each -> !each.getStatus().equals(BpmStatus.NEW_INSTANCE))
                    .collect(Collectors.toList());

            // 檢測是否有移除已簽名的人員
            StringBuilder moveErrorMessage = new StringBuilder();
            counterSignInfosSigned.forEach(
                item -> {
                    List<UserViewVO> signUser =
                        canSelectPerson.stream()
                            .filter(each -> each.getSid().equals(item.getUserSid()))
                            .collect(Collectors.toList());
                    if (signUser == null || signUser.isEmpty()) {
                        if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                            moveErrorMessage.append("\n");
                        }
                        User movedUser = WkUserCache.getInstance().findBySid(item.getUserSid());
                        moveErrorMessage.append(
                            "成員["
                                + movedUser.getPrimaryOrg().getName()
                                + "-"
                                + movedUser.getName()
                                + "]已簽名,不可移除");
                    }
                });
            if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                Preconditions.checkState(false, moveErrorMessage.toString());
            }

            // 檢測是否有需要移除作廢的流程
            List<SingleManagerSignInfo> removeSingleManagerSignInfos = Lists.newArrayList();
            canRemoveOld.forEach(
                item -> {
                    List<UserViewVO> selUsers =
                        canSelectPerson.stream()
                            .filter(each -> each.getSid().equals(item.getUserSid()))
                            .collect(Collectors.toList());
                    if (selUsers == null || selUsers.isEmpty()) {
                        removeSingleManagerSignInfos.add(item);
                    }
                });

            // 檢測是否有需要新增的流程
            List<Integer> addUserFlowSid = Lists.newArrayList();
            canSelectPerson.forEach(
                item -> {
                    List<SingleManagerSignInfo> createedSingleManagerSignInfo =
                        canRemoveOld.stream()
                            .filter(each -> item.getSid().equals(each.getUserSid()))
                            .collect(Collectors.toList());
                    if (createedSingleManagerSignInfo == null
                        || createedSingleManagerSignInfo.isEmpty()) {
                        addUserFlowSid.add(item.getSid());
                    }
                });
            WCExecManagerSignInfoManager.getInstance()
                .saveCounterSignInfo(
                    wc_Id, wc_no, removeSingleManagerSignInfos, addUserFlowSid, loginUserSid);
            workReportTabCallBack.reloadExecBpmTab();
            workReportTabCallBack.reloadWCMasterData();
            DisplayController displayController = SpringContextHolder.getBean(
                DisplayController.class);
            displayController.hidePfWidgetVar("execCounterSignDlg");

        } catch (Exception e) {
            workReportTabCallBack.reloadExecBpmTab();
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        } finally {
            DisplayController.getInstance().execute("doReloadDataToUpdate('" + wc_Id + "')");
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source,
        List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
            item -> {
                // 未挑選不可以包含已挑選的人員及自己本身
                if (!target.contains(item) && !loginUserSid.equals(item.getSid())) {
                    filterSource.add(item);
                }
            });
        return filterSource;
    }
}
