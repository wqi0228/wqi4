/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.listener.ReLoadCallBack;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.logic.components.WCOrgLogic;
import com.cy.work.connect.web.logic.components.WCMasterLogicComponents;
import com.cy.work.connect.web.logic.components.WCReadLogicComponents;
import com.cy.work.connect.web.logic.components.WCUserLogic;
import com.cy.work.connect.web.logic.components.WorkGroupSetupPrivateLogicComponents;
import com.cy.work.connect.web.util.pf.DataTablePickList;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.enumtype.TabType;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.ViewPersonVO;
import com.cy.work.connect.web.view.vo.WorkGroupVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 可閱人員元件
 *
 * @author brain0925_liao
 */
@Slf4j
public class ViewPersonComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7394421411553284671L;

    @Getter
    private final List<SelectItem> workGroupVOs;

    private final List<WorkGroupVO> realWorkGroupVOs;
    @Getter
    private final WorkGroupSetupPrivateComponent workGroupSetupPrivateComponent;
    private final Integer loginUserSid;
    private final ReLoadCallBack groupReloadCallBack = new ReLoadCallBack() {
        /** */
        private static final long serialVersionUID = 2781505964678801076L;

        @Override
        public void onReload() {
            loadGroupData();
        }
    };
    private final DepTreeComponent depTreeComponent;
    private final TabCallBack workReportTabCallBack;
    @Getter
    private final List<ViewPersonVO> viewPersonVO;
    @Setter
    @Getter
    private String selWorkGroupVOs;
    @Getter
    @Setter
    private String traceUserName;
    private LinkedHashSet<UserViewVO> leftViewVO;
    /**
     * DataTablePickList 物件
     */
    private DataTablePickList<UserViewVO> dataPickList;
    private LinkedHashSet<UserViewVO> allCompUser;
    private List<Org> selectOrgs;
    private String wc_Id;
    @Getter
    private boolean showViewPersonTab = false;

    public ViewPersonComponent(
            Integer loginUserSid, TabCallBack workReportTabCallBack) {
        this.loginUserSid = loginUserSid;
        this.workGroupSetupPrivateComponent = new WorkGroupSetupPrivateComponent(
                "pt_group_setup_private_setting_dlg_wv",
                "groupMaintainComponent_pickPanel",
                "workTraceGroup_deptUnitDlg",
                "workTraceGroup_dep",
                "pt_group_setup_private_dlg_panel_table_wv",
                "pt_group_setup_private_dlg_panel_table_id",
                loginUserSid,
                groupReloadCallBack);
        this.workReportTabCallBack = workReportTabCallBack;
        this.workGroupVOs = Lists.newArrayList();
        this.realWorkGroupVOs = Lists.newArrayList();
        initDataPickList();
        this.depTreeComponent = new DepTreeComponent();
        initOrgTree();
        this.viewPersonVO = Lists.newArrayList();
    }

    public void loadViewPersonData(String wcSid) {
        this.viewPersonVO.clear();
        this.viewPersonVO.addAll(WCMasterLogicComponents.getInstance().getViewPersonByWcSid(wcSid));
        showViewPersonTab = !this.viewPersonVO.isEmpty();
    }

    private void initDataPickList() {
        this.dataPickList = new DataTablePickList<>(UserViewVO.class);
    }

    private void initOrgTree() {
        Org group = new Org(1);
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    private void clear() {
        initDataPickList();
        DisplayController.getInstance().update("dlgTraceActionUsers_view_listView");
        initOrgTree();
        DisplayController.getInstance().update("dlgTraceActionUsers_dep");
        allCompUser.clear();
        realWorkGroupVOs.clear();
    }

    public void close() {
        clear();
        DisplayController.getInstance().hidePfWidgetVar("dlgViewUsers");
    }

    public void saveTrace() {

        try {
            if ((dataPickList.getTargetData() == null || dataPickList.getTargetData().isEmpty())
                    && WCMasterLogicComponents.getInstance().getViewPersonByWcSid(wc_Id).isEmpty()) {
                MessagesUtils.showWarn("您尚未選擇資料");
                return;
            }
            WCMasterLogicComponents.getInstance()
                    .saveViewUsers(wc_Id, dataPickList.getTargetData(), loginUserSid);
            WCReadLogicComponents.getInstance().readWCMaster(wc_Id, loginUserSid);
            clear();
            DisplayController.getInstance().hidePfWidgetVar("dlgViewUsers");
            workReportTabCallBack.reloadTraceTab();
            this.loadViewPersonData(wc_Id);
            if (showViewPersonTab) {
                workReportTabCallBack.reloadTabView(TabType.ViewPerson);
            } else {
                workReportTabCallBack.reloadTabView(TabType.Trace);
            }
            close();
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() { return depTreeComponent.getMultipleDepTreeManager(); }

    public void loadDepTree() {
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        Org group = new Org(user.getPrimaryOrg().getCompanySid());
        // 修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        List<Org> allOrg = WkOrgCache.getInstance().findAllDepByCompSid(group.getSid());
        depTreeComponent.init(
                allOrg,
                allOrg,
                group,
                selectOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    public void doSelectOrg() {
        try {
            selectOrgs = depTreeComponent.getSelectOrg();
            if (selectOrgs == null || selectOrgs.isEmpty()) {
                Preconditions.checkState(false, "請挑選部門!!");
            } else {
                LinkedHashSet<UserViewVO> selectDepUserView = Sets.newLinkedHashSet();
                selectOrgs.forEach(
                        item -> {
                            selectDepUserView.addAll(
                                    WCUserLogic.getInstance().findByDepSid(item.getSid()));
                        });
                dataPickList.setSourceData(
                        removeTargetUserView(
                                Lists.newArrayList(selectDepUserView), dataPickList.getTargetData()));
            }
            DisplayController.getInstance().update("dlgTraceActionUsers_view_listView");
            DisplayController.getInstance().hidePfWidgetVar("dlgTraceActionUsers_deptUnitDlg");

        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void filterName() {
        if (Strings.isNullOrEmpty(traceUserName)) {
            dataPickList.setSourceData(
                    removeTargetUserView(Lists.newArrayList(allCompUser),
                            dataPickList.getTargetData()));
        } else {
            dataPickList.setSourceData(
                    removeTargetUserView(
                            allCompUser.stream()
                                    .filter(
                                            each -> each.getName().toLowerCase()
                                                    .contains(traceUserName.toLowerCase()))
                                    .collect(Collectors.toList()),
                            dataPickList.getTargetData()));
        }
    }

    public void loadData(String wc_Id) {
        this.wc_Id = wc_Id;
        allCompUser = Sets.newLinkedHashSet();
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        Integer compSid = user.getPrimaryOrg().getCompanySid();
        try {
            WkOrgCache.getInstance().findAllDepByCompSid(compSid).stream()
                    .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                    .collect(Collectors.toList())
                    .forEach(
                            item -> {
                                allCompUser.addAll(
                                        WCUserLogic.getInstance().findByDepSid(item.getSid()));
                            });
        } catch (Exception e) {
            log.warn(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }
        leftViewVO = Sets.newLinkedHashSet();
        selectOrgs = Lists.newArrayList();
        Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        selectOrgs.add(baseOrg);
        List<Org> allChildOrg = WkOrgCache.getInstance().findAllChild(baseOrg.getSid()).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());
        selectOrgs.addAll(allChildOrg);
        leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(
                item -> {
                    leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(item.getSid()));
                });
        List<UserViewVO> right = WCMasterLogicComponents.getInstance().getViewUser(wc_Id);
        selWorkGroupVOs = "";
        traceUserName = "";
        dataPickList.setSourceData(removeTargetUserView(Lists.newArrayList(leftViewVO), right));
        dataPickList.setTargetData(right);
        loadGroupData();
        DisplayController.getInstance().update("dlgTraceActionUsers_view_listView");
    }

    public void changeGroupSelectItem() {
        if (!Strings.isNullOrEmpty(selWorkGroupVOs)) {
            if (realWorkGroupVOs != null) {
                List<WorkGroupVO> selWorkGroupVO = realWorkGroupVOs.stream()
                        .filter(each -> each.getGroup_sid().equals(selWorkGroupVOs))
                        .collect(Collectors.toList());
                if (selWorkGroupVO != null && !selWorkGroupVO.isEmpty()) {
                    dataPickList.setSourceData(
                            removeTargetUserView(
                                    selWorkGroupVO.get(0).getUserViewVO(), dataPickList.getTargetData()));
                }
            }
        } else {
            dataPickList.setSourceData(
                    removeTargetUserView(Lists.newArrayList(leftViewVO), dataPickList.getTargetData()));
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source,
            List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
                item -> {
                    if (!target.contains(item)) {
                        filterSource.add(item);
                    }
                });
        return filterSource;
    }

    public void loadGroupData() {
        workGroupVOs.clear();
        realWorkGroupVOs.clear();
        for (WorkGroupVO item : WorkGroupSetupPrivateLogicComponents.getInstance().getWorkGroupVO(loginUserSid)) {
            if (item.getStatusID().equals(Activation.ACTIVE.name())) {
                realWorkGroupVOs.add(item);
                SelectItem si = new SelectItem(item.getGroup_sid(), item.getGroup_name());
                workGroupVOs.add(si);
            }
        }
        DisplayController.getInstance().update("dlgTraceActionUsers_view_groupSetup");
    }

    public void toGroupManagerDialog() {
        this.workGroupSetupPrivateComponent.loadData();
    }

    public DataTablePickList<UserViewVO> getDataPickList() { return dataPickList; }
}
