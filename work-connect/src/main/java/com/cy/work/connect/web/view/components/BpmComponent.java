/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.commons.util.SpringContextHolder;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.PermissionLogicForExecDep;
import com.cy.work.connect.logic.helper.signflow.ExecFlowActionHelper;
import com.cy.work.connect.logic.helper.signflow.ReqFlowActionHelper;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.web.listener.BpmtCallBack;
import com.cy.work.connect.web.logic.components.BPMLogicComponent;
import com.cy.work.connect.web.logic.components.WCExecManagerSignInfoLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.vo.BpmVo;
import com.cy.work.connect.web.view.vo.ExecManagerInfoVO;
import com.cy.work.connect.web.view.vo.FlowNodeVO;
import com.cy.work.connect.web.view.vo.SignRole;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * BPM 控管元件
 *
 * @author kasim
 */
@Slf4j
@ManagedBean
public class BpmComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6239189835047781322L;

    @Getter
    /** BPM 按鈕監聽CallBack */
    private final BpmtCallBack bpmtCallBack;

    @Getter
    /** BPM 自訂物件 */
    private final BpmVo bpmVo;
    /**
     * 登入者
     */
    private final User user;
    @Getter
    private final List<ExecManagerInfoVO> execManagerInfoVO;
    /**
     * 簽核介面資訊List
     */
    @Getter
    private final List<SignRole> signRoles;
    /**
     * 需求流程物件
     */
    private SingleManagerSignInfo defaultSignInfo;
    @Getter
    private boolean showExecManagerTab;
    /**
     * 是否顯示執行單位簽名按鈕
     */
    @Getter
    private boolean showExecSignBtn;
    /**
     * 是否顯示執行單位復原按鈕
     */
    @Getter
    private boolean showExecRecoveryBtn;
    /**
     * 是否顯示執行單位退回按鈕
     */
    @Getter
    private boolean showExecRollBackBtn;
    /**
     * 是否顯示執行單位不執行按鈕
     */
    @Getter
    private boolean showExecStopBtn;
    /**
     * 是否顯示執行單位會簽按鈕
     */
    @Getter
    private boolean showExecAddSignBtn;
    /**
     * 是否顯示執行單位領單退回按鈕按鈕
     */
    @Getter
    private boolean showExecDepRollBackBtn;
    /**
     * 是否顯示執行單位領單不執行按鈕按鈕
     */
    @Getter
    private boolean showExecDepStopBtn;
    /**
     * 挑選的簽核介面資訊
     */
    @Getter
    @Setter
    private SignRole selSignRole;
    /**
     * 是否顯示意見說明按鈕
     */
    @Getter
    private boolean showBtnComponent = false;

    public BpmComponent(Integer loginUserSid, BpmtCallBack bpmtCallBack) {
        this.bpmtCallBack = bpmtCallBack;
        this.user = WkUserCache.getInstance().findBySid(loginUserSid);
        this.bpmVo = new BpmVo(user);
        this.defaultSignInfo = null;
        this.execManagerInfoVO = Lists.newArrayList();
        signRoles = Lists.newArrayList();
    }

    /**
     * 初始化單據簽核資料
     *
     * @param obj 主檔單據
     */
    public void loadData(WCMaster obj) {
        try {
            showBtnComponent = false;
            bpmVo.load(obj);
            this.bulidBpmList(obj);
        } catch (Exception e) {
            log.warn("loadData ERROR", e);
            this.bpmtCallBack.showMessage("載入簽核資訊失敗");
        }
    }

    /**
     * 執行單位簽名
     */
    // public void signExecInfo() {
    // try {
    // WCExecManagerSignInfoLogicComponent.getInstance()
    // .doSignBPMContinueSign(this.bpmVo.getSid(), user);
    // } catch (IllegalStateException | IllegalArgumentException e) {
    // log.warn("點擊 執行單位BPM簽名動作 發生錯誤!!：{}", e.getMessage());
    // bpmtCallBack.showMessage(e.getMessage());
    // } catch (Exception e) {
    // log.warn("點擊 執行單位BPM簽名動作 發生錯誤!!", e);
    // bpmtCallBack.showMessage(e.getMessage());
    // } finally {
    // try {
    // bpmtCallBack.reloadExecBPMTab();
    // DisplayController.getInstance()
    // .execute("doReloadDataToUpdate('" + this.bpmVo.getSid() + "')");
    // } catch (IllegalStateException | IllegalArgumentException e) {
    // log.warn("點擊 執行單位BPM簽名動作 發生錯誤!!：{}", e.getMessage());
    // bpmtCallBack.showMessage(e.getMessage());
    // }
    // }
    // }

    /**
     * 執行單位復原
     */
    // public void recoveryExecInfo() {
    // try {
    // WCExecManagerSignInfoLogicComponent.getInstance()
    // .doRecoveryBPMContinueSign(this.bpmVo.getSid(), user);
    // } catch (IllegalStateException | IllegalArgumentException e) {
    // log.warn("點擊 執行單位BPM復原動作 發生錯誤!!：{}", e.getMessage());
    // bpmtCallBack.showMessage(e.getMessage());
    // } catch (Exception e) {
    // log.warn("點擊 執行單位BPM復原動作 發生錯誤!!", e);
    // bpmtCallBack.showMessage(e.getMessage());
    // } finally {
    // try {
    // bpmtCallBack.reloadExecBPMTab();
    // DisplayController.getInstance()
    // .execute("doReloadDataToUpdate('" + this.bpmVo.getSid() + "')");
    // } catch (IllegalStateException | IllegalArgumentException e) {
    // log.warn("點擊 執行單位BPM復原動作 發生錯誤!!：{}", e.getMessage());
    // bpmtCallBack.showMessage(e.getMessage());
    // }
    // }
    // }

    /**
     * 建立 BPM 視圖
     *
     * @param signInfo
     */
    private void bulidBpmList(WCMaster obj) {

        try {
            Optional<WCManagerSignInfo> optional = WCManagerSignInfoManager.getInstance().findReqFlowSignInfoIncludeInvalid(obj.getSid());

            if (!optional.isPresent()) {
                log.warn("聯絡單 單號：" + bpmVo.getNo() + "，查無 BPM 簽核資訊 請確認!!");
                return;
            }

            this.defaultSignInfo = new SingleManagerSignInfo(optional.get());
        } catch (SystemOperationException e) {
            log.error(e.getMessage(), e);
        }

        this.loadTasksAndShowBtn(obj);
    }

    /**
     * 讀取水管圖 及 變更按鈕顯示
     */
    private void loadTasksAndShowBtn(WCMaster wcMaster) {
        List<FlowNodeVO> flowNodeVos = Lists.newArrayList();
        // 需求流程是否可簽名
        boolean showSign = false;
        // 需求流程是否可復原
        boolean showRecovery = false;
        // 需求流程是否可退回
        boolean showRollBack = false;
        // 需求流程是否可作廢
        boolean showInvalid = false;
        // 需求流程加簽是否可簽名
        boolean showSignManagerSignInfo = false;
        // 需求流程加簽是否可復原
        boolean showRecoveryManagerSignInfo = false;
        this.showExecSignBtn = false;
        this.showExecRollBackBtn = false;
        this.showExecStopBtn = false;
        this.showExecRecoveryBtn = false;

        if (!wcMaster.getWc_status().equals(WCStatus.INVALID)
                && !wcMaster.getWc_status().equals(WCStatus.CLOSE)
                && !wcMaster.getWc_status().equals(WCStatus.CLOSE_STOP)) {

            // 查詢所有需求流程
            List<WCManagerSignInfo> reqSignInfos = WCManagerSignInfoManager.getInstance().findActiveReqFlowSignInfos(
                    wcMaster.getSid());

            // 需求方流程判斷
            for (WCManagerSignInfo reqSignInfo : reqSignInfos) {
                List<ProcessTaskBase> pbs = BpmManager.getInstance().findSimulationChart("", reqSignInfo.getBpmId());

                if (reqSignInfo.getSignType().equals(SignType.SIGN)) {
                    if (reqSignInfo.getStatus().equals(BpmStatus.APPROVED)) {
                        if (WCManagerSignInfoManager.getInstance()
                                .isShowApprovedRecovery(bpmVo.getLoginUserId(), pbs, wcMaster.getSid())) {
                            showRecovery = true;
                            showBtnComponent = true;
                        }
                    } else {
                        if (WCManagerSignInfoManager.getInstance()
                                .isShowSign(bpmVo.getLoginUserId(), reqSignInfo.getBpmId())) {
                            showSign = true;
                            showBtnComponent = true;
                        }
                        if (WCManagerSignInfoManager.getInstance()
                                .isShowRecovery(bpmVo.getLoginUserId(), reqSignInfo.getStatus(), pbs)) {
                            showRecovery = true;
                            showBtnComponent = true;
                        }
                        if (WCManagerSignInfoManager.getInstance()
                                .isShowRollBack(bpmVo.getLoginUserId(), reqSignInfo.getBpmId(), pbs)) {
                            showRollBack = true;
                            showBtnComponent = true;
                        }
                        if (WCManagerSignInfoManager.getInstance()
                                .isShowInvalid(
                                        bpmVo.getLoginUserId(), reqSignInfo.getStatus(), reqSignInfo.getBpmId(),
                                        pbs)) {
                            showInvalid = true;
                            showBtnComponent = true;
                        }
                    }
                } else if (reqSignInfo.getSignType().equals(SignType.COUNTERSIGN)) {
                    if (reqSignInfo.getStatus().equals(BpmStatus.APPROVED)) {
                        if (WCManagerSignInfoManager.getInstance()
                                .isShowContinueRecovery(user, pbs, wcMaster.getSid())) {
                            showRecoveryManagerSignInfo = true;
                            showBtnComponent = true;
                        }
                    } else {
                        if (WCManagerSignInfoManager.getInstance()
                                .isShowSign(bpmVo.getLoginUserId(), reqSignInfo.getBpmId())) {
                            showSignManagerSignInfo = true;
                            showBtnComponent = true;
                        }
                    }
                }
            }
            // 執行方流程判斷 [簽名]按鈕
            if (WCExecManagerSignInfoLogicComponent.getInstance()
                    .canSignExecManagerInfo(wcMaster.getSid(), user)) {
                this.showExecSignBtn = true;
                showBtnComponent = true;
            }
            // 執行方流程判斷 [退回]按鈕
            if (WCExecManagerSignInfoLogicComponent.getInstance()
                    .canRollBackExecManagerInfo(wcMaster.getSid(), user)) {
                this.showExecRollBackBtn = true;
                showBtnComponent = true;
            }
            // 執行方流程判斷 [不執行]按鈕
            try {
                if (PermissionLogicForExecDep.getInstance()
                        .isCanExecFlowSignerStop(wcMaster.getSid(), user)) {
                    this.showExecStopBtn = true;
                    showBtnComponent = true;
                }
            } catch (UserMessageException e) {
                log.error("判斷執行方簽核者不執行按鈕失敗 [" + e.getExceptionMessage() + "]", e);
            }
            // 執行方流程判斷 [復原]按鈕
            if (WCExecManagerSignInfoLogicComponent.getInstance()
                    .canRecoveryExecManagerInfo(wcMaster.getSid(), user)) {
                this.showExecRecoveryBtn = true;
                showBtnComponent = true;
            }
            // 執行方流程判斷 無設定執行單位簽核人員[退回]按鈕
            this.showExecDepRollBackBtn = WCExecDepManager.getInstance()
                    .isShowExecDepRollBackPermisison(wcMaster.getSid(), user);
            if (showExecDepRollBackBtn) {
                showBtnComponent = true;
            }
            // 執行方流程判斷 無設定執行單位簽核人員[不執行]按鈕
            this.showExecDepStopBtn = WCExecDepManager.getInstance()
                    .isShowExecDepStopPermisison(wcMaster.getSid(), user);
            if (showExecDepStopBtn) {
                showBtnComponent = true;
            }
        }
        bpmVo.loadTasksAndShowBtn(
                flowNodeVos,
                showSign,
                showRecovery,
                showRollBack,
                showInvalid,
                showSignManagerSignInfo,
                showRecoveryManagerSignInfo);
        signRoles.clear();
        signRoles.addAll(BPMLogicComponent.getInstance().getSignRole(this.bpmVo.getSid(), user));
        DisplayController.getInstance().execute("mergeSignCell();");
    }

    private void reloadViewData(ActionType actType) {
        // 畫面控制
        try {

            // 重整畫面資料 (目前原始版本看起來是做一樣的事，先保留彈性 )
            if (actType.isReloadBPMTab()) {
                bpmtCallBack.reloadBPMTab();
            }
            if (actType.isReloadExecBPMTab()) {
                bpmtCallBack.reloadExecBPMTab();
            }
            //
            DisplayController.getInstance().execute("doReloadDataToUpdate('" + this.bpmVo.getSid() + "')");
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(讀取資料失敗)";
            log.error(errorMessage + e.getMessage(), e);
            MessagesUtils.showError(errorMessage);
            return;
        }
    }

    // ========================================================================
    //
    // ========================================================================

    /**
     * 執行需求單位 退回前 動作
     */
    public void btnBeforRollBackBpm() {
        try {
            bpmVo.initRollbackItems(this.bulidRollbackItems(this.defaultSignInfo));
            DisplayController.getInstance().update("dlg_bpm_rollback_view");
            DisplayController.getInstance().showPfWidgetVar("dlg_bpm_rollback");
        } catch (Exception e) {
            log.warn("點擊 BPM退回前動作 發生錯誤!!", e);
            bpmtCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行執行單位 退回前 動作
     */
    public void btnBeforRollBackBpmExec() {
        try {
            this.bpmVo.initRollbackItems(this.bulidRollbackItems(this.defaultSignInfo));
            DisplayController.getInstance().update("rollback_exec_panel_id");
            DisplayController.getInstance().showPfWidgetVar("dlg_bpm_rollback_exec");

        } catch (Exception e) {
            log.warn("點擊 BPM退回前動作 發生錯誤!!", e);
            bpmtCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行執行單位-無設定執行單位簽核人員 退回前 動作
     */
    public void btnBeforRollBackBpmExecDep() {
        try {
            bpmVo.initRollbackItems(this.bulidRollbackItems(this.defaultSignInfo));
            DisplayController.getInstance().update("rollback_execDep_panel_id");
            DisplayController.getInstance().showPfWidgetVar("dlg_bpm_rollback_execDep");
        } catch (Exception e) {
            log.warn("點擊 BPM退回前動作 發生錯誤!!", e);
            bpmtCallBack.showMessage(e.getMessage());
        }
    }

    public void doAction(
            String actionName) {
        this.doAction(actionName, "", "");
    }

    /**
     * 執行流程動作
     * 
     * @param actionName         動作名稱
     * @param comment            原因、理由、說明
     * @param rollBackReqTaskSid 退回的需求方節點 sid
     */
    public void doAction(
            String actionName,
            String comment,
            String rollBackReqTaskSid) {

        // ====================================
        // 取得動作類型
        // ====================================
        ActionType actType = ActionType.valueOf(WkStringUtils.safeTrim(actionName));
        if (actType == null) {
            String errorMesssage = WkMessage.EXECTION + "(動作類型錯誤)";
            log.error(errorMesssage + " actionName:[{}]", actionName);
            MessagesUtils.showError(errorMesssage);
            return;
        }

        // ====================================
        // log
        // ====================================
        String execInfo = "單據:[%s]，行為:[%s]-";
        execInfo = String.format(execInfo,
                bpmVo.getNo(),
                actType.getDescr());

        log.debug("\r\n" + execInfo + "START");

        // ====================================
        //
        // ====================================
        String wcSid = bpmVo.getSid();
        Integer execUserSid = SecurityFacade.getUserSid();

        // ====================================
        // 執行簽核動作
        // ====================================
        ReqFlowActionHelper reqFlowActionHelper = SpringContextHolder.getBean(ReqFlowActionHelper.class);
        ExecFlowActionHelper execFlowActionHelper = SpringContextHolder.getBean(ExecFlowActionHelper.class);
        try {
            switch (actType) {
            // -------------------
            // 需求單位-簽名
            // -------------------
            case REQ_FLOW_SIGN:
                reqFlowActionHelper.doReqflowSign(wcSid, execUserSid);
                break;

            // -------------------
            // 需求單位-復原
            // -------------------
            case REQ_FLOW_RECOVERY:
                reqFlowActionHelper.doReqFlowRecovery(wcSid, execUserSid);
                break;

            // -------------------
            // 需求單位-退回
            // -------------------
            case REQ_FLOW_ROLLBACK:
                // 退回
                reqFlowActionHelper.doReqFlowRollBack(
                        wcSid,
                        execUserSid,
                        bpmVo.getRollbackSelectTaskSid(),
                        bpmVo.getRollbackComment());

                // 畫面控制
                DisplayController.getInstance().hidePfWidgetVar("dlg_bpm_rollback");
                break;

            // -------------------
            // 需求單位-作廢
            // -------------------
            case REQ_FLOW_INVAILD:
                // 作廢
                reqFlowActionHelper.doReqFlowInvalid(wcSid, execUserSid, comment);
                // 畫面控制
                DisplayController.getInstance().hidePfWidgetVar("dlg_bpm_invaild");
                break;

            // -------------------
            // 需求單位會簽-簽名
            // -------------------
            case REQ_FLOW_COUNTER_SIGN:
                reqFlowActionHelper.doReqflowCounterSign(wcSid, execUserSid);
                break;

            // -------------------
            // 需求單位會簽-復原
            // -------------------
            case REQ_FLOW_COUNTER_RECOVERY:
                reqFlowActionHelper.doReqflowCounterSignRecovery(wcSid, execUserSid);
                break;

            // -------------------
            // 執行單位-簽名
            // -------------------
            case EXEC_FLOW_SIGN:
                execFlowActionHelper.doExecFlowSign(wcSid, execUserSid);
                break;

            // -------------------
            // 執行單位-復原
            // -------------------
            case EXEC_FLOW_RECOVERY:
                execFlowActionHelper.doExecFlowRecovery(wcSid, execUserSid);
                break;

            // -------------------
            // 執行方-簽核者退回
            // -------------------
            case EXEC_FLOW_SIGNER_ROLLBACK:
                // 執行
                execFlowActionHelper.doExecFlowRollBack(wcSid, execUserSid, rollBackReqTaskSid, comment);
                // 畫面控制
                DisplayController.getInstance().hidePfWidgetVar("dlg_bpm_rollback_exec");
                break;

            // -------------------
            // 執行方-單位成員退回
            // -------------------
            case EXEC_FLOW_MEMBER_ROLLBACK:
                // 執行
                execFlowActionHelper.doExecFlowMemberRollBack(wcSid, execUserSid, rollBackReqTaskSid, comment);
                // 畫面控制
                DisplayController.getInstance().hidePfWidgetVar("dlg_bpm_rollback_execDep");
                break;

            // -------------------
            // 執行方-簽核者不執行
            // -------------------
            case EXEC_FLOW_SIGNER_STOP:
                // 執行
                execFlowActionHelper.doExecFlowSignerStop(wcSid, execUserSid, comment);
                // 畫面控制
                DisplayController.getInstance().hidePfWidgetVar("dlg_bpm_stop_exec");
                break;

            // -------------------
            // 執行方-單位成員不執行
            // -------------------
            case EXEC_FLOW_MEMBER_STOP:
                // 執行
                execFlowActionHelper.doExecFlowMemberStop(wcSid, execUserSid, comment);
                // 畫面控制
                DisplayController.getInstance().hidePfWidgetVar("dlg_bpm_stop_execDep");
                break;

            default:
                String errorMessage = WkMessage.EXECTION + "(未定義的操作類型:[" + actType + "])";
                log.error(errorMessage);
                MessagesUtils.showError(errorMessage);
            }

        } catch (UserMessageException e) {
            // 預期的 exception , 已處理好畫面顯示訊息
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            // 未預期的 exception
            String errorMessage = WkMessage.EXECTION + "(" + actType.getDescr() + "失敗)";
            log.error(errorMessage + e.getMessage(), e);
            MessagesUtils.showError(errorMessage);
            return;
        } finally {
            this.reloadViewData(actType);
        }
        
        log.debug("\r\n" + execInfo + "FINISH");
    }

    /**
     * 建立退回選項
     */
    private SelectItem[] bulidRollbackItems(SingleManagerSignInfo defaultSignInfo) {
        try {
            List<ProcessTaskBase> pbs = BpmManager.getInstance().findSimulationChart("", defaultSignInfo.getInstanceID());
            List<ProcessTaskBase> copy = Lists.newArrayList(pbs);
            if (!defaultSignInfo.getStatus().equals(BpmStatus.APPROVED)) {
                copy.remove(copy.size() - 1);
            }
            SelectItem[] items = new SelectItem[copy.size()];
            int index = 0;
            for (ProcessTaskBase each : copy) {
                // 還沒簽的不可以退
                if (each instanceof ProcessTask) {
                    continue;
                }

                String label = "";
                if (each.getTaskName().contains("申請人")) {
                    label = "申請人-" + each.getUserName();
                } else {
                    label = each.getRoleName() + "-" + each.getUserName();
                }

                SelectItem item = new SelectItem(each.getSid(), label);

                items[index++] = item;
            }
            return items;
        } catch (Exception e) {
            log.warn("bulidRollbackItems ERROR", e);
        }
        return new SelectItem[0];
    }
}
