/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.cy.work.connect.web.common.setting.SourceCompanySetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 來源公司Item Bean
 *
 * @author jimmy_chou
 */
@Controller
@Scope("request")
public class SourceCompanyBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7982020681303569244L;

    /**
     * 取得來源公司SelectItems
     */
    public List<SelectItem> getSourceCompanyItems() {
        return SourceCompanySetting.getSourceCompanyItems();
    }
}
