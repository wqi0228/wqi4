/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.search;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.ReadableDocsInquireColumn;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.ExportExcelComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.TableUpDownListener;
import com.cy.work.connect.web.logic.components.WCReadReceiptComponents;
import com.cy.work.connect.web.logic.components.search.ReadableDocsInquireLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.TableUpDownBean;
import com.cy.work.connect.web.view.components.CustomColumnComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkReportSidTo;
import com.cy.work.connect.web.view.vo.column.search.ReadableDocsInquireColumnVO;
import com.cy.work.connect.web.view.vo.search.ReadableDocsInquireVO;
import com.cy.work.connect.web.view.vo.search.query.ReadableDocsInquireQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 可閱單據查詢
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class Search5Bean implements Serializable, TableUpDownListener {

    /**
     *
     */
    private static final long serialVersionUID = 5562759253492355694L;

    @Getter
    /** dataTable Id */
    private final String dataTableID = "dataTableWorkReport";

    @Getter
    /** dataTable widgetVar */
    private final String dataTableWv = "dataTableWorkReportWv";

    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Autowired
    private ReadableDocsInquireLogicComponent searchLogicComponent;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private ExportExcelComponent exportExcelComponent;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCMasterManager wcMasterManager;
    /**
     * 部門相關邏輯Component
     */
    private DepTreeComponent depTreeComponent;

    @Getter
    /** 登入者 */
    private UserViewVO userViewVO;

    @Getter
    /** 登入單位 */
    private OrgViewVo depViewVo;

    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;
    /**
     * 可查詢單位
     */
    // private List<Integer> depSids;

    @Getter
    /** view 顯示錯誤訊息 */
    private String errorMessage;

    private final MessageCallBack messageCallBack =
        new MessageCallBack() {
            /** */
            private static final long serialVersionUID = 1016790166787393782L;

            @Override
            public void showMessage(String m) {
                errorMessage = m;
                DisplayController.getInstance().update("confimDlgTemplate");
                DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
            }
        };

    @Setter
    @Getter
    /** 是否版面切換(顯示Frame畫面) */
    private boolean showFrame = false;

    @Getter
    /** 匯出使用 */
    private boolean hasDisplay = true;

    @Getter
    /** 列表高度 */
    private String dtScrollHeight = "400";

    @Getter
    /** dataTable 欄位資訊 */
    private ReadableDocsInquireColumnVO columnVO;

    private final DataTableReLoadCallBack dataTableReLoadCallBack =
        new DataTableReLoadCallBack() {
            /** */
            private static final long serialVersionUID = -1973974803290120068L;

            @Override
            public void reload() {
                columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
                DisplayController.getInstance().update(dataTableID);
            }
        };

    @Setter
    @Getter
    /** 列表資料 */
    private List<ReadableDocsInquireVO> allVos;

    @Setter
    @Getter
    /** 列表選取資料 */
    private ReadableDocsInquireVO selVo;
    /**
     * 暫存資料
     */
    private List<ReadableDocsInquireVO> tempVOs;
    /**
     * 暫存資料
     */
    private String tempSelSid;

    @Getter
    /** frame 連結路徑 */
    private String iframeUrl = "";

    @Getter
    /** 查詢物件 */
    private ReadableDocsInquireQuery query;

    @Getter
    /** dataTable filter items */
    private List<SelectItem> menuTagNameItems;

    @Getter
    /** dataTable filter items */
    private List<SelectItem> categoryTagNameItems;

    @Getter
    /** dataTable filter items */
    private List<SelectItem> statusNameItems;

    @Getter
    /** dataTable filter items */
    private List<SelectItem> readStatuItems;

    @Getter
    /** 類別(大類) 選項 */
    private List<SelectItem> categoryItems;

    @Getter
    /** 單據狀態 選項 */
    private List<SelectItem> statusItems;

    @Getter
    /** 閱讀狀態 選項 */
    private List<SelectItem> readStatusItems;

    private List<Org> canSelectOrgs;
    private List<Org> allOrgs;
    private List<Org> viewReadRecipts;
    private List<Org> selectOrgs;
    @Setter
    @Getter
    private String selReadReceiptStatus = "";
    @Getter
    private CustomColumnComponent customColumn;
    private boolean isFromPortal = false;
    private String readReceiptStatusPortal = "";

    @PostConstruct
    public void init() {
        this.initLogin();
        this.initComponents();
        this.initQueryItems();
        this.initDepSetting();
        this.query.init(userViewVO.getSid());
        initRequest();
        this.clear();
    }

    private void initRequest() {
        try {
            if (Faces.getRequestParameterMap().containsKey("readType")) {
                String readTypeStr = Faces.getRequestParameterMap().get("readType");
                readReceiptStatusPortal = readTypeStr;
                // 若是從Portal , 需顯示全部狀態
                if (WCReadReceiptStatus.UNREAD.name().equals(readReceiptStatusPortal)) {
                    isFromPortal = true;
                }
            }
        } catch (Exception e) {
            log.warn("init ERROR", e);
        }
    }

    private void initDepSetting() {
        canSelectOrgs = Lists.newArrayList();
        allOrgs = Lists.newArrayList();
        viewReadRecipts = Lists.newArrayList();
        selectOrgs = Lists.newArrayList();
        
        List<Org> deps = wcMasterManager.findViewReadReceiptsOrgs(SecurityFacade.getUserSid());
        viewReadRecipts.addAll(deps);
        viewReadRecipts.forEach(
            item -> {
                if (!canSelectOrgs.contains(item)) {
                    canSelectOrgs.add(item);
                }
                if (!selectOrgs.contains(item)) {
                    selectOrgs.add(item);
                }
            });
        allOrgs.addAll(canSelectOrgs);
        canSelectOrgs.forEach(
            item -> {
                List<Org> allParents = orgManager.findAllParent(item.getSid());
                if (allParents == null || allParents.isEmpty()) {
                    return;
                }
                allOrgs.addAll(
                    allParents.stream()
                        .filter(each -> !allOrgs.contains(each))
                        .collect(Collectors.toList()));
            });
        try {
            Collections.sort(
                allOrgs,
                new Comparator<Org>() {
                    @Override
                    public int compare(Org o1, Org o2) {
                        return o1.getSid().compareTo(o2.getSid());
                    }
                });
        } catch (Exception e) {
            log.warn("sort Error", e);
        }
        depTreeComponent = new DepTreeComponent();
        loadOrgs();
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    public void loadOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
            item -> {
                tempOrg.add(item);
            });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());
        depTreeComponent.init(
            tempOrg,
            canSelectOrgs,
            group,
            selectOrgs,
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);
    }

    /**
     * 初始化 登入者資訊
     */
    private void initLogin() {        
        userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
    }

    /**
     * 初始化 元件
     */
    private void initComponents() {
        columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
        customColumn =
            new CustomColumnComponent(
                WCReportCustomColumnUrlType.READABLE_DOCS_INQUIRE,
                Arrays.asList(ReadableDocsInquireColumn.values()),
                columnVO.getPageCount(),
                userViewVO.getSid(),
                messageCallBack,
                dataTableReLoadCallBack);
        query = new ReadableDocsInquireQuery();
    }

    /**
     * 初始化 類別元件選項
     */
    private void initQueryItems() {
        this.categoryItems = searchLogicComponent.getCategoryItems(compViewVo.getSid());
        this.statusItems = searchLogicComponent.getStatusItems();
        this.readStatusItems = searchLogicComponent.getReadStatusItems();
    }

    /**
     * 清除並查詢
     */
    public void clear() {
        this.query.init(userViewVO.getSid());
        selectOrgs.clear();
        viewReadRecipts.forEach(
            item -> {
                if (!selectOrgs.contains(item)) {
                    selectOrgs.add(item);
                }
            });
        selReadReceiptStatus = "";
        if (this.isFromPortal) {
            selReadReceiptStatus = readReceiptStatusPortal;
            query.getStatusList().clear();
            query
                .getStatusList()
                .addAll(
                    Lists.newArrayList(
                        WCStatus.WAITAPPROVE.name(),
                        WCStatus.APPROVING.name(),
                        WCStatus.EXEC.name(),
                        WCStatus.EXEC_FINISH.name(),
                        WCStatus.APPROVED.name(),
                        WCStatus.CLOSE_STOP.name(),
                        WCStatus.CLOSE.name()));
        }
        this.doSearch();
    }

    /**
     * 執行查詢
     */
    public void doSearch() {
        try {
            if (query.getStartDate() != null && query.getEndDate() != null) {
                if (query.getStartDate().after(query.getEndDate())) {
                    messageCallBack.showMessage("建立區間起始日期不可晚於結束日期");
                    return;
                }
            }
            allVos = this.searchView("");
            if (allVos.size() > 0) {
                DisplayController.getInstance()
                    .execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.warn("doSearch", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void readAllReceipts() {
        WCReadReceiptComponents.getInstance()
            .updateUNReadTimeAndReadReceiptByUserSid(userViewVO.getSid());
        DisplayController.getInstance().hidePfWidgetVar("readAllConfimDlg");
    }

    /**
     * 刷新畫面動作
     *
     * @param sid
     * @return
     */
    private List<ReadableDocsInquireVO> searchView(String sid) {
        List<ReadableDocsInquireVO> tempAllVos = this.search(sid);
        this.menuTagNameItems =
            tempAllVos.stream().map(each -> each.getMenuTagName()).collect(Collectors.toSet())
                .stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        this.categoryTagNameItems =
            tempAllVos.stream().map(each -> each.getCategoryName()).collect(Collectors.toSet())
                .stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        this.statusNameItems =
            tempAllVos.stream().map(each -> each.getStatusName()).collect(Collectors.toSet())
                .stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        this.readStatuItems =
            tempAllVos.stream().map(each -> each.getReadStatus()).collect(Collectors.toSet())
                .stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        return tempAllVos;
    }

    private void settingReaded() {
        allVos.forEach(
            item -> {
                if (this.selVo.getSid().equals(item.getSid())) {
                    this.selVo.replaceReadStatus(WCReadReceiptStatus.READ);
                    item.replaceReadStatus(WCReadReceiptStatus.READ);
                }
            });
    }

    /**
     * 執行查詢
     *
     * @param sid
     * @return
     */
    private List<ReadableDocsInquireVO> search(String sid) {
        List<Integer> selDepSids = Lists.newArrayList();
        if (selectOrgs != null && !selectOrgs.isEmpty()) {
            selectOrgs.forEach(
                item -> {
                    selDepSids.add(item.getSid());
                });
        }
        WCReadReceiptStatus readReceiptStatus = null;
        try {
            if (!Strings.isNullOrEmpty(this.selReadReceiptStatus)) {
                readReceiptStatus = WCReadReceiptStatus.valueOf(this.selReadReceiptStatus);
            }
        } catch (Exception e) {
            log.warn("search ERROR", e);
        }


        return searchLogicComponent.search(query, selDepSids, sid, readReceiptStatus);
    }

    /**
     * 回到列表查詢
     */
    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.warn("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行上一筆
     */
    @Override
    public void openerByBtnUp() {
        try {
            if (!allVos.isEmpty()) {
                this.toUp();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
                settingReaded();
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("工作聯絡單進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行上一筆
     */
    private void toUp() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index > 0) {
                    selVo = tempVOs.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index > 0) {
                    selVo = allVos.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
            }
        } catch (Exception e) {
            log.warn("toUp", e);
            this.clearTemp();
            if (!allVos.isEmpty()) {
                selVo = allVos.get(0);
            }
        }
    }

    /**
     * 執行下一筆
     */
    @Override
    public void openerByBtnDown() {
        try {
            if (!allVos.isEmpty()) {
                this.toDown();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
                settingReaded();
            }

            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("工作聯絡單查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行下一筆
     */
    private void toDown() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index >= 0) {
                    selVo = tempVOs.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index >= 0) {
                    selVo = allVos.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
            }
        } catch (Exception e) {
            log.warn("toDown", e);
            this.clearTemp();
            if (allVos.size() > 0) {
                selVo = allVos.get(allVos.size() - 1);
            }
        }
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNextAndReaded() {
        this.settingPreviousAndNext();
        tableUpDownBean.setSession_now_sid(selVo.getWcSid());
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNext() {
        int index = allVos.indexOf(selVo);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(allVos.get(index - 1).getWcSid());
        }
        if (index == allVos.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(allVos.get(index + 1).getWcSid());
        }
    }

    /**
     * 判斷是否有暫存資訊
     */
    private boolean checkTempVOs() {
        return tempVOs != null && !tempVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid);
    }

    /**
     * 開啟分頁
     *
     * @param sid
     */
    public void btnOpenUrl(String sid) {
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
            settingReaded();
        } catch (Exception e) {
            log.warn("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入滿版
     */
    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selVo.getWcSid();
            tableUpDownBean.setWorp_path("worp_iframe");
            settingReaded();
        } catch (Exception e) {
            log.warn("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 變更 selVo
     */
    private void settintSelVo(String sid) {
        ReadableDocsInquireVO sel = new ReadableDocsInquireVO(sid);
        int index = allVos.indexOf(sel);
        if (index > 0) {
            selVo = allVos.get(index);
        } else {
            selVo = allVos.get(0);
        }
    }

    /**
     * ?????
     */
    public void reloadDataByUpdate() {
        this.doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByFavorite() {
    }

    /**
     * ?????
     */
    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.warn("doReloadInfo", ex);
        }
    }

    /**
     * ?????
     *
     * @param sid
     */
    private void updateInfoToDataTable(String sid) {
        try {
            List<ReadableDocsInquireVO> result = search(sid);
            if (result == null || result.isEmpty()) {
                tempVOs = Lists.newArrayList();
                if (allVos != null && !allVos.isEmpty()) {
                    allVos.forEach(
                        item -> {
                            tempVOs.add(item);
                        });
                    allVos.remove(new ReadableDocsInquireVO(sid));
                    tempSelSid = sid;
                }
            } else {
                ReadableDocsInquireVO updateDeail = result.get(0);
                allVos.forEach(
                    item -> {
                        if (item.getWcSid().equals(updateDeail.getWcSid())) {
                            item.replaceValue(updateDeail);
                        }
                    });
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("updateInfoToDataTable Error", e);
        }
    }

    /**
     * 執行匯出前置
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcel(
                document, query.getStartDate(), query.getEndDate(), "工作聯絡單", true);
        } catch (Exception e) {
            log.warn("exportExcel", e);
        } finally {
            hasDisplay = true;
        }
    }

    /**
     * 清除暫存
     */
    private void clearTemp() {
        tempSelSid = "";
        tempVOs = null;
    }

    /**
     * 取得暫存索引
     */
    private int getTempIndex() {
        return tempVOs.indexOf(new ReadableDocsInquireVO(tempSelSid));
    }

    /**
     * 更新dtScrollHeight
     */
    public void updateDtScrollHeight() {
        String height =
            FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("dtScrollHeight");
        if (!Strings.isNullOrEmpty(height)) {
            dtScrollHeight = height;
        }
    }

    public void doSelectOrg() {
        selectOrgs = depTreeComponent.getSelectOrg();
        DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
    }
}
