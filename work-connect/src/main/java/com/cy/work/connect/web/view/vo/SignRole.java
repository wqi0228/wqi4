/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author jason_h
 */
@Data
@EqualsAndHashCode(of = {"signUserId"})
public class SignRole implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -290079067817820054L;

    private String groupId;

    private String signUserId;

    private String roleName;

    private String deptName;

    private String userName;

    private String signDate;

    private String comment;

    private String commentRollBackOrInvail;

    private String commentAll;

    private String clz;
}
