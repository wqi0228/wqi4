/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.common.vo.AttachmentService;
import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.logic.components.WCMemoAttachmentLogicComponent;
import com.cy.work.connect.web.view.enumtype.TabType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.primefaces.model.StreamedContent;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WCMemoAttachMaintainCompant implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 58005705501735607L;
    private final Integer userSid;
    private final MessageCallBack messageCallBack;
    private final TabCallBack tabCallBack;
    @Getter
    private List<AttachmentVO> attachmentVOs;
    @Getter
    private List<AttachmentVO> selectAttachmentVOs;
    private String wcMemoSid;
    @SuppressWarnings("unused")
    private String wcMemoNo;
    @Getter
    private AttachmentVO selDelAtt;
    @Getter
    private boolean showAttTab = false;

    public WCMemoAttachMaintainCompant(
        Integer userSid, MessageCallBack messageCallBack, TabCallBack tabCallBack) {
        this.userSid = userSid;
        this.selDelAtt = new AttachmentVO("", "", "", "", false, "");
        this.messageCallBack = messageCallBack;
        this.tabCallBack = tabCallBack;
    }

    public void loadData(String wcMemoSid, String wcMemoNo) {
        this.wcMemoSid = wcMemoSid;
        this.wcMemoNo = wcMemoNo;
        selectAttachmentVOs = Lists.newArrayList();
        attachmentVOs = Lists.newArrayList();
        loadData();
    }

    public void changeToEditMode(String sid) {
        attachmentVOs.forEach(
            item -> {
                if (item.getAttSid().equals(sid)) {
                    item.setEditMode(true);
                }
            });
    }

    public void loadData() {
        try {
            List<AttachmentVO> tempAttachmentVOs =
                WCMemoAttachmentLogicComponent.getInstance().getAttachmentsByWCMemoSid(wcMemoSid);
            tempAttachmentVOs.forEach(
                item -> {
                    if (!attachmentVOs.contains(item)) {
                        item.setChecked(false);
                        item.setShowEditBtn(
                            String.valueOf(userSid).equals(item.getCreateUserSId()));
                        item.setEditMode(false);
                        attachmentVOs.add(item);
                    }
                });
            List<AttachmentVO> tempReal = Lists.newArrayList();
            attachmentVOs.forEach(
                item -> {
                    if (tempAttachmentVOs.contains(item)) {
                        tempReal.add(item);
                    }
                });
            attachmentVOs.clear();
            attachmentVOs.addAll(tempReal);

            showAttTab = attachmentVOs.size() > 0;
        } catch (Exception e) {
            log.warn("loadData", e);
        }
    }

    public void loadDeleteAtt(String sid) {
        attachmentVOs.forEach(
            item -> {
                if (item.getAttSid().equals(sid)) {
                    this.selDelAtt = item;
                }
            });
    }

    public void deleteAtt() {
        if (selDelAtt == null || Strings.isNullOrEmpty(selDelAtt.getAttSid())) {
            return;
        }
        List<AttachmentVO> tempAv = Lists.newArrayList(attachmentVOs);
        tempAv.forEach(
            item -> {
                if (item.getAttSid().equals(selDelAtt.getAttSid())) {
                    try {
                        List<AttachmentVO> attachment =
                            WCMemoAttachmentLogicComponent.getInstance()
                                .getAttachmentsByWCMemoSid(wcMemoSid);
                        if (attachment.contains(selDelAtt)) {

                            WCMemoAttachmentLogicComponent.getInstance()
                                .deleteAttachment(item, userSid, wcMemoSid);
                            tabCallBack.reloadTraceTab();
                            tabCallBack.reloadAttTab();
                            if (showAttTab) {
                                tabCallBack.reloadTabView(TabType.Attachment);
                            } else {
                                tabCallBack.reloadTabView(TabType.Trace);
                            }
                        }
                        attachment = null;
                    } catch (IllegalStateException | IllegalArgumentException e) {
                        messageCallBack.showMessage(e.getMessage());
                        log.warn("deleteAtt Error：{}", e.getMessage());
                    } catch (Exception e) {
                        messageCallBack.showMessage(e.getMessage());
                        log.warn("deleteAtt Error :", e);
                    }
                }
            });
    }

    public void saveAttDesc(String sid) {
        attachmentVOs.forEach(
            item -> {
                if (item.getAttSid().equals(sid)) {
                    try {
                        WCMemoAttachmentLogicComponent.getInstance()
                            .updateAttachment(sid, item.getAttDesc(), userSid);
                        item.setEditMode(false);
                    } catch (Exception e) {
                        log.warn("saveAttDesc Error :", e);
                    }
                }
            });
    }

    public void selectAll() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(
            item -> {
                selectAttachmentVOs.add(item);
                item.setChecked(true);
            });
    }

    public void changeCheckBox() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(
            item -> {
                if (item.isChecked()) {
                    selectAttachmentVOs.add(item);
                }
            });
    }

    public StreamedContent downloadFile() throws IOException {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        // 下載單一檔案，預設檔名使用該檔案的原始檔名（但是要經過編碼）
        if (selectAttachmentVOs.size() == 1) {
            return AttachmentUtils.createStreamedContent(selectAttachmentVOs.get(0),
                "wcMemoMaster");
        }
        // 將多檔案壓縮成單一 zip 檔案再輸出
        return AttachmentUtils.createWrappedStreamedContent(selectAttachmentVOs, "wcMemoMaster");
    }

    public String downloadFileContentDisposition() {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        String encodedFileName =
            (selectAttachmentVOs.size() == 1)
                ? AttachmentUtils.encodeFileName(selectAttachmentVOs.get(0).getAttName())
                : AttachmentService.MULTIFILES_ZIP_FILENAME;
        String contentDispositionPattern =
            AttachmentUtils.isMSIE(Faces.getRequest())
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''{1}";
        return MessageFormat.format(contentDispositionPattern, "attachment", encodedFileName);
    }
}
