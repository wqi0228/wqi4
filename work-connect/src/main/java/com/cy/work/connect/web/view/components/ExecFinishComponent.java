/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.listener.ReLoadCallBack;
import com.cy.work.connect.web.logic.components.WCExceDepLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉單套件
 *
 * @author brain0925_liao
 */
@Slf4j
public class ExecFinishComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3932951783497668973L;
    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 存檔更新CallBacks
     */
    private final ReLoadCallBack reLoadCallBack;
    /**
     * 執行單位選單
     */
    @Getter
    private final List<SelectItem> execSelectItems;
    /**
     * 小類選單
     */
    @Getter
    private final List<SelectItem> tagSelectItems;
    /**
     * 執行單位及執行單位可挑選小類 Map
     */
    private final Map<Integer, List<WCTag>> tagMaps;
    /**
     * 工作聯絡單Sid
     */
    private String wcSid;
    /**
     * 挑選的執行單位
     */
    @Getter
    @Setter
    private String selExecDepSid;
    /**
     * 挑選的小類
     */
    @Getter
    @Setter
    private List<String> selTagSids;
    @Getter
    private String userInfo;
    @Getter
    private String time;
    @Getter
    @Setter
    private String content;

    public ExecFinishComponent(
            Integer loginUserSid, ReLoadCallBack reLoadCallBack) {
        this.loginUserSid = loginUserSid;
        this.reLoadCallBack = reLoadCallBack;
        this.execSelectItems = Lists.newArrayList();
        this.tagSelectItems = Lists.newArrayList();
        this.tagMaps = Maps.newConcurrentMap();
    }

    public void loadData(String wcSid) throws UserMessageException {
        this.wcSid = wcSid;
        execSelectItems.clear();
        tagSelectItems.clear();
        tagMaps.clear();
        this.selExecDepSid = "";

        this.content = WCExceDepLogicComponent.getInstance().prepareExecFinishDepsContent(wcSid, loginUserSid);
        if (WkStringUtils.isEmpty(content)) {
            throw new UserMessageException(WkMessage.NEED_RELOAD + "(找不到可以執行完成的項目)", InfomationLevel.WARN);
        }

        //
        // this.selTagSids = Lists.newArrayList();
        // Map<Org, List<WCTag>> execDepMap =
        // WCExceDepLogicComponent.getInstance().getExecFinishDeps(wcSid, loginUserSid);
        // Preconditions.checkState(execDepMap.keySet().size() > 0, "已無執行單位可進行執行完成");

        // execDepMap
        // .keySet()
        // .forEach(
        // item -> {
        //
        //
        // tagMaps.put(item.getSid(), execDepMap.get(item));
        // if (execSelectItems.isEmpty()) {
        // execDepMap
        // .get(item)
        // .forEach(
        // subItem -> {
        // tagSelectItems.add(
        // new SelectItem(subItem.getSid(), subItem.getTagName()));
        // selTagSids.add(subItem.getSid());
        // this.content = this.content.concat("<br>")
        // .concat(subItem.getTagName());
        // });
        // }
        // execSelectItems.add(
        // new SelectItem(String.valueOf(item.getSid()), item.getName()));
        // });

        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        time = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                new Date());
        userInfo = loginUser.getPrimaryOrg().getName() + "-" + loginUser.getName();
    }

    public void changeExecDep() {
        try {
            this.selTagSids = Lists.newArrayList();
            tagSelectItems.clear();
            tagMaps
                    .get(Integer.valueOf(selExecDepSid))
                    .forEach(
                            subItem -> {
                                tagSelectItems.add(new SelectItem(subItem.getSid(), subItem.getTagName()));
                                selTagSids.add(subItem.getSid());
                            });
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void saveExecFinish() {

        if (WkStringUtils.isEmpty(this.content)) {
            MessagesUtils.showWarn("請輸入執行完成內容");
            return;
        }

        try {
            WCExceDepLogicComponent.getInstance().saveExecFinish(wcSid, loginUserSid, content);
            reLoadCallBack.onReload();
            DisplayController.getInstance().hidePfWidgetVar("execFinishDlg");

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
            return;
        }
    }
}
