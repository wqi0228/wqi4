/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCTagTempletManager;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCTagSearchColumn;
import com.cy.work.connect.web.common.*;
import com.cy.work.connect.web.common.attachment.WCMenuTagComponent;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.logic.components.*;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.components.CustomColumnComponent;
import com.cy.work.connect.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.connect.web.view.components.qstree.OrgUserTreeMBean;
import com.cy.work.connect.web.view.vo.*;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.event.ReorderEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 類別維護作業 MBean
 *
 * @author brain0925_liao
 */

@Slf4j
@Scope("view")
@ManagedBean
@Controller
public class Setting3Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8985795972087816433L;

    @Getter
    private final String dataTableID = "dataTableTagModify";
    @Getter
    private final String dataTableWv = "dataTableTagModifyWv";
    @Getter
    private final WCMenuTagComponent wcMenuTagComponent = new WCMenuTagComponent();
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Getter
    private WorkReportSearchHeaderComponent menuHeaderComponent;
    @Getter
    private WorkReportSearchHeaderComponent subTagHeaderComponent;
    @Autowired
    private WCMenuTagLogicComponent wcMenuTagLogicComponent;
    @Autowired
    private WCTagLogicComponents wCTagLogicComponents;
    @Autowired
    private WCCategoryTagLogicComponent wcCategoryTagLogicComponent;
    @Autowired
    private WCReportCustomColumnLogicComponents wkReportCustomColumnLogicComponents;
    @Autowired
    private WCExecDepSettingLogicComponent execDepSettingLogicComponent;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCTagTempletManager wcTagTempletManager;
    @Autowired
    private WCUserLogic wcUserLogicComponents;
    /**
     * view 顯示錯誤訊息
     */
    @Getter
    private String errorMessage;
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /** */
        private static final long serialVersionUID = -876598561508174952L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confimDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
        }
    };
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Setter
    @Getter
    private List<CategoryTagSearchVO> tagSearchVO;
    @Setter
    @Getter
    private CategoryTagSearchVO selTagSearchVO;
    @Getter
    private WCTagSearchColumnVO searchColumnVO;
    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /** */
        private static final long serialVersionUID = 2761602427586457372L;

        @Override
        public void reload() {
            searchColumnVO = wkReportCustomColumnLogicComponents.getWCTagSearchColumnSetting(
                    userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };
    @Getter
    private CustomColumnComponent customColumn;
    private List<Org> selectOrgs;
    private List<Org> allOrgs;
    /**
     * 部門相關邏輯Component
     */
    private DepTreeComponent depTreeComponent;
    @Setter
    @Getter
    private String tagName = "";
    @Setter
    @Getter
    private String statusID = "";
    @Setter
    @Getter
    private String reqFlowType = "";
    @Setter
    @Getter
    private String memo = "";
    @Setter
    @Getter
    private String seq = "";
    @Setter
    @Getter
    /** 預設主題 */
    private String defaultTheme = "";
    @Setter
    @Getter
    /** 預設內容 */
    private String defaultContent = "";
    @Setter
    @Getter
    /** 預設備註 */
    private String defaultMemo = "";
    @Getter
    private boolean disableStatus = true;
    @Getter
    private CategoryTagEditVO categoryTagEditVO;
    @Getter
    private List<MenuTagSearchVO> menuTagSearchVOs;
    @Setter
    @Getter
    private MenuTagSearchVO selMenuTagSearchVO;
    @Getter
    private MenuTagEditVO menuTagEditVO;
    /**
     * 實做供元件呼叫取得已選清單的方法, 會將此份名單帶入右邊的【已選擇人員】清單
     */
    Function<Map<String, Object>, List<?>> getSelectedData = (condition) -> {
        return Lists.newArrayList(this.menuTagEditVO.getReqFinalApplyUserSid());
    };
    @Getter
    private List<TagSearchVO> subTagSearchVOs;
    @Setter
    @Getter
    private TagSearchVO selSubTagSearchVO;
    @Getter
    private SubTagEditVO subTagEditVO;
    /**
     * DataTablePickList 物件
     */
    @Getter
    @Setter
    private DualListModel<UserViewVO> signDataPickList;
    /**
     * DataTablePickList 物件
     */
    @Getter
    @Setter
    private DualListModel<UserViewVO> transDataPickList;
    @Getter
    @Setter
    private DualListModel<UserViewVO> managerReceviceDataPickList;
    @Getter
    @Setter
    private DualListModel<UserViewVO> receverDataPickList;
    private DepTreeComponent execDepTreeComponent;
    private DepTreeComponent viewDepTreeComponent;
    private DepTreeComponent assignDepTreeComponent;
    private DepTreeComponent tagUserDepTreeComponent;
    private LinkedHashSet<UserViewVO> allCompUser;
    private LinkedHashSet<UserViewVO> canReceiveUsers;
    private Org selExecDep;
    @Getter
    private String selExecDepName;
    private List<Org> selectExecOrgs;
    private List<Org> selectViewOrgs;
    private List<Org> selectAssignOrgs;
    private List<Org> selectTagUserOrgs;
    @Getter
    @Setter
    private String execDepDataFilterName;
    @Getter
    @Setter
    private String execDeptransDataFilterName;
    @Getter
    @Setter
    private String execDepManagerReceviceDataFilterName;
    @Getter
    @Setter
    private String execDepReceverDataFilterName;
    @Getter
    @Setter
    private String cyWindow;
    @Getter
    @Setter
    private boolean isNeedAssignUser;
    @Getter
    @Setter
    private boolean isReadAssignUserSetting;
    @Getter
    @Setter
    private boolean isTransDepContainFollowing;
    @Getter
    @Setter
    private boolean isOnlyReceviceUser;
    @Getter
    @Setter
    private String execDesc;
    @Getter
    private List<ExecDepSettingSearchVO> execDepSettingSearchVOs;
    @Getter
    @Setter
    private ExecDepSettingSearchVO selExecDepSettingSearchVO;
    private String wc_exec_dep_setting_sid;
    @Getter
    private String deleteMsg;
    @Getter
    @Setter
    private boolean isUploadPanel = false;
    @Getter
    @Setter
    private boolean legitimateRangeEdit;
    @Getter
    @Setter
    private boolean startEndEdit;
    @Getter
    private List<SelectItem> tagTypeItems;
    /**
     * 核決權限維護使用
     */
    @Getter
    @Setter
    private boolean fullversion;

    private List<Org> useOrgsPicker;
    private List<User> useUsersPicker;
    private DepTreePickerComponent useDepTreePickerComponent;
    private List<Org> readOrgsPicker;
    private List<User> readUsersPicker;
    private DepTreePickerComponent readDepTreePickerComponent;
    /**
     * 指派單位：顯示文字
     */
    @Setter
    @Getter
    private String transDepsShow;
    /**
     * 指派單位：顯示文字 - ToolTip
     */
    @Setter
    @Getter
    private String transDepsShowToolTip;
    /**
     *
     */
    @Getter
    @Setter
    private OrgUserTreeMBean reqFinalApplyUserQuickSelectedTree = new OrgUserTreeMBean();

    @Getter
    private List<User> selectUsers;
    @Getter
    private String tagUsingDep;

    @PostConstruct
    public void init() {
        userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        
        
        
        workReportSearchHeaderComponent = new WorkReportSearchHeaderComponent();
        subTagHeaderComponent = new WorkReportSearchHeaderComponent();
        searchColumnVO = wkReportCustomColumnLogicComponents.getWCTagSearchColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(
                WCReportCustomColumnUrlType.TAG_SEARCH,
                Arrays.asList(WCTagSearchColumn.values()),
                searchColumnVO.getPageCount(),
                userViewVO.getSid(),
                messageCallBack,
                dataTableReLoadCallBack);
        categoryTagEditVO = new CategoryTagEditVO();
        menuTagSearchVOs = Lists.newArrayList();
        subTagSearchVOs = Lists.newArrayList();
        execDepSettingSearchVOs = Lists.newArrayList();
        menuTagEditVO = new MenuTagEditVO();
        subTagEditVO = new SubTagEditVO();
        menuHeaderComponent = new WorkReportSearchHeaderComponent();
        deleteMsg = "";
        legitimateRangeEdit = true;
        startEndEdit = true;
        tagTypeItems = Lists.newArrayList();
        initOrgData();
        initDataPickList();
        doSearch();
        fullversion = true;
    }

    private void initDataPickList() {
        this.signDataPickList = new DualListModel<>();
        this.transDataPickList = new DualListModel<>();
        this.managerReceviceDataPickList = new DualListModel<>();
        this.receverDataPickList = new DualListModel<>();
        allCompUser = Sets.newLinkedHashSet();
        User user = WkUserCache.getInstance().findBySid(userViewVO.getSid());
        Integer compSid = user.getPrimaryOrg().getCompanySid();
        try {
            orgManager.findAllDepByCompSid(compSid).stream()
                    .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                    .collect(Collectors.toList())
                    .forEach(
                            item -> {
                                allCompUser.addAll(this.wcUserLogicComponents.findByDepSid(item.getSid()));
                            });
        } catch (Exception e) {
            log.warn(" orgManager.findAllDepByCompSid", e);
        }
    }

    /**
     * 執行單位簽核人員查詢
     */
    public void filterSignName() {
        if (Strings.isNullOrEmpty(execDepDataFilterName)) {
            signDataPickList.setSource(
                    removeTargetUserView(Lists.newArrayList(allCompUser),
                            signDataPickList.getTarget()));
        } else {
            signDataPickList.setSource(
                    removeTargetUserView(
                            allCompUser.stream()
                                    .filter(
                                            each -> each.getName()
                                                    .toLowerCase()
                                                    .contains(execDepDataFilterName.toLowerCase()))
                                    .collect(Collectors.toList()),
                            signDataPickList.getTarget()));
        }
    }

    public void checkHasReceiverLoadExecOrg() {
        loadExecOrgs();
        if (CollectionUtils.isNotEmpty(receverDataPickList.getTarget())) {
            DisplayController.getInstance().showPfWidgetVar("has_receiver_dialog_wv");
            return;
        }
        DisplayController.getInstance().showPfWidgetVar("execdepUnitDlg");
    }

    public void loadExecOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = true;
        Org group = new Org(compViewVo.getSid());
        execDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                selectExecOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    public void filterTransName() {
        if (Strings.isNullOrEmpty(execDeptransDataFilterName)) {
            transDataPickList.setSource(
                    removeTargetUserView(Lists.newArrayList(allCompUser),
                            transDataPickList.getTarget()));
        } else {
            transDataPickList.setSource(
                    removeTargetUserView(
                            allCompUser.stream()
                                    .filter(
                                            each -> each.getName()
                                                    .toLowerCase()
                                                    .contains(execDeptransDataFilterName.toLowerCase()))
                                    .collect(Collectors.toList()),
                            transDataPickList.getTarget()));
        }
    }

    /**
     * 管理領單人員設定查詢
     */
    public void filterManagerReceviceName() {
        if (Strings.isNullOrEmpty(this.execDepManagerReceviceDataFilterName)) {
            managerReceviceDataPickList.setSource(
                    removeTargetUserView(
                            Lists.newArrayList(allCompUser), this.managerReceviceDataPickList.getTarget()));
        } else {
            managerReceviceDataPickList.setSource(
                    removeTargetUserView(
                            allCompUser.stream()
                                    .filter(
                                            each -> each.getName()
                                                    .toLowerCase()
                                                    .contains(execDepManagerReceviceDataFilterName.toLowerCase()))
                                    .collect(Collectors.toList()),
                            managerReceviceDataPickList.getTarget()));
        }
    }

    /**
     * 領單人員設定查詢
     */
    public void filterReceiverName() {

        if (selExecDep == null) {
            MessagesUtils.showInfo("請先選擇執行部門!");
            return;
        }

        if (Strings.isNullOrEmpty(this.execDepReceverDataFilterName)) {
            List<UserViewVO> source = Lists.newArrayList();
            if (WkStringUtils.notEmpty(canReceiveUsers)) {
                source = Lists.newArrayList(canReceiveUsers);
            }
            receverDataPickList.setSource(
                    removeTargetUserView(
                            source, this.receverDataPickList.getTarget()));
        } else {
            receverDataPickList.setSource(
                    removeTargetUserView(
                            canReceiveUsers.stream()
                                    .filter(
                                            each -> each.getName()
                                                    .toLowerCase()
                                                    .contains(execDepReceverDataFilterName.toLowerCase()))
                                    .collect(Collectors.toList()),
                            receverDataPickList.getTarget()));
        }
    }

    public void searchTagItems() {
        try {
            subTagSearchVOs.clear();
            subTagSearchVOs.addAll(
                    wCTagLogicComponents.getTagSearch(
                            null,
                            selMenuTagSearchVO.getSid(),
                            subTagHeaderComponent.isHideInactiveTag()
                    ));
            DisplayController.getInstance().update("tag_list_datatable_id");
        } catch (Exception e) {
            log.warn("searchTagItems Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void searchExecItems() {
        try {
            execDepSettingSearchVOs.clear();
            execDepSettingSearchVOs.addAll(
                    execDepSettingLogicComponent.getExecDepSettingSearchVO(
                            selSubTagSearchVO.getSid(), this.fullversion));
            DisplayController.getInstance().update("execdep_list_datatable_id");
        } catch (Exception e) {
            log.warn("searchTagItems Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void doSearch() {
        try {
            tagSearchVO = doSearch("");
            if (tagSearchVO.size() > 0) {
                DisplayController.getInstance()
                        .execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
        } catch (Exception e) {
            log.warn("doSearch Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clear() {
        workReportSearchHeaderComponent.settingDefault();
        intOrgTree();
        DisplayController.getInstance().update("depDialogPanel");
        tagSearchVO = Lists.newArrayList();
        doSearch();
    }

    private List<CategoryTagSearchVO> doSearch(String sid) {
        return wcCategoryTagLogicComponent.getCategoryTagSearch(
                workReportSearchHeaderComponent.getSelectStatus(),
                workReportSearchHeaderComponent.getTagName(),
                sid,
                compViewVo.getSid(),
                workReportSearchHeaderComponent.isHideInactiveCategory()
        );
    }

    private void initOrgData() {
        allOrgs = Lists.newArrayList();
        allOrgs.addAll(orgManager.findAllDepByCompSid(compViewVo.getSid()));
        depTreeComponent = new DepTreeComponent();
        execDepTreeComponent = new DepTreeComponent();
        viewDepTreeComponent = new DepTreeComponent();
        tagUserDepTreeComponent = new DepTreeComponent();
        assignDepTreeComponent = new DepTreeComponent();
        useDepTreePickerComponent = new DepTreePickerComponent();
        readDepTreePickerComponent = new DepTreePickerComponent();

        intOrgTree();
    }

    private void intOrgTree() {
        selectOrgs = Lists.newArrayList();
        selectExecOrgs = Lists.newArrayList();
        selectViewOrgs = Lists.newArrayList();
        useOrgsPicker = Lists.newArrayList();
        readOrgsPicker = Lists.newArrayList();
        selectAssignOrgs = Lists.newArrayList();
        selectTagUserOrgs = Lists.newArrayList();

        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());
        depTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        boolean selectExecModeSingle = true;
        execDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectExecOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectExecModeSingle);

        viewDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectViewOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        useDepTreePickerComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                useOrgsPicker,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        readDepTreePickerComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                readOrgsPicker,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        assignDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectAssignOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        tagUserDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectTagUserOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    /**
     * 儲存執行項目設定
     */
    public void saveTag() {
        if (Activation.INACTIVE.name().equals(subTagEditVO.getStatus())) {
            List<String> templateList = wcTagTempletManager.findTemplateByExecTagSid(subTagEditVO.getSid())
                    .stream().map(x -> x.getUsename())
                    .collect(Collectors.toList());

            if (templateList.size() != 0) {
                tagUsingDep = "執行項目模版名稱：「" + templateList.toString().replace("[", "").replace("]", "") + "」正在使用";
                MessagesUtils.showWarn(tagUsingDep);
                return;
            }
        }

        try {
            wCTagLogicComponents.saveTag(
                    subTagEditVO.getSid(),
                    subTagEditVO.getName(),
                    subTagEditVO.getStatus(),
                    subTagEditVO.getMemo(),
                    userViewVO.getSid(),
                    selMenuTagSearchVO.getSid(),
                    subTagEditVO.getReqFlowType(),
                    subTagEditVO.getDefaultContentCss(),
                    subTagEditVO.getDefaultMemoCss(),
                    this.selectViewOrgs,
                    this.readUsersPicker,
                    subTagEditVO.isLegitimateRangeCheck(),
                    subTagEditVO.getLegitimateRangeDays(),
                    subTagEditVO.getLegitimateRangeTitle(),
                    subTagEditVO.isStartEndCheck(),
                    subTagEditVO.getStartEndDays(),
                    subTagEditVO.getStartEndTitle(),
                    subTagEditVO.getEndRuleDays(),
                    this.selectTagUserOrgs,
                    subTagEditVO.isViewContainFollowing(),
                    subTagEditVO.isUserContainFollowing(),
                    subTagEditVO.isOnlyForUseDepManager(),
                    subTagEditVO.getUseDepTempletSid(),
                    subTagEditVO.getCanUserDepTempletSid(),
                    subTagEditVO.getTagType(),
                    subTagEditVO.getTagDesc(),
                    subTagEditVO.isUploadFile());
            this.searchTagItems();
            DisplayController.getInstance().hidePfWidgetVar("subTag_maintain_setting_dlg_wv");
            this.selectTagUserOrgs=Lists.newArrayList();
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("saveTag：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("saveTag", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void saveCategory() {
        try {
            wcCategoryTagLogicComponent.saveCategoryTag(
                    categoryTagEditVO.getSid(),
                    categoryTagEditVO.getCategoryName(),
                    categoryTagEditVO.getStatus(),
                    categoryTagEditVO.getMemo(),
                    userViewVO.getSid(),
                    compViewVo.getSid());

            tagSearchVO = doSearch("");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("saveCategory：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("saveCategory", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void addCategoryTag() {
        categoryTagEditVO = new CategoryTagEditVO();
        DisplayController.getInstance().update("category_maintain_setting_dlg_panel_id");
        DisplayController.getInstance().showPfWidgetVar("category_maintain_setting_dlg_wv");
    }

    public void addExec() {
        try {
            this.selExecDep = null;
            this.selExecDepName = "";
            this.selectExecOrgs = Lists.newArrayList();
            this.signDataPickList.setSource(Lists.newArrayList());
            this.signDataPickList.setTarget(Lists.newArrayList());
            this.transDataPickList.setSource(Lists.newArrayList());
            this.transDataPickList.setTarget(Lists.newArrayList());
            this.managerReceviceDataPickList.setSource(Lists.newArrayList());
            this.managerReceviceDataPickList.setTarget(Lists.newArrayList());
            this.canReceiveUsers = Sets.newLinkedHashSet();
            this.receverDataPickList.setSource(Lists.newArrayList());
            this.receverDataPickList.setTarget(Lists.newArrayList());
            this.execDepDataFilterName = "";
            this.execDeptransDataFilterName = "";
            this.isNeedAssignUser = false;
            this.isReadAssignUserSetting = false;
            this.isOnlyReceviceUser = false;
            this.wc_exec_dep_setting_sid = "";
            DisplayController.getInstance().update("execdepDialogPanel");
            DisplayController.getInstance().update("execDep_data_dlg_panel_id");
            DisplayController.getInstance().showPfWidgetVar("execDep_data_dlg_wv");
        } catch (Exception e) {
            log.warn("addExec ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟設定執行單位dialog
     */
    public void loadExec() {
        try {
            if (selExecDepSettingSearchVO != null
                    && !Strings.isNullOrEmpty(selExecDepSettingSearchVO.getWc_exec_dep_setting_sid())) {
                WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(
                        selExecDepSettingSearchVO.getWc_exec_dep_setting_sid());
                this.selExecDep = orgManager.findBySid(execDepSetting.getExec_dep_sid());
                this.selExecDepName = selExecDep.getName();
                this.selectExecOrgs = Lists.newArrayList(this.selExecDep);
                List<UserViewVO> signData = Lists.newArrayList();
                if (execDepSetting.getExecSignMember() != null
                        && execDepSetting.getExecSignMember().getUserTos() != null) {
                    execDepSetting
                            .getExecSignMember()
                            .getUserTos()
                            .forEach(
                                    item -> {
                                        signData.add(new UserViewVO(item.getSid()));
                                    });
                }
                List<UserViewVO> transData = Lists.newArrayList();
                if (execDepSetting.getTransUser() != null
                        && execDepSetting.getTransUser().getUserTos() != null) {
                    execDepSetting
                            .getTransUser()
                            .getUserTos()
                            .forEach(
                                    item -> {
                                        transData.add(new UserViewVO(item.getSid()));
                                    });
                }

                List<Org> transOrg = Lists.newArrayList();
                if (execDepSetting.getTransdep() != null
                        && execDepSetting.getTransdep().getUserDepTos() != null) {
                    execDepSetting
                            .getTransdep()
                            .getUserDepTos()
                            .forEach(
                                    each -> {
                                        transOrg.add(
                                                orgManager.findBySid(Integer.valueOf(each.getDepSid())));
                                    });
                }

                List<UserViewVO> managerData = Lists.newArrayList();
                if (execDepSetting.getManagerRecevice() != null
                        && execDepSetting.getManagerRecevice().getUserTos() != null) {
                    execDepSetting
                            .getManagerRecevice()
                            .getUserTos()
                            .forEach(
                                    item -> {
                                        managerData.add(
                                                new UserViewVO(item.getSid()));
                                    });
                }

                List<Org> orgs = Lists.newArrayList(this.selExecDep);
                orgs.addAll(
                        orgManager.findAllChild(this.selExecDep.getSid()).stream()
                                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                .collect(Collectors.toList()));
                canReceiveUsers = Sets.newLinkedHashSet();
                orgs.forEach(
                        item -> {
                            canReceiveUsers.addAll(
                                    this.wcUserLogicComponents.findByDepSid(item.getSid()));
                        });

                List<UserViewVO> receverData = Lists.newArrayList();
                if (execDepSetting.getReceviceUser() != null
                        && execDepSetting.getReceviceUser().getUserTos() != null) {
                    execDepSetting
                            .getReceviceUser()
                            .getUserTos()
                            .forEach(
                                    item -> {
                                        receverData.add(
                                                new UserViewVO(item.getSid()));
                                    });
                }
                this.signDataPickList.setSource(Lists.newArrayList());
                this.signDataPickList.setTarget(signData);
                this.transDataPickList.setSource(Lists.newArrayList());
                this.transDataPickList.setTarget(transData);
                this.managerReceviceDataPickList.setSource(Lists.newArrayList());
                this.managerReceviceDataPickList.setTarget(managerData);
                this.receverDataPickList.setTarget(receverData);
                this.receverDataPickList.setSource(
                        removeTargetUserView(
                                Lists.newArrayList(canReceiveUsers),
                                this.receverDataPickList.getTarget()));
                this.selectAssignOrgs = transOrg;
                this.prepareExecDepTransDepsSettingContent();
                this.execDepDataFilterName = "";
                this.execDeptransDataFilterName = "";
                this.execDepManagerReceviceDataFilterName = "";
                this.execDepReceverDataFilterName = "";
                this.isNeedAssignUser = execDepSetting.isNeedAssigned();
                this.isReadAssignUserSetting = execDepSetting.isReadAssignSetting();
                this.isTransDepContainFollowing = execDepSetting.isTransDepContainFollowing();
                this.isOnlyReceviceUser = execDepSetting.isOnlyReceviceUser();
                this.wc_exec_dep_setting_sid = execDepSetting.getSid();
                this.cyWindow = execDepSetting.getContact();
                this.execDesc = execDepSetting.getExecDesc();
                DisplayController.getInstance().update("execdepDialogPanel");
                DisplayController.getInstance().update("execDep_data_dlg_panel_id");
                DisplayController.getInstance().update("execDep_data_dlg_panel_transDeps_show");
                DisplayController.getInstance().showPfWidgetVar("execDep_data_dlg_wv");
            } else {
                messageCallBack.showMessage("無取到類別資料");
            }
        } catch (Exception e) {
            log.warn("loadCategoryTag ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void toDeleteExecDep(String wc_exec_dep_setting_sid) {
        try {

            int index = execDepSettingSearchVOs.indexOf(
                    new ExecDepSettingSearchVO(wc_exec_dep_setting_sid));
            this.selExecDepSettingSearchVO = execDepSettingSearchVOs.get(index);
            // ========================================================================
            // 檢查是否有相關單據尚未結案
            // ========================================================================
            List<WCExceDep> exceDeps = wcExecDepManager.getByExecDepSettingSid(
                    wc_exec_dep_setting_sid);
            List<Integer> execDepOrgSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(exceDeps)) {
                execDepOrgSids = exceDeps.stream()
                        .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                        .filter(
                                item -> !item.getExecDepStatus().equals(WCExceDepStatus.CLOSE)
                                        && !item.getExecDepStatus().equals(WCExceDepStatus.STOP))
                        .map(WCExceDep::getDep_sid)
                        .distinct()
                        .collect(Collectors.toList());
            }
            if (WkStringUtils.notEmpty(execDepOrgSids)) {
                // 有相關單據尚未結案
                this.deleteMsg = "尚有單據未結案，刪除執行單位《"
                        + this.selExecDepSettingSearchVO.getDepName()
                        + "》，未結案單據則會遺留在《"
                        + this.selExecDepSettingSearchVO.getDepName()
                        + "》單位上，<br>"
                        + "若此單位已停用，或即將停用，可能會造成單據無法順利執行，請問確定要刪除嗎？<br>"
                        + "【確定】將單據留在《"
                        + this.selExecDepSettingSearchVO.getDepName()
                        + "》單位；<br>"
                        + "【取消】取消刪除。";
            } else {
                // 相關單據皆已結案
                this.deleteMsg = "是否刪除(" + this.selExecDepSettingSearchVO.getDepName() + ")";
            }
            DisplayController.getInstance().showPfWidgetVar("execDep_list_rm_dlg_wv");
        } catch (Exception e) {
            log.warn("toDeleteExecDep Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void doDeleteExecDep() {
        try {
            execDepSettingLogicComponent.doDeleteExecDepSetting(
                    this.selExecDepSettingSearchVO.getWc_exec_dep_setting_sid(), userViewVO.getSid());
            this.searchExecItems();
            this.searchTagItems();
            DisplayController.getInstance().execute("loadALLTags()");
        } catch (Exception e) {
            log.warn("doDeleteExecDep Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 儲存執行單位設定
     */
    public void saveExec() {
        try {
            List<Integer> signUserSids = Lists.newArrayList();
            this.signDataPickList
                    .getTarget()
                    .forEach(
                            item -> {
                                signUserSids.add(item.getSid());
                            });
            List<Integer> assignUserSids = Lists.newArrayList();
            this.transDataPickList
                    .getTarget()
                    .forEach(
                            item -> {
                                assignUserSids.add(item.getSid());
                            });

            List<Integer> managerReceviceUserSids = Lists.newArrayList();
            this.managerReceviceDataPickList
                    .getTarget()
                    .forEach(
                            item -> {
                                managerReceviceUserSids.add(item.getSid());
                            });
            List<Integer> receviceUserSids = Lists.newArrayList();
            this.receverDataPickList
                    .getTarget()
                    .forEach(
                            item -> {
                                receviceUserSids.add(item.getSid());
                            });
            List<String> assignOrgSids = Lists.newArrayList();
            selectAssignOrgs.forEach(
                    each -> {
                        assignOrgSids.add(String.valueOf(each.getSid()));
                    });

            execDepSettingLogicComponent.saveExecDepSetting(
                    wc_exec_dep_setting_sid,
                    userViewVO.getSid(),
                    selExecDep,
                    signUserSids,
                    isNeedAssignUser,
                    isReadAssignUserSetting,
                    assignOrgSids,
                    assignUserSids,
                    managerReceviceUserSids,
                    receviceUserSids,
                    isOnlyReceviceUser,
                    selSubTagSearchVO.getSid(),
                    cyWindow,
                    this.isTransDepContainFollowing,
                    execDesc);
            this.searchExecItems();
            this.searchTagItems();
            this.selectExecOrgs = Lists.newArrayList();
            DisplayController.getInstance().update("execdepDialogPanel");
            DisplayController.getInstance().hidePfWidgetVar("execDep_data_dlg_wv");
            DisplayController.getInstance().execute("loadALLTags()");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("saveExec ERROR：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("saveExec ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void addSubTag() {
        subTagEditVO = new SubTagEditVO();
        clickLegitimateRangeCheck();
        clickStartEndCheck();

        // 可使用單位:模版選項
        this.subTagEditVO.setCanUserDepTempletItems(
                this.wCTagLogicComponents.getDepsTempletItems(ConfigTempletType.SUB_TAG_CAN_USE));
        // 可使用單位:設定顯示文字
        this.prepareSubTagCanUserDepsSettingContent();

        // 預設可閱部門:模版選項
        this.subTagEditVO.setUseDepTempletItems(
                this.wCTagLogicComponents.getDepsTempletItems(ConfigTempletType.SUB_TAG_CAN_READ));
        // 預設可閱部門:設定顯示文字
        this.prepareSubTagUseDepSettingContent();

        DisplayController.getInstance().update("subTag_maintain_setting_dlg_panel_id");
        DisplayController.getInstance().showPfWidgetVar("subTag_maintain_setting_dlg_wv");
    }

    /**
     * 點選大項列表- 進入大項維護功能
     */
    public void loadCategoryTag() {
        try {
            if (selTagSearchVO != null && !Strings.isNullOrEmpty(selTagSearchVO.getSid())) {
                categoryTagEditVO = wcCategoryTagLogicComponent.getCategoryTagEditVOBySid(selTagSearchVO.getSid());
                DisplayController.getInstance().update("category_maintain_setting_dlg_panel_id");
                DisplayController.getInstance().showPfWidgetVar("category_maintain_setting_dlg_wv");
            } else {
                messageCallBack.showMessage("無取到類別資料");
            }
        } catch (Exception e) {
            log.warn("loadCategoryTag ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟小項執行單位設定
     *
     * @param sid
     */
    public void loadExecDepBySubTagSid(String sid) {
        try {
            TagSearchVO ctv = new TagSearchVO(sid);
            int index = subTagSearchVOs.indexOf(ctv);
            this.selSubTagSearchVO = subTagSearchVOs.get(index);
            DisplayController.getInstance().update("tag_list_datatable_id");
            this.searchExecItems();
            DisplayController.getInstance().update("execdep_list_header_id");
            DisplayController.getInstance().showPfWidgetVar("execdep_list_dlg_wv");
        } catch (Exception e) {
            log.warn("loadExecDepBySubTagSid ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟小類
     *
     * @param sid
     */
    public void loadSubTagByMenuSid(String sid) {
        try {
            MenuTagSearchVO ctv = new MenuTagSearchVO(sid);
            int index = menuTagSearchVOs.indexOf(ctv);
            this.selMenuTagSearchVO = menuTagSearchVOs.get(index);
            DisplayController.getInstance().update("menu_list_datatable_id");
            subTagHeaderComponent = new WorkReportSearchHeaderComponent();
            this.searchTagItems();
            DisplayController.getInstance().update("tag_list_header_id");
            DisplayController.getInstance().showPfWidgetVar("tag_list_dlg_wv");
        } catch (Exception e) {
            log.warn("loadSubTagByMenuSid ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 事件：依據大類 SID , 開啟類別（中類）列表
     *
     * @param sid
     */
    public void loadMenuByCategorySid(String sid) {
        try {
            CategoryTagSearchVO ctv = new CategoryTagSearchVO(sid);
            int index = tagSearchVO.indexOf(ctv);
            this.selTagSearchVO = tagSearchVO.get(index);
            DisplayController.getInstance().update(dataTableID);
            menuHeaderComponent = new WorkReportSearchHeaderComponent();
            searchMenuItems();
            DisplayController.getInstance().update("menu_list_header_id");
            DisplayController.getInstance().showPfWidgetVar("menu_list_dlg_wv");
        } catch (Exception e) {
            log.warn("loadMenuByCategorySid ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void searchMenuItems() {
        try {
            menuTagSearchVOs.clear();

            //處理不顯示停用中類
            Activation status = null;
            if (menuHeaderComponent.isHideInactiveMenuTag()) {
                status = Activation.ACTIVE;
            }

            menuTagSearchVOs.addAll(
                    wcMenuTagLogicComponent.getMenuTagSearchVO(
                            menuHeaderComponent.getTagName(), selTagSearchVO.getSid(), status));

            DisplayController.getInstance().update("menu_list_datatable_id");
        } catch (Exception e) {
            log.warn("searchMenuItems ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadSubTag() {
        try {
            if (selSubTagSearchVO != null && !Strings.isNullOrEmpty(selSubTagSearchVO.getSid())) {

                // ====================================
                // 取得小類(執行項目)檔資料
                // ====================================
                // 查詢
                this.subTagEditVO = this.wCTagLogicComponents.getSubTagEditVO(
                        selSubTagSearchVO.getSid());

                // 可使用單位
                this.selectTagUserOrgs = this.subTagEditVO.getCanUserDeps();
                // 可使用單位:模版選項
                this.subTagEditVO.setCanUserDepTempletItems(
                        this.wCTagLogicComponents.getDepsTempletItems(
                                ConfigTempletType.SUB_TAG_CAN_USE));
                // 可使用單位:設定顯示文字
                this.prepareSubTagCanUserDepsSettingContent();

                // 預設可閱部門
                this.selectViewOrgs = this.subTagEditVO.getOrgs();
                // 預設可閱部門:模版選項
                this.subTagEditVO.setUseDepTempletItems(
                        this.wCTagLogicComponents.getDepsTempletItems(
                                ConfigTempletType.SUB_TAG_CAN_READ));
                // 預設可閱部門:設定顯示文字
                this.prepareSubTagUseDepSettingContent();

                this.readUsersPicker = subTagEditVO.getUsers();

                List<Org> selOrgs = Lists.newArrayList();
                this.readUsersPicker.forEach(
                        each -> {
                            selOrgs.add(orgManager.findBySid(each.getPrimaryOrg().getSid()));
                        });
                this.readOrgsPicker = selOrgs;
                this.clickLegitimateRangeCheck();
                this.clickStartEndCheck();
                loadViewOrgs();

                // 查詢顯示分類清單Item
                this.tagTypeItems = this.wCTagLogicComponents.getTagTypeItems(
                        selMenuTagSearchVO.getSid());

                DisplayController.getInstance().update("viewdepDialogPanel");
                DisplayController.getInstance().update("subTag_maintain_setting_dlg_panel_id");
                DisplayController.getInstance().showPfWidgetVar("subTag_maintain_setting_dlg_wv");
            } else {
                messageCallBack.showMessage("無取到執行項目資料");
            }
        } catch (Exception e) {
            log.warn("loadSubTag ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 開啟類別選單(中類項目)維護
     */
    public void loadMenuTag() {
        try {

            this.wcMenuTagComponent.init();

            if (selMenuTagSearchVO == null || Strings.isNullOrEmpty(selMenuTagSearchVO.getSid())) {
                messageCallBack.showMessage("無取到類別資料");
                return;
            }

            // 取得選擇的中類項目資料
            this.menuTagEditVO = this.wcMenuTagLogicComponent.getMenuTagEditVO(selMenuTagSearchVO.getSid());

            isUploadPanel = menuTagEditVO.isAttachmentType();
            if (isUploadPanel) {
                File[] files = AttachmentUtils.getFolderFiles(menuTagEditVO.getAttachmentPath());
                for (File file : files) {
                    wcMenuTagComponent
                            .getAttachments()
                            .put(file.getName(),
                                    new ByteArrayInputStream(Files.readAllBytes(file.toPath())));
                    wcMenuTagComponent.getFinalUploadFiles().put(file.getName(), true);
                }
            }
            this.selectOrgs = menuTagEditVO.getOrgs();
            // 取得部門套用模版可選的選項
            List<WCConfigTempletVo> templets = this.wcMenuTagLogicComponent.getUseDepsTempletItems();
            this.menuTagEditVO.setUesDepsTempletItems(templets);
            // 產生可用部門顯示文字
            this.prepareMenuTagUseDepSettingContent();

            useUsersPicker = menuTagEditVO.getUsers();

            // 產生可使用人員顯示文字
            this.prepareMenuTagUseUsersSettingContent();

            this.useOrgsPicker = Lists.newArrayList();
            if (WkStringUtils.notEmpty(useUsersPicker)) {
                this.useOrgsPicker = useUsersPicker.stream()
                        .map(user -> WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid()))
                        .collect(Collectors.toList());
            }


            DisplayController.getInstance().update("depDialogPanel");
            DisplayController.getInstance().update("menu_maintain_setting_dlg_panel_id");
            DisplayController.getInstance().showPfWidgetVar("menu_maintain_setting_dlg_wv");

        } catch (Exception e) {
            log.warn("loadCategoryTag ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void addMenuTag() {
        try {
            menuTagEditVO = new MenuTagEditVO();
            this.selectOrgs = menuTagEditVO.getOrgs();

            // 取得部門套用模版可選的選項
            this.menuTagEditVO.setUesDepsTempletItems(
                    this.wcMenuTagLogicComponent.getUseDepsTempletItems());
            // 產生可使用部門顯示文字
            this.prepareMenuTagUseDepSettingContent();
            // 產生可使用人員顯示文字
            this.prepareMenuTagUseUsersSettingContent();

            DisplayController.getInstance().update("depDialogPanel");
            DisplayController.getInstance().update("menu_maintain_setting_dlg_panel_id");
            DisplayController.getInstance().showPfWidgetVar("menu_maintain_setting_dlg_wv");
        } catch (Exception e) {
            log.warn("addMenuTag ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 單據名稱維護：儲存
     */
    public void saveMenuTag() {
        try {
            if (!isUploadPanel) {
                // 一般維護
                wcMenuTagLogicComponent.saveMenuTag(
                        menuTagEditVO.getSid(),
                        selTagSearchVO.getSid(),
                        selectOrgs,
                        useUsersPicker,
                        menuTagEditVO.getName(),
                        menuTagEditVO.getItemName(),
                        menuTagEditVO.getMemo(),
                        menuTagEditVO.getStatus(),
                        // menuTagEditVO.getSeq(),
                        menuTagEditVO.getDefaultTheme(),
                        menuTagEditVO.getDefaultContent(),
                        menuTagEditVO.getDefaultMemo(),
                        menuTagEditVO.isToParentManager(),
                        userViewVO.getSid(),
                        menuTagEditVO.isUserContainFollowing(),
                        menuTagEditVO.getReqFinalApplyUserSid(),
                        menuTagEditVO.getUesDepTempletSid(),
                        menuTagEditVO.getReqFinalApplyIgnoredWcTagSids(),
                        menuTagEditVO.isOnlyForUseDepManager());

            } else {
                // 附加檔案類型
                String errorMsg = "";
                Map<String, ByteArrayInputStream> attachments = Maps.newHashMap();
                for (String key : wcMenuTagComponent.getFinalUploadFiles().keySet()) {
                    if (wcMenuTagComponent.getFinalUploadFiles().get(key)) {
                        attachments.put(key, wcMenuTagComponent.getAttachments().get(key));
                    }
                }
                if (Strings.isNullOrEmpty(menuTagEditVO.getName())) {
                    errorMsg = "單據名稱為必要輸入欄位，請確認！<br>";
                }
                if (attachments.size() == 0) {
                    errorMsg += "尚未上傳附加檔案，請確認！";
                }
                Preconditions.checkState(Strings.isNullOrEmpty(errorMsg), errorMsg);
                wcMenuTagLogicComponent.saveAttachMenuTag(
                        attachments, selTagSearchVO.getSid(), menuTagEditVO,
                        SecurityFacade.getUserSid());
                wcMenuTagComponent.init();
            }
            searchMenuItems();
            DisplayController.getInstance().hidePfWidgetVar("menu_maintain_setting_dlg_wv");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("save category fail..：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("save category fail..", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     *
     */
    public void loadTag() {
        try {
            if (selTagSearchVO != null && !Strings.isNullOrEmpty(selTagSearchVO.getSid())) {
                WCTag selWCTag = wCTagLogicComponents.getWCTagBySid(selTagSearchVO.getSid());
                if (selWCTag == null) {
                    messageCallBack.showMessage("主檔資料比對有誤,無法開啟");
                    return;
                }
                List<Org> selOrg = Lists.newArrayList();
                selectOrgs = selOrg;
                DisplayController.getInstance().update("depDialogPanel");
                tagName = selWCTag.getTagName();
                disableStatus = !Activation.ACTIVE.equals(selWCTag.getStatus());
                statusID = selWCTag.getStatus().name();
                memo = selWCTag.getMemo();
                this.reqFlowType = (selWCTag.getReqFlowType() != null) ? selWCTag.getReqFlowType().name() : "";
            } else {
                disableStatus = false;

                selectOrgs = Lists.newArrayList();
                DisplayController.getInstance().update("depDialogPanel");
                tagName = "";
                statusID = "";
                this.reqFlowType = "";
                memo = "";
                seq = String.valueOf((wCTagLogicComponents.getMaxTagSeq() + 1));
                this.defaultTheme = "";
                this.defaultContent = "";
                this.defaultMemo = "";
            }
        } catch (Exception e) {
            log.warn("loadTag", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        boolean defaultShowDisableDep = false;
        for (Org sO : selectOrgs) {
            if (Activation.INACTIVE.equals(sO.getStatus())) {
                defaultShowDisableDep = true;
                break;
            }
        }
        Org group = new Org(compViewVo.getSid());
        depTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                selectOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle,
                defaultShowDisableDep);
    }

    public void loadAssignOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        boolean defaultShowDisableDep = false;
        for (Org sO : selectAssignOrgs) {
            if (Activation.INACTIVE.equals(sO.getStatus())) {
                defaultShowDisableDep = true;
                break;
            }
        }
        Org group = new Org(compViewVo.getSid());
        assignDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                selectAssignOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle,
                defaultShowDisableDep);
    }

    /**
     * 中類 - 可使用部門
     */
    public void doSelectOrg() {
        selectOrgs = depTreeComponent.getSelectOrg();
        this.prepareMenuTagUseDepSettingContent();
        DisplayController.getInstance().update("menu_maintain_setting_dlg_panel_use_dep_show");
        DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
    }

    /**
     * 隱藏選項設定-可使用部門
     */
    public void doSelectTagUserOrg() {
        this.selectTagUserOrgs = this.tagUserDepTreeComponent.getSelectOrg();
        this.prepareSubTagCanUserDepsSettingContent();
        DisplayController.getInstance()
                .update("subTag_maintain_setting_dlg_panel_canUserDeps_show");
        DisplayController.getInstance().hidePfWidgetVar("tagUserDeptUnitDlg");
    }

    /**
     * 隱藏選項設定-預設可閱單位
     */
    public void doSelectViewOrg() {
        this.selectViewOrgs = this.viewDepTreeComponent.getSelectOrg();
        this.prepareSubTagUseDepSettingContent();
        DisplayController.getInstance().update("subTag_maintain_setting_dlg_panel_UseDep_show");
        DisplayController.getInstance().hidePfWidgetVar("viewdeptUnitDlg");
    }

    public void doSelectAssignOrg() {
        this.selectAssignOrgs = this.assignDepTreeComponent.getSelectOrg();
        this.prepareExecDepTransDepsSettingContent();
        DisplayController.getInstance().update("execDep_data_dlg_panel_transDeps_show");
        DisplayController.getInstance().hidePfWidgetVar("assigndeptUnitDlg");
    }

    public void doSelectExecOrg() {

        // 取得選擇的執行部門
        selectExecOrgs = execDepTreeComponent.getSelectOrg();
        // 關閉選單
        DisplayController.getInstance().hidePfWidgetVar("execdepUnitDlg");

        if (selectExecOrgs == null || selectExecOrgs.isEmpty()) {
            this.selExecDep = null;
            this.selExecDepName = "";
        } else {
            this.selExecDep = selectExecOrgs.get(0);
            this.selExecDepName = selExecDep.getName();

            if (selExecDep != null) {
                // ========================================================================
                // 更新【領單人員】
                // ========================================================================

                List<Org> orgs = Lists.newArrayList(this.selExecDep);
                orgs.addAll(
                        orgManager.findAllChild(this.selExecDep.getSid()).stream()
                                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                .collect(Collectors.toList()));
                canReceiveUsers = Sets.newLinkedHashSet();
                orgs.forEach(
                        item -> {
                            canReceiveUsers.addAll(
                                    this.wcUserLogicComponents.findByDepSid(item.getSid()));
                        });

                List<UserViewVO> receiverData = Lists.newArrayList();
                this.receverDataPickList.setTarget(receiverData);
                this.receverDataPickList.setSource(
                        removeTargetUserView(
                                Lists.newArrayList(canReceiveUsers),
                                this.receverDataPickList.getTarget()));
            }
        }
        DisplayController.getInstance().update("selDepName");
        DisplayController.getInstance().update("execDep_data_dlg_recever_panel_id");
    }

    public void loadViewOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        boolean defaultShowDisableDep = false;
        for (Org sO : selectViewOrgs) {
            if (Activation.INACTIVE.equals(sO.getStatus())) {
                defaultShowDisableDep = true;
                break;
            }
        }
        Org group = new Org(compViewVo.getSid());
        viewDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.selectViewOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle,
                defaultShowDisableDep);
    }

    public void loadTagUseOrgs() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        boolean defaultShowDisableDep = false;
        for (Org sO : selectTagUserOrgs) {
            if (Activation.INACTIVE.equals(sO.getStatus())) {
                defaultShowDisableDep = true;
                break;
            }
        }
        Org group = new Org(compViewVo.getSid());
        tagUserDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.selectTagUserOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle,
                defaultShowDisableDep);
    }

    public void loadUseOrgsAndPickerUsers() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

        if (this.useOrgsPicker == null) {
            this.useOrgsPicker = Lists.newArrayList();
        }

        if (this.useUsersPicker == null) {
            this.useUsersPicker = Lists.newArrayList();
        }

        List<User> useUsers = Lists.newArrayList();
        useUsersPicker.forEach(
                each -> {
                    useUsers.add(each);
                });

        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;

        Org group = new Org(compViewVo.getSid());
        useDepTreePickerComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.useOrgsPicker,
                useUsers,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    public void loadReadOrgsAndPickerUsers() {
        List<Org> tempOrg = Lists.newArrayList();
        allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

        if (this.readOrgsPicker == null) {
            this.readOrgsPicker = Lists.newArrayList();
        }

        if (this.readUsersPicker == null) {
            this.readUsersPicker = Lists.newArrayList();
        }

        List<User> useUsers = Lists.newArrayList();
        readUsersPicker.forEach(
                each -> {
                    useUsers.add(each);
                });

        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;

        Org group = new Org(compViewVo.getSid());
        readDepTreePickerComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.readOrgsPicker,
                useUsers,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getViewDepTreeManager() {
        return this.viewDepTreeComponent.getMultipleDepTreeManager();
    }

    public MultipleDepTreeManager getTagUserDepTreeManager() {
        return this.tagUserDepTreeComponent.getMultipleDepTreeManager();
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getUseDepTreePickerManager() {
        return this.useDepTreePickerComponent.getMultipleDepTreePickerManager();
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getReadDepTreePickerManager() {
        return this.readDepTreePickerComponent.getMultipleDepTreePickerManager();
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getAssignDepTreeManager() {
        return this.assignDepTreeComponent.getMultipleDepTreeManager();
    }

    public void saveUseOrgPicker() {
        this.useOrgsPicker = useDepTreePickerComponent.getSelectOrg();
        this.useUsersPicker = useDepTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected();
        this.prepareMenuTagUseUsersSettingContent();
        DisplayController.getInstance().update("menu_maintain_setting_dlg_panel_use_user_show");
        DisplayController.getInstance().hidePfWidgetVar("useDepPickerUnitDlg");
    }

    public void saveReadOrgPicker() {
        this.readOrgsPicker = readDepTreePickerComponent.getSelectOrg();
        this.readUsersPicker = readDepTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected();
        DisplayController.getInstance().hidePfWidgetVar("readDepPickerUnitDlg");
    }

    public void openCustomColumn() {
        try {
            customColumn.loadData();
            DisplayController.getInstance().update("dlgDefineField_view");
            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
        } catch (Exception e) {
            log.error("openCustomColumn Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    private List<UserViewVO> removeTargetUserView(
            List<UserViewVO> source,
            List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
                item -> {
                    if (!target.contains(item)) {
                        filterSource.add(item);
                    }
                });
        return filterSource;
    }

    public SingleDepTreeManager getExecDepTreeManager() {
        return execDepTreeComponent.getSingleDepTreeManager();
    }

    public void clickLegitimateRangeCheck() {
        legitimateRangeEdit = subTagEditVO.isLegitimateRangeCheck() != true;
        String legitimateRangeDays = (legitimateRangeEdit == false) ? subTagEditVO.getLegitimateRangeDays() : null;
        String legitimateRangeTitle = (legitimateRangeEdit == false) ? subTagEditVO.getLegitimateRangeTitle() : null;
        subTagEditVO.setLegitimateRangeDays(legitimateRangeDays);
        subTagEditVO.setLegitimateRangeTitle(legitimateRangeTitle);
    }

    public void clickStartEndCheck() {
        startEndEdit = subTagEditVO.isStartEndCheck() != true;
        String startEndDays = (startEndEdit == false) ? subTagEditVO.getStartEndDays() : null;
        String startEndTitle = (startEndEdit == false) ? subTagEditVO.getStartEndTitle() : null;
        String endRuleDays = (startEndEdit == false) ? subTagEditVO.getEndRuleDays() : null;
        subTagEditVO.setStartEndDays(startEndDays);
        subTagEditVO.setStartEndTitle(startEndTitle);
        subTagEditVO.setEndRuleDays(endRuleDays);
    }

    /**
     * 點選開啟組織人員選擇樹
     */
    public void openReqFinalApplyUserQuickSelectedTree() {

        this.reqFinalApplyUserQuickSelectedTree = new OrgUserTreeMBean();

        this.reqFinalApplyUserQuickSelectedTree.init(
                Optional.ofNullable(orgManager.findById(SecurityFacade.getCompanyId()))
                        .map(Org::getSid)
                        .orElseGet(() -> null),
                SecurityFacade.getPrimaryOrgSid(),
                getSelectedData,
                Maps.newHashMap());
    }

    /**
     * 更新畫面的已選擇人員清單
     */
    public void reNewReqFinalApplyUser() {

        // 取得元件中選取的 user 清單
        List<Integer> userSids = this.reqFinalApplyUserQuickSelectedTree.getSelectedDataList();

        if (WkStringUtils.isEmpty(userSids)) {
            // 代表清除人員
            this.menuTagEditVO.setReqFinalApplyUserSid(null);
        } else {
            // 單選模式下, 僅回傳一筆, 故 index 為 0
            Integer userSid = userSids.get(0);
            this.menuTagEditVO.setReqFinalApplyUserSid(userSid);
        }

        // 畫面顯示
        this.menuTagEditVO.setReqFinalApplyUserDisplayContent(
                wcMenuTagLogicComponent.getReqFinalApplyUserDisplayContent(
                        this.menuTagEditVO.getReqFinalApplyUserSid()));
    }

    public void doNotThingPostBack() {
        log.debug("doNotThingPostBack");
    }

    /**
     * 中類選單設定 - 可使用部門 - 顯示文字
     */
    public void prepareMenuTagUseDepSettingContent() {
        //已選擇模版時，清空選項
        if (this.menuTagEditVO.getUesDepTempletSid() != null) {
            this.menuTagEditVO.setUserContainFollowing(false);
            this.menuTagEditVO.setOnlyForUseDepManager(false);
        }
        this.menuTagEditVO.prepareUseDepsSettingContent(this.selectOrgs);
    }

    /**
     * 中類選單設定 - 可使用人員 - 顯示文字
     */
    public void prepareMenuTagUseUsersSettingContent() {
        this.menuTagEditVO.prepareUseUsersSettingContent(this.useUsersPicker);
    }

    /**
     * 小類(執行項目)設定 - 可使用部門 - 顯示文字
     */
    public void prepareSubTagCanUserDepsSettingContent() {
        //已選擇模版時，清空選項
        if (this.subTagEditVO.getCanUserDepTempletSid() != null) {
            this.subTagEditVO.setUserContainFollowing(false);
            this.subTagEditVO.setOnlyForUseDepManager(false);
        }

        this.subTagEditVO.prepareCanUserDepsSettingContent(this.selectTagUserOrgs);
    }

    /**
     * 小類(執行項目)設定 - 預設可閱部門 - 顯示文字
     */
    public void prepareSubTagUseDepSettingContent() {
        this.subTagEditVO.prepareUseDepSettingContent(this.selectViewOrgs);
    }

    /**
     * 執行單位設定 - 指派單位 - 顯示文字
     */
    public void prepareExecDepTransDepsSettingContent() {
        this.transDepsShowToolTip = WorkSettingOrgManager.getInstance()
                .prepareDepsShowTextByOrgs(
                        this.selectAssignOrgs, this.isTransDepContainFollowing, false);
        if (WkStringUtils.notEmpty(this.transDepsShowToolTip)) {
            this.transDepsShow = "詳細設定內容";
        } else {
            this.transDepsShow = "無";
        }
    }

    /**
     * 異動排序
     *
     * @param event
     */
    public void sortCategoryTag(ReorderEvent event) {

        // ====================================
        // 更新排序序號
        // ====================================
        log.info("更新類別顯示排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());

        try {
            // 更新排序序號
            this.wcCategoryTagLogicComponent.sort(this.tagSearchVO);

        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        tagSearchVO = doSearch("");
        DisplayController.getInstance().update(dataTableID);
    }

    /**
     * 異動排序
     *
     * @param event
     */
    public void sortMenuTag(ReorderEvent event) {

        // ====================================
        // 更新排序序號
        // ====================================
        log.info(
                "更新單據名稱顯示排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());

        try {
            // 更新排序序號
            this.wcMenuTagLogicComponent.sort(this.menuTagSearchVOs);

        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        this.searchMenuItems();
    }

    /**
     * 異動排序
     *
     * @param event
     */
    public void sortSubTag(ReorderEvent event) {

        // ====================================
        // 更新排序序號
        // ====================================
        log.info(
                "更新執行項目顯示排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());

        try {
            // 更新排序序號
            this.wCTagLogicComponents.sort(this.subTagSearchVOs);

        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        this.searchTagItems();
    }
}
