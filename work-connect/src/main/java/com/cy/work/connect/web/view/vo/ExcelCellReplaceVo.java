/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author marlow_chen
 */
public class ExcelCellReplaceVo {

    @Getter
    @Setter
    private int cellNo;
    @Getter
    @Setter
    private String[] replaceText;

    public ExcelCellReplaceVo(int cellNo, String[] replaceText) {
        this.cellNo = cellNo;
        this.replaceText = replaceText;
    }
}
