/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.util.List;
import java.util.Objects;
import lombok.Getter;

/**
 * 執行單位設定介面物件
 *
 * @author brain0925_liao
 */
public class ExecDepSettingSearchVO {

    // 執行單位設定檔 SID
    @Getter
    private final String wc_exec_dep_setting_sid;
    // 執行單位
    @Getter
    private String depName;
    // 執行單位簽核人員(逗點串接)
    @Getter
    private String userName;
    // 執行單位簽核人員(換行串接)
    @Getter
    private String userListName;
    // 該執行人員是否需被指派人員指派
    @Getter
    private String isNeedToAssign;
    // 是否讀取轉單人員設定
    @Getter
    private String isReadSetting;
    // 指派單位
    @Getter
    private String transdep;
    // 指派單位清單
    @Getter
    private List<Integer> transdepSids;
    // 指派人員(逗點串接)
    @Getter
    private String assignUserName;
    // 指派人員(換行串接)
    @Getter
    private String assignUserListName;
    // 管理領單人員
    @Getter
    private String receviceManager;
    // 領單人員
    @Getter
    private String receviceUserListName;
    // 中佑對應窗口
    @Getter
    private String cyWindow;

    public ExecDepSettingSearchVO(String wc_exec_dep_setting_sid) {
        this.wc_exec_dep_setting_sid = wc_exec_dep_setting_sid;
    }

    public ExecDepSettingSearchVO(
        String wc_exec_dep_setting_sid,
        String depName,
        String userName,
        String userListName,
        String isNeedToAssign,
        String isReadSetting,
        String assignUserName,
        String assignUserListName,
        String receviceManager,
        String receviceUserListName,
        String cyWindow,
        String transdep,
        List<Integer> transdepSids) {
        this.wc_exec_dep_setting_sid = wc_exec_dep_setting_sid;
        this.depName = depName;
        this.userName = userName;
        this.userListName = userListName;
        this.isNeedToAssign = isNeedToAssign;
        this.isReadSetting = isReadSetting;
        this.assignUserName = assignUserName;
        this.assignUserListName = assignUserListName;
        this.receviceManager = receviceManager;
        this.receviceUserListName = receviceUserListName;
        this.cyWindow = cyWindow;
        this.transdep = transdep;
        this.transdepSids = transdepSids;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.wc_exec_dep_setting_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExecDepSettingSearchVO other = (ExecDepSettingSearchVO) obj;
        return Objects.equals(this.wc_exec_dep_setting_sid,
            other.wc_exec_dep_setting_sid);
    }
}
