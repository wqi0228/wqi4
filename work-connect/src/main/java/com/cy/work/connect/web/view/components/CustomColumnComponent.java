package com.cy.work.connect.web.view.components;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.CustomColumnDetailTo;
import com.cy.work.connect.vo.enums.Column;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.logic.components.WCReportCustomColumnLogicComponents;
import com.cy.work.connect.web.util.SpringContextHolder;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.CustomCulumnVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 自訂欄位功能物件
 *
 * @author brain0925_liao
 */
@Slf4j
public class CustomColumnComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1249556906336818943L;
    /**
     * 自訂欄位War
     */
    @Getter
    private final String dialogWar = "dlgDefineField";
    /**
     * 報表類型列舉
     */
    private final WCReportCustomColumnUrlType wkReportCustomColumnUrlType;
    /**
     * 登入者Sid
     */
    private final Integer userSid;
    /**
     * 訊息CallBack
     */
    private final MessageCallBack messageCallBack;
    /**
     * DataTable ReloadCallBack
     */
    private final DataTableReLoadCallBack dataTableReLoadCallBack;
    private final List<Column> wcColumn;
    /**
     * 自訂欄位物件List
     */
    @Setter
    @Getter
    private List<CustomCulumnVO> customCulumnVO;
    /**
     * 單頁顯示筆數
     */
    @Setter
    @Getter
    private String pageCount;
    /**
     * 挑選全部
     */
    @Setter
    @Getter
    private boolean selectAll;
    /**
     * 取消全選
     */
    @Setter
    @Getter
    private boolean unSelectAll;
    /**
     * 是否顯示儲存寬度按鈕
     */
    @Getter
    private boolean showResizeColumn = false;

    public CustomColumnComponent(
        WCReportCustomColumnUrlType wkReportCustomColumnUrlType,
        List<Column> wcColumn,
        String pageCount,
        Integer userSid,
        MessageCallBack messageCallBack,
        DataTableReLoadCallBack dataTableReLoadCallBack) {
        this.pageCount = pageCount;
        this.wcColumn = wcColumn;
        this.messageCallBack = messageCallBack;
        this.dataTableReLoadCallBack = dataTableReLoadCallBack;
        this.wkReportCustomColumnUrlType = wkReportCustomColumnUrlType;
        this.userSid = userSid;
        customCulumnVO = Lists.newArrayList();
    }

    /**
     * 顯示儲存寬度按鈕
     */
    public void doResizeColumn() {
        showResizeColumn = true;
    }

    /**
     * 載入可挑選顯示欄位資料
     */
    public void loadData() {
        customCulumnVO.clear();
        List<CustomCulumnVO> selectList =
            WCReportCustomColumnLogicComponents.getInstance()
                .getSelectWCColumn(wkReportCustomColumnUrlType, userSid);

        wcColumn.forEach(
            item -> {
                if (!item.getCanSelectItem()) {
                    return;
                }
                // 若回傳null,代表個人尚未進行設定,全數都為預設
                if (selectList == null) {
                    CustomCulumnVO vo =
                        new CustomCulumnVO(item.getDefaultShow(), item.getVal(),
                            item.getDefaultWidth());
                    customCulumnVO.add(vo);
                    return;
                }

                CustomCulumnVO vo = new CustomCulumnVO(false, item.getVal(),
                    item.getDefaultWidth());
                int index = selectList.indexOf(vo);
                if (index > -1) {
                    customCulumnVO.add(selectList.get(index));
                } else {
                    customCulumnVO.add(vo);
                }
            });
    }

    /**
     * 儲存顯示欄位
     */
    public void doSaveCustomColumn() {
        try {
            List<CustomCulumnVO> selectCustomCulumnVOs =
                customCulumnVO.stream().filter(each -> each.isSelected())
                    .collect(Collectors.toList());
            WCReportCustomColumnLogicComponents.getInstance()
                .saveWCColumnShowSetting(
                    userSid, wcColumn, selectCustomCulumnVOs, pageCount,
                    wkReportCustomColumnUrlType);
            closeDialog();
            dataTableReLoadCallBack.reload();
        } catch (Exception e) {
            log.warn("doSaveCustomColumn Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 進行欄位寬度存檔
     */
    public void saveColumnWith() {
        try {
            String columnvalue = Faces.getRequestParameterMap().get("columnvalue");
            WkJsonUtils jsonUtils = SpringContextHolder.getBean(WkJsonUtils.class);
            List<CustomColumnDetailTo> customCols =
                jsonUtils.fromJsonToList(columnvalue, CustomColumnDetailTo.class);
            WCReportCustomColumnLogicComponents.getInstance()
                .saveWCColumnWidthSetting(
                    wkReportCustomColumnUrlType, userSid, customCols, wcColumn, pageCount);
            showResizeColumn = false;
            dataTableReLoadCallBack.reload();
        } catch (Exception e) {
            log.warn("儲存欄位失敗..." + e.getMessage(), e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 點選全選
     */
    public void doSelectAll() {
        selectAll = true;
        unSelectAll = false;
        customCulumnVO.forEach(
            item -> {
                item.setSelected(true);
            });
    }

    /**
     * 點選全不選
     */
    public void doUnSelectAll() {
        selectAll = false;
        unSelectAll = true;
        customCulumnVO.forEach(
            item -> {
                item.setSelected(false);
            });
    }

    /**
     * 關閉Dialog
     */
    public void closeDialog() {
        DisplayController.getInstance().hidePfWidgetVar(dialogWar);
    }
}
