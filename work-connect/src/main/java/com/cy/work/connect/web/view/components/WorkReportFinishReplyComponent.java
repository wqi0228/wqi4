/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.vo.enums.WCTraceType;
import com.cy.work.connect.web.logic.components.WCTraceLogicComponents;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * 執行完成回覆 Component
 *
 * @author kasim
 */
public class WorkReportFinishReplyComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4860952244949830315L;

    @Getter
    private List<WCTraceVO> wcTraceVO;

    private String wcSid;

    private Integer userSid;
    @Getter
    private boolean showTab = false;

    public WorkReportFinishReplyComponent() {
    }

    public void loadData(String wcSid, Integer userSid) {
        this.wcSid = wcSid;
        this.userSid = userSid;
        loadViewData(wcSid, userSid);
    }

    public void loadData() {
        loadViewData(wcSid, userSid);
    }

    private void loadViewData(String wcSid, Integer userSid) {
        wcTraceVO =
            WCTraceLogicComponents.getInstance()
                .getWCTraceVOsByWcSIDAndWCTraceType(userSid, wcSid, WCTraceType.FINISH_REPLY);
        if (wcTraceVO != null && !wcTraceVO.isEmpty()) {
            showTab = true;
            WCTraceLogicComponents.getInstance()
                .bulidWCAgainTraceVOsByWcSIDAndWCTraceType(
                    userSid, wcSid, wcTraceVO, WCTraceType.FINISH_REPLY_AND_REPLY);
        } else {
            showTab = false;
        }
    }
}
