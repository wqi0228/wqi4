/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search;

import com.cy.work.connect.vo.WCTag;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 退件一覽表 - 列表資料
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(of = {"sid"})
public class RollBackListVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3200127290692459240L;

    private String sid;

    /**
     * 聯絡單 sid
     */
    private String wcSid;

    /**
     * 聯絡單 單號
     */
    private String wcNo;

    /**
     * 建立日期
     */
    private String rollBackDate;

    private Date rollBackDt;

    /**
     * 類別(第一層)
     */
    private String categoryName;

    /**
     * 單據名稱(第二層)
     */
    private String menuTagName;

    /**
     * 執行人員
     */
    private String rollBackUserName = "";

    private String wcStatus = "";

    /**
     * 類別資訊(第三層)
     */
    private String tagInfo = "";

    private String content = "";

    private List<String> tgSids = Lists.newArrayList();

    private String reqDepName;

    /**
     * 主題
     */
    private String theme;

    public RollBackListVO(String sid) {
        this.sid = sid;
    }

    public void replaceValue(RollBackListVO updateObject) {
        this.wcSid = updateObject.getWcSid();
        this.wcNo = updateObject.getWcNo();
        this.rollBackDate = updateObject.getRollBackDate();
        this.rollBackDt = updateObject.getRollBackDt();
        this.menuTagName = updateObject.getMenuTagName();
        this.categoryName = updateObject.getCategoryName();
        this.rollBackUserName = updateObject.getRollBackUserName();
        this.wcStatus = updateObject.getWcStatus();
        this.theme = updateObject.getTheme();
        this.tagInfo = updateObject.getTagInfo();
        this.tgSids = updateObject.getTgSids();
        this.reqDepName = updateObject.getReqDepName();
    }

    public void bulidExecTag(WCTag tag) {
        if (tag == null) {
            return;
        }
        if (tgSids.contains(tag.getSid())) {
            return;
        }
        tgSids.add(tag.getSid());
        if (!Strings.isNullOrEmpty(tagInfo)) {
            tagInfo += "、";
        }
        tagInfo += tag.getTagName();
    }
}
