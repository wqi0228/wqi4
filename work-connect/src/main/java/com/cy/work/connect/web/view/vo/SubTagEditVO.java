/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;

/**
 * 類別編輯物件
 *
 * @author brain0925_liao
 */
public class SubTagEditVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2894079231198066567L;
    @Getter
    private final List<User> users = Lists.newArrayList();
    /**
     * 可使用單位：部門設定模版選項
     */
    @Setter
    @Getter
    List<WCConfigTempletVo> canUserDepTempletItems;
    /**
     * 預設可閱部門：部門設定模版選項
     */
    @Setter
    @Getter
    List<WCConfigTempletVo> useDepTempletItems;
    @Setter
    @Getter
    private String sid;
    @Setter
    @Getter
    private String name;
    @Setter
    @Getter
    private String status;
    @Setter
    @Getter
    private String memo;
    @Setter
    @Getter
    private String seq;
    @Setter
    @Getter
    private String reqFlowType;
    @Setter
    @Getter
    private String defaultContentCss;
    @Setter
    @Getter
    private String defaultMemoCss;
    @Setter
    @Getter
    private boolean legitimateRangeCheck;
    @Setter
    @Getter
    private boolean uploadFile;
    @Setter
    @Getter
    private String legitimateRangeDays;
    @Setter
    @Getter
    private String legitimateRangeTitle;
    @Setter
    @Getter
    private boolean startEndCheck;
    @Setter
    @Getter
    private String startEndDays;

    // ====================================
    // 可使用單位相關欄位
    // ====================================
    @Setter
    @Getter
    private String startEndTitle;
    @Setter
    @Getter
    private String endRuleDays;
    /**
     * 可使用單位
     */
    @Setter
    @Getter
    private List<Org> canUserDeps = Lists.newArrayList();
    /**
     * 可使用單位：是否含以下
     */
    @Setter
    @Getter
    private boolean isUserContainFollowing = false;

    /**
     * 可使用單位-是否僅主管可使用
     */
    @Setter
    @Getter
    private boolean onlyForUseDepManager;
    /**
     * 可使用單位：部門設定模版選項
     */
    @Setter
    @Getter
    private Long canUserDepTempletSid;
    /**
     * 可使用單位：顯示文字
     */
    @Setter
    @Getter
    private String canUserDepsShow;
    /**
     * 可使用單位：顯示文字 - ToolTip
     */
    @Setter
    @Getter
    private String canUserDepsShowToolTip;

    // ====================================
    // 預設可閱部門相關欄位
    // ====================================
    /**
     * 預設可閱部門
     */
    @Setter
    @Getter
    private List<Org> orgs = Lists.newArrayList();

    /**
     * 預設可閱部門：是否包含以下
     */
    @Setter
    @Getter
    private boolean isViewContainFollowing = false;

    /**
     * 預設可閱部門：部門設定模版選項
     */
    @Setter
    @Getter
    private Long useDepTempletSid;
    /**
     * 可使用單位：顯示文字
     */
    @Setter
    @Getter
    private String useDepShow;
    /**
     * 可使用單位：顯示文字 - ToolTip
     */
    @Setter
    @Getter
    private String useDepToolTip;
    /**
     * 顯示分類
     */
    @Setter
    @Getter
    private String tagType;
    /**
     * Tag 備註說明
     */
    @Setter
    @Getter
    private String tagDesc;

    /**
     * 可使用單位：準備顯示文字
     */
    public void prepareCanUserDepsSettingContent(List<Org> selectOrgs) {

        WorkSettingOrgManager orgManager = WorkSettingOrgManager.getInstance();

        if (this.canUserDepTempletSid == null) {

            List<Integer> depSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(selectOrgs)) {
                depSids = selectOrgs.stream().map(Org::getSid).collect(Collectors.toList());
            }
            // 含以下單位先最取得最上層處理
            if (this.isUserContainFollowing()) {
                depSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(depSids));
            }
            // 依組織樹排序
            depSids = WkOrgUtils.sortByOrgTree(depSids);

            // 產生顯示文字
            this.setCanUserDepsShowToolTip(
                orgManager.prepareDepsShowText(depSids, this.isUserContainFollowing(), false));

        } else {
            for (WCConfigTempletVo wcConfigTempletVo : this.canUserDepTempletItems) {
                if ((wcConfigTempletVo.getSid() + "").equals(this.canUserDepTempletSid + "")) {
                    // 產生顯示文字
                    this.setCanUserDepsShowToolTip(
                        orgManager.prepareDepsShowText(
                            wcConfigTempletVo.getConfigValue().getDeps(),
                            wcConfigTempletVo.getConfigValue().isDepContainFollowing(),
                            wcConfigTempletVo.getConfigValue().isDepExcept()));
                }
            }
        }

        if (WkStringUtils.isEmpty(this.getCanUserDepsShowToolTip())) {
            this.setCanUserDepsShow("全單位");
            this.setCanUserDepsShowToolTip("全單位");
        } else {
            this.setCanUserDepsShow("詳細設定內容");
        }
    }

    /**
     * 可使用單位：準備顯示文字
     */
    public void prepareUseDepSettingContent(List<Org> selectOrgs) {

        WorkSettingOrgManager orgManager = WorkSettingOrgManager.getInstance();

        if (this.useDepTempletSid == null) {

            List<Integer> depSids = Lists.newArrayList();
            if (WkStringUtils.notEmpty(selectOrgs)) {
                depSids = selectOrgs.stream().map(Org::getSid).collect(Collectors.toList());
            }
            // 含以下單位先最取得最上層
            if (this.isViewContainFollowing) {
                depSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(depSids));
            }
            // 依組織樹排序
            depSids = WkOrgUtils.sortByOrgTree(depSids);

            // 產生顯示文字
            this.useDepToolTip =
                orgManager.prepareDepsShowText(depSids, this.isViewContainFollowing, false);

        } else {
            for (WCConfigTempletVo wcConfigTempletVo : this.useDepTempletItems) {
                if ((wcConfigTempletVo.getSid() + "").equals(this.useDepTempletSid + "")) {
                    // 產生顯示文字
                    this.useDepToolTip =
                        orgManager.prepareDepsShowText(
                            wcConfigTempletVo.getConfigValue().getDeps(),
                            wcConfigTempletVo.getConfigValue().isDepContainFollowing(),
                            wcConfigTempletVo.getConfigValue().isDepExcept());
                }
            }
        }

        if (WkStringUtils.isEmpty(this.useDepToolTip)) {
            this.useDepShow = "無";
            this.useDepToolTip = "無單位預設可閱";
        } else {
            this.useDepShow = "詳細設定內容";
        }
    }
}
