package com.cy.work.connect.web.callable;

import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WorkFunItemComponentType;
import com.cy.work.connect.web.logic.components.search.WorkingListLogicComponent;

import lombok.extern.slf4j.Slf4j;

/**
 * 處理清單Call able
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkingSearchCallable implements HomeCallable {

    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 選單需計算筆數列舉
     */
    private final WorkFunItemComponentType workFunItemComponentType;
    /**
     * 選單Group物件
     */
    private final WorkFunItemGroupVO workFunItemGroupVO;

    public WorkingSearchCallable(
        WorkFunItemGroupVO workFunItemGroupVO,
        WorkFunItemComponentType workFunItemComponentType,
        Integer loginUserSid) {
        this.workFunItemGroupVO = workFunItemGroupVO;
        this.workFunItemComponentType = workFunItemComponentType;
        this.loginUserSid = loginUserSid;
    }

    @Override
    public HomeCallableCondition call() throws Exception {

        Integer count = 0;
        try {
            count = WorkingListLogicComponent.getInstance().count(this.loginUserSid);
        } catch (Exception e) {
            log.warn("CheckedSearchCallable ERROR", e);
        }
        HomeCallableCondition reuslt = new HomeCallableCondition();
        if (count > 0) {
            reuslt.setConntString("(" + count + ")");
            reuslt.setCssString("color: red");
        }
        reuslt.setWorkFunItemComponentType(workFunItemComponentType);
        reuslt.setWorkFunItemGroupVO(workFunItemGroupVO);
        return reuslt;
    }
}
