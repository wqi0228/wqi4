/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class ExecInfoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6744038510975934372L;

    @Getter
    private final String sid;
    @Getter
    private final String tagName;
    @Getter
    private final String execDepName;
    @Getter
    private final String execStatus;
    @Getter
    private final String execPersonInfo;
    @Getter
    private final String execPersonDetail;

    public ExecInfoVO(
        String sid,
        String tagName,
        String execDepName,
        String execStatus,
        String execPersonInfo,
        String execPersonDetail) {
        this.sid = sid;
        this.tagName = tagName;
        this.execDepName = execDepName;
        this.execStatus = execStatus;
        this.execPersonInfo = execPersonInfo;
        this.execPersonDetail = execPersonDetail;
    }
}
