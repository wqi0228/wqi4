/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class ReadReceiptVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6861361301414855850L;

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String departmentName;
    @Getter
    @Setter
    private String userSid;
    @Getter
    @Setter
    private WCReadReceiptStatus wReadReceiptStatus;
    @Getter
    @Setter
    private String readtime;
    @Getter
    @Setter
    private String requiretime;
    @Getter
    @Setter
    private String signtime;

    public ReadReceiptVO(
        String name,
        String departmentName,
        String userSid,
        WCReadReceiptStatus wReadReceiptStatus,
        String readtime,
        String requiretime,
        String signtime) {
        this.userSid = userSid;
        this.wReadReceiptStatus = wReadReceiptStatus;
        this.readtime = readtime;
        this.requiretime = requiretime;
        this.signtime = signtime;
        this.departmentName = departmentName;
        this.name = name;
    }
}
