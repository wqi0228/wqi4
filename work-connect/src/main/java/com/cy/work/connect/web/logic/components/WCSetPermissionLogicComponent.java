package com.cy.work.connect.web.logic.components;

import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.view.vo.TagVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * 設定權限邏輯元件
 *
 * @author jimmy_chou
 */
@Component
@Slf4j
public class WCSetPermissionLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5013599222291594901L;

    private static WCSetPermissionLogicComponent instance;

    public static WCSetPermissionLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCSetPermissionLogicComponent.instance = this;
    }

    /**
     * 取得編輯元件及設定權限清單
     *
     * @param editWCPermission 設定權限編輯元件
     * @param targetTag        挑選項目
     * @param targetType       挑選類型
     * @param loginCompSid     登入公司Sid
     * @return
     */
    public Object[] getNowPermissionList(
        WCSetPermission editWCPermission,
        Object targetTag,
        TargetType targetType,
        Integer loginCompSid) {
        Object[] result = new Object[2];
        List<Long> nowPermissionList = Lists.newArrayList();
        try {
            editWCPermission =
                WCSetPermissionManager.getInstance()
                    .findPermissionByCompSidAndTargetSidAndTargetType(
                        loginCompSid,
                        ((TagVO) targetTag).getSid(), targetType);
            nowPermissionList = Lists.newArrayList();
            if (editWCPermission != null) {
                nowPermissionList = editWCPermission.getPermissionGroups();
            } else {
                editWCPermission = new WCSetPermission();
                editWCPermission.setTargetType(targetType);
                editWCPermission.setTargetSid(((TagVO) targetTag).getSid());
            }
        } catch (Exception ex) {
            log.error("getNowPermissionList：" + ex.getMessage(), ex);
        }
        result[0] = nowPermissionList;
        result[1] = editWCPermission;
        return result;
    }
}
