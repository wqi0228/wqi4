/**
 * 
 */
package com.cy.work.connect.web.view.setting10DepSettingCheck;

import lombok.Getter;

/**
 * 組織異動-設定檢查-檢查項目
 * 
 * @author allen1214_wu
 */
public enum DepSettingCheckItemType {
    MENUTAG_CAN_USE_DEP("中類-可使用單位", "WS1-1-3"),
    ITEM_USE_DEP("小類-可使用單位", "WS1-1-4"),
    ITEM_CAN_VIEW_DEP("小類-預設可閱單位", "WS1-1-5"),
    EXEC_DEP("執行單位", "WS1-1-6"),
    ;

    @Getter
    private final String descr;

    @Getter
    private final String colorStyleClass;

    DepSettingCheckItemType(String descr, String colorStyleClass) {
        this.descr = descr;
        this.colorStyleClass = colorStyleClass;
    }

}
