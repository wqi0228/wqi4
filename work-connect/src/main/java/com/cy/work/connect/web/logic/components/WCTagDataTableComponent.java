package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.view.vo.ExecDepSettingSearchVO;
import com.cy.work.connect.web.view.vo.MenuTagSearchVO;
import com.cy.work.connect.web.view.vo.TagDtVO;
import com.cy.work.connect.web.view.vo.TagSearchVO;
import com.cy.work.connect.web.view.vo.TagVO;
import com.cy.work.connect.web.view.vo.search.query.CheckAuthoritySearchQuery;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author jimmy_chou
 */
@Component
public class WCTagDataTableComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7024634572845882738L;

    private static WCTagDataTableComponent instance;

    public static WCTagDataTableComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCTagDataTableComponent.instance = this;
    }

    /**
     * 建立Tag DataTable
     *
     * @param loginCompSid 公司Sid
     * @param status       狀態
     * @param query        查詢條件
     */
    public List<TagDtVO> buildTagSearchDt(
        Integer loginCompSid, Activation status, CheckAuthoritySearchQuery query) {
        List<TagDtVO> allTagDt = Lists.newArrayList();

        // query 選取物件前置處理(將父節點納入選取範圍)
        List<TreeNode> parentIncludeSelNode = Lists.newArrayList();
        for (TreeNode node : query.getSelNode()) {
            parentIncludeSelNode.add(node);
            while (node.getParent() != null) {
                parentIncludeSelNode.add(node.getParent());
                node = node.getParent();
            }
        }

        // 準備查詢條件物件
        List<String> multiCategoryTagList =
            parentIncludeSelNode.stream()
                .filter(item -> "CategoryTag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());
        List<String> multiMenuTagList =
            parentIncludeSelNode.stream()
                .filter(item -> "MenuTag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());
        List<String> multiTagList =
            parentIncludeSelNode.stream()
                .filter(item -> "Tag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());

        // 準備權限判斷物件
        List<TargetType> targetTypes =
            Lists.newArrayList(
                TargetType.CATEGORY_TAG, TargetType.MENU_TAG, TargetType.TAG, TargetType.EXEC_DEP);
        List<WCSetPermission> permissionAll =
            WCSetPermissionManager.getInstance()
                .findByCompSidAndTargetTypeIn(loginCompSid, targetTypes);
        Map<String, List<WCSetPermission>> permissionMap =
            permissionAll.stream()
                .collect(
                    Collectors.groupingBy(
                        each -> each.getTargetType().name().concat(each.getTargetSid())));

        List<WCCategoryTag> categoryTagList =
            WCCategoryTagLogicComponent.getInstance().findAll(status, loginCompSid);
        for (WCCategoryTag categoryTag : categoryTagList) {
            // 判斷設定權限
            List<WCSetPermission> categoryPermissionList =
                permissionMap.get(TargetType.CATEGORY_TAG.name().concat(categoryTag.getSid()));
            WCSetPermission categoryPermission =
                WkStringUtils.notEmpty(categoryPermissionList) ? categoryPermissionList.get(0)
                    : null;
            if (categoryPermission != null && !categoryPermission.getPermissionGroups().isEmpty()) {
                List<Integer> permissionUserSids =
                    WCSetPermissionManager.getInstance()
                        .groupPermissionUserSids(loginCompSid,
                            categoryPermission.getPermissionGroups());
                if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                    continue;
                }
            }

            // 判斷查詢條件(Tag)
            boolean isCategoryTag =
                multiCategoryTagList.isEmpty() || multiCategoryTagList.contains(
                    categoryTag.getSid());
            boolean isHaveCategoryTag = categoryTag.getSid().contains(query.getCategoryTagId());
            if (!(isHaveCategoryTag && isCategoryTag)) {
                continue;
            }

            List<MenuTagSearchVO> menuTagSearchVOs =
                WCMenuTagLogicComponent.getInstance()
                    .getMenuTagSearchVO(categoryTag.getSid(), status);
            List<WCMenuTag> menuTagList =
                WCMenuTagLogicComponent.getInstance()
                    .getWCMenuTagByCategorySid(categoryTag.getSid(), status);
            for (WCMenuTag menuTag : menuTagList) {
                // 判斷設定權限
                List<WCSetPermission> menuPermissionList =
                    permissionMap.get(TargetType.MENU_TAG.name().concat(menuTag.getSid()));
                WCSetPermission menuPermission =
                    WkStringUtils.notEmpty(menuPermissionList) ? menuPermissionList.get(0) : null;
                if (menuPermission != null && !menuPermission.getPermissionGroups().isEmpty()) {
                    List<Integer> permissionUserSids =
                        WCSetPermissionManager.getInstance()
                            .groupPermissionUserSids(loginCompSid,
                                menuPermission.getPermissionGroups());
                    if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                        continue;
                    }
                }

                // 判斷查詢條件(Tag)
                boolean isMenuTag =
                    multiMenuTagList.isEmpty() || multiMenuTagList.contains(menuTag.getSid());
                boolean isHaveMenuTag = menuTag.getSid().contains(query.getMenuTagId());
                if (!(isHaveMenuTag && isMenuTag)) {
                    continue;
                }

                // 判斷查詢條件(可使用單位)
                boolean isHaveMenuTagUse =
                    query.getCanUseOrg().isEmpty() || query.getCanUseOrg().stream()
                        .anyMatch(
                            item -> {
                                List<Integer> useDepSid = this.getUseDepSids(menuTag);
                                return useDepSid == null
                                    || useDepSid.isEmpty()
                                    || useDepSid.contains(item.getSid());
                            });
                if (!isHaveMenuTagUse) {
                    continue;
                }

                // 判斷查詢條件(可使用人員)
                boolean isHaveUseUser =
                    query.getUseUsers().isEmpty() || query.getUseUsers().stream()
                        .anyMatch(
                            item -> {
                                List<Integer> useUserSids = Lists.newArrayList();
                                if (menuTag.getUseUser() != null
                                    && menuTag.getUseUser().getUserTos() != null) {
                                    menuTag
                                        .getUseUser()
                                        .getUserTos()
                                        .forEach(
                                            subItem -> {
                                                useUserSids.add(subItem.getSid());
                                            });
                                }
                                return useUserSids != null && useUserSids.contains(item.getSid());
                            });
                if (!isHaveUseUser) {
                    continue;
                }

                int menuIndex = menuTagSearchVOs.indexOf(new MenuTagSearchVO(menuTag.getSid()));
                MenuTagSearchVO menuTagSearchVO = new MenuTagSearchVO(menuTag.getSid());
                if (menuIndex != -1) {
                    menuTagSearchVO = menuTagSearchVOs.get(menuIndex);
                }
                List<TagSearchVO> subTagSearchVOs =
                    WCTagLogicComponents.getInstance().getTagSearch(null, menuTag.getSid(), false);
                List<WCTag> tagList =
                    WCTagLogicComponents.getInstance().getWCTagByMenuSid(menuTag.getSid(), status);
                for (WCTag tag : tagList) {
                    // 判斷設定權限
                    List<WCSetPermission> tagPermissionList =
                        permissionMap.get(TargetType.TAG.name().concat(tag.getSid()));
                    WCSetPermission tagPermission =
                        WkStringUtils.notEmpty(tagPermissionList) ? tagPermissionList.get(0) : null;
                    if (tagPermission != null && !tagPermission.getPermissionGroups().isEmpty()) {
                        List<Integer> permissionUserSids =
                            WCSetPermissionManager.getInstance()
                                .groupPermissionUserSids(loginCompSid,
                                    tagPermission.getPermissionGroups());
                        if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                            continue;
                        }
                    }

                    // 判斷查詢條件(Tag)
                    boolean isTag = multiTagList.isEmpty() || multiTagList.contains(tag.getSid());
                    boolean isHaveSubTag = tag.getSid().contains(query.getSubTagId());
                    if (!(isHaveSubTag && isTag)) {
                        continue;
                    }

                    // 判斷查詢條件(單位)
                    // 可使用單位
                    boolean isHaveSubTagUse =
                        query.getCanUseOrg().isEmpty() || query.getCanUseOrg().stream()
                            .anyMatch(
                                item -> {
                                    List<Integer> useDepSid = this.getUseDepSids(tag);
                                    return useDepSid == null
                                        || useDepSid.isEmpty()
                                        || useDepSid.contains(item.getSid());
                                });
                    // 可閱部門
                    boolean isHaveViewDep =
                        query.getViewOrgs().isEmpty() || query.getViewOrgs().stream()
                            .anyMatch(
                                item -> {
                                    List<Integer> readDepSid = this.getReadDepSids(tag);
                                    return readDepSid != null
                                        && !readDepSid.isEmpty()
                                        && readDepSid.contains(item.getSid());
                                });
                    // 相關單位
                    boolean isRelateDep =
                        query.getRelatedOrg().isEmpty() || query.getRelatedOrg().stream()
                            .anyMatch(
                                item -> {
                                    List<Integer> useDepSid = this.getUseDepSids(tag);
                                    return useDepSid == null
                                        || useDepSid.isEmpty()
                                        || useDepSid.contains(item.getSid());
                                })
                            || query.getRelatedOrg().stream()
                            .anyMatch(
                                item -> {
                                    List<Integer> readDepSid = this.getReadDepSids(tag);
                                    return readDepSid != null
                                        && !readDepSid.isEmpty()
                                        && readDepSid.contains(item.getSid());
                                });
                    if (!(isHaveSubTagUse && isHaveViewDep && isRelateDep)) {
                        continue;
                    }

                    // 判斷查詢條件(人員)
                    // 可閱人員
                    boolean isHaveViewUser =
                        query.getViewUsers().isEmpty() || query.getViewUsers().stream()
                            .anyMatch(
                                item -> {
                                    List<Integer> viewUserSids = Lists.newArrayList();
                                    if (tag.getUseUser() != null
                                        && tag.getUseUser().getUserTos() != null) {
                                        tag.getUseUser()
                                            .getUserTos()
                                            .forEach(
                                                subItem -> {
                                                    viewUserSids.add(subItem.getSid());
                                                });
                                    }
                                    return viewUserSids != null && viewUserSids.contains(
                                        item.getSid());
                                });
                    if (!isHaveViewUser) {
                        continue;
                    }

                    // 判斷查詢條件(申請流程類型)
                    boolean isHaveFlowType =
                        query.getFlowType() == null || (tag.getReqFlowType() != null
                            && tag.getReqFlowType().equals(query.getFlowType()));
                    if (!isHaveFlowType) {
                        continue;
                    }

                    int tagIndex = subTagSearchVOs.indexOf(new TagSearchVO(tag.getSid()));
                    TagSearchVO subTagSearchVO = new TagSearchVO(tag.getSid());
                    if (tagIndex != -1) {
                        subTagSearchVO = subTagSearchVOs.get(tagIndex);
                    }
                    List<ExecDepSettingSearchVO> execDepSettingSearchVOs =
                        WCExecDepSettingLogicComponent.getInstance()
                            .getExecDepSettingSearchVO(tag.getSid());
                    List<WCExecDepSetting> execDepSettingList =
                        WCExecDepSettingLogicComponent.getInstance()
                            .getWCExecDepSettingByWcTagSid(tag.getSid(), Activation.ACTIVE);
                    if (execDepSettingList.isEmpty()) {
                        if (!allTagDt.contains(
                            new TagDtVO(
                                "CategoryTag",
                                categoryTag.getSid(),
                                categoryTag.getCategoryName(),
                                "",
                                "",
                                categoryTag.getSeq()))) {
                            allTagDt.add(
                                new TagDtVO(
                                    "CategoryTag",
                                    categoryTag.getSid(),
                                    categoryTag.getCategoryName(),
                                    "",
                                    "",
                                    categoryTag.getSeq(),
                                    categoryTag.getStatus()));
                            allTagDt.add(
                                new TagDtVO(
                                    "MenuTag",
                                    menuTag.getSid(),
                                    "",
                                    menuTag.getMenuName(),
                                    "",
                                    menuTag.getSeq(),
                                    menuTag.getStatus(),
                                    menuTagSearchVO,
                                    null,
                                    null,
                                    categoryTag.getSid(),
                                    menuTag.getSid()));
                            allTagDt.add(
                                new TagDtVO(
                                    "Tag",
                                    tag.getSid(),
                                    "",
                                    "",
                                    tag.getTagName(),
                                    tag.getSeq(),
                                    tag.getStatus(),
                                    null,
                                    subTagSearchVO,
                                    null,
                                    categoryTag.getSid(),
                                    menuTag.getSid()));
                        } else {
                            if (!allTagDt.contains(
                                new TagDtVO(
                                    "MenuTag",
                                    menuTag.getSid(),
                                    "",
                                    menuTag.getMenuName(),
                                    "",
                                    menuTag.getSeq()))) {
                                allTagDt.add(
                                    new TagDtVO(
                                        "MenuTag",
                                        menuTag.getSid(),
                                        "",
                                        menuTag.getMenuName(),
                                        "",
                                        menuTag.getSeq(),
                                        menuTag.getStatus(),
                                        menuTagSearchVO,
                                        null,
                                        null,
                                        categoryTag.getSid(),
                                        menuTag.getSid()));
                                allTagDt.add(
                                    new TagDtVO(
                                        "Tag",
                                        tag.getSid(),
                                        "",
                                        "",
                                        tag.getTagName(),
                                        tag.getSeq(),
                                        tag.getStatus(),
                                        null,
                                        subTagSearchVO,
                                        null,
                                        categoryTag.getSid(),
                                        menuTag.getSid()));
                            } else {
                                if (!allTagDt.contains(
                                    new TagDtVO("Tag", tag.getSid(), "", "", tag.getTagName(),
                                        tag.getSeq()))) {
                                    allTagDt.add(
                                        new TagDtVO(
                                            "Tag",
                                            tag.getSid(),
                                            "",
                                            "",
                                            tag.getTagName(),
                                            tag.getSeq(),
                                            tag.getStatus(),
                                            null,
                                            subTagSearchVO,
                                            null,
                                            categoryTag.getSid(),
                                            menuTag.getSid()));
                                }
                            }
                        }
                    } else {
                        for (WCExecDepSetting execDepSetting : execDepSettingList) {
                            // 判斷查詢條件(單位)
                            // 執行單位
                            boolean isHaveExec =
                                query.getExecOrg().isEmpty() || query.getExecOrg().stream()
                                    .anyMatch(
                                        item ->
                                            execDepSetting.getExec_dep_sid() != null
                                                && execDepSetting.getExec_dep_sid()
                                                .equals(item.getSid()));
                            // 指派單位
                            boolean isHaveTransDep =
                                query.getTransOrgs().isEmpty() || query.getTransOrgs().stream()
                                    .anyMatch(
                                        item -> {
                                            List<Integer> assignDepSids = Lists.newArrayList();
                                            if (execDepSetting.getTransdep() != null
                                                && execDepSetting.getTransdep().getUserDepTos()
                                                != null) {
                                                execDepSetting
                                                    .getTransdep()
                                                    .getUserDepTos()
                                                    .forEach(
                                                        subItem -> {
                                                            assignDepSids.add(Integer.valueOf(
                                                                subItem.getDepSid()));
                                                        });
                                            }
                                            return assignDepSids != null
                                                && assignDepSids.contains(item.getSid());
                                        });
                            // 相關單位
                            isRelateDep =
                                query.getRelatedOrg().isEmpty() || query.getRelatedOrg().stream()
                                    .anyMatch(
                                        item -> {
                                            List<Integer> useDepSid = this.getUseDepSids(tag);
                                            return useDepSid == null
                                                || useDepSid.isEmpty()
                                                || useDepSid.contains(item.getSid());
                                        })
                                    || query.getRelatedOrg().stream()
                                    .anyMatch(
                                        item ->
                                            execDepSetting.getExec_dep_sid() != null
                                                && execDepSetting.getExec_dep_sid()
                                                .equals(item.getSid()))
                                    || query.getRelatedOrg().stream()
                                    .anyMatch(
                                        item -> {
                                            List<Integer> readDepSid = this.getReadDepSids(tag);
                                            return readDepSid != null
                                                && !readDepSid.isEmpty()
                                                && readDepSid.contains(item.getSid());
                                        })
                                    || query.getRelatedOrg().stream()
                                    .anyMatch(
                                        item -> {
                                            List<Integer> assignDepSids = Lists.newArrayList();
                                            if (execDepSetting.getTransdep() != null
                                                && execDepSetting.getTransdep().getUserDepTos()
                                                != null) {
                                                execDepSetting
                                                    .getTransdep()
                                                    .getUserDepTos()
                                                    .forEach(
                                                        subItem -> {
                                                            assignDepSids.add(
                                                                Integer.valueOf(
                                                                    subItem.getDepSid()));
                                                        });
                                            }
                                            return assignDepSids != null
                                                && assignDepSids.contains(item.getSid());
                                        });
                            if (!(isHaveSubTagUse
                                && isHaveViewDep
                                && isHaveExec
                                && isHaveTransDep
                                && isRelateDep)) {
                                continue;
                            }

                            // 指派人員
                            boolean isHaveAssignUser =
                                query.getAssignUser().isEmpty() || query.getAssignUser().stream()
                                    .anyMatch(
                                        item -> {
                                            List<Integer> assignUserSids = Lists.newArrayList();
                                            if (execDepSetting.isNeedAssigned()
                                                && execDepSetting.isReadAssignSetting()) {
                                                if (execDepSetting.getTransUser() != null
                                                    && execDepSetting.getTransUser().getUserTos()
                                                    != null) {
                                                    execDepSetting
                                                        .getTransUser()
                                                        .getUserTos()
                                                        .forEach(
                                                            subItem -> {
                                                                assignUserSids.add(
                                                                    subItem.getSid());
                                                            });
                                                }
                                            }
                                            return assignUserSids != null
                                                && assignUserSids.contains(item.getSid());
                                        });
                            // 執行單位簽核人員
                            boolean isHaveExecUser =
                                query.getExecSignUser().isEmpty() || query.getExecSignUser()
                                    .stream()
                                    .anyMatch(
                                        item -> {
                                            List<Integer> execSignUserSids = Lists.newArrayList();
                                            if (execDepSetting.getExecSignMember() != null
                                                && execDepSetting.getExecSignMember().getUserTos()
                                                != null) {
                                                execDepSetting
                                                    .getExecSignMember()
                                                    .getUserTos()
                                                    .forEach(
                                                        subItem -> {
                                                            execSignUserSids.add(subItem.getSid());
                                                        });
                                            }
                                            return execSignUserSids != null
                                                && execSignUserSids.contains(item.getSid());
                                        });
                            if (!(isHaveAssignUser && isHaveExecUser)) {
                                continue;
                            }

                            // 判斷查詢條件(執行模式)
                            boolean isHaveModeType =
                                query.getExecMode().equals("")
                                    || query.getExecMode().equals("派工")
                                    == execDepSetting.isNeedAssigned();
                            if (!isHaveModeType) {
                                continue;
                            }

                            int execIndex =
                                execDepSettingSearchVOs.indexOf(
                                    new ExecDepSettingSearchVO(execDepSetting.getSid()));
                            ExecDepSettingSearchVO execDepSettingSearchVO =
                                new ExecDepSettingSearchVO(execDepSetting.getSid());
                            if (execIndex != -1) {
                                execDepSettingSearchVO = execDepSettingSearchVOs.get(execIndex);
                            }
                            if (!allTagDt.contains(
                                new TagDtVO(
                                    "CategoryTag",
                                    categoryTag.getSid(),
                                    categoryTag.getCategoryName(),
                                    "",
                                    "",
                                    categoryTag.getSeq()))) {
                                allTagDt.add(
                                    new TagDtVO(
                                        "CategoryTag",
                                        categoryTag.getSid(),
                                        categoryTag.getCategoryName(),
                                        "",
                                        "",
                                        categoryTag.getSeq(),
                                        categoryTag.getStatus()));
                                allTagDt.add(
                                    new TagDtVO(
                                        "MenuTag",
                                        menuTag.getSid(),
                                        "",
                                        menuTag.getMenuName(),
                                        "",
                                        menuTag.getSeq(),
                                        menuTag.getStatus(),
                                        menuTagSearchVO,
                                        null,
                                        null,
                                        categoryTag.getSid(),
                                        menuTag.getSid()));
                                allTagDt.add(
                                    new TagDtVO(
                                        "Tag",
                                        tag.getSid(),
                                        "",
                                        "",
                                        tag.getTagName(),
                                        tag.getSeq(),
                                        tag.getStatus(),
                                        null,
                                        subTagSearchVO,
                                        execDepSettingSearchVO,
                                        categoryTag.getSid(),
                                        menuTag.getSid()));
                            } else {
                                if (!allTagDt.contains(
                                    new TagDtVO(
                                        "MenuTag",
                                        menuTag.getSid(),
                                        "",
                                        menuTag.getMenuName(),
                                        "",
                                        menuTag.getSeq()))) {
                                    allTagDt.add(
                                        new TagDtVO(
                                            "MenuTag",
                                            menuTag.getSid(),
                                            "",
                                            menuTag.getMenuName(),
                                            "",
                                            menuTag.getSeq(),
                                            menuTag.getStatus(),
                                            menuTagSearchVO,
                                            null,
                                            null,
                                            categoryTag.getSid(),
                                            menuTag.getSid()));
                                    allTagDt.add(
                                        new TagDtVO(
                                            "Tag",
                                            tag.getSid(),
                                            "",
                                            "",
                                            tag.getTagName(),
                                            tag.getSeq(),
                                            tag.getStatus(),
                                            null,
                                            subTagSearchVO,
                                            execDepSettingSearchVO,
                                            categoryTag.getSid(),
                                            menuTag.getSid()));
                                } else {
                                    if (!allTagDt.contains(
                                        new TagDtVO("Tag", tag.getSid(), "", "", tag.getTagName(),
                                            tag.getSeq()))) {
                                        allTagDt.add(
                                            new TagDtVO(
                                                "Tag",
                                                tag.getSid(),
                                                "",
                                                "",
                                                tag.getTagName(),
                                                tag.getSeq(),
                                                tag.getStatus(),
                                                null,
                                                subTagSearchVO,
                                                execDepSettingSearchVO,
                                                categoryTag.getSid(),
                                                menuTag.getSid()));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return allTagDt;
    }

    /**
     * 抓取可使用單位
     *
     * @param obj
     * @return
     */
    private List<Integer> getUseDepSids(Object obj) {
        List<Integer> orgSids = Lists.newArrayList();
        WCConfigTempletVo templet = null;
        List<Org> orgs = Lists.newArrayList();
        boolean isUserContainFollowing = false;
        if (obj instanceof WCMenuTag) {
            WCMenuTag wcMenuTag = (WCMenuTag) obj;
            templet = WCConfigTempletManager.getInstance()
                .findBySid(wcMenuTag.getUseDepTempletSid());
            if (templet == null
                && wcMenuTag.getUseDep() != null
                && wcMenuTag.getUseDep().getUserDepTos() != null) {
                orgs =
                    wcMenuTag.getUseDep().getUserDepTos().stream()
                        .map(each -> WkOrgCache.getInstance()
                            .findBySid(Integer.valueOf(each.getDepSid())))
                        .collect(Collectors.toList());
                isUserContainFollowing = wcMenuTag.isUserContainFollowing();
            }
        } else if (obj instanceof WCTag) {
            WCTag wcTag = (WCTag) obj;
            templet = WCConfigTempletManager.getInstance()
                .findBySid(wcTag.getCanUserDepTempletSid());
            if (templet == null
                && wcTag.getCanUserDep() != null
                && wcTag.getCanUserDep().getUserDepTos() != null
                && !wcTag.getCanUserDep().getUserDepTos().isEmpty()) {
                orgs =
                    wcTag.getCanUserDep().getUserDepTos().stream()
                        .map(each -> WkOrgCache.getInstance()
                            .findBySid(Integer.valueOf(each.getDepSid())))
                        .collect(Collectors.toList());
                isUserContainFollowing = wcTag.isUserContainFollowing();
            }
        }

        if (templet != null && ConfigTempletType.SUB_TAG_CAN_USE.equals(templet.getTempletType())) {
            orgSids =
                WCConfigTempletManager.getInstance()
                    .getTempletDeps(templet.getSid(), templet.getCompSid());
        } else if (templet != null
            && ConfigTempletType.MENU_TAG_CAN_USE.equals(templet.getTempletType())) {
            orgSids =
                WCConfigTempletManager.getInstance()
                    .getTempletDeps(templet.getSid(), templet.getCompSid());
        } else {
            for (Org org : orgs) {
                orgSids.add(org.getSid());
                if (isUserContainFollowing) {
                    orgSids.addAll(
                        WkOrgCache.getInstance().findAllChild(org.getSid()).stream()
                            .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                            .map(Org::getSid)
                            .collect(Collectors.toList()));
                }
            }
        }

        return orgSids;
    }

    /**
     * 抓取可閱部門
     *
     * @param obj
     * @return
     */
    private List<Integer> getReadDepSids(WCTag wcTag) {
        List<Integer> orgSids = Lists.newArrayList();
        WCConfigTempletVo templet =
            WCConfigTempletManager.getInstance().findBySid(wcTag.getUseDepTempletSid());
        if (templet != null && ConfigTempletType.SUB_TAG_CAN_READ.equals(
            templet.getTempletType())) {
            orgSids =
                WCConfigTempletManager.getInstance()
                    .getTempletDeps(templet.getSid(), templet.getCompSid());
        } else if (wcTag.getUseDep() != null
            && wcTag.getUseDep().getUserDepTos() != null
            && !wcTag.getUseDep().getUserDepTos().isEmpty()) {
            List<Org> orgs =
                wcTag.getUseDep().getUserDepTos().stream()
                    .map(each -> WkOrgCache.getInstance()
                        .findBySid(Integer.valueOf(each.getDepSid())))
                    .collect(Collectors.toList());
            boolean isViewContainFollowing = wcTag.isViewContainFollowing();
            for (Org org : orgs) {
                orgSids.add(org.getSid());
                if (isViewContainFollowing) {
                    orgSids.addAll(
                        WkOrgCache.getInstance().findAllChild(org.getSid()).stream()
                            .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                            .map(Org::getSid)
                            .collect(Collectors.toList()));
                }
            }
        }

        return orgSids;
    }
}
