package com.cy.work.connect.web.view;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCMenuTagCache;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.manager.WCCategoryTagManager;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.web.common.MultipleALLTagTreeManager;
import com.cy.work.connect.web.common.MultipleTagTreeManager;
import com.cy.work.connect.web.logic.components.WCMasterLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.vo.TagVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 項目批次匯入 MBean
 *
 * @author jimmy_chou
 */
@Controller
@Scope("view")
public class Setting8Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8378344839023783361L;
    /**
     * 查詢區域組件
     */
    @Getter
    @Setter
    public Setting8SearchComponent searchComponent;

    @Getter
    @Setter
    public List<TreeNode> selNode;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCMasterLogicComponents wcMasterLogicComponents;
    @Autowired
    private WCCategoryTagManager wcCategoryTagManager;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WCCategoryTagCache wcCategoryTagCache;
    @Autowired
    private WCMenuTagCache wcMenuTagCache;
    @Autowired
    private WCTagCache wCTagCache;
    @Autowired
    private DisplayController displayController;
    @Getter
    private MultipleALLTagTreeManager allTagTreeManager;

    @PostConstruct
    public void init() {
        this.selNode = Lists.newArrayList();
        this.searchComponent = new Setting8SearchComponent();
        this.allTagTreeManager = new MultipleALLTagTreeManager();
        this.searchComponent
            .getSearchVo()
            .setImportCompany(
                Optional.ofNullable(orgManager.findById(SecurityFacade.getCompanyId()))
                    .map(Org::getSid)
                    .orElseGet(() -> null));
    }

    public void loadALLTags() {
        wcCategoryTagCache.updateCache();
        wcMenuTagCache.updateCache();
        wCTagCache.updateCache();
        this.selNode = Lists.newArrayList();
        allTagTreeManager = new MultipleALLTagTreeManager();
        allTagTreeManager.initTreeAllComp(
            searchComponent.getSearchVo().getImportCompany(), this.selNode);
    }

    public void copyTag() {
        boolean copyFlag = true;
        if (checkCopyCondition()) {
            /** 開始複製 */
            List<TreeNode> selNode = this.searchComponent.getSearchVo().getSelNode();
            List<String> multiCategoryTagList =
                selNode.stream()
                    .filter(item -> "CategoryTag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<String> multiMenuTagList =
                selNode.stream()
                    .filter(item -> "MenuTag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<String> multiTagList =
                selNode.stream()
                    .filter(item -> "Tag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<WCCategoryTag> CategoryTagList =
                wcCategoryTagManager.getWCCategoryTagsBySidList(multiCategoryTagList);
            List<WCMenuTag> MenuTagList = wcMenuTagManager.getWCMenuTagsBySidList(multiMenuTagList);
            List<WCTag> TagList = wcTagManager.getWCTagsBySidList(multiTagList);
            Integer countCopy = 0;
            StringBuilder sb = new StringBuilder();
            StringBuilder sbError = new StringBuilder();
            for (WCTag tag : TagList) {
                WCMenuTag menuTag = wcMenuTagManager.getWCMenuTagBySid(tag.getWcMenuTagSid());
                WCCategoryTag categoryTag =
                    wcCategoryTagManager.getWCCategoryTagBySid(menuTag.getWcCategoryTagSid());
                if (wcTagManager.countByTagNameAndCompanySid(
                    this.searchComponent.getSearchVo().getImportCompany(),
                    categoryTag.getCategoryName(),
                    menuTag.getMenuName(),
                    tag.getTagName())
                    == 0) {
                    copyFlag = true;
                    /** 複製類別 */
                    WCCategoryTag newCategoryTag = new WCCategoryTag();
                    if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        null,
                        null)
                        == 0) {
                        newCategoryTag.setCompSid(
                            this.searchComponent.getSearchVo().getImportCompany());
                        newCategoryTag.setMemo(categoryTag.getMemo());
                        newCategoryTag.setStatus(Activation.ACTIVE);
                        newCategoryTag.setCategoryName(categoryTag.getCategoryName());
                        if (copyFlag) { // 沒有錯誤才新增
                            newCategoryTag =
                                wcCategoryTagManager.createWCCategoryTag(
                                    newCategoryTag, SecurityFacade.getUserSid());
                        }
                    } else if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        null,
                        null)
                        == 1) {
                        newCategoryTag =
                            wcCategoryTagManager
                                .findByTagNameAndCompanySid(
                                    this.searchComponent.getSearchVo().getImportCompany(),
                                    categoryTag.getCategoryName(),
                                    null,
                                    null)
                                .get(0);
                    } else {
                        // 有多筆同名類別，無法複製。
                        copyFlag = false;
                        if (!WkStringUtils.isEmpty(sbError.toString())) {
                            sbError.append("<br/>");
                        }
                        sbError.append("【").append(categoryTag.getCategoryName()).append("】-");
                    }

                    /** 複製類別選單 */
                    WCMenuTag newMenuTag = new WCMenuTag();
                    if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        menuTag.getMenuName(),
                        null)
                        == 0) {
                        newMenuTag.setWcCategoryTagSid(newCategoryTag.getSid());
                        newMenuTag.setStatus(Activation.ACTIVE);
                        newMenuTag.setUseDep(new UserDep());
                        newMenuTag.setMenuName(menuTag.getMenuName());
                        newMenuTag.setItemName(menuTag.getItemName());
                        newMenuTag.setMemo(menuTag.getMemo());
                        newMenuTag.setDefaultTheme(menuTag.getDefaultTheme());
                        newMenuTag.setDefaultContent(menuTag.getDefaultContent());
                        newMenuTag.setDefaultMemo(menuTag.getDefaultMemo());
                        newMenuTag.setParentManager(false);
                        newMenuTag.setUserContainFollowing(false);
                        newMenuTag.setUseUser(new UsersTo());
                        if (copyFlag) { // 沒有錯誤才新增
                            newMenuTag =
                                wcMenuTagManager.createWCMenuTag(newMenuTag,
                                    SecurityFacade.getUserSid());
                        }
                    } else if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        menuTag.getMenuName(),
                        null)
                        == 1) {
                        newMenuTag =
                            wcMenuTagManager
                                .findByTagNameAndCompanySid(
                                    this.searchComponent.getSearchVo().getImportCompany(),
                                    categoryTag.getCategoryName(),
                                    menuTag.getMenuName(),
                                    null)
                                .get(0);
                    } else {
                        // 有多筆同名類別選單，無法複製。
                        copyFlag = false;
                        if (!WkStringUtils.isEmpty(sbError.toString())) {
                            sbError.append("<br/>");
                        }
                        sbError.append("【").append(categoryTag.getCategoryName()).append("】-");
                        sbError.append("【").append(menuTag.getMenuName()).append("】-");
                    }

                    /** 複製執行項目 */
                    WCTag newTag = new WCTag();
                    if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        menuTag.getMenuName(),
                        tag.getTagName())
                        == 0) {
                        newTag.setWcMenuTagSid(newMenuTag.getSid());
                        newTag.setStatus(Activation.ACTIVE);
                        newTag.setTagName(tag.getTagName());
                        newTag.setMemo(tag.getMemo());
                        newTag.setReqFlowType(tag.getReqFlowType());
                        newTag.setDefaultContent(tag.getDefaultContent());
                        newTag.setDefaultContentCss(tag.getDefaultContentCss());
                        newTag.setUploadFile(tag.getUploadFile());
                        newTag.setLegitimateRangeCheck(tag.isLegitimateRangeCheck());
                        newTag.setLegitimateRangeDays(tag.getLegitimateRangeDays());
                        newTag.setLegitimateRangeTitle(tag.getLegitimateRangeTitle());
                        newTag.setStartEndCheck(tag.isStartEndCheck());
                        newTag.setStartEndDays(tag.getStartEndDays());
                        newTag.setStartEndTitle(tag.getStartEndTitle());
                        newTag.setEndRuleDays(tag.getEndRuleDays());
                        newTag.setUseDep(new UserDep());
                        newTag.setUseUser(new UsersTo());
                        newTag.setCanUserDep(new UserDep());
                        if (copyFlag) { // 沒有錯誤才新增
                            newTag = wcTagManager.createWCTag(newTag, SecurityFacade.getUserSid());
                        }
                    } else if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        menuTag.getMenuName(),
                        tag.getTagName())
                        == 1) {
                        newTag =
                            wcTagManager
                                .findByTagNameAndCompanySid(
                                    this.searchComponent.getSearchVo().getImportCompany(),
                                    categoryTag.getCategoryName(),
                                    menuTag.getMenuName(),
                                    tag.getTagName())
                                .get(0);
                    } else {
                        // 有多筆同名執行項目，無法複製。
                        copyFlag = false;
                        if (!WkStringUtils.isEmpty(sbError.toString())) {
                            sbError.append("<br/>");
                        }
                        sbError.append("【").append(categoryTag.getCategoryName()).append("】-");
                        sbError.append("【").append(menuTag.getMenuName()).append("】-");
                        sbError.append("【").append(tag.getTagName()).append("】");
                    }

                    if (copyFlag) {
                        countCopy++;
                        if (!WkStringUtils.isEmpty(sb.toString())) {
                            sb.append("<br/>");
                        }
                        sb.append("【").append(categoryTag.getCategoryName()).append("】-");
                        sb.append("【").append(menuTag.getMenuName()).append("】-");
                        sb.append("【").append(tag.getTagName()).append("】");
                    }
                }
            }
            for (WCMenuTag menuTag : MenuTagList) {
                WCCategoryTag categoryTag =
                    wcCategoryTagManager.getWCCategoryTagBySid(menuTag.getWcCategoryTagSid());
                if (wcTagManager.countByTagNameAndCompanySid(
                    this.searchComponent.getSearchVo().getImportCompany(),
                    categoryTag.getCategoryName(),
                    menuTag.getMenuName(),
                    null)
                    == 0) {
                    copyFlag = true;
                    /** 複製類別 */
                    WCCategoryTag newCategoryTag = new WCCategoryTag();
                    if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        null,
                        null)
                        == 0) {
                        newCategoryTag.setCompSid(
                            this.searchComponent.getSearchVo().getImportCompany());
                        newCategoryTag.setMemo(categoryTag.getMemo());
                        newCategoryTag.setStatus(Activation.ACTIVE);
                        newCategoryTag.setCategoryName(categoryTag.getCategoryName());
                        if (copyFlag) { // 沒有錯誤才新增
                            newCategoryTag =
                                wcCategoryTagManager.createWCCategoryTag(
                                    newCategoryTag, SecurityFacade.getUserSid());
                        }
                    } else if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        null,
                        null)
                        == 1) {
                        newCategoryTag =
                            wcCategoryTagManager
                                .findByTagNameAndCompanySid(
                                    this.searchComponent.getSearchVo().getImportCompany(),
                                    categoryTag.getCategoryName(),
                                    null,
                                    null)
                                .get(0);
                    } else {
                        // 有多筆同名類別，無法複製。
                        copyFlag = false;
                        if (!WkStringUtils.isEmpty(sbError.toString())) {
                            sbError.append("<br/>");
                        }
                        sbError.append("【").append(categoryTag.getCategoryName()).append("】-");
                    }

                    /** 複製類別選單 */
                    WCMenuTag newMenuTag = new WCMenuTag();
                    if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        menuTag.getMenuName(),
                        null)
                        == 0) {
                        newMenuTag.setWcCategoryTagSid(newCategoryTag.getSid());
                        newMenuTag.setStatus(Activation.ACTIVE);
                        newMenuTag.setUseDep(new UserDep());
                        newMenuTag.setMenuName(menuTag.getMenuName());
                        newMenuTag.setItemName(menuTag.getItemName());
                        newMenuTag.setMemo(menuTag.getMemo());
                        newMenuTag.setDefaultTheme(menuTag.getDefaultTheme());
                        newMenuTag.setDefaultContent(menuTag.getDefaultContent());
                        newMenuTag.setDefaultMemo(menuTag.getDefaultMemo());
                        newMenuTag.setParentManager(false);
                        newMenuTag.setUserContainFollowing(false);
                        if (copyFlag) { // 沒有錯誤才新增
                            newMenuTag =
                                wcMenuTagManager.createWCMenuTag(newMenuTag,
                                    SecurityFacade.getUserSid());
                        }
                    } else if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        menuTag.getMenuName(),
                        null)
                        == 1) {
                        newMenuTag =
                            wcMenuTagManager
                                .findByTagNameAndCompanySid(
                                    this.searchComponent.getSearchVo().getImportCompany(),
                                    categoryTag.getCategoryName(),
                                    menuTag.getMenuName(),
                                    null)
                                .get(0);
                    } else {
                        // 有多筆同名類別選單，無法複製。
                        copyFlag = false;
                        if (!WkStringUtils.isEmpty(sbError.toString())) {
                            sbError.append("<br/>");
                        }
                        sbError.append("【").append(categoryTag.getCategoryName()).append("】-");
                        sbError.append("【").append(menuTag.getMenuName()).append("】-");
                    }

                    if (copyFlag) {
                        countCopy++;
                        if (!WkStringUtils.isEmpty(sb.toString())) {
                            sb.append("<br/>");
                        }
                        sb.append("【").append(categoryTag.getCategoryName()).append("】-");
                        sb.append("【").append(menuTag.getMenuName()).append("】-");
                    }
                }
            }
            for (WCCategoryTag categoryTag : CategoryTagList) {
                if (wcTagManager.countByTagNameAndCompanySid(
                    this.searchComponent.getSearchVo().getImportCompany(),
                    categoryTag.getCategoryName(),
                    null,
                    null)
                    == 0) {
                    copyFlag = true;
                    /** 複製類別 */
                    WCCategoryTag newCategoryTag = new WCCategoryTag();
                    if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        null,
                        null)
                        == 0) {
                        newCategoryTag.setCompSid(
                            this.searchComponent.getSearchVo().getImportCompany());
                        newCategoryTag.setMemo(categoryTag.getMemo());
                        newCategoryTag.setStatus(Activation.ACTIVE);
                        newCategoryTag.setCategoryName(categoryTag.getCategoryName());
                        if (copyFlag) { // 沒有錯誤才新增
                            newCategoryTag =
                                wcCategoryTagManager.createWCCategoryTag(
                                    newCategoryTag, SecurityFacade.getUserSid());
                        }
                    } else if (wcTagManager.countByTagNameAndCompanySid(
                        this.searchComponent.getSearchVo().getImportCompany(),
                        categoryTag.getCategoryName(),
                        null,
                        null)
                        == 1) {
                        newCategoryTag =
                            wcCategoryTagManager
                                .findByTagNameAndCompanySid(
                                    this.searchComponent.getSearchVo().getImportCompany(),
                                    categoryTag.getCategoryName(),
                                    null,
                                    null)
                                .get(0);
                    } else {
                        // 有多筆同名類別，無法複製。
                        copyFlag = false;
                        if (!WkStringUtils.isEmpty(sbError.toString())) {
                            sbError.append("<br/>");
                        }
                        sbError.append("【").append(categoryTag.getCategoryName()).append("】-");
                    }

                    if (copyFlag) {
                        countCopy++;
                        if (!WkStringUtils.isEmpty(sb.toString())) {
                            sb.append("<br/>");
                        }
                        sb.append("【").append(categoryTag.getCategoryName()).append("】-");
                    }
                }
            }

            loadALLTags();
            displayController.update("searchArea");
            displayController.update("id_queryResult");

            if (WkStringUtils.notEmpty(sb.toString())) {
                sb.insert(0, "複製完畢，共複製" + countCopy + "筆資料，複製類別如下:<br/><br/>");
                MessagesUtils.showInfo(sb.toString());
            }

            if (WkStringUtils.notEmpty(sbError.toString())) {
                sbError.insert(0, "複製發生問題，下述類別項目在目的公司有重複的名稱:<br/><br/>");
                MessagesUtils.showWarn(sbError.toString());
            }
        }
    }

    /**
     * 檢核複製Tag條件(並判斷是否需複製)
     */
    public boolean checkCopyCondition() {
        boolean result = true;
        if (WkStringUtils.isEmpty(this.searchComponent.getSearchVo().getSourceCompany())) {
            MessagesUtils.showWarn("請先選擇來源公司！");
            result = false;
        } else if (WkStringUtils.isEmpty(this.searchComponent.getSearchVo().getImportCompany())) {
            MessagesUtils.showWarn("請先選擇目的公司！");
            result = false;
        } else if (this.searchComponent
            .getSearchVo()
            .getSourceCompany()
            .equals(this.searchComponent.getSearchVo().getImportCompany())) {
            MessagesUtils.showWarn("來源公司與目的公司不可相同！");
            result = false;
        } else if (WkStringUtils.isEmpty(this.searchComponent.getSearchVo().getSelNode())) {
            MessagesUtils.showWarn("請先挑選複製類別！");
            result = false;
        } else {
            List<TreeNode> selNode = this.searchComponent.getSearchVo().getSelNode();
            List<String> multiCategoryTagList =
                selNode.stream()
                    .filter(item -> "CategoryTag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<String> multiMenuTagList =
                selNode.stream()
                    .filter(item -> "MenuTag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<String> multiTagList =
                selNode.stream()
                    .filter(item -> "Tag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<WCCategoryTag> CategoryTagList =
                wcCategoryTagManager.getWCCategoryTagsBySidList(multiCategoryTagList);
            List<WCMenuTag> MenuTagList = wcMenuTagManager.getWCMenuTagsBySidList(multiMenuTagList);
            List<WCTag> TagList = wcTagManager.getWCTagsBySidList(multiTagList);
            Integer countCopy = 0;
            StringBuilder sb = new StringBuilder();
            for (WCCategoryTag categoryTag : CategoryTagList) {
                if (wcTagManager.countByTagNameAndCompanySid(
                    this.searchComponent.getSearchVo().getImportCompany(),
                    categoryTag.getCategoryName(),
                    null,
                    null)
                    == 0) {
                    countCopy++;
                }
            }
            for (WCMenuTag menuTag : MenuTagList) {
                WCCategoryTag categoryTag =
                    wcCategoryTagManager.getWCCategoryTagBySid(menuTag.getWcCategoryTagSid());
                if (wcTagManager.countByTagNameAndCompanySid(
                    this.searchComponent.getSearchVo().getImportCompany(),
                    categoryTag.getCategoryName(),
                    menuTag.getMenuName(),
                    null)
                    == 0) {
                    countCopy++;
                }
            }
            for (WCTag tag : TagList) {
                WCMenuTag menuTag = wcMenuTagManager.getWCMenuTagBySid(tag.getWcMenuTagSid());
                WCCategoryTag categoryTag =
                    wcCategoryTagManager.getWCCategoryTagBySid(menuTag.getWcCategoryTagSid());
                if (wcTagManager.countByTagNameAndCompanySid(
                    this.searchComponent.getSearchVo().getImportCompany(),
                    categoryTag.getCategoryName(),
                    menuTag.getMenuName(),
                    tag.getTagName())
                    == 0) {
                    countCopy++;
                } else {
                    if (!WkStringUtils.isEmpty(sb.toString())) {
                        sb.append("<br/>");
                    }
                    sb.append("【").append(categoryTag.getCategoryName()).append("】-");
                    sb.append("【").append(menuTag.getMenuName()).append("】-");
                    sb.append("【").append(tag.getTagName()).append("】");
                }
            }
            if (WkStringUtils.notEmpty(sb.toString())) {
                sb.insert(0, "您挑選複製的類別在目的公司已有, 目前無法複製！ 已存在類別如下:<br/><br/>");
                MessagesUtils.showWarn(sb.toString());
                result = false;
            } else if (countCopy == 0) {
                MessagesUtils.showWarn("您挑選複製的類別在目的公司已存在，無須複製！");
                result = false;
            }
        }
        return result;
    }

    /**
     * 刪除Tag
     */
    public void deleteTag() {
        List<TreeNode> selNode =
            allTagTreeManager.getSelNode() == null
                ? Lists.newArrayList()
                : Arrays.asList(allTagTreeManager.getSelNode());
        List<String> multiCategoryTagList =
            selNode.stream()
                .filter(item -> "CategoryTag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());
        List<String> multiMenuTagList =
            selNode.stream()
                .filter(item -> "MenuTag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());
        List<String> multiTagList =
            selNode.stream()
                .filter(item -> "Tag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());
        StringBuilder sb = new StringBuilder();
        /** 刪除Tag檢核是否已被參考 */
        for (String categorySid : multiCategoryTagList) {
            if (wcMasterLogicComponents.checkCategoryTagUsed(categorySid)) {
                if (!WkStringUtils.isEmpty(sb.toString())) {
                    sb.append("<br/>");
                }
                WCCategoryTag category = wcCategoryTagManager.getWCCategoryTagBySid(categorySid);
                sb.append("【").append(category.getCategoryName()).append("】");
            }
        }

        for (String menuTagSid : multiMenuTagList) {
            if (wcMasterLogicComponents.checkMenuTagUsed(menuTagSid)) {
                if (!WkStringUtils.isEmpty(sb.toString())) {
                    sb.append("<br/>");
                }
                WCMenuTag menuTag = wcMenuTagManager.findMenuTagBySid(menuTagSid);
                WCCategoryTag category =
                    wcCategoryTagManager.getWCCategoryTagBySid(menuTag.getWcCategoryTagSid());
                sb.append("【").append(category.getCategoryName()).append("】-");
                sb.append("【").append(menuTag.getMenuName()).append("】");
            }
        }

        for (String tagSid : multiTagList) {
            if (wcMasterLogicComponents.checkTagUsed(tagSid)) {
                if (!WkStringUtils.isEmpty(sb.toString())) {
                    sb.append("<br/>");
                }
                WCTag tag = wcTagManager.getWCTagBySid(tagSid);
                WCMenuTag menuTag = wcMenuTagManager.findMenuTagBySid(tag.getWcMenuTagSid());
                WCCategoryTag category =
                    wcCategoryTagManager.getWCCategoryTagBySid(menuTag.getWcCategoryTagSid());
                sb.append("【").append(category.getCategoryName()).append("】-");
                sb.append("【").append(menuTag.getMenuName()).append("】-");
                sb.append("【").append(tag.getTagName()).append("】");
            }
        }

        if (WkStringUtils.notEmpty(sb.toString())) {
            sb.insert(0, "類別已經被使用, 目前無法刪除！ 被使用類別如下:<br/><br/>");
            MessagesUtils.showWarn(sb.toString());
        } else {
            /** 刪除類別開始 */
            Integer countDelete = 0;
            for (String tagSid : multiTagList) {
                List<WCExecDepSetting> execDepSettings =
                    execDepSettingManager.getExecDepSettingFromCacheByWcTagSid(tagSid, null);
                for (WCExecDepSetting execDepSetting : execDepSettings) {
                    execDepSettingManager.deleteExecDepSetting(execDepSetting.getSid());
                    countDelete++;
                }
                wcTagManager.deleteWCTag(tagSid);
                countDelete++;
            }
            wCTagCache.updateCache();

            for (String menuTagSid : multiMenuTagList) {
                List<WCTag> tags = wcTagManager.getWCTagForManager(menuTagSid, null);
                for (WCTag tag : tags) {
                    List<WCExecDepSetting> execDepSettings =
                        execDepSettingManager.getExecDepSettingFromCacheByWcTagSid(tag.getSid(), null);
                    for (WCExecDepSetting execDepSetting : execDepSettings) {
                        execDepSettingManager.deleteExecDepSetting(execDepSetting.getSid());
                        countDelete++;
                    }
                    wcTagManager.deleteWCTag(tag.getSid());
                    countDelete++;
                }
                wcMenuTagManager.deleteMenuTag(menuTagSid);
                countDelete++;
            }
            wCTagCache.updateCache();
            wcMenuTagCache.updateCache();

            for (String categorySid : multiCategoryTagList) {
                List<WCMenuTag> menuTags = wcMenuTagManager.getWCMenuTagByCategorySid(categorySid,
                    null);
                for (WCMenuTag menuTag : menuTags) {
                    List<WCTag> tags = wcTagManager.getWCTagForManager(menuTag.getSid(), null);
                    for (WCTag tag : tags) {
                        List<WCExecDepSetting> execDepSettings =
                            execDepSettingManager.getExecDepSettingFromCacheByWcTagSid(tag.getSid(), null);
                        for (WCExecDepSetting execDepSetting : execDepSettings) {
                            execDepSettingManager.deleteExecDepSetting(execDepSetting.getSid());
                            countDelete++;
                        }
                        wcTagManager.deleteWCTag(tag.getSid());
                        countDelete++;
                    }
                    wcMenuTagManager.deleteMenuTag(menuTag.getSid());
                    countDelete++;
                }
                wcCategoryTagManager.deleteCategoryTag(categorySid);
                countDelete++;
            }
            loadALLTags();
            displayController.update("id_queryResult");
            MessagesUtils.showInfo("刪除完畢，共刪除" + countDelete + "筆資料。");
        }
    }

    /**
     * 查詢區域組件
     *
     * @author allen
     */
    public class Setting8SearchComponent implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -6215270850591781325L;

        /**
         * 挑選類別
         */
        @Getter
        private MultipleTagTreeManager tagTreeManager;

        /**
         * 查詢條件
         */
        @Getter
        @Setter
        private Setting8SearchVo searchVo;

        /**
         * 建構子
         */
        public Setting8SearchComponent() {
            this.searchVo = new Setting8SearchVo();
            this.tagTreeManager = new MultipleTagTreeManager();
        }

        public void loadTags() {
            if (WkStringUtils.isEmpty(searchVo.getSourceCompany())) {
                MessagesUtils.showWarn("請先選擇來源公司！");
                return;
            }
            wcCategoryTagCache.updateCache();
            wcMenuTagCache.updateCache();
            wCTagCache.updateCache();
            tagTreeManager = new MultipleTagTreeManager();
            tagTreeManager.initTreeAllComp(searchVo.getSourceCompany(), searchVo.getSelNode());
            DisplayController.getInstance().showPfWidgetVar("tagUnitDlg");
        }

        /**
         * 挑選類別設定視窗點選確定
         */
        public void saveTag() {
            searchVo.setSelNode(
                tagTreeManager.getSelNode() == null
                    ? Lists.newArrayList()
                    : Arrays.asList(tagTreeManager.getSelNode()));
            DisplayController.getInstance().hidePfWidgetVar("tagUnitDlg");
        }

        /**
         * Setting8 查詢欄位容器
         *
         * @author allen
         */
        public class Setting8SearchVo implements Serializable {

            /**
             *
             */
            private static final long serialVersionUID = 8447209041255513261L;
            /**
             * 挑選類別
             */
            @Getter
            @Setter
            List<TreeNode> selNode;
            /**
             * 來源公司
             */
            @Getter
            @Setter
            private Integer sourceCompany;
            /**
             * 目的公司
             */
            @Getter
            @Setter
            private Integer importCompany;

            public Setting8SearchVo() {
                this.selNode = Lists.newArrayList();
            }
        }
    }
}
