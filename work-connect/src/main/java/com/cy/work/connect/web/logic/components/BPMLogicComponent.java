/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.view.vo.FlowNodeVO;
import com.cy.work.connect.web.view.vo.SignRole;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class BPMLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3729709798734779647L;

    private static BPMLogicComponent instance;
    /**
     * 介面使用(合併儲存格)-會簽者GroupID
     */
    @Getter
    public final String reqContinueGroupID = "1";
    /**
     * 介面使用(合併儲存格)-會簽者名稱
     */
    @Getter
    public final String continueRoleName = "需求方會簽";
    /**
     * 介面使用(合併儲存格)-會簽者 Style class
     */
    @Getter
    public final String continueClz = "continueClz";
    /**
     * 介面使用(合併儲存格)-加會簽者GroupID
     */
    @Getter
    public final String execContinueGroupID = "2";
    /**
     * 介面使用(合併儲存格)-加會簽者名稱
     */
    @Getter
    public final String execContinueRoleName = "執行方會簽";
    /**
     * 介面使用(合併儲存格)-加會簽者 Style class
     */
    @Getter
    public final String execContinueClz = "execContinueClz";
    /**
     * 介面使用(合併儲存格)-申請者GroupID
     */
    private final String reqFlowGroupID = "0";
    /**
     * 介面使用(合併儲存格)-申請者名稱
     */
    private final String reqRoleName = "需求方簽核";
    /**
     * 介面使用(合併儲存格)-申請者 Style class
     */
    private final String reqClz = "reqClz";
    /**
     * 介面使用(合併儲存格)-核准者GroupID
     */
    private final String execGroupID = "3";
    /**
     * 介面使用(合併儲存格)-核准者名稱
     */
    private final String execRoleName = "執行方簽核";
    /**
     * 介面使用(合併儲存格)-核准者 Style class
     */
    private final String execClz = "execClz";
    /**
     * 不執行
     */
    private final String NOT_EXECUTE = "不執行";

    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    /**
     * 簽呈意見說明邏輯
     */
    @Autowired
    private WCOpinionDescripeSettingLogicComponent opinionDescripeSettingLogicComponent;

    @Autowired
    private WkUserCache userManager;
    @Autowired
    private BpmManager bpmManager;

    public static BPMLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        BPMLogicComponent.instance = this;
    }

    public FlowNodeVO transToFlowNodeVO(
            ProcessTaskBase processTaskBase, String defaultSignName, String bpmId, User loginUser) {
        String roleName = (Strings.isNullOrEmpty(processTaskBase.getRoleName()))
                ? "申請人"
                : processTaskBase.getRoleName();
        if (processTaskBase.getIsAgent() != null
                && processTaskBase.getIsAgent()
                && processTaskBase instanceof ProcessTaskHistory) {
            roleName = roleName + "(代)";
        }
        String signName = "";
        String singTime = "";
        if (processTaskBase instanceof ProcessTask) {
            List<String> ids = bpmManager.findTaskCanSignUserIds(bpmId);
            if (ids.contains(loginUser.getId())) {
                signName = loginUser.getName();
            } else {
                signName = defaultSignName;
            }
        } else if (processTaskBase instanceof ProcessTaskHistory) {
            ProcessTaskHistory ph = (ProcessTaskHistory) processTaskBase;
            signName = ph.getExecutorName();
            singTime = ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(), ph.getTaskEndTime());
        }
        FlowNodeVO fnv = new FlowNodeVO(roleName, signName, singTime, processTaskBase.getRollbackInfo());
        return fnv;
    }

    /**
     * 取得簽核記錄List
     *
     * @param peSid     簽呈Sid
     * @param loginUser 登入者
     * @return
     */
    public List<SignRole> getSignRole(String wcSid, User loginUser) {
        List<SignRole> signRoles = Lists.newArrayList();
        try {
            // ====================================
            // 取得所有簽核流程資訊
            // ====================================
            // 需求方簽核流程
            List<WCManagerSignInfo> allActiveReqFlowSignInfos = this.wcManagerSignInfoManager.findAllReqFlowSignInfosForViewSignInfo(wcSid);
            // 執行方流程
            List<WCExecManagerSignInfo> allActiveExecFlowSignInfos = this.wcExecManagerSignInfoManager.findAllExecFlowSignInfosForViewSignInfo(wcSid);

            // ====================================
            // 需求方申請流程
            // ====================================
            List<SingleManagerSignInfo> reqFlowSignInfos = allActiveReqFlowSignInfos.stream()
                    .filter(reqSignInfo -> SignType.SIGN.equals(reqSignInfo.getSignType()))
                    .map(signInfo -> new SingleManagerSignInfo(signInfo))
                    .collect(Collectors.toList());

            signRoles.addAll(
                    this.transToSignRole(
                            reqFlowSignInfos,
                            ManagerSingInfoType.MANAGERSIGNINFO,
                            reqFlowGroupID,
                            reqRoleName,
                            reqClz,
                            loginUser,
                            SignType.SIGN));

            // ====================================
            // 需求方會簽流程
            // ====================================
            List<SingleManagerSignInfo> reqFlowCounterSignInfos = allActiveReqFlowSignInfos.stream()
                    .filter(reqSignInfo -> SignType.COUNTERSIGN.equals(reqSignInfo.getSignType()))
                    .map(signInfo -> new SingleManagerSignInfo(signInfo))
                    .collect(Collectors.toList());

            // 會簽者
            // List<SingleManagerSignInfo> reqContinueFlowSignInfos = wcManagerSignInfoManager.getCounterSignInfos(wcSid);
            signRoles.addAll(
                    this.transToSignRole(
                            reqFlowCounterSignInfos,
                            ManagerSingInfoType.MANAGERSIGNINFO,
                            reqContinueGroupID,
                            continueRoleName,
                            continueClz,
                            loginUser,
                            SignType.COUNTERSIGN));

            // ====================================
            // 執行方簽核流程
            // ====================================
            List<SingleManagerSignInfo> execFlowSignInfos = allActiveExecFlowSignInfos.stream()
                    .filter(reqSignInfo -> SignType.SIGN.equals(reqSignInfo.getSignType()))
                    .map(signInfo -> new SingleManagerSignInfo(signInfo))
                    .collect(Collectors.toList());

            signRoles.addAll(
                    this.transToSignRole(
                            execFlowSignInfos,
                            ManagerSingInfoType.CHECKMANAGERINFO,
                            execGroupID,
                            execRoleName,
                            execClz,
                            loginUser,
                            SignType.SIGN));

            // ====================================
            // 執行方會簽流程
            // ====================================
            // List<SingleManagerSignInfo> execContinueSignInfos = wcExecManagerSignInfoManager.getCounterSignInfos(wcSid);
            List<SingleManagerSignInfo> execFlowCounterSignInfos = allActiveExecFlowSignInfos.stream()
                    .filter(reqSignInfo -> SignType.COUNTERSIGN.equals(reqSignInfo.getSignType()))
                    .map(signInfo -> new SingleManagerSignInfo(signInfo))
                    .collect(Collectors.toList());

            signRoles.addAll(
                    this.transToSignRole(
                            execFlowCounterSignInfos,
                            ManagerSingInfoType.CHECKMANAGERINFO,
                            execContinueGroupID,
                            execContinueRoleName,
                            execContinueClz,
                            loginUser,
                            SignType.COUNTERSIGN));
        } catch (Exception e) {
            log.warn("getSignRole ERROR", e);
        }
        return signRoles;
    }

    private void transToSignRole(
            List<SignRole> signRoles,
            SingleManagerSignInfo singleManagerSignInfo,
            ManagerSingInfoType managerSingInfoType,
            String flowGroupID,
            String groupRoleName,
            String clz,
            User loginUser,
            List<ProcessTaskBase> processTaskBaseList,
            SignType signType) {

        // 水管圖為空時, 跳過
        if (WkStringUtils.isEmpty(processTaskBaseList)) {
            return;
        }

        Integer processTaskItemIndex = -1;
        for (ProcessTaskBase processTaskItem : processTaskBaseList) {
            processTaskItemIndex += 1;

            // 角色名稱
            String roleName = "";
            String signName = "";
            String singTime = "";
            String opinionDescripe = "";
            String opinionDescripeAll = "";
            String opinionDescripeRollBackOrInvail = "";

            // ====================================
            // 判斷簽核人員『角色』顯示名稱
            // ====================================
            if (SignType.SIGN.equals(signType)) {

                if (WkStringUtils.safeTrim(processTaskItem.getTaskName()).indexOf("需求方最終簽核") > -1) {
                    // 判斷為最終簽核節點時, roleName 顯示固定文字
                    roleName = "需求方最終簽核";

                } else if (processTaskItemIndex == 0) {
                    // 『第一筆』固定顯示為『申請人』
                    roleName = "申請人";

                } else if (WkStringUtils.isEmpty(processTaskItem.getRoleName())) {
                    // 『非層簽節點』未回傳角色名稱時，代表不是層簽節點, 需自行撈 role name
                    roleName = this.bpmManager.getUserMajorRoleName(loginUser.getId());

                } else {
                    // 『層簽』時, 會帶回角色名稱
                    roleName = processTaskItem.getRoleName();
                }

            }

            if (SignType.COUNTERSIGN.equals(signType) ||
                    (SignType.SIGN.equals(signType)
                            && ManagerSingInfoType.CHECKMANAGERINFO.equals(managerSingInfoType))) {
                // 已簽
                if (processTaskItem instanceof ProcessTaskHistory) {
                    // 取得簽核角色名稱
                    roleName = ((ProcessTaskHistory) processTaskItem).getExecutorRoleName();
                }

                // 未簽
                if (processTaskItem instanceof ProcessTask) {
                    // 撈回待簽者主要角色
                    roleName = BpmManager.getInstance().getUserMajorRoleName(processTaskItem.getUserID());
                }

                // 應該不會?
                if (WkStringUtils.isEmpty(roleName)) {
                    roleName = "申請人";
                }
            }

            // 代理人時，顯示 (代)
            if (processTaskItem.getIsAgent() != null
                    && processTaskItem.getIsAgent()
                    && processTaskItem instanceof ProcessTaskHistory) {
                roleName = roleName + "(代)";
            }

            // ====================================
            //
            // ====================================
            if (processTaskItem instanceof ProcessTask) {
                List<String> ids = bpmManager.findTaskCanSignUserIds(
                        singleManagerSignInfo.getInstanceID());
                if (ids.contains(loginUser.getId())) {
                    if (!ids.get(0).equals(loginUser.getId())) {
                        roleName = roleName + "(代)";
                    }
                    signName = loginUser.getName();

                } else {
                    signName = singleManagerSignInfo.getDefaultSignName();
                }
                if (!ids.isEmpty()) {
                    User user = userManager.findById(ids.get(0));
                    opinionDescripe = opinionDescripeSettingLogicComponent.getOpinionDescripe(
                            managerSingInfoType, singleManagerSignInfo.getSignInfoSid(),
                            user.getSid());
                    opinionDescripeRollBackOrInvail = opinionDescripeSettingLogicComponent.getFlowOpinionDescripe(
                            managerSingInfoType,
                            singleManagerSignInfo.getSignInfoSid(),
                            user.getSid(),
                            opinionDescripe);
                }

                // 執行方執行人員-不執行
                if (ManagerSingInfoType.CHECKMANAGERINFO.equals(managerSingInfoType)
                        && SignType.SIGN.equals(signType)
                        && BpmStatus.INVALID.equals(singleManagerSignInfo.getStatus())) {
                    opinionDescripe = opinionDescripeSettingLogicComponent.getInvaildReason(
                            managerSingInfoType,
                            singleManagerSignInfo.getSignInfoSid(),
                            singleManagerSignInfo.getUserSid());

                    singTime = NOT_EXECUTE;
                }

            } else if (processTaskItem instanceof ProcessTaskHistory) {
                ProcessTaskHistory ph = (ProcessTaskHistory) processTaskItem;
                signName = ph.getExecutorName();
                singTime = ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateTimestampss.getValue(), ph.getTaskEndTime());
                User user = userManager.findById(ph.getExecutorID());
                opinionDescripe = opinionDescripeSettingLogicComponent.getOpinionDescripe(
                        managerSingInfoType, singleManagerSignInfo.getSignInfoSid(), user.getSid());
                opinionDescripeRollBackOrInvail = opinionDescripeSettingLogicComponent.getFlowOpinionDescripe(
                        managerSingInfoType,
                        singleManagerSignInfo.getSignInfoSid(),
                        user.getSid(),
                        opinionDescripe);
            }
            opinionDescripeAll = opinionDescripe + opinionDescripeRollBackOrInvail;

            SignRole signRole = settingSignRole(
                    flowGroupID,
                    roleName + signName,
                    groupRoleName,
                    roleName,
                    signName,
                    singTime,
                    opinionDescripe,
                    opinionDescripeRollBackOrInvail,
                    opinionDescripeAll,
                    clz);

            if (ManagerSingInfoType.CHECKMANAGERINFO.equals(managerSingInfoType)) {
                if (signRoles.contains(signRole)) {
                    try {
                        SignRole alreadySignRole = signRoles.get(signRoles.indexOf(signRole));
                        if (Strings.isNullOrEmpty(alreadySignRole.getSignDate())
                                && Strings.isNullOrEmpty(signRole.getSignDate())) {
                            return;
                        }
                        if (!NOT_EXECUTE.equals(signRole.getSignDate())
                                && !NOT_EXECUTE.equals(alreadySignRole.getSignDate())
                                && !Strings.isNullOrEmpty(alreadySignRole.getSignDate())
                                && !Strings.isNullOrEmpty(signRole.getSignDate())) {
                            Date alreadySignDate = ToolsDate.transStringToDate(
                                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(),
                                    alreadySignRole.getSignDate());
                            Date newSignDate = ToolsDate.transStringToDate(
                                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(),
                                    signRole.getSignDate());
                            long seconds = (alreadySignDate.getTime() - newSignDate.getTime()) / 1000;
                            if (seconds <= 1 || seconds >= -1) {
                                return;
                            }
                        }
                    } catch (Exception e) {
                        log.warn("轉換日期比對ERROR", e);
                    }
                    signRole.setSignUserId(
                            signRole.getSignUserId() + singleManagerSignInfo.getInstanceID());
                }
            }
            signRoles.add(signRole);
        }
    }

    /**
     * 將簽核物件轉換成介面顯示物件
     *
     * @param signInfos           簽核物件List
     * @param managerSingInfoType 簽核類型
     * @param flowGroupID         合併儲存格使用GroupID
     * @param groupRoleName       合併儲存格使用角色名稱
     * @param clz                 合併儲存格使用背景色
     * @param loginUser           登入者
     * @return
     */
    private List<SignRole> transToSignRole(
            List<SingleManagerSignInfo> signInfos,
            ManagerSingInfoType managerSingInfoType,
            String flowGroupID,
            String groupRoleName,
            String clz,
            User loginUser,
            SignType signType) {

        if (WkStringUtils.isEmpty(signInfos)) {
            return Lists.newArrayList();
        }

        List<SignRole> signRoles = Lists.newArrayList();
        try {
            for (SingleManagerSignInfo signInfo : signInfos) {
                List<ProcessTaskBase> pbs = BpmManager.getInstance()
                        .findSimulationChart(loginUser.getId(), signInfo.getInstanceID());

                this.transToSignRole(
                        signRoles,
                        signInfo,
                        managerSingInfoType,
                        flowGroupID,
                        groupRoleName,
                        clz,
                        loginUser,
                        pbs,
                        signType);
            }

        } catch (Exception e) {
            log.warn("transToSignRole ERROR", e);
        }
        return signRoles;
    }

    public List<SignRole> transToAddSignRole(
            List<SingleManagerSignInfo> signInfos,
            ManagerSingInfoType managerSingInfoType,
            String flowGroupID,
            String groupRoleName,
            String clz,
            User loginUser) {
        List<SignRole> signRoles = Lists.newArrayList();
        try {
            signInfos.forEach(
                    item -> {
                        List<ProcessTaskBase> pbs = BpmManager.getInstance()
                                .findSimulationChart(loginUser.getId(), item.getInstanceID());
                        if (pbs != null && !pbs.isEmpty()) {
                            this.transToSignRole(
                                    signRoles,
                                    item,
                                    managerSingInfoType,
                                    flowGroupID,
                                    groupRoleName,
                                    clz,
                                    loginUser,
                                    pbs,
                                    SignType.COUNTERSIGN);
                        } else {
                            try {
                                User user = userManager.findBySid(item.getUserSid());
                                String opinionDescripe = "";
                                opinionDescripe = opinionDescripeSettingLogicComponent.getOpinionDescripe(
                                        managerSingInfoType, item.getSignInfoSid(), user.getSid());
                                SignRole signRole = settingSignRole(
                                        flowGroupID,
                                        item.getInstanceID(),
                                        groupRoleName,
                                        "",
                                        user.getName(),
                                        "",
                                        opinionDescripe,
                                        "",
                                        "",
                                        clz);
                                signRoles.add(signRole);
                            } catch (Exception e) {
                                log.warn("transToAddSignRole ERROR", e);
                            }
                        }
                    });
        } catch (Exception e) {
            log.warn("transToSignRole ERROR", e);
        }
        return signRoles;
    }

    /**
     * 建立介面顯示物件
     *
     * @param groupId                 合併儲存格使用GroupID
     * @param signUserId              簽名UserID
     * @param roleName                合併儲存格使用角色名稱
     * @param deptName                部門名稱
     * @param userName                簽名User名稱
     * @param signDate                簽名時間
     * @param comment                 意見說明
     * @param commentRollBackOrInvail 退回或作廢理由
     * @param commentAll              意見說明+退回或作廢理由
     * @param clz                     合併儲存格使用背景色
     * @return
     */
    public SignRole settingSignRole(
            String groupId,
            String signUserId,
            String roleName,
            String deptName,
            String userName,
            String signDate,
            String comment,
            String commentRollBackOrInvail,
            String commentAll,
            String clz) {
        SignRole sr = new SignRole();
        sr.setGroupId(groupId);
        sr.setSignUserId(signUserId);
        sr.setRoleName(roleName);
        sr.setDeptName(deptName);
        sr.setUserName(userName);
        sr.setSignDate(signDate);
        sr.setComment(comment);
        sr.setCommentRollBackOrInvail(commentRollBackOrInvail);
        sr.setCommentAll(commentAll);
        sr.setClz(clz);
        return sr;
    }
}
