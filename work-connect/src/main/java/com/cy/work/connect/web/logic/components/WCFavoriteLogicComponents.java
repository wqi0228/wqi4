/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.logic.manager.WCFavoriteManager;
import com.cy.work.connect.vo.WCFavorite;
import java.io.Serializable;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 收藏邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCFavoriteLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4670604556005398643L;

    private static WCFavoriteLogicComponents instance;

    @Autowired
    private WCFavoriteManager wcFavoriteManager;

    public static WCFavoriteLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCFavoriteLogicComponents.instance = this;
    }

    /**
     * 收藏筆數 By 登入者Sid 及 收藏起始時間 及 收藏結束時間
     *
     * @param loginUserSid 登入者Sid
     * @param start        收藏起始時間
     * @param end          收藏結束時間
     * @return
     */
    public Integer findFavoriteCountByUserSidAndUpdateDt(Integer loginUserSid, Date start,
        Date end) {
        try {
            return wcFavoriteManager.findFavoriteCountByUserSidAndUpdateDt(loginUserSid, start,
                end);
        } catch (Exception e) {
            log.warn("findFavoriteCountByUserSidAndUpdateDt", e);
        }
        return 0;
    }

    /**
     * 該登入者在工作聯絡單是否收藏
     *
     * @param wc_Id        工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public boolean isFavorite(String wc_Id, Integer loginUserSid) {
        try {
            WCFavorite wcFavorite =
                wcFavoriteManager.getWCFavoriteByloginUserSidAndWCSid(wc_Id, loginUserSid);
            if (wcFavorite != null && wcFavorite.getStatus().equals(Activation.ACTIVE)) {
                return true;
            }
        } catch (Exception e) {
            log.warn("isFavorite", e);
        }
        return false;
    }

    /**
     * 將該工作聯絡單加入收藏
     *
     * @param wc_Id        工作聯絡單Sid
     * @param wc_no        工作聯絡單單號
     * @param loginUserSid 登入者Sid
     */
    public void addFavorite(String wc_Id, String wc_no, Integer loginUserSid) {
        WCFavorite wcFavorite =
            wcFavoriteManager.getWCFavoriteByloginUserSidAndWCSid(wc_Id, loginUserSid);
        if (wcFavorite != null) {
            wcFavorite.setStatus(Activation.ACTIVE);
            wcFavoriteManager.updateWCFavorite(wcFavorite, loginUserSid);
        } else {
            wcFavorite = new WCFavorite();
            wcFavorite.setWc_no(wc_no);
            wcFavorite.setWc_sid(wc_Id);
            wcFavoriteManager.createWCFavorite(wcFavorite, loginUserSid);
        }
    }

    /**
     * 將該工作聯絡單移除收藏
     *
     * @param wc_Id        工作聯絡單Sid
     * @param wc_no        工作聯絡單單號
     * @param loginUserSid 登入者Sid
     */
    public void removeFavorite(String wc_Id, String wc_no, Integer loginUserSid) {
        WCFavorite wcFavorite =
            wcFavoriteManager.getWCFavoriteByloginUserSidAndWCSid(wc_Id, loginUserSid);
        if (wcFavorite != null) {
            wcFavorite.setStatus(Activation.INACTIVE);
            wcFavoriteManager.updateWCFavorite(wcFavorite, loginUserSid);
        }
    }
}
