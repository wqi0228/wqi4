/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.helper.WCExecManagerSignInfoCheckHelper;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.web.view.vo.ExecManagerInfoVO;
import com.cy.work.connect.web.view.vo.FlowNodeVO;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 執行單位簽核邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCExecManagerSignInfoLogicComponent implements InitializingBean, Serializable {

    private static WCExecManagerSignInfoLogicComponent instance;

    public static WCExecManagerSignInfoLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCExecManagerSignInfoLogicComponent.instance = this;
    }

    /**
     *
     */
    private static final long serialVersionUID = 8278293329542822699L;

    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private BPMLogicComponent bpmLogicComponent;
    @Autowired
    private WCExecManagerSignInfoCheckHelper execManagerSignInfoCheckHelper;

    public boolean canRecoveryExecManagerInfo(String wc_SID, User loginUser) {
        boolean result = false;
        try {
            List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoManager.getGroupManagerSignInfo(wc_SID);
            for (GroupManagerSignInfo gi : groupManagerSignInfos) {
                for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                    if (si.getStatus().equals(BpmStatus.INVALID)) {
                        continue;
                    }
                    WCExecManagerSignInfo wcExecManagerSignInfo = wcExecManagerSignInfoManager.findBySid(si.getSignInfoSid());
                    List<ProcessTaskBase> pbs = BpmManager.getInstance()
                            .findSimulationChart(loginUser.getId(), si.getInstanceID());
                    if (execManagerSignInfoCheckHelper.isShowRecovery(loginUser.getId(),
                            si.getStatus(), pbs)
                            && !wcExecDepManager.isExecWorking(wc_SID, wcExecManagerSignInfo)) {
                        result = true;
                    }
                }
            }
        } catch (Exception e) {
            log.warn("canSignExecManagerInfo ERROR", e);
        }
        return result;
    }

    public boolean canRollBackExecManagerInfo(String wc_SID, User loginUser) {
        boolean result = false;
        try {
            List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoManager.getGroupManagerSignInfo(wc_SID);
            for (GroupManagerSignInfo gi : groupManagerSignInfos) {
                for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                    List<String> ids = BpmManager.getInstance()
                            .findTaskCanSignUserIds(si.getInstanceID());
                    if (si.getStatus().equals(BpmStatus.INVALID)) {
                        continue;
                    }
                    if (ids.contains(loginUser.getId()) && si.getSignType().equals(SignType.SIGN)) {
                        result = execManagerSignInfoCheckHelper.isCanRollBackToReq(wc_SID);
                    }
                }
            }
        } catch (Exception e) {
            log.warn("canSignExecManagerInfo ERROR", e);
        }
        return result;
    }

    public boolean canSignExecManagerInfo(String wc_SID, User loginUser) {
        boolean result = false;
        try {
            List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoManager.getGroupManagerSignInfo(wc_SID);
            for (GroupManagerSignInfo gi : groupManagerSignInfos) {
                for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                    List<String> ids = BpmManager.getInstance()
                            .findTaskCanSignUserIds(si.getInstanceID());
                    if (si.getStatus().equals(BpmStatus.INVALID)) {
                        continue;
                    }
                    if (ids.contains(loginUser.getId())) {
                        result = true;
                    }
                }
            }
        } catch (Exception e) {
            log.warn("canSignExecManagerInfo ERROR", e);
        }
        return result;
    }

    public List<ExecManagerInfoVO> getExecManagerInfoVOs(String wc_SID, User loginUser) {
        List<ExecManagerInfoVO> result = Lists.newArrayList();
        try {
            List<WCExceDep> wcExceDeps = wcExecDepManager.findActiveByWcSid(wc_SID);
            List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoManager.getGroupManagerSignInfo(wc_SID);
            groupManagerSignInfos.forEach(
                    item -> {
                        int groupSeq = item.getGroupSeq();
                        String depName = "";
                        List<WCExceDep> selWCExceDep = wcExceDeps.stream()
                                .filter(
                                        each -> each.getExecManagerSignInfoGroupSeq() != null
                                                && each.getExecManagerSignInfoGroupSeq().equals(groupSeq))
                                .collect(Collectors.toList());
                        if (selWCExceDep != null && !selWCExceDep.isEmpty()) {
                            Org selOrg = orgManager.findBySid(selWCExceDep.get(0).getDep_sid());
                            if (selOrg != null) {
                                depName = selOrg.getName();
                            }
                        }
                        // 執行簽核節點屬於加簽性質,僅會有一個節點
                        for (SingleManagerSignInfo subItem : item.getSingleManagerSignInfos()) {
                            List<ProcessTaskBase> pbs = BpmManager.getInstance()
                                    .findSimulationChart(loginUser.getId(), subItem.getInstanceID());
                            if (pbs != null && !pbs.isEmpty()) {
                                FlowNodeVO flowNodeVO = bpmLogicComponent.transToFlowNodeVO(
                                        pbs.get(0),
                                        subItem.getDefaultSignName(),
                                        subItem.getInstanceID(),
                                        loginUser);
                                String addPersonName = "系統產生";
                                if (subItem.getCereateUserSid() != null) {
                                    User createUser = userManager.findBySid(
                                            subItem.getCereateUserSid());
                                    addPersonName = (createUser != null) ? createUser.getName() : "";
                                }
                                ExecManagerInfoVO evo = new ExecManagerInfoVO(
                                        depName,
                                        flowNodeVO.getSignName(),
                                        flowNodeVO.getRoleName(),
                                        flowNodeVO.getSignTime(),
                                        addPersonName,
                                        String.valueOf(subItem.getUserSid()));
                                result.add(evo);
                            }
                        }
                    });
        } catch (Exception e) {
            log.warn("getExecManagerInfoVOs ERROR", e);
        }
        return result;
    }

    /**
     * 取得該使用者目前可使用節點物件
     */
    public List<SingleManagerSignInfo> getSingleManagerSignInfo(
            String wcSid, User user, SignType signType) {
        List<SingleManagerSignInfo> result = Lists.newArrayList();
        try {
            List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoManager.getGroupManagerSignInfo(wcSid);
            for (GroupManagerSignInfo gi : groupManagerSignInfos) {
                for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                    List<String> ids = BpmManager.getInstance()
                            .findTaskCanSignUserIds(si.getInstanceID());
                    if (signType != null && !signType.equals(si.getSignType())) {
                        continue;
                    }
                    if (ids.contains(user.getId())) {
                        result.add(si);
                    }
                }
            }
        } catch (Exception e) {
            log.warn("getSingleManagerSignInfo ERROR", e);
        }
        return result;
    }


}
