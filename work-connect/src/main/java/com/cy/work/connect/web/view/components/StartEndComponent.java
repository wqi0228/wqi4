/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Marlow_Chen
 */
public class StartEndComponent {

    @Getter
    @Setter
    private String sid;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private Date selectStartDate;

    @Getter
    @Setter
    private Date selectEndDate;

    @Getter
    @Setter
    private String limitDate;

    @Getter
    @Setter
    private boolean canEdit;

    @Getter
    @Setter
    private String endRulDays;

    public StartEndComponent(
            String title, String limitDate, String sid, String endRulDays) {
        this.title = title;
        this.limitDate = limitDate;
        this.sid = sid;
        this.endRulDays = endRulDays;
    }

    @Override
    public String toString() {
        return "{\"sid\" : \""
                + sid
                + "\", \"startdate\" : \""
                + covertDate(selectStartDate)
                + "\" , \"enddate\" : \""
                + covertDate(selectEndDate)
                + "\"}";
    }

    public String covertDate(Date selectDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String dateString = sdf.format(selectDate);

        return dateString;
    }
}
