/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WaitReceiveLogic;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.WaitWorkReceiveListColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.WaitWorkReceiveListColumnVO;
import com.cy.work.connect.web.view.vo.search.WaitWorkReceiveListVO;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 待領單據清單 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class WaitWorkReceiveListLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4601656898632317025L;

    private static WaitWorkReceiveListLogicComponent instance;
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.WAIT_RECEIVE_WORK_LIST;
    private final transient Comparator<WaitWorkReceiveListVO> waitWorkReceiveListVOComparator = new Comparator<WaitWorkReceiveListVO>() {
        @Override
        public int compare(WaitWorkReceiveListVO obj1, WaitWorkReceiveListVO obj2) {
            final Date seq1 = obj1.getCreateDate();
            final Date seq2 = obj2.getCreateDate();
            return seq1.compareTo(seq2);
        }
    };
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;

    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WaitReceiveLogic waitReceiveLogic;

    public static WaitWorkReceiveListLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WaitWorkReceiveListLogicComponent.instance = this;
    }

    private Map<String, WaitWorkReceiveListVO> settingWaitReceviceList(
            Integer loginUserSid, String targetWcSid) {

        // ====================================
        // 兜組 查詢 SQL
        // ====================================

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT wc.wc_sid, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.require_establish_dt, ");
        sql.append("       wc.dep_sid, ");
        sql.append("       wc.create_dt, ");
        sql.append("       wc.create_usr, ");
        sql.append("       wc.menuTag_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       execDep.dep_sid AS execDepSid, ");
        sql.append("       execDep.wc_exec_dep_setting_sid ");
        // 取得核心 SQL
        sql.append(this.waitReceiveLogic.prepareCoreSql(loginUserSid));
        // 有指定 wcSid 時
        if (WkStringUtils.notEmpty(targetWcSid)) {
            sql.append("       AND wc.wc_sid = '" + targetWcSid + "' ");
        }

        if (WorkParamManager.getInstance().isShowSearchSql()) {            
            log.debug("待領單據查詢"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> dbRowDataMaps = jdbc.queryForList(sql.toString());
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "待領單據查詢，共[" + dbRowDataMaps.size() + "]筆"));
        if (WkStringUtils.isEmpty(dbRowDataMaps)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 解析資料
        // ====================================
        Map<String, WaitWorkReceiveListVO> results = Maps.newHashMap();

        for (Map<String, Object> dbRowMap : dbRowDataMaps) {
            this.settingObject(results, dbRowMap);
        }

        return results;
    }

    private void settingObject(Map<String, WaitWorkReceiveListVO> results,
            Map<String, Object> each) {
        try {
            String wcSid = (String) each.get("wc_sid");
            WaitWorkReceiveListVO obj = null;
            if (!results.containsKey(wcSid)) {
                obj = new WaitWorkReceiveListVO(wcSid);
                obj.setWcNo((String) each.get("wc_no"));
                obj.setCreateDate((Date) each.get("create_dt"));
                obj.setRequireEstablishDate(new DateTime(each.get("require_establish_dt")).toString("yyyy/MM/dd"));
                obj.setRequireDepName(WkOrgUtils.findNameBySid((Integer) each.get("dep_sid")));
                obj.setRequireUserName(WkUserUtils.findNameBySid((Integer) each.get("create_usr")));
                obj.setMenuTagName(menuTagLogicComponent.findToBySid((String) each.get("menuTag_sid")).getName());
                obj.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
                results.put(wcSid, obj);
            } else {
                obj = results.get(wcSid);
            }
            String wc_exec_dep_setting_sid = (String) each.get("wc_exec_dep_setting_sid");
            if (!Strings.isNullOrEmpty(wc_exec_dep_setting_sid)) {
                WCExecDepSetting execDepSettingObj = execDepSettingManager.findBySidFromCache(wc_exec_dep_setting_sid);
                if (execDepSettingObj != null) {
                    WCTag wcTag = wcTagManager.getWCTagBySid(execDepSettingObj.getWc_tag_sid());
                    obj.bulidExecTag(wcTag);
                }
            }
            results.put(wcSid, obj);
        } catch (Exception e) {
            log.warn("settingObject ERROR", e);
        }
    }

    /**
     * 取得 待領單據清單 查詢物件List
     *
     * @param depSids
     * @param sid     工作聯絡單Sid
     * @return
     */
    public List<WaitWorkReceiveListVO> search(Integer loginUserSid, String sid) {
        Map<String, WaitWorkReceiveListVO> results = Maps.newHashMap();
        try {
            results = this.settingWaitReceviceList(loginUserSid, sid);
        } catch (Exception e) {
            log.warn("search ERROR", e);
        }
        List<WaitWorkReceiveListVO> res = results.entrySet().stream()
                .map(each -> each.getValue())
                .collect(Collectors.toList());
        try {
            Collections.sort(res, waitWorkReceiveListVOComparator);
        } catch (Exception e) {
            log.warn("search ERROR", e);
        }
        return res;
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public WaitWorkReceiveListColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        WaitWorkReceiveListColumnVO vo = new WaitWorkReceiveListColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkReceiveListColumn.INDEX));
            vo.setRequireEstablishDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkReceiveListColumn.REQUIRE_ESTABLISH_DATE));
            vo.setRequireDepName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkReceiveListColumn.REQUIRE_DEP_NAME));
            vo.setRequireUserName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkReceiveListColumn.REQUIRE_USER_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkReceiveListColumn.MENUTAG_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkReceiveListColumn.THEME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkReceiveListColumn.WC_NO));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkReceiveListColumn.INDEX, columDB.getInfo()));
            vo.setRequireEstablishDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkReceiveListColumn.REQUIRE_ESTABLISH_DATE, columDB.getInfo()));
            vo.setRequireDepName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkReceiveListColumn.REQUIRE_DEP_NAME, columDB.getInfo()));
            vo.setRequireUserName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkReceiveListColumn.REQUIRE_USER_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkReceiveListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkReceiveListColumn.THEME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkReceiveListColumn.WC_NO, columDB.getInfo()));
        }
        return vo;
    }
}
