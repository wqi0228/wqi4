/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.logic.components.WCExecDepSettingLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCUserLogic;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.components.WorkReportSearchHeaderComponent;
import com.cy.work.connect.web.view.vo.ManagerExecDepSettingVO;
import com.cy.work.connect.web.view.vo.ManagerMenuVO;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 類別執行單位維護作業 MBean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
public class Setting6Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2755580112652347694L;

    @Getter
    private final String dataTableID = "dataTableTagExecDep";
    @Getter
    private final String dataTableWv = "dataTableTagExecDepWv";
    @Getter
    private WorkReportSearchHeaderComponent workReportSearchHeaderComponent;
    @Autowired
    private WCMenuTagLogicComponent wcMenuTagLogicComponent;
    @Autowired
    private WkOrgCache orgManager;
    /**
     * view 顯示錯誤訊息
     */
    @Getter
    private String errorMessage;

    private final MessageCallBack messageCallBack =
        new MessageCallBack() {
            /** */
            private static final long serialVersionUID = -5029212323261213884L;

            @Override
            public void showMessage(String m) {
                errorMessage = m;
                DisplayController.getInstance().update("confimDlgTemplate");
                DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
            }
        };
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Setter
    @Getter
    private List<ManagerMenuVO> managerMenuVO;
    @Setter
    @Getter
    private ManagerMenuVO selManagerMenuVO;
    @Setter
    @Getter
    private List<ManagerExecDepSettingVO> managerExecDepSettingVOs;
    @Setter
    @Getter
    private ManagerExecDepSettingVO selManagerExecDepSettingVO;
    @Autowired
    private WCExecDepSettingLogicComponent execDepSettingLogicComponent;
    @Getter
    private String selMenuName;
    @Getter
    @Setter
    private String execDepManagerReceviceDataFilterName;
    @Getter
    @Setter
    private DualListModel<UserViewVO> managerReceviceDataPickList;

    @PostConstruct
    public void init() {
        userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        
        selManagerExecDepSettingVO = null;
        managerExecDepSettingVOs = Lists.newArrayList();
        this.managerReceviceDataPickList = new DualListModel<>();
        this.doSearch();
    }

    public void loadExecDepSetting() {
        try {
            loadManagerExecDepSettingVO();
            selManagerExecDepSettingVO = null;
            selMenuName =
                selManagerMenuVO.getCategoryName()
                    + "-"
                    + selManagerMenuVO.getMenuName()
                    + selManagerMenuVO.getMenuMemo();
            DisplayController.getInstance().update("menu_list_header_id");
            DisplayController.getInstance().update("menu_list_datatable_id");
            DisplayController.getInstance().showPfWidgetVar("menu_list_dlg_wv");
        } catch (Exception e) {
            log.warn("loadExecDepSetting ERROR", e);
            this.messageCallBack.showMessage(e.getMessage());
        }
    }

    private void loadManagerExecDepSettingVO() {
        managerExecDepSettingVOs =
            execDepSettingLogicComponent.getManagerExecDepSettingVOs(
                selManagerMenuVO.getMenuSid(), userViewVO.getSid());
    }

    public void saveReceviceSetting() {
        try {
            List<Integer> receviceUserSids = Lists.newArrayList();
            this.managerReceviceDataPickList
                .getTarget()
                .forEach(
                    item -> {
                        receviceUserSids.add(item.getSid());
                    });
            execDepSettingLogicComponent.saveExecDepSetting(
                selManagerExecDepSettingVO.getExecDepSettingSid(), userViewVO.getSid(),
                receviceUserSids);
            loadManagerExecDepSettingVO();
            DisplayController.getInstance().update("menu_list_datatable_id");
            DisplayController.getInstance().hidePfWidgetVar("execDep_data_dlg_wv");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("loadReceviceSetting ERROR：{}", e.getMessage());
            this.messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.error("loadReceviceSetting ERROR", e);
            this.messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadReceviceSetting() {
        try {
            WCExecDepSetting execDepSetting =
                execDepSettingLogicComponent.getWCExecDepSettingBySid(
                    selManagerExecDepSettingVO.getExecDepSettingSid());
            Preconditions.checkState(!execDepSetting.isNeedAssigned(), "派工不可調整領單人員名單");
            Org execDep = orgManager.findBySid(execDepSetting.getExec_dep_sid());
            List<Org> orgs = Lists.newArrayList(execDep);
            orgs.addAll(
                orgManager.findAllChild(execDep.getSid()).stream()
                    .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                    .collect(Collectors.toList()));
            LinkedHashSet<UserViewVO> canReceviceUsers = Sets.newLinkedHashSet();
            orgs.forEach(
                item -> {
                    canReceviceUsers.addAll(
                        WCUserLogic.getInstance().findByDepSid(item.getSid()));
                });
            List<UserViewVO> receviceData = Lists.newArrayList();
            if (execDepSetting.getReceviceUser() != null
                && execDepSetting.getReceviceUser().getUserTos() != null) {
                execDepSetting
                    .getReceviceUser()
                    .getUserTos()
                    .forEach(
                        item -> {
                            receviceData.add(
                                    new UserViewVO(item.getSid()));
                        });
            }
            managerReceviceDataPickList.setTarget(receviceData);
            managerReceviceDataPickList.setSource(
                removeTargetUserView(
                    Lists.newArrayList(canReceviceUsers),
                    this.managerReceviceDataPickList.getTarget()));
            DisplayController.getInstance().update("execDep_data_dlg_panel_id");
            DisplayController.getInstance().showPfWidgetVar("execDep_data_dlg_wv");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("loadReceviceSetting ERROR：{}", e.getMessage());
            this.messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("loadReceviceSetting ERROR", e);
            this.messageCallBack.showMessage(e.getMessage());
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source,
        List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
            item -> {
                if (!target.contains(item)) {
                    filterSource.add(item);
                }
            });
        return filterSource;
    }

    public void doSearch() {
        try {
            managerMenuVO =
                wcMenuTagLogicComponent.getManagerMenuTags(userViewVO.getSid(),
                    compViewVo.getSid());
            selManagerMenuVO = null;
        } catch (Exception e) {
            log.warn("doSearch ERROR", e);
            this.messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clear() {
    }
}
