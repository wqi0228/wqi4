/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.monitor;

import com.cy.work.connect.logic.schedule.FlowDataSchedule;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.web.logic.components.WCMasterLogicComponents;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

/**
 * 清除簽核cache
 *
 * @author kasim
 */
@Controller
@Scope(WebApplicationContext.SCOPE_REQUEST)
@Slf4j
public class ClearProcessCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3603392387023411386L;

    @Autowired
    private FlowDataSchedule flowDataSchedule;
    @Autowired
    private WCMasterLogicComponents wcMasterLogicComponents;

    /**
     * 清除 Process Cache
     */
    public void clearCache() {
        log.debug("進行Process Cache清除");
        try {
            if (Faces.getRequestParameterMap().containsKey("processSid")) {
                String pSid = Faces.getRequestParameterMap().get("processSid");
                log.debug("processSid=>" + pSid);
                WCMaster wc = wcMasterLogicComponents.getWCMasterBySid(pSid);
                if (wc == null) {
                    log.warn("清除Cache,無資料");
                } else {
                    flowDataSchedule.updateFlowCache(wc);
                    log.debug("工作聯絡單單號:" + wc.getWc_no() + " FormSignCache 更新");
                }
            }
        } catch (Exception e) {
            log.warn("clearCache ERROR", e);
        }
        log.debug("完成Process Cache清除");
    }
}
