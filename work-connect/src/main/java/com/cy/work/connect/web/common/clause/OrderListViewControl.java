package com.cy.work.connect.web.common.clause;

import lombok.Getter;
import lombok.Setter;

public class OrderListViewControl {

    @Getter
    private final String url = "../worp/worp_iframe.xhtml?wrcId=";
    @Getter
    private final String dataTableID = "dataTableWorkReport";

    @Getter
    @Setter
    /** 是否版面切換(顯示Frame畫面) */
    private boolean showFrame = false;

    @Getter
    @Setter
    /** view 顯示錯誤訊息 */
    private String errorMsg;
    /**
     * 顯示dataTable裡的元件
     */
    @Getter
    @Setter
    private boolean displayButton = true;

    @Getter
    @Setter
    private String iframeUrl;

    @Getter
    @Setter
    /** 列表高度 */
    private String dtScrollHeight = "400";
}
