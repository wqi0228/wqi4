/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.vo.enums.WCTraceType;
import com.cy.work.connect.web.logic.components.WCTraceLogicComponents;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * 指示 Component
 *
 * @author kasim
 */
public class WorkReportDirectiveComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5530003974909131812L;

    @Getter
    private List<WCTraceVO> wcTraceVO;

    private String wrcId;

    private Integer userSid;
    @Getter
    private boolean showTab = false;

    public WorkReportDirectiveComponent() {
    }

    public void loadData(String wrcId, Integer userSid) {
        this.wrcId = wrcId;
        this.userSid = userSid;
        loadViewData(wrcId, userSid);
    }

    public void loadData() {
        loadViewData(wrcId, userSid);
    }

    private void loadViewData(String wrcId, Integer userSid) {
        wcTraceVO =
            WCTraceLogicComponents.getInstance()
                .getWCTraceVOsByWcSIDAndWCTraceType(userSid, wrcId, WCTraceType.DIRECTIVE);
        showTab = wcTraceVO != null && wcTraceVO.size() > 0;
    }
}
