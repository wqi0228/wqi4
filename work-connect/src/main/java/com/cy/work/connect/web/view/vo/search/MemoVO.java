/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search;

import com.cy.work.connect.vo.enums.WCReadStatus;
import com.cy.work.connect.web.logic.components.search.MemoSearchLogicComponent;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 備忘錄查詢 - 列表資料
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(of = {"sid"})
public class MemoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3066448342882341156L;

    /**
     * key - sid
     */
    private String sid;

    /**
     * 單號
     */
    private String memoNo;

    /**
     * 建立日期
     */
    private String createDate;

    private Integer createDepSid;

    /**
     * 建立單位
     */
    private String createDep;

    /**
     * 主題
     */
    private String theme;
    /**
     * 是否已轉過聯絡單
     */
    private String transWcStatus;

    private String readStatus;

    private String readStatusCss;

    private String readStatusBlodCss;

    public MemoVO(String sid) {
        this.sid = sid;
    }

    public void replaceValue(MemoVO updateObject) {
        this.sid = updateObject.getSid();
        this.memoNo = updateObject.getMemoNo();
        this.createDepSid = updateObject.getCreateDepSid();
        this.createDate = updateObject.getCreateDate();
        this.createDep = updateObject.getCreateDep();
        this.theme = updateObject.getTheme();
        this.transWcStatus = updateObject.getTransWcStatus();
        this.readStatus = updateObject.getReadStatus();
        this.readStatusCss = updateObject.getReadStatusCss();
        this.readStatusBlodCss = updateObject.getReadStatusBlodCss();
    }

    public void replaceReadStatus(WCReadStatus wcReadStatus) {
        if (wcReadStatus != null) {
            this.readStatus = wcReadStatus.getVal();
            this.readStatusCss = MemoSearchLogicComponent.getInstance()
                .transToColorCss(wcReadStatus);
            this.readStatusBlodCss = MemoSearchLogicComponent.getInstance()
                .transToBlodCss(wcReadStatus);
        } else {
            this.readStatus = "";
            this.readStatusCss = "";
            this.readStatusBlodCss = "";
        }
    }
}
