/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.column.search;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Data;

/**
 * 處理清單 - dataTable 欄位資訊
 *
 * @author kasim
 */
@Data
public class ProcessListColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7289327021520853618L;
    /**
     * 一頁 X 筆
     */
    private String pageCount = "50";
    /**
     * 序
     */
    private ColumnDetailVO index;
    /**
     * 建立日期
     */
    private ColumnDetailVO createDate;
    /**
     * 類別(第一層)
     */
    private ColumnDetailVO categoryName;
    /**
     * 單據名稱(第二層)
     */
    private ColumnDetailVO menuTagName;
    /**
     * 執行人員
     */
    private ColumnDetailVO execUserName;
    /**
     * 主題
     */
    private ColumnDetailVO theme;
    /**
     * 單號
     */
    private ColumnDetailVO wcNo;
    /**
     * 需求單位
     */
    private ColumnDetailVO reqDep;
}
