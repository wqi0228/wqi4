/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WCFuncGroupBaseType;
import com.cy.work.connect.web.logic.components.WCHomeLogicComponents;
import com.cy.work.connect.web.logic.components.WCHomepageFavoriteLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WCHomepageFavoriteVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 報表快選區設定 MBean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
public class Setting1Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4703183043671084716L;

    @Getter
    private String errorMessage;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;

    @Getter
    /** 報表選項 */
    private List<SelectItem> reportItems;

    private List<WCHomepageFavoriteVO> reports;
    /**
     * 選取的報表快選
     */
    @Setter
    @Getter
    private List<String> selectReportTypes = Lists.newArrayList();

    @Autowired
    private WCHomeLogicComponents wcHomeLogicComponents;
    @Autowired
    private WCHomepageFavoriteLogicComponents wcHomepageFavoriteLogicComponents;

    @PostConstruct
    public void init() {
        userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        initItmes();
        initDefault();
    }

    private void initItmes() {
        reportItems = Lists.newArrayList();
        reports = Lists.newArrayList();
        List<WorkFunItemGroupVO> workFunItemGroups =
            wcHomeLogicComponents.getWorkFunItemGroupByTypeAndUserPermission(
                WCFuncGroupBaseType.SEARCH.getKey(), userViewVO.getSid(), depViewVo.getSid());
        try {

            workFunItemGroups
                .get(0)
                .getWorkFunItemVOs()
                .forEach(
                    item -> {
                        reports.add(
                            new WCHomepageFavoriteVO(item.getUrl(), item.getFunction_title()));
                        SelectItem si = new SelectItem(item.getUrl(), item.getFunction_title());
                        reportItems.add(si);
                    });
        } catch (Exception e) {
            log.warn("initItmes", e);
        }
    }

    private void initDefault() {
        selectReportTypes = Lists.newArrayList();

        try {
            wcHomepageFavoriteLogicComponents
                .getWCHomepageFavoriteVOByUserSid(userViewVO.getSid())
                .forEach(
                    item -> {
                        selectReportTypes.add(item.getPagePath());
                    });
        } catch (Exception e) {
            log.warn("initDefault", e);
        }
    }

    public void saveSetting() {
        try {
            List<WCHomepageFavoriteVO> wcHomepageFavoriteVO = Lists.newArrayList();
            selectReportTypes.forEach(
                item -> {
                    List<WCHomepageFavoriteVO> sel =
                        reports.stream()
                            .filter(each -> each.getPagePath().equals(item))
                            .collect(Collectors.toList());
                    wcHomepageFavoriteVO.add(sel.get(0));
                });
            wcHomepageFavoriteLogicComponents.saveWCHomepageFavorite(
                wcHomepageFavoriteVO, userViewVO.getSid());
        } catch (Exception e) {
            log.warn("saveSetting", e);
            errorMessage = e.getMessage();
            DisplayController.getInstance().update("confimDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
        }
    }
}
