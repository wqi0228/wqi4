/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.condition;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class ExecBtnCondition implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2818051286214267620L;

    @Setter
    @Getter
    public boolean isExecDep;
    @Setter
    @Getter
    public boolean isExecDepDoing;
}
