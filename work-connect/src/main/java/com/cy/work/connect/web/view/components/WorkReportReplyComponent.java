/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.vo.enums.WCTraceType;
import com.cy.work.connect.web.logic.components.WCTraceLogicComponents;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class WorkReportReplyComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3138815873168083350L;

    @Getter
    private List<WCTraceVO> wcTraceVO;

    private String wcSid;

    private Integer userSid;
    @Getter
    private boolean showReplyTab = false;

    public WorkReportReplyComponent() {
    }

    public void loadData(String wcSid, Integer userSid) {
        this.wcSid = wcSid;
        this.userSid = userSid;
        loadViewData(wcSid, userSid);
    }

    public void loadData() {
        loadViewData(wcSid, userSid);
    }

    private void loadViewData(String wcSid, Integer userSid) {
        wcTraceVO =
            WCTraceLogicComponents.getInstance()
                .getWCTraceVOsByWcSIDAndWCTraceType(userSid, wcSid, WCTraceType.REPLY);
        showReplyTab = wcTraceVO != null && !wcTraceVO.isEmpty();
    }
}
