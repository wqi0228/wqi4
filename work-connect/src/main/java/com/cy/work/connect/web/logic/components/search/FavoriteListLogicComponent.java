/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.FavoriteSearchColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.view.vo.FavoriteSearchVO;
import com.cy.work.connect.web.view.vo.column.search.FavoriteSearchColumnVO;
import com.cy.work.connect.web.view.vo.search.query.CheckedListQuery;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 處理清單 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class FavoriteListLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1479477154944990377L;

    private static FavoriteListLogicComponent instance;
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.FAVORITE_LIST;
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;

    public static FavoriteListLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        FavoriteListLogicComponent.instance = this;
    }

    private FavoriteSearchVO settingObject(Map<String, Object> rowDataMap) {
        FavoriteSearchVO favoriteSearchVO = new FavoriteSearchVO((String) rowDataMap.get("favorite_sid"));
        favoriteSearchVO.setWcSid((String) rowDataMap.get("wc_sid"));
        favoriteSearchVO.setWcNo((String) rowDataMap.get("wc_no"));
        if (rowDataMap.get("category_tag") != null) {
            try {
                String categoryTagStr = (String) rowDataMap.get("category_tag");
                CategoryTagTo ct = WkJsonUtils.getInstance().fromJson(categoryTagStr, CategoryTagTo.class);
                ct.getTagSids()
                        .forEach(
                                item -> {
                                    favoriteSearchVO.bulidExecTag(wcTagManager.getWCTagBySid(item));
                                });
            } catch (Exception e) {
                log.warn("settingObject ERROR", e);
            }
        }
        favoriteSearchVO.setFavoriteDate(
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateTime.getValue(),
                        (Date) rowDataMap.get("favorite_update_dt")));
        favoriteSearchVO.setFavoritedt((Date) rowDataMap.get("favorite_update_dt"));
        favoriteSearchVO.setTheme(WkStringUtils.removeCtrlChr((String) rowDataMap.get("theme")));
        favoriteSearchVO.setContent(WkStringUtils.removeCtrlChr((String) rowDataMap.get("content")));

        return favoriteSearchVO;

    }

    

    /**
     * 取得 處理清單 查詢物件List
     *
     * @param query
     * @param depSids
     * @param sid     工作聯絡單Sid
     * @return
     * @throws UserMessageException 
     */
    public List<FavoriteSearchVO> search(CheckedListQuery query, Integer loginUserSid, String wcSid) throws UserMessageException {

        // ====================================
        // 兜組SQL
        // ====================================
        String sql = this.bulidSqlByFavoriteList(query, loginUserSid, wcSid);

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("收藏夾查詢"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> dbRowDataMaps = Lists.newArrayList();

        try {
            dbRowDataMaps = jdbc.queryForList(sql);
        } catch (Exception e) {
            String message = "收藏夾查詢失敗！" + e.getMessage();
            log.error(message, e);
            log.error("\r\nSQL:【\r\n"
                    + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
            throw new  UserMessageException(message, InfomationLevel.ERROR);
        }

        
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "收藏夾查詢，共[" + dbRowDataMaps.size() + "]筆"));
        if (WkStringUtils.isEmpty(dbRowDataMaps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 解析資料
        // ====================================
        String skypeAlertMessage = "";
        List<FavoriteSearchVO> resultVOs = Lists.newArrayList();
        for (Map<String, Object> dbRowDataMap : dbRowDataMaps) {
            try {
                FavoriteSearchVO vo = this.settingObject(dbRowDataMap);
                resultVOs.add(vo);
            } catch (Exception e) {
                String message = "收藏夾查詢。資料解析失敗!" + e.getMessage();
                log.error(message, e);
                log.error("row data:\r\n" + WkJsonUtils.getInstance().toPettyJson(dbRowDataMap));
                skypeAlertMessage = message;
            }
        }

        if (WkStringUtils.notEmpty(skypeAlertMessage)) {
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(skypeAlertMessage);
        }

        // ====================================
        // 排序：收藏日期
        // ====================================
        return resultVOs.stream()
                .sorted(Comparator.comparing(FavoriteSearchVO::getFavoritedt).reversed())
                .collect(Collectors.toList());

    }

    /**
     * 建立主要 Sql(收藏)
     *
     * @param query
     * @param depSids
     * @return
     */
    private String bulidSqlByFavoriteList(
            CheckedListQuery query, Integer loginUserSid, String wcSid) {

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        String searchKeyword = WkStringUtils.safeTrim(query.getLazyContent());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 為指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(wcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 組SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT wf.favorite_sid, ");
        sql.append("       wc.wc_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       wf.update_dt AS favorite_update_dt, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.content, ");
        sql.append("       wc.category_tag ");
        sql.append("FROM   wc_favorite wf ");
        sql.append("       INNER JOIN wc_master wc ");
        sql.append("               ON wc.wc_sid = wf.wc_sid ");
        sql.append("WHERE  1=1 ");

        // ----------------------------
        // 限制條件
        // ----------------------------
        sql.append("       AND wf.status = 0 ");
        sql.append("       AND wf.create_usr = '" + loginUserSid + "' ");

        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(wcSid)) {
                sql.append("  AND wc.wc_sid = '" + wcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND wc.wc_no = '" + forceWcNo + "' ");
            }
        }

        // ----------------------------
        // 收藏區間
        // ----------------------------
        // 起日
        if (!isForceSearch
                && query.getStartDate() != null) {
            sql.append("AND wf.update_dt >= '")
                    .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        }
        // 迄日
        if (!isForceSearch
                && query.getEndDate() != null) {
            sql.append("AND wf.update_dt <= '")
                    .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }

        // ----------------------------
        // 模糊查詢
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(searchKeyword)) {
            String searchText = WkStringUtils.safeTrim(searchKeyword).toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "wc.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));

            sql.append("      ) ");
        }

        sql.append("ORDER  BY wf.update_dt DESC");

        return sql.toString();
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public FavoriteSearchColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        FavoriteSearchColumnVO vo = new FavoriteSearchColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(FavoriteSearchColumn.INDEX));
            vo.setFavoriteDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            FavoriteSearchColumn.FAVORITE_TIME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(FavoriteSearchColumn.THEME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(FavoriteSearchColumn.WC_NO));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            FavoriteSearchColumn.INDEX, columDB.getInfo()));
            vo.setFavoriteDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            FavoriteSearchColumn.FAVORITE_TIME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            FavoriteSearchColumn.THEME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            FavoriteSearchColumn.WC_NO, columDB.getInfo()));
        }
        return vo;
    }
}
