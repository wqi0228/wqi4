package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.helper.CheckLegalDateHelper;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.WCMemoTraceHelper;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMasterCustomLogicManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCMemoMasterManager;
import com.cy.work.connect.logic.manager.WCMemoTraceManager;
import com.cy.work.connect.logic.manager.WCReadReceiptsManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WCTraceCustomLogicManager;
import com.cy.work.connect.logic.manager.WCTraceManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCMemoMaster;
import com.cy.work.connect.vo.WCMemoTrace;
import com.cy.work.connect.vo.WCReadReceipts;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.web.common.CaseNoGenericService;
import com.cy.work.connect.web.view.components.CategoryItemComponent;
import com.cy.work.connect.web.view.components.ExecDescComponent;
import com.cy.work.connect.web.view.components.LegitimateRangeComponent;
import com.cy.work.connect.web.view.components.StartEndComponent;
import com.cy.work.connect.web.view.components.WorkReportComponent;
import com.cy.work.connect.web.view.components.WorkReportDataComponent;
import com.cy.work.connect.web.view.components.WorkReportHeaderComponent;
import com.cy.work.connect.web.view.vo.ExecInfoVO;
import com.cy.work.connect.web.view.vo.SignRole;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.ViewPersonVO;
import com.cy.work.connect.web.view.vo.WCAgainTraceVO;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

/**
 * 工作聯絡單邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCMasterLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4792619611011830249L;
    
    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WCMasterLogicComponents instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static WCMasterLogicComponents getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WCMasterLogicComponents instance) { WCMasterLogicComponents.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WCConfigTempletManager wcConfigTempletManager;
    @Autowired
    private WkOrgCache wkOrgCache;
    @Autowired
    private WkUserCache wkUserCache;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;
    @Autowired
    private WCExceDepLogicComponent wcExceDepLogicComponent;
    @Autowired
    private WCTagLogicComponents wcTagLogicComponents;
    @Autowired
    private WCExecDepSettingLogicComponent execDepSettingLogicComponent;
    @Autowired
    private WCTraceCustomLogicManager wcTraceCustomLogicManager;
    @Autowired
    private WCTraceManager wcTraceManager;
    @Autowired
    private WCManagerSignInfoManager managerSignInfoManager;
    @Autowired
    private WCExecManagerSignInfoManager execManagerSignInfoManager;
    @Autowired
    private WCReadReceiptsManager readReceiptsManager;
    @Autowired
    private BPMLogicComponent bpmLogicComponent;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private CheckLegalDateHelper checkLegalDateHelper;
    @Autowired
    private WCMasterHelper wcMasterHelper;

    /**
     * 建立工作聯絡單
     *
     * @param workReportDataComponent   工作聯絡單資料物件
     * @param workReportHeaderComponent 工作聯絡單Header物件
     * @param attachmentVOs             附件介面物件List
     * @param categoryItemComponent     類別元件
     * @param userSid                   建立者Sid
     * @param departmentSid             建立者部門Sid
     * @param companySid                建立者公司Sid
     * @param reqUserViews              加簽人員
     * @param wcMemoSid                 備忘錄Sid
     * @return
     * @throws UserMessageException 錯誤時拋出
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCMaster createMaster(
            WorkReportDataComponent workReportDataComponent,
            WorkReportHeaderComponent workReportHeaderComponent,
            List<AttachmentVO> attachmentVOs,
            CategoryItemComponent categoryItemComponent,
            Integer userSid,
            Integer departmentSid,
            Integer companySid,
            List<UserViewVO> reqUserViews,
            String wcMemoSid) throws UserMessageException {

        // ====================================
        // 準備工作聯絡單主檔資料 wc_master
        // ====================================
        WCMaster wcMaster = new WCMaster();
        // 產生聯絡單號
        wcMaster.setWc_no(CaseNoGenericService.generateWCNum(SecurityFacade.getCompanyId()));

        wcMaster.setComp_sid(companySid);
        wcMaster.setDep_sid(departmentSid);
        wcMaster.setWc_status(WCStatus.NEW_INSTANCE);
        wcMaster.setHas_attachment(WkStringUtils.notEmpty(attachmentVOs));
        wcMaster.setHas_trace(false); // 預設false
        wcMaster.setHas_reply(false); // 預設false
        wcMaster.setTheme(workReportDataComponent.getTitle());
        wcMaster.setContent(
                WkJsoupUtils.getInstance().clearCssTag(workReportDataComponent.getContent()));
        wcMaster.setContent_css(workReportDataComponent.getContent());
        wcMaster.setMemo(WkJsoupUtils.getInstance().clearCssTag(workReportDataComponent.getRemark()));
        wcMaster.setMemo_css(workReportDataComponent.getRemark());
        wcMaster.setMenuTagSid(categoryItemComponent.getMiddleCategorySid());
        wcMaster.setCategoryTagTo(new CategoryTagTo(categoryItemComponent.getSelCategoryItems()));
        wcMaster.setWcMemoSid(wcMemoSid);
        // 合法區間時間
        wcMaster.setLegitimateRangeDate(
                this.legitimateRangeConvertJson(categoryItemComponent.getLegitimateRangeComponents()));
        // 起訖日時間
        wcMaster.setStartEndDate(
                this.starEndConvertJson(categoryItemComponent.getStartEndComponents()));
        // 加簽人員SID
        List<Integer> addUserFlowSids = reqUserViews.stream()
                .map(UserViewVO::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 建立工作聯絡單
        // ====================================
        wcMaster = this.wcMasterCustomLogicManager.createMaster(wcMaster, userSid, attachmentVOs, addUserFlowSids);

        // ====================================
        // 根據挑選執行項目,取得預設可閱名單 (預設可閱部門、預設可閱人員)
        // ====================================
        String wcSid = wcMaster.getSid();
        List<WCReadReceipts> addReadReceipts = this.getDefaultViewPerson(
                categoryItemComponent.getSelCategoryItems(), companySid, wcSid, userSid);

        if (addReadReceipts != null && !addReadReceipts.isEmpty()) {
            WCReadReceiptsManager.getInstance().save(addReadReceipts);
        }

        // ====================================
        // 備忘錄
        // ====================================
        if (!Strings.isNullOrEmpty(wcMemoSid)) {
            try {
                WCMemoMasterManager.getInstance().updateCopyWcMasterSid(wcMaster.getSid(), wcMemoSid);
                WCMemoMaster wcMemoMaster = WCMemoMasterManager.getInstance().findBySid(wcMemoSid);
                WCTrace wcTrace = WCTraceCustomLogicManager.getInstance()
                        .getWCTrace_TRANS_FROM_MEMO(
                                wcMemoMaster.getWcMemoNo(),
                                wcMemoMaster.getTheme(),
                                wcMaster.getSid(),
                                wcMaster.getWc_no(),
                                userSid);
                WCTraceManager.getInstance().create(wcTrace, userSid);

                WCMemoTrace wcMemoTrace = WCMemoTraceHelper.getInstance()
                        .getWCMemoTrace_TRANSTOWORKCONNECT(
                                wcMaster.getSid(),
                                wcMaster.getWc_no(),
                                userSid,
                                wcMemoMaster.getSid(),
                                wcMemoMaster.getWcMemoNo());
                WCMemoTraceManager.getInstance().create(wcMemoTrace, userSid);
            } catch (Exception e) {
                log.warn("wcMemoMasterManager ERROR", e);
            }
        }
        if (!addUserFlowSids.isEmpty()) {
            this.createCounterSignTrace(
                    wcMaster.getSid(), wcMaster.getWc_no(), userSid, null, addUserFlowSids);
        }
        return wcMaster;
    }

    /**
     * 檢核大類(類別)是否已使用 By Sid
     *
     * @param categoryTagSid 大類(類別)Sid
     * @return
     */
    public boolean checkCategoryTagUsed(String categoryTagSid) {
        boolean result = false;
        try {
            Integer count = wcMasterManager.findByCategoryTag(categoryTagSid);
            if (count > 0) {
                result = true;
            }
        } catch (Exception ex) {
            log.warn("checkCategoryTagUsed ERROR", ex);
        }
        return result;
    }

    /**
     * 檢核中類(單據名稱)是否已使用 By Sid
     *
     * @param menuTagSid 中類(單據名稱)Sid
     * @return
     */
    public boolean checkMenuTagUsed(String menuTagSid) {
        boolean result = false;
        try {
            Integer count = wcMasterManager.findByMenuTag(menuTagSid);
            if (count > 0) {
                result = true;
            }
        } catch (Exception ex) {
            log.warn("checkMenuTagUsed ERROR", ex);
        }
        return result;
    }

    /**
     * 檢核小類(執行項目)是否已使用 By Sid
     *
     * @param tagSid 小類(執行項目)Sid
     * @return
     */
    public boolean checkTagUsed(String tagSid) {
        boolean result = false;
        try {
            Integer count = wcMasterManager.findByTag(tagSid);
            if (count > 0) {
                result = true;
            }
        } catch (Exception ex) {
            log.warn("checkTagUsed ERROR", ex);
        }
        return result;
    }

    /**
     * 儲存可閱名單
     *
     * @param wcSid        工作聯絡單Sid
     * @param viewUsers    挑選可閱人員List
     * @param loginUserSid 登入者Sid
     */
    public void saveViewUsers(String wcSid, List<UserViewVO> viewUsers, Integer loginUserSid) {
        WCMaster wcMaster = wcMasterManager.findBySid(wcSid);
        List<WCReadReceipts> rpts = readReceiptsManager.findByWcsid(wcSid);
        List<WCReadReceipts> oldReadReceiptsMemberTo = Lists.newArrayList();
        List<WCReadReceipts> saveReadReceiptsMemberTo = Lists.newArrayList();
        if (rpts != null && !rpts.isEmpty()) {
            oldReadReceiptsMemberTo.addAll(rpts);
        }
        StringBuilder moveErrorMessage = new StringBuilder();
        List<WCReadReceipts> selectReadReceiptsMemberTo = Lists.newArrayList();
        Date now = new Date();
        viewUsers.forEach(
                item -> {
                    WCReadReceipts readReceipts = new WCReadReceipts();
                    readReceipts.setWcsid(wcSid);
                    readReceipts.setReader(item.getSid());
                    readReceipts.setReadreceipt(WCReadReceiptStatus.UNREAD);
                    readReceipts.setRequiretime(now);
                    readReceipts.setCreateUser(loginUserSid);
                    selectReadReceiptsMemberTo.add(readReceipts);
                });
        List<WCReadReceipts> canNotRemoved = oldReadReceiptsMemberTo.stream()
                .filter(
                        each -> each != null
                                && each.getReadreceipt() != null
                                && each.getReadreceipt().equals(WCReadReceiptStatus.READ)
                                && !selectReadReceiptsMemberTo.stream()
                                        .anyMatch(item -> item.getReader().equals(each.getReader())))
                .collect(Collectors.toList());
        canNotRemoved.forEach(
                item -> {
                    if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                        moveErrorMessage.append("\n");
                    }
                    User movedUser = wkUserCache.findBySid(item.getReader());
                    moveErrorMessage.append(
                            "成員["
                                    + movedUser.getPrimaryOrg().getName()
                                    + "-"
                                    + movedUser.getName()
                                    + "]已閱讀,不可移除");
                });

        List<WCReadReceipts> canRemoved = oldReadReceiptsMemberTo.stream()
                .filter(
                        each -> !selectReadReceiptsMemberTo.stream()
                                .anyMatch(item -> item.getReader().equals(each.getReader())))
                .collect(Collectors.toList());
        List<User> removeUsers = Lists.newArrayList();
        canRemoved.forEach(
                item -> {
                    removeUsers.add(wkUserCache.findBySid(item.getReader()));
                });

        List<User> addUser = Lists.newArrayList();
        selectReadReceiptsMemberTo.forEach(
                item -> {
                    // 若資料庫已有記錄讀取記錄,不可變更,必須以資料庫為主
                    List<WCReadReceipts> sameReadReceiptsMemberTo = oldReadReceiptsMemberTo.stream()
                            .filter(each -> each.getReader().equals(item.getReader()))
                            .collect(Collectors.toList());
                    if (sameReadReceiptsMemberTo != null && sameReadReceiptsMemberTo.size() > 0) {
                        saveReadReceiptsMemberTo.add(sameReadReceiptsMemberTo.get(0));
                        // 若原本閱讀紀錄已擁有將自動將閱讀紀錄取代至要求簽名
                    } else {
                        saveReadReceiptsMemberTo.add(item);
                        addUser.add(wkUserCache.findBySid(item.getReader()));
                    }
                });
        wcMasterCustomLogicManager.saveViewPerson(wcMaster, loginUserSid, addUser, removeUsers);

        // 刪除可閱名單
        if (!removeUsers.isEmpty()) {
            List<Integer> removeUserSidList = removeUsers.stream().map(User::getSid).collect(Collectors.toList());
            List<WCReadReceipts> removeReadReceipts = readReceiptsManager.findReadReceiptsByWCSidAndReaderList(wcSid, removeUserSidList);
            readReceiptsManager.deleteInBatch(removeReadReceipts);
        }
        // 新增可閱名單
        if (!addUser.isEmpty()) {
            List<WCReadReceipts> addReadReceipts = Lists.newArrayList();
            addUser.forEach(
                    item -> {
                        WCReadReceipts readReceipts = new WCReadReceipts();
                        readReceipts.setWcsid(wcSid);
                        readReceipts.setReader(item.getSid());
                        readReceipts.setReadreceipt(WCReadReceiptStatus.UNREAD);
                        readReceipts.setRequiretime(now);
                        readReceipts.setCreateUser(loginUserSid);
                        addReadReceipts.add(readReceipts);
                    });
            readReceiptsManager.save(addReadReceipts);
        }
    }

    /**
     * 取得可閱物件清單 By 工作聯絡單Sid
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<ViewPersonVO> getViewPersonByWcSid(String wcSid) {
        List<WCReadReceipts> rpts = readReceiptsManager.findByWcsid(wcSid);
        if (rpts == null || rpts.isEmpty()) {
            return Lists.newArrayList();
        }

        List<ViewPersonVO> readReceiptVOs = Lists.newArrayList();
        for (WCReadReceipts item : rpts) {
            try {
                User user = wkUserCache.findBySid(item.getReader());
                if (user == null) {
                    continue;
                }

                String readStatus = "待閱讀";
                String readStatusCss = "color: red;";
                if (!WkStringUtils.isEmpty(item.getReadreceipt())) {
                    try {
                        if (WCReadReceiptStatus.READ.equals(item.getReadreceipt())) {
                            readStatus = "已閱讀";
                            readStatusCss = "";
                        }
                    } catch (Exception e) {
                        log.warn("getViewPersonByWcSid ERROR", e);
                    }
                }
                readReceiptVOs.add(
                        new ViewPersonVO(
                                user.getSid(),
                                user.getName(),
                                user.getPrimaryOrg().getName(),
                                readStatus,
                                readStatusCss));
            } catch (Exception e) {
                log.warn("getViewPersonByWcSid ERROR", e);
            }
        }
        return readReceiptVOs;
    }

    /**
     * 取得可閱人員物件清單 By 工作聯絡單Sid
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<UserViewVO> getViewUser(String wcSid) {
        List<UserViewVO> result = Lists.newArrayList();
        try {
            List<WCReadReceipts> rpts = readReceiptsManager.findByWcsid(wcSid);
            if (rpts != null && !rpts.isEmpty()) {
                rpts.forEach(
                        item -> {
                            result.add(new UserViewVO(item.getReader()));
                        });
            }
        } catch (Exception e) {
            log.warn("getViewUser ERROR", e);
        }
        return result;
    }

    /**
     * 根據挑選執行項目,塞入預設可閱名單
     *
     * @param tags         小類(執行項目)Sid清單
     * @param loginCompSid 登入公司Sid
     * @param wcSid        工作聯絡單Sid
     * @param loginUserSid 建立者Sid
     * @return
     */
    private List<WCReadReceipts> getDefaultViewPerson(
            List<String> tags, Integer loginCompSid, String wcSid, Integer loginUserSid) {

        // 所有預設可閱人員 物件
        List<WCReadReceipts> readReceiptsMember = Lists.newArrayList();
        try {

            // 所有預設可閱人員 sid
            List<Integer> defaultUserSids = Lists.newArrayList();

            // 依據 tag sid 取得執行項目檔
            wcTagLogicComponents
                    .findBySid(tags)
                    .forEach(
                            wcTag -> {

                                // ----------------------
                                // 處理 - 預設可閱部門
                                // ----------------------
                                if (wcTag.getUseDepTempletSid() != null) {

                                    // 當有設定模版時, 以模版設定為主模版設定
                                    List<Integer> conReadUsersSids = this.wcConfigTempletManager.getDefultCanReadUsers(
                                            wcTag.getUseDepTempletSid(), loginCompSid);

                                    // 加入列表
                                    if (WkStringUtils.notEmpty(conReadUsersSids)) {
                                        for (Integer userSid : conReadUsersSids) {
                                            if (!defaultUserSids.contains(userSid)) {
                                                defaultUserSids.add(userSid);
                                            }
                                        }
                                    }

                                } else if (wcTag.getUseDep() != null
                                        && wcTag.getUseDep().getUserDepTos() != null) {
                                    // 未設定模版 - 自行設定
                                    this.getViewDepPerson(
                                            wcTag.getUseDep().getUserDepTos(),
                                            defaultUserSids,
                                            wcTag.isViewContainFollowing());
                                }

                                // ----------------------
                                // 處理 - 預設可閱人員
                                // ----------------------
                                if (wcTag.getUseUser() != null && wcTag.getUseUser().getUserTos() != null) {
                                    this.getViewPerson(wcTag.getUseUser().getUserTos(), defaultUserSids);
                                }
                            });

            // userSID => ReadReceiptsMemberTo
            Date now = new Date();
            defaultUserSids.forEach(
                    item -> {
                        WCReadReceipts readReceipts = new WCReadReceipts();
                        readReceipts.setWcsid(wcSid);
                        readReceipts.setReader(item);
                        readReceipts.setReadreceipt(WCReadReceiptStatus.UNREAD);
                        readReceipts.setRequiretime(now);
                        readReceipts.setCreateUser(loginUserSid);
                        readReceiptsMember.add(readReceipts);
                    });
        } catch (Exception e) {
            log.warn("getDefaultViewPerson ERROR", e);
        }
        return readReceiptsMember;
    }

    /**
     * 依據可閱部門, 取得可閱人員
     *
     * @param userDepTos           預設可閱部門
     * @param defaultUserSids      預設可閱人員Sids
     * @param containViewFollowing 是否含以下部門
     */
    private void getViewDepPerson(
            List<UserDepTo> userDepTos,
            List<Integer> defaultUserSids,
            boolean containViewFollowing) {

        if (containViewFollowing) { // 含以下部門

            List<Org> allDeps = Lists.newArrayList();
            // 取得所有預設可閱部門 + 預設
            userDepTos.forEach(
                    item -> {
                        // 取得預設部門資料
                        Org dep = wkOrgCache.findBySid(Integer.valueOf(item.getDepSid()));
                        if (!allDeps.contains(dep)) {
                            allDeps.add(dep);
                        }
                        // 取得預設部門所有的『子部門』資料
                        wkOrgCache.findAllChild(dep.getSid()).stream()
                                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                .collect(Collectors.toList())
                                .forEach(
                                        ditem -> {
                                            if (!allDeps.contains(ditem)) {
                                                allDeps.add(ditem);
                                            }
                                        });
                    });
            // 將所有部門的 user 加入 defaultUserSids
            allDeps.forEach(
                    item -> {
                        wkUserCache
                                .findUserWithManagerByOrgSids(Lists.newArrayList(item.getSid()), Activation.ACTIVE)
                                .stream()
                                .distinct()
                                .collect(Collectors.toList())
                                .forEach(
                                        pItem -> {
                                            if (!defaultUserSids.contains(pItem.getSid())) {
                                                defaultUserSids.add(pItem.getSid());
                                            }
                                        });
                    });

        } else {
            // 不含以下部門, 僅將預設部門人員加入
            userDepTos.forEach(
                    item -> {
                        wkUserCache
                                .findUserWithManagerByOrgSids(
                                        Lists.newArrayList(Integer.valueOf(item.getDepSid())),
                                        Activation.ACTIVE)
                                .stream()
                                .distinct()
                                .collect(Collectors.toList())
                                .forEach(
                                        pItem -> {
                                            if (!defaultUserSids.contains(pItem.getSid())) {
                                                defaultUserSids.add(pItem.getSid());
                                            }
                                        });
                    });
        }
    }

    /**
     * 塞入可閱人員by指定人員
     *
     * @param userDepTos      預設可閱部門
     * @param defaultUserSids 預設可閱人員Sids
     */
    private void getViewPerson(List<UserTo> userTos, List<Integer> defaultUserSids) {
        userTos.forEach(
                item -> {
                    if (!defaultUserSids.contains(item.getSid())) {
                        defaultUserSids.add(item.getSid());
                    }
                });
    }

    /**
     * 新增檢核
     *
     * @param wcSid                   主單 sid
     * @param workReportDataComponent 工作聯絡單資料物件
     * @param categoryItemComponent   類別元件
     * @param attachmentVOs           上傳附加檔案物件清單
     * @throws UserMessageException 檢核失敗時拋出
     */
    public void checkCreateMaster(
            String wcSid,
            WorkReportDataComponent workReportDataComponent,
            CategoryItemComponent categoryItemComponent,
            List<AttachmentVO> attachmentVOs) throws UserMessageException {

        // ====================================
        // 主題
        // ====================================
        if (WkStringUtils.isEmpty(jsoupUtils.clearCssTag(workReportDataComponent.getTitle()))) {
            throw new UserMessageException("[主題]為必要輸入資訊，請確認！！");
        }

        if (workReportDataComponent.getTitle().length() > 255) {
            throw new UserMessageException("主題字數超過255個字的限制，請重新輸入！！");
        }

        // ====================================
        // 內容
        // ====================================
        if (WkStringUtils.isEmpty(jsoupUtils.clearCssTag(workReportDataComponent.getContent()))) {
            throw new UserMessageException("[內容]為必要輸入資訊，請確認！！");
        }

        // ====================================
        // 小類(執行項目)
        // ====================================
        if (WkStringUtils.isEmpty(categoryItemComponent.getSelCategoryItems())) {
            throw new UserMessageException("[" + categoryItemComponent.getFieldName() + "]為必要勾選資訊，請確認！！");
        }

        // 查詢選擇的小類設定資料
        List<WCTag> wcTags = this.wcTagManager.findBySidIn(categoryItemComponent.getSelCategoryItems());

        // 防呆
        if (WkStringUtils.isEmpty(wcTags)
                || categoryItemComponent.getSelCategoryItems().size() != wcTags.size()) {
            String errorMessage = WkMessage.EXECTION + "(找不到對應的小類資料)";
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }
        Map<String, WCTag> wcTagMapBySid = wcTags.stream()
                .collect(Collectors.toMap(
                        WCTag::getSid,
                        wcTag -> wcTag));

        // ====================================
        // 取得送件時間
        // ====================================
        // 送件時間 (sid 為空時，代表新建檔，為系統日)
        Date sendDate = new Date();
        if (wcSid != null) {
            WCMaster wcMaster = this.wcMasterManager.findBySid(wcSid);
            if (wcMaster == null) {
                String errorMessage = WkMessage.NEED_RELOAD + "(找不到對應的單據資料)";
                log.error(errorMessage + "wcsid:[{}]", wcSid);
                throw new UserMessageException(errorMessage, InfomationLevel.WARN);
            }
            sendDate = wcMaster.getSendDate();
        }

        // ====================================
        // 執行項目維護-其他判斷1
        // ====================================
        if (WkStringUtils.notEmpty(categoryItemComponent.getLegitimateRangeComponents())) {
            for (LegitimateRangeComponent legitimateRange : categoryItemComponent.getLegitimateRangeComponents()) {

                WCTag wcTag = wcTagMapBySid.get(legitimateRange.getSid());
                if (wcTag == null) {
                    String errorMessage = WkMessage.NEED_RELOAD + "(找不到對應的單據資料)";
                    log.error(errorMessage + "wcsid:[{}],wcTagSid:[{}]", wcSid, legitimateRange.getSid());
                    throw new UserMessageException(errorMessage, InfomationLevel.WARN);
                }

                this.checkLegalDateHelper.checkLegitimateRangeDate(wcTag, sendDate, legitimateRange.getSelectDate());
            }
        }

        // ====================================
        // 執行項目維護-其他判斷2
        // ====================================
        if (WkStringUtils.notEmpty(categoryItemComponent.getStartEndComponents())) {
            for (StartEndComponent startEnd : categoryItemComponent.getStartEndComponents()) {

                WCTag wcTag = wcTagMapBySid.get(startEnd.getSid());
                if (wcTag == null) {
                    String errorMessage = WkMessage.NEED_RELOAD + "(找不到對應的單據資料)";
                    log.error(errorMessage + "wcsid:[{}],wcTagSid:[{}]", wcSid, startEnd.getSid());
                    throw new UserMessageException(errorMessage, InfomationLevel.WARN);
                }

                this.checkLegalDateHelper.checkStartEndDate(
                        wcTag,
                        sendDate,
                        startEnd.getSelectStartDate(),
                        startEnd.getSelectEndDate());
            }
        }

        // ====================================
        // 檢查需要上傳附加檔案
        // ====================================
        this.wcMasterHelper.checkNeedUpdateFile(wcTags, attachmentVOs);
    }

    /**
     * 起訖日轉JSON格式
     *
     * @param startEndList 起訖日物件清單
     * @return
     */
    private String starEndConvertJson(List<StartEndComponent> startEndList) {
        if (startEndList == null) {
            return "[]";
        }

        String startEndJson = "[";
        for (int i = 0; i < startEndList.size(); i++) {
            if (i != startEndList.size() - 1) {
                startEndJson = startEndJson + startEndList.get(i).toString() + ",";
            } else {
                startEndJson = startEndJson + startEndList.get(i).toString();
            }
        }
        startEndJson = startEndJson + "]";
        return startEndJson;
    }

    /**
     * 合法區間轉json格式
     *
     * @param legitimateRangeList 合法區間物件清單
     * @return
     */
    private String legitimateRangeConvertJson(List<LegitimateRangeComponent> legitimateRangeList) {
        if (legitimateRangeList == null) {
            return "[]";
        }
        String startJson = "[";
        for (int i = 0; i < legitimateRangeList.size(); i++) {
            if (i != legitimateRangeList.size() - 1) {
                startJson = startJson + legitimateRangeList.get(i).toString() + ",";
            } else {
                startJson = startJson + legitimateRangeList.get(i).toString();
            }
        }
        startJson = startJson + "]";
        return startJson;
    }

    /**
     * 更新工作聯絡單(主題內容備註)
     *
     * @param sid                     工作聯絡單Sid
     * @param workReportDataComponent 工作聯絡單資料物件
     * @param attachmentVOs           附加檔案物件清單
     * @param categoryItemComponent   小類(執行項目)選項物件
     * @param userSid                 更新者Sid
     * @throws UserMessageException
     */
    public void updateWCMaster(
            String sid,
            WorkReportDataComponent workReportDataComponent,
            List<AttachmentVO> attachmentVOs,
            CategoryItemComponent categoryItemComponent,
            Integer userSid) throws UserMessageException {

        // ====================================
        // 清除控制字元
        // ====================================
        workReportDataComponent.setContent(
                WkStringUtils.removeCtrlChr(workReportDataComponent.getContent()));
        workReportDataComponent.setTitle(
                WkStringUtils.removeCtrlChr(workReportDataComponent.getTitle()));
        workReportDataComponent.setRemark(
                WkStringUtils.removeCtrlChr(workReportDataComponent.getRemark()));

        this.checkCreateMaster(sid, workReportDataComponent, categoryItemComponent, attachmentVOs);

        String endStarJson = starEndConvertJson(categoryItemComponent.getStartEndComponents());
        String legitimateRangeJson = legitimateRangeConvertJson(categoryItemComponent.getLegitimateRangeComponents());

        wcMasterCustomLogicManager.updateContentWCMaster(
                sid,
                workReportDataComponent.getTitle(),
                workReportDataComponent.getContent(),
                workReportDataComponent.getRemark(),
                categoryItemComponent.getSelCategoryItems(),
                endStarJson,
                legitimateRangeJson,
                userSid);
    }

    /**
     * 取得工作聯絡單物件 By 工作聯絡單Sid
     *
     * @param sid 工作聯絡單Sid
     * @return
     */
    public WCMaster getWCMasterBySid(String sid) {
        try {
            return wcMasterManager.findBySid(sid);
        } catch (Exception e) {
            log.warn("getWCMasterBySid Error", e);
        }
        return null;
    }

    /**
     * 取得工作聯絡單介面物件
     *
     * @param sid          工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     * @param loginCompSid 登入公司Sid
     * @param editMode     編輯模式
     * @return
     */
    public WorkReportComponent getWorkReportComponentBySid(
            String sid, Integer loginUserSid, Integer loginCompSid, boolean editMode) {
        try {
            User user = wkUserCache.findBySid(loginUserSid);
            // ====================================
            // 收集登入者主要部門及管理部門
            // ====================================
            Set<Integer> loginManagedDeps = Sets.newHashSet();
            loginManagedDeps.add(user.getPrimaryOrg().getSid());
            Set<Integer> managedDeps = WkOrgCache.getInstance().findManagerOrgSids(loginUserSid);
            if (WkStringUtils.notEmpty(managedDeps)) {
                loginManagedDeps.addAll(managedDeps);
            }

            WCMaster master = wcMasterManager.findBySid(sid);
            WorkReportComponent wrc = new WorkReportComponent();
            wrc.setWorkReportHeaderComponent(transToWorkReportHeaderComponent(master));
            wrc.setWorkReportDataComponent(transToWorkReportDataComponent(master, user));
            wrc.setCategoryItemComponent(
                    this.transToCategoryItemComponent(
                            master, editMode, loginUserSid, loginManagedDeps, loginCompSid));
            wrc.setExecDescComponent(
                    transToExecDescComponent(master, loginUserSid, user.getPrimaryOrg().getSid()));
            wrc.setWcMaster(master);
            return wrc;
        } catch (Exception e) {
            log.warn("GetWorkReportComponentBySid Error", e);
        }
        return null;
    }

    /**
     * 將工作聯絡單物件轉換成工作聯絡單資料介面物件
     *
     * @param wc        工作聯絡單物件
     * @param loginUser 登入者Sid
     * @return
     */
    private WorkReportDataComponent transToWorkReportDataComponent(WCMaster wc, User loginUser) {
        WorkReportDataComponent wdc = new WorkReportDataComponent();
        wdc.setTitle(wc.getTheme());
        wdc.setContent(wc.getContent_css());
        wdc.setRemark(wc.getMemo_css());
        wdc.setLabelcontent(HtmlUtils.htmlUnescape(wc.getContent_css()));
        wdc.setLabelRemark(HtmlUtils.htmlUnescape(wc.getMemo_css()));
        wdc.setSignUser(transToSignInfosText(wc.getSid()));
        wdc.getSignRoleInfos().addAll(this.transToSignInfos(wc.getSid(), loginUser));
        return wdc;
    }

    /**
     * 轉換類別元件
     *
     * @param master           工作聯絡單物件
     * @param editMode         編輯模式
     * @param loginUserSid     登入者Sid
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginCompSid     登入公司Sid
     * @return
     */
    private CategoryItemComponent transToCategoryItemComponent(
            WCMaster master,
            boolean editMode,
            Integer loginUserSid,
            Set<Integer> loginManagedDeps,
            Integer loginCompSid) {

        CategoryItemComponent categoryItemComponent = new CategoryItemComponent();

        categoryItemComponent.loadItems(
                master.getMenuTagSid(),
                master.getCategoryTagTo(),
                loginUserSid.equals(master.getCreate_usr()) && editMode,
                loginManagedDeps,
                loginCompSid,
                loginUserSid);

        // categoryItemComponent.initForViewMode(master);

        categoryItemComponent.initLegitimateRange(
                master.getMenuTagSid(), editMode, loginManagedDeps, loginCompSid, loginUserSid);

        categoryItemComponent.initStartEnd(
                master.getMenuTagSid(), editMode, loginManagedDeps, loginCompSid);

        try {
            categoryItemComponent.convertLegitimateRangeJsonToList(master.getLegitimateRangeDate());
            categoryItemComponent.convertStartEndJsonToList(master.getStartEndDate());
        } catch (IOException ex) {
            log.warn("convertDateJsonToList Error", ex);
        }

        return categoryItemComponent;
    }

    /**
     * 將工作聯絡單物件轉換成工作聯絡單執行說明物件
     *
     * @param master       工作聯絡單物件
     * @param loginUserSid 登入者Sid
     * @param loginDepSid  登入單位Sid
     * @return
     */
    private ExecDescComponent transToExecDescComponent(
            WCMaster master, Integer loginUserSid, Integer loginDepSid) {
        ExecDescComponent execDescComponent = new ExecDescComponent();

        execDescComponent.loadData(master.getMenuTagSid(), master.getCategoryTagTo());

        execDescComponent.checkData(master, loginUserSid, loginDepSid);

        return execDescComponent;
    }

    /**
     * 將工作聯絡單物件轉換成工作聯絡單介面Header物件
     *
     * @param wcMaster 工作聯絡單物件
     * @return
     */
    private WorkReportHeaderComponent transToWorkReportHeaderComponent(WCMaster wcMaster) {
        WorkReportHeaderComponent whc = new WorkReportHeaderComponent();

        whc.setDepartmentName(WkOrgUtils.findNameBySid(wcMaster.getDep_sid()));

        whc.setCreateUserName(WkUserUtils.findNameBySid(wcMaster.getCreate_usr()));

        whc.setCreateTime(
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wcMaster.getCreate_dt()));
        whc.setCreateTimeTitle("建立日期");
        whc.setStatus(wcMaster.getWc_status() == null ? "" : wcMaster.getWc_status().getVal());

        whc.setWorkProjectSID(wcMaster.getSid());
        whc.setWorkProjectNo(wcMaster.getWc_no());

        // ====================================
        // 執行單位資訊
        // ====================================
        List<ExecInfoVO> execInfoVOs = Lists.newArrayList();
        whc.setExecInfoVOs(execInfoVOs);
        // 查詢所此單據有執行單位檔
        List<WCExceDep> allexecDeps = this.wcExceDepLogicComponent.getAllExecDeps(wcMaster.getSid());
        if (WkStringUtils.notEmpty(allexecDeps)) {
            whc.setShowExecStatus(true);

            for (WCExceDep currWCExceDep : allexecDeps) {
                // 項目名稱
                String tagName = "";
                if (Strings.isNullOrEmpty(currWCExceDep.getExecDepSettingSid())) {
                    tagName = "手動加派";
                } else {
                    WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(currWCExceDep.getExecDepSettingSid());
                    WCTag wcTag = wcTagLogicComponents.getWCTagBySid(execDepSetting.getWc_tag_sid());
                    tagName = wcTag.getTagName();
                }
                // 執行人員
                String execUserDetail = "";
                String execUserInfo = "";
                if (currWCExceDep.getExecUserSid() != null) {
                    execUserInfo = WkUserUtils.prepareUserNameWithDep(currWCExceDep.getExecUserSid(), "－");
                    execUserDetail = WkUserUtils.prepareUserNameWithDep(currWCExceDep.getExecUserSid(), OrgLevel.DIVISION_LEVEL, true, "－");
                }
                // 顯示資訊VO
                ExecInfoVO execInfoVo = new ExecInfoVO(
                        currWCExceDep.getSid(),
                        tagName,
                        WkOrgCache.getInstance().findNameBySid(currWCExceDep.getDep_sid()),
                        currWCExceDep.getExecDepStatus() == null ? "" : currWCExceDep.getExecDepStatus().getValue(),
                        execUserInfo,
                        execUserDetail);

                execInfoVOs.add(execInfoVo);
            }
        }

        return whc;
    }

    /**
     * 轉換簽核字串
     *
     * @param reqSignInfos   需求單位會簽List
     * @param checkSignInfos 核准單位會簽List
     * @return
     */
    public String transToSignInfosText(
            List<UserViewVO> reqSignInfos, List<UserViewVO> checkSignInfos) {
        try {
            StringBuilder sb = new StringBuilder();
            if (reqSignInfos != null && !reqSignInfos.isEmpty()) {
                sb.append("申請者會簽");
                sb.append("<br/>");
                StringBuilder usb = new StringBuilder();
                reqSignInfos.forEach(
                        item -> {
                            if (!Strings.isNullOrEmpty(usb.toString())) {
                                usb.append("\\");
                            }
                            usb.append(item.getName());
                        });
                sb.append(usb);
            }
            if (checkSignInfos != null && !checkSignInfos.isEmpty()) {
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("<br/>");
                }
                sb.append("核准者會簽");
                sb.append("<br/>");
                StringBuilder usb = new StringBuilder();
                checkSignInfos.forEach(
                        item -> {
                            if (!Strings.isNullOrEmpty(usb.toString())) {
                                usb.append("\\");
                            }
                            usb.append(item.getName());
                        });
                sb.append(usb);
            }
            return sb.toString();
        } catch (Exception e) {
            log.warn(" wdc.setSignUser(sb.toString()); ERROR", e);
        }
        return "";
    }

    /**
     * 轉換簽核字串
     *
     * @param reqSignInfos   需求單位會簽List
     * @param checkSignInfos 核准單位會簽List
     * @return
     */
    public List<SignRole> transToSignInfos(
            List<UserViewVO> reqSignInfos, List<UserViewVO> checkSignInfos) {
        List<SignRole> signRoles = Lists.newArrayList();
        try {

            if (reqSignInfos != null && !reqSignInfos.isEmpty()) {
                reqSignInfos.forEach(
                        item -> {
                            try {
                                User user = wkUserCache.findBySid(item.getSid());
                                String opinionDescripe = "";
                                SignRole signRole = bpmLogicComponent.settingSignRole(
                                        bpmLogicComponent.reqContinueGroupID,
                                        user.getId(),
                                        bpmLogicComponent.continueRoleName,
                                        "",
                                        user.getName(),
                                        "",
                                        opinionDescripe,
                                        "",
                                        "",
                                        bpmLogicComponent.continueClz);
                                signRoles.add(signRole);
                            } catch (Exception e) {
                                log.warn("transToAddSignRole ERROR", e);
                            }
                        });
            }

        } catch (Exception e) {
            log.warn(" wdc.setSignUser(sb.toString()); ERROR", e);
        }
        return signRoles;
    }

    /**
     * 轉換為簽核資訊物件清單 By 工作聯絡單Sid 及 登入者Sid
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 登入者Sid
     * @return
     */
    public List<SignRole> transToSignInfos(String wcSid, User loginUser) {
        List<SignRole> signRoles = Lists.newArrayList();
        // 會簽者
        List<SingleManagerSignInfo> reqContinueFlowSignInfos = managerSignInfoManager.getCounterSignInfos(wcSid);
        signRoles.addAll(
                bpmLogicComponent.transToAddSignRole(
                        reqContinueFlowSignInfos,
                        ManagerSingInfoType.MANAGERSIGNINFO,
                        bpmLogicComponent.getReqContinueGroupID(),
                        bpmLogicComponent.continueRoleName,
                        bpmLogicComponent.continueClz,
                        loginUser));
        List<SingleManagerSignInfo> checkSingleManagerSignInfos = execManagerSignInfoManager.getCounterSignInfos(wcSid);
        signRoles.addAll(
                bpmLogicComponent.transToAddSignRole(
                        checkSingleManagerSignInfos,
                        ManagerSingInfoType.CHECKMANAGERINFO,
                        bpmLogicComponent.execContinueGroupID,
                        bpmLogicComponent.execContinueRoleName,
                        bpmLogicComponent.execContinueClz,
                        loginUser));

        return signRoles;
    }

    /**
     * 轉換為簽核資訊字串
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public String transToSignInfosText(String wcSid) {
        try {
            StringBuilder sb = new StringBuilder();
            List<SingleManagerSignInfo> reqSingleManagerSignInfos = managerSignInfoManager.getCounterSignInfos(wcSid);
            if (reqSingleManagerSignInfos != null && !reqSingleManagerSignInfos.isEmpty()) {
                sb.append("申請者會簽");
                sb.append("<br/>");
                StringBuilder usb = new StringBuilder();
                reqSingleManagerSignInfos.forEach(
                        item -> {
                            if (!Strings.isNullOrEmpty(usb.toString())) {
                                usb.append("\\");
                            }
                            User u = wkUserCache.findBySid(item.getUserSid());
                            usb.append(u.getName());
                        });
                sb.append(usb);
            }
            List<SingleManagerSignInfo> checkSingleManagerSignInfos = execManagerSignInfoManager.getCounterSignInfos(wcSid);
            if (checkSingleManagerSignInfos != null && !checkSingleManagerSignInfos.isEmpty()) {
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("<br/>");
                }
                sb.append("執行者會簽");
                sb.append("<br/>");
                StringBuilder usb = new StringBuilder();
                checkSingleManagerSignInfos.forEach(
                        item -> {
                            if (!Strings.isNullOrEmpty(usb.toString())) {
                                usb.append("\\");
                            }
                            User u = wkUserCache.findBySid(item.getUserSid());
                            usb.append(u.getName());
                        });
                sb.append(usb);
            }
            return sb.toString();
        } catch (Exception e) {
            log.warn(" transToSignInfosText ERROR", e);
        }
        return "";
    }

    /**
     * 回復工作聯絡單
     *
     * @param sid         工作聯絡單Sid
     * @param userSid     回覆者Sid
     * @param replyEditVO 回覆內容物件
     * @return
     */
    public WCMaster replyWCMaster(String sid, Integer userSid, WCTraceVO replyEditVO) {
        Preconditions.checkState(!Strings.isNullOrEmpty(replyEditVO.getContent()), "請輸入回覆內容！");
        return wcMasterCustomLogicManager.replyWCMaster(
                sid, userSid, replyEditVO.getSid(), replyEditVO.getContent(), replyEditVO.getType());
    }

    public void updateLockUserAndLockDate(String wc_sid, Integer lockUserSid, Date lockTime) {
        try {
            wcMasterManager.updateLockUserAndLockDate(wc_sid, lockTime, lockUserSid);
        } catch (Exception e) {
            log.warn("updateLockUserAndLockDate", e);
        }
    }

    /**
     * 解除工作聯絡單鎖定
     *
     * @param wc_sid       工作聯絡單Sid
     * @param closeUserSid 鎖定者Sid
     */
    public void closeLockUserAndLockDate(String wc_sid, Integer closeUserSid) {
        try {
            wcMasterManager.closeLockUserAndLockDate(wc_sid, closeUserSid);
        } catch (Exception e) {
            log.warn("closeLockUserAndLockDate", e);
        }
    }

    /**
     * 更新 再次回覆資料
     *
     * @param sid              工作聯絡單Sid
     * @param userSid          回覆者Sid
     * @param againReplyEditVO 回覆內容物件
     * @return
     */
    public WCMaster againReplyWCMaster(String sid, Integer userSid,
            WCAgainTraceVO againReplyEditVO) {
        Preconditions.checkState(!Strings.isNullOrEmpty(againReplyEditVO.getContent()), "請輸入回覆內容！");
        return wcMasterCustomLogicManager.againReplyWCMaster(
                sid,
                againReplyEditVO.getSid(),
                againReplyEditVO.getTraceSid(),
                userSid,
                againReplyEditVO.getType(),
                againReplyEditVO.getContent());
    }

    /**
     * 建立加簽人員追蹤
     *
     * @param sid                          工作聯絡單Sid
     * @param wcNo                         工作聯絡單No
     * @param lgoinUserSid                 登入者Sid
     * @param removeSingleManagerSignInfos 移除的會簽人員
     * @param addUserFlowSid               增加的會簽人員
     * @return
     */
    public WCTrace createCounterSignTrace(
            String sid,
            String wcNo,
            Integer lgoinUserSid,
            List<SingleManagerSignInfo> removeSingleManagerSignInfos,
            List<Integer> addUserFlowSid) {

        WCTrace tr = wcTraceCustomLogicManager.getWCTrace_ADD_SIGN_CHANGE(
                sid, wcNo, lgoinUserSid, removeSingleManagerSignInfos, addUserFlowSid);
        wcTraceManager.create(tr, lgoinUserSid);

        return tr;
    }
}
