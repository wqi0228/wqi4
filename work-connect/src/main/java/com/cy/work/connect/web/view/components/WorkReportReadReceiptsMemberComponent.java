/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import com.cy.work.connect.web.logic.components.WCOrgLogic;
import com.cy.work.connect.web.logic.components.WCReadReceiptComponents;
import com.cy.work.connect.web.logic.components.WCUserLogic;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.ReadReceiptVO;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DualListModel;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportReadReceiptsMemberComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2725955151512647040L;
    private final Integer loginUserSid;
    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;
    private LinkedHashSet<UserViewVO> leftViewVO;
    private String wc_Id;

    @Getter
    @Setter
    private String filterNameValue;

    @Getter
    private List<ReadReceiptVO> readReceiptVOs;
    @Getter
    private boolean showReadReceiptTab = false;

    private Integer compSid;

    @SuppressWarnings("unused")
    private Integer createUser;

    private LinkedHashSet<UserViewVO> allCompUser;

    public WorkReportReadReceiptsMemberComponent(Integer loginUserSid) {
        this.loginUserSid = loginUserSid;
        pickUsers = new DualListModel<>();
        this.readReceiptVOs = Lists.newArrayList();
    }

    public void loadUserPicker(String wc_Id) {
        this.filterNameValue = "";
        this.wc_Id = wc_Id;
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        this.compSid = user.getPrimaryOrg().getCompanySid();
        allCompUser = Sets.newLinkedHashSet();
        try {
            WkOrgCache.getInstance().findAllDepByCompSid(this.compSid).stream()
                .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList())
                .forEach(
                    item -> {
                        allCompUser.addAll(
                            WCUserLogic.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.warn(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }

        leftViewVO = Sets.newLinkedHashSet();
        Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        List<Org> allChildOrg =
            WkOrgCache.getInstance().findAllChild(baseOrg.getSid()).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());
        leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(
            item -> {
                leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });
        List<UserViewVO> rightViewVO = Lists.newArrayList();
        List<ReadReceiptVO> readReceiptVOs =
            WCReadReceiptComponents.getInstance().getReadReceiptVOByWcSid(wc_Id);
        readReceiptVOs.forEach(
            item -> {
                User tempUser = WkUserCache.getInstance()
                    .findBySid(Integer.valueOf(item.getUserSid()));
                Org dep = WkOrgCache.getInstance().findBySid(tempUser.getPrimaryOrg().getSid());
                UserViewVO uv =
                    new UserViewVO(
                        tempUser.getSid(),
                        tempUser.getName(),
                        WkOrgUtils.prepareBreadcrumbsByDepName(
                            dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-"));
                if (item.getWReadReceiptStatus().equals(WCReadReceiptStatus.SIGN)) {
                    uv.setMoveAble(false);
                }
                rightViewVO.add(uv);
            });
        pickUsers.setSource(removeTargetUserView(Lists.newArrayList(leftViewVO), rightViewVO));
        pickUsers.setTarget(rightViewVO);
    }

    public void loadSelectDepartment(List<Org> departments) {
        settingTargetReaded();
        if (departments == null || departments.size() == 0) {
            Preconditions.checkState(false, "請挑選部門!!");
            return;
        }
        // leftViewVO.clear();

        LinkedHashSet<UserViewVO> selectDepUserView = Sets.newLinkedHashSet();
        departments.forEach(
            item -> {
                selectDepUserView.addAll(
                    WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });
        pickUsers.setSource(
            removeTargetUserView(Lists.newArrayList(selectDepUserView), pickUsers.getTarget()));
    }

    public void filterName() {
        settingTargetReaded();
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(
                removeTargetUserView(Lists.newArrayList(allCompUser), pickUsers.getTarget()));
        } else {
            pickUsers.setSource(
                removeTargetUserView(
                    allCompUser.stream()
                        .filter(
                            each -> each.getName().toLowerCase()
                                .contains(filterNameValue.toLowerCase()))
                        .collect(Collectors.toList()),
                    pickUsers.getTarget()));
        }
        DisplayController.getInstance().update("readReceiptPanel");
    }

    private void settingTargetReaded() {
        List<ReadReceiptVO> readReceiptVOs =
            WCReadReceiptComponents.getInstance().getReadReceiptVOByWcSid(wc_Id);
        // 過濾出已讀人員
        List<ReadReceiptVO> readedReadReceipt =
            readReceiptVOs.stream()
                .filter(each -> each.getWReadReceiptStatus().equals(WCReadReceiptStatus.SIGN))
                .collect(Collectors.toList());
        if (pickUsers.getTarget() != null && pickUsers.getTarget().size() > 0) {
            pickUsers
                .getTarget()
                .forEach(
                    item -> {
                        List<ReadReceiptVO> sameReadReceiptsMemberTo =
                            readedReadReceipt.stream()
                                .filter(
                                    each -> each.getUserSid().equals(String.valueOf(item.getSid())))
                                .collect(Collectors.toList());
                        if (sameReadReceiptsMemberTo != null
                            && sameReadReceiptsMemberTo.size() > 0) {
                            item.setMoveAble(false);
                        }
                    });
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source,
        List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
            item -> {
                // 未挑選不可以包含已挑選的人員及自己本身
                if (!target.contains(item) && !loginUserSid.equals(item.getSid())) {
                    filterSource.add(item);
                }
            });
        return filterSource;
    }

    public void loadData(String wc_Id, Integer createUser) {
        this.wc_Id = wc_Id;
        this.createUser = createUser;
        loadData();
    }

    public void loadData() {
        readReceiptVOs = WCReadReceiptComponents.getInstance().getReadInfoVOByWcSid(wc_Id);
        // boolean loginUserInReadReceiptMembers = false;
        showReadReceiptTab = readReceiptVOs != null && readReceiptVOs.size() > 0;
    }
}
