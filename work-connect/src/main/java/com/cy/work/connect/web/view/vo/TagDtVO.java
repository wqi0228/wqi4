package com.cy.work.connect.web.view.vo;

import com.cy.commons.enums.Activation;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 * @author jimmy_chou
 */
public class TagDtVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1446579797831089957L;

    @Getter
    private final String sid;
    @Getter
    private final String tagType;
    @Getter
    private final String categoryName;
    @Getter
    private final String menuName;
    @Getter
    private final String name;
    @Getter
    private final Integer seq;
    @Getter
    private String categorySid;
    @Getter
    private String menuSid;
    @Getter
    private Activation status;
    @Getter
    private MenuTagSearchVO menuTagSearchVO;
    @Getter
    private TagSearchVO subTagSearchVO;
    @Getter
    private ExecDepSettingSearchVO execDepSettingSearchVO;

    public TagDtVO(
        String tagType, String sid, String categoryName, String menuName, String name,
        Integer seq) {
        this.tagType = tagType;
        this.sid = sid;
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.name = name;
        this.seq = seq;
    }

    public TagDtVO(
        String tagType,
        String sid,
        String categoryName,
        String menuName,
        String name,
        Integer seq,
        Activation status) {
        this.tagType = tagType;
        this.sid = sid;
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.name = name;
        this.seq = seq;
        this.status = status;
    }

    public TagDtVO(
        String tagType,
        String sid,
        String categoryName,
        String menuName,
        String name,
        Integer seq,
        Activation status,
        MenuTagSearchVO menuTagSearchVO,
        TagSearchVO subTagSearchVO,
        ExecDepSettingSearchVO execDepSettingSearchVO,
        String categorySid,
        String menuSid) {
        this.tagType = tagType;
        this.sid = sid;
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.name = name;
        this.seq = seq;
        this.status = status;
        this.menuTagSearchVO = menuTagSearchVO;
        this.subTagSearchVO = subTagSearchVO;
        this.execDepSettingSearchVO = execDepSettingSearchVO;
        this.categorySid = categorySid;
        this.menuSid = menuSid;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 28 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TagDtVO other = (TagDtVO) obj;
        return Objects.equals(this.sid,
            other.sid);
    }
}
