/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class RelevanceVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7761150550096174206L;

    @Getter
    private final String wc_sid;
    @Getter
    private final String wc_no;
    @Getter
    private final String theme;
    @Getter
    private String updateDate;
    @Getter
    private String createDate;
    @Getter
    private String groupSid;
    @Getter
    private String createUserName;
    @Getter
    private String connectDate;

    public RelevanceVO(String wc_sid, String theme, String wc_no) {
        this.wc_sid = wc_sid;
        this.theme = theme;
        this.wc_no = wc_no;
    }

    public RelevanceVO(
        String wc_sid,
        String theme,
        String wc_no,
        String createDate,
        String groupSid,
        String createUserName,
        String connectDate) {
        this.wc_sid = wc_sid;
        this.theme = theme;
        this.wc_no = wc_no;
        this.createDate = createDate;
        this.groupSid = groupSid;
        this.createUserName = createUserName;
        this.connectDate = connectDate;
    }
}
