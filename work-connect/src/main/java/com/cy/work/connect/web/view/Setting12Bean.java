package com.cy.work.connect.web.view;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCSetGroupManager;
import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.logic.manager.WorkSettingUserManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.logic.vo.WCGroupVo;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleALLTagTreeManager;
import com.cy.work.connect.web.logic.components.WCSetGroupLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.components.qstree.OrgUserTreeMBean;
import com.cy.work.connect.web.view.vo.TagVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.event.ReorderEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 部門設定設定群組 - 維護作業 MBean
 *
 * @author allen
 */
@Controller
@Scope("view")
@Slf4j
public class Setting12Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6245919942534370618L;
    /**
     * 查詢區域組件
     */
    @Getter
    @Setter
    public Setting12SearchComponent searchComponent;
    /**
     * 查詢編輯區域組件
     */
    @Getter
    @Setter
    public Setting12EditComponent editComponent = new Setting12EditComponent();
    /**
     *
     */
    @Getter
    @Setter
    public List<WCGroupVo> groupList;
    /**
     *
     */
    @Getter
    @Setter
    public WCGroupVo selectedRow;

    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCSetGroupManager groupManager;
    @Autowired
    private DisplayController displayController;
    @Autowired
    private WCConfigTempletManager configTempletManager;
    @Autowired
    private WCSetPermissionManager permissionManager;
    // 工作聯絡單
    @Getter
    private MultipleALLTagTreeManager allTagTreeManager;
    // 選擇的節點
    private List<TreeNode> selectedNode;
    @Getter
    @Setter
    private TreeNode[] selectedTagNode;
    @Getter
    @Setter
    private TreeNode[] selectedTempletNode;
    @Getter
    @Setter
    private TreeNode[] selectedMenuNode;
    // Templet根節點
    @Getter
    private TreeNode templetTreeRoot;
    /**
     * 是否展開
     */
    @Getter
    @Setter
    private Boolean checkAll;

    /**
     * 是否編輯
     */
    @Getter
    @Setter
    private Boolean editable;

    private Org loginComp;

    private Map<String, TargetType> targetTypeMap;

    @PostConstruct
    public void init() {
        this.selectedNode = Lists.newArrayList();
        // 唯讀
        this.editable = false;
        // 收合
        this.checkAll = false;
        // 登入公司
        this.loginComp = this.orgManager.findById(SecurityFacade.getCompanyId());
        // 初始化 工作聯絡單 Tag 樹
        initRootTreeTag();
        // 初始化 模版 樹
        initRootTreeTemplet();
        // 初始化 搜尋元件
        searchComponent = new Setting12SearchComponent();
        // 執行搜尋
        this.doSearch();
        // 執行唯讀
        this.toggleDisable();
    }

    public TreeNode[] getSelectedNode() {
        if (selectedNode.isEmpty()) {
            return null;
        } else {
            return selectedNode.stream().toArray(size -> new TreeNode[size]);
        }
    }

    public void setSelectedNode(TreeNode[] selectedNode) {
        try {
        } catch (Exception e) {
            log.warn("setSelectedNode ERROR", e);
        }
    }

    public void onTreeSelect(NodeSelectEvent event) {
        try {
            this.selectedNode.add(event.getTreeNode());
        } catch (Exception e) {
            log.warn("onTreeSelect ERROR", e);
        }
    }

    public void onTreeUnSelect(NodeUnselectEvent event) {
        try {
            this.selectedNode.remove(event.getTreeNode());
        } catch (Exception e) {
            log.warn("onTreeUnSelect ERROR", e);
        }
    }

    /**
     * 查詢
     */
    public void doSearch() {
        this.selectedNode = Lists.newArrayList();
        // ====================================
        // 查詢
        // ====================================
        this.groupList =
            this.groupManager.queryByCondition(
                this.getSearchComponent().getSearchVo().getSelectedOrgSids(),
                this.getSearchComponent().getSearchVo().getSelectedUserSids(),
                Optional.ofNullable(this.orgManager.findById(SecurityFacade.getCompanyId()))
                    .map(Org::getSid)
                    .orElseGet(() -> null));
    }

    /**
     * 初始root 工作聯絡單單據
     */
    private void initRootTreeTag() {
        this.allTagTreeManager = new MultipleALLTagTreeManager();
        this.allTagTreeManager.buildTagTree(this.loginComp.getSid(), true, false);
    }

    /**
     * 初始root 模版
     */
    private void initRootTreeTemplet() {

        templetTreeRoot = new DefaultTreeNode();

        List<WCConfigTempletVo> templetList =
            this.configTempletManager.queryByCondition(
                SecurityFacade.getUserSid(), null, null, this.loginComp.getSid());

        for (WCConfigTempletVo templet : templetList) {
            new DefaultTreeNode("templet", templet, templetTreeRoot);
        }
    }

    /**
     * 切換 展開/收合
     */
    public void toggleExpand() {
        if (this.checkAll) {
            allTagTreeManager.expandAllTree();
            this.expandAll(this.templetTreeRoot, true, false);
        } else {
            allTagTreeManager.collapseAllTree();
            this.expandAll(this.templetTreeRoot, false, false);
        }
    }

    /**
     * 展開該節點下的所有節點
     *
     * @param root     : 根節點
     * @param isExpand : 設定為 true 則展開全部，反之則收起全部
     * @param isCheck  : 設定為 true 則全部取消勾選，反之則不異動
     */
    private void expandAll(TreeNode root, boolean isExpand, boolean isCheck) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        for (TreeNode treeNode : root.getChildren()) {
            treeNode.setExpanded(isExpand);
            if (!isExpand && isCheck) {
                treeNode.setSelected(false);
            }
            expandAll(treeNode, isExpand, isCheck);
        }
    }

    /**
     * 開始編輯
     */
    public void prepareToEdit() {
        try {
            editable = true;
            toggleDisable();
        } catch (Exception ex) {
            log.error("prepareToEdit：" + ex.getMessage(), ex);
            MessagesUtils.showError("編輯失敗，請洽系統人員! " + ex.getMessage());
        }
    }

    /**
     * 執行存檔
     */
    public void doUpdate() {
        try {
            WCSetGroupLogicComponent.getInstance()
                .doUpdate(
                    this.selectedNode,
                    this.targetTypeMap,
                    this.selectedRow,
                    this.loginComp.getSid(),
                    SecurityFacade.getUserSid());
            MessagesUtils.showInfo("存檔成功");
        } catch (Exception ex) {
            log.error("doUpdate 存檔失敗：" + ex.getMessage(), ex);
            MessagesUtils.showError("存檔失敗，請洽系統人員! ");
        }
    }

    /**
     * 執行取消
     */
    public void executeCancel() {
        editable = false;
        toggleDisable();
    }

    /**
     * 切換 唯讀/編輯
     */
    public void toggleDisable() {
        if (this.editable) {
            // 編輯
            this.disableAll(allTagTreeManager.getRootNode(), true);
            this.disableAll(this.templetTreeRoot, true);
        } else {
            // 唯讀
            this.disableAll(allTagTreeManager.getRootNode(), false);
            this.disableAll(this.templetTreeRoot, false);
        }
    }

    /**
     * 唯讀該節點下的所有節點
     *
     * @param root       : 根節點
     * @param selectable : 設定為 true 則全部唯讀，反之則全部可編輯
     */
    private void disableAll(TreeNode root, boolean selectable) {
        for (TreeNode treeNode : root.getChildren()) {
            treeNode.setSelectable(selectable);
            disableAll(treeNode, selectable);
        }
    }

    /**
     * 檢查勾選節點並展開
     *
     * @param root
     * @param sids
     */
    private void checkSelect(TreeNode root, Map<TargetType, List<String>> permissionMap) {
        for (TreeNode treeNode : root.getChildren()) {
            Object data = treeNode.getData();
            String sid;

            switch (treeNode.getType()) {
                case "CategoryTag":
                    // 大類
                case "MenuTag":
                    // 中類
                case "Tag":
                    // 小類
                case "ExecDep":
                    // 執行單位
                    sid = ((TagVO) data).getSid();
                    break;
                case "templet":
                    // 模版
                    sid = ((WCConfigTempletVo) data).getSid().toString();
                    break;
                default:
                    sid = "";
                    MessagesUtils.showWarn("此類節點尚未定義，請洽系統人員! ");
                    break;
            }

            List<String> setPermission = permissionMap.get(
                this.targetTypeMap.get(treeNode.getType()));
            if (setPermission != null && setPermission.size() > 0 && setPermission.contains(sid)) {
                treeNode.setSelected(true);
                this.selectedNode.add(treeNode);
                this.allTagTreeManager.expandUP(treeNode, true);
            }
            this.checkSelect(treeNode, permissionMap);
        }
    }

    private void resetTree() {
        // 工作聯絡單
        this.expandAll(this.allTagTreeManager.getRootNode(), false, true);
        // 模版
        this.expandAll(this.templetTreeRoot, false, true);
    }

    /**
     * 選取 設定群組 資料
     */
    public void onRowSelect() {
        this.selectedNode = Lists.newArrayList();
        this.editable = false;
        TreeNode root = new DefaultTreeNode();
        this.resetTree();
        this.toggleDisable();
        this.targetTypeMap = Maps.newHashMap();
        this.targetTypeMap.put("CategoryTag", TargetType.CATEGORY_TAG);
        this.targetTypeMap.put("MenuTag", TargetType.MENU_TAG);
        this.targetTypeMap.put("Tag", TargetType.TAG);
        this.targetTypeMap.put("ExecDep", TargetType.EXEC_DEP);
        this.targetTypeMap.put("templet", TargetType.CONFIG_TEMPLET);

        List<WCSetPermission> permissions =
            this.permissionManager.findPermissionByCompSidAndGroupSid(
                this.loginComp.getSid(), this.selectedRow.getSid());
        Map<TargetType, List<String>> permissionMap =
            permissions.stream()
                .collect(
                    Collectors.groupingBy(
                        WCSetPermission::getTargetType,
                        Collectors.mapping(WCSetPermission::getTargetSid, Collectors.toList())));

        // 工作聯絡單
        List<TargetType> tagTree =
            Lists.newArrayList(
                TargetType.CATEGORY_TAG, TargetType.MENU_TAG, TargetType.TAG, TargetType.EXEC_DEP);
        if (!Collections.disjoint(tagTree, permissionMap.keySet())) {
            root = this.allTagTreeManager.getRootNode();
            this.checkSelect(root, permissionMap);
        }

        // 模版
        List<TargetType> templetTree = Lists.newArrayList(TargetType.CONFIG_TEMPLET);
        if (!Collections.disjoint(templetTree, permissionMap.keySet())) {
            root = this.templetTreeRoot;
            this.checkSelect(root, permissionMap);
        }
    }

    /**
     * 開啟設定群組新增視窗
     */
    public void openAddDialog() {
        // 資料初始化
        this.editComponent.init();
        // 設定為新增模式
        this.editComponent.setEditMode("A");
        // 開啟編輯視窗
        displayController.showPfWidgetVar("editDialog");
    }

    /**
     * 開啟設定群組編輯視窗
     *
     * @param sid
     */
    public void openEditDialog(Long sid) {

        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (sid == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            log.error("openEditDialog：selectedRow 為空！");
            return;
        }

        WCGroupVo groupVo = this.groupManager.findBySid(sid);

        if (groupVo == null) {
            MessagesUtils.showError("該筆資料已經被刪除, 請重新整理頁面!");
            log.warn("該筆資料已經被刪除, 請重新整理頁面!");
            return;
        }

        // ====================================
        // 初始化
        // ====================================
        // 資料初始化
        this.editComponent.init();
        // 設定為編輯模式
        this.editComponent.setEditMode("E");
        // 放入編輯資料
        this.editComponent.setGroupVo(groupVo);
        // 開啟編輯視窗
        displayController.showPfWidgetVar("editDialog");
    }

    /**
     * 存檔 設定群組
     */
    public void saveSetGroup() {

        if (WkStringUtils.isEmpty(this.editComponent.getGroupVo().getGroupName())) {
            MessagesUtils.showInfo("請輸入群組名稱");
            return;
        }

        try {
            // ====================================
            // 儲存
            // ====================================
            this.groupManager.saveSetting(
                this.editComponent.getGroupVo(), SecurityFacade.getUserSid(),
                this.loginComp.getSid());

            // ====================================
            // 畫面操作
            // ====================================
            MessagesUtils.showInfo("儲存成功!");
            displayController.hidePfWidgetVar("editDialog");

        } catch (Exception e) {
            String msg = "儲存時發生錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 刪除設定群組
     *
     * @param sid
     */
    public void delete(Long sid) {

        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (sid == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            log.error("delete：selectedRow 為空！");
            return;
        }

        WCGroupVo wcGroupVo = this.groupManager.findBySid(sid);

        if (wcGroupVo == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            return;
        }

        // ====================================
        // 檢核是否有使用
        // ====================================
        // 查詢
        String inUsedMessage =
            this.groupManager.checkInUsed(this.loginComp.getSid(), wcGroupVo.getSid());

        if (WkStringUtils.notEmpty(inUsedMessage)) {
            MessagesUtils.showWarn(
                "此設定群組已經被使用, " + "目前無法刪除! " + "被使用項目如下:<br/><br/>" + inUsedMessage);
            return;
        }

        // ====================================
        // 刪除
        // ====================================
        try {
            this.groupManager.deleteGroup(wcGroupVo.getSid());
            // ====================================
            // 畫面操作
            // ====================================
            MessagesUtils.showInfo("刪除成功!");

        } catch (Exception e) {
            String msg = "刪除時發生錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 異動排序
     *
     * @param event
     */
    public void sort(ReorderEvent event) {

        // ====================================
        // 更新排序序號
        // ====================================
        log.info("更新顯示排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());

        try {
            // 更新排序序號
            this.groupManager.updateSortSeqByListIndex(groupList);

        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 初始化部門樹 - 查詢區塊
     */
    public void initSerachDepTree() {
        this.searchComponent.setDepTree(
            this.initDepTree(this.searchComponent.getSearchVo().getSelectedOrgSids()));
    }

    /**
     * 初始化部門樹 - 編輯區塊
     */
    public void initEditDepTree() {
        this.editComponent.setDepTree(
            this.initDepTree(this.editComponent.getGroupVo().getConfigValue().getDeps()));
    }

    /**
     * 初始化部門樹
     */
    private DepTreeComponent initDepTree(List<Integer> selectedOrgSids) {

        DepTreeComponent depTree = new DepTreeComponent();

        // ====================================
        // 準備已選擇部門
        // ====================================
        List<Org> selectedOrgs = Lists.newArrayList();
        boolean defaultShowDisableDep = false;
        if (WkStringUtils.notEmpty(selectedOrgSids)) {
            for (Integer orgSid : selectedOrgSids) {
                // 依據SID 取得Org物件
                if (orgSid == null) {
                    continue;
                }
                Org org = this.orgManager.findBySid(orgSid);
                if (org == null) {
                    continue;
                }
                selectedOrgs.add(org);
                // 判斷是否該顯示停用部門
                if (org.getStatus().equals(Activation.INACTIVE)) {
                    defaultShowDisableDep = true;
                }
            }
        }

        // ====================================
        // 初始化樹
        // ====================================
        List<Org> allOrgs = this.orgManager.findAll();

        depTree.init(
            allOrgs, allOrgs, this.loginComp, selectedOrgs, true, true, false,
            defaultShowDisableDep);

        return depTree;
    }

    /**
     * 初始化人員樹 - 查詢區塊
     */
    public void openSearchUserQuickSelectedTree() {
        this.searchComponent.setSearchUserQuickSelectedTree(new OrgUserTreeMBean());
        this.searchComponent
            .getSearchUserQuickSelectedTree()
            .init(
                this.loginComp.getSid(),
                SecurityFacade.getPrimaryOrgSid(),
                this.searchComponent.getSelectedData,
                Maps.newHashMap());
    }

    /**
     * 初始化人員樹 - 編輯區塊
     */
    public void openEditUserQuickSelectedTree() {
        this.editComponent.setEditUserQuickSelectedTree(new OrgUserTreeMBean());
        this.editComponent
            .getEditUserQuickSelectedTree()
            .init(
                this.loginComp.getSid(),
                SecurityFacade.getPrimaryOrgSid(),
                this.editComponent.getSelectedData,
                Maps.newHashMap());
    }

    /**
     * 查詢區域組件
     *
     * @author allen
     */
    public class Setting12SearchComponent implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -4957088356049454549L;

        /**
         * 部門樹組件
         */
        @Getter
        @Setter
        private DepTreeComponent depTree;

        /**
         * 人員樹組件
         */
        @Getter
        @Setter
        private OrgUserTreeMBean searchUserQuickSelectedTree = new OrgUserTreeMBean();

        @Setter
        @Getter
        private String userDisplayContent;

        @Setter
        @Getter
        private String depDisplayContent;

        /**
         * 查詢條件
         */
        @Getter
        @Setter
        private Setting12SearchVo searchVo;
        /**
         * 實做供元件呼叫取得已選清單的方法, 會將此份名單帶入右邊的【已選擇人員】清單
         */
        Function<Map<String, Object>, List<?>> getSelectedData =
            (condition) -> {
                return Lists.newArrayList(this.searchVo.getSelectedUserSids());
            };

        /**
         * 建構子
         */
        public Setting12SearchComponent() {
            this.depTree = new DepTreeComponent();
            this.searchVo = new Setting12SearchVo();
        }

        /**
         * 部門樹設定視窗點選確定
         */
        public void depTreeConfirm() {
            List<Integer> selectedOrgSids = Lists.newArrayList();

            // 取得畫面所選的單位
            List<Org> deps = depTree.getSelectOrg();
            // 整理為 SID List
            if (WkStringUtils.notEmpty(deps)) {
                selectedOrgSids = deps.stream().map(Org::getSid).collect(Collectors.toList());
            }

            this.searchVo.selectedOrgSids = selectedOrgSids;

            // 畫面顯示
            this.depDisplayContent =
                workSettingOrgManager.prepareDepsShowText(selectedOrgSids, false, false);

            DisplayController.getInstance().hidePfWidgetVar("setting12SearchDepDlg");
            DisplayController.getInstance().update("searchDepDisplayContent");
        }

        /**
         * 人員樹設定視窗點選確定
         */
        public void refreshUser() {
            // 取得元件中選取的 user 清單
            List<Integer> userSids = this.searchUserQuickSelectedTree.getSelectedDataList();

            if (WkStringUtils.isEmpty(userSids)) {
                // 代表清除人員
                this.searchVo.setSelectedUserSids(Lists.newArrayList());
            } else {
                this.searchVo.setSelectedUserSids(userSids);
            }

            // 畫面顯示
            this.userDisplayContent =
                WorkSettingUserManager.getInstance().prepareUsersShowText(userSids, false);
        }

        /**
         * Setting12 查詢欄位容器
         *
         * @author allen
         */
        public class Setting12SearchVo implements Serializable {

            /**
             *
             */
            private static final long serialVersionUID = -2993415769501979498L;

            /**
             * 含有人員設定
             */
            @Getter
            @Setter
            private List<Integer> selectedUserSids;

            /**
             * 含有部門設定
             */
            @Getter
            @Setter
            private List<Integer> selectedOrgSids;

            public Setting12SearchVo() {
                this.selectedUserSids = Lists.newArrayList();
                this.selectedOrgSids = Lists.newArrayList();
            }
        }
    }

    /**
     * 編輯區域組件
     *
     * @author allen
     */
    public class Setting12EditComponent implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -3826163155612684408L;

        /**
         * 編輯類型 (新增/編輯)
         */
        @Getter
        @Setter
        private String editMode;

        /**
         * 部門樹組件
         */
        @Getter
        @Setter
        private DepTreeComponent depTree = new DepTreeComponent();

        /**
         * 人員樹組件
         */
        @Getter
        @Setter
        private OrgUserTreeMBean editUserQuickSelectedTree = new OrgUserTreeMBean();

        /**
         * 設定群組資料
         */
        @Getter
        @Setter
        private WCGroupVo groupVo = new WCGroupVo();
        /**
         * 實做供元件呼叫取得已選清單的方法, 會將此份名單帶入右邊的【已選擇人員】清單
         */
        Function<Map<String, Object>, List<?>> getSelectedData =
            (condition) -> {
                if (this.groupVo.getConfigValue() == null
                    || this.groupVo.getConfigValue().getUsers() == null) {
                    return Lists.newArrayList();
                }
                return Lists.newArrayList(this.groupVo.getConfigValue().getUsers());
            };

        /**
         * 資料初始化
         */
        public void init() {
            this.depTree = new DepTreeComponent();
            this.groupVo = new WCGroupVo();
        }

        /**
         * 部門樹設定視窗點選確定
         */
        public void depTreeConfirm() {

            List<Integer> selectedOrgSids = Lists.newArrayList();

            // 取得畫面所選的單位
            List<Org> deps = depTree.getSelectOrg();
            // 整理為 SID List
            if (WkStringUtils.notEmpty(deps)) {
                selectedOrgSids = deps.stream().map(Org::getSid).collect(Collectors.toList());
            }

            // 放到編輯資料物件欄位中
            this.groupVo.getConfigValue().setDeps(selectedOrgSids);

            DisplayController.getInstance().hidePfWidgetVar("setting12EditDepDlg");
        }

        /**
         * 人員樹設定視窗點選確定
         */
        public void refreshUser() {
            // 取得元件中選取的 user 清單
            List<Integer> userSids = this.editUserQuickSelectedTree.getSelectedDataList();

            if (WkStringUtils.isEmpty(userSids)) {
                // 代表清除人員
                this.groupVo.getConfigValue().setUsers(Lists.newArrayList());
            } else {
                this.groupVo.getConfigValue().setUsers(userSids);
            }
        }
    }
}
