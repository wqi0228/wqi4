/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.search;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.FavoriteSearchColumn;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.TableUpDownListener;
import com.cy.work.connect.web.logic.components.WCFavoriteLogicComponents;
import com.cy.work.connect.web.logic.components.search.FavoriteListLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.TableUpDownBean;
import com.cy.work.connect.web.view.components.CustomColumnComponent;
import com.cy.work.connect.web.view.vo.FavoriteSearchVO;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkReportSidTo;
import com.cy.work.connect.web.view.vo.column.search.FavoriteSearchColumnVO;
import com.cy.work.connect.web.view.vo.search.query.CheckedListQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 處理清單
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class Search10Bean implements Serializable, TableUpDownListener {

    /**
     *
     */
    private static final long serialVersionUID = -8612441420031349609L;

    @Getter
    /** dataTable Id */
    private final String dataTableID = "dataTableWorkReport";

    @Getter
    /** dataTable widgetVar */
    private final String dataTableWv = "dataTableWorkReportWv";

    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Autowired
    private FavoriteListLogicComponent searchLogicComponent;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private WCFavoriteLogicComponents wcFavoriteLogicComponents;

    @Getter
    /** 登入者 */
    private UserViewVO userViewVO;
    // /** 可查詢單位 */
    // private List<Integer> depSids;
    @Getter
    /** 登入單位 */
    private OrgViewVo depViewVo;

    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;

    @Getter
    /** view 顯示錯誤訊息 */
    private String errorMessage;

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /** */
        private static final long serialVersionUID = 1244040152406548307L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confimDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
        }
    };

    @Setter
    @Getter
    /** 是否版面切換(顯示Frame畫面) */
    private boolean showFrame = false;

    @Getter
    /** 匯出使用 */
    private boolean hasDisplay = true;

    @Getter
    /** dataTable 欄位資訊 */
    private FavoriteSearchColumnVO columnVO;

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /** */
        private static final long serialVersionUID = -2238044483915716984L;

        @Override
        public void reload() {
            columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };

    @Setter
    @Getter
    /** 列表資料 */
    private List<FavoriteSearchVO> allVos;

    @Setter
    @Getter
    /** 列表選取資料 */
    private FavoriteSearchVO selVo;
    /**
     * 暫存資料
     */
    private List<FavoriteSearchVO> tempVOs;
    /**
     * 暫存資料
     */
    private String tempSelSid;

    @Getter
    /** frame 連結路徑 */
    private String iframeUrl = "";

    @Getter
    /** 查詢物件 */
    private CheckedListQuery query;

    @Getter
    private boolean showCancelBtn = false;
    @Getter
    private CustomColumnComponent customColumn;

    @PostConstruct
    public void init() {
        this.initLogin();
        this.initComponents();
        this.clear();
    }

    /**
     * 初始化 登入者資訊
     */
    private void initLogin() {
        this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
    }

    /**
     * 初始化 元件
     */
    private void initComponents() {
        columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(
                WCReportCustomColumnUrlType.FAVORITE_LIST,
                Arrays.asList(FavoriteSearchColumn.values()),
                columnVO.getPageCount(),
                userViewVO.getSid(),
                messageCallBack,
                dataTableReLoadCallBack);
        query = new CheckedListQuery();
    }

    /**
     * 清除並查詢
     */
    public void clear() {
        this.query.init();
        this.doSearch();
    }

    /**
     * 執行查詢
     */
    public void doSearch() {

        if (query.getStartDate() != null && query.getEndDate() != null) {
            if (query.getStartDate().after(query.getEndDate())) {
                MessagesUtils.showWarn("收藏區間起始日期不可晚於結束日期");
                return;
            }
        }

        try {
            this.allVos = this.searchLogicComponent.search(query, SecurityFacade.getUserSid(), null);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String message = "收藏夾查詢失敗！" + e.getMessage();
            log.error(message, e);
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
            MessagesUtils.showError(message);
            return;
        }

        if (allVos.size() > 0) {
            DisplayController.getInstance()
                    .execute("selectDataTablePage('" + dataTableWv + "',0);");
        }
    }

    /**
     * 回到列表查詢
     */
    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.warn("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行上一筆
     */
    @Override
    public void openerByBtnUp() {
        try {
            if (!allVos.isEmpty()) {
                this.toUp();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("工作聯絡單進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    public void selectCell() {
        showCancelBtn = (selVo != null);
    }

    public void cancelFavorite() {
        try {
            wcFavoriteLogicComponents.removeFavorite(
                    selVo.getWcSid(), selVo.getWcNo(), userViewVO.getSid());
            updateInfoToDataTable(selVo.getWcSid());
            selVo = null;
            DisplayController.getInstance().update(dataTableID);
            selectCell();
        } catch (Exception e) {
            log.warn("cancelFavorite", e);
            messageCallBack.showMessage("收藏取消失敗,請重新點選一次");
        }
    }

    /**
     * 執行上一筆
     */
    private void toUp() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index > 0) {
                    selVo = tempVOs.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index > 0) {
                    selVo = allVos.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
            }
        } catch (Exception e) {
            log.warn("toUp", e);
            this.clearTemp();
            if (!allVos.isEmpty()) {
                selVo = allVos.get(0);
            }
        }
    }

    /**
     * 執行下一筆
     */
    @Override
    public void openerByBtnDown() {
        try {
            if (!allVos.isEmpty()) {
                this.toDown();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("工作聯絡單查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行下一筆
     */
    private void toDown() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index >= 0) {
                    selVo = tempVOs.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index >= 0) {
                    selVo = allVos.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
            }
        } catch (Exception e) {
            log.warn("toDown", e);
            this.clearTemp();
            if (allVos.size() > 0) {
                selVo = allVos.get(allVos.size() - 1);
            }
        }
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNextAndReaded() {
        this.settingPreviousAndNext();
        tableUpDownBean.setSession_now_sid(selVo.getWcSid());
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNext() {
        int index = allVos.indexOf(selVo);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(allVos.get(index - 1).getWcSid());
        }
        if (index == allVos.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(allVos.get(index + 1).getWcSid());
        }
    }

    /**
     * 判斷是否有暫存資訊
     */
    private boolean checkTempVOs() {
        return tempVOs != null && !tempVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid);
    }

    /**
     * 開啟分頁
     *
     * @param sid
     */
    public void btnOpenUrl(String sid) {
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入滿版
     */
    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selVo.getWcSid();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.warn("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 變更 selVo
     */
    private void settintSelVo(String sid) {
        FavoriteSearchVO sel = new FavoriteSearchVO(sid);
        int index = allVos.indexOf(sel);
        if (index > 0) {
            selVo = allVos.get(index);
        } else {
            selVo = allVos.get(0);
        }
    }

    /**
     * ?????
     */
    public void reloadDataByUpdate() {
        this.doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByFavorite() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.warn("doReloadInfo", ex);
        }
    }

    /**
     * ?????
     *
     * @param sid
     */
    private void updateInfoToDataTable(String sid) {
        try {
            List<FavoriteSearchVO> result = this.searchLogicComponent.search(query, userViewVO.getSid(), sid);
            if (result == null || result.isEmpty()) {
                tempVOs = Lists.newArrayList();
                if (allVos != null && !allVos.isEmpty()) {
                    allVos.forEach(
                            item -> {
                                tempVOs.add(item);
                            });
                    if (selVo != null) {
                        tempSelSid = selVo.getWcSid();
                    }
                    List<FavoriteSearchVO> sel = allVos.stream()
                            .filter(each -> each.getWcSid().equals(sid))
                            .collect(Collectors.toList());
                    if (sel != null && !sel.isEmpty()) {
                        sel.forEach(
                                item -> {
                                    allVos.remove(item);
                                });
                    }
                }
            } else {
                result.forEach(
                        newItem -> {
                            int index = allVos.indexOf(newItem);
                            if (index >= 0) {
                                allVos.get(index).replaceValue(newItem);
                            } else {
                                allVos.add(newItem);
                            }
                            if (tempVOs != null && !tempVOs.isEmpty()) {
                                int indext = tempVOs.indexOf(newItem);
                                if (indext >= 0) {
                                    tempVOs.get(indext).replaceValue(newItem);
                                } else {
                                    tempVOs.add(newItem);
                                }
                            }
                        });
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("updateInfoToDataTable Error", e);
        }
    }

    /**
     * 執行匯出前置
     */
    public void hideColumnContent() {
        hasDisplay = false;
    }

    /**
     * 清除暫存
     */
    private void clearTemp() {
        tempSelSid = "";
        tempVOs = null;
    }

    /**
     * 取得暫存索引
     */
    private int getTempIndex() { return tempVOs.indexOf(new FavoriteSearchVO(tempSelSid)); }
}
