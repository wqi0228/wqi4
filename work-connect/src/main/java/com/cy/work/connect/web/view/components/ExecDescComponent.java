/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.web.logic.components.WCExceDepLogicComponent;
import com.cy.work.connect.web.logic.components.WCExecDepSettingLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCTagLogicComponents;
import com.cy.work.connect.web.view.vo.ExecDescVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jimmy_chou
 */
@Slf4j
public class ExecDescComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6690731972661117173L;

    private final WCMenuTagLogicComponent menuTagLogicComponent;
    private final WCTagLogicComponents tagLogicComponents;
    private final WCExecDepSettingLogicComponent execDepSettingLogicComponent;
    private final WkOrgCache orgManager;
    private final WCExceDepLogicComponent exceDepLogicComponent;

    /**
     * 欄位名稱
     */
    @Getter
    private String fieldName;

    /**
     * 執行說明
     */
    @Getter
    private List<ExecDescVO> execDescs;

    /**
     * 是否顯示執行說明
     */
    @Getter
    private boolean showExecDesc;

    public ExecDescComponent() {
        this.menuTagLogicComponent = WCMenuTagLogicComponent.getInstance();
        this.tagLogicComponents = WCTagLogicComponents.getInstance();
        this.execDepSettingLogicComponent = WCExecDepSettingLogicComponent.getInstance();
        this.orgManager = WkOrgCache.getInstance();
        this.exceDepLogicComponent = WCExceDepLogicComponent.getInstance();
        this.showExecDesc = false;
    }

    public static String delHTMLTag(String htmlStr) {
        if (htmlStr == null) {
            return null;
        }
        String regEx_script = "<script[^>]*?>[\\s\\S]*?</script>"; // 定義script的規則運算式
        String regEx_style = "<style[^>]*?>[\\s\\S]*?</style>"; // 定義style的規則運算式
        String regEx_html = "<[^>]+>"; // 定義HTML標籤的規則運算式

        Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll(""); // 過濾script標籤

        Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
        Matcher m_style = p_style.matcher(htmlStr);
        htmlStr = m_style.replaceAll(""); // 過濾style標籤

        Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        Matcher m_html = p_html.matcher(htmlStr);
        htmlStr = m_html.replaceAll(""); // 過濾html標籤

        return htmlStr.trim(); // 返回文本字串
    }

    /**
     * 讀取類別選項
     *
     * @param menuTagSid
     * @param to
     */
    public void loadData(String menuTagSid, CategoryTagTo to) {
        try {
            MenuTagVO category = menuTagLogicComponent.findToBySid(menuTagSid);
            this.fieldName = category.getItemName();
            this.bulidData(to.getTagSids());
        } catch (Exception e) {
            log.warn("讀取類別選項 ERROR", e);
        }
    }

    /**
     * 建立執行說明資料
     *
     * @param categorySid
     * @param sids
     * @param loginDepSid
     * @param loginCompSid
     */
    private void bulidData(List<String> sids) {
        execDescs =
            execDepSettingLogicComponent.getWCExecDepSettingByWcTagSidList(sids).stream()
                .filter(each -> !Strings.isNullOrEmpty(delHTMLTag(each.getExecDesc())))
                .map(
                    each ->
                        new ExecDescVO(
                            each.getSid(),
                            Optional.ofNullable(
                                    tagLogicComponents.getWCTagBySid(each.getWc_tag_sid()))
                                .map(WCTag::getTagName)
                                .orElseGet(() -> null),
                            Optional.ofNullable(orgManager.findBySid(each.getExec_dep_sid()))
                                .orElseGet(() -> null),
                            each.getExecDesc()))
                .collect(Collectors.toList());
    }

    /**
     * 建立選項
     *
     * @param master
     * @param loginUserSid
     * @param loginDepSid
     */
    public void checkData(WCMaster master, Integer loginUserSid, Integer loginDepSid) {
        Org dep = orgManager.findBySid(loginDepSid);
        List<Org> childOrgs = orgManager.findAllChild(dep.getSid());
        List<Org> depts =
            orgManager.findManagerWithChildOrgSids(loginUserSid).stream()
                .map(each -> orgManager.findBySid(each))
                .collect(Collectors.toList());
        childOrgs.add(dep);
        childOrgs.addAll(orgManager.findAllParent(dep.getSid()));
        childOrgs.addAll(depts);
        List<Integer> orgSids = Lists.newArrayList(); // 登入者擁有瀏覽權限的執行部門
        childOrgs.forEach(
            item -> {
                if (!orgSids.contains(item.getSid())) {
                    orgSids.add(item.getSid());
                }
            });

        // 簽核到執行方或完成簽核流程需執行時，才會有wc_exe_dep資料
        List<WCExceDep> exceDepList =
            exceDepLogicComponent.getAllExecDeps(master.getSid()).stream()
                .filter(
                    each -> !WCExceDepStatus.STOP.equals(each.getExecDepStatus())) // 過濾需求單位最高主管復原後
                .filter(each -> orgSids.contains(each.getDep_sid())) // 確認登入者為執行方
                .collect(Collectors.toList());

        execDescs =
            execDescs.stream()
                .filter(each -> orgSids.contains(each.getExecDep().getSid())) // 確認登入者為執行方
                .collect(Collectors.toList());

        if (execDescs != null
            && execDescs.size() > 0
            && exceDepList.size() > 0) { // 當此單執行說明有資料 且 登入者為執行方時，才顯示執行說明區塊
            this.showExecDesc = true;
        }
    }
}
