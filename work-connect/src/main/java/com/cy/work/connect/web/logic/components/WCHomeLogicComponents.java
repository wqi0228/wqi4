/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.security.repository.AuthUrlManager;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.FuncItemGroupLogicHelper;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.vo.WorkFunItemVO;
import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WCFuncGroupBaseType;
import com.cy.work.connect.web.callable.HomeCallable;
import com.cy.work.connect.web.callable.HomeCallableCondition;
import com.cy.work.connect.web.callable.HomeCallableService;
import com.cy.work.connect.web.view.vo.WCHomepageFavoriteVO;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 選單邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCHomeLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5416117885691906575L;

    private static WCHomeLogicComponents instance;
    /**
     * 模組名稱
     */
    private final String modelName = "/work-connect/";
    @Autowired
    private WCHomepageFavoriteLogicComponents wcHomepageFavoriteLogicComponents;
    @Autowired
    private AuthUrlManager authUrlManager;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    /**
     * 授權網址清單
     */
    private List<String> authUrls;

    public static WCHomeLogicComponents getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCHomeLogicComponents.instance = this;
    }

    /**
     * 取得選單 By 選單ID 及 登入者Sid 及 登入者部門Sid
     *
     * @param keepType     選單ID
     * @param loginUserSid 登入者Sid
     * @param depSid       登入者部門Sid
     * @return
     */
    public List<WorkFunItemGroupVO> getWorkFunItemGroupByTypeAndUserPermission(
            String keepType, Integer loginUserSid, Integer depSid) {
        this.authUrls = this.getWCAuthUrls();
        List<WorkFunItemGroupVO> workFunItemGroupVOs = null;

        // ====================================
        // 取得選單大項資料
        // ====================================
        try {
            // 查詢選單大項資料
            workFunItemGroupVOs = FuncItemGroupLogicHelper.getInstance().getWorkFunItemGroupVOs().stream()
                    .filter(p -> p.getGroupBaseSid().equals(keepType))
                    .collect(Collectors.toList());

            if (workFunItemGroupVOs == null || workFunItemGroupVOs.isEmpty()) {
                // log.error("選單資訊無相同type : keepType(" + keepType + ")");
                return Lists.newArrayList();
            }

        } catch (Exception e) {
            log.warn("getWorkFunItemGroupByTypeAndUserPermission", e);
            return Lists.newArrayList();
        }

        List<WorkFunItemGroupVO> menuGroups = Lists.newArrayList();
        List<HomeCallable> homeCallables = Lists.newArrayList();

        //收集『功能群組』下，登入者有權限的『功能』
        for (WorkFunItemGroupVO funItemGroupVO : workFunItemGroupVOs) {

            List<WorkFunItemVO> funcItemsByUserAuth = Lists.newArrayList();

            // ====================================
            // 報表快選區
            // ====================================
            if (WkCommonUtils.compareByStr(FuncItemGroupLogicHelper.REPORT_FAVORITE_ITEM_GROUP_SID, funItemGroupVO.getGroupSid())) {
                // 取得登入者自設的快選區功能
                List<WCHomepageFavoriteVO> wcHomepageFavoriteVOs = wcHomepageFavoriteLogicComponents.getWCHomepageFavoriteVOByUserSid(
                        loginUserSid);
                if (WkStringUtils.isEmpty(wcHomepageFavoriteVOs)) {
                    continue;
                }
                Set<String> favoritePaths = wcHomepageFavoriteVOs.stream()
                        .map(WCHomepageFavoriteVO::getPagePath)
                        .collect(Collectors.toSet());

                // 取得登入者有權限的選單群組
                List<WorkFunItemGroupVO> workFunItemGroups = this.getWorkFunItemGroupByTypeAndUserPermission(
                        WCFuncGroupBaseType.SEARCH.getKey(),
                        loginUserSid,
                        depSid);
                if (WkStringUtils.isEmpty(workFunItemGroups)) {
                    continue;
                }

                // 收集符合權限的的功能
                for (WorkFunItemGroupVO workFunItemGroup : workFunItemGroups) {
                    for (WorkFunItemVO funItemVO : workFunItemGroup.getWorkFunItemVOs()) {
                        if (favoritePaths.contains(funItemVO.getUrl())) {
                            funcItemsByUserAuth.add(funItemVO);
                        }
                    }
                }
            }
            // ====================================
            // 一般
            // ====================================
            else {
                for (WorkFunItemVO funItemVO : funItemGroupVO.getWorkFunItemVOs()) {
                    // 檢測 system menu 權限 (from security)
                    if (!hasPermission(funItemVO.getUrl())) {
                        continue;
                    }

                    // 功能為『管理領單人員』，此時多判斷是否有項目維護權限
                    if (funItemVO.getUrl().contains("setting6.xhtml")) {
                        if (!this.execDepSettingManager.isManagerRecevice(SecurityFacade.getUserSid())) {
                            continue;
                        }
                    }

                    funcItemsByUserAuth.add(funItemVO);

                    // 檢測是否有該進行撈取筆數的item
                    HomeCallable homeCallable = HomeCallableService.getHomeCallableByWorkFunItemComponentType(
                            funItemGroupVO,
                            funItemVO.getComponent_id(),
                            loginUserSid,
                            depSid);

                    if (homeCallable != null) {
                        homeCallables.add(homeCallable);
                    }
                }
            }

            //群組下有可使用的功能才顯示
            if(WkStringUtils.notEmpty(funcItemsByUserAuth)) {
                menuGroups.add(
                        new WorkFunItemGroupVO(
                                funItemGroupVO.getGroupSid(),
                                funItemGroupVO.getName(),
                                funItemGroupVO.getSeq(),
                                funItemGroupVO.getGroupBaseSid(),
                                funcItemsByUserAuth));
            }
        }

        List<HomeCallableCondition> results = HomeCallableService.doHomeCallableList(homeCallables);

        if (results != null && !results.isEmpty()) {
            menuGroups.forEach(
                    item -> {
                        List<HomeCallableCondition> selHomeCallableConditions = results.stream()
                                .filter(p -> p.getWorkFunItemGroupVO().getGroupSid()
                                        .equals(item.getGroupSid()))
                                .collect(Collectors.toList());
                        if (selHomeCallableConditions == null || selHomeCallableConditions.isEmpty()) {
                            return;
                        }

                        item.getWorkFunItemVOs()
                                .forEach(
                                        subItems -> {
                                            List<HomeCallableCondition> subSelHomeCallableCondition = selHomeCallableConditions.stream()
                                                    .filter(
                                                            p -> p.getWorkFunItemComponentType()
                                                                    .getComponentID()
                                                                    .equals(subItems.getComponent_id()))
                                                    .collect(Collectors.toList());
                                            if (subSelHomeCallableCondition == null
                                                    || subSelHomeCallableCondition.isEmpty()) {
                                                return;
                                            }
                                            subItems.setCountInfo(
                                                    subSelHomeCallableCondition.get(0).getConntString());
                                            subItems.setCssStr(
                                                    subSelHomeCallableCondition.get(0).getCssString());
                                        });
                    });
        }
        return menuGroups;
    }

    /**
     * 判斷該選單在該使用者是否有權限可顯示
     *
     * @param url 該選單位址
     * @return
     */
    private Boolean hasPermission(String url) {
        return this.authUrls.stream().anyMatch(each -> each.contains(url));
    }

    /**
     * 取得工作聯絡單所擁有的Url
     *
     * @return
     */
    private List<String> getWCAuthUrls() {
        return authUrlManager
                .getViewableSet(SecurityFacade.getUserId(), SecurityFacade.getCompanyId())
                .stream()
                .filter(each -> each.contains(modelName))
                .collect(Collectors.toList());
    }
}
