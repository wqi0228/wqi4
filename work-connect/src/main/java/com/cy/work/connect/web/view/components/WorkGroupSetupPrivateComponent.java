/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.cy.commons.enums.Activation;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.connect.web.listener.ReLoadCallBack;
import com.cy.work.connect.web.logic.components.WorkGroupSetupPrivateLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.vo.WorkGroupVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WorkGroupSetupPrivateComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1830136643282118166L;
    private final Integer loginUserSid;
    @Getter
    private final WorkGroupSetupPrivateMaitainComponent maintainComponent;
    private final ReLoadCallBack parentReloadCallBack;
    @Getter
    private final String group_setup_private_dlg_panel_table_id;
    @Getter
    private final String group_setup_private_dlg_panel_table_wv;
    @Setter
    @Getter
    private String statsId;
    @Setter
    @Getter
    private String filterValue;
    @Setter
    @Getter
    private List<WorkGroupVO> workGroupVOs;
    private final ReLoadCallBack groupReloadCallBack =
        new ReLoadCallBack() {
            /** */
            private static final long serialVersionUID = 9090742385327129418L;

            @Override
            public void onReload() {
                doSearch();
            }
        };
    @Getter
    @Setter
    private WorkGroupVO selectItem;
    @Getter
    private String deleteName;

    public WorkGroupSetupPrivateComponent(
        String group_setup_private_setting_dlg_wv,
        String groupMaintainComponent_pickPanel,
        String workGroup_deptUnitDlg,
        String workGroup_dep,
        String group_setup_private_dlg_panel_table_wv,
        String group_setup_private_dlg_panel_table_id,
        Integer loginUserSid,
        ReLoadCallBack parentReloadCallBack) {
        this.loginUserSid = loginUserSid;
        this.parentReloadCallBack = parentReloadCallBack;
        this.group_setup_private_dlg_panel_table_id = group_setup_private_dlg_panel_table_id;
        this.group_setup_private_dlg_panel_table_wv = group_setup_private_dlg_panel_table_wv;
        this.maintainComponent =
            new WorkGroupSetupPrivateMaitainComponent(
                group_setup_private_setting_dlg_wv,
                groupMaintainComponent_pickPanel,
                workGroup_deptUnitDlg,
                workGroup_dep,
                loginUserSid,
                groupReloadCallBack);
        this.workGroupVOs = Lists.newArrayList();
    }

    public void closeGroupManager() {
        if (this.workGroupVOs != null) {
            this.workGroupVOs.clear();
            DisplayController.getInstance().update(group_setup_private_dlg_panel_table_id);
        }
        parentReloadCallBack.onReload();
    }

    public void loadData() {
        this.statsId = Activation.ACTIVE.name();
        this.filterValue = "";
        callSearch();
    }

    private void doFilter() {
        List<WorkGroupVO> tempWorkGroupVO = Lists.newArrayList();
        if (workGroupVOs != null) {

            workGroupVOs.forEach(
                item -> {
                    if (!Strings.isNullOrEmpty(statsId) && !item.getStatusID().equals(statsId)) {
                        return;
                    }

                    if (!Strings.isNullOrEmpty(filterValue)) {
                        String groupName =
                            (Strings.isNullOrEmpty(item.getGroup_name())) ? ""
                                : item.getGroup_name();
                        String note = (Strings.isNullOrEmpty(item.getNote())) ? "" : item.getNote();
                        if (!groupName.toLowerCase().contains(filterValue.toLowerCase())
                            && !note.toLowerCase().contains(filterValue.toLowerCase())) {
                            return;
                        }
                    }
                    tempWorkGroupVO.add(item);
                });
        }
        workGroupVOs = tempWorkGroupVO;
    }

    public void callSearch() {
        doSearch();
        DisplayController.getInstance()
            .execute("selectDataTablePage('" + group_setup_private_dlg_panel_table_wv + "',0);");
    }

    private void doSearch() {
        try {
            this.workGroupVOs =
                WorkGroupSetupPrivateLogicComponents.getInstance().getWorkGroupVO(loginUserSid);
            doFilter();
            DisplayController.getInstance().update(group_setup_private_dlg_panel_table_id);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void doDeleteWorkGroup() {
        try {
            WorkGroupSetupPrivateLogicComponents.getInstance()
                .deleteWorkGroupVO(selectItem.getGroup_sid());
            loadData();
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void toDeleteWorkGroup(String sid) {
        if (!Strings.isNullOrEmpty(sid)) {
            List<WorkGroupVO> selWorkGroupVO =
                workGroupVOs.stream()
                    .filter(each -> each.getGroup_sid().equals(sid))
                    .collect(Collectors.toList());
            if (selWorkGroupVO != null && !selWorkGroupVO.isEmpty()) {
                selectItem = selWorkGroupVO.get(0);
            }
        }
        if (selectItem == null) {
            MessagesUtils.showError("載入刪除物件失敗");
            return;
        }
        deleteName = selectItem.getGroup_name();
        DisplayController.getInstance().update(group_setup_private_dlg_panel_table_id);
    }

    public void addWorkGroupSetupPrivateData() {
        maintainComponent.loadData(null);
    }

    public void loadWorkGroupSetupPrivateData() {
        WorkGroupVO wo = null;
        if (selectItem != null) {
            List<WorkGroupVO> selWorkGroupVO =
                workGroupVOs.stream()
                    .filter(each -> each.getGroup_sid().equals(selectItem.getGroup_sid()))
                    .collect(Collectors.toList());
            if (selWorkGroupVO != null && !selWorkGroupVO.isEmpty()) {
                wo = selWorkGroupVO.get(0);
            }
        }
        maintainComponent.loadData(wo);
    }
}
