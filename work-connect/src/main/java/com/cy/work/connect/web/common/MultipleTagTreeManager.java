/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;

import com.cy.commons.enums.Activation;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.eo.AbstractEo;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCExecDepSettingLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCTagLogicComponents;
import com.cy.work.connect.web.view.vo.ExecDepSettingSearchVO;
import com.cy.work.connect.web.view.vo.MenuTagSearchVO;
import com.cy.work.connect.web.view.vo.TagSearchVO;
import com.cy.work.connect.web.view.vo.TagVO;
import com.cy.work.connect.web.view.vo.search.query.CheckAuthoritySearchQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;

/**
 * 多選 類別、單據名稱、執行項目 元件
 *
 * @author jimmy_chou
 */
public class MultipleTagTreeManager extends BaseTreeManager {

    /**
     *
     */
    private static final long serialVersionUID = 5013497001044514730L;

    @Getter
    @Setter
    /** 是否全選 */
    private boolean checkAll = false;

    public MultipleTagTreeManager() {
        rootNode = new CheckboxTreeNode();
    }

    public TreeNode[] getSelNode() {
        if (selNode.isEmpty()) {
            return null;
        } else {
            return selNode.stream().toArray(size -> new TreeNode[size]);
        }
    }

    public void setSelNode(TreeNode[] selNode) {
        if (selNode == null) {
            return;
        }
        this.selNode = Arrays.asList(selNode);
    }

    /**
     * 進行展開
     */
    public void expandAllTree() {
        expandAll(rootNode, true, false);
    }

    /**
     * 進行收合
     */
    public void collapseAllTree() {
        expandAll(rootNode, false, false);
    }

    /**
     * 展開該節點的所有父節點
     *
     * @param root     : 根節點
     * @param isExpand : 設定為 true 則展開，反之則收起
     */
    public void expandUP(TreeNode root, boolean isExpand) {
        if (root == null) {
            return;
        }
        TreeNode treeNode = root.getParent();
        if (treeNode == null) {
            return;
        }
        treeNode.setExpanded(isExpand);
        if (treeNode.getParent() == null) {
            return;
        }
        expandUP(treeNode, isExpand);
    }

    /**
     * 進行全選
     */
    public void selectAllTree() {
        if (!checkAll) {
            this.selectUnAllTree();
            return;
        }
        selNode = Lists.newArrayList();
        List<TreeNode> list = Lists.newArrayList();
        storeAllSubNodes(list, this.getRootNode());
        list.forEach(
                each -> {
                    if (each.isSelectable()) {
                        this.selNode.add(each);
                        each.setSelected(true);
                    }
                });
        expandAll(rootNode, true, false);
    }

    /**
     * 進行反選
     */
    private void selectUnAllTree() {
        selNode = Lists.newArrayList();
        List<TreeNode> list = Lists.newArrayList();
        this.storeAllSubNodes(list, this.getRootNode());
        list.forEach(
                each -> {
                    if (each.isSelectable()) {
                        each.setSelected(false);
                    }
                });
        expandAll(rootNode, false, false);
    }

    public void cleanSelNodeAll() {
        cleanSelNode();
        selectUnAllTree();
        checkAll = false;
    }

    /**
     * 依照名稱進行搜尋
     */
    public void searchTagTree() {
        expandAll(rootNode, false, false);
        List<TreeNode> allNodes = Lists.newArrayList();
        storeAllSubNodes(allNodes, rootNode);

        if (Strings.isNullOrEmpty(this.getSearchText())) {
            return;
        }
        TagVO o = null;
        for (TreeNode tn : allNodes) {
            o = (TagVO) tn.getData();
            o.setFirstStyle("");
            o.setSecondStyle("");
            o.setThirdStyle("");
            if (o.getCategoryName().toUpperCase().contains(this.getSearchText().toUpperCase())
                    || o.getMenuName().toUpperCase().contains(this.getSearchText().toUpperCase())
                    || o.getName().toUpperCase().contains(this.getSearchText().toUpperCase())) {
                if (o.getCategoryName().toUpperCase()
                        .contains(this.getSearchText().toUpperCase())) {
                    o.setFirstStyle("WS1-1-3");
                } else if (o.getMenuName().toUpperCase()
                        .contains(this.getSearchText().toUpperCase())) {
                    o.setSecondStyle("WS1-1-3");
                } else if (o.getName().toUpperCase().contains(this.getSearchText().toUpperCase())) {
                    o.setThirdStyle("WS1-1-3");
                }
                if (tn.getChildren() != null && tn.getChildren().size() > 0) {
                    for (TreeNode trn : tn.getChildren()) {
                        expandToParent(trn);
                    }
                } else {
                    expandToParent(tn);
                }
            }
        }
    }

    public void initTreeAllComp(Integer loginCompSid, List<TreeNode> selNode) {
        this.initTreeAllComp(loginCompSid, selNode, false, false);
    }

    public void initTreeAllComp(
            Integer loginCompSid,
            List<TreeNode> selNode,
            boolean setPermissionFlag,
            boolean showNoExecDep) {
        this.rootNode = new CheckboxTreeNode();
        this.buildTagTree(loginCompSid, setPermissionFlag, showNoExecDep);
        this.loadSelTagTree(selNode);
    }

    /**
     * 建立Tag樹
     *
     * @param loginCompSid 公司Sid
     */
    protected void buildTagTree(Integer loginCompSid) {
        this.buildTagTree(loginCompSid, false, false);
    }

    /**
     * 建立Tag樹
     *
     * @param loginCompSid      公司Sid
     * @param setPermissionFlag 是否判斷設定權限
     * @param showNoExecDep     是否顯示尚未設定執行單位之執行項目
     */
    protected void buildTagTree(
            Integer loginCompSid, boolean setPermissionFlag, boolean showNoExecDep) {
        // 準備權限判斷物件
        List<TargetType> targetTypes = Lists.newArrayList(
                TargetType.CATEGORY_TAG, TargetType.MENU_TAG, TargetType.TAG, TargetType.EXEC_DEP);
        List<WCSetPermission> permissionAll = WCSetPermissionManager.getInstance()
                .findByCompSidAndTargetTypeIn(loginCompSid, targetTypes);
        Map<String, List<WCSetPermission>> permissionMap = permissionAll.stream()
                .collect(
                        Collectors.groupingBy(
                                each -> each.getTargetType().name().concat(each.getTargetSid())));

        TreeNode categroyNode = null, menuNode = null;
        List<WCCategoryTag> categoryTagList = WCCategoryTagLogicComponent.getInstance().findAll(Activation.ACTIVE, loginCompSid);
        for (WCCategoryTag categoryTag : categoryTagList) {
            List<WCMenuTag> menuTagList = WCMenuTagLogicComponent.getInstance()
                    .getWCMenuTagByCategorySid(categoryTag.getSid(), Activation.ACTIVE);
            for (WCMenuTag menuTag : menuTagList) {
                List<WCTag> tagList = WCTagLogicComponents.getInstance()
                        .getWCTagByMenuSid(menuTag.getSid(), Activation.ACTIVE);
                for (WCTag tag : tagList) {
                    List<WCExecDepSetting> execDepSettingList = WCExecDepSettingLogicComponent.getInstance()
                            .getWCExecDepSettingByWcTagSid(tag.getSid(), Activation.ACTIVE);
                    if (!showNoExecDep && execDepSettingList.isEmpty()) {
                        continue;
                    }
                    if (!rootNode.getChildren().stream()
                            .map(TreeNode::getData)
                            .collect(Collectors.toList())
                            .contains(
                                    new TagVO(
                                            categoryTag.getSid(),
                                            categoryTag.getCategoryName(),
                                            "",
                                            "",
                                            categoryTag.getSeq()))) {
                        // 判斷設定權限
                        if (setPermissionFlag) {
                            List<WCSetPermission> categoryPermissionList = permissionMap.get(
                                    TargetType.CATEGORY_TAG.name().concat(categoryTag.getSid()));
                            WCSetPermission categoryPermission = WkStringUtils.notEmpty(categoryPermissionList)
                                    ? categoryPermissionList.get(0)
                                    : null;
                            if (categoryPermission != null
                                    && !categoryPermission.getPermissionGroups().isEmpty()) {
                                List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                                        .groupPermissionUserSids(
                                                loginCompSid, categoryPermission.getPermissionGroups());
                                if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                                    continue;
                                }
                            }
                        }
                        categroyNode = new CheckboxTreeNode(
                                "CategoryTag",
                                new TagVO(
                                        categoryTag.getSid(),
                                        categoryTag.getCategoryName(),
                                        "",
                                        "",
                                        categoryTag.getSeq()),
                                rootNode);

                        // 判斷設定權限
                        if (setPermissionFlag) {
                            List<WCSetPermission> menuPermissionList = permissionMap.get(
                                    TargetType.MENU_TAG.name().concat(menuTag.getSid()));
                            WCSetPermission menuPermission = WkStringUtils.notEmpty(menuPermissionList) ? menuPermissionList.get(
                                    0) : null;
                            if (menuPermission != null && !menuPermission.getPermissionGroups()
                                    .isEmpty()) {
                                List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                                        .groupPermissionUserSids(
                                                loginCompSid, menuPermission.getPermissionGroups());
                                if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                                    continue;
                                }
                            }
                        }
                        menuNode = new CheckboxTreeNode(
                                "MenuTag",
                                new TagVO(menuTag.getSid(), "", menuTag.getMenuName(), "",
                                        menuTag.getSeq()),
                                categroyNode);

                        // 判斷設定權限
                        if (setPermissionFlag) {
                            List<WCSetPermission> tagPermissionList = permissionMap.get(TargetType.TAG.name().concat(tag.getSid()));
                            WCSetPermission tagPermission = WkStringUtils.notEmpty(tagPermissionList) ? tagPermissionList.get(0)
                                    : null;
                            if (tagPermission != null && !tagPermission.getPermissionGroups()
                                    .isEmpty()) {
                                List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                                        .groupPermissionUserSids(loginCompSid,
                                                tagPermission.getPermissionGroups());
                                if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                                    continue;
                                }
                            }
                        }
                        new CheckboxTreeNode(
                                "Tag",
                                new TagVO(tag.getSid(), "", "", tag.getTagName(), tag.getSeq()),
                                menuNode);
                    } else {
                        if (!categroyNode.getChildren().stream()
                                .map(TreeNode::getData)
                                .collect(Collectors.toList())
                                .contains(
                                        new TagVO(menuTag.getSid(), "", menuTag.getMenuName(), "",
                                                menuTag.getSeq()))) {
                            // 判斷設定權限
                            if (setPermissionFlag) {
                                List<WCSetPermission> menuPermissionList = permissionMap.get(
                                        TargetType.MENU_TAG.name().concat(menuTag.getSid()));
                                WCSetPermission menuPermission = WkStringUtils.notEmpty(menuPermissionList)
                                        ? menuPermissionList.get(0)
                                        : null;
                                if (menuPermission != null && !menuPermission.getPermissionGroups()
                                        .isEmpty()) {
                                    List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                                            .groupPermissionUserSids(
                                                    loginCompSid, menuPermission.getPermissionGroups());
                                    if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                                        continue;
                                    }
                                }
                            }
                            menuNode = new CheckboxTreeNode(
                                    "MenuTag",
                                    new TagVO(menuTag.getSid(), "", menuTag.getMenuName(), "",
                                            menuTag.getSeq()),
                                    categroyNode);

                            // 判斷設定權限
                            if (setPermissionFlag) {
                                List<WCSetPermission> tagPermissionList = permissionMap.get(TargetType.TAG.name().concat(tag.getSid()));
                                WCSetPermission tagPermission = WkStringUtils.notEmpty(tagPermissionList)
                                        ? tagPermissionList.get(0)
                                        : null;
                                if (tagPermission != null && !tagPermission.getPermissionGroups()
                                        .isEmpty()) {
                                    List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                                            .groupPermissionUserSids(
                                                    loginCompSid, tagPermission.getPermissionGroups());
                                    if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                                        continue;
                                    }
                                }
                            }
                            new CheckboxTreeNode(
                                    "Tag",
                                    new TagVO(tag.getSid(), "", "", tag.getTagName(), tag.getSeq()),
                                    menuNode);
                        } else {
                            if (!menuNode.getChildren().stream()
                                    .map(TreeNode::getData)
                                    .collect(Collectors.toList())
                                    .contains(new TagVO(tag.getSid(), "", "", tag.getTagName(),
                                            tag.getSeq()))) {
                                // 判斷設定權限
                                if (setPermissionFlag) {
                                    List<WCSetPermission> tagPermissionList = permissionMap.get(
                                            TargetType.TAG.name().concat(tag.getSid()));
                                    WCSetPermission tagPermission = WkStringUtils.notEmpty(tagPermissionList)
                                            ? tagPermissionList.get(0)
                                            : null;
                                    if (tagPermission != null
                                            && !tagPermission.getPermissionGroups().isEmpty()) {
                                        List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                                                .groupPermissionUserSids(
                                                        loginCompSid,
                                                        tagPermission.getPermissionGroups());
                                        if (!permissionUserSids.contains(
                                                SecurityFacade.getUserSid())) {
                                            continue;
                                        }
                                    }
                                }
                                new CheckboxTreeNode(
                                        "Tag",
                                        new TagVO(tag.getSid(), "", "", tag.getTagName(),
                                                tag.getSeq()),
                                        menuNode);
                            }
                        }
                    }
                }
            }
        }
    }

    public void loadSelTagTree(List<TreeNode> selNode) {
        this.checkAll = true;
        this.selNode = Lists.newArrayList();
        expandAll(rootNode, false, false);
        List<TreeNode> allNodes = Lists.newArrayList();
        storeAllSubNodes(allNodes, rootNode);
        for (TreeNode tn : allNodes) {
            if (selNode.contains(tn)) {
                tn.setSelected(true);
                this.selNode.add(tn);
                if (tn.getChildren() != null && tn.getChildren().size() > 0) {
                    for (TreeNode trn : tn.getChildren()) {
                        expandToParent(trn);
                    }
                } else {
                    expandToParent(tn);
                }
            } else if (checkAll) {
                this.checkAll = false;
            }
        }
    }

    public void initSearchTree(
            Integer loginCompSid, Boolean status, CheckAuthoritySearchQuery query) {
        rootNode = new CheckboxTreeNode();
        buildTagSearchTree(loginCompSid, status ? null : Activation.ACTIVE, query);
    }

    /**
     * 建立Tag樹
     *
     * @param loginCompSid 公司Sid
     * @param status       狀態
     * @param query        查詢條件
     */
    protected void buildTagSearchTree(
            Integer loginCompSid, Activation status, CheckAuthoritySearchQuery query) {
        // query 選取物件前置處理(將父節點納入選取範圍)
        List<TreeNode> parentIncludeSelNode = Lists.newArrayList();
        for (TreeNode node : query.getSelNode()) {
            parentIncludeSelNode.add(node);
            while (node.getParent() != null) {
                parentIncludeSelNode.add(node.getParent());
                node = node.getParent();
            }
        }

        // 準備查詢條件物件
        List<String> multiCategoryTagList = parentIncludeSelNode.stream()
                .filter(item -> "CategoryTag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());
        List<String> multiMenuTagList = parentIncludeSelNode.stream()
                .filter(item -> "MenuTag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());
        List<String> multiTagList = parentIncludeSelNode.stream()
                .filter(item -> "Tag".equals(item.getType()))
                .map(item -> ((TagVO) item.getData()).getSid())
                .collect(Collectors.toList());

        // 準備權限判斷物件
        List<TargetType> targetTypes = Lists.newArrayList(
                TargetType.CATEGORY_TAG, TargetType.MENU_TAG, TargetType.TAG, TargetType.EXEC_DEP);
        List<WCSetPermission> permissionAll = WCSetPermissionManager.getInstance()
                .findByCompSidAndTargetTypeIn(loginCompSid, targetTypes);
        Map<String, List<WCSetPermission>> permissionMap = permissionAll.stream()
                .collect(
                        Collectors.groupingBy(
                                each -> each.getTargetType().name().concat(each.getTargetSid())));

        TreeNode categroyNode = null, menuNode = null;
        List<WCCategoryTag> categoryTagList = WCCategoryTagLogicComponent.getInstance().findAll(status, loginCompSid);
        for (WCCategoryTag categoryTag : categoryTagList) {
            // 判斷設定權限
            List<WCSetPermission> categoryPermissionList = permissionMap.get(TargetType.CATEGORY_TAG.name().concat(categoryTag.getSid()));
            WCSetPermission categoryPermission = WkStringUtils.notEmpty(categoryPermissionList) ? categoryPermissionList.get(0)
                    : null;
            if (categoryPermission != null && !categoryPermission.getPermissionGroups().isEmpty()) {
                List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                        .groupPermissionUserSids(loginCompSid,
                                categoryPermission.getPermissionGroups());
                if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                    continue;
                }
            }

            // 判斷查詢條件(Tag)
            boolean isCategoryTag = multiCategoryTagList.isEmpty() || multiCategoryTagList.contains(
                    categoryTag.getSid());
            boolean isHaveCategoryTag = categoryTag.getSid().contains(query.getCategoryTagId());
            if (!(isHaveCategoryTag && isCategoryTag)) {
                continue;
            }

            List<MenuTagSearchVO> menuTagSearchVOs = WCMenuTagLogicComponent.getInstance()
                    .getMenuTagSearchVO(categoryTag.getSid(), status);
            List<WCMenuTag> menuTagList = WCMenuTagLogicComponent.getInstance()
                    .getWCMenuTagByCategorySid(categoryTag.getSid(), status);
            for (WCMenuTag menuTag : menuTagList) {
                // 判斷設定權限
                List<WCSetPermission> menuPermissionList = permissionMap.get(TargetType.MENU_TAG.name().concat(menuTag.getSid()));
                WCSetPermission menuPermission = WkStringUtils.notEmpty(menuPermissionList) ? menuPermissionList.get(0) : null;
                if (menuPermission != null && !menuPermission.getPermissionGroups().isEmpty()) {
                    List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                            .groupPermissionUserSids(loginCompSid,
                                    menuPermission.getPermissionGroups());
                    if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                        continue;
                    }
                }

                // 判斷查詢條件(Tag)
                boolean isMenuTag = multiMenuTagList.isEmpty() || multiMenuTagList.contains(menuTag.getSid());
                boolean isHaveMenuTag = menuTag.getSid().contains(query.getMenuTagId());
                if (!(isHaveMenuTag && isMenuTag)) {
                    continue;
                }

                // 判斷查詢條件(可使用單位)
                boolean isHaveMenuTagUse = query.getCanUseOrg().isEmpty() || query.getCanUseOrg().stream()
                        .anyMatch(
                                item -> {
                                    List<Integer> useDepSid = this.getUseDepSids(menuTag);
                                    return useDepSid == null
                                            || useDepSid.isEmpty()
                                            || useDepSid.contains(item.getSid());
                                });
                if (!isHaveMenuTagUse) {
                    continue;
                }

                // 判斷查詢條件(可使用人員)
                boolean isHaveUseUser = query.getUseUsers().isEmpty() || query.getUseUsers().stream()
                        .anyMatch(
                                item -> {
                                    List<Integer> useUserSids = Lists.newArrayList();
                                    if (menuTag.getUseUser() != null
                                            && menuTag.getUseUser().getUserTos() != null) {
                                        menuTag
                                                .getUseUser()
                                                .getUserTos()
                                                .forEach(
                                                        subItem -> {
                                                            useUserSids.add(subItem.getSid());
                                                        });
                                    }
                                    return useUserSids != null && useUserSids.contains(item.getSid());
                                });
                if (!isHaveUseUser) {
                    continue;
                }

                int menuIndex = menuTagSearchVOs.indexOf(new MenuTagSearchVO(menuTag.getSid()));
                MenuTagSearchVO menuTagSearchVO = new MenuTagSearchVO(menuTag.getSid());
                if (menuIndex != -1) {
                    menuTagSearchVO = menuTagSearchVOs.get(menuIndex);
                }
                List<TagSearchVO> subTagSearchVOs = WCTagLogicComponents.getInstance().getTagSearch(null, menuTag.getSid(), false);
                List<WCTag> tagList = WCTagLogicComponents.getInstance().getWCTagByMenuSid(menuTag.getSid(), status);
                for (WCTag tag : tagList) {
                    // 判斷設定權限
                    List<WCSetPermission> tagPermissionList = permissionMap.get(TargetType.TAG.name().concat(tag.getSid()));
                    WCSetPermission tagPermission = WkStringUtils.notEmpty(tagPermissionList) ? tagPermissionList.get(0) : null;
                    if (tagPermission != null && !tagPermission.getPermissionGroups().isEmpty()) {
                        List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                                .groupPermissionUserSids(loginCompSid,
                                        tagPermission.getPermissionGroups());
                        if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                            continue;
                        }
                    }

                    // 判斷查詢條件(Tag)
                    boolean isTag = multiTagList.isEmpty() || multiTagList.contains(tag.getSid());
                    boolean isHaveSubTag = tag.getSid().contains(query.getSubTagId());
                    if (!(isHaveSubTag && isTag)) {
                        continue;
                    }

                    // 判斷查詢條件(單位)
                    // 可使用單位
                    boolean isHaveSubTagUse = query.getCanUseOrg().isEmpty() || query.getCanUseOrg().stream()
                            .anyMatch(
                                    item -> {
                                        List<Integer> useDepSid = this.getUseDepSids(tag);
                                        return useDepSid == null
                                                || useDepSid.isEmpty()
                                                || useDepSid.contains(item.getSid());
                                    });
                    // 可閱部門
                    boolean isHaveViewDep = query.getViewOrgs().isEmpty() || query.getViewOrgs().stream()
                            .anyMatch(
                                    item -> {
                                        List<Integer> readDepSid = this.getReadDepSids(tag);
                                        return readDepSid != null
                                                && !readDepSid.isEmpty()
                                                && readDepSid.contains(item.getSid());
                                    });
                    // 相關單位
                    boolean isRelateDep = query.getRelatedOrg().isEmpty() || query.getRelatedOrg().stream()
                            .anyMatch(
                                    item -> {
                                        List<Integer> useDepSid = this.getUseDepSids(tag);
                                        return useDepSid == null
                                                || useDepSid.isEmpty()
                                                || useDepSid.contains(item.getSid());
                                    })
                            || query.getRelatedOrg().stream()
                                    .anyMatch(
                                            item -> {
                                                List<Integer> readDepSid = this.getReadDepSids(tag);
                                                return readDepSid != null
                                                        && !readDepSid.isEmpty()
                                                        && readDepSid.contains(item.getSid());
                                            });
                    if (!(isHaveSubTagUse && isHaveViewDep && isRelateDep)) {
                        continue;
                    }

                    // 判斷查詢條件(人員)
                    // 可閱人員
                    boolean isHaveViewUser = query.getViewUsers().isEmpty() || query.getViewUsers().stream()
                            .anyMatch(
                                    item -> {
                                        List<Integer> viewUserSids = Lists.newArrayList();
                                        if (tag.getUseUser() != null
                                                && tag.getUseUser().getUserTos() != null) {
                                            tag.getUseUser()
                                                    .getUserTos()
                                                    .forEach(
                                                            subItem -> {
                                                                viewUserSids.add(subItem.getSid());
                                                            });
                                        }
                                        return viewUserSids != null && viewUserSids.contains(
                                                item.getSid());
                                    });
                    if (!isHaveViewUser) {
                        continue;
                    }

                    // 判斷查詢條件(申請流程類型)
                    boolean isHaveFlowType = query.getFlowType() == null || (tag.getReqFlowType() != null
                            && tag.getReqFlowType().equals(query.getFlowType()));
                    if (!isHaveFlowType) {
                        continue;
                    }

                    int tagIndex = subTagSearchVOs.indexOf(new TagSearchVO(tag.getSid()));
                    TagSearchVO subTagSearchVO = new TagSearchVO(tag.getSid());
                    if (tagIndex != -1) {
                        subTagSearchVO = subTagSearchVOs.get(tagIndex);
                    }
                    List<ExecDepSettingSearchVO> execDepSettingSearchVOs = WCExecDepSettingLogicComponent.getInstance()
                            .getExecDepSettingSearchVO(tag.getSid());
                    List<WCExecDepSetting> execDepSettingList = WCExecDepSettingLogicComponent.getInstance()
                            .getWCExecDepSettingByWcTagSid(tag.getSid(), Activation.ACTIVE);
                    if (execDepSettingList.isEmpty()) {
                        if (!rootNode.getChildren().stream()
                                .map(TreeNode::getData)
                                .collect(Collectors.toList())
                                .contains(
                                        new TagVO(
                                                categoryTag.getSid(),
                                                categoryTag.getCategoryName(),
                                                "",
                                                "",
                                                categoryTag.getSeq()))) {
                            categroyNode = new CheckboxTreeNode(
                                    "CategoryTag",
                                    new TagVO(
                                            categoryTag.getSid(),
                                            categoryTag.getCategoryName(),
                                            "",
                                            "",
                                            categoryTag.getSeq(),
                                            categoryTag.getStatus()),
                                    rootNode);
                            menuNode = new CheckboxTreeNode(
                                    "MenuTag",
                                    new TagVO(
                                            menuTag.getSid(),
                                            "",
                                            menuTag.getMenuName(),
                                            "",
                                            menuTag.getSeq(),
                                            menuTag.getStatus(),
                                            menuTagSearchVO,
                                            null,
                                            null,
                                            categoryTag.getSid(),
                                            menuTag.getSid()),
                                    categroyNode);
                            new CheckboxTreeNode(
                                    "Tag",
                                    new TagVO(
                                            tag.getSid(),
                                            "",
                                            "",
                                            tag.getTagName(),
                                            tag.getSeq(),
                                            tag.getStatus(),
                                            null,
                                            subTagSearchVO,
                                            null,
                                            categoryTag.getSid(),
                                            menuTag.getSid()),
                                    menuNode);
                        } else {
                            if (!categroyNode.getChildren().stream()
                                    .map(TreeNode::getData)
                                    .collect(Collectors.toList())
                                    .contains(
                                            new TagVO(
                                                    menuTag.getSid(), "", menuTag.getMenuName(), "",
                                                    menuTag.getSeq()))) {
                                menuNode = new CheckboxTreeNode(
                                        "MenuTag",
                                        new TagVO(
                                                menuTag.getSid(),
                                                "",
                                                menuTag.getMenuName(),
                                                "",
                                                menuTag.getSeq(),
                                                menuTag.getStatus(),
                                                menuTagSearchVO,
                                                null,
                                                null,
                                                categoryTag.getSid(),
                                                menuTag.getSid()),
                                        categroyNode);
                                new CheckboxTreeNode(
                                        "Tag",
                                        new TagVO(
                                                tag.getSid(),
                                                "",
                                                "",
                                                tag.getTagName(),
                                                tag.getSeq(),
                                                tag.getStatus(),
                                                null,
                                                subTagSearchVO,
                                                null,
                                                categoryTag.getSid(),
                                                menuTag.getSid()),
                                        menuNode);
                            } else {
                                if (!menuNode.getChildren().stream()
                                        .map(TreeNode::getData)
                                        .collect(Collectors.toList())
                                        .contains(new TagVO(tag.getSid(), "", "", tag.getTagName(),
                                                tag.getSeq()))) {
                                    new CheckboxTreeNode(
                                            "Tag",
                                            new TagVO(
                                                    tag.getSid(),
                                                    "",
                                                    "",
                                                    tag.getTagName(),
                                                    tag.getSeq(),
                                                    tag.getStatus(),
                                                    null,
                                                    subTagSearchVO,
                                                    null,
                                                    categoryTag.getSid(),
                                                    menuTag.getSid()),
                                            menuNode);
                                }
                            }
                        }
                    } else {
                        for (WCExecDepSetting execDepSetting : execDepSettingList) {
                            // 判斷查詢條件(單位)
                            // 執行單位
                            boolean isHaveExec = query.getExecOrg().isEmpty() || query.getExecOrg().stream()
                                    .anyMatch(
                                            item -> execDepSetting.getExec_dep_sid() != null
                                                    && execDepSetting.getExec_dep_sid()
                                                            .equals(item.getSid()));
                            // 指派單位
                            boolean isHaveTransDep = query.getTransOrgs().isEmpty() || query.getTransOrgs().stream()
                                    .anyMatch(
                                            item -> {
                                                List<Integer> assignDepSids = Lists.newArrayList();
                                                if (execDepSetting.getTransdep() != null
                                                        && execDepSetting.getTransdep().getUserDepTos() != null) {
                                                    execDepSetting
                                                            .getTransdep()
                                                            .getUserDepTos()
                                                            .forEach(
                                                                    subItem -> {
                                                                        assignDepSids.add(Integer.valueOf(
                                                                                subItem.getDepSid()));
                                                                    });
                                                }
                                                return assignDepSids != null
                                                        && assignDepSids.contains(item.getSid());
                                            });
                            // 相關單位
                            isRelateDep = query.getRelatedOrg().isEmpty() || query.getRelatedOrg().stream()
                                    .anyMatch(
                                            item -> {
                                                List<Integer> useDepSid = this.getUseDepSids(tag);
                                                return useDepSid == null
                                                        || useDepSid.isEmpty()
                                                        || useDepSid.contains(item.getSid());
                                            })
                                    || query.getRelatedOrg().stream()
                                            .anyMatch(
                                                    item -> execDepSetting.getExec_dep_sid() != null
                                                            && execDepSetting.getExec_dep_sid()
                                                                    .equals(item.getSid()))
                                    || query.getRelatedOrg().stream()
                                            .anyMatch(
                                                    item -> {
                                                        List<Integer> readDepSid = this.getReadDepSids(tag);
                                                        return readDepSid != null
                                                                && !readDepSid.isEmpty()
                                                                && readDepSid.contains(item.getSid());
                                                    })
                                    || query.getRelatedOrg().stream()
                                            .anyMatch(
                                                    item -> {
                                                        List<Integer> assignDepSids = Lists.newArrayList();
                                                        if (execDepSetting.getTransdep() != null
                                                                && execDepSetting.getTransdep().getUserDepTos() != null) {
                                                            execDepSetting
                                                                    .getTransdep()
                                                                    .getUserDepTos()
                                                                    .forEach(
                                                                            subItem -> {
                                                                                assignDepSids.add(
                                                                                        Integer.valueOf(
                                                                                                subItem.getDepSid()));
                                                                            });
                                                        }
                                                        return assignDepSids != null
                                                                && assignDepSids.contains(item.getSid());
                                                    });
                            if (!(isHaveSubTagUse
                                    && isHaveViewDep
                                    && isHaveExec
                                    && isHaveTransDep
                                    && isRelateDep)) {
                                continue;
                            }

                            // 指派人員
                            boolean isHaveAssignUser = query.getAssignUser().isEmpty()
                                    || query.getAssignUser().stream()
                                            .anyMatch(item -> {
                                                if (execDepSetting.isNeedAssigned()
                                                        && execDepSetting.isReadAssignSetting()) {
                                                    return execDepSetting.getTransUserSids().contains(item.getSid());
                                                }
                                                return false;
                                            });
                            // 執行單位簽核人員
                            boolean isHaveExecUser = query.getExecSignUser().isEmpty() || query.getExecSignUser()
                                    .stream()
                                    .anyMatch(
                                            item -> {
                                                List<Integer> execSignUserSids = Lists.newArrayList();
                                                if (execDepSetting.getExecSignMember() != null
                                                        && execDepSetting.getExecSignMember().getUserTos() != null) {
                                                    execDepSetting
                                                            .getExecSignMember()
                                                            .getUserTos()
                                                            .forEach(
                                                                    subItem -> {
                                                                        execSignUserSids.add(subItem.getSid());
                                                                    });
                                                }
                                                return execSignUserSids != null
                                                        && execSignUserSids.contains(item.getSid());
                                            });
                            if (!(isHaveAssignUser && isHaveExecUser)) {
                                continue;
                            }

                            // 判斷查詢條件(執行模式)
                            boolean isHaveModeType = query.getExecMode().equals("")
                                    || query.getExecMode().equals("派工") == execDepSetting.isNeedAssigned();
                            if (!isHaveModeType) {
                                continue;
                            }

                            int execIndex = execDepSettingSearchVOs.indexOf(
                                    new ExecDepSettingSearchVO(execDepSetting.getSid()));
                            ExecDepSettingSearchVO execDepSettingSearchVO = new ExecDepSettingSearchVO(execDepSetting.getSid());
                            if (execIndex != -1) {
                                execDepSettingSearchVO = execDepSettingSearchVOs.get(execIndex);
                            }
                            if (!rootNode.getChildren().stream()
                                    .map(TreeNode::getData)
                                    .collect(Collectors.toList())
                                    .contains(
                                            new TagVO(
                                                    categoryTag.getSid(),
                                                    categoryTag.getCategoryName(),
                                                    "",
                                                    "",
                                                    categoryTag.getSeq()))) {
                                categroyNode = new CheckboxTreeNode(
                                        "CategoryTag",
                                        new TagVO(
                                                categoryTag.getSid(),
                                                categoryTag.getCategoryName(),
                                                "",
                                                "",
                                                categoryTag.getSeq(),
                                                categoryTag.getStatus()),
                                        rootNode);
                                menuNode = new CheckboxTreeNode(
                                        "MenuTag",
                                        new TagVO(
                                                menuTag.getSid(),
                                                "",
                                                menuTag.getMenuName(),
                                                "",
                                                menuTag.getSeq(),
                                                menuTag.getStatus(),
                                                menuTagSearchVO,
                                                null,
                                                null,
                                                categoryTag.getSid(),
                                                menuTag.getSid()),
                                        categroyNode);
                                new CheckboxTreeNode(
                                        "Tag",
                                        new TagVO(
                                                tag.getSid(),
                                                "",
                                                "",
                                                tag.getTagName(),
                                                tag.getSeq(),
                                                tag.getStatus(),
                                                null,
                                                subTagSearchVO,
                                                execDepSettingSearchVO,
                                                categoryTag.getSid(),
                                                menuTag.getSid()),
                                        menuNode);
                            } else {
                                if (!categroyNode.getChildren().stream()
                                        .map(TreeNode::getData)
                                        .collect(Collectors.toList())
                                        .contains(
                                                new TagVO(
                                                        menuTag.getSid(), "", menuTag.getMenuName(), "",
                                                        menuTag.getSeq()))) {
                                    menuNode = new CheckboxTreeNode(
                                            "MenuTag",
                                            new TagVO(
                                                    menuTag.getSid(),
                                                    "",
                                                    menuTag.getMenuName(),
                                                    "",
                                                    menuTag.getSeq(),
                                                    menuTag.getStatus(),
                                                    menuTagSearchVO,
                                                    null,
                                                    null,
                                                    categoryTag.getSid(),
                                                    menuTag.getSid()),
                                            categroyNode);
                                    new CheckboxTreeNode(
                                            "Tag",
                                            new TagVO(
                                                    tag.getSid(),
                                                    "",
                                                    "",
                                                    tag.getTagName(),
                                                    tag.getSeq(),
                                                    tag.getStatus(),
                                                    null,
                                                    subTagSearchVO,
                                                    execDepSettingSearchVO,
                                                    categoryTag.getSid(),
                                                    menuTag.getSid()),
                                            menuNode);
                                } else {
                                    if (!menuNode.getChildren().stream()
                                            .map(TreeNode::getData)
                                            .collect(Collectors.toList())
                                            .contains(new TagVO(tag.getSid(), "", "", tag.getTagName(),
                                                    tag.getSeq()))) {
                                        new CheckboxTreeNode(
                                                "Tag",
                                                new TagVO(
                                                        tag.getSid(),
                                                        "",
                                                        "",
                                                        tag.getTagName(),
                                                        tag.getSeq(),
                                                        tag.getStatus(),
                                                        null,
                                                        subTagSearchVO,
                                                        execDepSettingSearchVO,
                                                        categoryTag.getSid(),
                                                        menuTag.getSid()),
                                                menuNode);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 抓取可使用單位
     *
     * @param tagEntity
     * @return
     */
    private List<Integer> getUseDepSids(AbstractEo tagEntity) {
        List<Integer> orgSids = Lists.newArrayList();
        WCConfigTempletVo templet = null;
        Set<Integer> customDepSids = Sets.newHashSet();
        boolean isUserContainFollowing = false;

        // ====================================
        // 收集設定
        // ====================================
        // 中類
        if (tagEntity instanceof WCMenuTag) {
            WCMenuTag wcMenuTag = (WCMenuTag) tagEntity;
            templet = WCConfigTempletManager.getInstance().findBySid(wcMenuTag.getUseDepTempletSid());
            if (templet == null) {
                customDepSids = UserDep.parserDepSids(wcMenuTag.getUseDep());
                isUserContainFollowing = wcMenuTag.isUserContainFollowing();
            }
        }
        // 小類
        if (tagEntity instanceof WCTag) {
            WCTag wcTag = (WCTag) tagEntity;
            templet = WCConfigTempletManager.getInstance().findBySid(wcTag.getCanUserDepTempletSid());
            if (templet == null) {
                customDepSids = UserDep.parserDepSids(wcTag.getCanUserDep());
                isUserContainFollowing = wcTag.isUserContainFollowing();
            }
        }

        if (templet != null && ConfigTempletType.SUB_TAG_CAN_USE.equals(templet.getTempletType())) {
            orgSids = WCConfigTempletManager.getInstance().getTempletDeps(templet.getSid(), templet.getCompSid());
        } else if (templet != null
                && ConfigTempletType.MENU_TAG_CAN_USE.equals(templet.getTempletType())) {
            orgSids = WCConfigTempletManager.getInstance().getTempletDeps(templet.getSid(), templet.getCompSid());
        } else {

            // 含以下
            if (isUserContainFollowing) {
                Set<Integer> childSids = WkOrgCache.getInstance().findAllChildSids(customDepSids);
                if (WkStringUtils.isEmpty(childSids)) {
                    customDepSids.addAll(childSids);
                }
            }
            orgSids = Lists.newArrayList(customDepSids);
        }

        // 去除停用單位
        orgSids = orgSids.stream()
                .filter(orgSid -> WkOrgUtils.isActive(orgSid))
                .collect(Collectors.toList());

        return orgSids;
    }

    /**
     * 抓取可閱部門
     *
     * @param obj
     * @return
     */
    private List<Integer> getReadDepSids(WCTag wcTag) {
        List<Integer> orgSids = Lists.newArrayList();

        WCConfigTempletVo templet = WCConfigTempletManager.getInstance().findBySid(wcTag.getUseDepTempletSid());
        if (templet != null && ConfigTempletType.SUB_TAG_CAN_READ.equals(templet.getTempletType())) {
            // 讀取模版
            orgSids = WCConfigTempletManager.getInstance().getTempletDeps(templet.getSid(), templet.getCompSid());
        } else {

            // 自行設定
            Set<Integer> customDepSid = UserDep.parserDepSids(wcTag.getUseDep());
            // 含以下
            if (wcTag.isViewContainFollowing()) {
                Set<Integer> childSids = WkOrgCache.getInstance().findAllChildSids(customDepSid);
                if (WkStringUtils.isEmpty(childSids)) {
                    customDepSid.addAll(childSids);
                }
            }
            orgSids = Lists.newArrayList(customDepSid);
        }

        // 去除停用單位
        orgSids = orgSids.stream()
                .filter(orgSid -> WkOrgUtils.isActive(orgSid))
                .collect(Collectors.toList());

        return orgSids;
    }
}
