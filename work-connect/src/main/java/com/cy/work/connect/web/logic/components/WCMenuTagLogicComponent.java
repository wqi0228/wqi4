/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.WCCategoryTagManager;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.view.vo.ManagerMenuVO;
import com.cy.work.connect.web.view.vo.MenuTagEditVO;
import com.cy.work.connect.web.view.vo.MenuTagSearchVO;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCMenuTagLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6756941700261770783L;

    private static WCMenuTagLogicComponent instance;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private WCCategoryTagManager wcCategoryTagManager;
    @Autowired
    private WCConfigTempletManager wcConfigTempletManager;
    @Autowired
    private WCTagManager wcTagManager;

    public static WCMenuTagLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMenuTagLogicComponent.instance = this;
    }

    /**
     * 取得類別選單List
     *
     * @param wcCategoryTagSid 類別Sid
     * @return
     */
    public List<WCMenuTag> getWCMenuTagByCategorySid(String wcCategoryTagSid,
            Activation activation) {
        List<WCMenuTag> results = Lists.newArrayList();
        if (wcCategoryTagSid == null) {
            return results;
        }
        try {
            results = wcMenuTagManager.getWCMenuTagByCategorySid(wcCategoryTagSid, activation);
        } catch (Exception ex) {
            log.warn("getWCMenuTagByCategorySid ERROR", ex);
        }
        return results;
    }

    /**
     * find MenuTagVO By Sid
     *
     * @param sid
     * @return
     */
    public MenuTagVO findToBySid(String sid) {
        WCMenuTag obj = wcMenuTagManager.getWCMenuTagBySid(sid);
        if (obj == null) {
            return null;
        }
        return new MenuTagVO(
                obj.getWcCategoryTagSid(),
                obj.getSid(),
                obj.getMenuName(),
                obj.getItemName(),
                obj.getDefaultTheme(),
                obj.getDefaultContent(),
                obj.getDefaultMemo());
    }

    /**
     * find WCMenuTag By Sid
     *
     * @param sid
     * @return
     */
    public WCMenuTag getWCMenuTagBySid(String sid) {
        if (sid == null) {
            return null;
        }
        return wcMenuTagManager.getWCMenuTagBySid(sid);
    }

    /**
     * 儲存類別選單
     *
     * @param sid
     * @param categorySid
     * @param selOrgs
     * @param useUsers
     * @param name
     * @param itemName
     * @param memo
     * @param status
     * @param defaultTheme
     * @param defaultContent
     * @param defaultMemo
     * @param toParentManager
     * @param loginUserSid
     * @param isUserContainFollowing
     * @param reqFinalApplyUserSid
     * @param uesDepTempletSid
     * @param reqFinalApplyIgnoredWcTagSids
     */
    public void saveMenuTag(
            String sid,
            String categorySid,
            List<Org> selOrgs,
            List<User> useUsers,
            String name,
            String itemName,
            String memo,
            String status,
            // String seq,
            String defaultTheme,
            String defaultContent,
            String defaultMemo,
            boolean toParentManager,
            Integer loginUserSid,
            boolean isUserContainFollowing,
            Integer reqFinalApplyUserSid,
            Long uesDepTempletSid,
            List<String> reqFinalApplyIgnoredWcTagSids,
            boolean onlyForUseDepManager) {

        if (Strings.isNullOrEmpty(name)) {
            Preconditions.checkState(false, "單據名稱為必要輸入欄位，請確認！！");
        }
        if (Strings.isNullOrEmpty(itemName)) {
            Preconditions.checkState(false, "執行項目顯示標題為必要輸入欄位，請確認！！");
        }
        // if (Strings.isNullOrEmpty(seq)) {
        // Preconditions.checkState(false, "排序序號為必要輸入欄位，請確認！！");
        // }
        // try {
        // Integer.valueOf(seq);
        // } catch (Exception e) {
        // log.warn("saveCategoryTag ERROR", e);
        // Preconditions.checkState(false, "排序序號必須為數字，請確認！！");
        // }
        for (WCMenuTag wt : wcMenuTagManager.getWCMenuTagByCategorySid(categorySid, Activation.ACTIVE)) {
            if (!wt.getSid().equals(sid)) {
                if (wt.getMenuName().equals(name)) {
                    Preconditions.checkState(false, "該名稱已存在，請確認！！");
                }
                // if (wt.getSeq().equals(Integer.valueOf(seq))) {
                // Preconditions.checkState(false, "該序號已被[" + wt.getMenuName() + "]使用，請確認！！");
                // }
            }
        }
        // 取得類別選單設定
        WCMenuTag wcMenuTag = null;
        if (Strings.isNullOrEmpty(sid)) {
            // 為新增
            wcMenuTag = new WCMenuTag();
            wcMenuTag.setWcCategoryTagSid(categorySid);
        } else {
            // 已存在
            wcMenuTag = wcMenuTagManager.getWCMenuTagBySid(sid);
        }
        // 整理可使用單位
        UserDep userDep = new UserDep();
        List<UserDepTo> userDepTos = Lists.newArrayList();
        selOrgs.forEach(
                item -> {
                    UserDepTo udt = new UserDepTo();
                    udt.setDepSid(String.valueOf(item.getSid()));
                    userDepTos.add(udt);
                });
        userDep.setUserDepTos(userDepTos);

        // 整理可使用人員
        UsersTo useUser = new UsersTo();
        List<UserTo> useUserTos = Lists.newArrayList();
        if (useUsers != null) {
            useUsers.forEach(
                    item -> {
                        UserTo udt = new UserTo();
                        udt.setSid(item.getSid());
                        useUserTos.add(udt);
                    });
        }
        useUser.setUserTos(useUserTos);

        // 可使用單位
        wcMenuTag.setUseDep(userDep);
        // 可使用人員
        wcMenuTag.setUseUser(useUser);
        // 類別選單名稱
        wcMenuTag.setMenuName(name);
        // 類別選單主檔顯示名稱
        wcMenuTag.setItemName(itemName);
        // 備註
        wcMenuTag.setMemo(memo);
        // wcMenuTag.setReqFlowType(FlowType.valueOf(reqFlowType));
        // 使用狀態
        wcMenuTag.setStatus(Activation.valueOf(status));

        // 預設主題
        wcMenuTag.setDefaultTheme(defaultTheme);
        // 預設內容
        wcMenuTag.setDefaultContent(defaultContent);
        // 預設備住
        wcMenuTag.setDefaultMemo(defaultMemo);
        // 強制簽核至上層主管
        wcMenuTag.setParentManager(toParentManager);
        // 可使用單位含以下單位
        wcMenuTag.setUserContainFollowing(isUserContainFollowing);
        // 僅主管可使用
        wcMenuTag.setOnlyForUseDepManager(onlyForUseDepManager);
        // 需求方最終簽核人員
        wcMenuTag.setReqFinalApplyUserSid(reqFinalApplyUserSid);
        // 可使用單位模版
        wcMenuTag.setUseDepTempletSid(uesDepTempletSid);
        //
        if (uesDepTempletSid != null) {
            wcMenuTag.setUserContainFollowing(false);
            wcMenuTag.setOnlyForUseDepManager(false);
        }

        if (Strings.isNullOrEmpty(sid)) {
            wcMenuTag = wcMenuTagManager.createWCMenuTag(wcMenuTag, loginUserSid);
        } else {
            wcMenuTag = wcMenuTagManager.updateWCMenuTag(wcMenuTag, loginUserSid);
        }

        // 最終簽核排除項目（是否忽略「需求方最終簽核人員」設定）
        List<WCTag> wcTags = wcTagManager.getWCTagForManager(wcMenuTag.getSid(), Activation.ACTIVE);
        for (WCTag wcTag : wcTags) {
            boolean reqFinalApplyIgnored = reqFinalApplyIgnoredWcTagSids.contains(wcTag.getSid());
            if (reqFinalApplyIgnored != wcTag.isReqFinalApplyIgnored()) {
                wcTag.setReqFinalApplyIgnored(reqFinalApplyIgnored);
                wcTagManager.updateWCTag(wcTag, loginUserSid);
            }
        }
    }

    /**
     * 取得可使用人員
     *
     * @param menuTagSid 選單TagSid
     * @return
     */
    public List<UserViewVO> getUseUsersByMenuTagSid(String menuTagSid) {
        List<UserViewVO> results = Lists.newArrayList();
        try {
            WCMenuTag wcMenuTag = wcMenuTagManager.getWCMenuTagBySid(menuTagSid);
            if (wcMenuTag.getUseUser() != null) {
                wcMenuTag
                        .getUseUser()
                        .getUserTos()
                        .forEach(
                                item -> {
                                    results.add(new UserViewVO(item.getSid()));
                                });
            }
        } catch (Exception e) {
            log.warn("getUseUsersByMenuTagSid ERROR", e);
        }
        return results;
    }

    /**
     * @param sid
     * @return
     */
    public MenuTagEditVO getMenuTagEditVO(String sid) {
        WCMenuTag wcMenuTag = wcMenuTagManager.getWCMenuTagBySid(sid);
        MenuTagEditVO menuTagEditVO = new MenuTagEditVO();
        menuTagEditVO.setSid(sid);
        menuTagEditVO.setName(wcMenuTag.getMenuName());
        menuTagEditVO.setItemName(wcMenuTag.getItemName());
        menuTagEditVO.setMemo(wcMenuTag.getMemo());
        menuTagEditVO.setStatus(wcMenuTag.getStatus().name());
        menuTagEditVO.setSeq(String.valueOf(wcMenuTag.getSeq()));
        menuTagEditVO.setDefaultTheme(wcMenuTag.getDefaultTheme());
        menuTagEditVO.setDefaultContent(wcMenuTag.getDefaultContent());
        menuTagEditVO.setDefaultMemo(wcMenuTag.getDefaultMemo());
        if (wcMenuTag.getUseDep() != null && wcMenuTag.getUseDep().getUserDepTos() != null) {
            wcMenuTag
                    .getUseDep()
                    .getUserDepTos()
                    .forEach(
                            item -> {
                                menuTagEditVO
                                        .getOrgs()
                                        .add(WkOrgCache.getInstance()
                                                .findBySid(Integer.valueOf(item.getDepSid())));
                            });
        }
        if (wcMenuTag.getUseUser() != null && wcMenuTag.getUseUser().getUserTos() != null) {
            wcMenuTag
                    .getUseUser()
                    .getUserTos()
                    .forEach(
                            item -> {
                                menuTagEditVO.getUsers().add(userManager.findBySid(item.getSid()));
                            });
        }
        menuTagEditVO.setAttachmentType(wcMenuTag.isAttachmentType());
        menuTagEditVO.setAttachmentPath(wcMenuTag.getAttachmentPath());
        menuTagEditVO.setToParentManager(wcMenuTag.isParentManager());
        menuTagEditVO.setUserContainFollowing(wcMenuTag.isUserContainFollowing());
        // 僅主管可使用
        menuTagEditVO.setOnlyForUseDepManager(wcMenuTag.isOnlyForUseDepManager());

        // 最終需求需求方簽核人員
        menuTagEditVO.setReqFinalApplyUserSid(wcMenuTag.getReqFinalApplyUserSid());
        menuTagEditVO.setReqFinalApplyUserDisplayContent(
                this.getReqFinalApplyUserDisplayContent(wcMenuTag.getReqFinalApplyUserSid()));
        // 可使用單位套用模版
        menuTagEditVO.setUesDepTempletSid(wcMenuTag.getUseDepTempletSid());

        // 最終簽核排除項目 (issue WORKCOMMU-356)
        List<WCTag> wcTags = wcTagManager.getWCTagForManager(sid, Activation.ACTIVE);
        List<String> reqFinalApplyIgnoredWcTagSids = wcTags.stream()
                .filter(WCTag::isReqFinalApplyIgnored)
                .map(WCTag::getSid)
                .collect(Collectors.toList());
        menuTagEditVO.setReqFinalApplyIgnoredWcTagSids(reqFinalApplyIgnoredWcTagSids);
        List<SelectItem> wcTagItems = wcTags.stream()
                .sorted(Comparator.comparing(WCTag::getSeq))
                .map(t -> new SelectItem(t.getSid(), t.getTagName()))
                .collect(Collectors.toList());
        menuTagEditVO.setWcTagItems(wcTagItems);

        return menuTagEditVO;
    }

    /**
     * 取得可使用部門-模版項目
     *
     * @return
     */
    public List<WCConfigTempletVo> getUseDepsTempletItems() {
        // 依據模版類型查詢
        return this.wcConfigTempletManager.queryByCondition(
                SecurityFacade.getUserSid(),
                ConfigTempletType.MENU_TAG_CAN_USE,
                null,
                Optional.ofNullable(WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId()))
                        .map(Org::getSid)
                        .orElseGet(() -> null));
    }

    /**
     * 取得 最終需求需求方簽核人員 的顯示文字
     *
     * @param reqFinalApplyUserSid
     * @return
     */
    public String getReqFinalApplyUserDisplayContent(Integer reqFinalApplyUserSid) {

        if (reqFinalApplyUserSid == null || reqFinalApplyUserSid == 0) {
            return "<span class='WS1-1-3'>未設定</span>";
        }

        User reqFinalApplyUser = userManager.findBySid(reqFinalApplyUserSid);
        if (reqFinalApplyUser == null) {
            return "<span class='WS1-1-2'>未找到使用者,UserSID:[" + reqFinalApplyUserSid + "]</span>";
        }

        if (!Activation.ACTIVE.equals(reqFinalApplyUser.getStatus())) {
            return "<span class='WS1-1-2'>該使用者已停用:[" + reqFinalApplyUser.getName() + "]</span>";
        }

        if (reqFinalApplyUser.getPrimaryOrg() != null) {
            Org dep = WkOrgCache.getInstance()
                    .findBySid(reqFinalApplyUser.getPrimaryOrg().getSid());
            if (dep != null) {
                return dep.getName()
                        + "-[<span class='WS1-1-3' style='text-decoration:underline;'>"
                        + reqFinalApplyUser.getName()
                        + "</span>]";
            }
        }

        return reqFinalApplyUser.getName();
    }

    public List<MenuTagSearchVO> getMenuTagSearchVO(
            String name, String categorySid, Activation activation) {
        List<MenuTagSearchVO> result = Lists.newArrayList();
        try {
            wcMenuTagManager.getWCMenuTagByCategorySid(categorySid, activation)
                    .forEach(
                            item -> {
                                if (!Strings.isNullOrEmpty(name)
                                        && !item.getMenuName().toLowerCase().contains(name.toLowerCase())) {
                                    return;
                                }
                                String statusStr = (Activation.ACTIVE.equals(item.getStatus())) ? "正常" : "停用";

                                MenuTagSearchVO menuTagSearchVO = new MenuTagSearchVO(
                                        item.getSid(),
                                        ToolsDate.transDateToString(
                                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                                item.getCreateDate()),
                                        WkUserUtils.findNameBySid(item.getCreateUser()),
                                        item.getMenuName(),
                                        statusStr,
                                        ToolsDate.transDateToString(
                                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                                item.getUpdateDate()),
                                        String.valueOf(item.getSeq()),
                                        item.isAttachmentType(),
                                        WkUserUtils.findNameBySid(item.getUpdateUser()),
                                        item.getUseDep(),
                                        item.isUserContainFollowing(),
                                        item.getUseDepTempletSid(),
                                        item.getUseUser());

                                result.add(menuTagSearchVO);
                            });
        } catch (Exception e) {
            log.warn("getMenuTagSearchVO ERROR", e);
        }
        return result;
    }

    public List<MenuTagSearchVO> getMenuTagSearchVO(String categorySid, Activation activation) {
        List<MenuTagSearchVO> result = Lists.newArrayList();
        try {
            wcMenuTagManager
                    .getWCMenuTagByCategorySid(categorySid, activation)
                    .forEach(
                            item -> {
                                String statusStr = (Activation.ACTIVE.equals(item.getStatus())) ? "顯示" : "不顯示";

                                MenuTagSearchVO menuTagSearchVO = new MenuTagSearchVO(
                                        item.getSid(),
                                        ToolsDate.transDateToString(
                                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                                item.getCreateDate()),
                                        WkUserUtils.findNameBySid(item.getCreateUser()),
                                        item.getMenuName(),
                                        statusStr,
                                        ToolsDate.transDateToString(
                                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                                item.getUpdateDate()),
                                        String.valueOf(item.getSeq()),
                                        item.isAttachmentType(),
                                        WkUserUtils.findNameBySid(item.getUpdateUser()),
                                        item.getUseDep(),
                                        item.isUserContainFollowing(),
                                        item.getUseDepTempletSid(),
                                        item.getUseUser());
                                result.add(menuTagSearchVO);
                            });
        } catch (Exception e) {
            log.warn("getMenuTagSearchVO ERROR", e);
        }
        return result;
    }

    public MenuTagSearchVO getMenuTagSearchVOByMenuTagSid(String menuTagSid) {
        MenuTagSearchVO result = null;
        try {
            WCMenuTag item = wcMenuTagManager.getWCMenuTagBySid(menuTagSid);

            if (WkStringUtils.isEmpty(item)) {
                return result;
            }

            String statusStr = (Activation.ACTIVE.equals(item.getStatus())) ? "顯示" : "不顯示";

            result = new MenuTagSearchVO(
                    item.getSid(),
                    ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreateDate()),
                    WkUserUtils.findNameBySid(item.getCreateUser()),
                    item.getMenuName(),
                    statusStr,
                    ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdateDate()),
                    String.valueOf(item.getSeq()),
                    item.isAttachmentType(),
                    WkUserUtils.findNameBySid(item.getUpdateUser()),
                    item.getUseDep(),
                    item.isUserContainFollowing(),
                    item.getUseDepTempletSid(),
                    item.getUseUser());
        } catch (Exception e) {
            log.warn("getMenuTagSearchVOByMenuTagSid ERROR", e);
        }
        return result;
    }

    /**
     * 取得管理類別List
     *
     * @param loginUserSid 登入者Sid
     * @param loginCompSid
     * @return
     */
    public List<ManagerMenuVO> getManagerMenuTags(Integer loginUserSid, Integer loginCompSid) {
        List<ManagerMenuVO> result = Lists.newArrayList();
        try {
            wcCategoryTagManager
                    .getManagerCategoryTags(loginUserSid, loginCompSid)
                    .forEach(
                            item -> {
                                item.getMenuTagVOs()
                                        .forEach(
                                                menuItem -> {
                                                    ManagerMenuVO mv = new ManagerMenuVO(
                                                            menuItem.getSid(),
                                                            item.getName(),
                                                            menuItem.getName(),
                                                            menuItem.getMemo());
                                                    result.add(mv);
                                                });
                            });
        } catch (Exception e) {
            log.warn("getManagerMenuTags ERROR", e);
        }
        return result;
    }

    public void saveAttachMenuTag(
            Map<String, ByteArrayInputStream> attachments,
            String categorySid,
            MenuTagEditVO vo,
            Integer uerSid)
            throws IOException {
        WCMenuTag wcMenuTag = null;
        if (Strings.isNullOrEmpty(vo.getSid())) {
            if (wcMenuTagManager.findMenuTagByCategoryAndName(categorySid, vo.getName()) != null) {
                throw new RuntimeException("重複類別名稱，請重新輸入");
            }
            wcMenuTag = new WCMenuTag();
            wcMenuTag.setWcCategoryTagSid(categorySid);
            wcMenuTag.setUseDep(new UserDep());
            wcMenuTag.setSeq(wcMenuTagManager.getMenuTagNextSeqByCategory(categorySid));
            wcMenuTag.setAttachmentType(true);
            wcMenuTag.setStatus(Activation.valueOf(vo.getStatus()));
            wcMenuTag.setMenuName(vo.getName());
            File folder = AttachmentUtils.createDirectoryIfNotExist("wcMenuTag/" + vo.getName());
            wcMenuTag.setAttachmentPath(folder.getAbsolutePath());
            this.uploadFiles(attachments, folder.getAbsolutePath());

            wcMenuTagManager.createWCMenuTag(wcMenuTag, uerSid);
        } else {
            wcMenuTag = wcMenuTagManager.findMenuTagBySid(vo.getSid());
            File destDir = AttachmentUtils.createDirectoryIfNotExist("wcMenuTag/" + vo.getName());
            // if rename folder
            if (!wcMenuTag.getMenuName().equals(vo.getName())) {
                File srcDir = new File(wcMenuTag.getAttachmentPath());
                FileUtils.copyDirectory(srcDir, destDir);
                FileUtils.deleteDirectory(new File(wcMenuTag.getAttachmentPath()));
                wcMenuTag.setMenuName(vo.getName());
                wcMenuTag.setAttachmentPath(destDir.getAbsolutePath());
            }
            wcMenuTag.setStatus(Activation.valueOf(vo.getStatus()));
            deleteUnusefulFile(destDir, attachments);
            this.uploadFiles(attachments, destDir.getAbsolutePath());
            wcMenuTagManager.updateWCMenuTag(wcMenuTag, uerSid);
        }
    }

    private void uploadFiles(Map<String, ByteArrayInputStream> attachments, String outputPath) {
        for (String key : attachments.keySet()) {
            BufferedOutputStream bos = null;
            BufferedInputStream bis = null;
            try {
                bos = new BufferedOutputStream(new FileOutputStream(outputPath + "/" + key));
                bis = new BufferedInputStream(attachments.get(key));
                StreamUtils.copy(bis, bos);

            } catch (Exception e) {
                log.warn(e.getMessage(), e);
                throw new RuntimeException("儲存失敗，上傳附件檔案發生錯誤!");
            } finally {
                IOUtils.closeQuietly(bos);
                IOUtils.closeQuietly(bis);
            }
        }
    }

    private void deleteUnusefulFile(File folder, Map<String, ByteArrayInputStream> attachments) {
        for (File file : folder.listFiles()) {
            if (attachments.get(file.getName()) == null) {
                file.delete();
            }
        }
    }

    /**
     * 依據傳入順序排序
     *
     * @param vos
     */
    public void sort(List<MenuTagSearchVO> vos) {
        if (WkStringUtils.isEmpty(vos)) {
            return;
        }

        List<String> sids = vos.stream().map(MenuTagSearchVO::getSid).collect(Collectors.toList());

        this.wcMenuTagManager.updateSortSeqByListIndex(sids);
    }
}
