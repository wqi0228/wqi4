/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.manager.WCTraceManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.WCAgainTrace;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.cy.work.connect.web.view.vo.WCAgainTraceVO;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 追蹤邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCTraceLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6364806127138959262L;

    private static WCTraceLogicComponents instance;
    @Autowired
    private WCTraceManager wcTraceManager;
    @Autowired
    private WCMasterLogicComponents wcMasterLogicComponents;

    public static WCTraceLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCTraceLogicComponents.instance = this;
    }

    /**
     * 取得追蹤By 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WCTrace getWCTraceBySid(String sid) {
        return wcTraceManager.getWCTraceBySid(sid);
    }

    /**
     * 取得追蹤介面物件By 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WCTraceVO getWCTraceVOBySid(Integer userSid, String sid) {
        WCTrace wc = wcTraceManager.getWCTraceBySid(sid);
        User user = null;
        if (wc.getUpdate_usr() != null) {
            user = WkUserCache.getInstance().findBySid(wc.getUpdate_usr());
        } else {
            user = WkUserCache.getInstance().findBySid(wc.getCreate_usr());
        }
        String dateTime = "";
        if (wc.getUpdate_dt() != null) {
            dateTime =
                ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wc.getUpdate_dt());
        } else {
            dateTime =
                ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wc.getCreate_dt());
        }
        String userInfo = user.getPrimaryOrg().getName() + "-" + user.getName();
        String correctDepFullName = "";
        String correctDepName = "";
        if (wc.getCorrectDep() != null) {
            for (UserDepTo each : wc.getCorrectDep().getUserDepTos()) {
                String orgName = WkOrgUtils.findNameBySid(Integer.valueOf(each.getDepSid()));
                if (Strings.isNullOrEmpty(correctDepName)) {
                    if (wc.getCorrectDep().getUserDepTos().size() > 1) {
                        correctDepName = orgName + "...";
                    } else {
                        correctDepName = orgName;
                    }
                }
                if (!Strings.isNullOrEmpty(correctDepFullName)) {
                    correctDepFullName += "、";
                }
                correctDepFullName += orgName;
            }
        }
        return new WCTraceVO(
            0,
            wc.getSid(),
            wc.getWc_trace_type(),
            userInfo,
            dateTime,
            wc.getWc_trace_content_css(),
            wc.getCorrectDep(),
            correctDepFullName,
            correctDepName,
            wc.getCreate_usr().equals(userSid),
            wc.getCreate_usr());
    }

    /**
     * 取得追蹤介面物件List By 工作聯絡單Sid
     *
     * @param userSid     登入者Sid
     * @param wc_id       工作聯絡單Sid
     * @param wcTraceType 僅蒐尋特定追蹤類型(若傳入null,會全部搜尋)
     * @return
     */
    public List<WCTraceVO> getWCTraceVOsByWcSIDAndWCTraceType(
        Integer userSid, String wc_id, WCTraceType wcTraceType) {
        
        List<WCTraceVO> viewTraceResults = Lists.newArrayList();
        
        try {
            
            List<WCTrace> wcTraces = wcTraceManager.getWCTracesByWCSid(wc_id);
            List<WCAgainTrace> againTraces =
                wcTraceManager.findByStatusAndWcSid(Activation.ACTIVE, wc_id);
            WCMaster wcMaster = wcMasterLogicComponents.getWCMasterBySid(wc_id);
            int index = 0;
            for (WCTrace item : wcTraces) {
                if (wcTraceType != null && !wcTraceType.equals(item.getWc_trace_type())) {
                    continue;
                }
                // 回覆的資訊僅顯示在回覆頁籤中即可,故若顯示全部,需額外濾掉回覆
                if (wcTraceType == null
                    && (WCTraceType.FINISH_REPLY.equals(item.getWc_trace_type())
                    || WCTraceType.CORRECT_REPLY.equals(item.getWc_trace_type())
                    || WCTraceType.DIRECTIVE.equals(item.getWc_trace_type()))) {
                    continue;
                }

                User user = null;
                if (item.getUpdate_usr() != null) {
                    user = WkUserCache.getInstance().findBySid(item.getUpdate_usr());
                } else {
                    user = WkUserCache.getInstance().findBySid(item.getCreate_usr());
                }
                String dateTime = "";
                if (item.getUpdate_dt() != null) {
                    dateTime =
                        ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getUpdate_dt());
                } else {
                    dateTime =
                        ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), item.getCreate_dt());
                }
                String userInfo = user.getPrimaryOrg().getName() + "-" + user.getName();
                String correctDepFullName = "";
                String correctDepName = "";
                if (item.getCorrectDep() != null) {
                    for (UserDepTo each : item.getCorrectDep().getUserDepTos()) {
                        String orgName = WkOrgUtils.findNameBySid(
                            Integer.valueOf(each.getDepSid()));
                        if (item.getCorrectDep().getUserDepTos().size() > 1) {
                            correctDepName = orgName + "...";
                        } else {
                            correctDepName = orgName;
                        }
                        if (!Strings.isNullOrEmpty(correctDepFullName)) {
                            correctDepFullName += "、";
                        }
                        correctDepFullName += orgName;
                    }
                }
                StringBuilder traceContent = new StringBuilder(item.getWc_trace_content_css());

                List<WCAgainTrace> traceReplys =
                    againTraces.stream()
                        .filter(
                            each ->
                                !Strings.isNullOrEmpty(each.getTraceSid())
                                    && each.getTraceSid().equals(item.getSid()))
                        .collect(Collectors.toList());

                if (traceReplys != null && !traceReplys.isEmpty()) {
                    traceContent.append("<br>");
                    traceReplys.forEach(
                        each -> {
                            traceContent.append("<hr>");
                            User replyUser = WkUserCache.getInstance()
                                .findBySid(each.getCreateUser());
                            String replyUserInfo =
                                replyUser.getPrimaryOrg().getName() + "-" + replyUser.getName();
                            traceContent.append(
                                "<font  color='#0066FF'>" + replyUserInfo + "</font>" + "　說:");
                            traceContent.append("<br>");
                            traceContent.append("<br>");
                            traceContent.append(each.getContentCss());
                            traceContent.append("<br>");
                            traceContent.append("<br>");
                            traceContent.append(
                                "　　　"
                                    + ToolsDate.transDateToString(
                                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                    each.getUpdateDate()));
                            traceContent.append("<br>");
                        });
                }
                boolean showReplyBtn = false;
                boolean showReplyEditBtn = false;
                if (!wcMaster.getWc_status().equals(WCStatus.CLOSE)
                    && !wcMaster.getWc_status().equals(WCStatus.CLOSE_STOP)
                    && !wcMaster.getWc_status().equals(WCStatus.INVALID)) {
                    if (item.getCreate_usr().equals(userSid) || wcMaster.getCreate_usr()
                        .equals(userSid)) {
                        if (item.getWc_trace_type().equals(WCTraceType.EXEC_FINISH)) {
                            showReplyBtn = true;
                        }
                    }
                    if (item.getCreate_usr().equals(userSid)) {
                        if (item.getWc_trace_type().equals(WCTraceType.REPLY)) {
                            showReplyEditBtn = true;
                        }
                    }
                }
                WCTraceVO trace =
                    new WCTraceVO(
                        index,
                        item.getSid(),
                        item.getWc_trace_type(),
                        userInfo,
                        dateTime,
                        traceContent.toString(),
                        item.getCorrectDep(),
                        correctDepFullName,
                        correctDepName,
                        showReplyBtn,
                        item.getCreate_usr());
                trace.setShowReplyEditBtn(showReplyEditBtn);
                viewTraceResults.add(trace);
                index++;
            }
            return viewTraceResults;
        } catch (Exception e) {
            log.warn("getWCTraceVOsByWrIDAndWCTraceType Error ", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 在WRTraceVO 組合再次回覆資料
     *
     * @param userSid
     * @param wcSid
     * @param wcTraceVOs
     * @param wcTraceType
     */
    public void bulidWCAgainTraceVOsByWcSIDAndWCTraceType(
        Integer userSid, String wcSid, List<WCTraceVO> wcTraceVOs, WCTraceType wcTraceType) {
        Map<String, List<WCAgainTraceVO>> results =
            this.getWCAgainTraceVOsByWcSIDAndWCTraceType(userSid, wcSid, wcTraceType);
        if (results == null || results.isEmpty()) {
            return;
        }
        wcTraceVOs.forEach(
            each -> {
                if (results.containsKey(each.getSid())) {
                    each.setAgainTrace(results.get(each.getSid()));
                }
            });
    }

    /**
     * 取得回覆的回覆資訊
     *
     * @param userSid
     * @param wcSid
     * @param wcTraceType
     * @return
     */
    private Map<String, List<WCAgainTraceVO>> getWCAgainTraceVOsByWcSIDAndWCTraceType(
        Integer userSid, String wcSid, WCTraceType wcTraceType) {
        try {
            return wcTraceManager.findByStatusAndWcSid(Activation.ACTIVE, wcSid).stream()
                .filter(each -> wcTraceType.equals(each.getType()))
                .map(
                    each -> {
                        User user = null;
                        if (each.getUpdateUser() != null) {
                            user = WkUserCache.getInstance().findBySid(each.getUpdateUser());
                        } else {
                            user = WkUserCache.getInstance().findBySid(each.getCreateUser());
                        }
                        String dateTime = "";
                        if (each.getUpdateDate() != null) {
                            dateTime =
                                ToolsDate.transDateToString(
                                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                    each.getUpdateDate());
                        } else {
                            dateTime =
                                ToolsDate.transDateToString(
                                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                    each.getCreateDate());
                        }
                        String userInfo = user.getPrimaryOrg().getName() + "-" + user.getName();
                        return new WCAgainTraceVO(
                            each.getSid(),
                            each.getTraceSid(),
                            each.getType(),
                            userInfo,
                            dateTime,
                            each.getContentCss(),
                            each.getCreateUser().equals(userSid));
                    })
                .collect(Collectors.groupingBy(key -> key.getTraceSid()));
        } catch (Exception e) {
            log.warn("getWCAgainTraceVOsByWcSIDAndWCTraceType Error ", e);
            return Maps.newHashMap();
        }
    }

    /**
     * 取得追蹤介面物件By 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WCAgainTraceVO getWCAgainTraceVOBySid(Integer userSid, String sid) {
        WCAgainTrace againTrace = wcTraceManager.getWCAgainTraceBySid(sid);
        User user = null;
        if (againTrace.getUpdateUser() != null) {
            user = WkUserCache.getInstance().findBySid(againTrace.getUpdateUser());
        } else {
            user = WkUserCache.getInstance().findBySid(againTrace.getCreateUser());
        }
        String dateTime = "";
        if (againTrace.getUpdateDate() != null) {
            dateTime =
                ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), againTrace.getUpdateDate());
        } else {
            dateTime =
                ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), againTrace.getCreateDate());
        }
        String userInfo = user.getPrimaryOrg().getName() + "-" + user.getName();
        return new WCAgainTraceVO(
            againTrace.getSid(),
            againTrace.getTraceSid(),
            againTrace.getType(),
            userInfo,
            dateTime,
            againTrace.getContentCss(),
            againTrace.getCreateUser().equals(userSid));
    }
}
