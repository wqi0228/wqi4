/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.to;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自訂物件 - 類別樹
 *
 * @author kasim
 */
@EqualsAndHashCode(of = {"sid"})
@Data
public class CategoryTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5200425771325142038L;

    private String sid;

    private String name;

    private String clz;

    private boolean isAttachmentType;

    private String path;

    public CategoryTo(String sid, String name) {
        this.sid = sid;
        this.name = name;
        this.clz = "";
    }

    public CategoryTo(String sid, String name, boolean isAttachmentType, String path) {
        this.sid = sid;
        this.name = name;
        this.isAttachmentType = isAttachmentType;
        this.path = path;
        this.clz = "";
    }
}
