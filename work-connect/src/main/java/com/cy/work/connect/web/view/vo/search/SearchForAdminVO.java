/**
 * 
 */
package com.cy.work.connect.web.view.vo.search;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class SearchForAdminVO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1899702983378175537L;

    /**
     * SID
     */
    @Getter
    @Setter
    private String wcSid;
    /**
     * 單號
     */
    @Getter
    @Setter
    private String wcNo;
    /**
     * 申請單位
     */
    @Getter
    @Setter
    private Integer createDepSid;
    /**
     * 申請單位名稱
     */
    @Getter
    @Setter
    private String createDepName;
    /**
     * 申請人
     */
    @Getter
    @Setter
    private Integer createUserSid;
    /**
     * 申請人名稱
     */
    @Getter
    @Setter
    private String createUserName;
    /**
     * 建單時間
     */
    @Getter
    @Setter
    private Date createDate;
    /**
     * 建單時間
     */
    @Getter
    @Setter
    private String createDateDescr;
    /**
     * 單據狀態
     */
    @Getter
    @Setter
    private String wcStatusCode;
    /**
     * 
     */
    @Getter
    @Setter
    private String wcStatusDescr;
    /**
     * 主題
     */
    @Getter
    @Setter
    private String theme;
    /**
     * 內容
     */
    @Getter
    @Setter
    private byte[] contentCssBlob;

    /**
     * 內容
     */
    @Getter
    @Setter
    private String contentCss;

    /**
     * 內容-純文字
     */
    @Getter
    @Setter
    private String contentText;

    /**
     * 大類名稱
     */
    @Getter
    @Setter
    private String categoryName;

    /**
     * 中類名稱
     */
    @Getter
    @Setter
    private String menuName;

    /**
     * 小類JSON
     */
    @Getter
    @Setter
    private String categoryTagJsonStr;

    /**
     * 小類 SID
     */
    @Getter
    @Setter
    private List<String> tagSids = Lists.newArrayList();
    /**
     * 
     */
    @Getter
    @Setter
    private String tagNames;;

}
