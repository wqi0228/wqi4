/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.vo.enums.ReadRecordStatus;
import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class TransOrgViewVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2323015196284913752L;

    @Getter
    private final String sid;
    /**
     * 轉發部門Sid
     */
    @Getter
    private final Integer transOrgSid;
    /**
     * 轉發部門Name
     */
    @Getter
    private final String transOrgName;
    /**
     * 轉發者Sid
     */
    @Getter
    private final Integer transUserSid;
    /**
     * 轉發者Name
     */
    @Getter
    private final String transUserName;
    /**
     * 被轉發部門Sid
     */
    @Getter
    private final Integer transedOrgSid;
    /**
     * 被轉發部門Name
     */
    @Getter
    private final String transedOrgName;
    /**
     * 被轉發者Sid
     */
    @Getter
    private final Integer transedUserSid;
    /**
     * 被轉發者Name
     */
    @Getter
    private final String transedUserName;
    /**
     * 轉發時間
     */
    @Getter
    private final String transTime;
    /**
     * 狀態字串
     */
    @Getter
    private final String status;

    @Getter
    private final ReadRecordStatus readRecordStatus;

    public TransOrgViewVO(
        String sid,
        Integer transOrgSid,
        String transOrgName,
        Integer transUserSid,
        String transUserName,
        Integer transedOrgSid,
        String transedOrgName,
        Integer transedUserSid,
        String transedUserName,
        String transTime,
        String status,
        ReadRecordStatus readRecordStatus) {
        this.sid = sid;
        this.transOrgSid = transOrgSid;
        this.transOrgName = transOrgName;
        this.transUserSid = transUserSid;
        this.transUserName = transUserName;
        this.transedOrgSid = transedOrgSid;
        this.transedOrgName = transedOrgName;
        this.transedUserSid = transedUserSid;
        this.transedUserName = transedUserName;
        this.transTime = transTime;
        this.status = status;
        this.readRecordStatus = readRecordStatus;
    }
}
