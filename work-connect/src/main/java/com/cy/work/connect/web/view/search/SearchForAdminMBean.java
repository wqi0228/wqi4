/**
 * 
 */
package com.cy.work.connect.web.view.search;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultStreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkTreeUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.connect.web.config.WCProperties;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.vo.search.SearchForAdminQueryCondition;
import com.cy.work.connect.web.view.vo.search.SearchForAdminVO;
import com.cy.work.connect.web.view.vo.search.helper.SearchForAdminHelper;
import com.cy.work.viewcomponent.treepker.TreePickerCallback;
import com.cy.work.viewcomponent.treepker.TreePickerComponent;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Controller
@Scope("view")
@Slf4j
public class SearchForAdminMBean implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 791100804041902587L;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient SearchForAdminHelper searchForAdminHelper;
    @Autowired
    private transient WCProperties wcProperties;

    // ========================================================================
    // view
    // ========================================================================
    @Getter
    private Boolean canExport = false;
    @Getter
    private SearchForAdminQueryCondition condition = new SearchForAdminQueryCondition();
    @Getter
    private List<SearchForAdminVO> searchForAdminVOs = Lists.newArrayList();
    
    @Getter
    @Setter
    private List<SearchForAdminVO> filteredValue = Lists.newArrayList();

    @Getter
    public final String DATATABLE_NAME = "SearchByAdmin_DATATABLE_NAME";
    @Getter
    public final String DATATABLE_PANAL = "SearchByAdmin_DATATABLE_PANAL";

    // ========================================================================
    // 變數
    // ========================================================================
    private List<WkItem> allItems = Lists.newArrayList();
    @Getter
    private DefaultStreamedContent download;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        // 取得公司別
        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
        // 單據項目
        if (comp == null) {
            this.allItems = Lists.newArrayList();
        } else {
            this.allItems = searchForAdminHelper.findAllItem(comp.getSid());
        }

        // 是否可匯出
        this.canExport = wcProperties.isSuperAdmin(SecurityFacade.getUserId());

        this.itemTreePicker = new TreePickerComponent(itemTreePickerCallback, itemTreeDesc);
        // this.itemTreePicker.setShowInActiveItem(true);
        // 選取時總是含以下
        this.itemTreePicker.setAlwaysContainFollowing(true);

    }

    /**
     * 查詢
     */
    public void search() {
        // ====================================
        // 查詢
        // ====================================
        try {
            this.searchForAdminVOs = this.searchForAdminHelper.search(this.condition, this.allItems.size());
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 畫面更新
        // ====================================
        DisplayController.getInstance().clearFilter(DATATABLE_NAME);
        DisplayController.getInstance().update(DATATABLE_NAME);

    }

    /**
     * 清除
     */
    public void clear() {
        this.condition = new SearchForAdminQueryCondition();
        this.searchForAdminVOs = Lists.newArrayList();
        DisplayController.getInstance().clearFilter(DATATABLE_NAME);
        DisplayController.getInstance().update(DATATABLE_NAME);
    }

    /**
     * 匯出
     */
    public void excel() {
        // ====================================
        // 查詢
        // ====================================
        List<SearchForAdminVO> searchForAdminVOs = this.searchForAdminHelper.search(this.condition, this.allItems.size());

        DisplayController.getInstance().update(DATATABLE_NAME);

        if (WkStringUtils.isEmpty(searchForAdminVOs)) {
            MessagesUtils.showInfo("查無資料!");
            return;
        }

        log.debug("查詢:[{}]筆!", searchForAdminVOs.size());

        // ====================================
        // EXCEL
        // ====================================
        this.download = this.searchForAdminHelper.exportEXCEL(searchForAdminVOs);
        log.debug("匯出準備ok");

        DisplayController.getInstance().clearFilter(DATATABLE_NAME);
        DisplayController.getInstance().update(DATATABLE_NAME);

    }

    // ========================================================================
    // 單據項目選擇樹
    // ========================================================================
    /**
     * 單據項目選擇樹
     */
    @Getter
    private transient TreePickerComponent itemTreePicker;

    private final static String itemTreeDesc = "單據項目選擇樹";

    @Getter
    public final String ITEM_TREE_NAME = "SearchByAdmin_ITEM_TREE_NAME";
    @Getter
    public final String ITEM_TREE_CONTENT_CLASS_NAME = "SearchByAdmin_ITEM_TREE_CONTENT";

    /**
     * 單據項目選擇樹 - 開啟選單
     */
    public void event_dialog_itemTree_open() {

        try {

            this.itemTreePicker.rebuild();
        } catch (Exception e) {
            String errorMessage = "開啟【" + itemTreeDesc + "】設定視窗失敗";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        DisplayController.getInstance().showPfWidgetVar(ITEM_TREE_NAME);
    }

    /**
     * 單據項目選擇樹 - 關閉選單
     */
    public void event_dialog_itemTree_confirm() {
        // 取得選擇項目
        condition.setSelectedItemSids(this.itemTreePicker.getSelectedItemSids());
        // 關閉視窗
        DisplayController.getInstance().hidePfWidgetVar(ITEM_TREE_NAME);
    }

    /**
     * 顯示選擇項目
     * 
     * @return
     */
    public String showSelectedItems() {
        if (WkStringUtils.isEmpty(condition.getSelectedItemSids())) {
            return "全部";
        }

        return WkTreeUtils.prepareSelectedItemNameByTreeStyle(
                this.allItems,
                Lists.newArrayList(condition.getSelectedItemSids()),
                40);
    }

    /**
     * 單據項目選擇樹 - callback 事件
     */
    private final TreePickerCallback itemTreePickerCallback = new TreePickerCallback() {
        /**
         * 
         */
        private static final long serialVersionUID = 8202838031191184324L;

        @Override
        public List<WkItem> prepareAllItems() throws Exception {
            //不重新查會有問題 (找不到原因)
            Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
            return searchForAdminHelper.findAllItem(comp.getSid());
        }

        /**
         * 準備已選擇項目
         * 
         * @return
         */
        @Override
        public List<String> prepareSelectedItemSids() throws Exception {

            if (condition.getSelectedItemSids() == null) {
                return Lists.newArrayList();
            }
            return Lists.newArrayList(condition.getSelectedItemSids());
        }

        /**
         *
         */
        @Override
        public List<String> prepareDisableItemSids() throws Exception {
            // 此功能沒有不可選擇的資料
            return Lists.newArrayList();
        }
    };

}
