/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class ManagerExecDepSettingVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3974308977281505729L;

    @Getter
    private final String execDepSettingSid;
    @Getter
    private final String tagName;
    @Getter
    private final String tagMemo;
    @Getter
    private final String execDepName;
    @Getter
    private final String receviceType;
    @Getter
    private final String recevicePerson;
    @Getter
    private final String receviceInfo;
    @Getter
    private final String receviceCss;

    public ManagerExecDepSettingVO(
        String execDepSettingSid,
        String tagName,
        String tagMemo,
        String execDepName,
        String receviceType,
        String recevicePerson,
        String receviceInfo,
        String receviceCss) {
        this.execDepSettingSid = execDepSettingSid;
        this.tagName = tagName;
        this.tagMemo = tagMemo;
        this.execDepName = execDepName;
        this.receviceType = receviceType;
        this.recevicePerson = recevicePerson;
        this.receviceInfo = receviceInfo;
        this.receviceCss = receviceCss;
    }
}
