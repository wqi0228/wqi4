/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.SingleDepTreeManager;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.TreeNode;

/**
 * 類別樹 元件
 *
 * @author kasim
 */
public class OrgTreePersonComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4172140856141802717L;
    /**
     * 登入公司
     */
    private final Integer loginCompSid;
    /**
     * 部門相關邏輯Component
     */
    private final DepTreeComponent depTreeComponent;

    @Getter
    @Setter
    /** 使用者清單 */
    private List<UserViewVO> users;

    @Getter
    @Setter
    /** 選取的使用者清單 */
    private UserViewVO selUser;

    public OrgTreePersonComponent(Integer loginCompSid) {
        this.loginCompSid = loginCompSid;
        this.users = Lists.newArrayList();
        this.depTreeComponent = new DepTreeComponent();
        this.depTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                new Org(loginCompSid),
                Lists.newArrayList(),
                true,
                true,
                true);
    }

    /**
     * 初始化選項
     */
    public void initItems(Map<Org, List<WCTag>> execDepMap) {
        this.users = Lists.newArrayList();
        this.selUser = null;
        List<Org> tempOrg = Lists.newArrayList();
        execDepMap
                .keySet()
                .forEach(
                        item -> {
                            tempOrg.add(item);
                        });
        Set<Org> childs = tempOrg.stream()
                .flatMap(each -> WkOrgCache.getInstance().findAllChild(each.getSid()).stream())
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .distinct()
                .collect(Collectors.toSet());
        childs.addAll(tempOrg);
        Set<Org> parents = tempOrg.stream()
                .flatMap(each -> WkOrgCache.getInstance().findAllParent(each.getSid()).stream())
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .distinct()
                .collect(Collectors.toSet());
        parents.addAll(childs);
        depTreeComponent.init(
                Lists.newArrayList(parents),
                Lists.newArrayList(childs),
                new Org(loginCompSid),
                Lists.newArrayList(tempOrg.get(0)));
        if (depTreeComponent.getSelectOrg() != null && !depTreeComponent.getSelectOrg().isEmpty()) {
            this.selectNodeEvent();
        }
        users = getAllOrgsUsers(Lists.newArrayList(childs));
        DisplayController.getInstance()
                .resetDataTable("dlg_sendExecPerson_view_orgTree_person__view_person");
    }

    /**
     * 初始化選項 (僅領單可閱)
     */
    public void initItems(Org execDep, Collection<Integer> receviceUserSids) {
        this.users = Lists.newArrayList();
        this.selUser = null;
        List<Org> allParentOrgs = WkOrgCache.getInstance().findAllParent(execDep.getSid());
        allParentOrgs.add(execDep);
        depTreeComponent.init(
                allParentOrgs, Lists.newArrayList(), new Org(loginCompSid),
                Lists.newArrayList(execDep));
        if (depTreeComponent.getSelectOrg() != null && !depTreeComponent.getSelectOrg().isEmpty()) {
            this.selectNodeEvent();
        }
        users = getAllUsers(Lists.newArrayList(receviceUserSids));
        DisplayController.getInstance()
                .resetDataTable("dlg_sendExecPerson_view_orgTree_person__view_person");
    }

    /**
     * 初始化顯示出可選單位的所有人
     *
     * @param tempOrg
     * @return
     */
    private List<UserViewVO> getAllOrgsUsers(List<Org> tempOrg) {

        LinkedHashSet<UserViewVO> allUsers = Sets.newLinkedHashSet();
        tempOrg.forEach(
                each -> {
                    List<UserViewVO> orgUsers = WkUserCache.getInstance()
                            .findUserWithManagerByOrgSids(Lists.newArrayList(each.getSid()), Activation.ACTIVE)
                            .stream()
                            .distinct()
                            .filter(other -> Activation.ACTIVE.equals(other.getStatus()))
                            .map(other -> new UserViewVO(other.getSid(), other.getName()))
                            .collect(Collectors.toList());
                    allUsers.addAll(orgUsers);
                });

        return Lists.newArrayList(allUsers);
    }

    /**
     * 初始化顯示出可選名單的所有人
     *
     * @param tempUsersid
     * @return
     */
    private List<UserViewVO> getAllUsers(List<Integer> tempUsersid) {

        LinkedHashSet<UserViewVO> allUsers = Sets.newLinkedHashSet();
        List<UserViewVO> tempUsers = WkUserCache.getInstance().findBySids(tempUsersid).stream()
                .filter(other -> Activation.ACTIVE.equals(other.getStatus()))
                .map(other -> new UserViewVO(other.getSid(), other.getName()))
                .collect(Collectors.toList());
        allUsers.addAll(tempUsers);

        return Lists.newArrayList(allUsers);
    }

    /**
     * 挑選轉換前單位
     */
    public void selectNodeEvent() {
        DisplayController.getInstance()
                .resetDataTable("dlg_sendExecPerson_view_orgTree_person__view_person");
        this.selUser = null;
        if (depTreeComponent.getSelectOrg() == null || depTreeComponent.getSelectOrg().isEmpty()) {
            this.users = Lists.newArrayList();
        } else {
            this.users = WkUserCache.getInstance()
                    .findUserWithManagerByOrgSids(Lists.newArrayList(depTreeComponent.getSelectOrg().get(0).getSid()), Activation.ACTIVE)
                    .stream()
                    .distinct()
                    .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                    .map(each -> new UserViewVO(each.getSid(), each.getName()))
                    .collect(Collectors.toList());
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return SingleDepTreeManager 組織樹Manager
     */
    public SingleDepTreeManager getDepTreeManager() { return depTreeComponent.getSingleDepTreeManager(); }

    /**
     * 自動展開所有node
     *
     * @return TreeNode 組織樹
     */
    public TreeNode expandAllChildNode(TreeNode node) {

        node.setExpanded(true);

        if (node.getChildren() == null) {
            return node;
        }
        node.getChildren()
                .forEach(
                        each -> {
                            each.setExpanded(true);
                            if (each.getChildren() != null) {
                                this.expandAllChildNode(each);
                            }
                        });

        return node;
    }
}
