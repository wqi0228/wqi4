/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 類別編輯物件
 *
 * @author brain0925_liao
 */
public class CategoryTagEditVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1825578839582190987L;

    @Setter
    @Getter
    private String sid;
    @Setter
    @Getter
    private String categoryName;
    @Setter
    @Getter
    private String status;
    @Setter
    @Getter
    private String seq;
    @Setter
    @Getter
    private String memo;
}
