/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 使用者邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCUserLogic implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -542098812237882252L;

    private static WCUserLogic instance;
    @Autowired
    private WkUserCache wkUserCache;

    public static WCUserLogic getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCUserLogic.instance = this;
    }

    /**
     * 取得使用者介面物件 By 部門Sid
     *
     * @param depSid 部門Sid
     * @return
     */
    public List<UserViewVO> findByDepSid(Integer depSid) {
        try {
            return this.wkUserCache.findUserWithManagerByOrgSids(Lists.newArrayList(depSid), Activation.ACTIVE)
                    .stream()
                    .distinct()
                    .filter(user -> user != null)
                    .map(user -> new UserViewVO(user.getSid()))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.warn("findByDepSid error :", e);
        }
        return Lists.newArrayList();
    }
}
