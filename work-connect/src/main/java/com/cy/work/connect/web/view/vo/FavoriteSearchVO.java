/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.vo.WCTag;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class FavoriteSearchVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4293911331816002278L;
    /**
     * 類別資訊(第三層)
     */
    @Getter
    private String tagInfo = "";

    @Getter
    private List<String> tgSids = Lists.newArrayList();
    @Getter
    @Setter
    private String content;
    @Getter
    @Setter
    private String favorite_sid;
    @Getter
    @Setter
    private String wcSid;
    @Getter
    @Setter
    private String theme;
    @Getter
    @Setter
    private String favoriteDate;
    @Getter
    @Setter
    private String wcNo;
    @Getter
    @Setter
    private Date favoritedt;

    public FavoriteSearchVO(String favorite_sid) {
        this.favorite_sid = favorite_sid;
    }

    public void replaceValue(FavoriteSearchVO obj) {
        this.theme = obj.theme;
        this.favoriteDate = obj.favoriteDate;
        this.wcNo = obj.wcNo;
        this.content = obj.content;
        this.tagInfo = obj.tagInfo;
        this.tgSids = obj.tgSids;
    }

    public void bulidExecTag(WCTag tag) {
        if (tag == null) {
            return;
        }
        if (tgSids.contains(tag.getSid())) {
            return;
        }
        tgSids.add(tag.getSid());
        if (!Strings.isNullOrEmpty(tagInfo)) {
            tagInfo += "、";
        }
        tagInfo += tag.getTagName();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.favorite_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FavoriteSearchVO other = (FavoriteSearchVO) obj;
        return Objects.equals(this.favorite_sid,
            other.favorite_sid);
    }
}
