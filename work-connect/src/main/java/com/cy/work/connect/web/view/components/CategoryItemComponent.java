package com.cy.work.connect.web.view.components;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.primefaces.PrimeFaces;

import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStrFormatter;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCTagTempletManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.logic.vo.WCTagTempletVo;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.web.listener.WorkReportDataCallBack;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCTagLogicComponents;
import com.cy.work.connect.web.view.vo.TagTypeVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 小類(執行項目)選項物件
 *
 * @author kasim
 */
@Slf4j
public class CategoryItemComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2796324900802865003L;

    private final WCMenuTagLogicComponent menuTagLogicComponent;
    private final WCTagLogicComponents tagLogicComponents;
    private final Comparator<WCTag> wcTagComparator = new Comparator<WCTag>() {
        public int compare(WCTag obj1, WCTag obj2) {
            final Integer seq1 = obj1.getSeq();
            final Integer seq2 = obj2.getSeq();
            return seq1.compareTo(seq2);
        }
    };
    @Getter
    private String categoryMenuName;
    @Getter
    /** 是否可編輯 */
    private boolean canEdit = false;
    @Getter
    /** 欄位名稱 */
    private String fieldName;
    @Getter
    /** 中類sid */
    private String middleCategorySid;
    @Getter
    /** 主題 */
    private String title;
    @Getter
    /** 內容 */
    private String content;
    @Getter
    /** 備註 */
    private String remark;
    @Getter
    /** 選項 */
    private String categoryItem;
    @Getter
    /** 選項 */
    private List<SelectItem> categoryItems;
    @Getter
    @Setter
    /** 選取 */
    private List<String> selCategoryItems;
    /**
     * Tag分類選項
     */
    @Getter
    private List<TagTypeVO> tagTypes;
    private boolean isSelectItem = false;
    private String selectItemValue;
    @Getter
    @Setter
    private WorkReportDataCallBack workReportDataCallBack;
    @Getter
    @Setter
    /** 合法區間判斷 */
    private List<LegitimateRangeComponent> legitimateRangeComponents;
    @Getter
    @Setter
    /** 起訖日判斷 */
    private List<StartEndComponent> startEndComponents;
    /**
     * 初始化起訖判斷
     */
    private List<StartEndComponent> defaultStartEndComponents;
    /**
     * 初始化合法區間判斷
     */
    private List<LegitimateRangeComponent> defaultLegitimateRangeComponents;
    /**
     * 選擇的Tag模版(建議勾選項目)
     */
    @Getter
    @Setter
    private Long selTagTemplet;
    @Getter
    private List<SelectItem> tagTemplets;
    private String menuTagSid;
    private Set<Integer> loginManagedDeps;
    private Integer loginCompSid;
    private Integer loginUserSid;

    public CategoryItemComponent() {
        this.menuTagLogicComponent = WCMenuTagLogicComponent.getInstance();
        this.tagLogicComponents = WCTagLogicComponents.getInstance();
    }

    /**
     * 建立類別選項
     *
     * @param menuTagSid       中類(單據名稱)Sid
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginCompSid     登入公司Sid
     * @param loginUserSid     登入User Sid
     * @throws UserMessageException
     */
    public void initItems(
            String menuTagSid,
            final Set<Integer> loginManagedDeps,
            Integer loginCompSid,
            Integer loginUserSid) throws UserMessageException {

        this.menuTagSid = menuTagSid;
        this.loginManagedDeps = loginManagedDeps;
        this.loginCompSid = loginCompSid;
        this.loginUserSid = loginUserSid;

        this.canEdit = true;
        this.categoryItem = null;
        this.selCategoryItems = Lists.newArrayList();

        // 登入者
        User loginUser = WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid());

        // ====================================
        // 取得單據類別資料
        // ====================================
        // 中類
        MenuTagVO menuTagVO = this.menuTagLogicComponent.findToBySid(menuTagSid);
        if (menuTagVO == null) {
            String userMessage = WkMessage.NEED_RELOAD + "(找不到中類資料)";
            log.error(userMessage);
            throw new UserMessageException(userMessage, InfomationLevel.ERROR);
        }

        // 大類
        WCCategoryTag wcCategoryTag = WCCategoryTagLogicComponent.getInstance()
                .getWCCategoryTagBySid(menuTagVO.getCategorySid());
        if (wcCategoryTag == null) {
            String userMessage = WkMessage.NEED_RELOAD + "(找不到大類資料)";
            log.error(userMessage);
            throw new UserMessageException(userMessage, InfomationLevel.ERROR);
        }

        // 建議勾選項目
        this.tagTemplets = WCTagTempletManager.getInstance()
                .findTagTempletByMenuTagSidAndCompSid(menuTagVO.getSid(), loginCompSid);

        // ====================================
        // 設定
        // ====================================
        // 組單據名稱
        this.categoryMenuName = wcCategoryTag.getCategoryName() + "-" + menuTagVO.getName();
        // 中類sid
        this.middleCategorySid = menuTagVO.getSid();
        // 小類欄位顯示文字
        this.fieldName = menuTagVO.getItemName();
        // 預設主題 (加入參數轉換功能)
        this.title = WkStrFormatter.getInstance().processWithoutErrorLog(
                menuTagVO.getTitle(),
                loginUser);

        // 預設內容 (加入參數轉換功能)
        this.content = WkStrFormatter.getInstance().processWithoutErrorLog(
                menuTagVO.getContent(),
                loginUser);

        // 預設備註 (加入參數轉換功能)
        this.remark = WkStrFormatter.getInstance().processWithoutErrorLog(
                menuTagVO.getRemark(),
                loginUser);

        try {

            // 原 title 自動加入日期功能，改由 WORKCOMMU-485 方案取代
            // this.title = "「"
            // + ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDash.getValue(),
            // new Date())
            // + "」-"
            // + category.getTitle();

            this.buildItems(
                    menuTagSid,
                    null,
                    loginManagedDeps,
                    loginCompSid,
                    loginUserSid);
            if (!Strings.isNullOrEmpty(categoryItem)) {
                this.selCategoryItems.add((String) categoryItems.get(0).getValue());
            }
            this.initLegitimateRange(menuTagSid, true, loginManagedDeps, loginCompSid, loginUserSid);
            this.initStartEnd(menuTagSid, true, loginManagedDeps, loginCompSid);
            this.checkLegalRangeOrStartEnd();
        } catch (Exception e) {
            log.warn("建立類別選項 ERROR", e);
        }
    }

    public void initForViewMode(WCMaster wcMaster) {
        this.content = wcMaster.getContent_css();
        this.remark = wcMaster.getMemo_css();
    }

    /**
     * 讀取類別選項
     *
     * @param menuTagSid       中類(單據名稱)Sid
     * @param to               小類(執行項目)清單物件
     * @param editMode         編輯模式
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginCompSid     登入公司Sid
     * @param loginUserSid     登入User Sid
     */
    public void loadItems(
            String menuTagSid,
            CategoryTagTo to,
            boolean editMode,
            Set<Integer> loginManagedDeps,
            Integer loginCompSid,
            Integer loginUserSid) {
        this.menuTagSid = menuTagSid;
        this.loginManagedDeps = loginManagedDeps;
        this.loginCompSid = loginCompSid;
        this.loginUserSid = loginUserSid;
        try {
            this.canEdit = editMode;
            MenuTagVO category = menuTagLogicComponent.findToBySid(menuTagSid);
            try {
                this.tagTemplets = WCTagTempletManager.getInstance()
                        .findTagTempletByMenuTagSidAndCompSid(category.getSid(), loginCompSid);
                this.categoryMenuName = WCCategoryTagLogicComponent.getInstance()
                        .getCategoryTagEditVOBySid(category.getCategorySid())
                        .getCategoryName()
                        + "-"
                        + category.getName();
            } catch (Exception e) {
                log.warn("initItems ERROR", e);
            }
            this.fieldName = category.getItemName();
            this.middleCategorySid = category.getSid();
            this.selCategoryItems = to.getTagSids();
            this.buildItems(category.getSid(), to.getTagSids(), loginManagedDeps, loginCompSid, loginUserSid);
        } catch (Exception e) {
            log.warn("讀取類別選項 ERROR", e);
        }
    }

    /**
     * 建立選項
     *
     * @param categorySid      大類(類別)Sid
     * @param sids             小類(執行項目)Sid清單
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginCompSid     登入公司Sid
     * @param loginUserSid     登入user Sid
     */
    private void buildItems(
            String categorySid,
            List<String> sids,
            Set<Integer> loginManagedDeps,
            Integer loginCompSid,
            Integer loginUserSid) {
        this.categoryItem = null;
        this.tagTypes = Lists.newArrayList();
        Map<String, List<SelectItem>> tagTypeMap = Maps.newLinkedHashMap();
        List<WCTag> tags = Lists.newArrayList();
        if (canEdit) {
            tags = tagLogicComponents.getWCTagActiveForMaster(
                    categorySid,
                    loginManagedDeps,
                    loginUserSid,
                    loginCompSid);
            tagTypeMap = tags.stream()
                    .collect(
                            Collectors.groupingBy(
                                    each -> {
                                        if (each.getTagType() == null) {
                                            return "未分類";
                                        } else {
                                            return each.getTagType();
                                        }
                                    },
                                    LinkedHashMap::new,
                                    Collectors.mapping(
                                            each -> new SelectItem(
                                                    each.getSid(),
                                                    each.getTagName(),
                                                    each.getTagDesc(),
                                                    false,
                                                    sids != null && sids.contains(each.getSid())),
                                            Collectors.toList())));
            if (tagTypeMap.keySet().size() == 1
                    && tagTypeMap.keySet().iterator().next().equals("未分類")) {
                categoryItems = tagTypeMap.get("未分類");
            } else {
                for (String type : tagTypeMap.keySet()) {
                    this.tagTypes.add(new TagTypeVO(type, tagTypeMap.get(type), sids));
                }
            }
        } else {
            tags = tagLogicComponents.findBySid(sids);
            tagTypeMap = tags.stream()
                    .sorted(wcTagComparator)
                    .collect(
                            Collectors.groupingBy(
                                    each -> {
                                        if (each.getTagType() == null) {
                                            return "未分類";
                                        } else {
                                            return each.getTagType();
                                        }
                                    },
                                    LinkedHashMap::new,
                                    Collectors.mapping(
                                            each -> new SelectItem(
                                                    each.getSid(),
                                                    each.getTagName(),
                                                    each.getTagDesc(),
                                                    false,
                                                    sids != null && sids.contains(each.getSid())),
                                            Collectors.toList())));
            if (tagTypeMap.keySet().size() == 1
                    && tagTypeMap.keySet().iterator().next().equals("未分類")) {
                categoryItems = tagTypeMap.get("未分類");
            } else {
                for (String type : tagTypeMap.keySet()) {
                    this.tagTypes.add(new TagTypeVO(type, tagTypeMap.get(type), true));
                }
            }
        }
        if (tags.size() == 1 && categoryItems != null && !categoryItems.isEmpty()) {
            this.categoryItem = categoryItems.get(0).getLabel();
        }
    }

    /**
     * Tag分類是否全選
     *
     * @param tagType 小類(執行項目)分類物件
     */
    public void changeCheckBox(TagTypeVO tagType) {
        if (tagType.getCheckAll()) {
            this.selCategoryItems.removeAll(
                    tagType.getCategoryItems().stream()
                            .map(each -> String.valueOf(each.getValue()))
                            .collect(Collectors.toList()));
            this.selCategoryItems.addAll(
                    tagType.getCategoryItems().stream()
                            .map(each -> String.valueOf(each.getValue()))
                            .collect(Collectors.toList()));
        } else {
            this.selCategoryItems.removeAll(
                    tagType.getCategoryItems().stream()
                            .map(each -> String.valueOf(each.getValue()))
                            .collect(Collectors.toList()));
        }
        tagType.changeCheckBox();
    }

    /**
     * 判斷是否為有效資料
     *
     * @return
     */
    public boolean checkEffective() {
        return !Strings.isNullOrEmpty(middleCategorySid);
    }

    public void selectedItemsChanged(ValueChangeEvent event) {
        @SuppressWarnings("unchecked")
        List<String> oldItemsArr = (List<String>) event.getOldValue();
        @SuppressWarnings("unchecked")
        List<String> newItemsArr = (List<String>) event.getNewValue();

        if (oldItemsArr.isEmpty()) {
            oldItemsArr = Lists.newArrayList();
        }

        if (oldItemsArr.size() > newItemsArr.size()) {
            oldItemsArr = Lists.newArrayList(oldItemsArr);
            oldItemsArr.removeAll(newItemsArr);
            selectItemValue = oldItemsArr.get(0);
            isSelectItem = false;
        } else if (newItemsArr.size() > oldItemsArr.size()) {
            newItemsArr = Lists.newArrayList(newItemsArr);
            newItemsArr.removeAll(oldItemsArr);
            selectItemValue = newItemsArr.get(0);
            isSelectItem = true;
        }
    }

    /**
     * 小類(執行項目)選取事件
     *
     * @param tagType 小類(執行項目)分類物件
     * @param item    選取物件
     */
    public void selectedItemsChanged(TagTypeVO tagType, SelectItem item) {
        this.selectItemValue = String.valueOf(item.getValue());
        this.isSelectItem = item.isEscape();
        this.checkLegalRangeOrStartEnd();
        if (this.selectItemValue != null) {
            if (this.isSelectItem) {
                this.selCategoryItems.add(this.selectItemValue);
                tagType.getSelCategoryItems().add(this.selectItemValue);
            } else {
                this.selCategoryItems.remove(this.selectItemValue);
                tagType.getSelCategoryItems().remove(this.selectItemValue);
            }
        }
        tagType.changeItems();
        PrimeFaces.current().ajax().update("legitimate_range_panel_id");
        PrimeFaces.current().ajax().update("start_end_panel_id");
        PrimeFaces.current().ajax().update("workReportDataPanel");
        PrimeFaces.current().ajax().update("checkAll" + this.tagTypes.indexOf(tagType));
    }

    /**
     * 小類(執行項目)選取事件-檢查時間區間合法性
     */
    public void checkLegalRangeOrStartEnd() {
        List<LegitimateRangeComponent> copyLegitimateRange = legitimateRangeComponents;
        List<StartEndComponent> copyStartEnd = startEndComponents;

        legitimateRangeComponents = Lists.newArrayList();
        startEndComponents = Lists.newArrayList();

        // ====================================
        //
        // ====================================
        for (String each : selCategoryItems) {
            // 合法區間
            boolean wasCheckLegitimateRanges = wasCheckedLegitimateRanges(copyLegitimateRange, each);
            LegitimateRangeComponent legitimateRange = checkLegitimateRange(copyLegitimateRange, each, wasCheckLegitimateRanges);
            if (legitimateRange != null) {
                legitimateRangeComponents.add(legitimateRange);
            }

            // 起訖日
            boolean wasCheckedStartEnds = wasCheckedStartEnds(copyStartEnd, each);
            StartEndComponent startEnd = checkStartEndComponent(copyStartEnd, each, wasCheckedStartEnds);
            if (startEnd != null) {
                startEndComponents.add(startEnd);
            }
        }

        // ====================================
        // 點選小類時，取代內容文字
        // ====================================
        if (WkStringUtils.notEmpty(selectItemValue)) {
            WCTag wcTag = tagLogicComponents.getWCTagBySid(selectItemValue);
            if (wcTag != null) {
                if (workReportDataCallBack != null) {

                    this.content = workReportDataCallBack.getContent();
                    if (this.content == null) {
                        this.content = "";
                    }

                    this.remark = workReportDataCallBack.getMemo();
                    if (this.remark == null) {
                        this.remark = "";
                    }

                    // 內容欄位
                    if (WkStringUtils.notEmpty(wcTag.getDefaultContentCss())) {
                        String defaultContentCss = this.prepareDefaultText(wcTag.getDefaultContentCss());
                        if (isSelectItem) {

                            if (WkStringUtils.notEmpty(WkJsoupUtils.getInstance().htmlToText(this.content))) {
                                this.content = this.content + defaultContentCss;
                            } else {
                                this.content = defaultContentCss;
                            }

                        } else {
                            this.content = this.content.replaceAll(Pattern.quote(defaultContentCss), "");
                        }
                    }

                    // 備註欄位
                    if (WkStringUtils.notEmpty(wcTag.getDefaultMemoCss())) {
                        String defaultMemoCss = this.prepareDefaultText(wcTag.getDefaultMemoCss());
                        if (isSelectItem) {
                            if (WkStringUtils.notEmpty(WkJsoupUtils.getInstance().htmlToText(this.remark))) {
                                this.remark = this.remark + defaultMemoCss;
                            } else {
                                this.remark = defaultMemoCss;
                            }
                        } else {
                            this.remark = this.remark.replaceAll(Pattern.quote(defaultMemoCss), "");
                        }
                    }

                    workReportDataCallBack.updateWorkReportData(this.content, this.remark);
                }

            } else {
                log.error("WCTag not find! sid:[{}]", selectItemValue);
            }
        }
    }

    private String prepareDefaultText(String src) {
        if (WkStringUtils.isEmpty(src)) {
            return "";
        }

        User loginUser = WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid());

        String resultStr = WkStrFormatter.getInstance().processWithoutErrorLog(
                src,
                loginUser);

        return "<div>" + resultStr + "</div>";
    }

    /**
     * 合法區間判斷選項開啟的回傳list
     *
     * @param copyLegitimateRange 合法區間物件清單
     * @param sid                 合法區間物件Sid
     * @param wasCheck            上次是否選過
     * @return
     */
    private LegitimateRangeComponent checkLegitimateRange(
            List<LegitimateRangeComponent> copyLegitimateRange, String sid, boolean wasCheck) {
        // 如果上次選過，重新放回list內
        if (wasCheck) {
            Optional<LegitimateRangeComponent> newCopyLegitimateRange = copyLegitimateRange.stream().filter(each -> each.getSid().equals(sid)).findAny();

            return newCopyLegitimateRange.orElse(null);
        }

        Optional<LegitimateRangeComponent> legitimateRanges = defaultLegitimateRangeComponents.stream()
                .filter(each -> each.getSid().equals(sid))
                .findAny();
        return legitimateRanges.orElse(null);
    }

    /**
     * 起訖日判斷選項開啟的回傳list
     *
     * @param copyStartEnd 起訖日物件清單
     * @param sid          起訖日物件Sid
     * @param wasCheck     上次是否選過
     * @return
     */
    private StartEndComponent checkStartEndComponent(
            List<StartEndComponent> copyStartEnd, String sid, boolean wasCheck) {
        // 如果上次選過，重新放回list內
        if (wasCheck) {
            Optional<StartEndComponent> newCopyStartEnd = copyStartEnd.stream().filter(each -> each.getSid().equals(sid)).findAny();

            return newCopyStartEnd.orElse(null);
        }

        Optional<StartEndComponent> copyStartEnds = defaultStartEndComponents.stream().filter(each -> each.getSid().equals(sid)).findAny();
        return copyStartEnds.orElse(null);
    }

    /**
     * 合法區間判斷選項上次是否選取
     *
     * @param copyLegitimateRange 合法區間物件清單
     * @param sid                 合法區間物件Sid
     * @return
     */
    private boolean wasCheckedLegitimateRanges(
            List<LegitimateRangeComponent> copyLegitimateRange, String sid) {

        if (copyLegitimateRange != null) {
            Optional<LegitimateRangeComponent> getLegitimateRange = copyLegitimateRange.stream().filter(each -> each.getSid().equals(sid)).findAny();
            return getLegitimateRange.isPresent();
        }
        return false;
    }

    /**
     * 起訖日判斷選項上次是否選取
     *
     * @param copyStartEnd 起訖日物件清單
     * @param sid          起訖日物件Sid
     * @return
     */
    private boolean wasCheckedStartEnds(List<StartEndComponent> copyStartEnd, String sid) {

        if (copyStartEnd != null) {
            Optional<StartEndComponent> getStartEnd = copyStartEnd.stream().filter(each -> each.getSid().equals(sid)).findAny();
            return getStartEnd.isPresent();
        }
        return false;
    }

    /**
     * 初始化 合法區間判斷
     *
     * @param menuTagSid       中類(單據名稱)Sid
     * @param canEdit          編輯狀態
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginCompSid     登入公司Sid
     * @param loginUserSid     登入user Sid
     */
    public void initLegitimateRange(
            String menuTagSid,
            boolean canEdit,
            Set<Integer> loginManagedDeps,
            Integer loginCompSid,
            Integer loginUserSid) {

        defaultLegitimateRangeComponents = Lists.newArrayList();

        List<WCTag> wcTagList = tagLogicComponents.getWCTagActiveForMaster(
                menuTagSid,
                loginManagedDeps,
                loginUserSid,
                loginCompSid);

        wcTagList.stream()
                .filter(each -> each.isLegitimateRangeCheck() == true)
                .collect(Collectors.toList())
                .forEach(
                        each -> {
                            LegitimateRangeComponent legitimateRange = new LegitimateRangeComponent(
                                    each.getLegitimateRangeTitle(),
                                    each.getLegitimateRangeDays(),
                                    each.getSid());
                            legitimateRange.setCanEdit(canEdit);
                            defaultLegitimateRangeComponents.add(legitimateRange);
                        });
    }

    /**
     * 初始化 起訖日判斷
     *
     * @param menuTagSid       中類(單據名稱)Sid
     * @param canEdit          編輯狀態
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginCompSid     登入公司Sid
     */
    public void initStartEnd(
            String menuTagSid, boolean canEdit, Set<Integer> loginManagedDeps, Integer loginCompSid) {

        defaultStartEndComponents = Lists.newArrayList();
        List<WCTag> wcTagList = tagLogicComponents.getWCTagActiveForMaster(menuTagSid, loginManagedDeps, loginUserSid, loginCompSid);
        wcTagList.stream()
                .filter(each -> each.isStartEndCheck() == true)
                .collect(Collectors.toList())
                .forEach(
                        each -> {
                            StartEndComponent startEnd = new StartEndComponent(
                                    each.getStartEndTitle(),
                                    each.getStartEndDays(),
                                    each.getSid(),
                                    each.getEndRuleDays());
                            startEnd.setCanEdit(canEdit);
                            defaultStartEndComponents.add(startEnd);
                        });
    }

    /**
     * 將合法區間Json資料轉化為List到畫面上
     *
     * @param legitimateRangeJson 合法區間Json字串
     * @throws java.io.IOException
     */
    public void convertLegitimateRangeJsonToList(String legitimateRangeJson) throws IOException {
        if (legitimateRangeJson == null) {
            return;
        }
        legitimateRangeComponents = Lists.newArrayList();
        ObjectMapper mapper = new ObjectMapper();
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> legitimateRangeList = mapper.readValue(legitimateRangeJson, List.class);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        legitimateRangeList.forEach(
                each -> {
                    try {
                        Date getLegitimateRangDate = sdf.parse(each.get("date").toString());
                        LegitimateRangeComponent legitimateRange = checkLegitimateRange(null, each.get("sid").toString(), false);
                        if (legitimateRange != null) {
                            legitimateRangeComponents.add(legitimateRange);
                            legitimateRange.setSelectDate(getLegitimateRangDate);
                        }
                    } catch (ParseException ex) {
                        log.warn("getLegitimateRangDate Error", ex);
                    }
                });
    }

    /**
     * 將起訖日Json資料轉化為List到畫面上
     *
     * @param starEndJson 起訖日Json字串
     * @throws java.io.IOException
     */
    public void convertStartEndJsonToList(String starEndJson) throws IOException {
        if (starEndJson == null) {
            return;
        }
        startEndComponents = Lists.newArrayList();
        ObjectMapper mapper = new ObjectMapper();
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> endStartList = mapper.readValue(starEndJson, List.class);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        endStartList.forEach(
                each -> {
                    try {
                        Date getStartEndStartDate = sdf.parse(each.get("startdate").toString());
                        Date getendEndStartDate = sdf.parse(each.get("enddate").toString());
                        StartEndComponent starEnd = checkStartEndComponent(null, each.get("sid").toString(), false);
                        if (starEndJson != null) {
                            startEndComponents.add(starEnd);
                            starEnd.setSelectStartDate(getStartEndStartDate);
                            starEnd.setSelectEndDate(getendEndStartDate);
                        }
                    } catch (ParseException ex) {
                        log.warn("getLegitimateRangDate Error", ex);
                    }
                });
    }

    /**
     * 選用建議勾選項目事件
     *
     * @param selTagTempletSid (選取的)單據模版Sid
     */
    public void onSelectTagTemplet(Long selTagTempletSid) {
        // ====================================
        // 取得選用的資料
        // ====================================
        if (selTagTempletSid == null) {
            this.selCategoryItems = Lists.newArrayList();
            this.buildItems(menuTagSid, null, loginManagedDeps, loginCompSid, loginUserSid);
            return;
        }

        WCTagTempletVo wcTagTempletVo = WCTagTempletManager.getInstance()
                .findBySid(selTagTempletSid);
        List<String> defaultValue = Lists.newArrayList();
        if (wcTagTempletVo != null
                && wcTagTempletVo.getDefaultValue() != null
                && !wcTagTempletVo.getDefaultValue().getTagSids().isEmpty()) {
            defaultValue = wcTagTempletVo.getDefaultValue().getTagSids();

            // ====================================
            // 收集可用的執行項目
            // ====================================
            defaultValue = tagLogicComponents.checkCanUseTag(
                    defaultValue,
                    loginManagedDeps,
                    this.loginUserSid,
                    this.loginCompSid);
        }
        this.selCategoryItems = defaultValue;
        this.buildItems(
                menuTagSid,
                defaultValue,
                loginManagedDeps,
                loginCompSid,
                this.loginUserSid);
    }
}
