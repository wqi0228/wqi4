package com.cy.work.connect.web.view.setting10DepSettingCheck;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCExecDepSettingCache;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.vo.view.setting11.SettingOrgTransMappingVO;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.Setting3Bean;
import com.cy.work.connect.web.view.vo.CategoryTagSearchVO;
import com.cy.work.connect.web.view.vo.MenuTagSearchVO;
import com.cy.work.connect.web.view.vo.TagSearchVO;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 單位設定檢查
 * 
 * @author allen1214_wu
 */
@Slf4j
@Controller
@Scope("view")
public class Setting10DepSettingCheckMbean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4691550882464467643L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WCCategoryTagCache wCCategoryTagCache;
    @Autowired
    private WCExecDepSettingCache wcExecDepSettingCache;
    @Autowired
    private WCConfigTempletManager wcConfigTempletManager;
    @Autowired
    private DisplayController displayController;
    @Autowired
    private Setting3Bean setting3Bean;
    @Autowired
    private Setting10DepSettingCheckLogic setting10DepSettingCheckLogic;

    // ========================================================================
    // view 元件名稱
    // ========================================================================
    @Getter
    private final String DLG_WV = this.getClass().getSimpleName() + "DLG_WV";
    @Getter
    private final String DLG_CONTENT = this.getClass().getSimpleName() + "DLG_CONTENT";

    // ========================================================================
    // view 變數
    // ========================================================================
    /**
     * 不顯示模版項目
     */
    @Getter
    @Setter
    private boolean hideTempleteItem = false;

    /**
     * 不顯示無設定項目
     */
    @Getter
    @Setter
    private boolean hideNoSettingItem = true;

    /**
     * 所有的檢查項目
     */
    private List<Setting10DepSettingCheckItemVO> allCheckItems = Lists.newArrayList();

    /**
     * view 顯示資料 容器 list (過濾後)
     */
    @Getter
    private List<Setting10DepSettingCheckItemVO> showCheckItems = Lists.newArrayList();
    /**
     * 點選的檢查項目類別
     */
    @Getter
    private DepSettingCheckItemType targetCheckItemType;

    /**
     * 
     */
    @Getter
    private String titleContent;

    /**
     * 選擇的
     */
    private SettingOrgTransMappingVO selectedTransMappingVO;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 開啟檢查視窗
     * 
     * @param targetOrgSid
     * @param checkItemTypeStr
     */
    public void openDepSettingCheckDlg(
            SettingOrgTransMappingVO selectedTransMappingVO,
            String checkItemTypeStr) {

        this.selectedTransMappingVO = selectedTransMappingVO;

        // ====================================
        // DepSettingCheckItemType
        // ====================================
        this.targetCheckItemType = DepSettingCheckItemType.valueOf(checkItemTypeStr);
        if (this.targetCheckItemType == null) {
            String errorMessage = WkMessage.EXECTION;
            log.error(errorMessage + "傳入檢查項目錯誤:[{}]", checkItemTypeStr);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // ====================================
        // 上方顯示文字
        // ====================================
        this.titleContent = ""
                + "<span style='font-weight:900;' class='" + this.targetCheckItemType.getColorStyleClass() + "'>"
                + this.targetCheckItemType.getDescr()
                + "</span>"
                + "：" + WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeupAndDecorationStyle(
                        this.selectedTransMappingVO.getBeforeOrgSid(), OrgLevel.DIVISION_LEVEL, false, " → ");

        // ====================================
        // 準備檢查資料
        // ====================================
        this.prepareCheckItems();

    }

    /**
     * 準備檢查項目資料
     */
    public void prepareCheckItems() {

        // ====================================
        // 公司 sid
        // ====================================
        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
        if (comp == null) {
            MessagesUtils.showWarn(WkMessage.SESSION_TIMEOUT);
            return;
        }
        try {

            // ====================================
            // 取得所有部門設定模版
            // ====================================
            // 查詢
            List<WCConfigTemplet> wcConfigTemplets = this.wcConfigTempletManager.findAll();
            // 索引
            Map<Long, WCConfigTemplet> configTempletMapBySid = wcConfigTemplets.stream()
                    .collect(Collectors.toMap(
                            WCConfigTemplet::getSid, each -> each));

            // ====================================
            // 所有大類
            // ====================================
            List<WCCategoryTag> categoryTags = this.wCCategoryTagCache.findAllAndSortBySeq(
                    SecurityFacade.getCompanyId());

            // ====================================
            // 依據選擇，準備檢查項目
            // ====================================
            switch (this.targetCheckItemType) {
            case MENUTAG_CAN_USE_DEP:
                // 中類 : 可使用單位
                this.allCheckItems = this.setting10DepSettingCheckLogic.findCanUseDepBySecondLevel(
                        comp.getSid(),
                        this.selectedTransMappingVO.getBeforeOrgSid(),
                        this.selectedTransMappingVO.getAfterOrgSid(),
                        categoryTags,
                        configTempletMapBySid,
                        false);
                break;

            case ITEM_USE_DEP:
                // 小類-可使用單位
                this.allCheckItems = this.setting10DepSettingCheckLogic.findCanUseDepBythirdLevel(
                        comp.getSid(),
                        this.selectedTransMappingVO.getBeforeOrgSid(),
                        this.selectedTransMappingVO.getAfterOrgSid(),
                        categoryTags,
                        configTempletMapBySid,
                        false);

                break;
            case ITEM_CAN_VIEW_DEP:
                // 小類-預設可閱單位
                this.allCheckItems = this.setting10DepSettingCheckLogic.findCanViewDepBythirdLevel(
                        comp.getSid(),
                        this.selectedTransMappingVO.getBeforeOrgSid(),
                        this.selectedTransMappingVO.getAfterOrgSid(),
                        categoryTags,
                        configTempletMapBySid,
                        false);

                break;
            case EXEC_DEP:
                // 執行單位
                this.allCheckItems = this.setting10DepSettingCheckLogic.findExecDep(
                        this.selectedTransMappingVO.getBeforeOrgSid(),
                        categoryTags,
                        false);

                break;

            default:
                MessagesUtils.showInfo("未設定的檢查類型:[" + this.targetCheckItemType + "]");
                return;
            }

            // ====================================
            // 處理隱藏項目
            // ====================================
            this.prepareHideItem();

            // ====================================
            // 畫面控制
            // ====================================
            this.displayController.update(this.DLG_CONTENT);
            this.displayController.showPfWidgetVar(this.DLG_WV);

        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(" + e.getMessage() + ")";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }
    }

    /**
     * 更新執行單位項目
     */
    public void reflashExecDepCheckItems() {
        if (WkStringUtils.isEmpty(this.allCheckItems)) {
            return;
        }

        // 收集執行單位依舊存在的資料
        List<Setting10DepSettingCheckItemVO> tempCheckItems = Lists.newArrayList();
        for (Setting10DepSettingCheckItemVO checkItemVO : this.allCheckItems) {
            // 查詢對應執行單位
            List<WCExecDepSetting> wcExecDepSettings = this.wcExecDepSettingCache.findByTagSid(checkItemVO.getTagSid());
            if (WkStringUtils.isEmpty(wcExecDepSettings)) {
                continue;
            }

            for (WCExecDepSetting wcExecDepSetting : wcExecDepSettings) {
                if (WkCommonUtils.compareByStr(
                        wcExecDepSetting.getExec_dep_sid(),
                        this.selectedTransMappingVO.getBeforeOrgSid())) {
                    tempCheckItems.add(checkItemVO);
                }
            }
        }

        // 比較數量不相等時，需更新畫面資料
        if (!WkCommonUtils.compareByStr(this.allCheckItems.size(), tempCheckItems.size())) {
            this.allCheckItems = tempCheckItems;
            this.displayController.update(this.DLG_CONTENT);
        }

    }

    /**
     * 處理隱藏項目
     */
    public void prepareHideItem() {
        // 複製一份完整版
        this.showCheckItems = this.allCheckItems.stream().collect(Collectors.toList());
        // 不顯示未設定
        if (this.hideNoSettingItem) {
            this.showCheckItems = this.showCheckItems.stream()
                    .filter(item -> !DepSettingCheckSettingType.EMPTY.equals(item.getSettingType()))
                    .collect(Collectors.toList());
        }

        // 不顯示模版
        if (this.hideTempleteItem) {
            this.showCheckItems = this.showCheckItems.stream()
                    .filter(item -> !DepSettingCheckSettingType.TEMPLETE.equals(item.getSettingType()))
                    .collect(Collectors.toList());
        }
    }

    /**
     * 開啟設定視窗
     * 
     * @param checkItemTypeStr
     * @param itemVO
     */
    public void openSettingDlg(
            Setting10DepSettingCheckItemVO itemVO) {

        // ====================================
        // 依據檢查項目類型作動
        // ====================================
        switch (this.targetCheckItemType) {
        case MENUTAG_CAN_USE_DEP:
            setting3Bean.setFullversion(false);
            setting3Bean.setSelTagSearchVO(new CategoryTagSearchVO(itemVO.getCategorySid()));
            setting3Bean.setSelMenuTagSearchVO(new MenuTagSearchVO(itemVO.getMenuSid()));
            setting3Bean.loadMenuTag();
            break;

        case ITEM_USE_DEP:
        case ITEM_CAN_VIEW_DEP:
            setting3Bean.setFullversion(false);
            setting3Bean.doSearch();
            setting3Bean.loadMenuByCategorySid(itemVO.getCategorySid());
            setting3Bean.searchMenuItems();
            setting3Bean.loadSubTagByMenuSid(itemVO.getMenuSid());
            setting3Bean.setSelSubTagSearchVO(new TagSearchVO(itemVO.getTagSid()));
            setting3Bean.loadSubTag();
            this.displayController.hidePfWidgetVar("menu_list_dlg_wv");
            this.displayController.hidePfWidgetVar("tag_list_dlg_wv");
            break;

        case EXEC_DEP:

            setting3Bean.setFullversion(false);
            setting3Bean.doSearch();
            setting3Bean.loadMenuByCategorySid(itemVO.getCategorySid());
            setting3Bean.searchMenuItems();
            setting3Bean.loadSubTagByMenuSid(itemVO.getMenuSid());
            setting3Bean.searchTagItems();
            setting3Bean.loadExecDepBySubTagSid(itemVO.getTagSid());
            this.displayController.hidePfWidgetVar("menu_list_dlg_wv");
            this.displayController.hidePfWidgetVar("tag_list_dlg_wv");
            break;

        default:
            break;
        }

        // this.editRow = rowTagVO;
        // this.editIndex = allTagDt.indexOf(this.editRow);
        // switch (rowTagVO.getTagType()) {
        // case "CategoryTag":
        // setting3Bean.setSelTagSearchVO(new CategoryTagSearchVO(rowTagVO.getSid()));
        // setting3Bean.loadCategoryTag();
        // break;
        // case "MenuTag":
        // setting3Bean.setFullversion(false);
        // setting3Bean.setSelTagSearchVO(new CategoryTagSearchVO(rowTagVO.getCategorySid()));
        // setting3Bean.setSelMenuTagSearchVO(new MenuTagSearchVO(rowTagVO.getMenuSid()));
        // setting3Bean.loadMenuTag();
        // break;
        // case "Tag":
        // setting3Bean.setFullversion(false);
        // setting3Bean.doSearch();
        // setting3Bean.loadMenuByCategorySid(rowTagVO.getCategorySid());
        // setting3Bean.searchMenuItems();
        // setting3Bean.loadSubTagByMenuSid(rowTagVO.getMenuSid());
        // setting3Bean.setSelSubTagSearchVO(new TagSearchVO(rowTagVO.getSid()));
        // setting3Bean.loadSubTag();
        // this.displayController.hidePfWidgetVar("menu_list_dlg_wv");
        // this.displayController.hidePfWidgetVar("tag_list_dlg_wv");
        // break;
        // default:
        // break;
        // }
    }

}
