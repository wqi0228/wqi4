package com.cy.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkStringUtils;

public class UserConverter extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {
        String userID = SecurityFacade.getUserId();
        if (WkStringUtils.notEmpty(userID)) {
            return userID;
        }
        return "NO_USER";
    }
}
