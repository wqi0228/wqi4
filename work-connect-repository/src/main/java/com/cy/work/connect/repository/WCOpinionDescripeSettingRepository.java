/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author brain0925_liao
 */
public interface WCOpinionDescripeSettingRepository
    extends JpaRepository<WCOpinionDescripeSetting, String>, Serializable {

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.wcSid = :wcSid")
    List<WCOpinionDescripeSetting> findOneByWcSid(@Param("wcSid") String wcSid);

    @Modifying
    @Query(
        "SELECT  g FROM #{#entityName} g WHERE g.sourceType = :sourceType AND g.sourceSid = :sourceSid AND g.opinion_usr =:opinion_usr")
    @Transactional
    List<WCOpinionDescripeSetting> getOpinionDescripeSetting(
        @Param("sourceType") ManagerSingInfoType managerSingInfoType,
        @Param("sourceSid") String sourceSid,
        @Param("opinion_usr") Integer opinion_usr);
}
