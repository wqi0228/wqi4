/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.commons.vo.Role;
import com.cy.work.connect.vo.SpecificRoleInquireDep;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author brain0925_liao
 */
public interface SpecificRoleInquireDepRepository
    extends JpaRepository<SpecificRoleInquireDep, Long>, Serializable {

    List<SpecificRoleInquireDep> findByRoleIn(List<Role> roles);
}
