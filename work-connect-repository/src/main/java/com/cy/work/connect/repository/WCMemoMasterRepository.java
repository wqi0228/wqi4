/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCMemoMaster;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 工作聯絡單主檔
 *
 * @author brain0925_liao
 */
public interface WCMemoMasterRepository extends JpaRepository<WCMemoMaster, String>, Serializable {

    @Query("SELECT r FROM #{#entityName} r WHERE r.wcMemoNo = :wcMemoNo")
    List<WCMemoMaster> findByNo(@Param("wcMemoNo") String wcMemoNo);

    @Query("SELECT r FROM #{#entityName} r WHERE r.create_usr = :create_usr")
    List<WCMemoMaster> findByCreateUserSid(@Param("create_usr") Integer create_usr);

    /**
     * 搜尋備忘錄[提交]單號 By 建立日期區間
     */
    @Query(
        "SELECT wcMemoNo FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end order by r.create_dt DESC")
    List<String> findTodayLastWCMemoMasterNo(
        @Param("start") Date start,
        @Param("end") Date end);

    /**
     * 搜尋備忘錄筆數[提交] By 建立日期區間
     */
    @Query("SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end")
    Integer findTodayWCMemoMasterTotalSize(@Param("start") Date start,
        @Param("end") Date end);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} r SET r.lock_dt  = :lock_dt,r.lock_usr = :lock_usr "
            + " WHERE r.sid = :sid")
    int updateLockUserAndLockDate(
        @Param("lock_dt") Date lock_dt,
        @Param("lock_usr") Integer lock_usr,
        @Param("sid") String sid);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} r SET r.lock_dt  = :lock_dt,r.lock_usr = :lock_usr "
            + " WHERE r.sid = :sid and r.lock_usr  = :closeUserSid")
    int closeLockUserAndLockDate(
        @Param("lock_dt") Date lock_dt,
        @Param("lock_usr") Integer lock_usr,
        @Param("sid") String sid,
        @Param("closeUserSid") Integer closeUserSid);
}
