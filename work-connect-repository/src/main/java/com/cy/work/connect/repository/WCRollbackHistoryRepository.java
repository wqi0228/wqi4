/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCRollbackHistory;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author brain0925_liao
 */
public interface WCRollbackHistoryRepository
    extends JpaRepository<WCRollbackHistory, String>, Serializable {

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.wcSid = :wcSid AND w.userSid = :userSid")
    List<WCRollbackHistory> findByWcSidAndUserSid(
        @Param("wcSid") String wcSid,
        @Param("userSid") Integer userSid);
}
