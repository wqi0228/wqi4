/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCTag;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作聯絡單標籤
 *
 * @author brain0925_liao
 */
public interface WCTagRepository extends JpaRepository<WCTag, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param wcMenuTagSids 中類 sid
     * @return WCTag list
     */
    public List<WCTag> findByWcMenuTagSidIn(List<String> wcMenuTagSids);

    /**
     * 搜尋標籤資料
     *
     * @return
     */
    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 ")
    List<WCTag> getWCTagByStatus();

    /**
     * 以 sid 查詢
     * 
     * @param sids sid list
     * @return WCTag list
     */
    List<WCTag> findBySidIn(Collection<String> sids);

    /**
     * 以-可使用單位-模版SID查詢
     *
     * @param canUserDepTempletSid 小類(執行項目)之可使用單位模版Sid
     * @return
     */
    List<WCTag> findByCanUserDepTempletSid(Long canUserDepTempletSid);

    /**
     * 以-預設可閱部門-模版SID查詢
     *
     * @param useDepTempletSid 小類(執行項目)之可閱部門模版Sid
     * @return
     */
    public List<WCTag> findByUseDepTempletSid(Long useDepTempletSid);

    /**
     * 查詢傳入的小類中，有多少需要上傳檔案
     * 
     * @param sids
     * @return
     */
    @Query(value = ""
            + "SELECT Count(*) "
            + "FROM   wc_tag "
            + "WHERE  status = 0 "
            + "       AND wc_tag_sid IN ( :sids ) "
            + "       AND ( legitimate_range_files = 1 "
            + "              OR start_end_files = 1 )", nativeQuery = true)
    public Integer countIsNeedUpdateFile(@Param("sids") List<String> sids);

    /**
     * 查詢傳入小類中，需要做日期檢查的項目
     * 
     * @param sids 小類 sid
     * @return WCTag list
     */
    @Query(value = ""
            + "SELECT e "
            + "FROM   #{#entityName} e "
            + "WHERE  sid IN ( :sids ) "
            + "       AND ( e.legitimateRangeCheck = 1 "
            + "              OR startEndCheck = 1 )")
    public List<WCTag> findNeedDateCheckBySids(List<String> sids);

    @Query(value = "SELECT IF(:tagName is null, IF(:menuName is null,COUNT(distinct ct.wc_category_tag_sid),COUNT(distinct mt.wc_menu_tag_sid)),COUNT(distinct tt.wc_tag_sid)) "
            + "FROM wc_category_tag ct "
            + "LEFT JOIN wc_menu_tag mt ON mt.wc_category_tag_sid = ct.wc_category_tag_sid "
            + "LEFT JOIN wc_tag tt ON tt.wc_menu_tag_sid = mt.wc_menu_tag_sid "
            + "WHERE ct.compSid = :companySid "
            + "AND (ifnull(:categoryName, null) is null OR ct.category_name = :categoryName) "
            + "AND (ifnull(:menuName, null) is null OR mt.menu_name = :menuName) "
            + "AND (ifnull(:tagName, null) is null OR tt.tag_name = :tagName) ", nativeQuery = true)
    Integer countByTagNameAndCompanySid(
            @Param("companySid") Integer companySid,
            @Param("categoryName") String categoryName,
            @Param("menuName") String menuName,
            @Param("tagName") String tagName);

    @Query(value = "SELECT tt.* "
            + "FROM wc_category_tag ct "
            + "JOIN wc_menu_tag mt ON mt.wc_category_tag_sid = ct.wc_category_tag_sid "
            + "JOIN wc_tag tt ON tt.wc_menu_tag_sid = mt.wc_menu_tag_sid "
            + "WHERE ct.compSid = :companySid "
            + "AND (ifnull(:categoryName, null) is null OR ct.category_name = :categoryName) "
            + "AND (ifnull(:menuName, null) is null OR mt.menu_name = :menuName) "
            + "AND (ifnull(:tagName, null) is null OR tt.tag_name = :tagName) ", nativeQuery = true)
    List<WCTag> findByTagNameAndCompanySid(
            @Param("companySid") Integer companySid,
            @Param("categoryName") String categoryName,
            @Param("menuName") String menuName,
            @Param("tagName") String tagName);

    /**
     * 顯示分類清單 - 依單據名稱Sid查詢
     *
     * @param menuTagSid 中類(單據名稱)Sid
     * @return
     */
    @Query(value = "SELECT distinct tagType "
            + "FROM wc_tag "
            + "WHERE wc_menu_tag_sid = :menuTagSid "
            + "AND status = 0 "
            + "AND tagType is not null "
            + "AND tagType <> '' "
            + "ORDER BY tagType ", nativeQuery = true)
    List<String> findDistinctTagTypeByMenuTagSid(@Param("menuTagSid") String menuTagSid);
}
