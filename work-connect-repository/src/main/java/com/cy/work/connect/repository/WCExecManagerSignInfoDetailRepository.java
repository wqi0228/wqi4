/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.WCExecManagerSignInfoDetail;
import com.cy.work.connect.vo.enums.SignActionType;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author brain0925_liao
 */
public interface WCExecManagerSignInfoDetailRepository
    extends JpaRepository<WCExecManagerSignInfoDetail, String>, Serializable {

    @Modifying
    @Query(
        "UPDATE #{#entityName} g SET g.status = :status "
            + " WHERE g.wc_exec_manager_sign_info_sid = :wc_exec_manager_sign_info_sid")
    @Transactional
    int updateStatus(
        @Param("status") Activation status,
        @Param("wc_exec_manager_sign_info_sid") String wc_exec_manager_sign_info_sid);

    @Modifying
    @Query(
        "UPDATE #{#entityName} g SET g.signActionType = :signActionType "
            + " WHERE g.wc_exec_manager_sign_info_sid = :wc_exec_manager_sign_info_sid AND g.user_sid = :user_sid")
    @Transactional
    int updateSignActionType(
        @Param("signActionType") SignActionType signActionType,
        @Param("wc_exec_manager_sign_info_sid") String wc_exec_manager_sign_info_sid,
        @Param("user_sid") Integer user_sid);
}
