package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 部門模版設定 Repository
 *
 * @author allen
 */
public interface WCConfigTempletRepository
    extends JpaRepository<WCConfigTemplet, Long>, Serializable {

    /**
     * 以模版類型及公司Sid查詢
     *
     * @param templetType 模版類型
     * @param compSid     公司Sid
     * @return
     */
    List<WCConfigTemplet> findByTempletTypeAndCompSid(
        ConfigTempletType templetType,
        Integer compSid);

    /**
     * 以公司Sid查詢
     *
     * @param compSid 公司Sid
     * @return
     */
    List<WCConfigTemplet> findByCompSid(Integer compSid);
}
