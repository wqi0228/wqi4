/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCExecDepSetting;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author brain0925_liao
 */
public interface WCExecDepSettingRepository extends JpaRepository<WCExecDepSetting, String>, Serializable {

    /**
     * 以 Sid查詢
     * 
     * @param sid SID
     * @return WCExecDepSetting
     */
    public WCExecDepSetting findBySid(String sid);

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.wc_tag_sid = :wc_tag_sid and w.status = 0 ")
    List<WCExecDepSetting> findByWcTagSid(@Param("wc_tag_sid") String wc_tag_sid);

    @Query(value = "SELECT CASE WHEN (COUNT(*) > 0) THEN 'TRUE' ELSE 'FALSE' END "
            + "FROM   wc_exec_dep_setting ex "
            + "JOIN   wc_tag tag ON tag.wc_tag_sid = ex.wc_tag_sid "
            + "JOIN   wc_menu_tag menuTag ON menuTag.wc_menu_tag_sid = tag.wc_menu_tag_sid "
            + "JOIN   wc_category_tag categoryTag ON categoryTag.wc_category_tag_sid = menuTag.wc_category_tag_sid "
            + "WHERE  JSON_CONTAINS(JSON_EXTRACT(ex.manager_recevice, \"$[*].sid\"), :manager) =  1 "
            + "AND categoryTag.status = 0 "
            + "AND menuTag.status = 0 "
            + "AND tag.status = 0 "
            + "AND ex.status = 0", nativeQuery = true)
    Boolean isManagerRecevice(@Param("manager") Integer manager);
}
