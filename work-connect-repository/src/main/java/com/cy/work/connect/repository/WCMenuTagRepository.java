/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCMenuTag;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author brain0925_liao
 */
public interface WCMenuTagRepository extends JpaRepository<WCMenuTag, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param wcCategoryTagSids 大類 sid
     * @return WCMenuTag list
     */
    public List<WCMenuTag> findByWcCategoryTagSidIn(List<String> wcCategoryTagSids);

    @Query("SELECT CASE WHEN max(o.seq) is null THEN 1 ELSE (max(o.seq)+1) END "
            + " FROM #{#entityName} o "
            + " WHERE o.wcCategoryTagSid=:categorySid ")
    Integer getMenuTagNextSeqByCategory(@Param("categorySid") String categorySid);

    @Query("SELECT o "
            + " FROM #{#entityName} o "
            + " WHERE o.wcCategoryTagSid=:categorySid and o.menuName = :name")
    WCMenuTag findMenuTagByCategoryAndName(
            @Param("categorySid") String categorySid,
            @Param("name") String name);

    /**
     * 以可使用單位模版SID查詢
     *
     * @param useDepTempletSid
     * @return
     */
    List<WCMenuTag> findByUseDepTempletSid(Long useDepTempletSid);

    @Query(value = "SELECT mt.* "
            + "FROM wc_category_tag ct "
            + "LEFT JOIN wc_menu_tag mt ON mt.wc_category_tag_sid = ct.wc_category_tag_sid "
            + "LEFT JOIN wc_tag tt ON tt.wc_menu_tag_sid = mt.wc_menu_tag_sid "
            + "WHERE ct.compSid = :companySid "
            + "AND (ifnull(:categoryName, null) is null OR ct.category_name = :categoryName) "
            + "AND (ifnull(:menuName, null) is null OR mt.menu_name = :menuName) "
            + "AND (ifnull(:tagName, null) is null OR tt.tag_name = :tagName) ", nativeQuery = true)
    List<WCMenuTag> findByTagNameAndCompanySid(
            @Param("companySid") Integer companySid,
            @Param("categoryName") String categoryName,
            @Param("menuName") String menuName,
            @Param("tagName") String tagName);

    @Query(value = ""
            + "SELECT mt.* "
            + "  FROM wc_category_tag ct "
            + "  JOIN wc_menu_tag mt ON mt.wc_category_tag_sid = ct.wc_category_tag_sid "
            + " WHERE ct.compSid = :companySid "
            + "   AND ct.status = 0 "
            + "   AND mt.status = 0 "
            + "   AND EXISTS ( "
            + "                SELECT 1 FROM wc_tag tt "
            + "                 WHERE tt.wc_menu_tag_sid = mt.wc_menu_tag_sid "
            + "                   AND tt.status = 0 "
            + "                GROUP BY tt.wc_menu_tag_sid "
            + "                HAVING count(*) > 1"
            + "               ) "
            + " ORDER BY ct.seq,menu_name", nativeQuery = true)
    List<WCMenuTag> findMutiTagByCompanySid(@Param("companySid") Integer companySid);
}
