package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCSetGroup;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 工作聯絡單 設定群組 Repository
 *
 * @author jimmy_chou
 */
public interface WCSetGroupRepository extends JpaRepository<WCSetGroup, Long>, Serializable {

    /**
     * 以SID查詢
     *
     * @param sid
     * @return
     */
    WCSetGroup findBySid(Long sid);

    /**
     * 以公司SID查詢
     *
     * @param compSid
     * @return
     */
    List<WCSetGroup> findByCompSid(Integer compSid);
}
