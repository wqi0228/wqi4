/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCMemoAttachment;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 備忘錄 - 附件
 *
 * @author brain0925_liao
 */
public interface WCMemoAttachmentRepository
    extends JpaRepository<WCMemoAttachment, String>, Serializable {

    /**
     * 搜尋附件List By 備忘錄Sid
     */
    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 " + "AND o.wcMemoSid = :wcMemoSid")
    List<WCMemoAttachment> getWCAttachmentByWCMemoSid(@Param("wcMemoSid") String wcMemoSid);

    /**
     * 更新附件資訊 By 附件Sid
     */
    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} r SET r.wcMemoSid  = :wcMemoSid , r.wcMemoNo = :wcMemoNo ,r.description = :description , r.update_usr_sid = :update_usr_sid , r.update_dt = :update_dt"
            + " WHERE r.sid = :sid")
    int updateWCMemoMappingInfo(
        @Param("wcMemoSid") String wcMemoSid,
        @Param("wcMemoNo") String wcMemoNo,
        @Param("description") String description,
        @Param("update_usr_sid") Integer update_usr_sid,
        @Param("update_dt") Date update_dt,
        @Param("sid") String sid);
}
