/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCFavorite;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作聯絡單 - 收藏
 *
 * @author brain0925_liao
 */
public interface WCFavoriteRepository extends JpaRepository<WCFavorite, String>, Serializable {

    /**
     * 搜尋收藏紀錄by 工作聯絡單Sid
     */
    @Query("SELECT r FROM #{#entityName} r WHERE  r.wc_sid = :wc_sid")
    List<WCFavorite> findByWCSid(@Param("wc_sid") String wc_sid);

    /**
     * 搜尋收藏紀錄by 使用者與工作聯絡單Sid
     */
    @Query("SELECT r FROM #{#entityName} r WHERE r.create_usr = :create_usr  AND r.wc_sid = :wc_sid")
    List<WCFavorite> findByUserSidAndWCSid(
        @Param("wc_sid") String wc_sid,
        @Param("create_usr") Integer create_usr);

    /**
     * 搜尋收藏紀錄筆數 By 收藏時間區間及使用者
     */
    @Query(
        "SELECT COUNT(r) FROM #{#entityName} r WHERE r.create_usr = :create_usr AND r.status = 0 AND r.update_dt >= :startDate AND r.update_dt <= :endDate ")
    Integer findFavoriteCountByUserSidAndUpdateDt(
        @Param("startDate") Date startDate,
        @Param("endDate") Date endDate,
        @Param("create_usr") Integer create_usr);
}
