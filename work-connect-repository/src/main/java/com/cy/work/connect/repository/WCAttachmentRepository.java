/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCAttachment;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 工作聯絡單 - 附件
 *
 * @author brain0925_liao
 */
public interface WCAttachmentRepository extends JpaRepository<WCAttachment, String>, Serializable {

    /**
     * 搜尋附件List By 工作聯絡單Sid
     */
    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 " + "AND o.wc_sid = :wc_sid")
    List<WCAttachment> getWCAttachmentByWCSid(@Param("wc_sid") String wc_sid);
    
    /**
     * 計算單據的上傳檔案數量
     * @param wcSid
     * @return
     */
    @Query(value = ""
            + "SELECT COUNT(*) "
            + "FROM wc_attachment "
            + "WHERE status = 0 "
            + "  AND wc_sid = :wcSid ",
            nativeQuery = true)
    public Integer countByWcSid( @Param("wcSid") String wcSid);

    /**
     * 更新附件資訊 By 附件Sid
     */
    @Modifying
    @Transactional
    @Query(""
            + "UPDATE #{#entityName} r "
            + "SET r.wc_sid  = :wc_sid , "
            + "    r.wc_no = :wc_no , "
            + "    r.description = :description , "
            + "    r.update_usr_sid = :update_usr_sid , "
            + "    r.update_dt = :update_dt "
            + "WHERE r.sid = :sid ")
    int updateWCMasterMappingInfo(
            @Param("wc_sid") String wc_sid,
            @Param("wc_no") String wc_no,
            @Param("description") String description,
            @Param("update_usr_sid") Integer update_usr_sid,
            @Param("update_dt") Date update_dt,
            @Param("sid") String sid);
}
