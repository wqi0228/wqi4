/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCReadReceipts;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 工作聯絡單 可閱名單
 *
 * @author jimmy_chou
 */
public interface WCReadReceiptsRepository
        extends JpaRepository<WCReadReceipts, String>, Serializable {

    List<WCReadReceipts> findByWcsid(String wcsid);

    @Query(value = ""
            + "SELECT count(*) "
            + "  FROM wc_read_receipts  "
            + "  WHERE wc_sid= :wcSid "
            + "    AND reader = :userSid ", nativeQuery = true)
    public Long countByWcsidAndReader(
            @Param("wcSid") String wcSid,
            @Param("userSid") Integer userSid);

    @Query("SELECT o " + " FROM #{#entityName} o " + " WHERE o.wcsid=:wcsid and o.reader = :reader")
    WCReadReceipts findReadReceiptsByWCSidAndReader(
            @Param("wcsid") String wcsid,
            @Param("reader") Integer reader);

    @Query("SELECT o " + " FROM #{#entityName} o "
            + " WHERE o.wcsid=:wcsid and o.reader in :readerList")
    List<WCReadReceipts> findReadReceiptsByWCSidAndReaderList(
            @Param("wcsid") String wcsid,
            @Param("readerList") List<Integer> readerList);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.readtime = NOW(), r.readreceipt = com.cy.work.connect.vo.enums.WCReadReceiptStatus.READ "
            + " WHERE r.reader = :reader and r.readtime is null and r.readreceipt = com.cy.work.connect.vo.enums.WCReadReceiptStatus.UNREAD")
    int updateReadTime(@Param("reader") Integer reader);
}
