/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCMemoTrace;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 備忘錄追蹤
 *
 * @author brain0925_liao
 */
public interface WCMemoTraceRepository extends JpaRepository<WCMemoTrace, String>, Serializable {

    /**
     * 搜尋備忘錄追蹤 BY 備忘錄Sid
     */
    @Query(
        "SELECT o FROM #{#entityName} o WHERE o.status=0 and o.wcMemoSid = :wcMemoSid ORDER BY o.update_dt DESC ")
    List<WCMemoTrace> getWCTraceByWcMemoSid(@Param("wcMemoSid") String wcMemoSid);
}
