/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao Org物件管理器
 */
@Component
@Slf4j
public class WorkSettingOrgManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5196475530912508924L;

    private static WorkSettingOrgManager instance;
    /**
     * Org物件Cache管理器
     */
    @Autowired
    private WkOrgCache wkOrgCache;

    public static WorkSettingOrgManager getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WorkSettingOrgManager.instance = this;
    }

    /**
     * * 取得該部門所有上層部門 & 取得該部門所有子部門 & 自己
     *
     * @param dep 部門
     * @return
     */
    public List<Org> getAllParentAndChildAndOneself(Org dep) {
        Set<Org> orgs = Sets.newHashSet(dep);
        orgs.addAll(this.wkOrgCache.findAllParent(dep.getSid()));
        orgs.addAll(
                this.wkOrgCache.findAllChild(dep.getSid()).stream()
                        .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                        .collect(Collectors.toList()));
        return Lists.newArrayList(orgs);
    }

    /**
     * 取得全部資料
     *
     * @return
     */
    public List<Integer> findAllSid() {
        return this.fromOrgsToSids(this.wkOrgCache.findAll());
    }

    /**
     * 轉換為org sid
     *
     * @param target
     * @return
     */
    public List<Integer> fromOrgsToSids(List<Org> target) {
        if (target == null || target.isEmpty()) {
            return Lists.newArrayList();
        }
        Set<Integer> sids = Sets.newHashSet();
        target.forEach(
                each -> {
                    sids.add(each.getSid());
                });
        return Lists.newArrayList(sids);
    }

    /**
     * 轉換為org
     *
     * @param target
     * @return
     */
    public List<Org> fromOrgSidsToOrgs(List<Integer> target) {
        if (target == null || target.isEmpty()) {
            return Lists.newArrayList();
        }
        Set<Org> orgs = Sets.newHashSet();
        wkOrgCache.findAll().stream()
                .forEach(
                        each -> {
                            if (target.contains(each.getSid())) {
                                orgs.add(each);
                            }
                        });
        return Lists.newArrayList(orgs);
    }

    /**
     * 比對是否為父單位
     *
     * @param targetOrgSID
     * @param parentOrgSID
     * @return
     */
    public boolean compareParent(Integer targetOrgSID, Integer parentOrgSID) {
        Integer targetParentOrgSID = this.getParentByOrg(targetOrgSID);
        if (targetParentOrgSID == null) {
            return false;
        }
        return targetParentOrgSID.equals(parentOrgSID);
    }

    /**
     * 取得Org Parent
     *
     * @param sid
     * @return
     */
    public Integer getParentByOrg(Integer sid) {
        Org result = this.wkOrgCache.findBySid(sid);
        if (result == null) {
            // log.debug("result為null，無法取得Org Parent，sid=" + sid + "。");
            return null;
        }
        if (result.getParent() == null) {
            // log.debug("Parent為null，無法取得Org Parent，sid=" + sid + "。");
            return null;
        }
        return result.getParent().getSid();
    }

    /**
     * 取得Org Status
     *
     * @param sid
     * @return
     */
    public Activation getStatusByOrg(Integer sid) {
        Org result = this.wkOrgCache.findBySid(sid);
        if (result == null) {
            log.debug("result為null，無法取得Org Status，sid=" + sid + "。");
            return null;
        }
        return result.getStatus();
    }

    /**
     * @param dep
     * @param isDepContainFollowing
     * @return
     */
    public String prepareDepsShowTextByOrgs(
            List<Org> dep, boolean isDepContainFollowing, boolean isExecpt) {
        List<Integer> depSids = Lists.newArrayList();
        if (WkStringUtils.notEmpty(dep)) {
            depSids = dep.stream().map(Org::getSid).collect(Collectors.toList());
        }
        return this.prepareDepsShowText(depSids, isDepContainFollowing, isExecpt);
    }

    /**
     * 組選擇部門的顯示文字
     *
     * @param depSids               選擇部門
     * @param isDepContainFollowing 是否含以下
     * @param isExecpt              是否為：被選擇單位除外
     * @return
     */
    public String prepareDepsShowText(
            List<Integer> depSids, boolean isDepContainFollowing, boolean isExecpt) {

        if (depSids == null) {
            depSids = Lists.newArrayList();
        }

        // clone 以免影響到外部參數
        List<Integer> newDepSids = depSids.stream().collect(Collectors.toList());

        // ====================================
        // 取得『以下部門』
        // ====================================
        if (isDepContainFollowing) {
            newDepSids = Lists.newArrayList(WkOrgUtils.prepareAllDepsByTopmost(newDepSids, false));
        }

        // ====================================
        // 若為『含以下』, 僅留最上層部門
        // ====================================
        if (isDepContainFollowing) {
            // 過濾後僅留『最上層部門』
            newDepSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(newDepSids));
        }

        // ====================================
        // 取得公司別
        // ====================================
        Integer compSid = null;

        if (WkStringUtils.notEmpty(newDepSids)) {
            for (Integer depSid : newDepSids) {
                Org sameoneOrg = this.wkOrgCache.findBySid(depSid);
                if (sameoneOrg != null && sameoneOrg.getCompany() != null) {
                    compSid = sameoneOrg.getCompany().getSid();
                    break;
                }
            }
            if (compSid == null) {
                log.warn("無法由傳入部門清單, 取得公司別！:" + new Gson().toJson(newDepSids));
                return "無法由傳入部門清單, 取得公司別";
            }
        }

        // ====================================
        // 排序
        // ====================================
        // 依組織樹順序排序
        newDepSids = WkOrgUtils.sortByOrgTree(newDepSids);

        // ====================================
        // 組顯示文字
        // ====================================
        String showContent = "";
        for (Integer depSid : newDepSids) {
            if (depSid == null) {
                continue;
            }
            Org dep = this.wkOrgCache.findBySid(depSid);
            if (dep == null) {
                continue;
            }

            String currDepStr = WkOrgUtils.prepareBreadcrumbsByDepName(dep.getSid(), OrgLevel.DIVISION_LEVEL, true,
                    "-");

            if (WkStringUtils.notEmpty(currDepStr)) {
                showContent += "<br/>";

                if (Activation.ACTIVE.equals(dep.getStatus())) {
                    showContent += currDepStr;

                } else {
                    showContent += "<span class='WS1-1-2' style='text-decoration: line-through;'>(停用)&nbsp;";
                    showContent += "<span class='WS1-1-1' style='text-decoration: none;'>" + currDepStr
                            + "</span>";
                    showContent += "</span>";
                }

                if (isDepContainFollowing || isExecpt) {
                    showContent += "【";
                    if (isExecpt) {
                        showContent += "<span class='WS1-1-2'>除外</span>";
                    }
                    if (isDepContainFollowing) {
                        if (isExecpt) {
                            showContent += "-";
                        }
                        showContent += "<span class='WS1-1-3'>含以下</span>";
                    }
                    showContent += "】";
                }
            }
        }
        if (WkStringUtils.notEmpty(showContent)) {
            showContent = showContent.substring(5);
        }
        return showContent;
    }

    /**
     * 取得傳入單位以外所有單位 [不含停用單位]
     *
     * @param exceptDepSids 除外單位
     * @param loginCompSid  登入公司 sid
     * @return
     */
    public List<Integer> findAllWhitOutDeps(List<Integer> exceptDepSids, Integer loginCompSid) {
        return this.findAllWhitOutDeps(exceptDepSids, loginCompSid, false);
    }

    /**
     * 取得傳入單位以外所有單位
     * 
     * @param exceptDepSids     除外單位
     * @param loginCompSid      登入公司 sid
     * @param isWithOutInactive 是否排除停用單位
     * @return
     */
    public List<Integer> findAllWhitOutDeps(
            List<Integer> exceptDepSids,
            Integer loginCompSid,
            boolean isWithOutInactive) {

        Org compOrg = this.wkOrgCache.findBySid(loginCompSid);

        if (compOrg == null) {
            log.warn("傳入的公司別不存在! loginCompSid:[" + loginCompSid + "]");
            return Lists.newArrayList();
        }

        // 取得所有的單位
        List<Org> allOrgs = this.wkOrgCache.findAllChild(compOrg.getSid()).stream()
                .collect(Collectors.toList());

        // 防呆
        if (WkStringUtils.isEmpty(allOrgs)) {
            allOrgs = Lists.newArrayList();
        }

        // 防呆
        if (WkStringUtils.isEmpty(exceptDepSids)) {
            exceptDepSids = Lists.newArrayList();
        }

        // 結果容器
        List<Integer> resultOrgSids = Lists.newArrayList();
        // 逐筆過濾
        for (Org org : allOrgs) {
            // 過濾停用
            if (isWithOutInactive
                    && Activation.INACTIVE.equals(org.getStatus())) {
                continue;
            }
            // 過濾已存在
            if (resultOrgSids.contains(org.getSid())) {
                continue;
            }
            // 過濾除外單位
            if (exceptDepSids.contains(org.getSid())) {
                continue;
            }
            resultOrgSids.add(org.getSid());
        }

        return resultOrgSids;

    }

    /**
     * 轉 UserDep
     *
     * @param useDep
     * @return
     */
    public List<Org> convertUserDep(UserDep useDep) {
        List<Org> orgs = Lists.newArrayList();

        // 檢核傳入為空
        if (useDep == null || WkStringUtils.isEmpty(useDep.getUserDepTos())) {
            return orgs;
        }

        for (UserDepTo userDepTo : useDep.getUserDepTos()) {
            try {
                orgs.add(this.wkOrgCache.findBySid(Integer.valueOf(userDepTo.getDepSid())));
            } catch (Exception e) {
                // 失敗時可能為資料錯誤, 略過該筆
                log.warn(
                        "findBySID 時發生失敗! SID:[ " + userDepTo.getDepSid() + "] -> " + e.getMessage(),
                        e);
            }
        }

        return orgs;
    }

    /**
     * 轉 UserDep
     *
     * @param useDep
     * @return
     */
    public List<Integer> convertUserDepToSids(UserDep useDep) {

        List<Integer> depSids = Lists.newArrayList();

        // 檢核傳入為空
        if (useDep == null || WkStringUtils.isEmpty(useDep.getUserDepTos())) {
            return depSids;
        }

        // 轉 sid list
        for (UserDepTo userDepTo : useDep.getUserDepTos()) {
            try {
                depSids.add(Integer.parseInt(userDepTo.getDepSid()));
            } catch (Exception e) {
                // 轉數字失敗時略過
            }
        }

        return depSids;
    }
}
