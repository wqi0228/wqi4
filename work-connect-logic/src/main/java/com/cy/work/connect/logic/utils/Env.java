/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.utils;

import java.io.Serializable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class Env implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -47381667417780677L;

    private static Env instance;

    @Value(value = "${work-connect.attachment.root}")
    private String attachRootPath;

    public static Env getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Env.instance = this;
    }

    public String getAttachmentRoot() {
        return attachRootPath;
    }
}
