/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.query;

import com.cy.commons.vo.User;
import com.cy.work.connect.vo.WCMaster;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component("wcMasterQueryService")
public class WCMasterQueryService implements QueryService<WCMaster>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6132938285384799990L;

    @PersistenceContext
    private transient EntityManager em;

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public List findWithQuery(String sql, Map<String, Object> parameters, User executor) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        List result = query.getResultList();
        return result;
    }
}
