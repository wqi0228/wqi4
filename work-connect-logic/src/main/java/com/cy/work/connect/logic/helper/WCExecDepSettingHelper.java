/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WCExecDepSettingHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2438196653077062323L;

    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;

    /**
     * 檢測欲存檔的執行單位設定是否有與其他執行單位設定衝突
     *
     * @param wc_exec_dep_setting_sid 執行單位設定Sid
     * @param execDep                 挑選的執行單位
     * @param signUserSids            挑選的欲簽核人員
     * @param wc_tag_Sid              執行項目Sid
     */
    public void checkOtherExecDepSetting(
        String wc_exec_dep_setting_sid, Org execDep, List<Integer> signUserSids,
        String wc_tag_Sid) {
        List<WCExecDepSetting> results =
            wcExecDepSettingManager.getExecDepSettingFromCacheByWcTagSid(wc_tag_Sid, Activation.ACTIVE);
        StringBuilder errorSb = new StringBuilder();
        results.forEach(
            item -> {
                if (!Strings.isNullOrEmpty(wc_exec_dep_setting_sid)
                    && item.getSid().equals(wc_exec_dep_setting_sid)) {
                    return;
                }
                if (item.getExec_dep_sid().equals(execDep.getSid())) {
                    if (!Strings.isNullOrEmpty(errorSb.toString())) {
                        errorSb.append("\n");
                    }
                    errorSb.append("執行單位 : " + execDep.getName() + "已存在");
                }
            });
        Preconditions.checkState(Strings.isNullOrEmpty(errorSb.toString()), errorSb.toString());
    }
}
