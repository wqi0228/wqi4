package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCMenuTagCache;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.repository.WCConfigTempletRepository;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.ConfigValueTo;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.enums.TargetType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 部門設定模版維護 Manager
 *
 * @author allen
 */
@Slf4j
@Component
public class WCConfigTempletManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1309454833222199793L;

    private static WCConfigTempletManager instance;
    /**
     *
     */
    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;
    /**
     *
     */
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    /**
     *
     */
    @Autowired
    private WCTagManager wcTagManager;

    @Autowired
    private WkUserCache wkUserCache;
    @Autowired
    private WCMenuTagCache wcMenuTagCache;
    @Autowired
    private WCCategoryTagCache wcCategoryTagCache;
    /**
     *
     */
    @Autowired
    private WCConfigTempletRepository configTempletRepository;

    public static WCConfigTempletManager getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCConfigTempletManager.instance = this;
    }

    /**
     * 以SID ˊ查詢
     *
     * @param sid
     * @return
     */
    public WCConfigTempletVo findBySid(Long sid) {

        // ====================================
        // 查詢
        // ====================================
        if (sid == null) {
            // log.warn("傳入 sid 為 null");
            return null;
        }

        WCConfigTemplet configTemplet = this.configTempletRepository.findOne(sid);
        if (configTemplet == null) {
            log.warn("WcConfigTemplet 找不到指定的資料檔");
            return null;
        }

        // ====================================
        // 轉VO 並回傳
        // ====================================
        return new WCConfigTempletVo(configTemplet);
    }

    /**
     * 依據條件查詢
     *
     * @param userSid
     * @param templetType
     * @param deps
     * @param compSid
     * @return
     */
    public List<WCConfigTempletVo> queryByCondition(
            Integer userSid, ConfigTempletType templetType, List<Integer> deps, Integer compSid) {
        return this.queryByCondition(userSid, templetType, deps, compSid, false);
    }

    /**
     * 依據條件查詢
     *
     * @param userSid
     * @param templetType
     * @param deps
     * @param compSid
     * @param permissionFlg 設定權限篩選
     * @return
     */
    public List<WCConfigTempletVo> queryByCondition(
            Integer userSid,
            ConfigTempletType templetType,
            List<Integer> deps,
            Integer compSid,
            boolean permissionFlg) {

        // ====================================
        // 查詢
        // ====================================
        List<WCConfigTemplet> results = null;
        if (templetType == null) {
            results = this.configTempletRepository.findByCompSid(compSid);
        } else {
            results = this.configTempletRepository.findByTempletTypeAndCompSid(templetType,
                    compSid);
        }

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾查詢條件：部門
        // ====================================
        results = results.stream()
                .filter(
                        templet -> {
                            if (templet.getConfigValue() == null || WkStringUtils.isEmpty(deps)) {
                                return true;
                            }
                            List<Integer> settingDeps = templet.getConfigValue().getDeps();

                            // 部門含以下
                            if (templet.getConfigValue().isDepContainFollowing()) {
                                settingDeps = Lists.newArrayList(WkOrgUtils.prepareAllDepsByTopmost(settingDeps, false));
                            }

                            // 被選擇部門除外
                            if (templet.getConfigValue().isDepExcept()) {
                                settingDeps = this.workSettingOrgManager.findAllWhitOutDeps(settingDeps, compSid);
                            }

                            // 未設定代表全部部門, 故符合
                            if (WkStringUtils.isEmpty(settingDeps)) {
                                switch (templet.getTempletType()) {
                                case MENU_TAG_CAN_USE:
                                case SUB_TAG_CAN_USE:
                                    // 可使用部門為空時, 代表全部可使用. 故包含
                                    return true;
                                case SUB_TAG_CAN_READ:
                                    // 可閱部門為空時, 代表全部不可閱, 故不包含
                                    return false;
                                default:
                                    break;
                                }
                            }

                            // 判斷回傳『符合任一』
                            return !Collections.disjoint(settingDeps, deps);
                        })
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 排序
        // ====================================
        // 1.顯示狀態
        Comparator<WCConfigTemplet> comparator = Comparator.comparing(
                WCConfigTemplet::getTempletType);
        // 2.類別
        comparator = comparator.thenComparing(Comparator.comparing(WCConfigTemplet::getStatus));
        // 3.排序序號
        comparator = comparator.thenComparing(Comparator.comparing(WCConfigTemplet::getSortSeq));
        // 4.建立時間
        comparator = comparator.thenComparing(Comparator.comparing(WCConfigTemplet::getCreateDate));

        results = results.stream().sorted(comparator).collect(Collectors.toList());

        // ====================================
        // 轉成 VO 並回傳
        // ====================================
        // 準備權限判斷物件
        List<TargetType> targetTypes = Lists.newArrayList(TargetType.CONFIG_TEMPLET);
        List<WCSetPermission> permissionAll = WCSetPermissionManager.getInstance().findByCompSidAndTargetTypeIn(compSid, targetTypes);
        Map<String, List<WCSetPermission>> permissionMap = permissionAll.stream()
                .collect(
                        Collectors.groupingBy(
                                each -> each.getTargetType().name().concat(each.getTargetSid())));

        List<WCConfigTempletVo> voResult = Lists.newArrayList();
        for (WCConfigTemplet templet : results) {
            // 判斷設定權限
            if (permissionFlg) {
                List<WCSetPermission> templetPermissionList = permissionMap.get(
                        TargetType.CONFIG_TEMPLET.name().concat(templet.getSid().toString()));
                WCSetPermission templetPermission = WkStringUtils.notEmpty(templetPermissionList) ? templetPermissionList.get(0)
                        : null;
                if (templetPermission != null && !templetPermission.getPermissionGroups()
                        .isEmpty()) {
                    List<Integer> permissionUserSids = WCSetPermissionManager.getInstance()
                            .groupPermissionUserSids(compSid,
                                    templetPermission.getPermissionGroups());
                    if (!permissionUserSids.contains(userSid)) {
                        continue;
                    }
                }
            }

            voResult.add(new WCConfigTempletVo(templet));
        }
        return voResult;
    }

    /**
     * @param templetVO
     * @param loginUserSid
     * @param loginUserCompSid
     * @throws Exception
     */
    public void saveSetting(
            WCConfigTempletVo templetVO, Integer loginUserSid, Integer loginUserCompSid)
            throws Exception {

        Date sysDate = new Date();

        // ====================================
        // 準備主檔 DB entities
        // ====================================
        WCConfigTemplet configTemplet;

        if (WkStringUtils.isEmpty(templetVO.getSid())) {
            // 為新增
            configTemplet = new WCConfigTemplet();
            // 公司別 （避免不同的公司混用）
            configTemplet.setCompSid(loginUserCompSid);
            // 建立者
            configTemplet.setCreateUser(loginUserSid);
            // 建立時間
            configTemplet.setCreateDate(sysDate);
            // 狀態
            configTemplet.setStatus(Activation.ACTIVE);
            // 排列序號 => 初始不給, 由DB給 default (最大值, 讓他排在最後面)
            // configTemplet.setSortSeq(Integer.MAX_VALUE);

        } else {
            // 查詢
            configTemplet = this.configTempletRepository.findOne(templetVO.getSid());
            // 檢核
            if (configTemplet == null) {
                throw new Exception("編輯項目已不存在, 請重新查詢");
            }

            // 更新者
            configTemplet.setUpdateUser(loginUserSid);
            // 更新時間
            configTemplet.setUpdateDate(sysDate);
        }

        // 模版類型
        configTemplet.setTempletType(templetVO.getTempletType());
        // 名稱
        configTemplet.setTempletName(templetVO.getTempletName());
        // 設定值
        configTemplet.setConfigValue(
                this.prepareDeps(loginUserCompSid, templetVO.getConfigValue()));
        // 備註
        configTemplet.setMemo(templetVO.getMemo());

        // ====================================
        // save wc_config_templet
        // ====================================
        this.configTempletRepository.save(configTemplet);
    }

    /**
     * 在『包含以下時』將單位列表縮減成最上層
     *
     * @param configValueVo
     * @return
     */
    private ConfigValueTo prepareDeps(Integer compSid, ConfigValueTo configValueVo) {
        if (configValueVo == null || configValueVo.getDeps() == null) {
            return configValueVo;
        }

        List<Integer> deps = configValueVo.getDeps();

        // 在『包含以下時』將單位列表縮減成最上層
        if (configValueVo.isDepContainFollowing()) {
            deps = Lists.newArrayList(WkOrgUtils.collectToTopmost(deps));
        }

        // 依據組織樹排序
        deps = WkOrgUtils.sortByOrgTree(deps);

        // 回傳
        configValueVo.setDeps(deps);
        return configValueVo;
    }

    /**
     * 取得模版部門
     *
     * @param templetSid
     * @param loginCompSid
     * @return
     */
    public List<Integer> getTempletDeps(Long templetSid, Integer loginCompSid) {

        // ====================================
        // 以sid 查詢
        // ====================================
        WCConfigTemplet templet = this.configTempletRepository.findOne(templetSid);

        if (templet == null) {
            log.error("getUseDeps 找不到對應資料 templetSid : [" + templetSid + "]");
            return Lists.newArrayList();
        }

        // ====================================
        // 取得設定部門
        // ====================================
        List<Integer> depSids = this.prepareTempletDepSids(templet, loginCompSid, false);

        // 排序部門
        depSids = WkOrgUtils.sortByOrgTree(depSids);

        return depSids;
    }

    /**
     * 取得模版設定部門
     * @param templet  WCConfigTemplet
     * @param loginCompSid 登入公司 sid
     * @param isWithoutInactiveDep 是否排除停用
     * @return
     */
    public List<Integer> prepareTempletDepSids(
            WCConfigTemplet templet,
            Integer loginCompSid,
            Boolean isWithoutInactiveDep) {

        // ====================================
        // 防呆
        // ====================================
        if (templet == null
                || templet.getConfigValue() == null) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得設定部門
        // ====================================
        List<Integer> depSids = templet.getConfigValue().getDeps();
        // 部門為空時，代表全單位皆可使用
        if (WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // 部門含以下
        if (templet.getConfigValue().isDepContainFollowing()) {
            depSids = Lists.newArrayList(WkOrgUtils.prepareAllDepsByTopmost(depSids, isWithoutInactiveDep));
        }

        // 被選擇部門除外
        if (templet.getConfigValue().isDepExcept()) {
            depSids = this.workSettingOrgManager.findAllWhitOutDeps(depSids, loginCompSid, isWithoutInactiveDep);
        }
        
        return depSids;

    }

    /**
     * 取得(預設可閱項目)的模版部門 user
     *
     * @param templetSid
     * @param loginCompSid
     * @return
     */
    public List<Integer> getDefultCanReadUsers(Long templetSid, Integer loginCompSid) {

        // ====================================
        // 以sid 查詢
        // ====================================
        WCConfigTemplet templet = this.configTempletRepository.findOne(templetSid);

        if (templet == null) {
            log.error("getDefultCanReadUsers 找不到對應資料 templetSid : [" + templetSid + "]");
            return Lists.newArrayList();
        }

        // ====================================
        // 取得設定部門
        // ====================================
        List<Integer> depSids = templet.getConfigValue().getDeps();
        // 為空時代表無人預設可閱
        if (WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // 部門含以下
        if (templet.getConfigValue().isDepContainFollowing()) {
            depSids = Lists.newArrayList(WkOrgUtils.prepareAllDepsByTopmost(depSids, false));
        }

        // 被選擇部門除外
        if (templet.getConfigValue().isDepExcept()) {
            depSids = this.workSettingOrgManager.findAllWhitOutDeps(depSids, loginCompSid);
        }

        // 排序部門
        depSids = WkOrgUtils.sortByOrgTree(depSids);

        // ====================================
        // 取得設定部門以下的 user
        // ====================================
        List<Integer> allUsers = Lists.newArrayList();
        for (Integer depSid : depSids) {
            // 查部門下所有的User
            List<User> users = this.findUserByDepSid(depSid);
            if (WkStringUtils.isEmpty(users)) {
                continue;
            }

            // 以名稱排序
            users = users.stream().sorted(Comparator.comparing(User::getName))
                    .collect(Collectors.toList());

            for (User usr : users) {
                // 為啟用時加入
                if (Activation.ACTIVE.equals(usr.getStatus()) && !allUsers.contains(usr.getSid())) {
                    allUsers.add(usr.getSid());
                }
            }
        }

        return allUsers;
    }

    /**
     * 依據單位 sid , 由快取中取得該部門的 user, 包含代理主管
     *
     * @param depSid
     * @return
     */
    private List<User> findUserByDepSid(Integer depSid) {
        // 由快取中取得
        List<User> depUsers = this.wkUserCache.findUserWithManagerByOrgSids(Lists.newArrayList(depSid), Activation.ACTIVE);
        // 防呆
        if (depUsers == null) {
            return Lists.newArrayList();
        }
        return depUsers;
    }

    /**
     * 依據傳入 List 順序, 更新排序序號
     *
     * @param templetList
     */
    @Transactional
    public void updateSortSeqByListIndex(List<WCConfigTempletVo> templetList) {

        // 傳入列表為空
        if (WkStringUtils.isEmpty(templetList)) {
            return;
        }

        // 以模版類別, 紀錄 index 序號
        Map<ConfigTempletType, Integer> indexMap = Maps.newConcurrentMap();

        for (WCConfigTempletVo templetVo : templetList) {

            // 取得模版資料檔
            WCConfigTemplet wcConfigTemplet = this.configTempletRepository.findOne(
                    templetVo.getSid());
            if (wcConfigTemplet == null) {
                continue;
            }

            // 依據模版類別累加序號
            ConfigTempletType templetType = wcConfigTemplet.getTempletType();
            Integer index = indexMap.get(templetType);
            if (index == null) {
                index = 1;
                indexMap.put(templetType, index);
            }
            wcConfigTemplet.setSortSeq(index);
            indexMap.put(templetType, index + 1);

            // 更新
            this.configTempletRepository.save(wcConfigTemplet);
        }
    }

    /**
     * 準備部門設定要顯示的文字
     *
     * @param useDep             UserDep 容器 - for 自行設定資料
     * @param isContainFollowing 是否包含以下 - for 自行設定資料
     * @param templetSid         部門範本SID
     * @return
     */
    public String[] prepareDepSettingShowContent(
            UserDep useDep, boolean isContainFollowing, Long templetSid) {

        // 結果資料
        String depTag = "";
        String depTooltip = "";

        // ====================================
        // 查詢模版設定檔
        // ====================================
        WCConfigTempletVo templetVo = null;
        if (templetSid != null) {
            templetVo = this.findBySid(templetSid);
        }

        // ====================================
        // 自行設定
        // ====================================
        if (templetVo == null) {

            // 轉 sid list
            List<Integer> depSids = this.workSettingOrgManager.convertUserDepToSids(useDep);

            // 含以下單位先最取得最上層處理
            if (isContainFollowing) {
                depSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(depSids));
            }
            // 依組織樹排序
            depSids = WkOrgUtils.sortByOrgTree(depSids);

            // 產生部門列表顯示文字
            depTooltip = workSettingOrgManager.prepareDepsShowText(depSids, isContainFollowing,
                    false);

            if (WkStringUtils.notEmpty(depSids)) {
                depTag = "自行設定";
                if (isContainFollowing) {
                    depTag += "&nbsp;&nbsp;(<span class='WS1-1-3'>含以下</span>)";
                }
            } else {
                depTag = "-";
            }

            depTag = WkStringUtils.notEmpty(depSids) ? "自行設定" : "";
        }

        // ====================================
        // 依模版設定
        // ====================================
        if (templetVo != null) {
            // 產生部門列表顯示文字
            depTooltip = workSettingOrgManager.prepareDepsShowText(
                    templetVo.getConfigValue().getDeps(),
                    templetVo.getConfigValue().isDepContainFollowing(),
                    templetVo.getConfigValue().isDepExcept());

            depTag = "模版:&nbsp";
            depTag += this.getPettyTempletName(
                    templetVo.getTempletName(), templetVo.getConfigValue(), true, true);
        }

        return new String[] { depTag, depTooltip };
    }

    /**
     * @param templetType
     * @param templetSid
     * @return
     */
    public String checkInUsed(ConfigTempletType templetType, Long templetSid) {

        // ====================================
        // 依據不同的模版類別作檢核
        // ====================================
        switch (templetType) {
        case MENU_TAG_CAN_USE:
            return this.checkMenuTagInUsed(templetSid);
        case SUB_TAG_CAN_USE:
            return this.checkSubTagInUsed(templetSid, true);
        case SUB_TAG_CAN_READ:
            return this.checkSubTagInUsed(templetSid, false);
        default:
            return "未實做傳入的模版類別:" + templetType.getDescr();
        }
    }

    /**
     * 檢查類別選單項目-可使用部門
     *
     * @param templetSid
     * @return
     */
    private String checkMenuTagInUsed(Long templetSid) {

        // ====================================
        // 查詢
        // ====================================
        List<WCMenuTag> inUsedTags = this.wcMenuTagManager.findByUseDepTempletSid(templetSid);

        if (WkStringUtils.isEmpty(inUsedTags)) {
            return "";
        }

        // ====================================
        // 組訊息
        // ====================================
        String inUseMessage = "";

        for (WCMenuTag wcMenuTag : inUsedTags) {

            inUseMessage += "<br/>";

            // 查詢大類
            WCCategoryTag categoryTag = this.wcCategoryTagCache.getWCCategoryTagBySid(wcMenuTag.getWcCategoryTagSid());
            // 組訊息文字
            if (categoryTag != null) {
                inUseMessage += categoryTag.getCategoryName();
            } else {
                inUseMessage += "[找不到大類]";
            }

            inUseMessage += "-" + wcMenuTag.getMenuName();
        }

        return inUseMessage.substring(5);
    }

    /**
     * 檢查小類(執行項目)
     *
     * @param templetSid
     * @param isCanUseDep
     * @return
     */
    private String checkSubTagInUsed(Long templetSid, boolean isCanUseDep) {
        List<WCTag> inUsedTags = null;

        // ====================================
        // 查詢
        // ====================================
        if (isCanUseDep) {
            // 查詢可使用單位
            inUsedTags = this.wcTagManager.findByCanUserDepTempletSid(templetSid);
        } else {
            // 查詢預設可閱部門
            inUsedTags = this.wcTagManager.findByUseDepTempletSid(templetSid);
        }

        if (WkStringUtils.isEmpty(inUsedTags)) {
            return "";
        }

        // ====================================
        // 組訊息
        // ====================================
        String inUseMessage = "";
        for (WCTag wcTag : inUsedTags) {

            // 取得中類
            WCMenuTag menuTag = this.wcMenuTagCache.getWCMenuTagBySid(wcTag.getWcMenuTagSid());

            // 取得大類
            WCCategoryTag categoryTag = null;
            if (menuTag != null) {
                categoryTag = this.wcCategoryTagCache.getWCCategoryTagBySid(
                        menuTag.getWcCategoryTagSid());
            }

            // 組訊息文字 - 大類
            if (categoryTag != null) {
                inUseMessage += categoryTag.getCategoryName();
            } else {
                inUseMessage += "[找不到大類]";
            }

            inUseMessage += "-";

            // 組訊息文字 - 中類
            if (menuTag != null) {
                inUseMessage += menuTag.getMenuName();
            } else {
                inUseMessage += "[找不到中類]";
            }

            inUseMessage += "-" + wcTag.getTagName();
        }

        return inUseMessage.substring(5);
    }

    /**
     * 刪除模版
     *
     * @param templetSid
     */
    public void deleteTemplet(Long templetSid) {
        this.configTempletRepository.delete(templetSid);
        log.info("刪除模版:" + templetSid);
    }

    /**
     * @param templetName
     * @param configValueTo
     * @param isColorful
     * @param isAddUnderline
     * @return
     */
    public String getPettyTempletName(
            String templetName, 
            ConfigValueTo configValueTo, 
            boolean isColorful,
            boolean isAddUnderline) {

        // 組模版名稱資訊
        String showText = "";
        if (isAddUnderline) {
            showText += "<span style='text-decoration:underline;'>";
        }
        showText = templetName;
        if (isAddUnderline) {
            showText += "</span>";
        }

        boolean isDepContainFollowing = configValueTo.isDepContainFollowing();
        boolean isExecpt = configValueTo.isDepExcept();

        if (isDepContainFollowing || isExecpt) {
            showText += "【";
            if (isExecpt) {
                if (isColorful) {
                    showText += "<span class='WS1-1-2'>";
                }
                showText += "除外";
                if (isColorful) {
                    showText += "</span>";
                }
            }
            if (isDepContainFollowing) {
                if (isExecpt) {
                    showText += "-";
                }
                if (isColorful) {
                    showText += "<span class='WS1-1-3'>";
                }
                showText += "含以下";
                if (isColorful) {
                    showText += "</span>";
                }
            }
            showText += "】";
        }
        return showText;
    }

    /**
     * 取得所有部門設定模版
     *
     * @return
     */
    public List<WCConfigTemplet> findAll() {
        return this.configTempletRepository.findAll();
    }
}
