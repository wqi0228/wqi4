/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCGroupSetupPrivateRepository;
import com.cy.work.connect.vo.WCGroupSetupPrivate;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 群組
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WorkGroupSetupPrivateManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3903174521552452222L;

    @Autowired
    private WCGroupSetupPrivateRepository groupSetupPrivateRepository;

    /**
     * 取得群組 By 群組Sid
     *
     * @param sid 群組Sid
     * @return
     */
    public WCGroupSetupPrivate getWorkGroupSetupPrivateBySid(String sid) {
        try {
            return groupSetupPrivateRepository.findOne(sid);
        } catch (Exception e) {
            log.warn("getWorkGroupSetupPrivateBySid Error : " + e, e);
            return null;
        }
    }

    /**
     * 取得群組List By 登入者Sid
     *
     * @param create_usr 登入者Sid
     * @return
     */
    public List<WCGroupSetupPrivate> getWorkFunItemByCategoryModel(Integer create_usr) {
        try {
            String type = "WC";
            return groupSetupPrivateRepository.getWorkGroupSetupPrivateByCreateUsr(create_usr,
                type);
        } catch (Exception e) {
            log.warn("getWorkFunItemByCategoryModel Error : " + e, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 建立群組
     *
     * @param workGroupSetupPrivate 群組物件
     * @param create_usr            建立者Sid
     * @return
     */
    public WCGroupSetupPrivate create(WCGroupSetupPrivate workGroupSetupPrivate,
        Integer create_usr) {
        try {
            String type = "WC";
            workGroupSetupPrivate.setType(type);
            workGroupSetupPrivate.setCreate_dt(new Date());
            workGroupSetupPrivate.setCreate_usr(create_usr);
            return groupSetupPrivateRepository.save(workGroupSetupPrivate);
        } catch (Exception e) {
            log.warn("create Error : " + e, e);
            return null;
        }
    }

    /**
     * 刪除群組 By 群組Sid
     *
     * @param sid 群組Sid
     */
    public void delete(String sid) {
        groupSetupPrivateRepository.delete(sid);
    }

    /**
     * 更新群組
     *
     * @param workGroupSetupPrivate 群組物件
     * @param update_usr            更新者Sid
     * @return
     */
    public WCGroupSetupPrivate update(WCGroupSetupPrivate workGroupSetupPrivate,
        Integer update_usr) {
        try {
            workGroupSetupPrivate.setUpdate_dt(new Date());
            workGroupSetupPrivate.setUpdate_usr(update_usr);
            return groupSetupPrivateRepository.save(workGroupSetupPrivate);
        } catch (Exception e) {
            log.warn("update Error : " + e, e);
            return null;
        }
    }
}
