/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.vo.WCMemoTrace;
import com.cy.work.connect.vo.enums.WCTraceType;
import java.io.Serializable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 備忘錄追蹤邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WCMemoTraceHelper implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7588454826112524288L;

    private static WCMemoTraceHelper instance;
    @Autowired
    private WkUserCache userManager;

    public static WCMemoTraceHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMemoTraceHelper.instance = this;
    }

    /**
     * 建立修改主題追蹤物件
     *
     * @param oriContent    原本主題內容
     * @param targetContent 更新後主題內容
     * @param userSid       更新者Sid
     * @param memoSid       備忘錄Sid
     * @param memoNo        備忘錄單號
     * @return
     */
    public WCMemoTrace getWCMemoTrace_MODIFY_THEME(
        String oriContent, String targetContent, Integer userSid, String memoSid, String memoNo) {
        WCMemoTrace wcMemoTrace = new WCMemoTrace();
        wcMemoTrace.setWcMemoSid(memoSid);
        wcMemoTrace.setWcMemoNo(memoNo);
        wcMemoTrace.setWcMemoTraceType(WCTraceType.MODIFY_THEME);
        String content = "主題由[" + oriContent + "] 變更為 [" + targetContent + "]";
        wcMemoTrace.setStatus(Activation.ACTIVE);
        wcMemoTrace.setWc_trace_content(content);
        wcMemoTrace.setWc_trace_content_css(content);
        return wcMemoTrace;
    }

    /**
     * 建立修改內容追蹤物件
     *
     * @param oriContent    原本內容
     * @param targetContent 修改後內容
     * @param userSid       修改者Sid
     * @param memoSid       備忘錄Sid
     * @param memoNo        備忘錄單號
     * @return
     */
    public WCMemoTrace getWCMemoTrace_MODIFY_CONTNET(
        String oriContent, String targetContent, Integer userSid, String memoSid, String memoNo) {
        WCMemoTrace wcMemoTrace = new WCMemoTrace();
        wcMemoTrace.setWcMemoSid(memoSid);
        wcMemoTrace.setWcMemoNo(memoNo);
        wcMemoTrace.setWcMemoTraceType(WCTraceType.MODIFY_CONTNET);
        //        String content = "變更前 [" + oriContent + "] 變更後 ["
        //                + targetContent + "]";
        String content = "內容資訊異動";
        wcMemoTrace.setStatus(Activation.ACTIVE);
        wcMemoTrace.setWc_trace_content(content);
        wcMemoTrace.setWc_trace_content_css(content);
        return wcMemoTrace;
    }

    public WCMemoTrace getWCMemoTrace_TRANSTOWORKCONNECT(
        String wcSid, String wcNo, Integer userSid, String memoSid, String memoNo) {
        WCMemoTrace wcMemoTrace = new WCMemoTrace();
        wcMemoTrace.setWcMemoSid(memoSid);
        wcMemoTrace.setWcMemoNo(memoNo);
        wcMemoTrace.setWcMemoTraceType(WCTraceType.TRANSTOWORKCONNECT_LINK);
        String content = wcSid;
        wcMemoTrace.setStatus(Activation.ACTIVE);
        wcMemoTrace.setWc_trace_content(content);
        wcMemoTrace.setWc_trace_content_css(content);
        return wcMemoTrace;
    }

    /**
     * 建立修改備註追蹤物件
     *
     * @param oriContent    原本備註內容
     * @param targetContent 修改後備註內容
     * @param userSid       修改者Sid
     * @param memoSid       備忘錄Sid
     * @param memoNo        備忘錄單號
     * @return
     */
    public WCMemoTrace getWCMemoTrace_MODIFY_MEMO(
        String oriContent, String targetContent, Integer userSid, String memoSid, String memoNo) {
        WCMemoTrace wcMemoTrace = new WCMemoTrace();
        wcMemoTrace.setWcMemoSid(memoSid);
        wcMemoTrace.setWcMemoNo(memoNo);
        wcMemoTrace.setWcMemoTraceType(WCTraceType.MODIFY_MEMO);
        //        String content = "變更前 [" + oriContent + "] 變更後 ["
        //                + targetContent + "]";
        String content = "備註資訊異動";
        wcMemoTrace.setStatus(Activation.ACTIVE);
        wcMemoTrace.setWc_trace_content(content);
        wcMemoTrace.setWc_trace_content_css(content);
        return wcMemoTrace;
    }

    /**
     * 建立刪除附件追蹤物件
     *
     * @param attSid        附件Sid
     * @param fileName      附件名稱
     * @param createUserSid 建立者Sid
     * @param deleteUserSid 刪除者Sid
     * @param memoSid       備忘錄Sid
     * @param memoNo        備忘錄單號
     * @return
     */
    public WCMemoTrace getWCMemoTrace_DEL_ATTACHMENT(
        String attSid,
        String fileName,
        Integer createUserSid,
        Integer deleteUserSid,
        String memoSid,
        String memoNo) {
        WCMemoTrace wcMemoTrace = new WCMemoTrace();
        wcMemoTrace.setWcMemoSid(memoSid);
        wcMemoTrace.setWcMemoNo(memoNo);
        wcMemoTrace.setWcMemoTraceType(WCTraceType.DEL_ATTACHMENT);
        wcMemoTrace.setStatus(Activation.ACTIVE);
        User createUser = userManager.findBySid(createUserSid);
        User deleteUser = userManager.findBySid(deleteUserSid);
        String content =
            ""
                + "檔案名稱：【"
                + fileName
                + "】\n"
                + "上傳成員：【"
                + ((createUser != null) ? createUser.getName() : "")
                + "】\n"
                + "\n"
                + "刪除來源：【備忘錄 － 單號："
                + memoNo
                + "  】\n"
                + "刪除成員：【"
                + ((deleteUser != null) ? deleteUser.getName() : "")
                + "】\n"
                + "刪除註記：【"
                + attSid
                + "】";
        wcMemoTrace.setWc_trace_content(content);
        wcMemoTrace.setWc_trace_content_css(content);
        return wcMemoTrace;
    }
}
