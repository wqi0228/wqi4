/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Role;
import com.cy.work.connect.repository.SpecificRoleInquireDepRepository;
import com.cy.work.connect.vo.SpecificRoleInquireDep;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 可閱部門
 *
 * @author brain0925_liao
 */
@Component
public class SpecificRoleInquireDepManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2692757312745171145L;

    @Autowired
    private SpecificRoleInquireDepRepository specificRoleInquireDepRepository;

    /**
     * 取得可閱部門 By 角色Sid
     *
     * @param roleSID 角色Sid
     * @return
     */
    public List<SpecificRoleInquireDep> getSpecificRoleInquireDepByRoleSids(List<Long> roleSID) {
        List<Role> roles = Lists.newArrayList();
        roleSID.forEach(
            item -> {
                roles.add(new Role(item));
            });
        List<SpecificRoleInquireDep> results = specificRoleInquireDepRepository.findByRoleIn(roles);
        List<SpecificRoleInquireDep> realResults =
            results.stream()
                .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList());

        return realResults;
    }
}
