/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * 類別 - 中類 (單據名稱)
 *
 * @author brain0925_liao
 */
@EqualsAndHashCode(of = {"sid"})
public class MenuTagVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4406086501797759292L;
    /**
     * sid
     */
    @Getter
    private final String sid;
    /**
     * 單據名稱
     */
    @Getter
    private final String name;
    /**
     * 執行項目顯示標題
     */
    @Getter
    private final String itemName;
    @Getter
    private String categorySid;
    /**
     * 預設主題
     */
    @Getter
    private String title;
    /**
     * 預設內容
     */
    @Getter
    private String content;
    /**
     * 預設備註
     */
    @Getter
    private String remark;
    /**
     * 管理者備註
     */
    @Getter
    private String memo;
    /**
     * 是否為附加檔案類別
     */
    @Getter
    private boolean isAttachmentType;
    /**
     * 附加檔案下載路徑
     */
    @Getter
    private String path;

    /**
     * 建構式
     *
     * @param sid              sid
     * @param name             單據名稱
     * @param itemName         執行項目顯示標題
     * @param isAttachmentType 是否為附加檔案類別
     * @param path             附加檔案下載路徑
     */
    public MenuTagVO(
        String sid, String name, String itemName, boolean isAttachmentType, String path) {
        this.sid = sid;
        this.name = name;
        this.itemName = itemName;
        this.isAttachmentType = isAttachmentType;
        this.path = path;
    }

    /**
     * 建構式
     *
     * @param sid      sid
     * @param name     單據名稱
     * @param itemName 執行項目顯示標題
     * @param memo     管理者備註
     */
    public MenuTagVO(String sid, String name, String itemName, String memo) {
        this.sid = sid;
        this.name = name;
        this.itemName = itemName;
        this.memo = memo;
    }

    /**
     * 建構式
     *
     * @param categorySid
     * @param sid         sid
     * @param name        單據名稱
     * @param itemName    執行項目顯示標題
     * @param title       預設主題
     * @param content     預設內容
     * @param remark      預設備註
     */
    public MenuTagVO(
        String categorySid,
        String sid,
        String name,
        String itemName,
        String title,
        String content,
        String remark) {
        this.categorySid = categorySid;
        this.sid = sid;
        this.name = name;
        this.itemName = itemName;
        this.title = title;
        this.content = content;
        this.remark = remark;
    }
}
