package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCMasterCustomLogicManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCTraceCustomLogicManager;
import com.cy.work.connect.logic.manager.WCTraceManager;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 待領單據邏輯
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class WaitReceiveLogic implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8729909306771143661L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WaitReceiveLogic instance;

    public static WaitReceiveLogic getInstance() { return instance; }

    private static void setInstance(WaitReceiveLogic instance) { WaitReceiveLogic.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;
    @Autowired
    private WCTraceCustomLogicManager wcTraceCustomLogicManager;
    @Autowired
    private WCTraceManager wcTraceManager;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 核心查詢 SQL
     * 
     * @param loginUserSid
     * @return
     */
    public String prepareCoreSql(Integer loginUserSid) {
        // ====================================
        // 取得登入者可領單的單位清單
        // ====================================
        // 查詢可領單位
        Set<Integer> canReceiveDepSids = RelationDepHelper.findRelationOrgSidsForExecCanReceive(loginUserSid);
        // 組成 SQL字串
        String canReceiveDepSidSqlStr = "-1"; // 無可領單位時，使其查不到 (應該不可能, 除非資料錯誤)
        if (WkStringUtils.notEmpty(canReceiveDepSids)) {
            // 轉為 111,222,333
            canReceiveDepSidSqlStr = String.join(
                    ",",
                    canReceiveDepSids.stream().map(String::valueOf).collect(Collectors.toList()));
        }

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();

        sql.append("FROM   wc_master wc ");
        sql.append("       INNER JOIN wc_exec_dep execDep ");
        sql.append("              ON execDep.status = 0 ");
        sql.append("             AND wc.wc_sid = execDep.wc_sid ");
        sql.append("       LEFT JOIN wc_exec_dep_setting depSetting ");
        sql.append("              ON depSetting.wc_exec_dep_setting_sid = execDep.wc_exec_dep_setting_sid ");

        sql.append("WHERE  1=1 ");

        // ----------------------------------
        // 基本條件
        // ----------------------------------
        // 排除錯誤資料
        sql.append("       AND wc.wc_status != 'INVALID' ");
        // 執行單位狀態，限制為『待領單』
        sql.append("       AND execDep.status = 0 ");
        sql.append("       AND execDep.exec_dep_status = 'WAITRECEIVCE' ");

        // ----------------------------------
        // 條件 1 (執行單位領單)
        // ----------------------------------
        sql.append("   AND ( ");
        sql.append("           ( ");
        // 限制執行單位需為『登入者可領單位』
        sql.append("              execDep.dep_sid IN ( " + canReceiveDepSidSqlStr + " ) ");
        sql.append("              AND ( ");
        // 無需讀取可領單人員,
        sql.append("                    depSetting.readReceviceSetting = 0    ");
        // 沒對應到設定檔 (加派單位)
        sql.append("                    OR depSetting.wc_exec_dep_setting_sid IS NULL ");
        sql.append("                  ) ");
        sql.append("            ) ");

        // ----------------------------------
        // 條件 2 (限定可領單人員)
        // ----------------------------------
        // 若執行單位有限制『可領單人員』，在此判斷
        sql.append("       OR ( depSetting.wc_exec_dep_setting_sid IS NOT NULL "); // 執行單位有對應到設定檔
        sql.append("             AND depSetting.isNeedAssigned = 0 "); // 為領單模式
        sql.append("             AND depSetting.readReceviceSetting = 1 "); // 需讀取可領單人員
        sql.append("             AND depSetting.receviceUser LIKE '%{\"sid\":" + loginUserSid + "}%' ) ");
        sql.append("    ) ");

        return sql.toString();

    }

    /**
     * @param loginUserSid
     * @return
     */
    public Integer queryWaitReceiveCount(Integer loginUserSid) {
        // ====================================
        // 兜組 SQL
        // ====================================
        String sql = this.prepareCoreSql(loginUserSid);
        sql = "SELECT count(*) FROM (SELECT wc.wc_sid  " + sql + " GROUP BY wc_sid) aa;";// 去除重複
        return this.jdbc.queryForObject(sql, Integer.class);
    }

    /**
     * 執行領單
     * 
     * @param wcSid       主單 sid
     * @param ececUserSid 執行者 sid
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doReceive(String wcSid, Integer ececUserSid) throws UserMessageException {

        // ====================================
        // 檢查主單狀態
        // ====================================
        WCMaster wcMaster = this.wcMasterManager.findBySid(wcSid);
        if (wcMaster == null) {
            throw new UserMessageException(WkMessage.NEED_RELOAD, InfomationLevel.WARN);
        }

        List<WCStatus> failStatus = Lists.newArrayList(
                WCStatus.INVALID,
                WCStatus.NEW_INSTANCE,
                WCStatus.RECONSIDERATION,
                WCStatus.CLOSE,
                WCStatus.EXEC_FINISH);

        if (failStatus.contains(wcMaster.getWc_status())) {
            String message = WkMessage.NEED_RELOAD + "(主單狀態為:[{" + wcMaster.getWc_status().getVal() + "}] 故無法領單)";
            log.info(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 取得『本張聯絡單』可以『執行領單』的『執行單位檔』
        // ====================================
        // 兜組 查詢 SQL
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT execDep.wc_exec_dep_sid ");
        // 取得核心 SQL
        sql.append(this.prepareCoreSql(ececUserSid));
        // 有指定 wcSid
        sql.append("       AND wc.wc_sid = '" + wcSid + "' ");

        // 查詢
        List<String> canReceiveWcExecDepSids = this.jdbc.queryForList(sql.toString(), null, String.class);

        if (WkStringUtils.isEmpty(canReceiveWcExecDepSids)) {
            String message = WkMessage.NEED_RELOAD + "(該單據已無可領項目)";
            log.info(message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 取得主檔
        // ====================================
        List<WCExceDep> wcExceDeps = this.wcExecDepManager.findBySids(Sets.newHashSet(canReceiveWcExecDepSids));
        if (WkStringUtils.isEmpty(canReceiveWcExecDepSids)) {
            String message = WkMessage.NEED_RELOAD + "(執行單位檔已異動)";
            log.info(message + "(找不到 wc_exec_dep :" + String.join(",", canReceiveWcExecDepSids) + ")");
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 逐筆異動狀態
        // ====================================
        // 收集領單項目
        Set<String> tagSids = Sets.newHashSet();
        // 收集領單-加派單位
        Set<Integer> depSids = Sets.newHashSet();
        for (WCExceDep waitReceiveExceDep : wcExceDeps) {

            // 收集追蹤用的資訊
            if (WkStringUtils.notEmpty(waitReceiveExceDep.getExecDepSettingSid())) {
                // 非加派
                // 取得設定檔
                WCExecDepSetting execDepSetting = wcExecDepSettingManager.findBySidFromCache(waitReceiveExceDep.getExecDepSettingSid());
                // 收集項目Sid
                if (execDepSetting != null) { // null防呆
                    tagSids.add(execDepSetting.getWc_tag_sid());
                }
            } else {
                depSids.add(waitReceiveExceDep.getDep_sid());
            }

            // 更新執行單位狀態
            this.wcExecDepManager.updateWCExceDepStatus(
                    wcSid,
                    waitReceiveExceDep,
                    WCExceDepStatus.PROCEDD,
                    ececUserSid,
                    ececUserSid);
        }

        // ====================================
        // 寫追蹤
        // ====================================
        WCTrace wcTrace = wcTraceCustomLogicManager.getTraceExecRecevice(
                wcSid,
                wcMaster.getWc_no(),
                tagSids,
                depSids,
                ececUserSid);
        this.wcTraceManager.create(wcTrace, ececUserSid);

        // ====================================
        // 若主檔狀態為核准,領單後,必須變為執行中
        // ====================================
        if (WCStatus.APPROVED.equals(wcMaster.getWc_status())) {
            this.wcMasterCustomLogicManager.updateToExec(wcMaster.getSid(), ececUserSid);
        }
    }
}
