/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCHomepageFavoriteRepository;
import com.cy.work.connect.vo.WCHomepageFavorite;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 報表快選區Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCHomepageFavoriteCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4190198250005291103L;
    /**
     * WCHomepageFavoriteRepository
     */
    @Autowired
    private WCHomepageFavoriteRepository wcHomepageFavoriteRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<Integer, WCHomepageFavorite> wcHomepageFavoriteMap;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    public WCHomepageFavoriteCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcHomepageFavoriteMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay =  ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立首頁收藏資訊Cache");
        try {
            Map<Integer, WCHomepageFavorite> tempWCHomepageFavoriteMap = Maps.newConcurrentMap();
            wcHomepageFavoriteRepository
                .findAll()
                .forEach(
                    item -> {
                        tempWCHomepageFavoriteMap.put(item.getUsersid(), item);
                    });
            wcHomepageFavoriteMap = tempWCHomepageFavoriteMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立首頁收藏資訊Cache結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新Cache資料
     */
    public synchronized void updateCacheByUserSid(WCHomepageFavorite wcHomepageFavorite) {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立首頁收藏資訊Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立首頁收藏資訊Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
        wcHomepageFavoriteMap.put(wcHomepageFavorite.getUsersid(), wcHomepageFavorite);
    }

    /**
     * 取得報表快選區資料 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public WCHomepageFavorite findByUserSid(Integer userSid) {
        return wcHomepageFavoriteMap.get(userSid);
    }
}
