/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;

/**
 * 收藏報表轉出物件
 *
 * @author brain0925_liao
 */
public class WCFavoriteWorkReportVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4332687519966656433L;
    /**
     * 收藏Sid
     */
    @Getter
    private final String favorite_sid;
    /**
     * 工作聯絡單Sid
     */
    @Getter
    private final String wc_sid;
    /**
     * 主題
     */
    @Getter
    private final String theme;
    /**
     * 收藏日期
     */
    @Getter
    private final Date favoriteDate;
    /**
     * 工作聯絡單單號
     */
    @Getter
    private final String wc_no;

    public WCFavoriteWorkReportVO(
        String favorite_sid, String wc_sid, String theme, Date favoriteDate, String wc_no) {
        this.favorite_sid = favorite_sid;
        this.wc_sid = wc_sid;
        this.theme = theme;
        this.favoriteDate = favoriteDate;
        this.wc_no = wc_no;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.favorite_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WCFavoriteWorkReportVO other = (WCFavoriteWorkReportVO) obj;
        return Objects.equals(this.favorite_sid,
            other.favorite_sid);
    }
}
