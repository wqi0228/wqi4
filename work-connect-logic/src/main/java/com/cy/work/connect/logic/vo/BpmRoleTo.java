package com.cy.work.connect.logic.vo;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cy.bpm.rest.to.RoleTo;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

public class BpmRoleTo extends RoleTo {

    /**
     * 
     */
    private static final long serialVersionUID = -953923922794147665L;

    public BpmRoleTo(RoleTo roleTo) {
        BeanUtils.copyProperties(roleTo, this);
    }
    
    /**
     * 角色下所有的 user ID
     */
    @Getter
    @Setter
    private List<String> roleUserIds = Lists.newArrayList();

}
