/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.converter.to.ExecDepSidTo;
import com.cy.work.connect.vo.converter.to.ExecDepSids;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.ExecDepCreateType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 追蹤邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WCTraceCustomLogicManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6733909376396896060L;

    private static WCTraceCustomLogicManager instance;
    @Autowired
    private WCTraceManager wcTraceManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCTagCache wCTagCache;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;

    public static WCTraceCustomLogicManager getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCTraceCustomLogicManager.instance = this;
    }

    /**
     * 取得追蹤By 追蹤Sid
     *
     * @param sid 追蹤Sid
     * @return
     */
    public WCTrace getWCTraceBySid(String sid) {
        return wcTraceManager.getWCTraceBySid(sid);
    }

    public WCTrace getWCTrace_TRANS_FROM_MEMO(
            String memoNo, String memoTitle, String wcSid, String wcNo, Integer userSid) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        String content = "["
                + WkUserUtils.findNameBySid(userSid)
                + "]於["
                + ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())
                + "]將["
                + memoNo
                + "]["
                + memoTitle
                + "]轉入工作聯絡單中";
        wrt.setWc_trace_type(WCTraceType.TRANS_FROM_MEMO);
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getTraceTransWork(String wcSid, String wcNo, String contentInfo) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        wrt.setWc_trace_type(WCTraceType.TRANS_WORK);
        String content = "執行人員由 \n" + contentInfo;
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getTraceExecRecevice(
            String wcSid,
            String wcNo,
            Set<String> tagSids,
            Set<Integer> execDepSids,
            Integer receviceUserSid) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        wrt.setWc_trace_type(WCTraceType.EXEC_RECEVICE);
        StringBuilder sb = new StringBuilder();

        if (WkStringUtils.notEmpty(tagSids)) {
            String tagContent = tagSids.stream()
                    .map(tagSid -> wCTagCache.findBySid(tagSid).getTagName())
                    .collect(Collectors.joining("、", "[", "]"));
            sb.append("類別：" + tagContent);
        }
        if (WkStringUtils.notEmpty(execDepSids)) {
            String execDepContent = execDepSids.stream()
                    .map(depSid -> WkOrgUtils.findNameBySid(depSid))
                    .collect(Collectors.joining("、", "[", "]"));

            sb.append("加派單位：" + execDepContent);
        }

        sb.append(" - 領單完畢");
        sb.append(" \n");
        sb.append("領單人員:");
        sb.append(WkUserUtils.findNameBySid(receviceUserSid));
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(sb.toString());
        wrt.setWc_trace_content_css(sb.toString());
        return wrt;
    }

    /**
     * @param wcSid                 主單 sid
     * @param wcNo                  主單單號
     * @param reason                退回原因
     * @param rollBackTargetRole    退回目標人員角色
     * @param rollBackTargetUserSid 退回目標人員SID
     * @param actionType            EXEC_FLOW_SIGNER_ROLLBACK/EXEC_FLOW_MEMBER_ROLLBACK
     * @return WCTrace
     */
    public WCTrace prepareForRollBackToReqFlow(
            String wcSid,
            String wcNo,
            String reason,
            String rollBackTargetRole,
            Integer rollBackTargetUserSid,
            ActionType actionType) {

        // ====================================
        // 組追蹤內容
        // ====================================
        String content = "";
        if (ActionType.REQ_FLOW_ROLLBACK.equals(actionType)) {
            content += "<span class='WS1-1-2'>【需求方簽核者退回】</span>";
        }
        if (ActionType.EXEC_FLOW_SIGNER_ROLLBACK.equals(actionType)) {
            content += "<span class='WS1-1-2'>【執行方簽核者退回】</span>";
        }
        if (ActionType.EXEC_FLOW_MEMBER_ROLLBACK.equals(actionType)) {
            content += "<span class='WS1-1-2'>【執行單位退回】</span>";
        }
        content += "<br/>";
        content += "退回至 [" + rollBackTargetRole + "-" + WkUserUtils.findNameBySid(rollBackTargetUserSid) + "]";
        content += "<br/>";
        content += "<br/>";
        content += "退回原因：";
        content += "<br/>";
        content += reason;

        // ====================================
        // 組 WCTrace
        // ====================================
        WCTraceType traceType = WCTraceType.EXECROLLBACK;
        if (ActionType.REQ_FLOW_ROLLBACK.equals(actionType)) {
            traceType = WCTraceType.REQROLLBACK;
        }

        return this.prepareWCTrace(wcSid, wcNo, traceType, content);
    }

    /**
     * 準備追蹤資料 for 執行單位簽核者-不執行
     * 
     * @param wcSid       主單 sid
     * @param wcNo        主單單號
     * @param execDepSids 執行單位 SID
     * @param reason      原因
     * @return WCTrace
     */
    public WCTrace prepareForExecFlowSignerStop(
            String wcSid, String wcNo, List<Integer> execDepSids, String reason) {
        // ====================================
        // 組追蹤內容
        // ====================================
        String content = "";
        content += "<span class='WS1-1-2'>【簽核者取消執行】</span>";
        content += "<br/>";
        content += "以下單位 : [" + WkOrgUtils.findNameBySid(execDepSids, "、", false) + "]";
        content += "<br/>";
        content += "<span class='WS1-1-2'>取消執行</span>";
        content += "<br/>";
        content += "<br/>";
        content += "原因：";
        content += "<br/>";
        content += reason;

        // ====================================
        // 組 WCTrace
        // ====================================
        return this.prepareWCTrace(wcSid, wcNo, WCTraceType.EXEC_SIGNER_STOP, content);
    }

    /**
     * 準備追蹤資料 for 執行單位成員-不執行
     * 
     * @param wcSid       主單 sid
     * @param wcNo        主單單號
     * @param execDepSids 執行單位 SID
     * @param reason      原因
     * @return WCTrace
     */
    public WCTrace prepareForExecFlowMemberStop(
            String wcSid, String wcNo, List<Integer> execDepSids, String reason) {

        // ====================================
        // 組追蹤內容
        // ====================================
        String content = "";
        content += "執行單位 : [" + WkOrgUtils.findNameBySid(execDepSids, "、", false) + "]";
        content += "<br/>";
        content += "<span class='WS1-1-2'>取消執行 (不執行)</span>";
        content += "<br/>";
        content += "<br/>";
        content += "原因：";
        content += "<br/>";
        content += reason;

        // ====================================
        // 組 WCTrace
        // ====================================
        return this.prepareWCTrace(wcSid, wcNo, WCTraceType.EXEC_MEMBER_STOP, content);
    }

    private WCTrace prepareWCTrace(
            String wcSid,
            String wcNo,
            WCTraceType wcTraceType,
            String traceCssContent) {
        WCTrace wcTrace = new WCTrace();
        wcTrace.setStatus(Activation.ACTIVE);

        wcTrace.setWc_sid(wcSid);
        wcTrace.setWc_no(wcNo);
        wcTrace.setWc_trace_type(wcTraceType);

        wcTrace.setWc_trace_content_css(traceCssContent);
        // 轉為純文字
        wcTrace.setWc_trace_content(WkJsoupUtils.getInstance().htmlToText(traceCssContent));

        return wcTrace;
    }

    public WCTrace getTraceExecFinish(
            String wcSid,
            String wcNo,
            String content,
            List<WCExceDep> wcExecDeps) {

        // ====================================
        // 組追蹤訊息
        // ====================================

        List<String> traceMessages = Lists.newArrayList();

        // ------------------
        // 取得執行完成類別
        // ------------------
        String execFinishTagDescr = WkCommonUtils.safeStream(wcExecDeps)
                .filter(wcExecDep -> ExecDepCreateType.SYSTEM.equals(wcExecDep.getExecDepCreateType()))
                // 取得執行單位對應的『tag (小類項目)』
                .map(wcExecDep -> wcExecDepSettingManager.findWCTagBySettingSid(wcExecDep.getExecDepSettingSid()))
                // 排除未對應到執行項目者 (防呆)
                .filter(wcTag -> wcTag != null)
                // 去除重複
                .distinct()
                // 項目排序
                .sorted(Comparator.comparing(wcTag -> wcTag.getSeq()))
                // 取得項目名稱 (項目分類+名稱)
                .map(wcTag -> (WkStringUtils.notEmpty(wcTag.getTagType()) ? wcTag.getTagType() + "&nbsp;" : "") + wcTag.getTagName())
                .collect(Collectors.joining("、"));

        if (WkStringUtils.notEmpty(execFinishTagDescr)) {
            traceMessages.add("類別：[&nbsp;" + execFinishTagDescr + "&nbsp;]");
        }

        // ------------------
        // 加派單位
        // ------------------
        Set<Integer> customExecDepSids = WkCommonUtils.safeStream(wcExecDeps)
                .filter(wcExecDep -> ExecDepCreateType.CUSTOMER.equals(wcExecDep.getExecDepCreateType()))
                .map(wcExecDep -> wcExecDep.getDep_sid())
                .collect(Collectors.toSet());

        if (WkStringUtils.notEmpty(customExecDepSids)) {
            traceMessages.add("加派執行單位：[" + WkOrgUtils.findNameBySid(customExecDepSids, "、") + "]");
        }

        // ------------------
        // 加派單位
        // ------------------
        traceMessages.add("已完成");
        traceMessages.add("");
        traceMessages.add(content);

        // 組所有追蹤訊息
        String traceMessage = String.join("<br/>", traceMessages);

        // ====================================
        // 執行單位
        // ====================================
        ExecDepSids execDepSidsMaster = new ExecDepSids();

        Set<Integer> execDepSids = WkCommonUtils.safeStream(wcExecDeps)
                .map(wcExecDep -> wcExecDep.getDep_sid())
                .collect(Collectors.toSet());

        for (Integer execDepSid : execDepSids) {
            ExecDepSidTo execDepSidTo = new ExecDepSidTo();
            execDepSidTo.setExecDepSid(execDepSid + "");
            execDepSidsMaster.getExecDepSidTos().add(execDepSidTo);
        }

        // ====================================
        // 組追蹤訊息
        // ====================================

        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        wrt.setWc_trace_type(WCTraceType.EXEC_FINISH);
        wrt.setExecDepSids(execDepSidsMaster);
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(WkJsoupUtils.getInstance().htmlToText(traceMessage));
        wrt.setWc_trace_content_css(traceMessage);
        return wrt;
    }

    public WCTrace getTraceSendWork(
            String wcSid,
            String wcNo,
            Integer lgoinUserSid,
            Integer sendPerson,
            List<String> tagSids,
            List<Integer> depSids) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        User user = userManager.findBySid(sendPerson);
        wrt.setWc_trace_type(WCTraceType.SEND_WORK);
        StringBuilder sb = new StringBuilder();
        if (tagSids != null && !tagSids.isEmpty()) {
            sb.append("類別");
            tagSids.forEach(
                    item -> {
                        sb.append("[" + wCTagCache.findBySid(item).getTagName() + "]");
                    });
        }
        if (depSids != null && !depSids.isEmpty()) {
            sb.append("執行單位");
            depSids.forEach(
                    item -> {
                        sb.append("[" + WkOrgUtils.findNameBySid(item) + "]");
                    });
        }
        String content = "將單據" + sb + "分派給" + user.getName();
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_ADD_SIGN_CHANGE(
            String wc_ID,
            String wc_no,
            Integer lgoinUserSid,
            List<SingleManagerSignInfo> removeSingleManagerSignInfos,
            List<Integer> addUserFlowSid) {
        User user = userManager.findBySid(lgoinUserSid);
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_ID);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.ADD_SIGN_CHANGE);
        StringBuilder message = new StringBuilder();
        message.append(
                "["
                        + user.getName()
                        + "]"
                        + "於["
                        + ToolsDate.transDateToString(
                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())
                        + "] 調整加簽人員 \n");
        if (addUserFlowSid != null && !addUserFlowSid.isEmpty()) {
            message.append("此次增加的加簽人員 : \n");
            StringBuilder addSb = new StringBuilder();
            addUserFlowSid.forEach(
                    item -> {
                        if (!Strings.isNullOrEmpty(addSb.toString())) {
                            addSb.append("，");
                        }
                        User assignUser = userManager.findBySid(item);
                        addSb.append(assignUser.getName());
                    });
            message.append(addSb);
            message.append("\n");
        }

        if (removeSingleManagerSignInfos != null && !removeSingleManagerSignInfos.isEmpty()) {
            message.append("此次減少的加簽人員 : \n");
            StringBuilder removeSb = new StringBuilder();
            removeSingleManagerSignInfos.forEach(
                    item -> {
                        if (!Strings.isNullOrEmpty(removeSb.toString())) {
                            removeSb.append("，");
                        }
                        User assignUser = userManager.findBySid(item.getUserSid());
                        removeSb.append(assignUser.getName());
                    });
            message.append(removeSb);
            message.append("\n");
        }

        if ((removeSingleManagerSignInfos != null && removeSingleManagerSignInfos.isEmpty())
                && (addUserFlowSid != null && addUserFlowSid.isEmpty())) {
            message.append("此次僅為確認，無異動人員。");
        }

        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(message.toString());
        wrt.setWc_trace_content_css(message.toString());
        return wrt;
    }

    public WCTrace getWCTrace_MODIFY_CUSTOMER_EXCEDEP(
            String wc_ID,
            String wc_no,
            Integer lgoinUserSid,
            Collection<Integer> addDepSids,
            Collection<Integer> removeDepSids) {

        // ====================================
        // 組追蹤內容
        // ====================================
        Date sysDate = new Date();
        String userName = WkUserUtils.findNameBySid(lgoinUserSid);
        String modifyDateTimeInfo = WkDateUtils.formatDate(sysDate, WkDateUtils.YYYY_MM_DD_HH24_mm_ss2);

        List<String> messages = Lists.newArrayList();
        messages.add("[" + userName + "] 於 [" + modifyDateTimeInfo + "] 調整加派的執行單位");
        messages.add("");

        if (WkStringUtils.notEmpty(addDepSids)) {
            messages.add("此次增加的執行部門：");
            messages.add(WkOrgUtils.findNameBySid(WkOrgUtils.sortByOrgTree(addDepSids), "、"));
            messages.add("");
        }

        if (WkStringUtils.notEmpty(removeDepSids)) {
            messages.add("此次移除的執行部門：");
            messages.add(WkOrgUtils.findNameBySid(WkOrgUtils.sortByOrgTree(removeDepSids), "、"));
            messages.add("");
        }

        // ====================================
        // entity
        // ====================================
        WCTrace wrt = new WCTrace();
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_sid(wc_ID);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.MODIFY_CUSTOMER_EXCEDEP);

        wrt.setWc_trace_content(messages.stream().collect(Collectors.joining("\r\n")));
        wrt.setWc_trace_content_css(messages.stream().collect(Collectors.joining("<br/>")));

        return wrt;
    }

    /**
     * 取得提交追蹤物件
     *
     * @param userSid 提交者Sid
     * @param wc_sid  工作聯絡單Sid
     * @param wc_no   工作聯絡單單號
     * @return
     */
    public WCTrace getWCTrace_MODIFY_APPROVED(
            Integer userSid, String wc_sid, String wc_no, boolean isFromReqSign) {

        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        String content = "";
        if (isFromReqSign) {
            wrt.setWc_trace_type(WCTraceType.MODIFY_APPRVOED);
            content = "需求方已核准";
        }
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 取得提交追蹤物件
     *
     * @param userSid 提交者Sid
     * @param wc_sid  工作聯絡單Sid
     * @param wc_no   工作聯絡單單號
     * @return
     */
    public WCTrace getWCTrace_MODIFY_EXEC(Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.MODIFY_EXEC);
        String content = "["
                + ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())
                + "] 變為執行中";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_CLOSE(Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.CLOSE);
        String content = "工作聯絡單已結案";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_EXEC_FINISH_STATUS(Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.EXEC_FINISH_STATUS);
        String content = "工作聯絡單已完成";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_CLOSE_STOP(Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.CLOSE_STOP);
        String content = "工作聯絡單已結案(終止)";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getEXECDEP_STATUS_CHANGE(
            Integer userSid, String wc_sid, String wc_no, Integer depSid) {
        User user = userManager.findBySid(userSid);
        Org dep = orgManager.findBySid(depSid);
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.EXECDEP_STATUS_CHANGE);
        String content = "["
                + user.getName()
                + "] 於 ["
                + ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())
                + "]"
                + "進行加簽，故目前["
                + dep.getName()
                + "]的狀態變更為 : 待執行"
                + "";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 取得提交追蹤物件
     *
     * @param userSid 提交者Sid
     * @param wc_sid  工作聯絡單Sid
     * @param wc_no   工作聯絡單單號
     * @return
     */
    public WCTrace getWCTrace_COMMIT(Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.COMMIT);
        String content = WkUserUtils.findNameBySid(userSid)
                + "於["
                + ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())
                + "] 發佈提交";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改主題追蹤物件
     *
     * @param oriContent    原本主題內容
     * @param targetContent 更新後主題內容
     * @param userSid       更新者Sid
     * @param wc_sid        工作聯絡單Sid
     * @param wc_no         工作聯絡單單號
     * @return
     */
    public WCTrace getWCTrace_MODIFY_THEME(
            String oriContent, String targetContent, Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.MODIFY_THEME);
        String content = "主題由[" + oriContent + "] 變更為 [" + targetContent + "]";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改內容追蹤物件
     *
     * @param oriContent    原本內容
     * @param targetContent 修改後內容
     * @param userSid       修改者Sid
     * @param wc_sid        工作聯絡單Sid
     * @param wc_no         工作聯絡單單號
     * @return
     */
    public WCTrace getWCTrace_MODIFY_CONTNET(
            String oriContent, String targetContent, Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.MODIFY_CONTNET);
        // String content = "變更前 [" + oriContent + "] 變更後 ["
        // + targetContent + "]";
        String content = "內容資訊異動";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改備註追蹤物件
     *
     * @param oriContent    原本備註內容
     * @param targetContent 修改後備註內容
     * @param userSid       修改者Sid
     * @param wc_sid        工作聯絡單Sid
     * @param wc_no         工作聯絡單號
     * @return
     */
    public WCTrace getWCTrace_MODIFY_MEMO(
            String oriContent, String targetContent, Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.MODIFY_MEMO);
        // String content = "變更前 [" + oriContent + "] 變更後 ["
        // + targetContent + "]";
        String content = "備註資訊異動";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立修改簽核層級追蹤物件
     *
     * @param userSid 修改者Sid
     * @param wc_sid  工作聯絡單Sid
     * @param wc_no   工作聯絡單號
     * @return
     */
    public WCTrace getWCTrace_MODIFY_FLOWTYPE(Integer userSid, String wc_sid, String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.MODIFY_FLOWTYPE);
        String content = "簽核層級資訊異動";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立新增回覆追蹤物件
     *
     * @param content   回覆內容
     * @param userSid   回覆者Sid
     * @param wc_sid    工作聯絡單Sid
     * @param wc_no     工作聯絡單單號
     * @param traceType 型態
     * @return
     */
    public WCTrace getWCTrace(
            String content, Integer userSid, String wc_sid, String wc_no, WCTraceType traceType) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(traceType);
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(jsoupUtils.clearCssTag(content));
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立刪除附件追蹤物件
     *
     * @param attSid        附件Sid
     * @param fileName      附件名稱
     * @param createUserSid 建立者Sid
     * @param deleteUserSid 刪除者Sid
     * @param wc_sid        工作聯絡單Sid
     * @param wc_no         工作聯絡單單號
     * @return
     */
    public WCTrace getWCTrace_DEL_ATTACHMENT(
            String attSid,
            String fileName,
            Integer createUserSid,
            Integer deleteUserSid,
            String wc_sid,
            String wc_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.DEL_ATTACHMENT);
        wrt.setStatus(Activation.ACTIVE);
        User createUser = userManager.findBySid(createUserSid);
        User deleteUser = userManager.findBySid(deleteUserSid);
        String content = ""
                + "檔案名稱：【"
                + fileName
                + "】\n"
                + "上傳成員：【"
                + ((createUser != null) ? createUser.getName() : "")
                + "】\n"
                + "\n"
                + "刪除來源：【工作聯絡單 － 單號："
                + wc_no
                + "  】\n"
                + "刪除成員：【"
                + ((deleteUser != null) ? deleteUser.getName() : "")
                + "】\n"
                + "刪除註記：【"
                + attSid
                + "】";
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_ADD_VIEW_PERSON(
            String wc_sid,
            String wc_no,
            List<User> addUser,
            List<User> removeUser,
            Integer loginUserSid) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.ADD_VIEW_PERSON);
        wrt.setStatus(Activation.ACTIVE);
        User user = userManager.findBySid(loginUserSid);
        StringBuilder message = new StringBuilder();
        message.append(
                "["
                        + user.getName()
                        + "]"
                        + "於["
                        + ToolsDate.transDateToString(
                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())
                        + "] 調整可閱單位 \n");
        if (addUser != null && !addUser.isEmpty()) {
            message.append("此次增加的可閱名單 : \n");
            StringBuilder addSb = new StringBuilder();
            addUser.forEach(
                    item -> {
                        if (!Strings.isNullOrEmpty(addSb.toString())) {
                            addSb.append("，");
                        }
                        addSb.append(item.getName());
                    });
            message.append(addSb);
            message.append("\n");
        }

        if (removeUser != null && !removeUser.isEmpty()) {
            message.append("此次減少的可閱名單 : \n");
            StringBuilder removeSb = new StringBuilder();
            removeUser.forEach(
                    item -> {
                        if (!Strings.isNullOrEmpty(removeSb.toString())) {
                            removeSb.append("，");
                        }
                        removeSb.append(item.getName());
                    });
            message.append(removeSb);
            message.append("\n");
        }
        wrt.setWc_trace_content(message.toString());
        wrt.setWc_trace_content_css(message.toString());
        return wrt;
    }

    /**
     * 建立刪除工作聯絡單追蹤物件
     *
     * @param wc_sid        工作聯絡單Sid
     * @param wc_no         工作聯絡單單號
     * @param deleteUserSid 刪除者Sid
     * @return
     */
    public WCTrace getWCTrace_DEL_WC(String wc_sid, String wc_no, Integer deleteUserSid) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.DEL_WR);
        wrt.setStatus(Activation.ACTIVE);
        String content = "工作聯絡單已被【"
                + ((userManager.findBySid(deleteUserSid) != null)
                        ? WkUserUtils.findNameBySid(deleteUserSid)
                        : "")
                + "】執行刪除";
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立作廢工作聯絡單追蹤物件
     *
     * @param wc_sid 工作聯絡單Sid
     * @param wc_no  工作聯絡單單號
     * @param reason 作廢理由
     * @return
     */
    public WCTrace getWCTrace_INVAILD(String wc_sid, String wc_no, String reason) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wc_sid);
        wrt.setWc_no(wc_no);
        wrt.setWc_trace_type(WCTraceType.INVAILD);
        wrt.setStatus(Activation.ACTIVE);
        String content = "作廢原因 :" + reason;
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }
}
