/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.repository.WCMemoAttachmentRepository;
import com.cy.work.connect.repository.WCMemoMasterRepository;
import com.cy.work.connect.vo.WCMemoMaster;
import com.cy.work.connect.vo.converter.to.WCSidTo;
import com.cy.work.connect.vo.converter.to.WCSids;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 備忘錄Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMemoMasterManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4209790160995600041L;

    private static WCMemoMasterManager instance;
    @Autowired
    private WCMemoMasterRepository wcMemoMasterRepository;
    @Autowired
    private WCMemoAttachmentRepository memoAttachmentRepository;

    public static WCMemoMasterManager getInstance() { return instance; }

    /**
     * 新增
     *
     * @param compId
     * @param wcMemoMaster
     * @param atts
     * @return
     * @throws InterruptedException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public static synchronized WCMemoMaster create(
            String compId,
            WCMemoMaster wcMemoMaster,
            List<AttachmentVO> atts) {

        WCMemoMaster result = WCMemoMasterManager.getInstance().save(wcMemoMaster);

        if (WkStringUtils.notEmpty(atts)) {
            for (AttachmentVO attachmentVO : atts) {
                WCMemoMasterManager.getInstance()
                        .updateWCMemoMappingInfo(
                                result.getSid(),
                                result.getWcMemoNo(),
                                attachmentVO.getAttDesc(),
                                result.getCreate_usr(),
                                new Date(),
                                attachmentVO.getAttSid());
            }
        }
        return result;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMemoMasterManager.instance = this;
    }

    /**
     * 取得備忘錄 By Sid
     *
     * @param sid 備忘錄Sid
     * @return
     */
    public WCMemoMaster findBySid(String sid) {
        return wcMemoMasterRepository.findOne(sid);
    }

    public List<WCMemoMaster> findByNo(String no) {
        return wcMemoMasterRepository.findByNo(no);
    }

    public List<WCMemoMaster> findByCreateUserSid(Integer createUserSid) {
        return wcMemoMasterRepository.findByCreateUserSid(createUserSid);
    }

    /**
     * 更新備忘錄 (不更新修改時間及修改者)
     *
     * @param wcMemoMaster 備忘錄物件
     * @return
     */
    public WCMemoMaster updateOnlyWCMemoMaster(WCMemoMaster wcMemoMaster) {
        try {
            WCMemoMaster wc = wcMemoMasterRepository.save(wcMemoMaster);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 取得最後一筆備忘錄單號 By 備忘錄單據狀態及 建立時間區間
     *
     * @param start 建立起始時間
     * @param end   建立結束時間
     * @return
     */
    public String findTodayLastWCMemoMasterNo(Date start, Date end) {
        List<String> nos = null;
        nos = wcMemoMasterRepository.findTodayLastWCMemoMasterNo(start, end);
        if (nos.size() > 0) {
            return nos.get(0);
        }
        return "";
    }

    /**
     * 取得建立筆數 By 建立時間區間
     *
     * @param start 建立起始時間
     * @param end   建立結束時間
     * @return
     */
    public Integer findTodayWCMemoMasterTotalSize(Date start, Date end) {
        try {
            return wcMemoMasterRepository.findTodayWCMemoMasterTotalSize(start, end);
        } catch (Exception e) {
            log.warn("findTodayWCMemoMasterTotalSize Error : " + e, e);
        }
        return 0;
    }

    /**
     * 備忘錄,更新附件資訊 By 附件Sid
     *
     * @param wcMemoSid
     * @param wcMemoNo
     * @param description
     * @param update_usr_sid
     * @param update_dt
     * @param sid
     * @return
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public Integer updateWCMemoMappingInfo(
            String wcMemoSid,
            String wcMemoNo,
            String description,
            Integer update_usr_sid,
            Date update_dt,
            String sid) {
        return memoAttachmentRepository.updateWCMemoMappingInfo(
                wcMemoSid, wcMemoNo, description, update_usr_sid, update_dt, sid);
    }

    /**
     * 進行轉單完畢,需將轉單後的工作聯絡單Sid,存進備忘錄
     *
     * @param wcSid     工作聯絡單Sid
     * @param wcMemoSid 備忘錄Sid
     */
    public void updateCopyWcMasterSid(String wcSid, String wcMemoSid) {
        try {
            WCMemoMaster wcMemoMaster = this.findBySid(wcMemoSid);
            if (wcMemoMaster.getTransWcSid() == null
                    || wcMemoMaster.getTransWcSid().getWcSidTos() == null) {
                WCSids wcSids = new WCSids();
                wcMemoMaster.setTransWcSid(wcSids);
            }
            WCSidTo wcSidTo = new WCSidTo();
            wcSidTo.setWcSid(wcSid);
            wcMemoMaster.getTransWcSid().getWcSidTos().add(wcSidTo);
            this.save(wcMemoMaster);
        } catch (Exception e) {
            log.warn("updateCopyWcMasterSid ERROR", e);
        }
    }

    /**
     * 存檔
     *
     * @param obj
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCMemoMaster save(WCMemoMaster obj) {
        return wcMemoMasterRepository.save(obj);
    }

    /**
     * 鎖定備忘錄
     *
     * @param memoSid
     * @param user
     * @param lockDate
     */
    public void lock(String memoSid, Integer user, Date lockDate) {
        try {
            wcMemoMasterRepository.updateLockUserAndLockDate(lockDate, user, memoSid);
        } catch (Exception e) {
            log.warn("lock Error : ", e);
        }
    }

    /**
     * 解除鎖定備忘錄
     *
     * @param memoSid
     * @param user
     */
    public void unLock(String memoSid, Integer user) {
        try {
            wcMemoMasterRepository.closeLockUserAndLockDate(null, null, memoSid, user);
        } catch (Exception e) {
            log.warn("unLock Error : ", e);
        }
    }
}
