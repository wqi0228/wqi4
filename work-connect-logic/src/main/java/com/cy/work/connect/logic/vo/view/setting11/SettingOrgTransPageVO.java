/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo.view.setting11;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jimmy_chou
 */
public class SettingOrgTransPageVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8324309346111171438L;

    /**
     * 組織異動生效日
     */
    @Getter
    @Setter
    private Long qryEffectiveDate;

    /**
     * 組織異動前後對應設定資料查詢結果
     */
    @Getter
    @Setter
    private List<SettingOrgTransMappingVO> verifyDtVOList;

    @Getter
    @Setter
    private SettingOrgTransMappingVO editVerifyDtVO;

    @Getter
    @Setter
    private SettingOrgTransMappingVO selVerifyDtVO;

    /**
     * 建構式
     */
    public SettingOrgTransPageVO() {
        // 初始化
        this.verifyDtVOList = Lists.newArrayList();
    }
}
