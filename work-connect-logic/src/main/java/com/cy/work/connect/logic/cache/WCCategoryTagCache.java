/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCCategoryTagRepository;
import com.cy.work.connect.vo.WCCategoryTag;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 類別大類Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCCategoryTagCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6544490582365405731L;
    private final transient Comparator<WCCategoryTag> wcCategoryTagComparator = new Comparator<WCCategoryTag>() {
        @Override
        public int compare(WCCategoryTag obj1, WCCategoryTag obj2) {
            final Integer seq1 = obj1.getSeq();
            final Integer seq2 = obj2.getSeq();
            return seq1.compareTo(seq2);
        }
    };
    @Autowired
    private WCCategoryTagRepository wcCategoryTagRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WCCategoryTag> wcCategoryTagMap;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    public WCCategoryTagCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcCategoryTagMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay =  ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立大類(類別)Cache");
        try {
            Map<String, WCCategoryTag> tempWCCategoryTagMap = Maps.newConcurrentMap();
            wcCategoryTagRepository
                    .findAll()
                    .forEach(
                            item -> {
                                tempWCCategoryTagMap.put(item.getSid(), item);
                            });
            wcCategoryTagMap = tempWCCategoryTagMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立類別大類Cache結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新Cache
     *
     * @param wcCategoryTag 類別大類物件
     */
    public synchronized void updateCacheByWCCategoryTag(WCCategoryTag wcCategoryTag) {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立大類(類別)Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立大類(類別)Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
        wcCategoryTagMap.put(wcCategoryTag.getSid(), wcCategoryTag);
    }

    /**
     * 取得類別大類
     *
     * @param sid key
     * @return
     */
    public WCCategoryTag getWCCategoryTagBySid(String sid) {
        return wcCategoryTagMap.get(sid);
    }

    /**
     * 取得類別大類 List
     *
     * @return
     */
    public List<WCCategoryTag> getWCCategoryTags() {
        List<WCCategoryTag> result = Lists.newArrayList();
        try {
            wcCategoryTagMap
                    .values()
                    .forEach(
                            item -> {
                                result.add(item);
                            });
        } catch (Exception e) {
            log.warn("getWCCategoryTags ERROR", e);
        }
        Collections.sort(result, wcCategoryTagComparator);
        return result;
    }

    /**
     * @param compId
     * @return
     */
    public List<WCCategoryTag> findAllAndSortBySeq(String compId) {
        // ====================================
        // 查詢 公司別 sid
        // ====================================
        Org comp = WkOrgCache.getInstance().findById(compId);
        if (comp == null) {
            return Lists.newArrayList();
        }

        return wcCategoryTagMap.values().stream()
                // 過濾公司別
                .filter(wCCategoryTag -> WkCommonUtils.compareByStr(wCCategoryTag.getCompSid(), comp.getSid()))
                // 以 SEQ 排序
                .sorted(wcCategoryTagComparator)
                .collect(Collectors.toList());
    }
}
