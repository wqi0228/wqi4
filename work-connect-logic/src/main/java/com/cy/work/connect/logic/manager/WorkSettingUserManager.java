/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WorkSettingUserManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7923238658938171840L;

    private static WorkSettingUserManager instance;

    public static WorkSettingUserManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WorkSettingUserManager.instance = this;
    }

    /**
     * 組選擇部門的顯示文字
     *
     * @param userSids 選擇人員
     * @param isExecpt 是否為：被選擇除外
     * @return
     */
    public String prepareUsersShowText(List<Integer> userSids, boolean isExecpt) {

        if (userSids == null) {
            return "";
        }

        // ====================================
        // 取得人員
        // ====================================
        List<User> users = WkUserCache.getInstance().findBySids(userSids);

        if (WkStringUtils.notEmpty(users)) {
            // 依暱稱排序
            users =
                users.stream().sorted(Comparator.comparing(User::getName))
                    .collect(Collectors.toList());
        }

        // ====================================
        // 組顯示文字
        // ====================================
        String showContent = "";
        for (User user : users) {
            if (user == null || WkStringUtils.isEmpty(user.getName())) {
                continue;
            }

            if (WkStringUtils.notEmpty(showContent)) {
                showContent += "<br/>";
            }

            if (Activation.ACTIVE.equals(user.getStatus())) {
                showContent += user.getName();

            } else {
                showContent += "<span class='WS1-1-2' style='text-decoration: line-through;'>(停用)&nbsp;";
                showContent +=
                    "<span class='WS1-1-1' style='text-decoration: none;'>" + user.getName()
                        + "</span>";
                showContent += "</span>";
            }

            if (isExecpt) {
                showContent += "【<span class='WS1-1-2'>除外</span>】";
            }
        }
        return showContent;
    }
}
