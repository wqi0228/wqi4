package com.cy.work.connect.logic.vo.view.setting11.subfunc;

import com.cy.work.connect.repository.vo.Setting11BaseDtVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

/**
 * @author jimmy_chou
 */
public class Setting11TransDepVO extends Setting11BaseDtVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3846173290112191123L;

    /**
     * 指派單位
     */
    @Getter
    @Setter
    List<Integer> transDeps;

    /**
     * 需求方簽核
     */
    @Getter
    @Setter
    String reqSignInfo;

    /**
     * 執行模式
     */
    @Getter
    @Setter
    String execMode;

    /**
     * @param baseVO
     */
    public Setting11TransDepVO(Setting11BaseDtVO baseVO) {
        BeanUtils.copyProperties(baseVO, this);
    }
}
