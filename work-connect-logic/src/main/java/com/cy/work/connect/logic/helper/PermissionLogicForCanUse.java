/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
@Service
public class PermissionLogicForCanUse implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -9064357082700131535L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static PermissionLogicForCanUse instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static PermissionLogicForCanUse getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(PermissionLogicForCanUse instance) { PermissionLogicForCanUse.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient WCMasterManager wcMasterManager;
    @Autowired
    private transient WCExecDepManager wcExecDepManager;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 可使用編輯、上傳附件 (申請單位)
     * 
     * @param wcMaster     主檔
     * @param loginUserSid 登入者 sid
     * @return
     */
    public boolean isCanUseReqSideEditAndUploadAttch(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseReqSideEditAndUploadAttch()) {
            return false;
        }

        // ====================================
        // 判斷是否為申請單位流程可簽名
        // ====================================
        return SignFlowForRequireLogic.getInstance().isReqFlowSignUser(wcMaster.getSid(), loginUserSid, null);
    }

    /**
     * 可使用編輯、上傳附件 (申請單位)
     * 
     * @param wcSid        主檔 sid
     * @param loginUserSid 登入者 sid
     * @return
     */
    public boolean isCanUseReqSideEditAndUploadAttch(String wcSid, Integer loginUserSid) {
        return this.isCanUseReqSideEditAndUploadAttch(
                this.wcMasterManager.findBySid(wcSid),
                loginUserSid);
    }

    /**
     * 可上傳附件 (執行單位)
     * 
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public boolean isCanExecSideUploadAttch(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanExecSideUploadAttch()) {
            return false;
        }

        // ====================================
        // 判斷是否擁有執行方檢視權限
        // ====================================
        Set<WCExceDepStatus> withoutWCExceDepStatus = Sets.newHashSet(
                WCExceDepStatus.CLOSE);

        return PermissionLogicForExecDep.getInstance().isExecDepRelationUser(
                wcMaster.getSid(), loginUserSid, withoutWCExceDepStatus);
    }

    /**
     * 是否可增加『可閱人員』
     * 
     * @param wcMaster     主檔
     * @param loginUserSid 登入者 sid
     * @return
     */
    public boolean isCanEditSpecViewPerson(WCMaster wcMaster, Integer loginUserSid) {

        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanEditSpecViewPerson()) {
            return false;
        }

        // ====================================
        // 申請人
        // ====================================
        if (WkCommonUtils.compareByStr(wcMaster.getCreate_usr(), loginUserSid)) {
            return true;
        }

        // ====================================
        // 申請部門主管(含直系向上主管)
        // ====================================
        Set<Integer> managerWithChildOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);
        if (managerWithChildOrgSids.contains(loginUserSid)) {
            return true;
        }

        // ====================================
        // 為執行人員
        // ====================================
        List<WCExceDep> wcExceDeps = wcExecDepManager.findActiveByWcSid(wcMaster.getSid());
        for (WCExceDep wcExceDep : wcExceDeps) {
            if (WkCommonUtils.compareByStr(wcExceDep.getExecUserSid(), loginUserSid)) {
                return true;
            }
        }

        // ====================================
        // 為需求單位[主流程或會簽]流程，待簽或已簽人員
        // ====================================
        if (SignFlowForRequireLogic.getInstance().isRequireFlowCanSignOrSignedUser(wcMaster.getSid(), loginUserSid)) {
            return true;
        }

        return false;
    }

    /**
     * 可使用回覆
     *
     * @param wcMaster 工作聯絡單物件
     * @param user     登入者
     * @return boolean
     */
    public boolean isCanUseReply(WCMaster wcMaster, Integer loginUserSid) {

        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseReply()) {
            return false;
        }

        // ====================================
        // 申請者
        // ====================================
        if (WkCommonUtils.compareByStr(wcMaster.getCreate_usr(), loginUserSid)) {
            return true;
        }

        // ====================================
        // 申請部門主管(含直系向上主管)
        // ====================================
        Set<Integer> managerWithChildOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);
        if (managerWithChildOrgSids.contains(loginUserSid)) {
            return true;
        }

        // ====================================
        // 執行單位可回覆權限
        // ====================================
        if (PermissionLogicForExecDep.getInstance().isCanReplyByExecDep(wcMaster.getSid(), loginUserSid)) {
            return true;
        }

        // ====================================
        // 單據簽核人員
        // ====================================
        // 為需求單位[主流程或會簽]流程，待簽或已簽人員
        if (SignFlowForRequireLogic.getInstance().isRequireFlowCanSignOrSignedUser(wcMaster.getSid(), loginUserSid)) {
            return true;
        }
        // 為執行單位[主流程或會簽]流程，待簽或已簽人員
        if (SignFlowForExecDepLogic.getInstance().isExecDepFlowCanSignOrSignedUser(wcMaster.getSid(), loginUserSid)) {
            return true;
        }

        return false;
    }

    /**
     * 登入者在此單是否可進行轉單
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 登入者
     * @return
     */
    public boolean isCanUseExecSideDepTransExecutor(
            WCMaster wcMaster,
            Integer loginUserSid,
            Integer loginDepSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseExecSideDepTransExecutor()) {
            return false;
        }

        // ====================================
        // 取得轉單相關的執行單位
        // ====================================
        return ExecDepLogic.getInstance().findExecDepForCanTransPerson(
                wcMaster.getSid(),
                loginUserSid,
                loginDepSid).size() > 0;
    }

    /**
     * 可使用執行方派工給執行人員
     * 
     * @param wcSid        主單 sid
     * @param loginUserSid 登入者 sid
     * @return
     */
    public boolean isCanUseExecSideAssignExecutor(WCMaster wcMaster, Integer loginUserSid) {

        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseExecSideAssignExecutor()) {
            return false;
        }

        // ====================================
        // 判斷是否有『可派工』的『待派工』執行單位
        // ====================================
        return WaitAssignLogic.getInstance().findCanAssignWcExecDepsByUser(wcMaster.getSid(), loginUserSid)
                .size() > 0;
    }

    /**
     * 可使用執行方加派執行單位
     * 
     * @param wcMaster     工作聯絡單
     * @param loginUserSid 登入者
     * @return
     */
    public boolean isCanUseExecSideDepAddExecDep(WCMaster wcMaster, Integer loginUserSid) {

        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseExecSideDepAddExecDep()) {
            return false;
        }

        // ====================================
        // 判斷使用者是否可加派
        // ====================================
        return PermissionLogicForExecDep.getInstance().isCanAddExecDep(wcMaster.getSid(), loginUserSid);
    }

    /**
     * 可使用執行方加派執行單位
     * 
     * @param wcSid        工作聯絡單 sid
     * @param loginUserSid 登入者
     * @return
     * @return
     */
    public boolean isCanUseExecSideDepAddExecDep(String wcSid, Integer loginUserSid) {
        return this.isCanUseExecSideDepAddExecDep(
                this.wcMasterManager.findBySid(wcSid),
                loginUserSid);
    }

    /**
     * 可使用執行方『執行完成』
     *
     * @param wcSid        工作聯絡單Sid
     * @param loginUserSid 登入者
     * @return
     */
    public boolean isCanUseExecSideComplete(WCMaster wcMaster, Integer loginUserSid) {

        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseExecSideComplete()) {
            return false;
        }

        // ====================================
        // 查詢是否有可執行完成的單位
        // ====================================
        return PermissionLogicForExecDep.getInstance().findCanExecFinish(wcMaster.getSid(), loginUserSid).size() > 0;
    }

    /**
     * 是否可進行『結案』(確認完成)
     * 
     * @param wcMaster     主檔
     * @param loginUserSid user sid
     * @return
     */
    public boolean isCanUseReqSideClose(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseReqSideClose()) {
            return false;
        }

        // ====================================
        // 比對登入者是否有權限結案
        // ====================================
        // 收集有權限的 user sid
        Set<Integer> rightUserSids = Sets.newHashSet();
        // 1. 『申請者』可結案
        rightUserSids.add(wcMaster.getCreate_usr());
        // 2. 『申請部門』直系向上主管可結案
        Set<Integer> directManagerUserSids = WkOrgCache.getInstance().findDirectManagerUserSids(wcMaster.getDep_sid());
        if (WkStringUtils.notEmpty(directManagerUserSids)) {
            rightUserSids.addAll(directManagerUserSids);
        }

        // 比對
        if (!rightUserSids.contains(loginUserSid)) {
            return false;
        }

        // ====================================
        // 比對是否所有單位都執行完成
        // ====================================
        return this.wcExecDepManager.isAllExecDepFinish(wcMaster.getSid());

    }

    /**
     * 是否可進行『結案』(確認完成)
     * 
     * @param wcSid        主檔 sid
     * @param loginUserSid 登入者 sid
     * @return
     */
    public boolean isCanUseReqSideClose(String wcSid, Integer loginUserSid) {
        return this.isCanUseReqSideClose(
                this.wcMasterManager.findBySid(wcSid),
                loginUserSid);
    }

    /**
     * 可使用意見說明(申請單位)
     * 
     * @param wcMaster     主檔
     * @param loginUserSid user sid
     * @return
     */
    public boolean isCanUseReqSideComments(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseReqSideComments()) {
            return false;
        }

        // ====================================
        // 為『需求方-主流程』的『待簽者』
        // ====================================
        return SignFlowForRequireLogic.getInstance().isReqFlowSignUser(wcMaster.getSid(), loginUserSid, SignType.SIGN);
    }

    /**
     * 意見說明(會簽者)
     * 
     * @param wcMaster     主檔
     * @param loginUserSid user sid
     * @return
     */
    public boolean isCanUseReqSideCountersignComments(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseReqSideCountersignComments()) {
            return false;
        }

        // ====================================
        // 為『需求方-會簽流程』的『待簽者』
        // ====================================
        return SignFlowForRequireLogic.getInstance().isReqFlowSignUser(wcMaster.getSid(), loginUserSid, SignType.COUNTERSIGN);
    }

    /**
     * 可使用意見說明(執行方簽核者)
     * 
     * @param wcMaster     主檔
     * @param loginUserSid user sid
     * @return
     */
    public boolean isCanUseExecSideCmments(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseExecSideCmments()) {
            return false;
        }

        // ====================================
        // 為『執行方-簽核流程』的『待簽者』
        // ====================================
        return SignFlowForExecDepLogic.getInstance().isCanSignBtn(wcMaster.getSid(), loginUserSid, SignType.SIGN);
    }

    /**
     * 可使用意見說明(執行方會簽核者)
     *
     * @param wcMaster     主檔
     * @param loginUserSid user sid
     * @return
     */
    public boolean isCanUseExecSideCountersignCmments(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseExecSideCmments()) {
            return false;
        }

        // ====================================
        // 為『執行方-會簽流程』的『待簽者』
        // ====================================
        return SignFlowForExecDepLogic.getInstance().isCanSignBtn(wcMaster.getSid(), loginUserSid, SignType.COUNTERSIGN);
    }

    /**
     * 可使用申請方加會簽
     * 
     * @param wcMaster     主檔
     * @param loginUserSid user sid
     * @return
     */
    public boolean isCanUseReqSideAddCountersign(WCMaster wcMaster, Integer loginUserSid) {
        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseReqSideAddCountersign()) {
            return false;
        }

        // ====================================
        // 檢查為需求單位[主流程或會簽]流程，待簽或或已簽人員
        // ====================================
        return SignFlowForRequireLogic.getInstance().isRequireFlowCanSignOrSignedUser(wcMaster.getSid(), loginUserSid);
    }

    /**
     * 可使用執行方加會簽
     * 
     * @param wcMaster     主檔
     * @param loginUserSid user sid
     * @return
     */
    public boolean isCanUseExecSideAddCountersign(WCMaster wcMaster, Integer loginUserSid) {

        // ====================================
        // 單據狀態檢查
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null
                || !wcMaster.getWc_status().isCanUseExecSideAddCountersign()) {
            return false;
        }

        // ====================================
        // 為執行方簽核人員『待簽、已簽』
        // ====================================
        if (SignFlowForExecDepLogic.getInstance().isExecDepFlowCanSignOrSignedUser(wcMaster.getSid(), loginUserSid)) {
            return true;
        }

        // ====================================
        // 為進行中、已完成執行單位人員
        // ====================================
        if (PermissionLogicForExecDep.getInstance().isCanCountersign(wcMaster.getSid(), loginUserSid)) {
            return true;
        }

        return false;
    }

}
