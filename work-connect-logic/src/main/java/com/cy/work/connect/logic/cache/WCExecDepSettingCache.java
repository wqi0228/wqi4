/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCExecDepSettingRepository;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 執行單位設定Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCExecDepSettingCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6800349385431491684L;

    @Autowired
    private WCExecDepSettingRepository wcExecDepSettingRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WCExecDepSetting> wcExecDepSettingMap;
    /**
     * Cache 物件
     */
    private Map<String, Map<String, WCExecDepSetting>> wcExecDepSettingsMapByTagSid;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    public WCExecDepSettingCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcExecDepSettingMap = Maps.newConcurrentMap();
        wcExecDepSettingsMapByTagSid = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay = ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立執行單位設定Cache");
        try {
            List<WCExecDepSetting> wCExecDepSettings = wcExecDepSettingRepository.findAll();

            Map<String, WCExecDepSetting> tempExecDepSettingMap = Maps.newConcurrentMap();
            Map<String, Map<String, WCExecDepSetting>> tempExecDepSettingsMapByTagSid = Maps.newConcurrentMap();
            for (WCExecDepSetting wcExecDepSetting : wCExecDepSettings) {
                
                //以 wc_exec_dep_setting_sid 為 key
                tempExecDepSettingMap.put(wcExecDepSetting.getSid(), wcExecDepSetting);
                
                //以 tagSid 為 key
                Map<String, WCExecDepSetting> execDepSettingMap  = tempExecDepSettingsMapByTagSid.get(wcExecDepSetting.getWc_tag_sid());
                if(execDepSettingMap==null) {
                    execDepSettingMap = Maps.newConcurrentMap();
                    tempExecDepSettingsMapByTagSid.put(wcExecDepSetting.getWc_tag_sid(), execDepSettingMap);
                }
                execDepSettingMap.put(wcExecDepSetting.getSid(), wcExecDepSetting);
            }

            this.wcExecDepSettingMap = tempExecDepSettingMap;
            this.wcExecDepSettingsMapByTagSid = tempExecDepSettingsMapByTagSid;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立執行單位設定Cache結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新Cache
     *
     * @param execDepSetting 執行單位設定物件
     */
    public synchronized void updateCacheByExecDepSetting(WCExecDepSetting execDepSetting) {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立執行單位設定Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立執行單位設定Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
        wcExecDepSettingMap.put(execDepSetting.getSid(), execDepSetting);
        
        //以 tagSid 為 key
        Map<String, WCExecDepSetting> execDepSettingMap  = wcExecDepSettingsMapByTagSid.get(execDepSetting.getWc_tag_sid());
        if(execDepSettingMap==null) {
            execDepSettingMap = Maps.newConcurrentMap();
            wcExecDepSettingsMapByTagSid.put(execDepSetting.getWc_tag_sid(), execDepSettingMap);
        }
        execDepSettingMap.put(execDepSetting.getSid(), execDepSetting);
        
    }

    /**
     * 取得執行單位設定 By 執行單位設定Sid
     *
     * @param wc_exec_dep_setting_sid 執行單位設定Sid
     * @return
     */
    public WCExecDepSetting getExecDepSettingBySid(String wc_exec_dep_setting_sid) {
        return wcExecDepSettingMap.get(wc_exec_dep_setting_sid);
    }

    /**
     * 取得執行單位設定s By Tag Sid
     *
     * @param wc_tag_sid TagSid
     * @return
     */
    public List<WCExecDepSetting> getExecDepSettingsByTagSid(
            String wc_tag_sid, Activation activation) {
        List<WCExecDepSetting> result = Lists.newArrayList();
        try {
            wcExecDepSettingMap
                    .values()
                    .forEach(
                            item -> {
                                if (activation != null && !activation.equals(item.getStatus())) {
                                    return;
                                }
                                if (wc_tag_sid.equals(item.getWc_tag_sid())) {
                                    result.add(item);
                                }
                            });
        } catch (Exception e) {
            log.warn("getExecDepSettingsByTagSid ERROR", e);
        }
        return result;
    }

    /**
     * 以小類 sid 查詢
     * 
     * @param wcTagSid 小類 sid 查詢
     * @return WCExecDepSetting list
     */
    public List<WCExecDepSetting> findByTagSid(String wcTagSid) {
        
        Map<String, WCExecDepSetting> execDepSettingMap  = this.wcExecDepSettingsMapByTagSid.get(wcTagSid);
        if(execDepSettingMap==null) {
            return Lists.newArrayList();
        }
        return execDepSettingMap.values().stream().collect(Collectors.toList());
    }
}
