package com.cy.work.connect.logic.cache;

import com.cy.work.connect.logic.helper.WCManagerSignInfoHelper;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.repository.WCManagerSignInfoRepository;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 需求單位簽核Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCManagerSignInfoCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6952611706982953358L;

    @Autowired
    private WCManagerSignInfoRepository wcManagerSignInfoRepository;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;
    @Autowired
    private WCManagerSignInfoHelper wcManagerSignInfoHelper;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;
    /**
     * Cache 物件
     */
    private Map<String, Map<Integer, List<WCManagerSignInfo>>> wcManagerSignInfoMap;

    private Map<String, SingleManagerSignInfo> singleManagerSignInfoMap;


    private boolean isRunning = false;
    @Autowired
    private WCMasterManager wcMasterManager;

    public WCManagerSignInfoCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcManagerSignInfoMap = Maps.newConcurrentMap();
        singleManagerSignInfoMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 更新Cache By 工作聯絡單Sid
     *
     * @param wcSid 工作聯絡單Sid
     */
    public void updateCacheByWcSid(String wcSid) {
        try {
            this.processWait();
            List<WCManagerSignInfo> dbDatas = wcManagerSignInfoRepository.findOneByWCSid(wcSid);
            Map<Integer, List<WCManagerSignInfo>> wcManagerSignInfos = Maps.newConcurrentMap();
            dbDatas.forEach(
                    item -> {
                        singleManagerSignInfoMap.put(
                                item.getBpmId(),
                                new SingleManagerSignInfo(
                                        item.getSid(),
                                        item.getBpmId(),
                                        item.getUser_sid(),
                                        item.getDep_sid(),
                                        item.getSignType(),
                                        item.getStatus(),
                                        item.getBpmDefaultSignedName(),
                                        null,
                                        item.getGroupSeq()));
                        List<WCManagerSignInfo> groupWcManagerSignInfos = wcManagerSignInfos.get(item.getGroupSeq());
                        if (groupWcManagerSignInfos == null) {
                            groupWcManagerSignInfos = Lists.newArrayList();
                        }
                        groupWcManagerSignInfos.add(item);
                        wcManagerSignInfos.put(item.getGroupSeq(), groupWcManagerSignInfos);
                    });
            wcManagerSignInfoMap.put(wcSid, wcManagerSignInfos);
        } catch (Exception e) {
            log.warn("updateCacheByWcSid ERROR", e);
        }
    }

    /**
     * 取得需求單位群組簽核資訊 By 工作聯絡單Sid
     *
     * @param wc_ID 工作聯絡單Sid
     * @return
     */
    public List<GroupManagerSignInfo> getGroupManagerSignInfo(String wc_ID) {

        Map<Integer, List<WCManagerSignInfo>> selWsis = wcManagerSignInfoMap.get(wc_ID);

        if (selWsis == null || selWsis.values().isEmpty()) {
            return Lists.newArrayList();
        }

        List<GroupManagerSignInfo> groupManagerSignInfos = Lists.newArrayList();

        selWsis
                .values()
                .forEach(
                        item -> {
                            if (item == null || item.isEmpty()) {
                                return;
                            }
                            GroupManagerSignInfo gsi = new GroupManagerSignInfo(
                                    item.get(0).getGroupSeq(),
                                    wcManagerSignInfoHelper.settingSingleManagerSignInfo(item));
                            groupManagerSignInfos.add(gsi);
                        });
        return groupManagerSignInfos;
    }

    /**
     * 更新Cache By 需求單位簽核物件
     *
     * @param wcManagerSignInfo 需求單位簽核物件
     */
    public void updateCacheByWcIDAndGroupSeq(WCManagerSignInfo wcManagerSignInfo) {
        this.processWait();
        Map<Integer, List<WCManagerSignInfo>> wcManagerSignInfos = wcManagerSignInfoMap.get(wcManagerSignInfo.getWcSid());
        if (wcManagerSignInfos == null) {
            wcManagerSignInfos = Maps.newConcurrentMap();
        }
        List<WCManagerSignInfo> groupWcManagerSignInfos = wcManagerSignInfos.get(wcManagerSignInfo.getGroupSeq());
        if (groupWcManagerSignInfos == null) {
            groupWcManagerSignInfos = Lists.newArrayList();
        }
        List<WCManagerSignInfo> tempWCManagerSignInfos = Lists.newArrayList();
        groupWcManagerSignInfos.forEach(
                item -> {
                    if (item.getSid().equals(wcManagerSignInfo.getSid())) {
                        return;
                    }
                    tempWCManagerSignInfos.add(item);
                });
        if (wcManagerSignInfo.getStatus() != null
                && !BpmStatus.DELETE.equals(wcManagerSignInfo.getStatus())) {
            tempWCManagerSignInfos.add(wcManagerSignInfo);
        }
        wcManagerSignInfos.put(wcManagerSignInfo.getGroupSeq(), tempWCManagerSignInfos);
        wcManagerSignInfoMap.put(wcManagerSignInfo.getWcSid(), wcManagerSignInfos);
        singleManagerSignInfoMap.put(
                wcManagerSignInfo.getBpmId(),
                new SingleManagerSignInfo(
                        wcManagerSignInfo.getSid(),
                        wcManagerSignInfo.getBpmId(),
                        wcManagerSignInfo.getUser_sid(),
                        wcManagerSignInfo.getDep_sid(),
                        wcManagerSignInfo.getSignType(),
                        wcManagerSignInfo.getStatus(),
                        wcManagerSignInfo.getBpmDefaultSignedName(),
                        null,
                        wcManagerSignInfo.getGroupSeq()));
    }

    /**
     * 取得目前Seq 最大值
     *
     * @param groupSeq 群組Seq
     * @param wc_ID    工作聯絡單Sid
     * @return
     */
    public int getMaxSeq(int groupSeq, String wc_ID) {
        Map<Integer, List<WCManagerSignInfo>> selWsis = wcManagerSignInfoMap.get(wc_ID);
        if (selWsis == null || selWsis.values().isEmpty()) {
            return 0;
        }
        List<WCManagerSignInfo> grouMsis = selWsis.get(groupSeq);
        if (grouMsis == null || grouMsis.isEmpty()) {
            return 0;
        }
        int seq = 0;
        for (WCManagerSignInfo ws : grouMsis) {
            if (seq < ws.getSeq()) {
                seq = ws.getSeq();
            }
        }
        return seq + 1;
    }

    /**
     * 取得目前GroupSeq最大值
     *
     * @param wc_ID 工作聯絡單Sid
     * @return
     */
    public int getMaxGroupSeq(String wc_ID) {
        Map<Integer, List<WCManagerSignInfo>> selWsis = wcManagerSignInfoMap.get(wc_ID);
        if (selWsis == null || selWsis.values().isEmpty()) {
            return 0;
        }
        int groupSeq = 0;
        for (Integer gs : selWsis.keySet()) {
            if (groupSeq < gs) {
                groupSeq = gs;
            }
        }
        return groupSeq + 1;
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(cron = "0 0 0 ? * *")
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立需求方簽核資訊Cache");
        try {
            Map<String, Map<Integer, List<WCManagerSignInfo>>> tempGroupManagerSignInfoMapByWcSid = Maps.newConcurrentMap();
            Map<String, SingleManagerSignInfo> tempSingleManagerSignInfoMapByBpmID = Maps.newConcurrentMap();

            List<WCManagerSignInfo> wcManagerSignInfos = wcManagerSignInfoRepository.findAll();
            for (WCManagerSignInfo wcManagerSignInfo : wcManagerSignInfos) {

                if (wcManagerSignInfo.getGroupSeq() == null
                        || wcManagerSignInfo.getStatus() == null
                        || BpmStatus.DELETE.equals(wcManagerSignInfo.getStatus())) {
                    continue;
                }

                tempSingleManagerSignInfoMapByBpmID.put(
                        wcManagerSignInfo.getBpmId(),
                        new SingleManagerSignInfo(
                                wcManagerSignInfo.getSid(),
                                wcManagerSignInfo.getBpmId(),
                                wcManagerSignInfo.getUser_sid(),
                                wcManagerSignInfo.getDep_sid(),
                                wcManagerSignInfo.getSignType(),
                                wcManagerSignInfo.getStatus(),
                                wcManagerSignInfo.getBpmDefaultSignedName(),
                                null,
                                wcManagerSignInfo.getGroupSeq()));

                Map<Integer, List<WCManagerSignInfo>> wcManagerSignInfosMapByGroupSeq = tempGroupManagerSignInfoMapByWcSid.get(wcManagerSignInfo.getWcSid());
                if (wcManagerSignInfosMapByGroupSeq == null) {
                    wcManagerSignInfosMapByGroupSeq = Maps.newConcurrentMap();
                    tempGroupManagerSignInfoMapByWcSid.put(wcManagerSignInfo.getWcSid(), wcManagerSignInfosMapByGroupSeq);
                }
                List<WCManagerSignInfo> groupWcManagerSignInfos = wcManagerSignInfosMapByGroupSeq.get(wcManagerSignInfo.getGroupSeq());
                if (groupWcManagerSignInfos == null) {
                    groupWcManagerSignInfos = Lists.newArrayList();
                    wcManagerSignInfosMapByGroupSeq.put(wcManagerSignInfo.getGroupSeq(), groupWcManagerSignInfos);
                }
                groupWcManagerSignInfos.add(wcManagerSignInfo);

            }

            wcManagerSignInfoMap = tempGroupManagerSignInfoMapByWcSid;
            singleManagerSignInfoMap = tempSingleManagerSignInfoMapByBpmID;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立需求方簽核資訊Cache結束");
        produceTime = Boolean.FALSE;
    }

    @Scheduled(cron = "0 0 0 ? * *") // 每隔四小時觸發 0 4 8 12...
    public void syncUpdateBpm() {
        if (isRunning) {
            return;
        }
        isRunning = true;
        log.debug("建立需求方簽核資訊同步資訊");

        for (Map<Integer, List<WCManagerSignInfo>> managerSignInfoListMap : wcManagerSignInfoMap.values()) {

            for (List<WCManagerSignInfo> groupWcManagerSignInfoItems : managerSignInfoListMap.values()) {

                for (WCManagerSignInfo wcManagerSignInfo : groupWcManagerSignInfoItems) {

                    try {

                        // ====================================
                        // 取得資料庫資料
                        // ====================================
                        WCManagerSignInfo wsi = wcManagerSignInfoRepository.findBySid(wcManagerSignInfo.getSid());
                        if (Strings.isNullOrEmpty(wsi.getWcSid())) {
                            continue;
                        }

                        // ====================================
                        // pass 未產生流程
                        // ====================================
                        if (Strings.isNullOrEmpty(wsi.getBpmId())) {
                            continue;
                        }

                        // ====================================
                        // pass 不需要處理的BPM狀態
                        // ====================================
                        List<BpmStatus> passBpmStatus = Lists.newArrayList(
                                BpmStatus.DELETE,
                                BpmStatus.APPROVED,
                                BpmStatus.INVALID);

                        if (passBpmStatus.contains(wsi.getStatus())) {
                            continue;
                        }

                        // ====================================
                        // pass 不需要處理的主單狀態
                        // ====================================

                        WCStatus wcStatus = this.wcMasterManager.findWcStatusByWcSid(wsi.getWcSid());

                        List<WCStatus> passStatus = Lists.newArrayList(
                                WCStatus.CLOSE,
                                WCStatus.CLOSE_STOP,
                                WCStatus.INVALID);

                        if (passStatus.contains(wcStatus)) {
                            continue;
                        }

                        this.wcManagerSignInfoManager.doSyncBpmData(wsi, null, null);

                    } catch (Exception e) {
                        log.warn("同步BPM資訊有誤-bpm_instance_id:" + wcManagerSignInfo.getBpmId(), e);
                    }
                }
            }
        }

        log.debug("建立需求方簽核資訊同步資訊結束");
        updateCache();
        isRunning = false;
    }

    /**
     * 等待 建立需求方簽核資訊Cache結束
     */
    private synchronized void processWait() {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立需求方簽核資訊Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立需求方簽核資訊Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
    }
}
