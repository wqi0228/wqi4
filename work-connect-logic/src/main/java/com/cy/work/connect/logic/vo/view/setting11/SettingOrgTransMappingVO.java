/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo.view.setting11;

import com.cy.commons.enums.OrgLevel;
import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.Serializable;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

/**
 * 組織異動驗證檔, 查詢結果 VO
 *
 * @author jimmy_chou
 */
public class SettingOrgTransMappingVO extends OrgTransMappingTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2373458757392443476L;
    /**
     * 顯示用:轉換前單位
     */
    @Getter
    private final String showBeforeDep;
    /**
     * 顯示用:轉換前單位 -Tool Tip
     */
    @Getter
    private final String showBeforeDepForToolTip;
    @Getter
    private final String filterBeforeDepName;
    /**
     * 顯示用:轉換後單位
     */
    @Getter
    private final String showAfterDep;
    /**
     * 顯示用:轉換後單位 -Tool Tip
     */
    @Getter
    private final String showAfterDepForToolTip;
    
    @Getter
    private final String filterAfterDepName;

    /**
     * 顯示用:生效日
     */
    @Getter
    private final String showEffectiveDate;

    @Getter
    private Map<String, Object> dataMap = Maps.newHashMap();

    @Getter
    @Setter
    private Integer sortSeq;

    /**
     * @param entity
     */
    public SettingOrgTransMappingVO(OrgTransMappingTo entity) {
        // ====================================
        // 複製所有欄位屬性
        // ====================================
        BeanUtils.copyProperties(entity, this);

        // ====================================
        // 準備頁面顯示資料
        // ====================================

        // 顯示用:轉換前單位
        this.showBeforeDep = "<span class='WS1-1-3'>[" + this.getBeforeOrgSid() + "]</span>"
                + WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeupAndDecorationStyle(
                        this.getBeforeOrgSid(), OrgLevel.DIVISION_LEVEL, true, " → ");

        this.showBeforeDepForToolTip = WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(this.getBeforeOrgSid()), 10);
        this.filterBeforeDepName = WkOrgUtils.findNameBySid(this.getBeforeOrgSid());
        

        // 顯示用:轉換後單位
        this.showAfterDep = "<span class='WS1-1-3'>[" + this.getAfterOrgSid() + "]</span>"
                + WkOrgUtils.prepareBreadcrumbsByDepNameAndMakeupAndDecorationStyle(
                        this.getAfterOrgSid(), OrgLevel.DIVISION_LEVEL, true, " → ");

        this.showAfterDepForToolTip = WkOrgUtils.prepareDepsNameByTreeStyle(Lists.newArrayList(this.getAfterOrgSid()), 10);
        this.filterAfterDepName = WkOrgUtils.findNameBySid(this.getAfterOrgSid());

        // 顯示用:生效日
        this.showEffectiveDate = WkDateUtils.formatDate(this.getEffectiveDate(), WkDateUtils.YYYY_MM_DD);
    }
}
