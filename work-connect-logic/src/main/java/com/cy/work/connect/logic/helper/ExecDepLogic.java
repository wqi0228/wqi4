/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.Activation;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCTransSpecialPermissionManager;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.ExecDepCreateType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
@Service
public class ExecDepLogic implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6640903596643886779L;
    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static ExecDepLogic instance;

    public static ExecDepLogic getInstance() { return instance; }

    private static void setInstance(ExecDepLogic instance) { ExecDepLogic.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;
    @Autowired
    private WCTransSpecialPermissionManager wcTransSpecialPermissionManager;

    // ========================================================================
    // 方法
    // ========================================================================

    /**
     * 是否擁有轉單權限
     * 
     * @param wcExceDep             執行單位物件
     * @param loginUser             登入者
     * @param canTransPersonDepSids 登入者可轉單的部門
     * @param isTransSpecial        是否為特殊可轉單部門
     * @param managerOrgSids
     * @param loginDep
     * @return
     */
    public boolean isTransPersonPermission(
            WCExceDep wcExceDep,
            Integer loginUserSid,
            Set<Integer> managerOrgSids,
            Integer loginDepSid) {

        // ====================================
        // 僅領單人員可閱名單
        // ====================================
        if (WkStringUtils.notEmpty(wcExceDep.getCanViewReceviceUserSids())) {
            if (wcExceDep.getCanViewReceviceUserSids().contains(loginUserSid)) {
                return true;
            }
        }

        // ====================================
        // 為執行人員
        // ====================================
        if (WkCommonUtils.compareByStr(wcExceDep.getExecUserSid(), loginUserSid)) {
            return true;
        }

        // ====================================
        // 為執行部門主副主管(含兼職)
        // ====================================
        if (managerOrgSids.contains(wcExceDep.getDep_sid())) {
            return true;
        }

        // ====================================
        // 判斷為單位的可派工人員
        // ====================================
        if (ExecDepLogic.getInstance().isCanAssignUser(wcExceDep, loginUserSid)) {
            return true;
        }

        // ====================================
        // 為特別轉單權限部門
        // ====================================
        if (wcTransSpecialPermissionManager.isSpecialPermission(loginUserSid, wcExceDep.getDep_sid())) {
            return true;
        }

        return false;
    }

    /**
     * 取得執行單位 for 轉單
     * 
     * @param wcSid
     * @return
     */
    public List<WCExceDep> findExecDepForCanTransPerson(
            String wcSid,
            Integer loginUserSid,
            Integer loginDepSid) {

        // ====================================
        // 取得所有執行單位
        // ====================================
        List<WCExceDep> wcExceDeps = wcExecDepManager.findActiveByWcSid(wcSid);

        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return wcExceDeps;
        }

        // ====================================
        // 過濾掉不可轉單的執行單位
        // ====================================
        // 過濾執行單位狀態需為進行中、已完成未結案
        List<WCExceDepStatus> targetStatus = Lists.newArrayList(
                WCExceDepStatus.PROCEDD,
                WCExceDepStatus.FINISH);

        wcExceDeps = WkCommonUtils.safeStream(wcExceDeps)
                // 過濾執行單位狀態
                .filter(exceDep -> targetStatus.contains(exceDep.getExecDepStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得管理的部門
        // ====================================
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);

        // ====================================
        // 比對轉單權限
        // ====================================
        return wcExceDeps.stream()
                .filter(wcExecDep -> this.isTransPersonPermission(
                        wcExecDep,
                        loginUserSid,
                        managerOrgSids,
                        loginDepSid))
                .collect(Collectors.toList());

    }

    /**
     * 進行加派變更
     *
     * @param wcSid        工作聯絡單Sid
     * @param orgs         挑選的執行部門
     * @param loginUserSid 登入者Sid
     * @throws UserMessageException
     */
    public void saveCustomerExceDeps(String wcSid, Set<Integer> selectedDepSids, Integer loginUserSid) throws UserMessageException {

        // ====================================
        // 主檔
        // ====================================
        // 查詢主檔
        WCMaster wcMaster = this.wcMasterManager.findBySid(wcSid);
        // 檢核主檔狀態
        this.wcMasterHelper.checkWCMasterStatus(wcMaster);

        // ====================================
        // 檢查使用權限
        // ====================================
        if (!PermissionLogicForCanUse.getInstance().isCanUseExecSideDepAddExecDep(wcSid, loginUserSid)) {
            throw new UserMessageException(WkMessage.NEED_RELOAD + "(無法進行加派)", InfomationLevel.WARN);
        }

        // ====================================
        // 取得單據執行單位
        // ====================================
        // 查詢非停用的執行單位檔
        List<WCExceDep> wcExceDeps = this.wcExecDepManager.findActiveByWcSid(wcSid);

        // ====================================
        // 檢核選擇單位中，
        // ====================================
        Set<Integer> exsitSystemExecDepSids = wcExceDeps.stream()
                .filter(wcExceDep -> ExecDepCreateType.SYSTEM.equals(wcExceDep.getExecDepCreateType()))
                .filter(wcExceDep -> selectedDepSids.contains(wcExceDep.getDep_sid()))
                .map(WCExceDep::getDep_sid)
                .collect(Collectors.toSet());

        if (WkStringUtils.notEmpty(exsitSystemExecDepSids)) {
            String userMessage = ""
                    + "『"
                    + WkOrgUtils.findNameBySid(exsitSystemExecDepSids, "、")
                    + "』已經是執行單位！";
            throw new UserMessageException(userMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 計算異動單位
        // ====================================
        // 取得已經存在的加派單位
        Set<Integer> exsitCustomerExecDepSids = wcExceDeps.stream()
                .filter(wcExceDep -> ExecDepCreateType.CUSTOMER.equals(wcExceDep.getExecDepCreateType()))
                .map(WCExceDep::getDep_sid)
                .collect(Collectors.toSet());

        // 避免有同一單位被加派多次, 故用 for loop 處理
        Map<Integer, WCExceDep> customerExecDepMapByDepSid = Maps.newHashMap();
        for (WCExceDep wcExceDep : wcExceDeps) {
            if (!ExecDepCreateType.CUSTOMER.equals(wcExceDep.getExecDepCreateType())) {
                continue;
            }
            // 若加派單位已存在，則以執行中的資料為優先
            if (customerExecDepMapByDepSid.containsKey(wcExceDep.getDep_sid())) {
                WCExceDepStatus exceDepStatus = customerExecDepMapByDepSid.get(wcExceDep.getDep_sid()).getExecDepStatus();
                if (exceDepStatus != null && exceDepStatus.isAlreadyDo()) {
                    continue;
                }
            }
            customerExecDepMapByDepSid.put(wcExceDep.getDep_sid(), wcExceDep);
        }

        // 比對異動單位
        ItemsCollectionDiffTo<Integer> diffTo = WkCommonUtils.itemCollectionDiff(
                exsitCustomerExecDepSids, selectedDepSids);

        // ====================================
        // 檢查是否有不可移除的加派單位
        // ====================================
        if (WkStringUtils.notEmpty(diffTo.getReduceItems())) {
            // 已經開始執行的單位，就不能移除
            Set<Integer> canotRemoveCustomerExecDepSids = diffTo.getReduceItems().stream()
                    // 防呆,應該不會發生
                    .filter(reduceDepSid -> exsitCustomerExecDepSids.contains(reduceDepSid))
                    // 比對執行單位狀態
                    .filter(reduceDepSid -> customerExecDepMapByDepSid.get(reduceDepSid).getExecDepStatus() != null)
                    .filter(reduceDepSid -> customerExecDepMapByDepSid.get(reduceDepSid).getExecDepStatus().isAlreadyDo())
                    .collect(Collectors.toSet());

            if (WkStringUtils.notEmpty(canotRemoveCustomerExecDepSids)) {
                String userMessage = ""
                        + "『"
                        + WkOrgUtils.findNameBySid(canotRemoveCustomerExecDepSids, "、")
                        + "』已經開始執行，無法移除！";
                throw new UserMessageException(userMessage, InfomationLevel.WARN);
            }
        }

        // ====================================
        // 異動加派單位
        // ====================================
        List<WCExceDep> removeExecDeps = WkCommonUtils.safeStream(diffTo.getReduceItems())
                .filter(depSid -> customerExecDepMapByDepSid.containsKey(depSid))
                .map(depSid -> customerExecDepMapByDepSid.get(depSid))
                .collect(Collectors.toList());

        this.wcExecDepManager.saveCustomerExceDep(
                wcSid,
                loginUserSid,
                diffTo.getPlusItems(),
                removeExecDeps);
    }

    /**
     * 登入者，是否為指定的分派人員
     *
     * @param execDepSetting 執行單位設定值檔
     * @param loginUserSid   登入部門
     * @return
     */
    public boolean isDesignateAssignUser(WCExecDepSetting execDepSetting, Integer loginUserSid) {

        // ====================================
        // 需為派工模式、且有設定指派模式
        // ====================================
        if (!execDepSetting.isNeedAssigned() || !execDepSetting.isReadAssignSetting()) {
            return false;
        }

        // 名單為空
        if (WkStringUtils.isEmpty(execDepSetting.getTransUserSids())
                && WkStringUtils.isEmpty(execDepSetting.getTransdepSids())) {
            return false;
        }

        // ====================================
        // 判斷指定分派人員
        // ====================================
        if (execDepSetting.getTransUserSids().contains(loginUserSid)) {
            return true;
        }

        // ====================================
        // 判斷指定分派部門
        // ====================================
        Set<Integer> settingCanAssignDepSids = execDepSetting.getTransdepSids();

        // 含以下
        if (execDepSetting.isTransDepContainFollowing()) {
            // 取得所有設定單位下的子單位
            Set<Integer> settingCanAssignChildDepSids = WkOrgCache.getInstance().findAllChildSids(settingCanAssignDepSids);
            // 將子單位加入清單
            if (WkStringUtils.notEmpty(settingCanAssignChildDepSids)) {
                settingCanAssignDepSids.addAll(settingCanAssignChildDepSids);
            }
        }

        // 關係部門：主部門+管理部門
        Set<Integer> loginUserRelationDepSidsForAssignSetting = RelationDepHelper.findRelationOrgSidsForExecCanAssignSettingAssignDeps(
                loginUserSid);

        // 比對
        for (Integer settingCanAssignDepSid : settingCanAssignDepSids) {
            if (loginUserRelationDepSidsForAssignSetting.contains(settingCanAssignDepSid)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 使用者在此執行單位是否有權限派工
     * 
     * @param wcExceDep
     * @param loginUser
     * @return
     */
    public boolean isCanAssignUser(WCExceDep wcExceDep, Integer loginUserSid) {

        // ====================================
        // 取得可派工部門
        // ====================================
        // 有快取
        Set<Integer> canAssignDepSids = RelationDepHelper.findRelationOrgSidsForExecCanAssign(loginUserSid);

        // ====================================
        // 處理加派單位
        // ====================================
        if (ExecDepCreateType.CUSTOMER.equals(wcExceDep.getExecDepCreateType())) {
            // 比對user 是否有此部門派工權限
            return canAssignDepSids.contains(wcExceDep.getDep_sid());
        }

        // ====================================
        // 處理一般執行部門 (ExecDepCreateType.SYSTEM)
        // ====================================
        // 取得對應執行單位設定檔
        WCExecDepSetting execDepSetting = this.wcExecDepSettingManager.findBySidFromCache(
                wcExceDep.getExecDepSettingSid());

        // 不是派工模式，跳過
        if (execDepSetting == null
                || !execDepSetting.isNeedAssigned()) {
            return false;
        }

        // ================
        // 非指定派工
        // ================
        if (!execDepSetting.isReadAssignSetting()) {
            // 直接比對user 是否有此部門派工權限
            return canAssignDepSids.contains(wcExceDep.getDep_sid());
        }
        // ================
        // 指定派工
        // ================
        else {
            // 登入者，是否為指定的分派人員
            return this.isDesignateAssignUser(execDepSetting, loginUserSid);
        }
    }

    /**
     * 取得所有執行單位單位的僅領單人員可閱名單
     *
     * @param wcSid
     * @return
     */
    public Set<Integer> findOnlyReceviceUserSidsByAllExecDep(String wcSid) {
        // 查詢所有有效的執行單位
        List<WCExceDep> wcExceDeps = wcExecDepManager.findActiveByWcSid(wcSid);
        // 收集僅領單人員名單
        return this.findOnlyReceviceUserSidsByAllExecDep(wcExceDeps);
    }

    /**
     * 取得所有執行單位單位的僅領單人員可閱名單
     * 
     * @param wcExceDeps WCExceDep list
     * @return
     */
    public Set<Integer> findOnlyReceviceUserSidsByAllExecDep(List<WCExceDep> wcExceDeps) {

        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return Sets.newHashSet();
        }

        return wcExceDeps.stream()
                // 過濾無效的執行單位檔
                .filter(wcExceDep -> Activation.ACTIVE.equals(wcExceDep.getStatus()))
                // 取得僅領單人員可閱名單
                .map(WCExceDep::getReceviceUserSids)
                // 排除名單為空
                .filter(receviceUserSids -> WkStringUtils.notEmpty(receviceUserSids))
                // 收集領單人員名單
                .flatMap(receviceUserSids -> receviceUserSids.stream()) // list merge
                .collect(Collectors.toSet());
    }

}
