/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.helper.MenuTagHelper;
import com.cy.work.connect.repository.WCConfigTempletRepository;
import com.cy.work.connect.repository.WCTagRepository;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.WCTag;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCTagManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6785199385388411121L;

    private static WCTagManager instance;

    public static WCTagManager getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCTagManager.instance = this;
    }

    @Autowired
    private WCTagRepository wcTagRepository;
    @Autowired
    private WCConfigTempletRepository configTempletRepository;
    @Autowired
    private WCTagCache wCTagCache;
    @Autowired
    private MenuTagHelper menuTagHelper;

    /**
     * 依公司檢查類別名稱是否已使用
     *
     * @param companySid   登入公司Sid
     * @param categoryName 大類(類別)名稱
     * @param menuName     中類(單據名稱)名稱
     * @param tagName      小類(執行項目)名稱
     * @return
     */
    public Integer countByTagNameAndCompanySid(
            Integer companySid, String categoryName, String menuName, String tagName) {
        return wcTagRepository.countByTagNameAndCompanySid(companySid, categoryName, menuName,
                tagName);
    }

    /**
     * 依公司及類別名稱尋找執行項目
     *
     * @param companySid   登入公司Sid
     * @param categoryName 大類(類別)名稱
     * @param menuName     中類(單據名稱)名稱
     * @param tagName      小類(執行項目)名稱
     * @return
     */
    public List<WCTag> findByTagNameAndCompanySid(
            Integer companySid, String categoryName, String menuName, String tagName) {
        return wcTagRepository.findByTagNameAndCompanySid(companySid, categoryName, menuName,
                tagName);
    }

    /**
     * 取得執行項目List By Sid list
     *
     * @param sids 小類(執行項目)Sid清單
     * @return
     */
    public List<WCTag> getWCTagsBySidList(List<String> sids) {
        List<WCTag> result = wCTagCache.getWCTag(null, null).stream()
                .filter(each -> sids.contains(each.getSid()))
                .collect(Collectors.toList());
        return result;
    }

    /**
     * 刪除執行項目
     *
     * @param sid
     */
    public void deleteWCTag(String sid) {
        this.wcTagRepository.delete(sid);
        log.info("刪除執行項目:" + sid);
    }

    public WCTag getWCTagBySid(String tagSid) {
        return wCTagCache.findBySid(tagSid);
    }

    public WCTag createWCTag(WCTag wCTag, Integer loginUserSid) {
        wCTag.setCreateDate(new Date());
        wCTag.setCreateUser(loginUserSid);
        // 排序序號
        // wCTag.setSeq(Integer.MAX_VALUE);
        wCTag.setSeq(
                wCTagCache.getWCTag(wCTag.getWcMenuTagSid(), Activation.ACTIVE).stream()
                        .reduce((a, b) -> b)
                        .map(WCTag::getSeq)
                        .orElseGet(() -> 0)
                        + 1);
        wCTag = wcTagRepository.save(wCTag);
        wCTagCache.updateCacheBySid(wCTag);
        return wCTag;
    }

    public void updateWCTag(WCTag wCTag, Integer loginUserSid) {
        wCTag.setUpdateDate(new Date());
        wCTag.setUpdateUser(loginUserSid);
        wCTag = wcTagRepository.save(wCTag);
        wCTagCache.updateCacheBySid(wCTag);
    }

    public List<WCTag> getWCTagForManager(String wcMenuTagSid, Activation status) {
        return wCTagCache.getWCTag(wcMenuTagSid, status);
    }

    /**
     * 取得執行項目List For 主檔編輯使用
     *
     * @param wcMenuTagSid     中類(單據名稱)Sid
     * @param loginManagedDeps 登入單位Sid & 管理單位Sid
     * @param loginUserSid     登入者 sid
     * @param loginCompSid     登入公司
     * @return
     */
    public List<WCTag> getWCTagActiveForMaster(
            String wcMenuTagSid,
            final Set<Integer> loginManagedDeps,
            Integer loginUserSid,
            Integer loginCompSid) {

        List<WCTag> result = Lists.newArrayList();

        // ====================================
        // 由快取取得所有可用的執行項目
        // ====================================
        List<WCTag> tags = wCTagCache.getWCTag(wcMenuTagSid, Activation.ACTIVE);
        if (WkStringUtils.isEmpty(tags)) {
            return result;
        }

        // ====================================
        // 取得所有部門設定模版
        // ====================================
        List<WCConfigTemplet> wcConfigTemplets = configTempletRepository.findAll();

        // ====================================
        // 收集可用的執行項目
        // ====================================
        // 取得全公司的非停用單位
        List<Org> allCompActiveDeps = WkOrgCache.getInstance().findAllActiveDepByCompSid(loginCompSid);

        for (WCTag tag : tags) {
            if (menuTagHelper.checkCanShowTagItem(
                    tag,
                    wcConfigTemplets,
                    loginManagedDeps,
                    loginUserSid,
                    loginCompSid,
                    allCompActiveDeps)) {

                result.add(tag);
            }
        }

        return result;
    }

    /**
     * 取得執行項目(For 使用者管理執行單位)
     *
     * @param wcMenuTagSid 中類(單據名稱)Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<WCTag> getWCTagActiveForManager(String wcMenuTagSid, Integer loginUserSid) {
        List<WCTag> result = Lists.newArrayList();
        wCTagCache
                .getWCTag(wcMenuTagSid, Activation.ACTIVE)
                .forEach(
                        item -> {
                            if (menuTagHelper.checkCanShowManagerTagItem(item, loginUserSid)) {
                                result.add(item);
                            }
                        });
        return result;
    }

    /**
     * 依據傳入 List 順序, 更新排序序號
     *
     * @param sids 小類(執行項目)Sid清單
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void updateSortSeqByListIndex(List<String> sids) {

        // 傳入列表為空
        if (WkStringUtils.isEmpty(sids)) {
            return;
        }

        Integer index = 1;
        for (String sid : sids) {

            // 取資料檔
            WCTag wcTag = wcTagRepository.findOne(sid);

            if (wcTag == null) {
                continue;
            }

            wcTag.setSeq(index++);

            // 更新
            this.wcTagRepository.save(wcTag);
            wCTagCache.updateCacheBySid(wcTag);
        }
    }

    /**
     * 以-可使用單位-模版SID查詢
     *
     * @param canUserDepTempletSid 小類(執行項目)之可使用單位模版Sid
     * @return
     */
    public List<WCTag> findByCanUserDepTempletSid(Long canUserDepTempletSid) {
        // 查詢
        List<WCTag> results = this.wcTagRepository.findByCanUserDepTempletSid(canUserDepTempletSid);
        if (results == null) {
            return Lists.newArrayList();
        }
        return results;
    }

    /**
     * 以-預設可閱部門-模版SID查詢
     *
     * @param useDepTempletSid 小類(執行項目)之預設可閱部門模版Sid
     * @return
     */
    public List<WCTag> findByUseDepTempletSid(Long useDepTempletSid) {
        // 查詢
        List<WCTag> results = this.wcTagRepository.findByUseDepTempletSid(useDepTempletSid);
        if (results == null) {
            return Lists.newArrayList();
        }
        return results;
    }

    public List<String> findDistinctTagTypeByMenuTagSid(String menuTagSid) {
        if (menuTagSid == null) {
            return Lists.newArrayList();
        }

        List<String> results = this.wcTagRepository.findDistinctTagTypeByMenuTagSid(menuTagSid);
        if (results == null) {
            return Lists.newArrayList();
        }
        return results;
    }

    /**
     * 是否需要上傳檔案
     *
     * @param sids 項目 sid
     * @return 是/否
     */
    public boolean isNeedUpdateFile(List<String> sids) {
        return this.wcTagRepository.countIsNeedUpdateFile(sids) > 0;
    }

    /**
     * 以 sid 查詢
     *
     * @param sids sid list
     * @return WCTag list
     */
    public List<WCTag> findBySidIn(Collection<String> sids) {
        if(WkStringUtils.isEmpty(sids)) {
            return Lists.newArrayList();
        }
        return this.wcTagRepository.findBySidIn(sids);
    }

    /**
     * 以下列條件查詢
     * 
     * @param wcMenuTagSids 中類 sid
     * @return WCTag list
     */
    public List<WCTag> findByWcMenuTagSidIn(List<String> wcMenuTagSids) {
        if(WkStringUtils.isEmpty(wcMenuTagSids)) {
            return Lists.newArrayList();
        }
        return this.wcTagRepository.findByWcMenuTagSidIn(wcMenuTagSids);
    }
}
