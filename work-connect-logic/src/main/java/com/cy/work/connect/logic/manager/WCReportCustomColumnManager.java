/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCReportCustomColumnRepository;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 自訂欄位
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCReportCustomColumnManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8535366265282964314L;

    @Autowired
    private WCReportCustomColumnRepository wkReportCustomColumnRepository;

    /**
     * 儲存自訂欄位設定
     *
     * @param wkReportCustomColumn 自訂欄位設定物件
     * @param createUserSid        儲存者Sid
     * @return
     */
    public WCReportCustomColumn save(
        WCReportCustomColumn wkReportCustomColumn, Integer createUserSid) {
        try {
            wkReportCustomColumn.setUsersid(createUserSid);
            wkReportCustomColumn.setType("WC");
            Date date = new Date();
            wkReportCustomColumn.setUpdate_dt(date);
            WCReportCustomColumn wc = wkReportCustomColumnRepository.save(wkReportCustomColumn);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 取得自訂欄位設定 By Url 及 登入者Sid
     *
     * @param wkReportCustomColumnUrlType Url列舉
     * @param userSid                     登入者Sid
     * @return
     */
    public WCReportCustomColumn getWCReportCustomColumnByUrlAndUserSid(
        WCReportCustomColumnUrlType wkReportCustomColumnUrlType, Integer userSid) {
        try {
            List<WCReportCustomColumn> wkReportCustomColumns =
                wkReportCustomColumnRepository.getWkReportCustomColumnByUrlAndUserSid(
                    wkReportCustomColumnUrlType, userSid);
            if (wkReportCustomColumns != null && wkReportCustomColumns.size() > 0) {
                return wkReportCustomColumns.get(0);
            }
        } catch (Exception e) {
            log.warn("getWkReportCustomColumnByUrlAndUserSid Error ", e);
        }
        return null;
    }
}
