/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCMemoCategoryRepository;
import com.cy.work.connect.vo.WCMemoCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 備忘錄類別
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMemoCategoryCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -607101108712192527L;

    @Autowired
    private WCMemoCategoryRepository wcMemoCategoryRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WCMemoCategory> wcMemoCategoryMap;

    public WCMemoCategoryCache() {
        startController = Boolean.TRUE;
        wcMemoCategoryMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            this.updateCache();
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay =  ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        log.debug("建立備忘錄類別Cache");
        try {
            Map<String, WCMemoCategory> tempMemoCategoryMap = Maps.newConcurrentMap();
            wcMemoCategoryRepository
                .findAll()
                .forEach(
                    item -> {
                        if (item.getStatus().equals(Activation.ACTIVE)) {
                            tempMemoCategoryMap.put(item.getSid(), item);
                        }
                    });
            wcMemoCategoryMap = tempMemoCategoryMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立備忘錄類別Cache結束");
    }

    /**
     * 取得備忘錄類別 By 備忘錄類別Sid
     *
     * @param wc_memo_category_sid 執行單位設定Sid
     * @return
     */
    public WCMemoCategory getWCMemoCategoryBySid(String wc_memo_category_sid) {
        return wcMemoCategoryMap.get(wc_memo_category_sid);
    }

    public List<WCMemoCategory> getAllWCMemoCategory() {
        try {
            return Lists.newArrayList(wcMemoCategoryMap.values());
        } catch (Exception e) {
            log.warn("getAllWCMemoCategory ERROR", e);
        }
        return Lists.newArrayList();
    }
}
