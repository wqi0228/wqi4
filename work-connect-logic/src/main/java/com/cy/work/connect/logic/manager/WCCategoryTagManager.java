/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.helper.MenuTagHelper;
import com.cy.work.connect.logic.helper.RelationDepHelper;
import com.cy.work.connect.logic.vo.CategoryTagVO;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.repository.WCCategoryTagRepository;
import com.cy.work.connect.repository.WCConfigTempletRepository;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.WCMenuTag;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 類別大項 Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCCategoryTagManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1444442306054489590L;

    @Autowired
    private WCCategoryTagRepository wcCategoryTagRepository;
    @Autowired
    private WCConfigTempletRepository configTempletRepository;
    @Autowired
    private WCCategoryTagCache wcCategoryTagCache;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private MenuTagHelper menuTagHelper;

    /**
     * 刪除大類(類別)
     *
     * @param sid 大類(類別)Sid
     */
    public void deleteCategoryTag(String sid) {
        this.wcCategoryTagRepository.delete(sid);
        log.info("刪除類別大項:" + sid);
    }

    /**
     * 取得公司Sid By 大類(類別)為啟用狀態
     *
     * @return
     */
    public List<Integer> findDistinctCompSidByActiveStatus() {
        return wcCategoryTagRepository.findDistinctCompSidByActiveStatus();
    }

    /**
     * 依公司及類別名稱尋找大類(類別)
     *
     * @param companySid   公司Sid
     * @param categoryName 大類(類別)名稱
     * @param menuName     中類(單據名稱)名稱
     * @param tagName      小類(執行項目)名稱
     * @return
     */
    public List<WCCategoryTag> findByTagNameAndCompanySid(
            Integer companySid, String categoryName, String menuName, String tagName) {
        return wcCategoryTagRepository.findByTagNameAndCompanySid(
                companySid, categoryName, menuName, tagName);
    }

    /**
     * 建立類別大項
     *
     * @param wcCategoryTag 大類(類別)物件
     * @param loginUserSid  登入者Sid
     * @return
     */
    public WCCategoryTag createWCCategoryTag(WCCategoryTag wcCategoryTag, Integer loginUserSid) {
        wcCategoryTag.setCreateDate(new Date());
        wcCategoryTag.setCreateUser(loginUserSid);
        // wcCategoryTag.setSeq(Integer.MAX_VALUE);
        wcCategoryTag.setSeq(
                wcCategoryTagCache.getWCCategoryTags().stream()
                        .reduce((a, b) -> b)
                        .map(WCCategoryTag::getSeq)
                        .orElseGet(() -> 0)
                        + 1);
        wcCategoryTag = wcCategoryTagRepository.save(wcCategoryTag);
        wcCategoryTagCache.updateCacheByWCCategoryTag(wcCategoryTag);
        return wcCategoryTag;
    }

    /**
     * 更新類別大項
     *
     * @param wcCategoryTag 大類(類別)物件
     * @param loginUserSid  登入者Sid
     * @return
     */
    public WCCategoryTag updateWCCategoryTag(WCCategoryTag wcCategoryTag, Integer loginUserSid) {
        wcCategoryTag.setUpdateDate(new Date());
        wcCategoryTag.setUpdateUser(loginUserSid);
        wcCategoryTag = wcCategoryTagRepository.save(wcCategoryTag);
        wcCategoryTagCache.updateCacheByWCCategoryTag(wcCategoryTag);
        return wcCategoryTag;
    }

    /**
     * 取得類別樹-所需大類中類(類別管理清單)
     *
     * @param loginUserSid 登入者Sid
     * @param loginCompSid 登入公司Sid
     * @return
     */
    public List<CategoryTagVO> getManagerCategoryTags(Integer loginUserSid, Integer loginCompSid) {
        List<CategoryTagVO> result = Lists.newArrayList();
        try {
            this.getWCCategoryTags(Activation.ACTIVE, loginCompSid)
                    .forEach(
                            item -> {
                                List<MenuTagVO> menuTags = Lists.newArrayList();
                                wcMenuTagManager
                                        .getWCMenuTagByCategorySid(item.getSid(), Activation.ACTIVE)
                                        .forEach(
                                                subItem -> {
                                                    if (menuTagHelper.checkCanShowManagerMenuItem(subItem,
                                                            loginUserSid)) {
                                                        String memo = subItem.getMemo();
                                                        if (!Strings.isNullOrEmpty(memo)) {
                                                            memo = "(" + memo + ")";
                                                        }
                                                        menuTags.add(
                                                                new MenuTagVO(
                                                                        subItem.getSid(),
                                                                        subItem.getMenuName(),
                                                                        subItem.getItemName(),
                                                                        memo));
                                                    }
                                                });
                                if (!menuTags.isEmpty()) {
                                    result.add(
                                            new CategoryTagVO(item.getSid(), item.getCategoryName(), menuTags));
                                }
                            });
        } catch (Exception e) {
            log.warn("getCategoryTags ERROR", e);
        }
        return result;
    }

    /**
     * 取得類別樹-所需大類中類
     *
     * @param loginCompSid 登入公司Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<CategoryTagVO> getCategoryTags(
            Integer loginCompSid, Integer loginUserSid) {

        // 取得使用者對應單位，for 聯絡單單據類別可使用權限
        Set<Integer> loginUserMatchDepSid = RelationDepHelper.findMatchOrgSidsForWcForm(loginUserSid);

        // 取得全公司的非停用單位
        List<Org> allCompActiveDeps = WkOrgCache.getInstance().findAllActiveDepByCompSid(loginCompSid);

        // 可顯示的大類
        List<CategoryTagVO> canUseCategoryTags = Lists.newArrayList();
        try {
            // ====================================
            // 由快取中取得所有『可顯示 - ACTIVE』的大類
            // ====================================
            List<WCCategoryTag> categoryTags = this.getWCCategoryTags(
                    Activation.ACTIVE,
                    loginCompSid);

            // ====================================
            // 取得所有部門設定模版
            // ====================================
            List<WCConfigTemplet> wcConfigTemplets = this.configTempletRepository.findAll();

            // ====================================
            // 取得要顯示的中類
            // ====================================
            for (WCCategoryTag wcCategoryTag : categoryTags) {

                // 依據大類, 由快取中, 取得所有『可顯示 - ACTIVE』的 中類
                List<WCMenuTag> menuTags = this.wcMenuTagManager.getWCMenuTagByCategorySid(
                        wcCategoryTag.getSid(), Activation.ACTIVE);

                // 可使用的中類
                List<MenuTagVO> canUseMenuTags = Lists.newArrayList();

                // 逐筆檢查中類
                for (WCMenuTag wCMenuTag : menuTags) {
                    // 檢查該中類是否可使用
                    if (menuTagHelper.checkCanShowMenuItem(
                            wCMenuTag,
                            wcConfigTemplets,
                            loginUserMatchDepSid,
                            loginUserSid,
                            loginCompSid,
                            allCompActiveDeps)) {

                        // 加入可用列表
                        canUseMenuTags.add(
                                new MenuTagVO(
                                        wCMenuTag.getSid(),
                                        wCMenuTag.getMenuName(),
                                        wCMenuTag.getItemName(),
                                        wCMenuTag.isAttachmentType(),
                                        wCMenuTag.getAttachmentPath()));
                    }
                }

                // 當該有可以顯示的中類時, 將該大類加入可顯示清單
                if (!canUseMenuTags.isEmpty()) {
                    canUseCategoryTags.add(
                            new CategoryTagVO(
                                    wcCategoryTag.getSid(), wcCategoryTag.getCategoryName(),
                                    canUseMenuTags));
                }
            }

        } catch (Exception e) {
            log.warn("getCategoryTags ERROR", e);
        }
        return canUseCategoryTags;
    }

    /**
     * 取得大類(類別)BySid
     *
     * @param sid 大類(類別)Sid
     * @return
     */
    public WCCategoryTag getWCCategoryTagBySid(String sid) {
        return wcCategoryTagCache.getWCCategoryTagBySid(sid);
    }

    /**
     * 取得大類(類別)List By Sid list
     *
     * @param sids 大類(類別)Sid清單
     * @return
     */
    public List<WCCategoryTag> getWCCategoryTagsBySidList(List<String> sids) {
        List<WCCategoryTag> result = Lists.newArrayList();
        result = wcCategoryTagCache.getWCCategoryTags().stream()
                .filter(each -> sids.contains(each.getSid()))
                .collect(Collectors.toList());
        return result;
    }

    /**
     * 取得尚未失效大類(類別)
     *
     * @param status       失效狀態
     * @param loginCompSid 登入公司Sid
     * @return
     */
    public List<WCCategoryTag> getWCCategoryTags(Activation status, Integer loginCompSid) {
        List<WCCategoryTag> result = Lists.newArrayList();
        wcCategoryTagCache
                .getWCCategoryTags()
                .forEach(
                        item -> {
                            if (item.getCompSid().equals(loginCompSid)) {
                                if (status != null && !status.equals(item.getStatus())) {
                                    return;
                                }
                                result.add(item);
                            }
                        });
        return result;
    }

    /**
     * 依據傳入 List 順序, 更新排序序號
     *
     * @param sids 大類(類別)Sid清單
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void updateSortSeqByListIndex(List<String> sids) {

        // 傳入列表為空
        if (WkStringUtils.isEmpty(sids)) {
            return;
        }

        Integer index = 1;
        for (String sid : sids) {

            // 取資料檔
            WCCategoryTag wcCategoryTag = wcCategoryTagRepository.findOne(sid);

            if (wcCategoryTag == null) {
                continue;
            }

            wcCategoryTag.setSeq(index++);

            // 更新
            this.wcCategoryTagRepository.save(wcCategoryTag);
            wcCategoryTagCache.updateCacheByWCCategoryTag(wcCategoryTag);
        }
    }

    /**
     * 以下列條件查詢
     * 
     * @param compSid 公司SID
     * @return WCCategoryTag list
     */
    public List<WCCategoryTag> findByCompSid(Integer compSid) {
        return this.wcCategoryTagRepository.findByCompSid(compSid);
    }
}
