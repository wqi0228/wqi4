/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.repository.WCExecRollBackHistoryRepository;
import com.cy.work.connect.vo.WCExecRollBackHistory;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 退回歷程
 *
 * @author brain0925_liao
 */
@Component
public class WCExecRollBackHistoryManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3985658734161778446L;

    @Autowired
    private WCExecRollBackHistoryRepository wcExecRollBackHistoryRepository;

    public void create(String wcSid, Integer rollBackUserSid, List<Integer> rollbackDepSids) {
        WCExecRollBackHistory wbh = new WCExecRollBackHistory();
        wbh.setRollBackDate(new Date());
        wbh.setRollBackSid(rollBackUserSid);
        wbh.setRollBackDep(transToUserDep(rollbackDepSids));
        wbh.setWcSid(wcSid);
        wcExecRollBackHistoryRepository.save(wbh);
    }

    private UserDep transToUserDep(List<Integer> rollbackDepSids) {
        UserDep ud = new UserDep();
        if(WkStringUtils.notEmpty(rollbackDepSids)) {
            for (Integer depSid : rollbackDepSids) {
                UserDepTo udt = new UserDepTo();
                udt.setDepSid(String.valueOf(depSid));
                ud.getUserDepTos().add(udt);
            }
        }
        return ud;
    }
}
