package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCMenuTagCache;
import com.cy.work.connect.logic.vo.WCTagTempletVo;
import com.cy.work.connect.repository.WCTagTempletRepository;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTagTemplet;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author jimmy_chou
 */
@Slf4j
@Component
public class WCTagTempletManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4564020588835234122L;

    private static WCTagTempletManager instance;
    @Autowired
    private WCTagTempletRepository tagTempletRepository;
    @Autowired
    private WCMenuTagCache wcMenuTagCache;
    @Autowired
    private WCCategoryTagCache wcCategoryTagCache;
    @Autowired
    private EntityManager entityManager;

    public static WCTagTempletManager getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCTagTempletManager.instance = this;
    }

    /**
     * 以SID ˊ查詢
     *
     * @param sid
     * @return
     */
    public WCTagTempletVo findBySid(Long sid) {

        // ====================================
        // 查詢
        // ====================================
        if (sid == null) {
            // log.warn("傳入 sid 為 null");
            return null;
        }

        WCTagTemplet tagTemplet = this.tagTempletRepository.findOne(sid);
        if (tagTemplet == null) {
            log.warn("WCTagTemplet 找不到指定的資料檔");
            return null;
        }

        // ====================================
        // 轉VO 並回傳
        // ====================================
        return new WCTagTempletVo(tagTemplet);
    }

    /**
     * 以類別Sid 取得大類名稱
     *
     * @param sid
     * @return
     */
    public String findCategoryNameByWCCategoryTagSid(String sid) {
        return Optional.ofNullable(wcCategoryTagCache.getWCCategoryTagBySid(sid))
                .map(WCCategoryTag::getCategoryName)
                .orElseGet(() -> null);
    }

    /**
     * 以單據名稱Sid 取得單據名稱
     *
     * @param sid
     * @return
     */
    public String findMenuNameByWCMenuTagSid(String sid) {
        return Optional.ofNullable(wcMenuTagCache.getWCMenuTagBySid(sid))
                .map(WCMenuTag::getMenuName)
                .orElseGet(() -> null);
    }

    /**
     * 以單據名稱Sid 取得大類名稱 及 單據名稱
     *
     * @param sid
     * @return
     */
    public String[] findHierarchyTagNameByWCMenuTagSid(String sid) {
        if (sid == null) {
            return null;
        }

        String[] result = new String[2];

        // 取得中類
        WCMenuTag menuTag = this.wcMenuTagCache.getWCMenuTagBySid(sid);

        // 取得大類
        WCCategoryTag categoryTag = null;
        if (menuTag != null) {
            categoryTag = this.wcCategoryTagCache.getWCCategoryTagBySid(
                    menuTag.getWcCategoryTagSid());
        }

        // 組訊息文字 - 大類
        if (categoryTag != null) {
            result[0] = categoryTag.getCategoryName();
        } else {
            result[0] = "[找不到大類]";
        }

        // 組訊息文字 - 中類
        if (menuTag != null) {
            result[1] = menuTag.getMenuName();
        } else {
            result[1] = "[找不到中類]";
        }

        return result;
    }

    public WCMenuTag findMenuTagByWCMenuTagSid(String sid) {
        return this.wcMenuTagCache.getWCMenuTagBySid(sid);
    }

    public WCCategoryTag findCategoryTagByWCCategoryTagSid(String sid) {
        return this.wcCategoryTagCache.getWCCategoryTagBySid(sid);
    }

    /**
     * 以公司sid查詢
     *
     * @param compSid
     * @return
     */
    public List<WCTagTempletVo> findByCompSid(Integer compSid) {
        List<WCTagTempletVo> results = Lists.newArrayList();

        if (compSid != null) {
            results = this.tagTempletRepository.findByCompSid(compSid).stream()
                    .map(each -> new WCTagTempletVo(each))
                    .collect(Collectors.toList());
        }

        // ====================================
        // 排序
        // ====================================
        if (results != null && !results.isEmpty()) {
            // 1.類別
            Comparator<WCTagTempletVo> comparator = Comparator.comparing(
                    WCTagTempletVo::getCategoryName);
            // 2.單據名稱
            comparator = comparator.thenComparing(
                    Comparator.comparing(WCTagTempletVo::getMenuTagSid));
            // 3.排序序號
            comparator = comparator.thenComparing(Comparator.comparing(WCTagTempletVo::getSortSeq));

            results = results.stream().sorted(comparator).collect(Collectors.toList());
        }

        return results;
    }

    /**
     * 依據傳入 List 順序, 更新排序序號
     *
     * @param templetList
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void updateSortSeqByListIndex(List<WCTagTempletVo> templetList) {

        // 傳入列表為空
        if (WkStringUtils.isEmpty(templetList)) {
            return;
        }

        // 以模版類別, 紀錄 index 序號
        Map<String, Integer> indexMap = Maps.newConcurrentMap();

        for (WCTagTempletVo templetVo : templetList) {

            // 取得模版資料檔
            WCTagTemplet tagTemplet = this.tagTempletRepository.findOne(templetVo.getSid());
            if (tagTemplet == null) {
                continue;
            }

            // 依據單據名稱累加序號
            String menuTagSid = tagTemplet.getMenuTagSid();
            Integer index = indexMap.get(menuTagSid);
            if (index == null) {
                index = 1;
                indexMap.put(menuTagSid, index);
            }
            tagTemplet.setSortSeq(index);
            indexMap.put(menuTagSid, index + 1);

            // 更新
            this.tagTempletRepository.save(tagTemplet);
        }
    }

    /**
     * @param templateVO
     * @param loginUserSid
     * @param loginUserCompSid
     * @throws Exception
     */
    public void saveSetting(WCTagTempletVo templateVO, Integer loginUserSid,
            Integer loginUserCompSid)
            throws Exception {

        Date sysDate = new Date();

        // ====================================
        // 準備主檔 DB entities
        // ====================================
        WCTagTemplet tagTemplate;

        if (WkStringUtils.isEmpty(templateVO.getSid())) {
            // 新增前檢核是否已有重覆《使用名稱》
            if (this.tagTempletRepository.isExistUsenameByUsenameAndMenuTagSidAndCompSid(
                    templateVO.getUsename(), templateVO.getMenuNameVO().getSid(), loginUserCompSid)) {
                Preconditions.checkState(false, "使用名稱有重覆，請重新輸入！！");
            }

            // 為新增
            tagTemplate = new WCTagTemplet();
            // 公司別 （避免不同的公司混用）
            tagTemplate.setCompSid(loginUserCompSid);
            // 建立者
            tagTemplate.setCreateUser(loginUserSid);
            // 建立時間
            tagTemplate.setCreateDate(sysDate);
            // 狀態
            tagTemplate.setStatus(Activation.ACTIVE);
            // 排列序號 => 初始不給, 由DB給 default (最大值, 讓他排在最後面)
            // configTemplet.setSortSeq(Integer.MAX_VALUE);

        } else {
            // 查詢
            tagTemplate = this.tagTempletRepository.findOne(templateVO.getSid());
            // 檢核
            if (tagTemplate == null) {
                throw new Exception("編輯項目已不存在, 請重新查詢");
            }
        }

        // 更新日期
        tagTemplate.setUpdateDate(templateVO.getUpdateDate());
        // 更新者
        tagTemplate.setUpdateUser(templateVO.getUpdateUser());
        // 單據名稱
        tagTemplate.setMenuTagSid(templateVO.getMenuNameVO().getSid());
        // 使用名稱
        tagTemplate.setUsename(templateVO.getUsename());
        // 設定值
        tagTemplate.setDefaultValue(templateVO.getDefaultValue());
        // 備註
        tagTemplate.setMemo(templateVO.getMemo());

        // ====================================
        // save wc_config_templet
        // ====================================
        this.tagTempletRepository.save(tagTemplate);
    }

    /**
     * 刪除模版
     *
     * @param sid
     */
    public void deleteTemplet(Long sid) {
        this.tagTempletRepository.delete(sid);
        log.info("刪除模版:" + sid);
    }

    /**
     * 使用名稱清單 - 依公司Sid查詢
     *
     * @param compSid
     * @return
     */
    public List<SelectItem> findDistinctUseNameByCompSid(Integer compSid) {
        if (compSid == null) {
            return Lists.newArrayList();
        }

        List<SelectItem> results = this.tagTempletRepository.findDistinctUseNameByCompSid(compSid).stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        if (results == null) {
            return Lists.newArrayList();
        }
        return results;
    }

    /**
     * 建議勾選項目清單 - 依單據名稱Sid查詢 (僅查詢出有建議勾選項目設定)
     *
     * @param menuTagSid
     * @param compSid
     * @return
     */
    public List<SelectItem> findTagTempletByMenuTagSidAndCompSid(String menuTagSid,
            Integer compSid) {
        if (menuTagSid == null || compSid == null) {
            return Lists.newArrayList();
        }

        List<SelectItem> results = this.tagTempletRepository
                .findByMenuTagSidAndCompSidOrderBySortSeq(menuTagSid, compSid)
                .stream()
                .filter(each -> each.getDefaultValue() != null
                        && !each.getDefaultValue().getTagSids().isEmpty())
                .map(each -> new SelectItem(each.getSid(), each.getUsename()))
                .collect(Collectors.toList());
        if (results == null) {
            return Lists.newArrayList();
        }
        return results;
    }

    /**
     * 小類停用的時候，刪除json中的停用tag資料
     *
     * @param tagSid
     */
    public void removeTagTemplateTagJsonValue(String tagSid) {
        String sql = "" +
                "SELECT  * " +
                "FROM wc_tag_templet " +
                "WHERE JSON_CONTAINS(JSON_EXTRACT(default_value, '$.tagSids'),JSON_ARRAY('" + tagSid + "'))";

        Query query = entityManager.createNativeQuery(sql, WCTagTemplet.class);
        query.getResultList();
        @SuppressWarnings("unchecked")
        List<WCTagTemplet> tagTemplates = query.getResultList();
        for (WCTagTemplet tagTemplate : tagTemplates) {
            if (tagTemplate.getDefaultValue() == null) {
                continue;
            }
            if (tagTemplate.getDefaultValue().getTagSids().contains(tagSid)) {
                tagTemplate.getDefaultValue().getTagSids().remove(tagSid);
            }
        }
        this.tagTempletRepository.save(tagTemplates);
    }

    /**
     * 查詢使用傳入sid執行項目的模板
     *
     * @param tagSid
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<WCTagTemplet> findTemplateByExecTagSid(String tagSid) {
        String sql = "" +
                "SELECT  * " +
                "FROM wc_tag_templet " +
                "WHERE JSON_CONTAINS(JSON_EXTRACT(default_value, '$.tagSids'),JSON_ARRAY('" + tagSid + "'))";

        Query query = entityManager.createNativeQuery(sql, WCTagTemplet.class);
        return query.getResultList();
    }
}
