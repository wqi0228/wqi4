/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo.view;

import com.cy.work.connect.logic.vo.WorkFunItemVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class WorkFunItemGroupVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2956957829023049675L;

    @Getter
    private final String groupSid;
    @Getter
    private final String name;
    @Getter
    private final Integer seq;
    @Getter
    private final String groupBaseSid;
    @Getter
    private final List<WorkFunItemVO> workFunItemVOs;

    public WorkFunItemGroupVO(
        String groupSid,
        String name,
        Integer seq,
        String groupBaseSid,
        List<WorkFunItemVO> workFunItemVOs) {
        this.groupSid = groupSid;
        this.name = name;
        this.seq = seq;
        this.groupBaseSid = groupBaseSid;
        this.workFunItemVOs = workFunItemVOs;
    }
}
