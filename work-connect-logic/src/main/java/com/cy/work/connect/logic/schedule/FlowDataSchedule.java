/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.schedule;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.work.connect.logic.cache.WCExecManagerSignInfoCache;
import com.cy.work.connect.logic.cache.WCManagerSignInfoCache;
import com.cy.work.connect.logic.cache.WCOpinionDescripeSettingCache;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMasterCustomLogicManager;
import com.cy.work.connect.repository.WCManagerSignInfoRepository;
import com.cy.work.connect.repository.WCMasterRepository;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class FlowDataSchedule implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8072745730859306280L;

    @Autowired
    private WCMasterRepository wcMasterRepository;
    @Autowired
    private WCManagerSignInfoRepository wcManagerSignInfoRepository;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;
    @Autowired
    private WCManagerSignInfoCache wcManagerSignInfoCache;
    @Autowired
    private WCExecManagerSignInfoCache wcExecManagerSignInfoCache;
    @Autowired
    private WCOpinionDescripeSettingCache wcOpinionDescripeSettingCache;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;
    //      @Value("${work-connect.WERP_PROGRAM.signUserID}")
    //    private String WERP_PROGRAM_signUserID;
    //    @Value("${work-connect.WERP_PROGRAM.execDepID}")
    //    private String WERP_PROGRAM_execDepID;

    public void checkAllData(Integer loginUserSid) {
        wcMasterRepository
            .findAll()
            .forEach(
                item -> {
                    try {
                        checkOneData(item, loginUserSid);
                    } catch (IllegalStateException | IllegalArgumentException e) {
                        log.warn("checkOneData ERROR：{}", e.getMessage());
                    } catch (Exception e) {
                        log.warn("checkOneData ERROR", e);
                    }
                });
    }

    public void updateFlowCache(WCMaster wc) {
        log.debug("更新BPM Cache資料單號:[" + wc.getWc_no() + "] 開始(待表重單據簽核開啟)");
        wcManagerSignInfoCache.updateCacheByWcSid(wc.getSid());
        wcExecManagerSignInfoCache.updateCacheByWcSid(wc.getSid());
        wcOpinionDescripeSettingCache.updateCache(wc.getSid());
        log.debug("更新BPM Cache資料單號:[" + wc.getWc_no() + "] 結束(待表重單據簽核開啟)");
    }

    public void checkOneData(WCMaster wc, Integer loginUserSid) throws ProcessRestException {
        if ((WCStatus.CLOSE.equals(wc.getWc_status())
            || WCStatus.CLOSE_STOP.equals(wc.getWc_status())
            || WCStatus.INVALID.equals(wc.getWc_status()))) {
            log.debug("檢測資料單號:[" + wc.getWc_no() + "]--已為作廢或結案,不進行檢測");
            return;
        }
        // 優先檢測單據是否有需求單位流程
        List<WCManagerSignInfo> wcManagerSignInfos =
            wcManagerSignInfoRepository.findOneByWCSid(wc.getSid());
        List<WCManagerSignInfo> signFlow =
            wcManagerSignInfos.stream()
                .filter(each -> SignType.SIGN.equals(each.getSignType()))
                .collect(Collectors.toList());
        log.debug("檢測資料單號:[" + wc.getWc_no() + "]--確認需求流程有無");
        if (signFlow == null || signFlow.isEmpty()) {
            try {
                log.warn("問題資料單號:[" + wc.getWc_no() + "]無需求流程,需建立,開始進行建立");
                String bpmID = wcManagerSignInfoManager.createBpmFlow(wc, wc.getCreate_usr());
                WCManagerSignInfo signInfo = wcManagerSignInfoManager.createSignInfo(wc, bpmID);
                Preconditions.checkState(signInfo != null, "建立需求方簽核資訊失敗！！");
                log.warn("問題資料單號:[" + wc.getWc_no() + "]無需求流程,需建立,建立完成");
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("建立需求流程失敗：{}", e.getMessage());
                return;
            } catch (Exception e) {
                log.warn("建立需求流程失敗", e);
                return;
            }
        } else {
            log.debug("檢測資料單號:[" + wc.getWc_no() + "]--確認需求流程有");
        }
        log.debug("檢測資料單號:[" + wc.getWc_no() + "]--確認需求流程簽核進度");
        List<WCManagerSignInfo> all = wcManagerSignInfoRepository.findOneByWCSid(wc.getSid());
        boolean reqSignFinish = true;
        for (WCManagerSignInfo item : all) {
            if (SignType.SIGN.equals(item.getSignType())) {
                // 若是預設的需求單位流程,若為作廢代表流程未完成,
                if (!(BpmStatus.APPROVED.equals(item.getStatus())
                    || BpmStatus.CLOSED.equals(item.getStatus()))) {
                    reqSignFinish = false;
                }
            } else if (SignType.COUNTERSIGN.equals(item.getSignType())) {
                // 若是加簽的需求人員,若為作廢代表流程被刪除,故作廢代表流程已完成
                if (!(BpmStatus.APPROVED.equals(item.getStatus())
                    || BpmStatus.DELETE.equals(item.getStatus())
                    || BpmStatus.CLOSED.equals(item.getStatus()))) {
                    reqSignFinish = false;
                }
            }
        }
        if (!reqSignFinish) {
            log.debug("檢測資料單號:[" + wc.getWc_no() + "]--確認需求流程簽核進度未完成");
            return;
        }
        log.debug("檢測資料單號:[" + wc.getWc_no() + "]--確認需求流程簽核進度全部完成,包含加簽");
        if (wc.getWc_status().equals(WCStatus.NEW_INSTANCE)
            || wc.getWc_status().equals(WCStatus.APPROVING)
            || wc.getWc_status().equals(WCStatus.WAITAPPROVE)
            || wc.getWc_status().equals(WCStatus.RECONSIDERATION)) {
            log.warn(
                "問題資料單號:[" + wc.getWc_no() + "]需求流程已完成,但狀態依舊為[" + wc.getWc_status().getVal() + "]");
            log.warn("進行資料修正,需求流程皆已完成,需修改成核准--START");
            // boolean isFromReqSign = true;
            wcMasterCustomLogicManager.updateToApproved(wc.getSid(), loginUserSid, reqSignFinish);
            log.warn("進行資料修正,需求流程皆已完成,需修改成核准--END");
        }
    }
}
