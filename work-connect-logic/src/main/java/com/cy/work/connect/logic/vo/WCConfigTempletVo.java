package com.cy.work.connect.logic.vo;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.converter.to.ConfigValueTo;
import java.io.Serializable;
import lombok.Getter;
import org.springframework.beans.BeanUtils;

/**
 * @author allen
 */
public class WCConfigTempletVo extends WCConfigTemplet implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8289972001794372324L;
    /**
     *
     */
    @Getter
    private String configValueDepsShow;

    /**
     *
     */
    public WCConfigTempletVo() {
        this.setConfigValue(new ConfigValueTo());
    }

    /**
     * 建構子
     *
     * @param configTemplet
     */
    public WCConfigTempletVo(WCConfigTemplet configTemplet) {
        // 複製所有欄位屬性
        BeanUtils.copyProperties(configTemplet, this);

        // 初始化設定值物件
        if (this.getConfigValue() == null) {
            this.setConfigValue(new ConfigValueTo());
        }

        // 組部門顯示資訊
        this.configValueDepsShow =
            WorkSettingOrgManager.getInstance()
                .prepareDepsShowText(
                    this.getConfigValue().getDeps(),
                    this.getConfigValue().isDepContainFollowing(),
                    this.getConfigValue().isDepExcept());

        if (WkStringUtils.isEmpty(this.configValueDepsShow)) {
            // if (!ConfigTempletType.EXEC_TRANS_DEP.equals(this.getTempletType())) {
            this.configValueDepsShow = "<span class='WS1-1-5'>全單位</span>";
            // }
        }
    }

    /**
     * 顯示用：狀態
     *
     * @return
     */
    public String getStatusShow() {
        return this.isActive() ? "" : "不顯示";
    }

    /**
     * 顯示用：模版類別
     *
     * @return
     */
    public String getTempletTypeShow() {
        if (this.getTempletType() == null) {
            return "";
        }
        return this.getTempletType().getDescr();
    }

    /**
     * 顯示用：模版類別
     *
     * @return
     */
    public String getTempletNameShow() {
        return WCConfigTempletManager.getInstance()
            .getPettyTempletName(this.getTempletName(), this.getConfigValue(), false, false);
    }

    /**
     * 顯示用：模版類別
     *
     * @return
     */
    public String getTempletNameShowColorful() {
        return WCConfigTempletManager.getInstance()
            .getPettyTempletName(this.getTempletName(), this.getConfigValue(), true, false);
    }
}
