/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.query.WCMasterQueryService;
import com.cy.work.connect.repository.WCMasterRepository;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.converter.WCStatusConverter;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 工作聯絡單
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMasterManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 9062071661659480086L;

    private static WCMasterManager instance;

    public static WCMasterManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMasterManager.instance = this;
    }

    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private WCMasterRepository wcMasterRepository;

    @Autowired
    @Qualifier("wcMasterQueryService")
    private WCMasterQueryService wcMasterQueryService;

    @Autowired
    private WkOrgCache wkOrgCache;

    private WCStatusConverter wcStatusConverter = new WCStatusConverter();

    /**
     * 依大類(類別)Sid尋找工作聯絡單已使用次數
     *
     * @param categoryTag
     * @return
     */
    public Integer findByCategoryTag(String categoryTag) {
        return wcMasterRepository.findByCategoryTag(categoryTag);
    }

    /**
     * 依中類(單據名稱)Sid尋找工作聯絡單已使用次數
     *
     * @param menuTagSid
     * @return
     */
    public Integer findByMenuTag(String menuTagSid) {
        return wcMasterRepository.findByMenuTagSid(menuTagSid);
    }

    /**
     * 依小類(執行項目)Sid尋找工作聯絡單已使用次數
     *
     * @param tagSid
     * @return
     */
    public Integer findByTag(String tagSid) {
        return wcMasterRepository.findByTag("\"" + tagSid + "\"");
    }

    /**
     * 取得工作聯絡單 By Sid
     *
     * @param sid 工作聯絡單Sid
     * @return
     */
    public WCMaster findBySid(String sid) {
        return wcMasterRepository.findBySid(sid);
    }

    /**
     * 取得單據狀態
     *
     * @param wcSid
     * @return
     */
    public WCStatus findWcStatusByWcSid(String wcSid) {
        String sql = "SELECT wc_status FROM wc_master WHERE wc_sid = ? ;";
        String wcStatusStr = this.jdbcTemplate.queryForObject(sql, String.class, wcSid);
        return wcStatusConverter.convertToEntityAttribute(wcStatusStr);
    }

    /**
     * 取得工作聯絡單 By 單號
     *
     * @param no 工作聯絡單單號
     * @return
     */
    public List<WCMaster> findByNo(String no) {
        return wcMasterRepository.findByNo(no);
    }

    /**
     * 取得工作聯絡單 By 建單者
     *
     * @param createUserSid 建單者
     * @return
     */
    public List<WCMaster> findByCreateUserSid(Integer createUserSid) {
        return wcMasterRepository.findByCreateUserSid(createUserSid);
    }

    /**
     * 建立工作聯絡單
     *
     * @param wcMaster      工作聯絡單物件
     * @param createUserSid 建單者Sid
     * @return
     */
    public WCMaster create(WCMaster wcMaster, Integer createUserSid) {
        try {
            wcMaster.setCreate_usr(createUserSid);
            Date createDate = new Date();
            if (wcMaster.getWc_status().equals(WCStatus.NEW_INSTANCE)) {
                wcMaster.setCreate_dt(createDate);
                wcMaster.setUpdate_dt(createDate);
            }
            WCMaster wc = wcMasterRepository.save(wcMaster);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 更新工作聯絡單 (不更新修改時間及修改者)
     *
     * @param wcMaster 工作聯絡單物件
     * @return
     */
    public WCMaster updateOnlyWCMaster(WCMaster wcMaster) {
        try {
            WCMaster wc = wcMasterRepository.save(wcMaster);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 更新工作聯絡單(更新修改時間及修改者)
     *
     * @param wcMaster      工作聯絡單物件
     * @param updateUserSid 更新者Sid
     * @return
     */
    public WCMaster update(WCMaster wcMaster, Integer updateUserSid) {
        try {
            wcMaster.setUpdate_usr(updateUserSid);
            wcMaster.setUpdate_dt(new Date());
            WCMaster wc = wcMasterRepository.save(wcMaster);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 取得最後一筆工作聯絡單單號 By 工作聯絡單單據狀態及 建立時間區間
     *
     * @param start      建立起始時間
     * @param end        建立結束時間
     * @param wcStatus   工作聯絡單單據狀態
     * @param companySid 建單者公司Sid
     * @return
     */
    public String findTodayLastWCMaster(Date start, Date end, WCStatus wcStatus,
                                        Integer companySid) {
        List<String> nos = null;
        if (wcStatus.equals(WCStatus.NEW_INSTANCE)) {
            nos = wcMasterRepository.findTodayLastWCMaster(start, end, companySid);
        }
        if (nos != null && nos.size() > 0) {
            return nos.get(0);
        }
        return "";
    }

    /**
     * 取得建立筆數 By 工作聯絡單單據狀態及 建立時間區間
     *
     * @param start    建立起始時間
     * @param end      建立結束時間
     * @param wcStatus 工作聯絡單單據狀態
     * @return
     */
    public Integer findTodayWCMasterTotalSize(
            Date start, Date end, WCStatus wcStatus, Integer companySid) {
        try {
            if (wcStatus.equals(WCStatus.NEW_INSTANCE)) {
                return wcMasterRepository.findTodayWCMasterTotalSize(start, end, companySid);
            }
        } catch (Exception e) {
            log.warn("findTodayWCMasterTotalSize Error : " + e, e);
        }
        return 0;
    }

    /**
     * 鎖定工作聯絡單
     *
     * @param wc_sid      工作聯絡單Sid
     * @param lockDate    鎖定日期
     * @param lockUserSid 鎖定者Sid
     */
    public void updateLockUserAndLockDate(String wc_sid, Date lockDate, Integer lockUserSid) {
        wcMasterRepository.updateLockUserAndLockDate(lockDate, lockUserSid, wc_sid);
    }

    /**
     * 解除工作聯絡單鎖定
     *
     * @param wc_sid       工作聯絡單Sid
     * @param closeUserSid 鎖定者Sid
     */
    public void closeLockUserAndLockDate(String wc_sid, Integer closeUserSid) {
        wcMasterRepository.closeLockUserAndLockDate(null, null, wc_sid, closeUserSid);
    }

    /**
     * 取得工作聯絡單內設定索取讀取回條的部門List By 登入者Sid
     *
     * @param searchUserSid 登入者Sid
     * @return
     */
    public List<Org> findSendReadReceiptsOrg(Integer searchUserSid) {

        // ====================================
        // 限定查詢類別
        // ====================================
        List<String> wcStatus = Lists.newArrayList();
        wcStatus.add(WCStatus.NEW_INSTANCE.name());
        wcStatus.add(WCStatus.WAIT_EXEC.name());
        wcStatus.add(WCStatus.EXEC.name());
        wcStatus.add(WCStatus.CLOSE.name());
        wcStatus.add(WCStatus.CLOSE_STOP.name());
        wcStatus.add(WCStatus.INVALID.name());

        // ====================================
        // 準備 SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();
        String sql = this.prepareSQLByCanReadUserSid("dep_sid", parameters, searchUserSid);

        // 限定單據狀態
        sql += " AND wc.wc_status in (:wc_status) ";
        parameters.put("wc_status", wcStatus);

        sql += " GROUP BY  dep_sid ";

        // ====================================
        // 查詢
        // ====================================
        @SuppressWarnings("rawtypes")
        List results = wcMasterQueryService.findWithQuery(sql, parameters, null);

        // ====================================
        // 整理查詢結果
        // ====================================
        // 收集 Org (不重複)
        Set<Org> deps = Sets.newHashSet();
        for (Object depSid : results) {
            String depSidStr = depSid + "";
            if (!WkStringUtils.isNumber(depSidStr)) {
                continue;
            }
            Org dep = wkOrgCache.findBySid(Integer.valueOf(depSidStr));
            if (dep != null) {
                deps.add(dep);
            }
        }

        return Lists.newArrayList(deps);
    }

    /**
     * 查詢登入者的可閱單據
     *
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<String> findViewReadReceiptsWCSids(Integer loginUserSid) {

        // ====================================
        // 準備 SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();
        String sql = this.prepareSQLByCanReadUserSid("wc_sid", parameters, loginUserSid);
        sql += " GROUP BY  wc_sid ";

        // ====================================
        // 查詢
        // ====================================
        @SuppressWarnings("rawtypes")
        List results = wcMasterQueryService.findWithQuery(sql, parameters, null);

        // ====================================
        // 整理查詢結果
        // ====================================
        // 收集 wcSid (不重複)
        Set<String> wcSids = Sets.newHashSet();
        for (Object wcSid : results) {
            wcSids.add(wcSid + "");
        }

        return Lists.newArrayList(wcSids);
    }

    /**
     * 取得工作聯絡單內設定可閱名單的部門List By 登入者Sid
     *
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<Org> findViewReadReceiptsOrgs(Integer loginUserSid) {

        // ====================================
        // 準備 SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();
        String sql = this.prepareSQLByCanReadUserSid("dep_sid", parameters, loginUserSid);
        sql += " GROUP BY  dep_sid ";

        // ====================================
        // 查詢
        // ====================================
        @SuppressWarnings("rawtypes")
        List results = wcMasterQueryService.findWithQuery(sql, parameters, null);

        // ====================================
        // 整理查詢結果
        // ====================================
        // 收集 Org (不重複)
        Set<Org> deps = Sets.newHashSet();
        for (Object depSid : results) {
            String depSidStr = depSid + "";
            if (!WkStringUtils.isNumber(depSidStr)) {
                continue;
            }
            Org dep = wkOrgCache.findBySid(Integer.valueOf(depSidStr));
            if (dep != null) {
                deps.add(dep);
            }
        }

        return Lists.newArrayList(deps);
    }

    /**
     * 兜組可閱單據查詢 SQL
     *
     * @param selectField    選取的欄位
     * @param parameters     參數
     * @param canReadUserSid 可閱者Sid
     * @return
     */
    private String prepareSQLByCanReadUserSid(
            String selectField, Map<String, Object> parameters, Integer canReadUserSid) {

        // ====================================
        // 兜組查詢 SQL
        // ====================================
        StringBuffer builder = new StringBuffer();

        builder.append("SELECT wc." + selectField + " ");
        builder.append("  FROM wc_read_receipts rpt ");
        builder.append("INNER JOIN  wc_master wc ");
        builder.append("        ON  wc.wc_sid  = rpt.wc_sid ");
        builder.append("WHERE rpt.reader = :loginUserSid ");

        parameters.put("loginUserSid", canReadUserSid);

        return builder.toString();
    }

    /**
     * 變更送件時間
     * @param wcSid 工作聯絡單Sid
     * @param sendDate 送件時間
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void updateSendDate(String wcSid, Date sendDate) throws UserMessageException {

        // ====================================
        // 取得主檔
        // ====================================
        WCMaster wcMaster = this.findBySid(wcSid);
        if (wcMaster == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到單據資料)";
            log.error(errorMessage + "wcSid:[{}]", wcSid);
            throw new UserMessageException(wcSid, null);
        }

        // ====================================
        // 兜組 SQL
        // ====================================
        String sql = ""
                + "UPDATE wc_master "
                + "   SET send_dt = ? "
                + " WHERE wc_sid = ? ;";

        List<Object> args = Lists.newArrayList();
        args.add(sendDate);
        args.add(wcSid);

        // ====================================
        // update
        // ====================================
        this.jdbcTemplate.update(sql, args.toArray());

        // 清快取
        this.entityManager.refresh(wcMaster);
    }

    /**
     * 判斷並執行更新為『已送件』
     *
     * @param wcSid       主單 SID
     * @param execUserSid 執行者 SID
     * @throws UserMessageException 錯誤時拋出
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void processUpdateSended(
            String wcSid,
            Integer execUserSid) throws UserMessageException {

        // 見 WORKCOMMU-503

        // ====================================
        // 取得主檔
        // ====================================
        WCMaster wcMaster = this.findBySid(wcSid);
        if (wcMaster == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到單據資料)";
            log.error(errorMessage + "wcSid:[{}]", wcSid);
            throw new UserMessageException(wcSid, InfomationLevel.ERROR);
        }

        // ====================================
        // 判斷無需執行
        // ====================================
        // 已經更新了
        if (wcMaster.isSended()) {
            return;
        }

        // 過若狀態已經更新為 核准, 代表需求方已全部完成
        // 就算一路上所有動作都是申請人做的, 也更新為已送出
        // APPROVED 時無條件註記為 1
        if (!WCStatus.APPROVED.equals(wcMaster.getWc_status())) {

            // 執行者不是申請人時，代表其他人看過了且動作過了 => 更新為已送件
            // 比對執行者同申請人, 無需更新
            if (WkCommonUtils.compareByStr(execUserSid, wcMaster.getCreate_usr())) {
                return;
            }
        }

        // ====================================
        // 兜組 SQL
        // ====================================
        String sql = ""
                + "UPDATE wc_master "
                + "   SET is_sended = 1 "
                + " WHERE wc_sid = ? ;";

        // ====================================
        // update
        // ====================================
        this.jdbcTemplate.update(sql, wcSid);

        // 清快取
        this.entityManager.refresh(wcMaster);

        log.info("[{}] 註記已送件!", wcSid);
    }

}
