/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCExecDepSettingCache;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.helper.LogHelper;
import com.cy.work.connect.logic.helper.MenuTagHelper;
import com.cy.work.connect.repository.WCExecDepSettingRepository;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 執行單位設定檔Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCExecDepSettingManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3628068050293235849L;

    @Autowired
    private WCExecDepSettingCache wcExecDepSettingCache;
    @Autowired
    private WCExecDepSettingRepository wcExecDepSettingRepository;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private LogHelper logHelper;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private MenuTagHelper menuTagHelper;
    @Autowired
    private WCTagCache wcTagCache;

    /**
     * 以 sid 查詢 (直接撈 DB, 不透由 cache)
     * 
     * @param sid sid
     * @return WCExecDepSetting
     */
    public WCExecDepSetting findBySidWithoutCache(String sid) {
        return this.wcExecDepSettingRepository.findBySid(sid);
    }

    public void saveExecDepSetting(
            String wc_exec_dep_setting_sid, Integer loginUserSid, List<Integer> receviceUserSids) {
        WCExecDepSetting execDepSetting = null;
        execDepSetting = wcExecDepSettingRepository.findOne(wc_exec_dep_setting_sid);
        Preconditions.checkState(!execDepSetting.isNeedAssigned(), "派工不可調整領單人員名單");
        execDepSetting.setUpdate_dt(new Date());
        execDepSetting.setUpdate_usr(loginUserSid);
        execDepSetting.setReadReceviceSetting(!receviceUserSids.isEmpty());
        execDepSetting.setReceviceUser(this.transToUsersTo(receviceUserSids));
        execDepSetting = wcExecDepSettingRepository.save(execDepSetting);
        wcExecDepSettingCache.updateCacheByExecDepSetting(execDepSetting);
    }

    /**
     * 儲存執行設定檔
     *
     * @param wc_exec_dep_setting_sid    執行設定檔SID
     * @param loginUserSid               登入者Sid
     * @param execDep                    執行單位
     * @param signUserSids               簽核人員List
     * @param wc_tag_Sid                 執行項目SID
     * @param isNeedAssign               是否需指派
     * @param isReadAssignSetting        是否指派人員讀取設定檔
     * @param selectAssignOrgs           指派單位
     * @param assignUserSids             設定檔的指派人員List
     * @param managerReceviceUserSids    管理領單人員
     * @param receviceUserSids           領單人員
     * @param isOnlyReceviceUser         是否僅領單人員可閱
     * @param cyWindow                   中佑對應窗口
     * @param isTransDepContainFollowing 指派單位含以下單位
     * @param execDesc                   執行說明
     */
    public void saveExecDepSetting(
            String wc_exec_dep_setting_sid,
            Integer loginUserSid,
            Org execDep,
            List<Integer> signUserSids,
            String wc_tag_Sid,
            boolean isNeedAssign,
            boolean isReadAssignSetting,
            List<String> selectAssignOrgs,
            List<Integer> assignUserSids,
            List<Integer> managerReceviceUserSids,
            List<Integer> receviceUserSids,
            boolean isOnlyReceviceUser,
            String cyWindow,
            boolean isTransDepContainFollowing,
            String execDesc) {
        WCExecDepSetting execDepSetting = null;
        if (Strings.isNullOrEmpty(wc_exec_dep_setting_sid)) {
            execDepSetting = new WCExecDepSetting();
            execDepSetting.setCreate_dt(new Date());
            execDepSetting.setCreate_usr(loginUserSid);
            execDepSetting.setStatus(Activation.ACTIVE);
            execDepSetting.setWc_tag_sid(wc_tag_Sid);
        } else {
            execDepSetting = wcExecDepSettingRepository.findOne(wc_exec_dep_setting_sid);
            execDepSetting.setUpdate_dt(new Date());
            execDepSetting.setUpdate_usr(loginUserSid);
        }
        execDepSetting.setNeedAssigned(isNeedAssign);
        execDepSetting.setExec_dep_sid(execDep.getSid());
        execDepSetting.setExecSignMember(this.transToUsersTo(signUserSids));
        execDepSetting.setTransUser(this.transToUsersTo(assignUserSids));
        execDepSetting.setManagerRecevice(this.transToUsersTo(managerReceviceUserSids));
        execDepSetting.setReadReceviceSetting(!receviceUserSids.isEmpty() && !isNeedAssign);
        execDepSetting.setReceviceUser(this.transToUsersTo(receviceUserSids));
        execDepSetting.setOnlyReceviceUser(isOnlyReceviceUser);
        execDepSetting.setTransdep(this.transToUserDep(selectAssignOrgs));
        execDepSetting.setReadAssignSetting(isReadAssignSetting);
        execDepSetting.setContact(cyWindow);
        execDepSetting.setTransDepContainFollowing(isTransDepContainFollowing);
        execDepSetting.setExecDesc(execDesc);
        execDepSetting = wcExecDepSettingRepository.save(execDepSetting);
        wcExecDepSettingCache.updateCacheByExecDepSetting(execDepSetting);
        this.transWCExecDep(execDepSetting, loginUserSid);
    }

    /**
     * 轉換To UsersTo
     *
     * @param sids
     * @return
     */
    private UsersTo transToUsersTo(List<Integer> sids) {
        UsersTo utt = new UsersTo();
        sids.forEach(
                item -> {
                    UserTo uo = new UserTo();
                    uo.setSid(item);
                    utt.getUserTos().add(uo);
                });
        return utt;
    }

    /**
     * 轉換To UserDep
     *
     * @param sids
     * @return
     */
    private UserDep transToUserDep(List<String> sids) {
        UserDep utt = new UserDep();
        sids.forEach(
                item -> {
                    UserDepTo uo = new UserDepTo();
                    uo.setDepSid(item);
                    utt.getUserDepTos().add(uo);
                });
        return utt;
    }

    /**
     * 執行執行單位轉換
     *
     * @param execDepSetting 執行單位設定
     * @param loginUserSid   登入者Sid
     */
    private void transWCExecDep(WCExecDepSetting execDepSetting, Integer loginUserSid) {
        List<WCExceDep> exceDeps = wcExecDepManager.getByExecDepSettingSid(execDepSetting.getSid());
        if (exceDeps != null) {
            for (WCExceDep item : exceDeps) {
                if (!item.getStatus().equals(Activation.ACTIVE)) {
                    continue;
                }
                if (!item.getExecDepStatus().equals(WCExceDepStatus.CLOSE)
                        && !item.getExecDepStatus().equals(WCExceDepStatus.STOP)) {
                    try {
                        // 僅領單人可閱
                        // item.setReceviceUser(execDepSetting.getReceviceUser());
                        // 執行單位轉換
                        item.setDep_sid(execDepSetting.getExec_dep_sid());
                        item.setUpdate_dt(new Date());
                        item.setUpdate_usr(loginUserSid);
                        wcExecDepManager.updateWCExceDep(item);
                        logHelper.addLogInfo(
                                loginUserSid,
                                "WCExceDep:sid["
                                        + item.getSid()
                                        + "] 執行單位["
                                        + WkOrgUtils.findNameBySid(item.getDep_sid())
                                        + "]->["
                                        + WkOrgUtils.findNameBySid(execDepSetting.getExec_dep_sid())
                                        + "]");
                    } catch (Exception e) {
                        log.warn("transWCExecDep ERROR", e);
                    }
                }
            }
        }
    }

    /**
     * 取得執行單位部門名稱
     *
     * @param compID
     * @param wc_tag_sid
     * @return
     */
    public String[] getExecDepSettingDepsName(String compID, String wc_tag_sid) {

        // 回傳結果
        String[] results = new String[] { "-", "未設定" };

        // 查詢項目的執行單位
        List<WCExecDepSetting> execDeps = this.getExecDepSettingFromCacheByWcTagSid(wc_tag_sid, Activation.ACTIVE);
        if (WkStringUtils.isEmpty(execDeps)) {
            return results;
        }

        // 收集部門名稱和 sid
        List<String> depsName = Lists.newArrayList();
        List<Integer> depSids = Lists.newArrayList();
        for (WCExecDepSetting execDepSetting : execDeps) {
            Org dep = this.orgManager.findBySid(execDepSetting.getExec_dep_sid());
            if (dep != null) {
                depSids.add(dep.getSid());
//                depsName.add(dep.getName());
                depsName.add(dep.getOrgNameWithParentIfDuplicate());
            }
        }

        // 組部門名稱顯示文字
        results[0] = String.join("<br/>", depsName);
        // 組部門名稱tooltip
        results[1] = WkOrgUtils.prepareDepsNameByTreeStyle(compID, depSids, 20);

        return results;
    }

    /**
     * 取得執行單位設定檔 By 執行項目Sid and 狀態
     *
     * @param wc_tag_sid 執行項目Sid
     * @param status     狀態
     * @return WCExecDepSetting list
     */
    public List<WCExecDepSetting> getExecDepSettingFromCacheByWcTagSid(
            String wcTagSid, Activation status) {

        return this.findByWcTagSidAndStatus(wcTagSid, status, true);

    }

    /**
     * 以 wcTagSid 查詢執行單位設定資料
     * 
     * @param wcTagSid    執行項目Sid
     * @param status      狀態
     * @param isFromCache true:以cache查詢 /false:撈 DB
     * @return WCExecDepSetting list
     */
    public List<WCExecDepSetting> findByWcTagSidAndStatus(
            String wcTagSid,
            Activation status,
            boolean isFromCache) {

        // ====================================
        // 查詢
        // ====================================
        List<WCExecDepSetting> wcExecDepSettings = null;
        if (isFromCache) {
            // 由 cache 取得
            wcExecDepSettings = this.wcExecDepSettingCache.getExecDepSettingsByTagSid(wcTagSid, status);
        } else {
            // 直接撈 DB
            wcExecDepSettings = this.wcExecDepSettingRepository.findByWcTagSid(wcTagSid);
            // 狀態過濾
            wcExecDepSettings = wcExecDepSettings.stream()
                    .filter(sett -> Activation.ACTIVE.equals(sett.getStatus()))
                    .collect(Collectors.toList());
        }

        if (WkStringUtils.isEmpty(wcExecDepSettings)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 整裡資料
        // ====================================
        // 取得所有單位排序
        Map<Integer, Integer> orgOrderSeqMap = WkOrgCache.getInstance().findAllOrgOrderSeqMap();
        // 建立排序器
        Comparator<WCExecDepSetting> comparator = Comparator.comparing(
                depSetting -> orgOrderSeqMap.get(depSetting.getExec_dep_sid()),
                Comparator.nullsLast(Comparator.naturalOrder()));

        // 排序
        return wcExecDepSettings.stream()
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    /**
     * 取得執行單位設定檔 (For 使用者管理執行單位)
     *
     * @param wc_tag_sid   執行項目Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<WCExecDepSetting> getExecDepSettingForManager(
            String wc_tag_sid, Integer loginUserSid) {
        List<WCExecDepSetting> results = Lists.newArrayList();
        wcExecDepSettingCache
                .getExecDepSettingsByTagSid(wc_tag_sid, Activation.ACTIVE)
                .forEach(
                        item -> {
                            if (menuTagHelper.checkCanShowManagerExecDepSetting(item, loginUserSid)) {
                                results.add(item);
                            }
                        });
        return results;
    }

    /**
     * 取得執行單位設定檔 By sid
     *
     * @param wc_exec_dep_setting_sid 執行單位設定檔Sid
     * @return
     */
    public WCExecDepSetting findBySidFromCache(String wc_exec_dep_setting_sid) {
        return wcExecDepSettingCache.getExecDepSettingBySid(wc_exec_dep_setting_sid);
    }

    public WCTag findWCTagBySettingSid(String wc_exec_dep_setting_sid) {

        // ====================================
        // 傳入設定檔 sid 為空 (加派單位)
        // ====================================
        if (WkStringUtils.isEmpty(wc_exec_dep_setting_sid)) {
            return null;
        }

        // ====================================
        // 取得設定檔
        // ====================================
        WCExecDepSetting wcExecDepSetting = this.wcExecDepSettingCache.getExecDepSettingBySid(wc_exec_dep_setting_sid);
        if (wcExecDepSetting == null) {
            return null;
        }

        // ====================================
        // 取得 tag
        // ====================================
        return this.wcTagCache.findBySid(wcExecDepSetting.getWc_tag_sid());

    }

    /**
     * 刪除執行單位設定檔
     *
     * @param wc_exec_dep_setting_sid 執行單位設定檔Sid
     * @param loginUserSid
     */
    public void deleteExecDepSetting(String wc_exec_dep_setting_sid, Integer loginUserSid) {
        WCExecDepSetting execDepSetting = wcExecDepSettingRepository.findOne(
                wc_exec_dep_setting_sid);
        execDepSetting.setUpdate_usr(loginUserSid);
        execDepSetting.setUpdate_dt(new Date());
        execDepSetting.setStatus(Activation.INACTIVE);
        execDepSetting = wcExecDepSettingRepository.save(execDepSetting);
        wcExecDepSettingCache.updateCacheByExecDepSetting(execDepSetting);
    }

    /**
     * 刪除執行單位設定檔
     *
     * @param sid
     */
    public void deleteExecDepSetting(String sid) {
        this.wcExecDepSettingRepository.delete(sid);
        log.info("刪除執行單位設定檔:" + sid);
    }

    /**
     * 判斷是否為管理領單人員
     *
     * @param user_sid 登入者 Sid
     * @return
     */
    public boolean isManagerRecevice(Integer user_sid) {
        return wcExecDepSettingRepository.isManagerRecevice(user_sid);
    }
}
