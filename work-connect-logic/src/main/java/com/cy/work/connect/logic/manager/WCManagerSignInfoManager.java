package com.cy.work.connect.logic.manager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.cache.WCManagerSignInfoCache;
import com.cy.work.connect.logic.helper.SignFlowHelper;
import com.cy.work.connect.logic.helper.WCManagerSignInfoCheckHelper;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.repository.WCManagerSignInfoRepository;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.FlowType;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.SignActionType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 需求方簽核資訊 Manager
 *
 * @author kasim
 */
@Slf4j
@Component
public class WCManagerSignInfoManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8257917083101090963L;

    private static WCManagerSignInfoManager instance;

    public static WCManagerSignInfoManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCManagerSignInfoManager.instance = this;
    }

    @Autowired
    private WCManagerSignInfoRepository wcManagerSignInfoRepository;
    @Autowired
    private BpmManager bpmManager;
    @Autowired
    private WCManagerSignInfoCache wcManagerSignInfoCache;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    @Autowired
    private WCRollbackHistoryManager wcRollbackHistoryManager;
    /**
     * 意見說明,退回理由,作廢理由 Manager
     */
    @Autowired
    private WCOpinionDescripeSettingManager opinionDescripeSettingManager;
    @Autowired
    private WCManagerSignInfoCheckHelper managerSignInfoCheckHelper;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WCManagerSignInfoDetailManager wcManagerSignInfoDetailManager;
    @Autowired
    private TaskClient taskClient;
    @Autowired
    private SignFlowHelper signFlowHelper;

    /**
     * 查詢 by Sid
     *
     * @param sid
     * @return
     */
    public WCManagerSignInfo findBySid(String sid) {
        return wcManagerSignInfoRepository.findOne(sid);
    }

    /**
     * 刪除需求單位簽核物件
     *
     * @param signInfoSid 需求單位簽核物件Sid
     * @param wcNo 工作聯絡單單號
     * @throws Exception
     */
    private void deleteManagerSignInfo(String signInfoSid, String wcNo) throws Exception {

        // 取得 需求單位簽核物件
        WCManagerSignInfo signInfo = this.findBySid(signInfoSid);
        // 取得 簽核 user
        User user = WkUserCache.getInstance().findBySid(signInfo.getUser_sid());

        log.debug("需求單位加簽移除 工作聯絡單單號-" + wcNo + "==移除加簽人員-userId" + user.getId() + "==");
        if (!Strings.isNullOrEmpty(signInfo.getBpmId())) {
            // 執行作廢 - bpm 終止流程
            bpmManager.invalid(user.getId(), signInfo.getBpmId());
        }

        // 作廢不進行刪除流程
        // signInfo = wcManagerSignInfoCache.doSyncBpmData(signInfo);
        this.wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);

        // 更新 需求單位簽核物件 狀態為 作廢
        this.updateBpmStatus(user.getId(), signInfo, BpmStatus.DELETE);
    }

    /**
     * 建立需求流程會簽簽核物件
     *
     * @param wcSid 工作聯絡單Sid
     * @param wcNo 工作聯絡單單號
     * @param item 欲加會簽簽核人員Sid
     * @throws Exception
     */
    private void createContinueManagerSignInfo(String wcSid, String wcNo, Integer item)
            throws Exception {
        User user = WkUserCache.getInstance().findBySid(item);
        String bpmId = "";
        WCManagerSignInfo signInfo = new WCManagerSignInfo();
        signInfo.setWcSid(wcSid);
        signInfo.setWcNo(wcNo);

        // 是否需求單位流程(層簽)已簽核完畢
        if (isReqFlowSigned(wcSid)) {

            // 新增BPM 流程 (工作連絡單-需求單位加簽)
            bpmId = bpmManager.createBpmFlow(
                    wcNo,
                    user.getSid(),
                    user.getPrimaryOrg().getCompanySid(),
                    WcBpmFlowType.WC_CS_REQ_UNIT);

            log.debug("需求單位加簽新增 工作聯絡單單號-" + wcNo + "==新增加簽人員-userId" + user.getId());
            Preconditions.checkState(!Strings.isNullOrEmpty(bpmId), "BPM 流程建立失敗！！");
            signInfo.setStatus(BpmStatus.NEW_INSTANCE);
        } else {
            signInfo.setStatus(BpmStatus.WAIT_CREATE);
        }
        signInfo.setSignType(SignType.COUNTERSIGN);
        signInfo.setCreate_dt(new Date());
        signInfo.setUser_sid(item);
        signInfo.setDep_sid(user.getPrimaryOrg().getSid());
        signInfo.setGroupSeq(0);
        signInfo.setSeq(wcManagerSignInfoCache.getMaxSeq(signInfo.getGroupSeq(), wcSid));
        signInfo.setBpmId(bpmId);
        signInfo = this.doSyncBpmData(signInfo, null, null);
        wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
    }

    /**
     * 修改需求單位會簽
     *
     * @param wcSid 工作聯絡單Sid
     * @param wc_no 工作聯絡單單號
     * @param removeSingleManagerSignInfos 欲刪除需求單位會簽簽核物件
     * @param addUserFlowSid 欲新增需求單位會簽簽核人員
     * @param loginUserSid 執行人員Sid
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void saveCounterSignInfo(
            String wcSid,
            String wc_no,
            List<SingleManagerSignInfo> removeSingleManagerSignInfos,
            List<Integer> addUserFlowSid,
            Integer loginUserSid)
            throws ProcessRestException {

        // 移除加簽人員簽核流程 by 移除名單
        removeSingleManagerSignInfos.forEach(
                item -> {
                    try {
                        this.deleteManagerSignInfo(item.getSignInfoSid(), wc_no);
                    } catch (IllegalStateException | IllegalArgumentException e) {
                        log.warn("bpmManager.invalid ERROR：{}", e.getMessage());
                    } catch (Exception ex) {
                        log.warn("bpmManager.invalid ERROR", ex);
                    }
                });

        // 新增加簽人員簽核流程 by 新增名單
        addUserFlowSid.forEach(
                item -> {
                    try {
                        // 建立需求流程會簽簽核物件
                        this.createContinueManagerSignInfo(wcSid, wc_no, item);
                    } catch (IllegalStateException | IllegalArgumentException e) {
                        log.warn("bpmManager.create ERROR：{}", e.getMessage());
                    } catch (Exception ex) {
                        log.warn("bpmManager.create ERROR", ex);
                    }
                });
        // 若需求單位皆簽核完畢,單據狀態變為核准,並且自動產生系統預設執行單位資訊
        this.changeWCMasterStatus(wcSid, loginUserSid);
    }

    /**
     * 是否需求單位流程已簽核完畢
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    private boolean isReqFlowSigned(String wcSid) {
        boolean result = false;
        try {

            List<SingleManagerSignInfo> notApproveds = this.getFlowSignInfos(wcSid).stream()
                    .filter(each -> !each.getStatus().equals(BpmStatus.APPROVED))
                    .collect(Collectors.toList());

            if (notApproveds == null || notApproveds.isEmpty()) {
                result = true;
            }
        } catch (Exception e) {
            log.warn("isReqFlowSigned ERROR", e);
        }
        return result;
    }

    /**
     * 檢測會簽是否已有人簽名
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isCheckContinueSign(String wcSid) {
        for (SingleManagerSignInfo si : getCounterSignInfos(wcSid)) {
            if (si.getStatus().equals(BpmStatus.APPROVED)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 清除會簽節點
     *
     * @param wc_ID 工作聯絡單Sid
     * @param loginUser 執行者
     */
    public void clearContinueSignInfo(String wcSid, User loginUser) {

        // ====================================
        // 查詢會簽
        // ====================================
        List<WCManagerSignInfo> activeCounterSignInfos = this.findReqFlowCounterSignInfos(wcSid);
        if (WkStringUtils.isEmpty(activeCounterSignInfos)) {
            return;
        }

        // ====================================
        // 作廢會簽流程
        // ====================================
        for (WCManagerSignInfo wcManagerSignInfo : activeCounterSignInfos) {
            try {
                this.doClearBpmContinueSign(
                        loginUser.getId(),
                        wcManagerSignInfo);
            } catch (ProcessRestException e) {
                log.error("清除會簽節點失敗!sid:[" + wcManagerSignInfo.getSid() + "]", e);
            }
        }
    }

    /**
     * 取得需求單位會簽List
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<SingleManagerSignInfo> getCounterSignInfos(String wcSid) {
        try {
            List<GroupManagerSignInfo> groupManagerSignInfos = wcManagerSignInfoCache.getGroupManagerSignInfo(wcSid);
            if (groupManagerSignInfos == null || groupManagerSignInfos.isEmpty()) {
                return Lists.newArrayList();
            }
            GroupManagerSignInfo reqGroupManagerSignInfo = groupManagerSignInfos.get(0);
            List<SingleManagerSignInfo> counterSignInfos = reqGroupManagerSignInfo.getSingleManagerSignInfos().stream()
                    .filter(each -> each.getSignType().equals(SignType.COUNTERSIGN))
                    .collect(Collectors.toList());
            if (counterSignInfos == null) {
                return Lists.newArrayList();
            }
            return counterSignInfos;
        } catch (Exception e) {
            log.warn("getCounterSignInfos ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得需求流程List
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<SingleManagerSignInfo> getFlowSignInfos(String wcSid) {

        // 由快取中, 取得該案件單 所有的 需求單位群組簽核資訊
        List<GroupManagerSignInfo> groupManagerSignInfos = wcManagerSignInfoCache.getGroupManagerSignInfo(wcSid);

        if (groupManagerSignInfos == null || groupManagerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }

        GroupManagerSignInfo reqGroupManagerSignInfo = groupManagerSignInfos.get(0);

        List<SingleManagerSignInfo> counterSignInfos = reqGroupManagerSignInfo.getSingleManagerSignInfos().stream()
                .filter(each -> each.getSignType().equals(SignType.SIGN))
                .collect(Collectors.toList());

        if (counterSignInfos == null) {
            return Lists.newArrayList();
        }
        return counterSignInfos;
    }

    public List<WCManagerSignInfo> getAllFlowSignInfos(String wc_ID) {
        List<WCManagerSignInfo> managerSignInfos = wcManagerSignInfoRepository.findOneByWCSid(
                wc_ID);
        if (managerSignInfos == null || managerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<WCManagerSignInfo> activeWCManagerSignInfos = managerSignInfos.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                        && !each.getStatus().equals(BpmStatus.INVALID))
                .collect(Collectors.toList());
        return activeWCManagerSignInfos;
    }

    /**
     * 取得需求單位簽核流程
     *
     * @param wcSid
     * @return
     * @throws SystemOperationException
     */
    public Optional<WCManagerSignInfo> findReqUnitFlowSignInfo(String wcSid) throws SystemOperationException {
        // ====================================
        // 查詢
        // ====================================
        List<WCManagerSignInfo> managerSignInfos = this.findActiveReqFlowSignInfos(wcSid);

        if (WkStringUtils.isEmpty(managerSignInfos)) {
            return Optional.empty();
        }

        // ====================================
        // 過濾需為『需求單位簽核流程』
        // ====================================
        List<WCManagerSignInfo> reqUnitSignInfos = managerSignInfos.stream()
                // 過濾需為『需求單位簽核流程』
                .filter(signInfo -> SignType.SIGN.equals(signInfo.getSignType()))
                .collect(Collectors.toList());

        // ====================================
        // 回傳
        // ====================================
        // 為空
        if (WkStringUtils.isEmpty(reqUnitSignInfos)) {
            return Optional.empty();
        }

        // 檢核資料錯誤 (需求流程同時間僅能有一筆)
        if (reqUnitSignInfos.size() > 1) {
            String message = "資料錯誤，需求單位簽核流程有多筆! wcsid:[" + wcSid + "]";
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
            log.error(message);
            throw new SystemOperationException(WkMessage.EXECTION, InfomationLevel.ERROR);
        }

        // 回傳
        return reqUnitSignInfos.stream().findFirst();

    }

    /**
     * 查詢作用中的需求單位流程 (主流程 + 會簽)
     *
     * @param wcSid
     * @return
     */
    public List<WCManagerSignInfo> findActiveReqFlowSignInfos(String wcSid) {
        // 無效的流程狀態
        Set<BpmStatus> inActiveBpmStatus = Sets.newHashSet(BpmStatus.DELETE, BpmStatus.INVALID, BpmStatus.WAIT_CREATE);

        // 查詢
        return this.findReqFlowSignInfosByWithoutStatus(wcSid, inActiveBpmStatus);
    }

    private List<WCManagerSignInfo> findReqFlowSignInfosByWithoutStatus(
            String wcSid,
            Set<BpmStatus> inActiveBpmStatus) {

        // ====================================
        // 查詢
        // ====================================
        List<WCManagerSignInfo> managerSignInfos = this.wcManagerSignInfoRepository.findByWcSid(wcSid);

        if (WkStringUtils.isEmpty(managerSignInfos)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾狀態
        // ====================================
        return managerSignInfos.stream()
                // 過濾無效狀態
                .filter(signInfo -> !inActiveBpmStatus.contains(signInfo.getStatus()))
                .collect(Collectors.toList());

    }

    /**
     * 依據『執行項目』SID 取得建單需求流程類型
     *
     * @param tagSids
     * @param bpmLevelMapByFlowType
     * @return
     */
    public FlowType findTopFlowType(List<WCTag> wcTags, Map<FlowType, Integer> bpmLevelMapByFlowType) {
        // 防呆
        if (WkStringUtils.isEmpty(wcTags)) {
            return null;
        }

        // 尋找層級最高
        FlowType topFlowType = null;

        for (WCTag wcTag : wcTags) {
            FlowType currFlowType = wcTag.getReqFlowType();
            if (currFlowType == null) {
                continue;
            }
            // 第一次
            if (topFlowType == null) {
                topFlowType = currFlowType;
                continue;
            }

            if (!topFlowType.isEnable()) {
                log.error("簽核層級：[{}]應已停用!", topFlowType);
                continue;
            }

            // 若層級高於(數字小)，則取代
            Integer currFlowTypeLevel = bpmLevelMapByFlowType.get(currFlowType);
            Integer topFlowTypeLevel = bpmLevelMapByFlowType.get(topFlowType);
            if (currFlowTypeLevel < topFlowTypeLevel) {
                topFlowType = currFlowType;
            }
        }

        return topFlowType;
    }

    /**
     * 建立需求單位流程
     *
     * @param wcMaster
     * @param execUserSid
     * @throws UserMessageException
     */
    @Transactional(rollbackFor = Exception.class)
    public String createBpmFlow(WCMaster wcMaster, Integer execUserSid) throws UserMessageException {

        // ====================================
        // 計算簽核參數
        // ====================================
        Map<String, Object> parameters = this.signFlowHelper.prepareModelerParamsForApplyFlow(wcMaster, execUserSid);

        // ====================================
        // 建立 BPM 流程
        // ====================================
        String bpmId = "";

        try {
            bpmId = this.bpmManager.createBpmFlow(
                    wcMaster.getWc_no(),
                    execUserSid,
                    wcMaster.getComp_sid(),
                    WcBpmFlowType.WC_REQUEST,
                    parameters);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(建立需求流程失敗!)";
            log.error(errorMessage + e.getMessage());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        log.info("[{}] 建立需求方流程:[{}]", wcMaster.getWc_no(), bpmId);

        return bpmId;
    }

    /**
     * 建立需求方簽核資訊
     *
     * @param wcMaster 主單資料
     * @param bpmId BPM ID
     * @return WCManagerSignInfo
     * @throws UserMessageException 錯誤時拋出
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public WCManagerSignInfo createSignInfo(WCMaster wcMaster, String bpmId) throws UserMessageException {
        // ====================================
        // 準備執行方簽核資訊主檔資料
        // ====================================
        WCManagerSignInfo signInfo = new WCManagerSignInfo();
        signInfo.setWcSid(wcMaster.getSid());
        signInfo.setWcNo(wcMaster.getWc_no());
        signInfo.setStatus(BpmStatus.NEW_INSTANCE);
        signInfo.setSignType(SignType.SIGN);
        signInfo.setCreate_dt(new Date());
        signInfo.setUser_sid(wcMaster.getCreate_usr());
        signInfo.setDep_sid(wcMaster.getDep_sid());
        signInfo.setGroupSeq(wcManagerSignInfoCache.getMaxGroupSeq(wcMaster.getSid()));
        signInfo.setSeq(wcManagerSignInfoCache.getMaxSeq(signInfo.getGroupSeq(), wcMaster.getSid()));
        signInfo.setBpmId(bpmId);

        // ====================================
        // 同步BPM 水管圖 後, 建立需求方簽核資訊檔
        // insert wc_manager_sign_info、wc_manager_sign_info_detail
        // ====================================
        try {
            signInfo = this.doSyncBpmData(signInfo, null, wcMaster.getCreate_usr());
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(建立需求流程資料失敗!)";
            log.error(errorMessage + e.getMessage(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 更新Cache By 需求單位簽核物件
        // ====================================
        this.wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);

        return signInfo;
    }

    /**
     * 依據聯絡單取得 BPM流程
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<GroupManagerSignInfo> getGroupManagerSignInfo(String wcSid) {
        try {
            return wcManagerSignInfoCache.getGroupManagerSignInfo(wcSid);
        } catch (Exception e) {
            log.warn("getGroupManagerSignInfo ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 此為清除會簽節點的BPM資訊, 會簽節點會根據需求流程節點簽名完畢,才會產生 若有人進行退回或復原
     * 該BPM節點必須清除,但會簽人員資訊不需要再重新挑選
     *
     * @param userId 清除節點User ID
     * @param signInfo 簽核物件
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doClearBpmContinueSign(String userId, WCManagerSignInfo signInfo)
            throws ProcessRestException {
        bpmManager.invalid(userId, signInfo.getBpmId());
        signInfo.setBpmId("");
        signInfo.setStatus(BpmStatus.WAIT_CREATE);
        signInfo.setUpdate_dt(new Date());
        signInfo.setBpmDefaultSignedName("");
        signInfo = wcManagerSignInfoRepository.save(signInfo);
        wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
    }

    /**
     * BPM 簽名
     *
     * @param signInfo
     * @param signUser
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void onlyDoBPMSign(WCManagerSignInfo signInfo, User signUser) throws ProcessRestException {

        // ====================================
        // BPM 簽名
        // ====================================
        // 為主簽核流程時，預設連簽
        if (SignType.SIGN.equals(signInfo.getSignType())) {
            // 串簽
            bpmManager.signContinue(signUser.getId(), signInfo.getBpmId());
        } else {
            // 一般簽核
            bpmManager.sign(signUser.getId(), signInfo.getBpmId());
        }

        // ====================================
        // 更新流程資訊 from BPM
        // ====================================
        signInfo = this.doSyncBpmData(
                signInfo,
                SignActionType.NORMAL,
                signUser.getSid());

        // 更新Cache By 需求單位簽核物件
        this.wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);

        // 取得當前BPM狀態
        BpmStatus bpmStatus = this.bpmManager.getBpmStatus(
                ManagerSingInfoType.MANAGERSIGNINFO,
                signUser.getId(), signInfo.getBpmId(), null);

        // 更新BpmStatus
        this.updateBpmStatus(signUser.getId(), signInfo, bpmStatus);

        // 根據需求流程BPM狀態更新工作聯絡單主檔狀態
        this.updateStatusByBPM(signInfo.getWcSid(), signUser.getSid(), bpmStatus);

        // ====================================
        // log
        // ====================================
        String userInfo = WkUserUtils.prepareUserNameWithDep(
                Lists.newArrayList(signUser.getSid()), OrgLevel.THE_PANEL, false, "-", "、", true);

        WCMaster wcMaster = this.wcMasterManager.findBySid(signInfo.getWcSid());
        log.debug("[{}]：執行需求單位簽名,單號[{}],BPM狀態[{}],聯絡單狀態[{}]",
                userInfo,
                signInfo.getWcNo(),
                bpmStatus,
                wcMaster.getWc_status());
    }

    /**
     * 將需求會簽恢復簽核資訊
     *
     * @param item 會簽物件
     * @throws ProcessRestException
     */
    private void createContinueManagerSignInfo(String wcSid) {

        // ====================================
        // 取得待建立的會簽流程
        // ====================================
        List<WCManagerSignInfo> counterSignInfos = this.wcManagerSignInfoRepository.findByWcSidAndStatusAndSignType(
                wcSid,
                BpmStatus.WAIT_CREATE,
                SignType.COUNTERSIGN);

        // 無需處理
        if (WkStringUtils.isEmpty(counterSignInfos)) {
            return;
        }

        // ====================================
        // 逐筆重建會簽流程
        // ====================================
        for (WCManagerSignInfo wcManagerSignInfo : counterSignInfos) {
            try {
                // 取得會簽人員資料
                User signUser = WkUserCache.getInstance().findBySid(wcManagerSignInfo.getUser_sid());

                // 建立BPM流程
                String bpmId = bpmManager.createBpmFlow(
                        wcManagerSignInfo.getWcNo(),
                        wcManagerSignInfo.getUser_sid(),
                        signUser.getPrimaryOrg().getCompanySid(),
                        WcBpmFlowType.WC_CS_REQ_UNIT);

                if (bpmId == null) {
                    log.error("BPM 流程建立失敗！！，回傳ID為空");
                    continue;
                }

                // 更新資料
                wcManagerSignInfo.setStatus(BpmStatus.NEW_INSTANCE);
                wcManagerSignInfo.setBpmId(bpmId);

                wcManagerSignInfo = this.doSyncBpmData(wcManagerSignInfo, null, null);
                wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(wcManagerSignInfo);
            } catch (Exception e) {
                // 失敗暫不拋出
                log.error("建立會簽流程失敗!" + e.getMessage(), e);
            }
        }
    }

    /**
     * 若需求單位皆簽核完畢,單據狀態變為核准,並且自動產生系統預設執行單位資訊
     *
     * @param wcSid
     * @param loginUserSid
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void changeWCMasterStatus(String wcSid, Integer loginUserSid) {

        // ====================================
        // 查詢有效流程
        // ====================================
        // List<WCManagerSignInfo> activeManagerSignInfos = this.findAllActiveFlowSignInfos(wcSid);
        // ====================================
        // 檢查需求主流程已簽完
        // ====================================
        // --------------------
        // 需求簽核流程是否簽核完畢
        // --------------------
        boolean isReqFinish = this.managerSignInfoCheckHelper.isReqFlowFinish(wcSid);

        // 需求流程未簽完，無需往下處理
        if (!isReqFinish) {
            return;
        }

        // --------------------
        // 若需求流程簽核完畢,建立會簽流程
        // --------------------
        this.createContinueManagerSignInfo(wcSid);

        // ====================================
        // 檢查需求流程已全部簽完 (需求流程 & 需求會簽)
        // ====================================
        // 需求簽核流程是否全數簽核完畢(需求流程 & 需求會簽)
        boolean isAllFinish = this.managerSignInfoCheckHelper.isReqAllFinish(wcSid);
        // 未全部簽完，不往下處理
        if (!isAllFinish) {
            return;
        }

        // ====================================
        // 主單狀態更新為核准
        // ====================================
        this.wcMasterCustomLogicManager.updateToApproved(wcSid, loginUserSid, true);

        // ====================================
        // 建立執行單位流程
        // ====================================
        this.wcExecManagerSignInfoManager.createDefaultExecFlow(wcSid, loginUserSid);

    }

    /**
     * 執行BPM復原
     *
     * @param userId 執行者 ID
     * @param managerSignInfo 需求單位簽核流程資料
     * @param isCounterSignRecovery true:會簽復原/false:單位簽核復原
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCManagerSignInfo recoveryBpm(
            User execUser,
            WCManagerSignInfo managerSignInfo,
            boolean isCounterSignRecovery)
            throws ProcessRestException {

        // ====================================
        // 水管圖
        // ====================================
        List<ProcessTaskBase> tasks = bpmManager.findSimulationChart(
                execUser.getId(),
                managerSignInfo.getBpmId());

        // ====================================
        // BPM復原
        // ====================================
        int recoverySub = BpmStatus.APPROVED.equals(managerSignInfo.getStatus()) ? 1 : 2;
        ProcessTaskHistory recoveryTargetTask = (ProcessTaskHistory) tasks.get(tasks.size() - recoverySub);
        log.info("復原至:[{}]", recoveryTargetTask.getExecutorID());

        // bpmManager.recovery(execUser.getId(), (ProcessTaskHistory) tasks.get(tasks.size() - recoverySub));
        bpmManager.recovery(execUser.getId(), recoveryTargetTask);

        // ====================================
        // 即時更新流程資訊
        // ====================================
        // 更新簽核流程資料 (待簽資訊)
        managerSignInfo = this.doSyncBpmData(managerSignInfo, SignActionType.RECOVERY, execUser.getSid());

        // 更新 簽核狀態
        // 判斷狀態
        BpmStatus bpmStatus = bpmManager.getBpmStatus(
                ManagerSingInfoType.MANAGERSIGNINFO,
                execUser.getId(),
                managerSignInfo.getBpmId(),
                opinionDescripeSettingManager.getOpinionDescripeSetting(
                        ManagerSingInfoType.MANAGERSIGNINFO, managerSignInfo.getSid(), execUser.getSid()));
        // 更新狀態
        managerSignInfo = this.updateBpmStatus(execUser.getId(), managerSignInfo, bpmStatus);

        // ====================================
        // 更新簽核流程快取
        // ====================================
        this.wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(managerSignInfo);

        // ====================================
        // 更新主單狀態
        // ====================================
        if (isCounterSignRecovery) {
            // 需求單位會簽復原. 狀態一定退回 APPROVING
            wcMasterCustomLogicManager.updateStatus(
                    managerSignInfo.getWcSid(), execUser.getSid(), WCStatus.APPROVING);
        } else {
            // 依據狀況判斷更新狀態
            this.updateStatusByBPM(managerSignInfo.getWcSid(), execUser.getSid(), bpmStatus);
        }

        return managerSignInfo;
    }

    /**
     * 執行退回動作
     *
     * @param execUserId 執行人員Id
     * @param signInfo 簽核節點物件
     * @param rollBackTask 欲退回至的節點物件
     * @param comment 退回理由
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public WCManagerSignInfo doRollBack(
            String execUserId,
            WCManagerSignInfo signInfo,
            ProcessTaskHistory rollBackTask,
            String comment,
            ActionType actionType) throws UserMessageException {

        // 執行者
        User execUser = WkUserCache.getInstance().findById(execUserId);
        // 被退回的節點簽核者
        User backToUser = WkUserCache.getInstance().findById(rollBackTask.getExecutorID());

        // ====================================
        // 將退回理由存至對應人員(被退的人員)
        // ====================================
        WCOpinionDescripeSetting opinionDescripeSetting = this.opinionDescripeSettingManager.saveRollbackReason(
                ManagerSingInfoType.MANAGERSIGNINFO,
                actionType,
                signInfo.getSid(),
                backToUser.getSid(),
                comment,
                execUser.getSid(),
                signInfo.getWcSid());

        // ====================================
        // BPM 退回
        // ====================================
        try {
            this.bpmManager.rollBack(execUserId, rollBackTask, comment);
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.EXECTION + "(執行BPM退回失敗)";
            log.error(errorMessage + "BPMOD:[{}]" + e.getMessage(), rollBackTask.getInstanceID(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 更新簽核流程資訊
        // ====================================
        try {
            // 更新簽名記錄、待簽者
            signInfo = this.doSyncBpmData(signInfo, SignActionType.ROLLBACK, backToUser.getSid());
            // 更新快取
            this.wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
            // 更新流程狀態
            BpmStatus bpmStatus = bpmManager.getBpmStatus(
                    ManagerSingInfoType.MANAGERSIGNINFO,
                    execUserId,
                    signInfo.getBpmId(),
                    opinionDescripeSetting);
            this.updateBpmStatus(execUserId, signInfo, bpmStatus);
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.EXECTION + "(更新簽核流程資訊失敗)";
            log.error(errorMessage + "BPMOD:[{}]" + e.getMessage(), rollBackTask.getInstanceID(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 更新主檔狀態
        // ====================================
        this.updateStatusByBPM(signInfo.getWcSid(), execUser.getSid(), signInfo.getStatus());

        // ====================================
        // 新增退回記錄
        // ====================================
        this.wcRollbackHistoryManager.insertRollBackHistory(execUser.getSid(), signInfo.getWcSid());

        return signInfo;
    }

    /**
     * 根據需求流程BPM狀態更新工作聯絡單主檔狀態
     *
     * @param wcSid 工作聯絡單Sid
     * @param userSid 執行人員Sid
     * @param reqFlowBpmStatus 需求流程BPM狀態
     */
    public void updateStatusByBPM(String wcSid, Integer userSid, BpmStatus reqFlowBpmStatus) {
        if (null == reqFlowBpmStatus) {
            wcMasterCustomLogicManager.updateStatus(wcSid, userSid, WCStatus.APPROVING);
        } else {
            switch (reqFlowBpmStatus) {
                case NEW_INSTANCE:
                    wcMasterCustomLogicManager.updateStatus(wcSid, userSid, WCStatus.NEW_INSTANCE);
                    break;
                case RECONSIDERATION:
                    wcMasterCustomLogicManager.updateStatus(wcSid, userSid, WCStatus.RECONSIDERATION);
                    break;
                case WAITING_FOR_APPROVE:
                    wcMasterCustomLogicManager.updateStatus(wcSid, userSid, WCStatus.WAITAPPROVE);
                    break;
                default:
                    wcMasterCustomLogicManager.updateStatus(wcSid, userSid, WCStatus.APPROVING);
                    break;
            }
        }
    }

    /**
     * 更新BpmStatus
     *
     * @param userId
     * @param signInfo
     * @param status
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCManagerSignInfo updateBpmStatus(String userId, WCManagerSignInfo signInfo, BpmStatus status) {
        Preconditions.checkState(status != null, "BpmStatus 為NULL 無法更新簽核資訊，請確認！！");
        signInfo.setStatus(status);
        signInfo.setUpdate_dt(new Date());
        signInfo = this.wcManagerSignInfoRepository.save(signInfo);
        this.wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
        return signInfo;
    }

    /**
     * 是否顯示簽核按鈕
     *
     * @param userId
     * @param bpmId
     * @return
     */
    public Boolean isShowSign(String userId, String bpmId) {
        List<String> ids;
        try {
            ids = bpmManager.findTaskCanSignUserIdsWithException(bpmId);
        } catch (SystemOperationException e) {
            return false;
        }
        return ids.contains(userId);
    }

    /**
     * 是否顯示復原按鈕
     *
     * @param userId
     * @param status
     * @param tasks
     * @return
     */
    public Boolean isShowRecovery(String userId, BpmStatus status, List<ProcessTaskBase> tasks) {
        // 任務模擬圖需超過兩位才能進行復原，結案也無法復原
        if (tasks.size() <= 1 || BpmStatus.CLOSED.equals(status) || BpmStatus.APPROVED.equals(
                status)) {
            return Boolean.FALSE;
        }
        // 前一個簽核者為當前執行者才可復原
        ProcessTaskBase rTask = tasks.get(tasks.size() - 2);
        if (rTask instanceof ProcessTaskHistory) {
            return userId.equals(((ProcessTaskHistory) rTask).getExecutorID());
        }
        return false;
    }

    /**
     * 是否顯示退回按鈕
     *
     * @param userId
     * @param bpmId
     * @param tasks
     * @return
     */
    public Boolean isShowRollBack(String userId, String bpmId, List<ProcessTaskBase> tasks) {
        List<String> ids = bpmManager.findTaskCanSignUserIds(bpmId);
        return ids.contains(userId) && !tasks.isEmpty() && tasks.size() > 1;
    }

    /**
     * 是否顯示作廢按鈕
     *
     * @param userId
     * @param status
     * @param bpmId
     * @param tasks
     * @return
     */
    public Boolean isShowInvalid(
            String userId, BpmStatus status, String bpmId, List<ProcessTaskBase> tasks) {
        List<String> ids = bpmManager.findTaskCanSignUserIds(bpmId);
        if (!ids.contains(userId)) {
            return Boolean.FALSE;
        }
        if (!tasks.isEmpty()
                && (BpmStatus.NEW_INSTANCE.equals(status)
                || BpmStatus.WAITING_FOR_APPROVE.equals(status)
                || BpmStatus.APPROVING.equals(status)
                || BpmStatus.RECONSIDERATION.equals(status))) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * 是否顯示復原按鈕(正常需求流程-核准)
     *
     * @param userId
     * @param tasks
     * @param wcSid
     * @return
     */
    public Boolean isShowApprovedRecovery(
            String userId,
            List<ProcessTaskBase> tasks,
            String wcSid) {
        try {
            if (WkStringUtils.isEmpty(tasks)) {
                return false;
            }
            ProcessTaskBase rTask = tasks.get(tasks.size() - 1);
            if (rTask instanceof ProcessTaskHistory) {
                if (userId.equals(((ProcessTaskHistory) rTask).getExecutorID())) {
                    // 檢測需求加簽是否已有簽名
                    boolean isSignCheckContinue = this.isCheckContinueSign(wcSid);
                    if (isSignCheckContinue) {
                        return false;
                    }
                    // 是否已有執行單位正在執行
                    boolean isSignCheckFlow = wcExecDepManager.isAnyExecDepWorking(wcSid);
                    if (isSignCheckFlow) {
                        return false;
                    }
                    // 是否已有再會簽
                    boolean isExecSignCheckContinue = wcExecManagerSignInfoManager.isCheckAnyContinue(wcSid);
                    if (isExecSignCheckContinue) {
                        return false;
                    }
                    // 檢測核准者流程是否已有簽名
                    boolean isExecSignCheckFlow = wcExecManagerSignInfoManager.isCheckAnyFlowSign(wcSid);
                    return !isExecSignCheckFlow;
                }
            }
        } catch (Exception e) {
            log.warn("isShowApprovedRecovery ERROR", e);
        }
        return false;
    }

    /**
     * 是否顯示復原按鈕(會簽)
     *
     * @param user 登入者
     * @param tasks BPM水管資訊
     * @param wcSid 主檔 sid
     * @return
     */
    public Boolean isShowContinueRecovery(
            User user, List<ProcessTaskBase> tasks, String wcSid) {
        // 無水管圖資料
        if (WkStringUtils.isEmpty(tasks)) {
            return false;
        }
        // 取得最後一筆
        ProcessTaskBase rTask = tasks.get(tasks.size() - 1);
        // 非『已簽核』節點
        if (!(rTask instanceof ProcessTaskHistory)) {
            return false;
        }

        // 執行者非簽核者
        if (!WkCommonUtils.compareByStr(user.getId(), ((ProcessTaskHistory) rTask).getExecutorID())) {
            return false;
        }

        // 是否已有執行單位正在執行
        boolean isSignCheckFlow = wcExecDepManager.isAnyExecDepWorking(wcSid);
        if (isSignCheckFlow) {
            return false;
        }

        // 是否已有再會簽
        boolean isExecSignCheckContinue = wcExecManagerSignInfoManager.isCheckAnyContinue(wcSid);
        if (isExecSignCheckContinue) {
            return false;
        }

        // 檢測核准者流程是否已有簽名
        boolean isExecSignCheckFlow = wcExecManagerSignInfoManager.isCheckAnyFlowSign(wcSid);
        if (isExecSignCheckFlow) {
            return false;
        }

        return true;
    }

    /**
     * 從BPM REST 更新資訊至Fusion DB
     *
     * @param wcManagerSignInfo 需求單位簽核物件
     * @param signActionType
     * @param signUserSid
     * @return
     * @throws ProcessRestException
     */
    public WCManagerSignInfo doSyncBpmData(
            WCManagerSignInfo wcManagerSignInfo,
            SignActionType signActionType,
            Integer signUserSid)
            throws ProcessRestException {

        // ====================================
        // 由 BPM 取回水管圖 (有 BPM id 時)
        // ====================================
        List<ProcessTaskBase> tasks = Lists.newArrayList();

        String signUserID = "";
        if (signUserSid != null) {
            signUserID = WkUserUtils.findUserIDBySid(signUserSid);
        }

        try {
            if (WkStringUtils.notEmpty(wcManagerSignInfo.getBpmId())) {
                tasks = taskClient.findSimulationChart(
                        wcManagerSignInfo.getBpmId(),
                        signUserID);
            }
        } catch (Exception ex) {
            if (ex != null
                    && ex instanceof ProcessRestException
                    && WkStringUtils.isEmpty(ex.getMessage())) {
                log.error("findSimulationChart ERROR (ProcessRestException and null message) bpmID:[{}]", wcManagerSignInfo.getBpmId());
            } else {
                log.error("findSimulationChart ERROR", ex);
            }
        }

        // ====================================
        // 更新簽核者
        // ====================================
        wcManagerSignInfo.setBpmDefaultSignedName("");
        if (WkStringUtils.notEmpty(tasks)) {
            // 預設簽核人名
            wcManagerSignInfo.setBpmDefaultSignedName(
                    bpmManager.findTaskDefaultUserName(tasks.get(tasks.size() - 1)));
        }

        // save wc_manager_sign_info
        wcManagerSignInfo = wcManagerSignInfoRepository.save(wcManagerSignInfo);

        // ====================================
        // 更新簽名記錄 (由 BPM 同步簽名記錄 wc_manager_sign_info_detail)
        // ====================================
        // (inactive - insert)
        this.wcManagerSignInfoDetailManager.updateSignData(
                wcManagerSignInfo.getSid(),
                tasks,
                signActionType,
                signUserSid);

        return wcManagerSignInfo;
    }

    /**
     * 查詢有效的簽核流程
     *
     * @param WcSid
     * @return
     */
    public List<WCManagerSignInfo> findAllActiveFlowSignInfos(String WcSid) {
        // ====================================
        // 查詢
        // ====================================
        List<WCManagerSignInfo> managerSignInfos = this.wcManagerSignInfoRepository.findOneByWCSid(WcSid);

        if (WkStringUtils.isEmpty(managerSignInfos)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾
        // ====================================
        // 排除刪除、作廢、未建立
        List<BpmStatus> statusfilters = Lists.newArrayList(
                BpmStatus.DELETE,
                BpmStatus.INVALID,
                BpmStatus.WAIT_CREATE);

        return managerSignInfos.stream()
                .filter(signInfo -> !statusfilters.contains(signInfo.getStatus()))
                .collect(Collectors.toList());
    }

    /**
     * 查詢簽核流程 for 畫面顯示
     *
     * @param WcSid
     * @return
     */
    public List<WCManagerSignInfo> findAllReqFlowSignInfosForViewSignInfo(String WcSid) {
        // ====================================
        // 查詢
        // ====================================
        List<WCManagerSignInfo> managerSignInfos = this.wcManagerSignInfoRepository.findByWcSid(WcSid);

        if (WkStringUtils.isEmpty(managerSignInfos)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾
        // ====================================
        // 簽核資訊不顯示刪除
        List<BpmStatus> statusfilters = Lists.newArrayList(
                BpmStatus.DELETE);

        return managerSignInfos.stream()
                .filter(signInfo -> !statusfilters.contains(signInfo.getStatus()))
                .collect(Collectors.toList());
    }

    /**
     * 回傳正常的需求方申請單位流程
     *
     * @param wcSid
     * @return
     * @throws SystemOperationException
     */
    public Optional<WCManagerSignInfo> findReqFlowSignInfo(String wcSid) throws SystemOperationException {
        return this.findReqFlowSignInfo(wcSid, false);
    }

    /**
     * @param wcSid
     * @return
     * @throws SystemOperationException
     */
    public Optional<WCManagerSignInfo> findReqFlowSignInfoIncludeInvalid(String wcSid) throws SystemOperationException {
        return this.findReqFlowSignInfo(wcSid, true);
    }

    /**
     * 取得需求單位簽核流程
     *
     * @param wcSid
     * @return
     * @throws SystemOperationException
     */
    private Optional<WCManagerSignInfo> findReqFlowSignInfo(String wcSid, boolean isIncludeInvalid) throws SystemOperationException {
        // ====================================
        // 查詢
        // ====================================
        List<WCManagerSignInfo> activeReqFlowSignInfos = Lists.newArrayList();

        if (isIncludeInvalid) {
            // 包含作廢
            Set<BpmStatus> inActiveBpmStatus = Sets.newHashSet(BpmStatus.DELETE, BpmStatus.WAIT_CREATE);
            activeReqFlowSignInfos = this.findReqFlowSignInfosByWithoutStatus(wcSid, inActiveBpmStatus);
        } else {
            // 不包含作廢
            activeReqFlowSignInfos = this.findActiveReqFlowSignInfos(wcSid);
        }

        if (WkStringUtils.isEmpty(activeReqFlowSignInfos)) {
            return Optional.empty();
        }

        // ====================================
        // 過濾狀態
        // ====================================
        // 過濾
        List<WCManagerSignInfo> activeSignInfos = activeReqFlowSignInfos.stream()
                // 過濾需為『需求單位簽核流程』
                .filter(signInfo -> SignType.SIGN.equals(signInfo.getSignType()))
                .collect(Collectors.toList());

        // ====================================
        // 回傳
        // ====================================
        // 為空
        if (WkStringUtils.isEmpty(activeSignInfos)) {
            return Optional.empty();
        }

        // 檢核資料錯誤
        if (activeSignInfos.size() > 1) {
            log.error("資料錯誤，需求單位簽核流程有多筆! wcsid:[{}]", wcSid);
            throw new SystemOperationException(WkMessage.EXECTION, InfomationLevel.ERROR);
        }

        // 回傳
        return activeSignInfos.stream().findFirst();

    }

    /**
     * 取得需求單位會簽流程
     *
     * @param wcSid
     * @return
     * @throws SystemOperationException
     */
    public List<WCManagerSignInfo> findReqFlowCounterSignInfos(String wcSid) {
        // ====================================
        // 查詢
        // ====================================
        List<WCManagerSignInfo> activeReqFlowSignInfos = this.findActiveReqFlowSignInfos(wcSid);

        if (WkStringUtils.isEmpty(activeReqFlowSignInfos)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾需為『需求單位簽核流程』
        // ====================================
        return activeReqFlowSignInfos.stream()
                .filter(signInfo -> SignType.COUNTERSIGN.equals(signInfo.getSignType()))
                .collect(Collectors.toList());

    }

}
