/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author allen1214_wu
 */
@Service
public class PermissionLogicForExecDep implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -9064357082700131535L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static PermissionLogicForExecDep instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static PermissionLogicForExecDep getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(PermissionLogicForExecDep instance) { PermissionLogicForExecDep.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient WCExecDepManager wcExecDepManager;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    @Autowired
    private WCExecManagerSignInfoCheckHelper wcExecManagerSignInfoCheckHelper;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 執行單位可閱人員
     * 
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public boolean isCanViewByExecDep(
            String wcSid,
            Integer loginUserSid) {
        return this.isExecDepRelationUser(wcSid, loginUserSid, null);
    }

    /**
     * 執行單位相關人員- 是否可進行回覆
     * 
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public boolean isCanReplyByExecDep(
            String wcSid,
            Integer loginUserSid) {

        Set<WCExceDepStatus> withoutWCExceDepStatus = Sets.newHashSet(
                WCExceDepStatus.CLOSE,
                WCExceDepStatus.STOP);

        return this.isExecDepRelationUser(wcSid, loginUserSid, withoutWCExceDepStatus);
    }

    /**
     * 取得可進行『執行完成』的執行單位檔
     * 
     * @param wcSid        主單 sid
     * @param loginUserSid 登入者 sid
     * @return WCExceDep list
     */
    public List<WCExceDep> findCanExecFinish(String wcSid, Integer loginUserSid) {

        // ====================================
        // 取得單據執行單位
        // ====================================
        // 查詢執行單位檔
        List<WCExceDep> wcExceDeps = this.wcExecDepManager.findActiveByWcSid(wcSid);
        // 留下『進行中』的執行單位
        wcExceDeps = wcExceDeps.stream()
                .filter(wcExceDep -> WCExceDepStatus.PROCEDD.equals(wcExceDep.getExecDepStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得登入者管理的單位 (不含子單位)
        // ====================================
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(loginUserSid);

        // ====================================
        // 『排除』執行單位狀態
        // ====================================
        List<WCExceDep> canExecFinishExecDeps = Lists.newArrayList();
        for (WCExceDep wcExceDep : wcExceDeps) {

            // 為執行單位-執行人員
            if (WkCommonUtils.compareByStr(wcExceDep.getExecUserSid(), loginUserSid)) {
                canExecFinishExecDeps.add(wcExceDep);
            }

            // 為執行單位主管 (不含上層主管)
            if (managerOrgSids.contains(wcExceDep.getDep_sid())) {
                canExecFinishExecDeps.add(wcExceDep);
            }
        }
        return canExecFinishExecDeps;
    }

    /**
     * 判斷使用者是否可加派
     * 
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public boolean isCanAddExecDep(String wcSid, Integer loginUserSid) {

        // ====================================
        // 取得單據執行單位
        // ====================================
        // 查詢執行單位檔
        List<WCExceDep> wcExceDeps = this.wcExecDepManager.findActiveByWcSid(wcSid);

        // ====================================
        // 僅領單人員可閱，則不支援加派功能。
        // ====================================
        // 有任一執行單位為『僅領單人員可閱』則不支援加派功能
        Set<Integer> receviceUserSids = ExecDepLogic.getInstance().findOnlyReceviceUserSidsByAllExecDep(wcExceDeps);
        if (WkStringUtils.notEmpty(receviceUserSids)) {
            return false;
        }

        // ====================================
        // 執行單位狀態過濾
        // ====================================
        // 留下『進行中、執行完成』的執行單位
        List<WCExceDepStatus> status = Lists.newArrayList(
                WCExceDepStatus.PROCEDD,
                WCExceDepStatus.FINISH);

        wcExceDeps = wcExceDeps.stream()
                .filter(wcExceDep -> status.contains(wcExceDep.getExecDepStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return false;
        }

        // ====================================
        // 執行方簽核流程人員 (已簽或待簽)
        // ====================================
        boolean isExecDepFlowMainSignedUser = SignFlowForExecDepLogic.getInstance().isExecDepFlowMainSignedUser(
                wcSid, loginUserSid);
        if (isExecDepFlowMainSignedUser) {
            return true;
        }

        // ====================================
        // 逐筆判斷
        // ====================================
        for (WCExceDep wcExceDep : wcExceDeps) {
            if (this.isExecDepRelationUser(wcExceDep, loginUserSid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判斷使用者是否可會簽
     * 
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public boolean isCanCountersign(String wcSid, Integer loginUserSid) {

        // ====================================
        // 取得單據執行單位
        // ====================================
        // 查詢執行單位檔
        List<WCExceDep> wcExceDeps = this.wcExecDepManager.findActiveByWcSid(wcSid);

        // ====================================
        // 逐筆判斷
        // ====================================
        for (WCExceDep wcExceDep : wcExceDeps) {
            if (this.isExecDepRelationUser(wcExceDep, loginUserSid)) {
                return true;
            }
        }
        return false;
    }

    // ========================================================================
    // 內部方法
    // ========================================================================
    /**
     * 是否擁有執行方檢視權限
     *
     * @param wcSid        工作聯絡單Sid
     * @param loginUserSid 登入者
     * @return
     */
    public boolean isExecDepRelationUser(
            String wcSid,
            Integer loginUserSid,
            Set<WCExceDepStatus> withoutWCExceDepStatus) {

        if (wcSid == null) {
            return false;
        }

        // ====================================
        // 取得單據執行單位
        // ====================================
        List<WCExceDep> wcExceDeps = this.wcExecDepManager.findActiveByWcSid(wcSid);
        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return false;
        }

        // ====================================
        // 『排除』執行單位狀態
        // ====================================
        if (WkStringUtils.notEmpty(withoutWCExceDepStatus)) {
            wcExceDeps = wcExceDeps.stream()
                    // 留下不為 withoutWCExceDepStatus 者
                    .filter(wcExceDep -> !withoutWCExceDepStatus.contains(wcExceDep.getExecDepStatus()))
                    .collect(Collectors.toList());

            if (WkStringUtils.isEmpty(wcExceDeps)) {
                return false;
            }
        }

        // ====================================
        // 逐筆判斷執行單位資料
        // ====================================
        for (WCExceDep wcExceDep : wcExceDeps) {
            if (this.isExecDepRelationUser(wcExceDep, loginUserSid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否為執行部門關係人員
     * 
     * @param wcExceDep
     * @param loginUserSid
     * @return
     */
    private boolean isExecDepRelationUser(WCExceDep wcExceDep, Integer loginUserSid) {
        // ====================================
        // 為執行人員
        // ====================================
        if (WkCommonUtils.compareByStr(loginUserSid, wcExceDep.getExecUserSid())) {
            return true;
        }

        switch (wcExceDep.getExecDepStatus()) {
        case APPROVING:
            // 僅簽核人員(已簽、待簽)可閱，在外部判斷
            return false;

        case WAITSEND:
            // 1.簽核人員(已簽、待簽)可閱，在外部判斷
            // 2.可派工人員
            if (ExecDepLogic.getInstance().isCanAssignUser(wcExceDep, loginUserSid)) {
                return true;
            }
            return false;

        case WAITRECEIVCE:
            // 僅簽核人員(已簽、待簽)可閱，在外部判斷
            return false;

        case PROCEDD:
        case STOP:
        case FINISH:
        case CLOSE:

            // ====================================
            // 先判斷僅領單人員可閱
            // ====================================
            // 取得可閱的領單人員名單
            Set<Integer> canViewReceviceUserSids = wcExceDep.getCanViewReceviceUserSids();
            // 名單為空時，代表此單據不是僅領單人員可閱，不進入判斷
            if (WkStringUtils.notEmpty(canViewReceviceUserSids)) {

                // WORKCOMMU-438 有設定《僅領單人員可閱》時，該執行單位僅剩領單人員可閱；以下單位不可閱；直屬上層單位可閱。
                // 1.判斷為領單人員
                if (canViewReceviceUserSids.contains(loginUserSid)) {
                    return true;
                }

                // 及直屬上層單位主管可閱
                // 查詢使用者所有管理的單位, 包含管理單位子部門
                Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);
                return managerOrgSids.contains(wcExceDep.getDep_sid());
            }

            // ====================================
            // 相關單位人員
            // ====================================
            // 相關單位取得有快取
            Set<Integer> canViewOrgSids = RelationDepHelper.findRelationOrgSidsForExecCanView(loginUserSid);
            if (canViewOrgSids.contains(wcExceDep.getDep_sid())) {
                return true;
            }

            // ====================================
            // 判斷為單位的派工人員
            // ====================================
            if (ExecDepLogic.getInstance().isCanAssignUser(wcExceDep, loginUserSid)) {
                return true;
            }
        default:
            break;
        }

        return false;
    }
    
    // ========================================================================
    // 執行方-簽核人員不執行
    // ========================================================================
    /**
     * 是否可執行『執行方-簽核人員不執行』
     * 
     * @param wcSid    主單SID
     * @param execUser 執行者
     * @return true:可執行
     * @throws UserMessageException 資料錯誤時拋出
     */
    public boolean isCanExecFlowSignerStop(String wcSid, User execUser) throws UserMessageException {

        // 查詢所有執行者可簽的執行方簽核流程 (主流程)
        List<WCExecManagerSignInfo> canSignExecFlowSignInfos = this.wcExecManagerSignInfoManager.findCanSignExecFlowSignInfos(
                wcSid, 
                execUser.getId());

        // 判斷
        return this.isCanExecFlowSignerStop(wcSid, canSignExecFlowSignInfos);

    }

    public boolean isCanExecFlowSignerStop(String wcSid, List<WCExecManagerSignInfo> selfCanSignExecFlowSignInfos) throws UserMessageException {
        // ====================================
        // 檢查自己為待簽人員
        // ====================================
        if (WkStringUtils.isEmpty(selfCanSignExecFlowSignInfos)) {
            return false;
        }

        // ====================================
        // 若可『退回』執行方，則不可使用『不執行』
        // ====================================
        // 檢核『執行方狀態』是否允許退回需求方
        // 不能有其他執行方已簽完或已執行
        boolean isCanRollBackToReq = this.wcExecManagerSignInfoCheckHelper.isCanRollBackToReq(wcSid);
        if (isCanRollBackToReq) {
            return false;
        }

        return true;
    }

}
