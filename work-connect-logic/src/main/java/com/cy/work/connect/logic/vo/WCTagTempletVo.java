/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import com.cy.work.connect.logic.manager.WCTagTempletManager;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTagTemplet;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

/**
 * @author jimmy_chou
 */
public class WCTagTempletVo extends WCTagTemplet implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5492771976478369588L;

    /**
     * 類別
     */
    @Getter
    private String categoryName;

    /**
     * 單據名稱
     */
    @Getter
    private String menuName;

    /**
     * 單據名稱VO
     */
    @Getter
    @Setter
    private MenuTagVO menuNameVO;

    /**
     * 建構子
     *
     * @param tagTemplet
     */
    public WCTagTempletVo(WCTagTemplet tagTemplet) {
        // 複製所有欄位屬性
        BeanUtils.copyProperties(tagTemplet, this);

        String[] tagNames =
            WCTagTempletManager.getInstance()
                .findHierarchyTagNameByWCMenuTagSid(this.getMenuTagSid());

        if (tagNames != null) {
            this.categoryName = tagNames[0];
            this.menuName = tagNames[1];
        }

        WCMenuTag menuTag =
            WCTagTempletManager.getInstance().findMenuTagByWCMenuTagSid(this.getMenuTagSid());
        if (menuTag != null) {
            this.menuNameVO =
                new MenuTagVO(
                    menuTag.getSid(),
                    menuTag.getMenuName(),
                    WCTagTempletManager.getInstance()
                        .findCategoryTagByWCCategoryTagSid(menuTag.getWcCategoryTagSid())
                        .getCategoryName(),
                    "");
        }
    }

    /**
     * 建構子
     */
    public WCTagTempletVo() {
    }
}
