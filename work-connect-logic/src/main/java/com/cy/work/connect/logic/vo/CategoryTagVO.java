/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * 類別大類
 *
 * @author brain0925_liao
 */
@EqualsAndHashCode(of = {"sid"})
public class CategoryTagVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6414155440482019309L;

    @Getter
    /** sid */
    private final String sid;

    @Getter
    /** 大類名稱 */
    private final String name;

    @Getter
    /** 該大類下的中類 (單據名稱) */
    private List<MenuTagVO> menuTagVOs = Lists.newArrayList();

    public CategoryTagVO(String sid, String name, List<MenuTagVO> menuTagVOs) {
        this.sid = sid;
        this.name = name;
        this.menuTagVOs = menuTagVOs;
    }
}
