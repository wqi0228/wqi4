/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.work.connect.logic.cache.WorkParamCache;
import com.cy.work.connect.vo.enums.WorkParamEnum;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 特殊參數
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WorkParamManager implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1152723867370652072L;
    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WorkParamManager instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static WorkParamManager getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WorkParamManager instance) { WorkParamManager.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    @Autowired
    private WorkParamCache workParamCache;

    /**
     * 取得特殊參數 By keyword
     *
     * @param keyword
     * @return String
     */
    public String findByKeyword(String keyword) {
        String result = workParamCache.findContentByKeyword(keyword);
        if (result == null) {
            result = "";
        }
        return result;
    }

    /**
     * 取得特殊參數 By keyword
     *
     * @param keyword
     * @return List Integer
     */
    public List<Integer> findIntListByKeyword(String keyword) {
        List<Integer> result = Lists.newArrayList();
        try {
            String[] param = workParamCache.findContentByKeyword(keyword).split(",");
            for (String each : param) {
                try {
                    result.add(Integer.valueOf(each));
                } catch (Exception e) {
                    log.warn("work param 轉型 Integer失敗 keyword = " + keyword, e);
                }
            }
        } catch (Exception e) {
            log.warn("findIntListByKeyword", e);
        }
        return result;
    }

    /**
     * 取得特殊參數 By keyword
     *
     * @param keyword
     * @return double
     */
    private Double findDoubleByKeyword(String keyword) {
        Double result = null;
        try {
            String param = workParamCache.findContentByKeyword(keyword);
            try {
                result = Double.valueOf(param);
            } catch (Exception e) {
                log.warn("work param 轉型 Double失敗 keyword = " + keyword, e);
            }
        } catch (Exception e) {
            log.warn("findDoubleByKeyword", e);
        }
        return result;
    }

    public Double findRestResponseTimeBound() {
        Double restResponseTimeBound = this.findDoubleByKeyword(WorkParamEnum.REST_RESPONSE_TIME_BOUND.name());
        //未設定時預設5秒
        if (restResponseTimeBound == null) {
            restResponseTimeBound = 5000.0;
        }
        return restResponseTimeBound;
    }

    /**
     * @return
     */
    public boolean isSHOW_DEBUG_FOR_WAIT_ASSIGN() { return "Y".equals(this.findByKeyword(WorkParamEnum.SHOW_DEBUG_FOR_WAIT_ASSIGN.getVal())); }

    public boolean isShowSearchSql() { return "Y".equals(this.findByKeyword(WorkParamEnum.SHOW_SEARCH_SQL.name())); }

    /**
     * 是否為 BigBoss
     * 
     * @param userSid
     * @return
     */
    public boolean isBigBoss(Integer userSid) {
        if (userSid == null) {
            return false;
        }
        List<Integer> bigBossSids = this.findIntListByKeyword(WorkParamEnum.BIG_BOSS_SIDS.name());
        return bigBossSids.contains(userSid);
    }

}
