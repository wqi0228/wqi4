/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.query;

import com.cy.commons.vo.User;
import java.util.List;
import java.util.Map;

/**
 * @author brain0925_liao
 */
public interface QueryService<T> {

    List<T> findWithQuery(String sql,
        Map<String, Object> parameters,
        User executor);
}
