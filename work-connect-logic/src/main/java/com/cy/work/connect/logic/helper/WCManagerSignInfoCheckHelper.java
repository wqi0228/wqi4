/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WCManagerSignInfoCheckHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2170224205624841204L;

    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;

    /**
     * 檢測需求單位流程是否已核准,並且回傳需求單位流程物件
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public SingleManagerSignInfo getApprovedReqFlow(String wcSid) {
        SingleManagerSignInfo reqFlow = this.getReqFlow(wcSid);
        Preconditions.checkState(reqFlow.getStatus().equals(BpmStatus.APPROVED), "需求單位非核准狀態，請確認！！");
        return reqFlow;
    }

    /**
     * 需求簽核流程是否全數簽核完畢(需求流程 & 需求會簽)
     * 
     * @param wcSid 主單 SID
     * @return 是否全數簽核完畢
     */
    public boolean isReqAllFinish(String wcSid) {

        List<WCManagerSignInfo> activeManagerSignInfos = this.wcManagerSignInfoManager.findAllActiveFlowSignInfos(wcSid);

        // ====================================
        // 逐筆判斷，有任一筆未完成，即回傳
        // ====================================
        for (WCManagerSignInfo wcManagerSignInfo : activeManagerSignInfos) {
            // 有效流程中，有任何一筆狀態為 APPROVED , 救代表還沒簽完
            if (!BpmStatus.APPROVED.equals(wcManagerSignInfo.getStatus())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 需求簽核流程是否簽核完畢
     *
     * @param wcSid 主單SID
     * @return 是否簽核完畢
     */
    public boolean isReqFlowFinish(String wcSid) {

        List<WCManagerSignInfo> activeManagerSignInfos = this.wcManagerSignInfoManager.findAllActiveFlowSignInfos(wcSid);

        // ====================================
        // 逐筆判斷，有任一筆未完成，即回傳
        // ====================================
        for (WCManagerSignInfo wcManagerSignInfo : activeManagerSignInfos) {

            if (SignType.SIGN.equals(wcManagerSignInfo.getSignType())) {
                // 若是預設的需求單位流程,若為作廢代表流程未完成,
                if (!(BpmStatus.APPROVED.equals(wcManagerSignInfo.getStatus())
                        || BpmStatus.CLOSED.equals(wcManagerSignInfo.getStatus()))) {
                    return false;
                }
            }

        }
        return true;
    }

    /**
     * 取得需求流程物件
     *
     * @param wcSid
     * @return
     */
    public SingleManagerSignInfo getReqFlow(String wcSid) {
        List<SingleManagerSignInfo> flowReqs = wcManagerSignInfoManager.getFlowSignInfos(wcSid);
        SingleManagerSignInfo reqFlow = flowReqs.get(0);
        Preconditions.checkState(!BpmStatus.DELETE.equals(reqFlow.getStatus()), "聯絡單已被作廢 請確認!!");
        return reqFlow;
    }
}
