/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.utils;

import com.google.common.base.Strings;
import java.io.Serializable;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class ReqularPattenUtils implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 56779005095409470L;
    //    /**
    //     * 覆蓋SQL Query Like的非法字元
    //     *
    //     * @param str
    //     * @return
    //     */
    //    public String replaceIllegalSqlLikeStr(String str) {
    //        return str.replaceAll(ReqularPattenUtils.REPLACE_FUZZY_PATTEN, "'$1$2'");
    //    }
    private static final String REPLACE_FUZZY_PATTEN = "(%)+|(_)";
    private static final String HELF_EMPTY = " ";
    private static final String FULL_EMPTY = "　";
    private static final String PERCENT = "%";
    private static final String BACK_SLASH = "\\\\";
    private static final String REPLACE_CHAR = "_";

    /**
     * 覆蓋SQL Query Like的非法字元
     *
     * @param str
     * @return
     */
    public String replaceIllegalSqlLikeStr(String str) {
        if (Strings.isNullOrEmpty(str)) {
            return "";
        }
        if (str.length() == 1 || this.isAllSameSpChar(str)) {
            return this.replaceSpStr(str);
        }
        String replaceHelfEmpty = str.replaceAll(HELF_EMPTY, REPLACE_CHAR);
        String replaceFullEmpty = replaceHelfEmpty.replaceAll(FULL_EMPTY, REPLACE_CHAR);
        String replacePercent = replaceFullEmpty.replaceAll(PERCENT, REPLACE_CHAR);
        String replaceBackslash = replacePercent.replaceAll(BACK_SLASH, PERCENT);
        return replaceBackslash;
    }

    private String replaceSpStr(String str) {
        if (str.contains("\\")) {
            return str.replaceAll("\\\\", "\\\\\\\\");
        }
        return str.replaceAll(ReqularPattenUtils.REPLACE_FUZZY_PATTEN, "\\\\$1$2");
    }

    /**
     * 檢測是否全部為相同字元
     *
     * @param str
     * @return
     */
    private boolean isAllSameSpChar(String str) {
        if (!str.contains("%") && !str.contains("_") && !str.contains("\\")) {
            return false;
        }
        char[] charArray = str.toCharArray();
        char prevC = 0;
        for (char c : charArray) {
            if (prevC == 0) {
                prevC = c;
                continue;
            }
            if (prevC != c) {
                return false;
            }
            prevC = c;
        }
        return true;
    }
}
