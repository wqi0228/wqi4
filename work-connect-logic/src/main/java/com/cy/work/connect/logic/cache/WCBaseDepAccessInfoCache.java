/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCBaseDepAccessInfoRepository;
import com.cy.work.connect.vo.WCBaseDepAccessInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 可閱讀部門權限Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCBaseDepAccessInfoCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1222223368405804726L;
    /**
     * WCBaseDepAccessInfoRepository
     */
    @Autowired
    private WCBaseDepAccessInfoRepository wcBaseDepAccessInfoRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WCBaseDepAccessInfo> wcBaseDepAccessInfoMap;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    public WCBaseDepAccessInfoCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcBaseDepAccessInfoMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay = ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立可閱讀部門權限Cache");
        try {
            Map<String, WCBaseDepAccessInfo> tempWCBaseDepAccessInfoMap = Maps.newConcurrentMap();
            wcBaseDepAccessInfoRepository
                .findAll()
                .forEach(
                    item -> {
                        tempWCBaseDepAccessInfoMap.put(item.getSid(), item);
                    });
            wcBaseDepAccessInfoMap = tempWCBaseDepAccessInfoMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立可閱讀部門權限Cache結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新Cache資料
     */
    public synchronized void updateCacheBySid(WCBaseDepAccessInfo wcBaseDepAccessInfo) {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立可閱讀部門權限Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立可閱讀部門權限Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
        wcBaseDepAccessInfoMap.put(wcBaseDepAccessInfo.getSid(), wcBaseDepAccessInfo);
    }

    /**
     * 取得可閱讀部門權限 By Sid
     *
     * @param sid 可閱讀部門權限Sid
     * @return
     */
    public WCBaseDepAccessInfo findBySid(String sid) {
        return wcBaseDepAccessInfoMap.get(sid);
    }

    /**
     * 取得可閱讀部門權限 By 部門Sid
     *
     * @param depSid 部門Sid
     * @return
     */
    public WCBaseDepAccessInfo getByDepSid(Integer depSid) {
        for (WCBaseDepAccessInfo wi : Lists.newArrayList(wcBaseDepAccessInfoMap.values())) {
            if (wi.getLoginUserDepSid().equals(depSid)) {
                return wi;
            }
        }
        return null;
    }

    /**
     * 取得單位可閱權限增加(聯集)已設定單位
     *
     * @return
     */
    public List<Integer> getSettingOtherBaseOrgSids() {
        List<Integer> result = Lists.newArrayList();
        for (WCBaseDepAccessInfo wi : Lists.newArrayList(wcBaseDepAccessInfoMap.values())) {
            if (WkStringUtils.notEmpty(wi.getOtherBaseAccessViewDep())) {
                result.add(wi.getLoginUserDepSid());
            }
        }
        return result;
    }
}
