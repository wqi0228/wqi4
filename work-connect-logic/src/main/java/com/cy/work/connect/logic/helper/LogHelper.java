/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkUserUtils;
import java.io.Serializable;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class LogHelper implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4720003724853697049L;

    private static LogHelper instance;
    /**
     * 使用者Manager
     */
    @Autowired
    private WkUserCache userManager;

    public static LogHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LogHelper.instance = this;
    }

    public void addLogInfo(Integer loginUserSid, String message) {
        try {
            log.debug("執行者[" + WkUserUtils.findNameBySid(loginUserSid) + "]訊息[" + message + "]");
        } catch (Exception e) {
            log.warn("addLogInfo ERROR", e);
        }
    }

    public void addLogInfo(String wcSid, Integer loginUserSid, String message) {
        try {
            log.debug(
                "工作聯絡單SID["
                    + wcSid
                    + "]執行者["
                    + WkUserUtils.findNameBySid(loginUserSid)
                    + "]訊息["
                    + message
                    + "]");
        } catch (Exception e) {
            log.warn("addLogInfo ERROR", e);
        }
    }

    public void addLogInfo(String wcSid, String loginUserID, String message) {
        try {
            log.debug(
                "工作聯絡單SID["
                    + wcSid
                    + "]執行者["
                    + Optional.ofNullable(userManager.findById(loginUserID))
                    .map(each -> each.getName())
                    .orElseGet(() -> "")
                    + "]訊息["
                    + message
                    + "]");
        } catch (Exception e) {
            log.warn("addLogInfo ERROR", e);
        }
    }
}
