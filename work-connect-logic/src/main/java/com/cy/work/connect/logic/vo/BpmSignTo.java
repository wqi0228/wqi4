/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import com.cy.bpm.rest.vo.client.ISign;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

/**
 * BPM 簽核 所需自定物件
 *
 * @author kasim
 */
public class BpmSignTo implements ISign {

    /**
     *
     */
    private static final long serialVersionUID = 6027123238154372474L;

    @Getter
    private final String instanceId;
    @Getter
    private final String executorId;
    @Getter
    private final String comment;
    @Getter
    private final Map<String, Object> parameter;

    public BpmSignTo(String userId, String bpmId) {
        this.executorId = userId;
        this.instanceId = bpmId;
        this.comment = "";
        this.parameter = new HashMap<>();
    }
}
