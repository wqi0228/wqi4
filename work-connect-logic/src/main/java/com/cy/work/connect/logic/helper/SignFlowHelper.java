/**
 *
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.vo.BpmRoleTo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.FlowType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SignFlowHelper implements Serializable, InitializingBean {

    /**
     *
     */
    private static final long serialVersionUID = 3326750341075148475L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SignFlowHelper instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static SignFlowHelper getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(SignFlowHelper instance) { SignFlowHelper.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private BpmManager bpmManager;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;

    @Autowired
    private BrBpmLevelSettingHelper brBpmLevelSettingHelper;

    // ========================================================================
    // 外部介面
    // ========================================================================
    public Map<String, Object> prepareModelerParamsForApplyFlow(WCMaster wcMaster, Integer execUserSid) throws UserMessageException {
        // ====================================
        // 防呆
        // ====================================
        if (wcMaster == null
                || WkStringUtils.isEmpty(wcMaster.getMenuTagSid())
                || wcMaster.getCategoryTagTo() == null
                || WkStringUtils.isEmpty(wcMaster.getCategoryTagTo().getTagSids())) {

            String errorMessage = WkMessage.NEED_RELOAD + "(資料錯誤)";
            log.error(errorMessage + " wcsid:[{}]", wcMaster != null ? wcMaster.getSid() : "傳入wcMaster 為空");
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 取得中類資料檔
        // ====================================
        WCMenuTag wcMenuTag = this.wcMenuTagManager.getWCMenuTagBySid(wcMaster.getMenuTagSid());
        // 防呆
        if (WkStringUtils.isEmpty(wcMenuTag)) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到對應的中類資料)";
            log.error(errorMessage + "wc_menu_tag.sid :[{}]", wcMaster.getMenuTagSid());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 取得小類資料檔
        // ====================================
        // 查詢
        List<WCTag> wcTags = this.wcTagManager.findBySidIn(wcMaster.getCategoryTagTo().getTagSids());

        // 防呆
        if (WkStringUtils.isEmpty(wcTags)) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到對應的小類資料)";
            log.error(errorMessage + "單據無執行項目(小類)資料 (wc_master.category_tag) wcsid:[{}]", wcMaster.getCategoryTagTo().getTagSids());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 依據選擇的項目判斷簽核層級
        // ====================================
        // 取得PPS6 簽核層級 BY compId
        Org comp = WkOrgCache.getInstance().findBySid(wcMaster.getComp_sid());
        Map<FlowType, Integer> bpmLevelMapByFlowType = Maps.newConcurrentMap();
        try {
            bpmLevelMapByFlowType = brBpmLevelSettingHelper.getFlowTypeBpmLevelMapByCompId(comp.getId());
            //log.debug("bpmLevelMapByFlowType:\r\n" + WkJsonUtils.getInstance().toPettyJson(bpmLevelMapByFlowType));

        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(取得PPS6簽核層級失敗)";
            log.error(errorMessage + "[" + e.getMessage() + "]", e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // 依據『執行項目』SID 取得建單需求流程類型 (各類別設定不同時，以優先序高者為準)
        FlowType topFlowType = this.wcManagerSignInfoManager.findTopFlowType(wcTags, bpmLevelMapByFlowType);
        if (topFlowType == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(項目簽核層級未設定)";
            log.error(errorMessage + " wcsid:[{}]", wcMaster.getSid());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }
        log.info("計算後，項目最高簽核層級:[{}]", topFlowType.getDesc());

        // ====================================
        // 準備BPM流程 參數
        // ====================================
        // 查詢由登入者開始往上的簽核流程角色
        // ex: allen -> yi-fan -> linus -> aon_lin -> jk
        List<BpmRoleTo> execUserSignFlowRoles = this.bpmManager.prepareUserSignFlow(execUserSid);

        // 判斷最高簽核層級
        BpmRoleTo signFlowTopRole = this.prepareModelerParamsForApplyFlow_SignLv(
                execUserSid,
                topFlowType,
                wcMenuTag.isParentManager(),
                execUserSignFlowRoles, bpmLevelMapByFlowType);

        // 組簽核參數
        String sign_lv = "";
        BpmRoleTo execUserRoleTo = execUserSignFlowRoles.get(0);
        // 當最高簽核節點不是自己時，指定層簽參數
        if (!WkCommonUtils.compareByStr(execUserRoleTo.getId(), signFlowTopRole.getId())) {
            sign_lv = "預設路線-最高至層級" + signFlowTopRole.getRoleLevel();
        }

        // 需求方最終簽核人員
        String finalApplyUserID = this.prepareModelerParamsForApplyFlow_finalApplyUserID(
                wcMenuTag,
                wcTags,
                execUserSignFlowRoles,
                signFlowTopRole.getId());

        Map<String, Object> parameters = Maps.newLinkedHashMap();
        parameters.put("sign_lv", sign_lv);
        parameters.put("finalApplyUserID", finalApplyUserID);

        if (WkStringUtils.notEmpty(sign_lv)) {
            parameters.put("log-最高層簽角色", signFlowTopRole.getId());
            parameters.put("log-最高層簽人員", signFlowTopRole.getRoleUserIds());
        }
        log.info("流程參數\r\n" + WkJsonUtils.getInstance().toPettyJson(parameters));

        return parameters;
    }

    /**
     * 準備BPM流程參數 ：最高簽核層級
     *
     * @param execUserSid            執行者
     * @param topFlowType            最高簽核層級
     * @param isForceUpParent        是否強制簽到上層
     * @param loginUserSignFlowRoles 簽核流程角色 list
     * @param bpmLevelByFlowType     簽核層級ByFlowTypeMap
     * @return 最高簽核角色 (為自己簽核時，會回傳第一個 role)
     * @throws UserMessageException 錯誤時拋出
     */
    private BpmRoleTo prepareModelerParamsForApplyFlow_SignLv(
            Integer execUserSid,
            FlowType topFlowType,
            Boolean isForceUpParent,
            List<BpmRoleTo> loginUserSignFlowRoles,
            Map<FlowType, Integer> bpmLevelMapByFlowType) throws UserMessageException {

        // log.info("topFlowType:[{}]" , topFlowType.getBpmLevel());
        // ====================================
        // 判斷層簽層級為自己
        // ====================================
        // 層級為自己 && 不用強制簽上層時，不用簽層簽
        if (FlowType.SELF.equals(topFlowType)
                && !isForceUpParent) {
            return loginUserSignFlowRoles.get(0);
        }

        // 層級為自己，但是需要往上簽一層
        if (FlowType.SELF.equals(topFlowType) && isForceUpParent) {
            // 整條流程上只有自己，不用再給上層簽了
            if (loginUserSignFlowRoles.size() <= 1) {
                return loginUserSignFlowRoles.get(0);
            }
        }

        // ====================================
        // 查詢使用者簽核流程
        // ====================================
        // 登入者角色
        String loginUserRoleID = loginUserSignFlowRoles.get(0).getId();

        BpmRoleTo topRoleTo = null;
        for (BpmRoleTo bpmRoleTo : loginUserSignFlowRoles) {

            // 檢核自己以上的簽核角色，的合理性
            if (!WkCommonUtils.compareByStr(loginUserRoleID, bpmRoleTo.getId())) {
                // 檢查已經沒人可以簽
                if (WkStringUtils.isEmpty(bpmRoleTo.getRoleUserIds())) {
                    String errorMessage = WkMessage.EXECTION + "(BPM 角色下已經沒有使用者![" + bpmRoleTo.getId() + " - " + bpmRoleTo.getName() + "]) ";
                    log.error(errorMessage);
                    throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
                }
                // 檢查可以簽的使用者大於1 (模擬兩可，不確定BPM會分給誰簽)
                if (WkStringUtils.isEmpty(bpmRoleTo.getRoleUserIds())) {
                    String errorMessage = WkMessage.EXECTION + "(BPM 角色下有多個使用者![" + bpmRoleTo.getId() + " - " + bpmRoleTo.getName() + "]) ";
                    log.error(errorMessage);
                    throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
                }
            }
            // 更新最高層級節點
            topRoleTo = bpmRoleTo;

            // WORKCOMMU-544 【簽核調整】帳號所在單位層級大於表單設定的層級，成員亦需簽核至上層主管
            // _Manager (忽略大小寫、空白)
            String compareRoleId = WkStringUtils.safeTrim(bpmRoleTo.getId()).toUpperCase();
            // log.info("compareRoleId:[{}],level:[{}]" , compareRoleId, bpmRoleTo.getRoleLevel());
            // 主副主管都會進入判斷
            if (compareRoleId.endsWith("_MANAGER")) {
                // 節點角色層級，已等於高於簽核層級時停止(數字小的為高層)
                Integer topFlowTypeLevel = bpmLevelMapByFlowType.get(topFlowType);
                if (bpmRoleTo.getRoleLevel() <= topFlowTypeLevel) {
                    break;
                }
            }
        }

        // ====================================
        // 判斷高簽核角色為自己
        // ====================================
        if (WkCommonUtils.compareByStr(loginUserRoleID, topRoleTo.getId())) {
            // 強制簽核到上層
            if (isForceUpParent) {
                // 若登入者上層有人，取得登入者上層節點
                if (loginUserSignFlowRoles.size() > 1) {
                    return loginUserSignFlowRoles.get(1);
                }
            }

            // 簽到自己，不用層簽
            return loginUserSignFlowRoles.get(0);
        }

        // ====================================
        // 回傳最高層級
        // ====================================
        return topRoleTo;
    }

    /**
     * 準備BPM流程參數 ：需求方最終簽核人員
     *
     * @param wcMenuTag             中類資料
     * @param selectedWcTags        選擇的小類
     * @param execUserSignFlowRoles 由執行者往上簽核的角色流程
     * @param signFlowTopRoleID     最高簽核角色 ID
     * @return 需求方最終簽核人員 userID (為空時代表不用簽)
     * @throws UserMessageException 錯誤時拋出
     */
    private String prepareModelerParamsForApplyFlow_finalApplyUserID(
            WCMenuTag wcMenuTag,
            List<WCTag> selectedWcTags,
            List<BpmRoleTo> execUserSignFlowRoles,
            String signFlowTopRoleID) throws UserMessageException {

        // ====================================
        // 未設定最終簽核人員
        // ====================================
        if (wcMenuTag.getReqFinalApplyUserSid() == null) {
            return "";
        }

        // ====================================
        // 檢查選擇項目有任何一個設定為『不簽核』
        // ====================================
        // 是否忽略「需求方最終簽核人員」設定
        for (WCTag wcTag : selectedWcTags) {
            if (wcTag.isReqFinalApplyIgnored()) {
                return "";
            }
        }

        // ====================================
        // 檢查設定人員
        // ====================================
        // 查詢
        User reqFinalApplyUser = WkUserCache.getInstance().findBySid(wcMenuTag.getReqFinalApplyUserSid());

        // 檢查資料合理性
        if (reqFinalApplyUser == null) {
            String errorMessage = WkMessage.EXECTION + "(最終簽核人員資料不存在) ";
            log.error(errorMessage + " userSid:[{}]", wcMenuTag.getReqFinalApplyUserSid());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        if (!WkUserUtils.isActive(reqFinalApplyUser)) {
            String errorMessage = WkMessage.EXECTION + "(最終簽核人員已停用)";
            log.error(errorMessage + " userSid:[{}]" + wcMenuTag.getReqFinalApplyUserSid());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 檢核最終簽核人員，是否曾在簽核流程出現過
        // ====================================
        List<String> reqFinalApplyUserRoles = Lists.newArrayList();
        try {
            reqFinalApplyUserRoles = this.bpmManager.findRoleFromUser(reqFinalApplyUser.getId());
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.EXECTION + "(查詢BPM角色失敗)";
            log.error(errorMessage + " userID:[{}]" + reqFinalApplyUser.getId());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        for (BpmRoleTo bpmRoleTo : execUserSignFlowRoles) {
            // 命中，不用簽
            if (reqFinalApplyUserRoles.contains(bpmRoleTo.getId())) {
                log.info("最終簽核人員[{}]已在流程中出現過，無需再次簽核", reqFinalApplyUser.getId());
                return "";
            }
            // 已達目標簽核層級，不再比對
            if (WkCommonUtils.compareByStr(signFlowTopRoleID, bpmRoleTo.getId())) {
                break;
            }
        }

        return reqFinalApplyUser.getId();

    }

}
