/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCHomepageFavoriteRepository;
import com.cy.work.connect.vo.WCHomepageFavorite;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 報表快選區
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCHomepageFavoriteManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6441493447593010184L;

    @Autowired
    private WCHomepageFavoriteRepository wcHomepageFavoriteRepository;

    /**
     * 取得報表快選區資料 By 登入者Sid
     *
     * @param loginUserSid 登入者Sid
     * @return
     */
    public WCHomepageFavorite getWCHomepageFavoriteByUserSid(Integer loginUserSid) {
        List<WCHomepageFavorite> wcHomepageFavorites =
            wcHomepageFavoriteRepository.findByUserSid(loginUserSid);
        if (wcHomepageFavorites != null && !wcHomepageFavorites.isEmpty()) {
            return wcHomepageFavorites.get(0);
        }
        return null;
    }

    /**
     * 建立報表快選區資料
     *
     * @param wcHomepageFavorite 報表快選區物件
     * @param createUserSid      建立者
     * @return
     */
    public WCHomepageFavorite create(WCHomepageFavorite wcHomepageFavorite, Integer createUserSid) {
        try {
            wcHomepageFavorite.setUsersid(createUserSid);
            wcHomepageFavorite.setUpdate_dt(new Date());
            WCHomepageFavorite wc = wcHomepageFavoriteRepository.save(wcHomepageFavorite);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 更新報表快選區資料
     *
     * @param wcHomepageFavorite 報表快選區物件
     * @param createUserSid      更新者
     * @return
     */
    public WCHomepageFavorite update(WCHomepageFavorite wcHomepageFavorite, Integer createUserSid) {
        try {
            wcHomepageFavorite.setUsersid(createUserSid);
            wcHomepageFavorite.setUpdate_dt(new Date());
            WCHomepageFavorite wc = wcHomepageFavoriteRepository.save(wcHomepageFavorite);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }
}
