/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCFunItemGroupRepository;
import com.cy.work.connect.vo.WCFunItemGroup;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 選單大項
 *
 * @author brain0925_liao
 */
@Slf4j
@Service
public class WCFunItemGroupManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 927764194537497473L;

    @Autowired
    private WCFunItemGroupRepository funItemGroupRepository;

    /**
     * 取得選單大項
     */
    public List<WCFunItemGroup> getFunItemGroupByCategoryModel() {
        try {
            String category_model = "WC";
            return funItemGroupRepository.getFunItemGroupByCategoryModel(category_model);
        } catch (Exception e) {
            log.warn("getFunItemGroupByCategoryModel Error : " + e, e);
            return Lists.newArrayList();
        }
    }
}
