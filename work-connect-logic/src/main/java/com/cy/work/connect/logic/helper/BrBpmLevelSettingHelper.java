/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.bpm.rest.client.BpmSettingClient;
import com.cy.bpm.rest.client.to.BrSignCompLevelSettingTo;
import com.cy.work.connect.vo.enums.FlowType;
import com.google.common.collect.Maps;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PPS6 層級 Helper
 *
 * @author Brain0925_Liao
 */
@Service
public class BrBpmLevelSettingHelper {

    /**
     * BPM特殊設定Client
     */
    @Autowired
    private BpmSettingClient bpmSettingClient;

    /**
     * 簽核層級至自己的固定層級
     */
    private final Integer FLOWTYPE_SELF_BPM_LEVEL = 999;

    /**
     * 簽核層級至執行長的固定層級
     */
    private final Integer FLOWTYPE_MANAGER_BPM_LEVEL = 1;

    /**
     * 取得 流程類型BPM簽核層級的 Map By CompId
     *
     * @param compId
     * @return
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public Map<FlowType, Integer> getFlowTypeBpmLevelMapByCompId(String compId) throws Exception {
        BrSignCompLevelSettingTo bt = bpmSettingClient.getSignCompLevelSetting(compId);
        Map<FlowType, Integer> bpmLevelMapByFlowType = Maps.newConcurrentMap();
        //僅需自己簽核
        bpmLevelMapByFlowType.put(FlowType.SELF, FLOWTYPE_SELF_BPM_LEVEL);
        //簽核至組級
        bpmLevelMapByFlowType.put(FlowType.GROUP, Integer.valueOf(bt.getPanelLevel()));
        //簽核至部級
        bpmLevelMapByFlowType.put(FlowType.DEPARTMENT, Integer.valueOf(bt.getMinisterialLevel()));
        //簽核至處級
        bpmLevelMapByFlowType.put(FlowType.OFFICE, Integer.valueOf(bt.getDivisionLevel()));
        //簽核至群級
        bpmLevelMapByFlowType.put(FlowType.BUSSINESSGROUP, Integer.valueOf(bt.getGroupLevel()));
        //執行長
        bpmLevelMapByFlowType.put(FlowType.MANAGER, FLOWTYPE_MANAGER_BPM_LEVEL);
        //以下不再使用
        //[壐識]簽核至總經理
        bpmLevelMapByFlowType.put(FlowType.XS_MANAGER, FLOWTYPE_MANAGER_BPM_LEVEL);
        //會員服務處特殊流程-簽核至部級
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_DEPARTMENT, Integer.valueOf(bt.getMinisterialLevel()));
        //會員服務處特殊流程-簽核至處級
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_OFFICE, Integer.valueOf(bt.getDivisionLevel()));
        //會員服務處特殊流程-簽核至群級
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_BUSSINESSGROUP, Integer.valueOf(bt.getGroupLevel()));
        //會員服務處特殊流程-執行長
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_MANAGER, FLOWTYPE_MANAGER_BPM_LEVEL);

        //會員服務處特殊流程-簽核至部級_XS
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_DEPARTMENT_XS, Integer.valueOf(bt.getMinisterialLevel()));
        //會員服務處特殊流程-簽核至處級_XS
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_OFFICE_XS, Integer.valueOf(bt.getDivisionLevel()));
        //會員服務處特殊流程-簽核至群級_XS
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_BUSSINESSGROUP_XS, Integer.valueOf(bt.getGroupLevel()));
        //會員服務處特殊流程-執行長_XS
        bpmLevelMapByFlowType.put(FlowType.SPECIAL_MANAGER_XS, FLOWTYPE_MANAGER_BPM_LEVEL);

        return bpmLevelMapByFlowType;
    }
}
