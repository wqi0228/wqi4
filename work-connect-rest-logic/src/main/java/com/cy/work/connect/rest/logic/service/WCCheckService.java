/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.work.connect.rest.logic.to.StatusResultTo;
import java.io.Serializable;
import java.util.Date;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author kasim
 */
@Component
public class WCCheckService implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7370704046971483316L;

    private static WCCheckService instance;
    @Autowired
    private WCAlertService wcAlertService;
    @Autowired
    private WCTodoService todoService;

    public static WCCheckService getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCCheckService.instance = this;
    }

    // private String restBaseUrl = FusionUrlServiceUtils.getUrlByPropKey("work-connect.rest.url");

    /**
     * 檢核 WCAlertController findByRecipientAndLimit
     *
     * @return
     */
    public StatusResultTo checkFindByRecipientAndLimit() {
        StatusResultTo to = null;
        String runningUrl =
            FusionUrlServiceUtils.getUrlByPropKey("work-connect.rest.url")
                + "/alert/findByRecipientAndLimit";
        try {
            wcAlertService.findByRecipientAndLimit(80, 1);
            to = new StatusResultTo(runningUrl + "-correct");
        } catch (Exception ex) {
            to = new StatusResultTo(Boolean.FALSE, runningUrl + "-failure-" + ex);
        }
        return to;
    }

    /**
     * 檢核 WCAlertController findByRecipient
     *
     * @return
     */
    public StatusResultTo checkFindByRecipient() {
        StatusResultTo to = null;
        String runningUrl =
            FusionUrlServiceUtils.getUrlByPropKey("work-connect.rest.url")
                + "/alert/findByRecipient";
        try {
            wcAlertService.findByRecipient(881);
            to = new StatusResultTo(runningUrl + "-correct");
        } catch (Exception ex) {
            to = new StatusResultTo(Boolean.FALSE, runningUrl + "-failure-" + ex);
        }
        return to;
    }

    /**
     * 檢核 WCAlertController findHistoryByRecipientAndBetweenCreateDate
     *
     * @return
     */
    public StatusResultTo checkFindHistoryByRecipientAndBetweenCreateDate() {
        StatusResultTo to = null;
        String runningUrl =
            FusionUrlServiceUtils.getUrlByPropKey("work-connect.rest.url")
                + "/alert/indHistoryByRecipientAndBetweenCreateDate";
        try {
            wcAlertService.findHistoryByRecipientAndBetweenCreateDate(881, new Date(), new Date());
            to = new StatusResultTo(runningUrl + "-correct");
        } catch (Exception ex) {
            to = new StatusResultTo(Boolean.FALSE, runningUrl + "-failure-" + ex);
        }
        return to;
    }

    /**
     * 檢核 WCTodoController findByDep
     *
     * @return
     */
    public StatusResultTo checkFindAllToDo() {
        StatusResultTo to = null;
        String runningUrl =
            FusionUrlServiceUtils.getUrlByPropKey("work-connect.rest.url") + "/todo/findAllToDo";
        try {
            todoService.findAllToDo(881);
            to = new StatusResultTo(runningUrl + "-correct");
        } catch (Exception ex) {
            to = new StatusResultTo(Boolean.FALSE, runningUrl + "-failure-" + ex);
        }
        return to;
    }
}
