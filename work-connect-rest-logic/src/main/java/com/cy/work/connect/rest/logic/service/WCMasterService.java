/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.work.connect.logic.manager.WCMasterCustomLogicManager;
import com.cy.work.connect.logic.manager.WCTraceManager;
import com.cy.work.connect.repository.WCMasterRepository;
import com.cy.work.connect.rest.logic.helper.WCTraceHelper;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.WCStatus;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCMasterService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5851168808099294671L;

    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;
    @Autowired
    private WCTraceHelper wcTraceHelper;

    @Autowired
    private WCMasterRepository wcMasterRepository;
    @Autowired
    private WCTraceManager wcTraceManager;

    @PersistenceContext
    private EntityManager em;

    public WCMaster getWCMasterBySid(String sid) {
        return wcMasterRepository.findOne(sid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToExecFinish(String sid, Integer userSid) {
        WCMaster wc = wcMasterRepository.findBySid(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為已執行完成");
        wcMasterCustomLogicManager.updateWCStatus(sid, userSid, WCStatus.EXEC_FINISH);
        em.refresh(wc);

        WCTrace wcTrace = wcTraceHelper.getWCTrace_EXEC_FINISH_STATUS(userSid, wc.getSid(), wc.getWc_no());
        this.wcTraceManager.create(wcTrace, userSid);

        this.wcMasterCustomLogicManager.updateWaitReadWCMaster(sid, userSid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToExec(String sid, Integer userSid) {
        WCMaster wc = wcMasterRepository.findOne(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為執行中");
        wcMasterCustomLogicManager.updateWCStatus(sid, userSid, WCStatus.EXEC);
        em.refresh(wc);
        this.wcMasterCustomLogicManager.updateWaitReadWCMaster(sid, userSid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToCloseStop(String sid, Integer userSid) {
        WCMaster wc = wcMasterRepository.findOne(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為結案(終止)");
        wcMasterCustomLogicManager.updateWCStatus(sid, userSid, WCStatus.CLOSE_STOP);
        em.refresh(wc);
        this.wcMasterCustomLogicManager.updateWaitReadWCMaster(sid, userSid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToClose(String sid, Integer userSid) {
        WCMaster wc = wcMasterRepository.findOne(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為結案");
        wcMasterCustomLogicManager.updateWCStatus(sid, userSid, WCStatus.CLOSE);
        em.refresh(wc);
        this.wcMasterCustomLogicManager.updateWaitReadWCMaster(sid, userSid);
    }

    /**
     * 更新工作聯絡單(更新修改時間及修改者)
     *
     * @param wcMaster      工作聯絡單物件
     * @param updateUserSid 更新者Sid
     * @return
     */
    public WCMaster updateWCMaster(WCMaster wcMaster, Integer updateUserSid) {
        wcMaster.setUpdate_usr(updateUserSid);
        wcMaster.setUpdate_dt(new Date());
        WCMaster wr = wcMasterRepository.save(wcMaster);
        return wr;
    }
}
