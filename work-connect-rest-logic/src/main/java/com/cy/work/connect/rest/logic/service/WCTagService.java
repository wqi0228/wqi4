/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import com.cy.work.connect.repository.WCTagRepository;
import com.cy.work.connect.rest.logic.common.Constants;
import com.cy.work.connect.rest.logic.common.SQLUtil;
import com.cy.work.connect.vo.WCTag;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCTagService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3285941682025122418L;

    @Autowired
    @Qualifier(Constants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    @Autowired
    private SQLUtil sqlUtil;
    @Autowired
    private WCTagRepository WCTagRepository;

    public WCTag getWCTagBySid(String wc_tag_sid) {
        return WCTagRepository.findOne(wc_tag_sid);
    }

    public Map<String, String> getTagMaps() {
        Map<String, String> tagMaps = Maps.newConcurrentMap();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT wt.wc_tag_sid,wt.tag_name ");
            sql.append(" FROM wc_tag wt ");
            List<Map<String, Object>> results = this.jdbc.queryForList(sql.toString());
            Iterator<Map<String, Object>> it = results.iterator();
            while (it.hasNext()) {
                Map<String, Object> result = it.next();
                String wc_tag_sid = sqlUtil.transToString(result.get("wc_tag_sid"));
                String tag_name = sqlUtil.transToString(result.get("tag_name"));
                tagMaps.put(wc_tag_sid, tag_name);
            }
        } catch (Exception e) {
            log.warn("getUnSignData", e);
        }
        return tagMaps;
    }
}
