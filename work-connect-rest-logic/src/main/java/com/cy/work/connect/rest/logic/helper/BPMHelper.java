/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.client.ProcessClient;
import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.common.ProcessType;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.User;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.ButtonInfoWithOneSelectMenuAndOneTextarea;
import com.cy.formsigning.vo.ButtonInfoWithOneTextarea;
import com.cy.formsigning.vo.SelectItems;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.rest.logic.common.Constants;
import com.cy.work.connect.rest.logic.common.SQLUtil;
import com.cy.work.connect.rest.logic.service.BpmService;
import com.cy.work.connect.rest.logic.service.WCExecManagerSignInfoService;
import com.cy.work.connect.rest.logic.service.WCManagerSignInfoService;
import com.cy.work.connect.rest.logic.service.WCMasterService;
import com.cy.work.connect.rest.logic.vo.BpmCreateTo;
import com.cy.work.connect.rest.logic.vo.BpmSignTo;
import com.cy.work.connect.rest.logic.vo.SignDataVO;
import com.cy.work.connect.rest.logic.vo.SignInfoVO;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.RestFlowType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class BPMHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8917412314017834192L;

    @Autowired
    @Qualifier(Constants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    @Autowired
    private SQLUtil sqlUtil;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private TaskClient taskClient;
    @Autowired
    private BpmOrganizationClient organizationClient;
    @Autowired
    private ProcessClient processClient;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCMasterService wcMasterService;
    @Autowired
    private WCManagerSignInfoService wcManagerSignInfoService;
    @Autowired
    private WCExecManagerSignInfoService wcExecManagerSignInfoService;
    @Autowired
    private WCExecManagerSignInfoRestHelper wcExecManagerSignInfoHelper;
    @Autowired
    private WCManagerSignInfoRestCheckHelper managerSignInfoCheckHelper;

    /**
     * 取得可簽核人員ID By BmpID(InstanceID)
     *
     * @param bpmID BmpID(InstanceID)
     * @return
     */
    public List<String> findTaskCanSignUserIds(String bpmID) {
        try {
            if (Strings.isNullOrEmpty(bpmID)) {
                return Lists.newArrayList();
            }
            return taskClient.findTaskCanSignUserIds(bpmID);
        } catch (Exception e) {
            log.warn("findTaskCanSignUserIds ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 建立流程
     *
     * @param masterNo
     * @param userSid
     * @param companySid
     * @param flowType
     * @return
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public String createBpmFlow(
        String masterNo, Integer userSid, Integer companySid, WcBpmFlowType flowType)
        throws ProcessRestException {
        return this.createBpmFlow(
            masterNo,
            userManager.findBySid(userSid).getId(),
            orgManager.findBySid(companySid).getId(),
            flowType);
    }

    /**
     * 建立流程
     *
     * @param masterNo
     * @param userId
     * @param companyId
     * @param flowType
     * @return
     * @throws ProcessRestException
     */
    private String createBpmFlow(
        String masterNo, String userId, String companyId, WcBpmFlowType flowType)
        throws ProcessRestException {
        return processClient.create(
            new BpmCreateTo(masterNo, userId, this.findUserRoleByCompany(userId, companyId),
                flowType));
    }

    /**
     * 尋找使用者在指定公司角色
     *
     * @param userId
     * @param companyId
     * @return
     * @throws ProcessRestException
     */
    private String findUserRoleByCompany(String userId, String companyId)
        throws ProcessRestException {
        return organizationClient.findUserRoleByCompany(userId, companyId);
    }

    /**
     * 執行簽核
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void sign(String userId, String bpmId) throws ProcessRestException {
        processClient.sign(new BpmSignTo(userId, bpmId));
    }

    /**
     * 執行簽核 (串簽)
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void signContinue(String userId, String bpmId) throws ProcessRestException {
        processClient.signContinue(new BpmSignTo(userId, bpmId));
    }

    /**
     * 執行退回
     *
     * @param userId
     * @param rollBackTask
     * @param comment
     * @throws ProcessRestException
     */
    public void rollBack(String userId, ProcessTaskHistory rollBackTask, String comment)
        throws ProcessRestException {
        processClient.rollback(userId, ProcessType.SIGNFLOW, rollBackTask, comment);
    }

    /**
     * 執行作廢
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void invalid(String userId, String bpmId) throws ProcessRestException {
        processClient.terminate(bpmId, userId);
    }

    /**
     * 執行復原
     *
     * @param userId
     * @param recoveryTask
     * @throws ProcessRestException
     */
    public void recovery(String userId, ProcessTaskHistory recoveryTask)
        throws ProcessRestException {
        processClient.recovery(userId, ProcessType.SIGNFLOW, recoveryTask);
    }

    /**
     * 尋找流程實例主要執行任務<br> 可能會回傳NULL值
     *
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public ProcessTask findMajorTask(String bpmId) throws ProcessRestException {
        return taskClient.findMajorTask(bpmId);
    }

    /**
     * 取得當前BPM狀態
     *
     * @param userId
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public BpmStatus getBpmStatus(
        String userId,
        String bpmId,
        List<ProcessTaskBase> tasks,
        WCOpinionDescripeSetting wcOpinionDescripeSetting)
        throws ProcessRestException {
        // 新建檔，第一節點未簽核時
        ProcessTaskBase firstInfo = tasks.get(0);
        if (firstInfo == null) {
            log.warn("流程ID:" + bpmId + " 取得任務節點為空值，請檢查相關流程資料...");
            return null; // 如果有問題暫時回傳無意義值
        }

        // 核准，狀態為結束且最後節點簽完
        ProcessTaskBase majorTask = this.findMajorTask(bpmId);
        boolean isFinish = majorTask == null;

        // 如果為歷史的任務代表已簽核過
        boolean firstDoSign = firstInfo instanceof ProcessTaskHistory;
        if (!firstDoSign && !isFinish) {
            if (firstInfo.getRollbackInfo().isEmpty()
                && (wcOpinionDescripeSetting == null
                || Strings.isNullOrEmpty(wcOpinionDescripeSetting.getRollBackReason()))) {
                return BpmStatus.NEW_INSTANCE;
            } else {
                return BpmStatus.RECONSIDERATION;
            }
        }

        if (!isFinish) {
            // 待簽核，流程未結束且，第一節點簽了，但第二節點未簽核時
            ProcessTaskBase secondInfo = tasks.get(1);
            boolean secondDoSign = secondInfo instanceof ProcessTaskHistory;
            if (firstDoSign && !secondDoSign) {
                return BpmStatus.WAITING_FOR_APPROVE;
            }
        }

        ProcessTaskBase lastInfo = tasks.get(tasks.size() - 1);
        boolean lastDoSign = lastInfo instanceof ProcessTaskHistory;
        if (lastDoSign && isFinish) {
            return BpmStatus.APPROVED;
        }

        // 簽核中
        return BpmStatus.APPROVING;
    }

    /**
     * 查詢當前簽核人員名稱
     *
     * @param lastTask
     * @return
     */
    public String findTaskDefaultUserName(ProcessTaskBase lastTask) {
        try {
            // 最後節點如果為歷史任務的話就無待簽人員..
            if (lastTask instanceof ProcessTaskHistory) {
                return "";
            }
            if (!Strings.isNullOrEmpty(lastTask.getUserName())) {
                return lastTask.getUserName();
            }
            List<String> userids = this.findUserFromRole(lastTask.getRoleID());
            if (userids.isEmpty()) {
                return "";
            }
            User user = userManager.findById(userids.get(0));
            return user.getName();
        } catch (ProcessRestException ex) {
            log.warn("查詢當前簽核人員名稱 ERROR", ex);
        }
        return "";
    }

    private List<SignDataVO> getSignDataByWcSid(String wcSid) {
        List<SignDataVO> unSignDatas = Lists.newArrayList();
        try {
            StringBuilder sql = new StringBuilder();
            // 需求單位流程
            sql.append(" SELECT ");
            sql.append(" wsi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wsi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wsi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wsi.signType as signType,"); // 簽核類型
            sql.append(" wsi.create_dt as create_dt,"); // 建立時間
            sql.append(" wsi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'REQ' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wsi.status_code as status_code ");
            sql.append(" FROM wc_manager_sign_info wsi ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wsi.wc_sid ");
            sql.append(" WHERE wsi.status_code <> '" + BpmStatus.CLOSED + "' ");
            sql.append(" AND wsi.status_code <> '" + BpmStatus.DELETE + "' ");
            sql.append(" AND wsi.status_code <> '" + BpmStatus.INVALID + "' ");
            sql.append(" AND wsi.wc_sid =  '" + wcSid + "' ");
            sql.append(" UNION ALL ");
            // 執行單位流程
            sql.append(" SELECT ");
            sql.append(" wesi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wesi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wesi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wesi.signType as signType,"); // 簽核類型
            sql.append(" wesi.create_dt as create_dt,"); // 建立時間
            sql.append(" wesi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'EXEC' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wesi.status_code as status_code ");
            sql.append(" FROM wc_exec_manager_sign_info wesi ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wesi.wc_sid ");
            sql.append(" WHERE wesi.status_code <> '" + BpmStatus.CLOSED + "' ");
            sql.append(" AND wesi.status_code <> '" + BpmStatus.DELETE + "' ");
            sql.append(" AND wesi.status_code <> '" + BpmStatus.INVALID + "' ");
            sql.append(" AND wesi.wc_sid =  '" + wcSid + "' ");
            List<Map<String, Object>> results = this.jdbc.queryForList(sql.toString());
            Iterator<Map<String, Object>> it = results.iterator();
            while (it.hasNext()) {
                Map<String, Object> result = it.next();
                String wc_sid = sqlUtil.transToString(result.get("wc_sid"));
                String wc_no = sqlUtil.transToString(result.get("wc_no"));
                String bpm_instance_id = sqlUtil.transToString(result.get("bpm_instance_id"));
                String signTypeStr = sqlUtil.transToString(result.get("signType"));
                Date create_dt = sqlUtil.transToDate(result.get("create_dt"));
                Date update_dt = sqlUtil.transToDate(result.get("update_dt"));
                String theme = sqlUtil.transToString(result.get("theme"));
                String flowTypeStr = sqlUtil.transToString(result.get("FlowType"));
                RestFlowType flowType = RestFlowType.valueOf(flowTypeStr);
                SignType signType = null;
                try {
                    signType = SignType.valueOf(signTypeStr);
                } catch (Exception e) {
                    log.warn("SignType.valueOf(signTypeStr)", e);
                }
                Integer wc_create_usr = sqlUtil.transToInteger(result.get("wc_create_usr"));
                Date wc_create_dt = sqlUtil.transToDate(result.get("wc_create_dt"));
                String wc_statusStr = sqlUtil.transToString(result.get("wc_status"));
                WCStatus wcStatus = null;
                try {
                    wcStatus = WCStatus.valueOf(wc_statusStr);
                } catch (Exception e) {
                    log.warn("WCStatus.valueOf(wc_statusStr)", e);
                }
                String statusCodeStr = sqlUtil.transToString(result.get("status_code"));
                BpmStatus bpmStatus = null;
                try {
                    bpmStatus = BpmStatus.valueOf(statusCodeStr);
                } catch (Exception e) {
                    log.warn("WCStatus.valueOf(wc_statusStr)", e);
                }
                List<String> canSignSids = this.findTaskCanSignUserIds(bpm_instance_id);
                Integer compSid = sqlUtil.transToInteger(result.get("comp_sid"));
                SignDataVO sd =
                    new SignDataVO(
                        wc_sid,
                        wc_no,
                        bpm_instance_id,
                        signType,
                        create_dt,
                        update_dt,
                        theme,
                        flowType,
                        null,
                        wc_create_usr,
                        wc_create_dt,
                        wcStatus,
                        canSignSids,
                        compSid,
                        bpmStatus);
                unSignDatas.add(sd);
            }
        } catch (Exception e) {
            log.warn("getUnSignData", e);
        }
        Collections.sort(
            unSignDatas,
            new Comparator<SignDataVO>() {
                @Override
                public int compare(SignDataVO o1, SignDataVO o2) {
                    Date d1 = (o1.getUpdate_dt() != null) ? o1.getUpdate_dt() : o1.getCreate_dt();
                    Date d2 = (o2.getUpdate_dt() != null) ? o2.getUpdate_dt() : o2.getCreate_dt();
                    return d1.compareTo(d2);
                }
            });
        return unSignDatas;
    }

    public List<String> tansCanSignIds(String json) {
        List<String> canSignIds = Lists.newArrayList();
        try {
            JsonStringListTo jsonStringListTo =
                WkJsonUtils.getInstance().fromJson(json, JsonStringListTo.class);
            if (jsonStringListTo == null) {
                return canSignIds;
            }
            canSignIds.addAll(jsonStringListTo.getValue());
        } catch (Exception ex) {
            log.warn("canSignIds ERROR" + ex.getMessage(), ex);
        }
        return canSignIds;
    }

    public List<ProcessTaskBase> transToProcessTaskBases(String json) {
        List<ProcessTaskBase> bs = Lists.newArrayList();
        try {
            List<ProcessTaskHistory> historys =
                WkJsonUtils.getInstance().fromJsonToList(json, ProcessTaskHistory.class);
            if (historys == null || historys.isEmpty()) {
                return bs;
            }
            for (ProcessTaskBase each : historys) {
                if (Strings.isNullOrEmpty(((ProcessTaskHistory) each).getExecutorID())) {
                    ProcessTask news = new ProcessTask();
                    each = (ProcessTaskBase) WkEntityUtils.getInstance().copyProperties(each, news);
                }
                bs.add(each);
            }
        } catch (Exception ex) {
            log.warn("parser json to List<ProcessTaskBase> fail!!" + ex.getMessage(), ex);
        }
        return bs;
    }

    /**
     * 尋找角色全部使用者
     *
     * @param roleId
     * @return
     * @throws ProcessRestException
     */
    private List<String> findUserFromRole(String roleId) throws ProcessRestException {
        return organizationClient.findUserFromRole(roleId);
    }

    public List<ButtonInfo> createButtonInfo(SignInfoVO sv, User loginUser) {
        List<ButtonInfo> btns = Lists.newArrayList();
        try {
            WCMaster master = wcMasterService.getWCMasterBySid(sv.getWc_sid());
            if (master.getWc_status().equals(WCStatus.INVALID)
                || master.getWc_status().equals(WCStatus.CLOSE)
                || master.getWc_status().equals(WCStatus.CLOSE_STOP)) {
                return btns;
            }
            boolean showSignBtn = false;
            boolean showRecovery = false;
            boolean showRollBack = false;
            boolean showInvalid = false;
            boolean showExecSignBtn = false;
            boolean showReqContinueRecovery = false;
            boolean showExecContinueRecovery = false;
            boolean showExecRollBack = false;
            boolean showExecStop = false;
            List<SignDataVO> allSignDatas = this.getSignDataByWcSid(master.getSid());
            List<ProcessTaskBase> taskHs = Lists.newArrayList();

            for (SignDataVO sd : allSignDatas) {
                List<ProcessTaskBase> tasks =
                    BpmService.getInstance()
                        .findSimulationChart(loginUser.getId(), sd.getBpm_instance_id());

                if (RestFlowType.REQ.equals(sd.getFlowType())) {
                    if (SignType.SIGN.equals(sd.getSignType())) {
                        if (sd.getBpmStatus().equals(BpmStatus.APPROVED)) {

                            if (wcManagerSignInfoService.isShowApprovedRecovery(
                                loginUser.getId(), tasks, master.getSid())) {
                                showRecovery = true;
                            }
                        } else {
                            if (this.isShowSign(loginUser.getId(), sd.getBpm_instance_id())) {
                                showSignBtn = true;
                            }
                            if (this.isShowRecovery(loginUser.getId(), sd.getBpmStatus(), tasks)) {
                                showRecovery = true;
                            }
                            if (this.isShowRollBack(loginUser.getId(), sd.getBpm_instance_id(),
                                tasks)) {
                                showRollBack = true;
                                taskHs = tasks;
                            }
                            if (this.isShowInvalid(
                                loginUser.getId(), sd.getBpmStatus(), sd.getCanSignSids(), tasks)) {
                                showInvalid = true;
                            }
                        }

                    } else if (SignType.COUNTERSIGN.equals(sd.getSignType())) {
                        if (this.isShowSign(loginUser.getId(), sd.getBpm_instance_id())) {
                            showSignBtn = true;
                        }
                        if (wcManagerSignInfoService.isShowContinueRecovery(
                            loginUser, tasks, master.getSid(), null)) {
                            showReqContinueRecovery = true;
                        }

                        // 暫不開放
                    }
                } else if (RestFlowType.EXEC.equals(sd.getFlowType())) {
                    if (SignType.SIGN.equals(sd.getSignType())) {
                        if (this.isShowSign(loginUser.getId(), sd.getBpm_instance_id())) {
                            showExecSignBtn = true;
                            if (wcExecManagerSignInfoHelper.isCanRollBackToReq(sv.getWc_sid())) {
                                showExecRollBack = true;
                            } else {
                                showExecStop = true;
                            }
                        }
                    } else if (SignType.COUNTERSIGN.equals(sd.getSignType())) {
                        if (this.isShowSign(loginUser.getId(), sd.getBpm_instance_id())) {
                            showExecSignBtn = true;
                        }
                    }
                    if (wcExecManagerSignInfoService.canRecoveryExecManagerInfo(master.getSid(),
                        loginUser)) {
                        showExecContinueRecovery = true;
                    }
                }
            }

            if (showSignBtn) {
                btns.add(new ButtonInfo("簽名", sv.getInstanceID() + "," + ActionType.REQ_FLOW_SIGN));
            }
            if (showRecovery) {
                btns.add(new ButtonInfo("復原", sv.getInstanceID() + "," + ActionType.REQ_FLOW_RECOVERY));
            }
            if (showReqContinueRecovery) {
                btns.add(new ButtonInfo("復原",
                    sv.getInstanceID() + "," + ActionType.REQ_FLOW_COUNTER_RECOVERY));
            }
            if (showExecContinueRecovery) {
                btns.add(new ButtonInfo("復原", sv.getInstanceID() + "," + ActionType.EXEC_FLOW_RECOVERY));
            }
            if (showRollBack) {
                btns.add(createRollBackBtn(sv.getInstanceID(), loginUser, taskHs));
            }
            if (showInvalid) {
                btns.add(createInvailBtn(sv.getInstanceID()));
            }
            if (showExecSignBtn) {
                btns.add(new ButtonInfo("簽名", sv.getInstanceID() + "," + ActionType.EXEC_FLOW_SIGN));
            }
            //執行方簽核者退回
            if (showExecRollBack) {
                WCManagerSignInfo reqFlow = managerSignInfoCheckHelper.getApprovedReqFlow(
                    sv.getWc_sid());
                List<ProcessTaskBase> tasks =
                    BpmService.getInstance()
                        .findSimulationChart(loginUser.getId(), reqFlow.getBpmId());
                btns.add(createExecRollBackBtn(sv.getInstanceID(), tasks));
            }
            if (showExecStop) {
                btns.add(createExecStopBtn(sv.getInstanceID()));
            }
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("createFormBtn ERROR：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("createFormBtn ERROR", e);
        }
        return btns;
    }

    private ButtonInfo createExecRollBackBtn(String wc_ID, List<ProcessTaskBase> tasks) {
        ButtonInfoWithOneSelectMenuAndOneTextarea btn = new ButtonInfoWithOneSelectMenuAndOneTextarea();
        btn.setButtonValue("退回");
        btn.setAction(wc_ID + "," + ActionType.EXEC_FLOW_SIGNER_ROLLBACK);
        btn.setInputTitle("退回原因"); // 退回視窗下方輸入框旁左側文字
        btn.setInputTextareaRequired(true);
        btn.setSelectItems(
            this.createRollBackSelectItem(tasks, ActionType.EXEC_FLOW_SIGNER_ROLLBACK)); // 退回視窗下拉選項
        return btn;
    }

    private ButtonInfo createExecStopBtn(String wc_ID) {
        ButtonInfoWithOneTextarea btn = new ButtonInfoWithOneTextarea();
        btn.setButtonValue("不執行");
        btn.setAction(wc_ID + "," + ActionType.EXEC_FLOW_SIGNER_STOP);
        btn.setInputTitle("不執行原因"); // 不執行視窗下方輸入框旁左側文字
        btn.setRequired(true);
        return btn;
    }

    private ButtonInfo createInvailBtn(String wc_ID) {
        ButtonInfoWithOneTextarea btn = new ButtonInfoWithOneTextarea();
        btn.setButtonValue("作廢");
        btn.setAction(wc_ID + "," + ActionType.REQ_FLOW_INVAILD);
        btn.setInputTitle("作廢原因"); // 退回視窗下方輸入框旁左側文字
        btn.setRequired(true);
        return btn;
    }

    private ButtonInfo createRollBackBtn(String wc_ID, User executor, List<ProcessTaskBase> tasks) {
        ButtonInfoWithOneSelectMenuAndOneTextarea btn = new ButtonInfoWithOneSelectMenuAndOneTextarea();
        btn.setButtonValue("退回");
        btn.setAction(wc_ID + "," + ActionType.REQ_FLOW_ROLLBACK);
        btn.setInputTitle("退回原因"); // 退回視窗下方輸入框旁左側文字
        btn.setSelectItems(
            this.createRollBackSelectItem(tasks, ActionType.REQ_FLOW_ROLLBACK)); // 退回視窗下拉選項
        return btn;
    }

    private SelectItems createRollBackSelectItem(List<ProcessTaskBase> tasks, ActionType type) {
        List<ProcessTaskBase> nTasks = Lists.newArrayList(tasks);
        SelectItems items = SelectItems.create("退回至");
        if (!ActionType.EXEC_FLOW_SIGNER_ROLLBACK.equals(type)) {
            nTasks.remove(nTasks.size() - 1);
        }
        nTasks.forEach(
            each -> {
                if (each instanceof ProcessTaskHistory) {
                    ProcessTaskHistory history = (ProcessTaskHistory) each;
                    // key = 顯示名稱 , value = 回傳資訊
                    items.addItem(
                        history.getTaskName().contains("申請人")
                            ? "申請人-" + history.getExecutorName()
                            : history.getRoleName() + "-" + history.getExecutorName(),
                        history.getSid());
                }
            });
        return items;
    }

    /**
     * 是否顯示簽核按鈕
     *
     * @param userId
     * @param bpmId
     * @return
     */
    public Boolean isShowSign(String userId, String bpmId) {
        List<String> ids = this.findTaskCanSignUserIds(bpmId);
        return ids.contains(userId);
    }

    /**
     * 是否顯示復原按鈕
     *
     * @param userId
     * @param status
     * @param tasks
     * @return
     */
    public Boolean isShowRecovery(String userId, BpmStatus status, List<ProcessTaskBase> tasks) {
        // 任務模擬圖需超過兩位才能進行復原，結案也無法復原
        if (tasks.size() <= 1 || BpmStatus.CLOSED.equals(status) || BpmStatus.APPROVED.equals(
            status)) {
            return Boolean.FALSE;
        }
        // 前一個簽核者為當前執行者才可復原
        ProcessTaskBase rTask = tasks.get(tasks.size() - 2);
        if (rTask instanceof ProcessTaskHistory) {
            return userId.equals(((ProcessTaskHistory) rTask).getExecutorID());
        }
        return false;
    }

    /**
     * 是否顯示退回按鈕
     *
     * @param userId
     * @param bpmId
     * @param tasks
     * @return
     */
    public Boolean isShowRollBack(String userId, String bpmId, List<ProcessTaskBase> tasks) {
        List<String> ids = this.findTaskCanSignUserIds(bpmId);
        return ids.contains(userId) && !tasks.isEmpty() && tasks.size() > 1;
    }

    /**
     * 是否顯示作廢按鈕
     *
     * @param userId
     * @param status
     * @param canSignIds
     * @param tasks
     * @return
     */
    public Boolean isShowInvalid(
        String userId, BpmStatus status, List<String> canSignIds, List<ProcessTaskBase> tasks) {
        if (!canSignIds.contains(userId)) {
            return Boolean.FALSE;
        }
        if (!tasks.isEmpty()
            && (BpmStatus.NEW_INSTANCE.equals(status)
            || BpmStatus.WAITING_FOR_APPROVE.equals(status)
            || BpmStatus.APPROVING.equals(status)
            || BpmStatus.RECONSIDERATION.equals(status))) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public List<String> getDefaultApprover(ProcessTaskHistory processTaskHistory)
        throws ProcessRestException {
        return !Strings.isNullOrEmpty(processTaskHistory.getUserID())
            ? Lists.newArrayList(processTaskHistory.getUserID())
            : organizationClient.findUserFromRole(processTaskHistory.getRoleID());
    }

    public List<String> getDefaultApprover(HistoryTaskTo historyTaskTo)
        throws ProcessRestException {
        return !Strings.isNullOrEmpty(historyTaskTo.getUserID())
            && !historyTaskTo.getUserID().equals("nvl")
            ? Lists.newArrayList(historyTaskTo.getUserID())
            : organizationClient.findUserFromRole(historyTaskTo.getRoleID());
    }
}
