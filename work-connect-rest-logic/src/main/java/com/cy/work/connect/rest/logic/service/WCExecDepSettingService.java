/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.work.connect.repository.WCExecDepSettingRepository;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.google.common.collect.Maps;

/**
 * @author brain0925_liao
 */
@Component
public class WCExecDepSettingService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 945130896449806709L;

    @Autowired
    private WCExecDepSettingRepository wcExecDepSettingRepository;


    public Map<String, WCExecDepSetting> getAllMap() {
        Map<String, WCExecDepSetting> wcExecDepSettingMaps = Maps.newConcurrentMap();
        wcExecDepSettingRepository
            .findAll()
            .forEach(
                item -> {
                    wcExecDepSettingMaps.put(item.getSid(), item);
                });
        return wcExecDepSettingMaps;
    }

    public WCExecDepSetting getWCExecDepSettingBySid(String wc_exec_dep_setting_sid) {
        return wcExecDepSettingRepository.findOne(wc_exec_dep_setting_sid);
    }
}
