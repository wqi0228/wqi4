/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.rest.logic.service.BpmService;
import com.cy.work.connect.rest.logic.service.WCExceDepService;
import com.cy.work.connect.rest.logic.service.WCExecManagerSignInfoService;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * @author brain0925_liao
 */
@Component
public class WCExecManagerSignInfoRestHelper {

    @Autowired
    private BPMHelper bpmHelper;
    @Autowired
    private WCExecManagerSignInfoService wcExecManagerSignInfoService;
    @Autowired
    private WCExceDepService wcExceDepService;

    /**
     * 建立執行單位BPM流程
     *
     * @param wcMaster 工作聯絡單物件
     * @param signUser 欲簽核人員
     * @return
     * @throws Exception
     */
    public WCExecManagerSignInfo createExecFlow(WCMaster wcMaster, User signUser, int groupSeq)
            throws Exception {
        // int groupSeq = wcExecManagerSignInfoCache.getMaxGroupSeq(wcMaster.getSid());
        String bpmId = bpmHelper.createBpmFlow(
                wcMaster.getWc_no(),
                signUser.getSid(),
                signUser.getPrimaryOrg().getCompanySid(),
                WcBpmFlowType.WC_EXEC);
        WCExecManagerSignInfo wcExecManagerSignInfo = new WCExecManagerSignInfo();
        wcExecManagerSignInfo.setWcSid(wcMaster.getSid());
        wcExecManagerSignInfo.setWcNo(wcMaster.getWc_no());
        wcExecManagerSignInfo.setStatus(BpmStatus.NEW_INSTANCE);
        wcExecManagerSignInfo.setSignType(SignType.SIGN);
        wcExecManagerSignInfo.setBpmId(bpmId);
        wcExecManagerSignInfo.setUser_sid(signUser.getSid());
        wcExecManagerSignInfo.setDep_sid(signUser.getPrimaryOrg().getSid());
        wcExecManagerSignInfo.setGroupSeq(groupSeq);
        wcExecManagerSignInfo.setSeq(
                wcExecManagerSignInfoService.getMaxSeq(
                        wcMaster.getSid(), wcExecManagerSignInfo.getGroupSeq()));
        wcExecManagerSignInfo.setCreate_user_sid(null);
        wcExecManagerSignInfo.setCreate_dt(new Date());
        wcExecManagerSignInfo = wcExecManagerSignInfoService.doSyncBpmData(wcExecManagerSignInfo, null, null);
        return wcExecManagerSignInfo;
    }

    /**
     * 取得可簽名節點物件List
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 執行者
     * @return
     */
    public List<WCExecManagerSignInfo> getSignExecSignInfo(String wcSid, User loginUser) {
        List<WCExecManagerSignInfo> allWCManagerSignInfo = wcExecManagerSignInfoService.getAllFlowSignInfos(wcSid);
        List<WCExecManagerSignInfo> activeWCManagerSignInfos = allWCManagerSignInfo.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                                && !each.getStatus().equals(BpmStatus.INVALID))
                .collect(Collectors.toList());
        List<WCExecManagerSignInfo> signExecManagerInfos = Lists.newArrayList();
        for (WCExecManagerSignInfo gi : activeWCManagerSignInfos) {
            List<String> canSignedIds = bpmHelper.findTaskCanSignUserIds(gi.getBpmId());
            if (canSignedIds.contains(loginUser.getId())) {
                signExecManagerInfos.add(gi);
            }
        }
        Preconditions.checkState(signExecManagerInfos.size() > 0, "執行BPM簽名，比對簽核人員不符，請確認！！");
        return signExecManagerInfos;
    }

    /**
     * 取得可復原執行單位簽核物件List
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 登入者
     * @return
     */
    public List<WCExecManagerSignInfo> getRecoveryExecSignInfo(String wcSid, User loginUser) {
        List<WCExecManagerSignInfo> result = Lists.newArrayList();
        List<WCExecManagerSignInfo> wcExecManagerSignInfos = wcExecManagerSignInfoService.getAllFlowSignInfos(wcSid);
        for (WCExecManagerSignInfo si : wcExecManagerSignInfos) {
            if (si.getStatus().equals(BpmStatus.INVALID)) {
                continue;
            }
            List<ProcessTaskBase> tasks = BpmService.getInstance().findSimulationChart(loginUser.getId(), si.getBpmId());
            if (wcExecManagerSignInfoService.isShowRecovery(loginUser.getId(), si.getStatus(),
                    tasks)
                    && !wcExceDepService.isExecWorking(wcSid, si)) {
                result.add(si);
            }
        }
        Preconditions.checkState(result.size() > 0, "執行BPM簽名，比對簽核人員不符，請確認！！");
        return result;
    }

    /**
     * 取得可退回執行單位簽核物件List
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 登入者
     * @return
     */
    public List<WCExecManagerSignInfo> getRollBackExecSignInfo(String wcSid, User loginUser) {
        List<WCExecManagerSignInfo> allWCManagerSignInfo = wcExecManagerSignInfoService.getAllFlowSignInfos(wcSid);
        List<WCExecManagerSignInfo> activeWCManagerSignInfos = allWCManagerSignInfo.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                                && !each.getStatus().equals(BpmStatus.INVALID))
                .collect(Collectors.toList());
        List<WCExecManagerSignInfo> rollBackExecManagerSignInfos = Lists.newArrayList();
        for (WCExecManagerSignInfo gi : activeWCManagerSignInfos) {
            List<String> canSignedIds = bpmHelper.findTaskCanSignUserIds(gi.getBpmId());
            if (canSignedIds.contains(loginUser.getId())) {
                rollBackExecManagerSignInfos.add(gi);
            }
        }
        Preconditions.checkState(rollBackExecManagerSignInfos.size() > 0, "執行BPM簽名，比對簽核人員不符，請確認！！");
        return rollBackExecManagerSignInfos;
    }

    /**
     * 是否可退回至需求流程
     *
     * @param wcSid 工作聯絡單Sid
     */
    public boolean isCanRollBackToReq(String wcSid) {
        List<WCExecManagerSignInfo> execFlows = wcExecManagerSignInfoService.getFlowSignInfos(
                wcSid);
        List<WCExecManagerSignInfo> signExecFlows = execFlows.stream()
                .filter(each -> each.getStatus().equals(BpmStatus.APPROVED))
                .collect(Collectors.toList());
        List<WCExceDep> doingExceDeps = wcExceDepService.getWCExceDepByWcSid(wcSid).stream()
                .filter(
                        each -> (each.getStatus().equals(Activation.ACTIVE)
                                && (each.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                                        || each.getExecDepStatus().equals(WCExceDepStatus.FINISH)
                                        || each.getExecDepStatus().equals(WCExceDepStatus.CLOSE)
                                        || each.getExecDepStatus().equals(WCExceDepStatus.STOP))))
                .collect(Collectors.toList());
        // 若已有人簽名,僅中止自己的執行單位狀態,
        // 若都無人簽名,將退回至核准者,並將簽核節點清除
        return (signExecFlows == null || signExecFlows.isEmpty())
                && (doingExceDeps == null || doingExceDeps.isEmpty())
                && !wcExecManagerSignInfoService.isCheckAnyContinueSign(wcSid);
    }

    /**
     * 是否相同群組的執行單位簽核物件皆已簽核完畢
     *
     * @param wcSid    工作聯絡單Sid
     * @param groupSeq 群組Seq
     * @return
     */
    public boolean isGroupExecSignInfoApproved(String wcSid, Integer groupSeq) {
        List<WCExecManagerSignInfo> flows = wcExecManagerSignInfoService.getAllFlowSignInfos(wcSid);
        List<WCExecManagerSignInfo> groupFlows = flows.stream()
                .filter(
                        each -> groupSeq.equals(each.getGroupSeq())
                                && !each.getStatus().equals(BpmStatus.INVALID))
                .collect(Collectors.toList());
        boolean signFinish = true;
        for (WCExecManagerSignInfo si : groupFlows) {
            if (!si.getStatus().equals(BpmStatus.APPROVED)) {
                signFinish = false;
            }
        }
        return signFinish;
    }
}
