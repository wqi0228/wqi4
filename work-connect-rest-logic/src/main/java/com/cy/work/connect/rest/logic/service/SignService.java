/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormSigningType;
import com.cy.formsigning.enums.FormType;
import com.cy.formsigning.enums.QueryType;
import com.cy.formsigning.vo.BatchSignedResult;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.ButtonInfoWithOneSelectMenuAndOneTextarea;
import com.cy.formsigning.vo.ButtonInfoWithOneTextarea;
import com.cy.formsigning.vo.FormInfo;
import com.cy.formsigning.vo.TodoSummary;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.signflow.ExecFlowActionHelper;
import com.cy.work.connect.logic.helper.signflow.ReqFlowActionHelper;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.rest.logic.common.Constants;
import com.cy.work.connect.rest.logic.common.SQLUtil;
import com.cy.work.connect.rest.logic.helper.BPMHelper;
import com.cy.work.connect.rest.logic.helper.CacheHelper;
import com.cy.work.connect.rest.logic.vo.SignDataVO;
import com.cy.work.connect.rest.logic.vo.SignInfoVO;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.RestFlowType;
import com.cy.work.connect.vo.enums.SignActionType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class SignService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3216241239332532811L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    @Qualifier(Constants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    @Autowired
    private SQLUtil sqlUtil;
    @Autowired
    private WCTagService wcTagService;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private BPMHelper bpmHelper;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private CacheHelper cacheHelper;
    @Autowired
    private WCExecDepSettingService execDepSettingService;
    @Autowired
    private TaskClient taskClient;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private ReqFlowActionHelper reqFlowActionHelper;
    @Autowired
    private ExecFlowActionHelper execFlowActionHelper;
    @Autowired
    private BpmManager bpmManager;

    // ========================================================================
    // 方法
    // ========================================================================
    public BatchSignedResult doBatchSign(List<String> formIds, String executorUuid) {
        Preconditions.checkArgument(formIds != null && !formIds.isEmpty(), "傳送的formIds 不可為空值");
        User executor = userManager.findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null,
                "傳送的執行者uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<SignDataVO> reqSign = Lists.newArrayList();
        List<SignDataVO> execSign = Lists.newArrayList();
        formIds.forEach(
                item -> {
                    try {
                        SignDataVO info = getSignDataByInstanceID(item);
                        Preconditions.checkArgument(info != null, "查詢不到工作聯絡單資訊 SID:" + item);
                        WCMaster wcMaster = wcMasterManager.findBySid(info.getWc_sid());
                        Preconditions.checkArgument(wcMaster != null, "查詢不到工作聯絡單資訊 SID:" + item);
                        if (info.getFlowType().equals(RestFlowType.REQ)) {
                            reqSign.add(info);
                        } else if (info.getFlowType().equals(RestFlowType.EXEC)) {
                            execSign.add(info);
                        }
                    } catch (Exception e) {
                        log.warn("doBatchSign ERROR", e);
                    }
                });
        Preconditions.checkArgument(
                reqSign.size() != 0 || execSign.size() != 0, "無法產生有效批次簽核資訊 formIds = " + formIds);
        BatchSignedResult toReturn = new BatchSignedResult((reqSign.size() + execSign.size()),
                FormType.WORK_CONNENT_SUMMARY);
        reqSign.forEach(
                item -> {
                    try {
                        WCMaster wcMaster = wcMasterManager.findBySid(item.getWc_sid());
                        this.doAction(ActionType.REQ_FLOW_SIGN, wcMaster.getSid(), wcMaster.getWc_no(), executor, null);
                    } catch (IllegalStateException | IllegalArgumentException e) {
                        log.warn(
                                "processHelper.doAction(ActionType.SIGN_REQ, item, executor, null) ERROR：{}",
                                e.getMessage());
                        String msg = "Unexpected exception: when "
                                + executor.getId()
                                + " sign the form("
                                + item.getWc_sid()
                                + ") -- "
                                + e.getClass().getName()
                                + ": "
                                + e.getMessage();
                        toReturn.addFail(item.getBpm_instance_id(), msg);
                    } catch (Exception ex) {
                        log.warn(
                                "processHelper.doAction(ActionType.SIGN_REQ, item, executor, null) ERROR",
                                ex);
                        String msg = "Unexpected exception: when "
                                + executor.getId()
                                + " sign the form("
                                + item.getWc_sid()
                                + ") -- "
                                + ex.getClass().getName()
                                + ": "
                                + ex.getMessage();
                        toReturn.addFail(item.getBpm_instance_id(), msg);
                    } finally {
                        cacheHelper.clearProcessCache(item.getWc_sid());
                    }
                });
        execSign.forEach(
                item -> {
                    try {
                        WCMaster wcMaster = wcMasterManager.findBySid(item.getWc_sid());
                        this.doAction(ActionType.EXEC_FLOW_SIGN, wcMaster.getSid(), wcMaster.getWc_no(), executor, null);
                    } catch (Exception ex) {
                        log.warn(
                                "processHelper.doAction(ActionType.SIGN_REQ, item, executor, null) ERROR",
                                ex);
                        String msg = "Unexpected exception: when "
                                + executor.getId()
                                + " sign the form("
                                + item.getWc_sid()
                                + ") -- "
                                + ex.getClass().getName()
                                + ": "
                                + ex.getMessage();
                        toReturn.addFail(item.getBpm_instance_id(), msg);
                    } finally {
                        cacheHelper.clearProcessCache(item.getWc_sid());
                    }
                });
        return toReturn;
    }

    public FormSigningType getFormSigningType(String instanceID, String executorUuid) {
        User executor = userManager.findByUUID(executorUuid);
        Preconditions.checkArgument(executor != null,
                "傳送的執行者uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        SignDataVO info = getSignDataByInstanceID(instanceID);
        Preconditions.checkArgument(info != null, "查詢不到工作聯絡單資訊 instanceID:" + instanceID);
        WCMaster wcMaster = wcMasterManager.findBySid(info.getWc_sid());

        Preconditions.checkArgument(wcMaster != null, "查詢不到工作聯絡單資訊 instanceID:" + instanceID);
        if (wcMaster.getWc_status().equals(WCStatus.INVALID)) {
            return FormSigningType.NOT_SIGNER;
        }
        List<String> canSigned = info.getCanSignSids();
        if (canSigned.contains(executor.getId())) {
            // 為表單現在的簽核者
            return FormSigningType.CURRENT_SIGNER;
        }
        List<ProcessTaskBase> tasks = BpmService.getInstance().findSimulationChart(executor.getId(), instanceID);
        if (tasks.stream()
                .filter(base -> base instanceof ProcessTaskHistory)
                .map(base -> (ProcessTaskHistory) base)
                .anyMatch(history -> history.getExecutorID().equals(executor.getId()))) {
            // 非表單現在的簽核者(已處理過該單據)
            return FormSigningType.NOT_CURRENT_SIGNER_PROCESSED;
        } else {
            // 非表單可進行簽核的人員
            return FormSigningType.NOT_SIGNER;
        }
    }

    public FormInfo doActionImpl(
            String action,
            Map<String, Object> inputData,
            String executorUuid) throws UserMessageException {

        // ====================================
        // 處理傳入參數
        // ====================================
        String[] param = WkStringUtils.safeTrim(action).split(",");
        if (param.length != 2) {
            log.error("簽核action參數無法切割為效數值 action：[" + action + "]");
            throw new UserMessageException(WkMessage.EXECTION + "(參數錯誤)");
        }

        if (WkStringUtils.isEmpty(param[1]) || ActionType.valueOf(param[1]) == null) {
            log.error("簽核action 執行方法錯誤 action：[" + action + "]");
            throw new UserMessageException(WkMessage.EXECTION + "(參數錯誤)");
        }

        // ====================================
        // 執行者
        // ====================================
        User executor = WkUserCache.getInstance().findByUUID(executorUuid);
        if (executor == null) {
            log.error(WkMessage.EXECTION + "(找不到簽核者資訊) uuid:[" + executorUuid + "]");
            throw new UserMessageException(WkMessage.EXECTION + "(找不到簽核者資訊)");
        }

        // ====================================
        // 資訊
        // ====================================
        // 取得簽核流程資料
        SignDataVO info = getSignDataByInstanceID(param[0]);
        if (info == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到簽核流程資訊)";
            log.error(errorMessage + "BPMID:[" + param[0] + "]");
            throw new UserMessageException(errorMessage);
        }

        // ====================================
        // 執行
        // ====================================
        try {
            this.doAction(
                    ActionType.valueOf(param[1]),
                    info.getWc_sid(),
                    info.getWc_no(),
                    executor,
                    inputData);
        } finally {
            // 清 web快取
            this.cacheHelper.clearProcessCache(info.getWc_sid());
        }
        return this.getFormInfo(param[0], executorUuid);
    }

    public List<ButtonInfo> createFormBtn(String instanceID, String executorUuid) {
        List<ButtonInfo> btns = Lists.newArrayList();
        User loginUser = userManager.findByUUID(executorUuid);
        SignInfoVO sv = getSignInfo(instanceID);
        Preconditions.checkArgument(sv != null, "查詢不到工作聯絡單資訊 InstanceID:" + instanceID);
        Preconditions.checkArgument(loginUser != null,
                "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        btns.addAll(bpmHelper.createButtonInfo(sv, loginUser));
        return btns;
    }

    public FormInfo getFormInfo(String instanceID, String executorUuid) {
        User user = userManager.findByUUID(executorUuid);
        SignDataVO info = getSignDataByInstanceID(instanceID);
        if (info == null) {
            return new FormInfo();
        }
        return this.transToFormInfo(info, user);
    }

    public SignInfoVO getSignInfo(String instanceID) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT ");
            sql.append(" wsi.wc_sid as wc_sid,");
            sql.append(" wsi.signType as signType,");
            sql.append(" wsi.status_code as status_code,");
            sql.append(" 'REQ' as FlowType ");
            sql.append(" FROM wc_manager_sign_info wsi ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wsi.wc_sid ");
            sql.append(" WHERE wsi.bpm_instance_id = '" + instanceID + "'");
            sql.append(" UNION ALL ");
            sql.append(" SELECT ");
            sql.append(" sei.wc_sid as wc_sid,");
            sql.append(" sei.signType as signType,");
            sql.append(" sei.status_code as status_code,");
            sql.append(" 'EXEC' as FlowType ");
            sql.append(" FROM wc_exec_manager_sign_info sei ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = sei.wc_sid ");
            sql.append(" WHERE sei.bpm_instance_id = '" + instanceID + "'");
            List<Map<String, Object>> results = this.jdbc.queryForList(sql.toString());
            Iterator<Map<String, Object>> it = results.iterator();
            while (it.hasNext()) {
                Map<String, Object> result = it.next();
                String wc_sid = sqlUtil.transToString(result.get("wc_sid"));
                String signTypeStr = sqlUtil.transToString(result.get("signType"));
                String status_codeStr = sqlUtil.transToString(result.get("status_code"));
                List<String> canSignSids = bpmHelper.findTaskCanSignUserIds(instanceID);
                String flowTypeStr = sqlUtil.transToString(result.get("FlowType"));
                RestFlowType flowType = RestFlowType.valueOf(flowTypeStr);
                SignType signType = null;
                try {
                    signType = SignType.valueOf(signTypeStr);
                } catch (Exception e) {
                    log.warn("SignType.valueOf(signTypeStr:{})", signTypeStr);
                    log.warn("SignType.valueOf(signTypeStr)", e);
                }
                BpmStatus bpmStatus = null;
                try {
                    bpmStatus = BpmStatus.valueOf(status_codeStr);
                } catch (Exception e) {
                    log.warn(" BpmStatus.valueOf(status_codeStr:{})", status_codeStr);
                    log.warn(" BpmStatus.valueOf(status_codeStr)", e);
                }
                SignInfoVO sv = new SignInfoVO(
                        wc_sid,
                        signType,
                        bpmStatus,
                        // pks,
                        canSignSids,
                        flowType,
                        instanceID);
                return sv;
            }

        } catch (Exception e) {
            log.error("getSignInfo (instanceID:{})", instanceID);
            log.error("getSignInfo", e);
        }
        return null;
    }

    public List<FormInfo> getUnsignFormInfo(final String executorUuid, final Integer compSid) {
        User user = userManager.findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<SignDataVO> unSignVOs = getUnSignData(user.getId());
        List<FormInfo> unSignForm = Lists.newArrayList();
        unSignVOs.forEach(
                item -> {
                    if (compSid != null && !compSid.equals(item.getCompSid())) {
                        return;
                    }
                    unSignForm.add(transToFormInfo(item, user));
                });
        return unSignForm;
    }

    public List<FormInfo> getSignedFormInfo(
            final String executorUuid, final Integer compSid, final Date start, final Date end) {
        User user = userManager.findByUUID(executorUuid);
        Preconditions.checkArgument(user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + executorUuid);
        List<String> bpmIds = Lists.newArrayList();
        Map<String, HistoryTaskTo> historyTaskToMaps = Maps.newHashMap();
        // Map<String, String> executorIdMap = Maps.newHashMap();
        for (WcBpmFlowType wft : WcBpmFlowType.values()) {
            try {
                taskClient
                        .findHistoryByUserAndTimeAndDefinition(user.getId(), start, end,
                                wft.getDefinition())
                        .forEach(
                                item -> {
                                    bpmIds.add(item.getInstanceID());
                                    // List<String> approvers = Lists.newArrayList();
                                    // try {
                                    // approvers.addAll(bpmHelper.getDefaultApprover(item));
                                    // } catch (Exception e) {
                                    // log.error("Call taskService.getDefaultApprover method is
                                    // fail(taskId: " + item.getInstanceID() + "), error cause: " + e.getMessage(),
                                    // e);
                                    // }
                                    // executorIdMap.put(item.getInstanceID(),
                                    // !approvers.contains(item.getExecutorID()) ? item.getExecutorID() :
                                    // user.getId());
                                    historyTaskToMaps.put(item.getInstanceID(), item);
                                });
            } catch (Exception e) {
                log.warn(
                        "getSignedFormInfo (executorUuid:{}, start:{}, end:{}, wft.getDefinition():{})",
                        executorUuid,
                        start,
                        end,
                        wft.getDefinition());
                log.warn("getSignedFormInfo ERROR", e);
            }
        }

        List<SignDataVO> signVOs = this.getSignData(user.getId(), bpmIds);
        List<FormInfo> signFormed = Lists.newArrayList();
        signVOs.forEach(
                item -> {
                    if (compSid != null && !compSid.equals(item.getCompSid())) {
                        return;
                    }
                    FormInfo fi = transToFormInfo(item, user);
                    try {
                        HistoryTaskTo ht = historyTaskToMaps.get(item.getBpm_instance_id());
                        fi.setIsHistoryTask(Boolean.TRUE);
                        fi.setBpmTaskId(ht.getSid());
                        fi.setBpmDefUserId(ht.getUserID());
                        fi.setBpmDefRoleId(ht.getRoleID());
                        List<String> approvers = Lists.newArrayList();
                        try {
                            approvers.addAll(bpmHelper.getDefaultApprover(ht));
                        } catch (Exception e) {
                            log.warn(
                                    "Call taskService.getDefaultApprover method is fail(taskId: "
                                            + ht.getInstanceID()
                                            + "), error cause: "
                                            + e.getMessage(),
                                    e);
                        }
                        fi.setDefApproves(approvers);
                        fi.setExecutorId(ht.getExecutorID());
                        resetAgentInfo(fi, user.getId());
                    } catch (Exception e) {
                        log.warn("getSignedFormInfo (item.getBpm_instance_id():{})",
                                item.getBpm_instance_id());
                        log.warn("getSignedFormInfo ERROR", e);
                    }
                    signFormed.add(fi);
                });
        return signFormed;
    }

    /**
     * 設定代理人資訊 (FormInfo 的 defApproves & executorId需先設值)
     *
     * @param info
     * @param viewerId
     * @return
     */
    public FormInfo resetAgentInfo(FormInfo info, String viewerId) {
        List<String> defApproves = Lists.newArrayList();
        if (info.getDefApproves() != null) {
            defApproves = info.getDefApproves(); // 預設簽核名單
        }
        String executorId = info.getExecutorId(); // 執行者ID
        // 代理他人 = 預設簽核名單不包含檢閱者 且檢閱者ID等於執行者ID
        // 被代理 = 預設簽核名單包含檢閱者 且檢閱者ID不等於執行者ID
        // 本身單據 = 預設簽核名單包含檢閱者 且檢閱者ID等於執行者ID
        if (!defApproves.contains(viewerId) && viewerId.equals(executorId)) {
            if (!defApproves.isEmpty()) {
                info.setBeAgentName(userManager.findById(defApproves.get(0)).getName()); // 顯示被代理人ID
            }
        } else if (defApproves.contains(viewerId) && !viewerId.equals(executorId)) {
            if (!defApproves.isEmpty()) {
                info.setAgentName(userManager.findById(executorId).getName()); // 顯示執行者ID
            }
        }
        return info;
    }

    private FormInfo transToFormInfo(SignDataVO sv, User executor) {
        Org company = orgManager.findBySid(sv.getCompSid());
        FormInfo info = new FormInfo();
        info.setFormId(sv.getBpm_instance_id());
        info.setBpmFlowId(sv.getBpm_instance_id());
        info.setCompany(company.getName());
        info.setCompanyUuid(company.getUuid());
        info.setFormType(FormType.WORK_CONNENT_SUMMARY);
        info.setFormDate(sv.getWc_create_dt()); // 單據建立日期
        List<ProcessTaskBase> pbs = BpmService.getInstance().findSimulationChart(executor.getId(), sv.getBpm_instance_id());
        for (int i = pbs.size() - 1; i >= 0; i--) {
            ProcessTaskBase each = pbs.get(i);
            if (each instanceof ProcessTaskHistory) {
                ProcessTaskHistory history = (ProcessTaskHistory) each;
                try {
                    info.setDefApproves(bpmHelper.getDefaultApprover(history));
                } catch (Exception e) {
                    log.warn(
                            "transToFormInfo (executor.getId():{}, sv.getBpm_instance_id():{})",
                            executor.getId(),
                            sv.getBpm_instance_id());
                    log.warn("transToFormInfo ERROR", e);
                }
                if (history.getExecutorID().equals(executor.getId())
                        || info.getDefApproves().contains(executor.getId())) {
                    info.setSubmittedDate(
                            history.getStartTime() != null
                                    ? history.getStartTime()
                                    : history.getDeliverTime()); // 送簽日期
                    info.setExecutorId(history.getExecutorID());
                    info.setBpmTaskId(history.getSid());
                    info.setIsHistoryTask(Boolean.TRUE);
                    info.setBpmDefUserId(history.getUserID());
                    info.setBpmDefRoleId(history.getRoleID());
                    try {
                        info.setDefApproves(bpmHelper.getDefaultApprover(history));
                    } catch (Exception e) {
                        log.warn(
                                "transToFormInfo (executor.getId():{}, sv.getBpm_instance_id():{})",
                                executor.getId(),
                                sv.getBpm_instance_id());
                        log.warn("transToFormInfo ERROR", e);
                    }
                    this.resetAgentInfo(info, executor.getId());
                    info.setSignedDate(history.getTaskEndTime()); // 簽核日期
                    break;
                }
            }
        }
        if (info.getSubmittedDate() == null) {
            List<String> canSigned = sv.getCanSignSids();
            if (canSigned.contains(executor.getId())) {
                ProcessTaskBase base = pbs.get(pbs.size() - 1);
                if (base instanceof ProcessTask) {
                    ProcessTask task = (ProcessTask) base;
                    info.setSubmittedDate(
                            task.getStartTime() != null ? task.getStartTime()
                                    : task.getDeliverTime()); // 送簽日期
                }
            }
        }
        info.setApplierId(userManager.findBySid(sv.getWc_create_usr()).getId()); // 申請人ID
        info.setApplierName(WkUserUtils.findNameBySid(sv.getWc_create_usr()));
        // 【Portal 單據簽核作業】敘述欄位內容增加 (單號)
        if (!Strings.isNullOrEmpty(sv.getTagInfo())) {

            info.setDescription(
                    WkStringUtils.removeCtrlChr(sv.getTheme())
                            + "(" + sv.getTagInfo() + ")"
                            + "(" + sv.getWc_no() + ")"); // 主題
        } else {
            info.setDescription(WkStringUtils.removeCtrlChr(sv.getTheme()) + "(" + sv.getWc_no() + ")"); // 主題
        }
        if (sv.getFlowType().equals(RestFlowType.REQ)) {
            info.setUrl(
                    FusionUrlServiceUtils.getApPathByPropKey("work-connect.ap.url")
                            + "/worp/worp_full.xhtml?wrcId="
                            + sv.getWc_sid()
                            + "&isFromFormsign=true");
        } else if (sv.getFlowType().equals(RestFlowType.EXEC)) {
            info.setUrl(
                    FusionUrlServiceUtils.getApPathByPropKey("work-connect.ap.url")
                            + "/worp/worp_full.xhtml?wrcId="
                            + sv.getWc_sid()
                            + "&isFromFormsign=true");
        }
        info.setFormStatus(sv.getWcStatus().getVal());
        info.setHasEditMode(Boolean.FALSE);
        if (sv.getCanSignSids() != null && !sv.getCanSignSids().isEmpty()) {
            info.setBeingAgentId(sv.getCanSignSids().get(0));
        }
        return info;
    }

    private SignDataVO getSignDataByInstanceID(String instanceID) {
        List<SignDataVO> datas = Lists.newArrayList();
        try {
            Map<String, String> tagMaps = wcTagService.getTagMaps();
            StringBuilder sql = new StringBuilder();
            // 需求單位流程
            sql.append(" SELECT ");
            sql.append(" wsi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wsi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wsi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wsi.signType as signType,"); // 簽核類型
            sql.append(" wsi.create_dt as create_dt,"); // 建立時間
            sql.append(" wsi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'REQ' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wsi.status_code as status_code, ");
            sql.append(" wc.category_tag as category_tag ");
            sql.append(" FROM wc_manager_sign_info wsi ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wsi.wc_sid ");
            sql.append(" WHERE wsi.bpm_instance_id = '" + instanceID + "' ");
            sql.append(" UNION ALL ");
            // 執行單位流程
            sql.append(" SELECT ");
            sql.append(" wesi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wesi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wesi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wesi.signType as signType,"); // 簽核類型
            sql.append(" wesi.create_dt as create_dt,"); // 建立時間
            sql.append(" wesi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'EXEC' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wesi.status_code as status_code, ");
            sql.append(" wc.category_tag as category_tag ");
            sql.append(" FROM wc_exec_manager_sign_info wesi ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wesi.wc_sid ");
            sql.append(" WHERE wesi.bpm_instance_id = '" + instanceID + "' ");
            List<Map<String, Object>> results = this.jdbc.queryForList(sql.toString());
            Iterator<Map<String, Object>> it = results.iterator();
            while (it.hasNext()) {
                Map<String, Object> result = it.next();
                String wc_sid = sqlUtil.transToString(result.get("wc_sid"));
                String wc_no = sqlUtil.transToString(result.get("wc_no"));
                String bpm_instance_id = sqlUtil.transToString(result.get("bpm_instance_id"));
                String signTypeStr = sqlUtil.transToString(result.get("signType"));
                Date create_dt = sqlUtil.transToDate(result.get("create_dt"));
                Date update_dt = sqlUtil.transToDate(result.get("update_dt"));
                String theme = sqlUtil.transToString(result.get("theme"));
                String flowTypeStr = sqlUtil.transToString(result.get("FlowType"));
                RestFlowType flowType = RestFlowType.valueOf(flowTypeStr);
                SignType signType = null;
                try {
                    signType = SignType.valueOf(signTypeStr);
                } catch (Exception e) {
                    log.warn("SignType.valueOf(signTypeStr:{})", signTypeStr);
                    log.warn("SignType.valueOf(signTypeStr)", e);
                }
                Integer wc_create_usr = sqlUtil.transToInteger(result.get("wc_create_usr"));
                Date wc_create_dt = sqlUtil.transToDate(result.get("wc_create_dt"));
                String wc_statusStr = sqlUtil.transToString(result.get("wc_status"));
                WCStatus wcStatus = null;
                try {
                    wcStatus = WCStatus.valueOf(wc_statusStr);
                } catch (Exception e) {
                    log.warn("WCStatus.valueOf(wc_statusStr:{})", wc_statusStr);
                    log.warn("WCStatus.valueOf(wc_statusStr)", e);
                }
                String statusCodeStr = sqlUtil.transToString(result.get("status_code"));
                BpmStatus bpmStatus = null;
                try {
                    bpmStatus = BpmStatus.valueOf(statusCodeStr);
                } catch (Exception e) {
                    log.warn("BpmStatus.valueOf(statusCodeStr:{})", statusCodeStr);
                    log.warn("BpmStatus.valueOf(statusCodeStr)", e);
                }

                List<String> canSignSids = bpmHelper.findTaskCanSignUserIds(bpm_instance_id);
                Integer compSid = sqlUtil.transToInteger(result.get("comp_sid"));
                SignDataVO sd = new SignDataVO(
                        wc_sid,
                        wc_no,
                        bpm_instance_id,
                        signType,
                        create_dt,
                        update_dt,
                        theme,
                        flowType,
                        null,
                        wc_create_usr,
                        wc_create_dt,
                        wcStatus,
                        canSignSids,
                        compSid,
                        bpmStatus);
                if (result.get("category_tag") != null
                        && !Strings.isNullOrEmpty(sqlUtil.transToString(result.get("category_tag")))) {
                    try {
                        String categoryTagStr = sqlUtil.transToString(result.get("category_tag"));
                        CategoryTagTo ct = WkJsonUtils.getInstance().fromJson(categoryTagStr, CategoryTagTo.class);
                        ct.getTagSids()
                                .forEach(
                                        item -> {
                                            sd.bulidExecTag(item, tagMaps.get(item));
                                        });
                    } catch (Exception e) {
                        log.warn("settingObject ERROR", e);
                    }
                }
                datas.add(sd);
            }
        } catch (Exception e) {
            log.warn("getSignDataByInstanceID (instanceID:{})", instanceID);
            log.warn("getSignDataByInstanceID", e);
        }
        Collections.sort(
                datas,
                new Comparator<SignDataVO>() {
                    @Override
                    public int compare(SignDataVO o1, SignDataVO o2) {
                        Date d1 = (o1.getUpdate_dt() != null) ? o1.getUpdate_dt() : o1.getCreate_dt();
                        Date d2 = (o2.getUpdate_dt() != null) ? o2.getUpdate_dt() : o2.getCreate_dt();
                        return d1.compareTo(d2);
                    }
                });
        if (datas == null || datas.isEmpty()) {
            return null;
        }
        return datas.get(0);
    }

    private String getSameSignDateBpmInstanceId(
            Date signDate, Map<String, Map<String, SignDataVO>> allDatas, String wcSid) {
        if (allDatas.containsKey(wcSid)) {
            for (String bpmInstanceId : allDatas.get(wcSid).keySet()) {
                boolean isSame = false;
                for (Date oldSignDate : allDatas.get(wcSid).get(bpmInstanceId).getSignDates()) {
                    long seconds = (signDate.getTime() - oldSignDate.getTime()) / 1000;
                    if (seconds <= 2 || seconds >= -2) {
                        continue;
                    }
                    isSame = true;
                    break;
                }
                if (isSame) {
                    allDatas.get(wcSid).get(bpmInstanceId).getSignDates().add(signDate);
                    return bpmInstanceId;
                }
            }
        }
        return "";
    }

    private void settingReqSignData(
            Map<String, Map<String, SignDataVO>> allDatas,
            User loginUser,
            Map<String, String> tagMaps,
            Map<String, WCExecDepSetting> execDepSettingMaps,
            List<String> instanceIDs) {
        if (instanceIDs == null || instanceIDs.isEmpty()) {
            return;
        }
        StringBuilder bpmSb = new StringBuilder();
        instanceIDs.forEach(
                item -> {
                    if (!Strings.isNullOrEmpty(bpmSb.toString())) {
                        bpmSb.append(",");
                    }
                    bpmSb.append("'" + item + "'");
                });
        try {
            StringBuilder sql = new StringBuilder();
            // 需求單位流程
            sql.append(" SELECT ");
            sql.append(" wmsd.sign_dt as sign_dt,"); // 簽核時間
            sql.append(" wsi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wsi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wsi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wsi.signType as signType,"); // 簽核類型
            sql.append(" wsi.create_dt as create_dt,"); // 建立時間
            sql.append(" wsi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'REQ' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wsi.status_code as status_code, ");
            sql.append(" wc.category_tag as category_tag ");
            sql.append(" FROM wc_manager_sign_info_detail wmsd ");
            sql.append(
                    " INNER JOIN wc_manager_sign_info wsi ON wsi.wc_manager_sign_info_sid = wmsd.wc_manager_sign_info_sid");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wsi.wc_sid ");
            sql.append(
                    " WHERE wsi.bpm_instance_id in ("
                            + bpmSb
                            + ") AND wmsd.status = 0 AND ((wmsd.signActionType <> '"
                            + SignActionType.RECOVERY
                            + "'  AND wmsd.signActionType <> '"
                            + SignActionType.ROLLBACK
                            + "') OR wmsd.signActionType is null)");

            List<Map<String, Object>> results = this.jdbc.queryForList(sql.toString());
            Iterator<Map<String, Object>> it = results.iterator();
            while (it.hasNext()) {
                Map<String, Object> result = it.next();
                Date signDate = sqlUtil.transToDate(result.get("sign_dt"));
                String wc_sid = sqlUtil.transToString(result.get("wc_sid"));
                SignDataVO sd = null;
                String sameSignDateBpmInstanceId = this.getSameSignDateBpmInstanceId(signDate, allDatas, wc_sid);
                if (Strings.isNullOrEmpty(sameSignDateBpmInstanceId)) {
                    String wc_no = sqlUtil.transToString(result.get("wc_no"));
                    String bpm_instance_id = sqlUtil.transToString(result.get("bpm_instance_id"));
                    String signTypeStr = sqlUtil.transToString(result.get("signType"));
                    Date create_dt = sqlUtil.transToDate(result.get("create_dt"));
                    Date update_dt = sqlUtil.transToDate(result.get("update_dt"));
                    String theme = sqlUtil.transToString(result.get("theme"));
                    String flowTypeStr = sqlUtil.transToString(result.get("FlowType"));
                    RestFlowType flowType = RestFlowType.valueOf(flowTypeStr);
                    SignType signType = null;
                    try {
                        signType = SignType.valueOf(signTypeStr);
                    } catch (Exception e) {
                        log.warn("SignType.valueOf(signTypeStr:{})", signTypeStr);
                        log.warn("SignType.valueOf(signTypeStr)", e);
                    }

                    Integer wc_create_usr = sqlUtil.transToInteger(result.get("wc_create_usr"));
                    Date wc_create_dt = sqlUtil.transToDate(result.get("wc_create_dt"));
                    String wc_statusStr = sqlUtil.transToString(result.get("wc_status"));
                    WCStatus wcStatus = null;
                    try {
                        wcStatus = WCStatus.valueOf(wc_statusStr);
                    } catch (Exception e) {
                        log.warn("WCStatus.valueOf(wc_statusStr:{})", wc_statusStr);
                        log.warn("WCStatus.valueOf(wc_statusStr)", e);
                    }
                    String statusCodeStr = sqlUtil.transToString(result.get("status_code"));
                    BpmStatus bpmStatus = null;
                    try {
                        bpmStatus = BpmStatus.valueOf(statusCodeStr);
                    } catch (Exception e) {
                        log.warn("BpmStatus.valueOf(statusCodeStr:{})", statusCodeStr);
                        log.warn("BpmStatus.valueOf(statusCodeStr)", e);
                    }
                    List<String> canSignSids = bpmHelper.findTaskCanSignUserIds(bpm_instance_id);
                    Integer compSid = sqlUtil.transToInteger(result.get("comp_sid"));
                    sd = new SignDataVO(
                            wc_sid,
                            wc_no,
                            bpm_instance_id,
                            signType,
                            create_dt,
                            update_dt,
                            theme,
                            flowType,
                            signDate,
                            wc_create_usr,
                            wc_create_dt,
                            wcStatus,
                            canSignSids,
                            compSid,
                            bpmStatus);
                    if (result.get("category_tag") != null
                            && !Strings.isNullOrEmpty(
                                    sqlUtil.transToString(result.get("category_tag")))) {
                        try {
                            String categoryTagStr = sqlUtil.transToString(
                                    result.get("category_tag"));
                            CategoryTagTo ct = WkJsonUtils.getInstance()
                                    .fromJson(categoryTagStr, CategoryTagTo.class);
                            this.settingCategoryTagTo(sd, ct, tagMaps);
                        } catch (Exception e) {
                            log.warn("settingObject ERROR", e);
                        }
                    }
                    sameSignDateBpmInstanceId = bpm_instance_id;
                } else {
                    sd = allDatas.get(wc_sid).get(sameSignDateBpmInstanceId);
                }
                if (allDatas.get(wc_sid) == null) {
                    Map<String, SignDataVO> dataMap = Maps.newConcurrentMap();
                    allDatas.put(wc_sid, dataMap);
                }
                allDatas.get(wc_sid).put(sameSignDateBpmInstanceId, sd);
            }
        } catch (Exception e) {
            log.warn(
                    "settingReqSignData (allDatas:{}, tagMaps:{}, instanceIDs:{})",
                    new Gson().toJson(allDatas),
                    new Gson().toJson(tagMaps),
                    new Gson().toJson(instanceIDs));
            log.warn("settingReqSignData", e);
        }
    }

    private void settingCategoryTagTo(SignDataVO sd, CategoryTagTo ct,
            Map<String, String> tagMaps) {
        ct.getTagSids()
                .forEach(
                        item -> {
                            sd.bulidExecTag(item, tagMaps.get(item));
                        });
    }

    @SuppressWarnings("rawtypes")
    private void settingExecSignData(
            Map<String, Map<String, SignDataVO>> allDatas,
            User loginUser,
            Map<String, String> tagMaps,
            Map<String, WCExecDepSetting> wcExecDepSettingMaps,
            List<String> instanceIDs) {
        if (instanceIDs == null || instanceIDs.isEmpty()) {
            return;
        }
        StringBuilder bpmSb = new StringBuilder();
        instanceIDs.forEach(
                item -> {
                    if (!Strings.isNullOrEmpty(bpmSb.toString())) {
                        bpmSb.append(",");
                    }
                    bpmSb.append("'" + item + "'");
                });
        try {
            StringBuilder sql = new StringBuilder();
            // 執行單位流程
            sql.append(" SELECT ");
            sql.append(" wesd.sign_dt as sign_dt,"); // 簽核時間
            sql.append(" wesi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wesi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wesi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wesi.signType as signType,"); // 簽核類型
            sql.append(" wesi.create_dt as create_dt,"); // 建立時間
            sql.append(" wesi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'EXEC' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wesi.status_code as status_code, ");
            sql.append(" wed.wc_exec_dep_setting_sid as wc_exec_dep_setting_sid, ");
            sql.append(" wc.category_tag as category_tag ");
            sql.append(" FROM wc_exec_manager_sign_info_detail wesd ");
            sql.append(
                    " INNER JOIN wc_exec_manager_sign_info wesi ON wesd.wc_exec_manager_sign_info_sid = wesi.wc_exec_manager_sign_info_sid ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wesi.wc_sid ");
            sql.append(
                    " LEFT JOIN wc_exec_dep wed ON wed.wc_exec_manager_sign_info_sid = wesi.wc_exec_manager_sign_info_sid ");
            sql.append(
                    " WHERE wesi.bpm_instance_id in ("
                            + bpmSb
                            + ") AND wesd.status = 0 AND ((wesd.signActionType <> '"
                            + SignActionType.RECOVERY
                            + "' AND wesd.signActionType <> '"
                            + SignActionType.ROLLBACK
                            + "') OR wesd.signActionType is null)");

            List results = this.jdbc.queryForList(sql.toString());
            Iterator it = results.iterator();
            while (it.hasNext()) {
                Map result = (Map) it.next();
                String wc_sid = sqlUtil.transToString(result.get("wc_sid"));
                Date signDate = sqlUtil.transToDate(result.get("sign_dt"));
                SignDataVO sd = null;
                String sameSignDateBpmInstanceId = this.getSameSignDateBpmInstanceId(signDate, allDatas, wc_sid);
                if (Strings.isNullOrEmpty(sameSignDateBpmInstanceId)) {
                    String wc_no = sqlUtil.transToString(result.get("wc_no"));
                    String bpm_instance_id = sqlUtil.transToString(result.get("bpm_instance_id"));
                    String signTypeStr = sqlUtil.transToString(result.get("signType"));
                    Date create_dt = sqlUtil.transToDate(result.get("create_dt"));
                    Date update_dt = sqlUtil.transToDate(result.get("update_dt"));
                    String theme = sqlUtil.transToString(result.get("theme"));
                    String flowTypeStr = sqlUtil.transToString(result.get("FlowType"));
                    RestFlowType flowType = RestFlowType.valueOf(flowTypeStr);
                    SignType signType = null;
                    try {
                        signType = SignType.valueOf(signTypeStr);
                    } catch (Exception e) {
                        log.warn("SignType.valueOf(signTypeStr:{})", signTypeStr);
                        log.warn("SignType.valueOf(signTypeStr)", e);
                    }
                    Integer wc_create_usr = sqlUtil.transToInteger(result.get("wc_create_usr"));
                    Date wc_create_dt = sqlUtil.transToDate(result.get("wc_create_dt"));
                    String wc_statusStr = sqlUtil.transToString(result.get("wc_status"));
                    WCStatus wcStatus = null;
                    try {
                        wcStatus = WCStatus.valueOf(wc_statusStr);
                    } catch (Exception e) {
                        log.warn("WCStatus.valueOf(wc_statusStr:{})", wc_statusStr);
                        log.warn("WCStatus.valueOf(wc_statusStr)", e);
                    }
                    String statusCodeStr = sqlUtil.transToString(result.get("status_code"));
                    BpmStatus bpmStatus = null;
                    try {
                        bpmStatus = BpmStatus.valueOf(statusCodeStr);
                    } catch (Exception e) {
                        log.warn("BpmStatus.valueOf(statusCodeStr:{})", statusCodeStr);
                        log.warn("BpmStatus.valueOf(statusCodeStr)", e);
                    }
                    List<String> canSignSids = bpmHelper.findTaskCanSignUserIds(bpm_instance_id);
                    Integer compSid = sqlUtil.transToInteger(result.get("comp_sid"));
                    sd = new SignDataVO(
                            wc_sid,
                            wc_no,
                            bpm_instance_id,
                            signType,
                            create_dt,
                            update_dt,
                            theme,
                            flowType,
                            signDate,
                            wc_create_usr,
                            wc_create_dt,
                            wcStatus,
                            canSignSids,
                            compSid,
                            bpmStatus);
                    sameSignDateBpmInstanceId = bpm_instance_id;
                } else {
                    sd = allDatas.get(wc_sid).get(sameSignDateBpmInstanceId);
                }
                if (result.get("wc_exec_dep_setting_sid") != null
                        && !Strings.isNullOrEmpty(
                                sqlUtil.transToString(result.get("wc_exec_dep_setting_sid")))
                        && wcExecDepSettingMaps != null
                        && !wcExecDepSettingMaps.isEmpty()
                        && wcExecDepSettingMaps.get(
                                sqlUtil.transToString(result.get("wc_exec_dep_setting_sid"))) != null) {
                    String wc_exec_dep_setting_sid = sqlUtil.transToString(result.get("wc_exec_dep_setting_sid"));
                    WCExecDepSetting wcExecDeppSetting = wcExecDepSettingMaps.get(
                            wc_exec_dep_setting_sid);
                    sd.bulidExecTag(
                            wcExecDeppSetting.getWc_tag_sid(),
                            tagMaps.get(wcExecDeppSetting.getWc_tag_sid()));
                } else if (result.get("category_tag") != null
                        && !Strings.isNullOrEmpty(sqlUtil.transToString(result.get("category_tag")))) {
                    try {
                        String categoryTagStr = sqlUtil.transToString(result.get("category_tag"));
                        CategoryTagTo ct = WkJsonUtils.getInstance().fromJson(categoryTagStr, CategoryTagTo.class);
                        this.settingCategoryTagTo(sd, ct, tagMaps);
                    } catch (Exception e) {
                        log.warn("settingObject ERROR", e);
                    }
                }
                if (allDatas.get(wc_sid) == null) {
                    Map<String, SignDataVO> dataMap = Maps.newConcurrentMap();
                    allDatas.put(wc_sid, dataMap);
                }
                allDatas.get(wc_sid).put(sameSignDateBpmInstanceId, sd);
            }
        } catch (Exception e) {
            log.warn(
                    "settingExecSignData (allDatas:{}, tagMaps:{}, wcExecDepSettingMaps:{}, instanceIDs:{})",
                    new Gson().toJson(allDatas),
                    new Gson().toJson(tagMaps),
                    new Gson().toJson(wcExecDepSettingMaps),
                    new Gson().toJson(instanceIDs));
            log.warn("settingExecSignData", e);
        }
    }

    private List<SignDataVO> getSignData(String userID, List<String> instanceIDs) {
        if (instanceIDs == null || instanceIDs.isEmpty()) {
            return Lists.newArrayList();
        }
        Map<String, Map<String, SignDataVO>> allDatas = Maps.newConcurrentMap();
        try {
            User loginUser = userManager.findById(userID);
            Map<String, String> tagMaps = wcTagService.getTagMaps();
            Map<String, WCExecDepSetting> wcExecDepSettingMaps = execDepSettingService.getAllMap();

            this.settingExecSignData(allDatas, loginUser, tagMaps, wcExecDepSettingMaps,
                    instanceIDs);
            this.settingReqSignData(allDatas, loginUser, tagMaps, wcExecDepSettingMaps,
                    instanceIDs);
        } catch (Exception e) {
            log.warn("getSignData (userID:{}, instanceIDs:{})", userID,
                    new Gson().toJson(instanceIDs));
            log.warn("getSignData", e);
        }
        List<SignDataVO> signedDatas = Lists.newArrayList();
        List<String> wcSids = Lists.newArrayList();
        allDatas
                .values()
                .forEach(
                        item -> {
                            item.keySet()
                                    .forEach(
                                            keyItem -> {
                                                try {
                                                    if (!wcSids.contains(item.get(keyItem).getWc_sid())) {
                                                        signedDatas.add(item.get(keyItem));
                                                        wcSids.add(item.get(keyItem).getWc_sid());
                                                    }
                                                } catch (Exception e) {
                                                    log.warn("getSignData ERROR", e);
                                                }
                                            });
                        });
        Collections.sort(
                signedDatas,
                new Comparator<SignDataVO>() {
                    @Override
                    public int compare(SignDataVO o1, SignDataVO o2) {
                        Date d1 = (o1.getSignDate());
                        Date d2 = (o2.getSignDate());
                        return d1.compareTo(d2);
                    }
                });
        return signedDatas;
    }

    @SuppressWarnings("rawtypes")
    private void settingExecUnSignDatas(
            Map<String, SignDataVO> allDatas, List<String> instanceIDs, Map<String, String> tagMaps) {
        if (instanceIDs == null || instanceIDs.isEmpty()) {
            return;
        }
        StringBuilder bpmSb = new StringBuilder();
        instanceIDs.forEach(
                item -> {
                    if (!Strings.isNullOrEmpty(bpmSb.toString())) {
                        bpmSb.append(",");
                    }
                    bpmSb.append("'" + item + "'");
                });
        try {
            StringBuilder sql = new StringBuilder();
            // 執行單位流程
            sql.append(" SELECT ");
            sql.append(" wesi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wesi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wesi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wesi.signType as signType,"); // 簽核類型
            sql.append(" wesi.create_dt as create_dt,"); // 建立時間
            sql.append(" wesi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'EXEC' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wesi.status_code as status_code, ");
            sql.append(" wed.wc_exec_dep_setting_sid as wc_exec_dep_setting_sid, ");
            sql.append(" wc.category_tag as category_tag ");
            sql.append(" FROM wc_exec_manager_sign_info wesi ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wesi.wc_sid ");
            sql.append(
                    " LEFT JOIN wc_exec_dep wed ON wed.wc_exec_manager_sign_info_sid = wesi.wc_exec_manager_sign_info_sid ");
            sql.append(" WHERE wesi.status_code <> '" + BpmStatus.APPROVED + "' ");
            sql.append(" AND wesi.status_code <> '" + BpmStatus.CLOSED + "' ");
            sql.append(" AND wesi.status_code <> '" + BpmStatus.DELETE + "' ");
            sql.append(" AND wesi.status_code <> '" + BpmStatus.INVALID + "' ");
            sql.append(" AND wc.wc_status <> '" + WCStatus.CLOSE + "' ");
            sql.append(" AND wc.wc_status <> '" + WCStatus.CLOSE_STOP + "' ");
            sql.append(" AND wesi.bpm_instance_id in (" + bpmSb + ")");
            List results = this.jdbc.queryForList(sql.toString());
            Iterator it = results.iterator();
            while (it.hasNext()) {
                Map result = (Map) it.next();
                SignDataVO sd = null;
                String wc_sid = sqlUtil.transToString(result.get("wc_sid"));
                if (!allDatas.containsKey(wc_sid)) {
                    String wc_no = sqlUtil.transToString(result.get("wc_no"));
                    String bpm_instance_id = sqlUtil.transToString(result.get("bpm_instance_id"));
                    String signTypeStr = sqlUtil.transToString(result.get("signType"));
                    Date create_dt = sqlUtil.transToDate(result.get("create_dt"));
                    Date update_dt = sqlUtil.transToDate(result.get("update_dt"));
                    String theme = sqlUtil.transToString(result.get("theme"));
                    String flowTypeStr = sqlUtil.transToString(result.get("FlowType"));
                    RestFlowType flowType = RestFlowType.valueOf(flowTypeStr);
                    SignType signType = null;
                    try {
                        signType = SignType.valueOf(signTypeStr);
                    } catch (Exception e) {
                        log.warn("SignType.valueOf(signTypeStr:{})", signTypeStr);
                        log.warn("SignType.valueOf(signTypeStr)", e);
                    }
                    Integer wc_create_usr = sqlUtil.transToInteger(result.get("wc_create_usr"));
                    Date wc_create_dt = sqlUtil.transToDate(result.get("wc_create_dt"));
                    String wc_statusStr = sqlUtil.transToString(result.get("wc_status"));
                    WCStatus wcStatus = null;
                    try {
                        wcStatus = WCStatus.valueOf(wc_statusStr);
                    } catch (Exception e) {
                        log.warn("WCStatus.valueOf(wc_statusStr:{})", wc_statusStr);
                        log.warn("WCStatus.valueOf(wc_statusStr)", e);
                    }
                    String statusCodeStr = sqlUtil.transToString(result.get("status_code"));
                    BpmStatus bpmStatus = null;
                    try {
                        bpmStatus = BpmStatus.valueOf(statusCodeStr);
                    } catch (Exception e) {
                        log.warn("BpmStatus.valueOf(statusCodeStr:{})", statusCodeStr);
                        log.warn("BpmStatus.valueOf(statusCodeStr)", e);
                    }

                    List<String> canSignSids = bpmHelper.findTaskCanSignUserIds(bpm_instance_id);
                    Integer compSid = sqlUtil.transToInteger(result.get("comp_sid"));
                    sd = new SignDataVO(
                            wc_sid,
                            wc_no,
                            bpm_instance_id,
                            signType,
                            create_dt,
                            update_dt,
                            theme,
                            flowType,
                            null,
                            wc_create_usr,
                            wc_create_dt,
                            wcStatus,
                            canSignSids,
                            compSid,
                            bpmStatus);
                } else {
                    sd = allDatas.get(wc_sid);
                }
                WCExecDepSetting wcExecDeppSetting = null;
                if (result.get("wc_exec_dep_setting_sid") != null
                        && !Strings.isNullOrEmpty(
                                sqlUtil.transToString(result.get("wc_exec_dep_setting_sid")))) {
                    String wc_exec_dep_setting_sid = sqlUtil.transToString(result.get("wc_exec_dep_setting_sid"));
                    wcExecDeppSetting = execDepSettingService.getWCExecDepSettingBySid(wc_exec_dep_setting_sid);
                }
                if (wcExecDeppSetting != null) {
                    sd.bulidExecTag(
                            wcExecDeppSetting.getWc_tag_sid(),
                            tagMaps.get(wcExecDeppSetting.getWc_tag_sid()));
                } else if (result.get("category_tag") != null
                        && !Strings.isNullOrEmpty(sqlUtil.transToString(result.get("category_tag")))) {
                    try {
                        String categoryTagStr = sqlUtil.transToString(result.get("category_tag"));
                        CategoryTagTo ct = WkJsonUtils.getInstance().fromJson(categoryTagStr, CategoryTagTo.class);
                        this.settingCategoryTagTo(sd, ct, tagMaps);
                    } catch (Exception e) {
                        log.warn("settingObject ERROR", e);
                    }
                }
                allDatas.put(wc_sid, sd);
            }
        } catch (Exception e) {
            log.warn(
                    "settingExecUnSignDatas (allDatas:{}, instanceIDs:{}, tagMaps:{})",
                    new Gson().toJson(allDatas),
                    new Gson().toJson(instanceIDs),
                    new Gson().toJson(tagMaps));
            log.warn("settingExecUnSignDatas", e);
        }
    }

    @SuppressWarnings("rawtypes")
    private void settingReqUnSignDatas(
            Map<String, SignDataVO> allDatas, List<String> instanceIDs, Map<String, String> tagMaps) {
        if (instanceIDs == null || instanceIDs.isEmpty()) {
            return;
        }
        StringBuilder bpmSb = new StringBuilder();
        instanceIDs.forEach(
                item -> {
                    if (!Strings.isNullOrEmpty(bpmSb.toString())) {
                        bpmSb.append(",");
                    }
                    bpmSb.append("'" + item + "'");
                });
        try {
            StringBuilder sql = new StringBuilder();
            // 需求單位流程
            sql.append(" SELECT ");
            sql.append(" wsi.wc_sid as wc_sid,"); // 工作聯絡單SID
            sql.append(" wsi.wc_no as wc_no,"); // 工作聯絡單單號
            sql.append(" wsi.bpm_instance_id as bpm_instance_id,"); // 工作聯絡端BPMSID
            sql.append(" wsi.signType as signType,"); // 簽核類型
            sql.append(" wsi.create_dt as create_dt,"); // 建立時間
            sql.append(" wsi.update_dt as update_dt,"); // 更新時間
            sql.append(" wc.theme as theme, ");
            sql.append(" wc.comp_sid as comp_sid, ");
            sql.append(" 'REQ' as FlowType, ");
            sql.append(" wc.create_usr as wc_create_usr, ");
            sql.append(" wc.create_dt as wc_create_dt, ");
            sql.append(" wc.wc_status as wc_status, ");
            sql.append(" wsi.status_code as status_code, ");
            sql.append(" wc.category_tag as category_tag ");
            sql.append(" FROM wc_manager_sign_info wsi ");
            sql.append(" INNER JOIN wc_master wc ON wc.wc_sid = wsi.wc_sid ");
            sql.append(" WHERE wsi.status_code <> '" + BpmStatus.APPROVED + "' ");
            sql.append(" AND wsi.status_code <> '" + BpmStatus.CLOSED + "' ");
            sql.append(" AND wsi.status_code <> '" + BpmStatus.DELETE + "' ");
            sql.append(" AND wsi.status_code <> '" + BpmStatus.INVALID + "' ");
            sql.append(" AND wc.wc_status <> '" + WCStatus.CLOSE + "' ");
            sql.append(" AND wc.wc_status <> '" + WCStatus.CLOSE_STOP + "' ");
            sql.append(" AND wsi.bpm_instance_id in (" + bpmSb + ")");
            List results = this.jdbc.queryForList(sql.toString());
            Iterator it = results.iterator();
            while (it.hasNext()) {
                Map result = (Map) it.next();
                String wc_sid = sqlUtil.transToString(result.get("wc_sid"));
                SignDataVO sd = null;
                if (!allDatas.containsKey(wc_sid)) {
                    String wc_no = sqlUtil.transToString(result.get("wc_no"));
                    String bpm_instance_id = sqlUtil.transToString(result.get("bpm_instance_id"));
                    String signTypeStr = sqlUtil.transToString(result.get("signType"));
                    Date create_dt = sqlUtil.transToDate(result.get("create_dt"));
                    Date update_dt = sqlUtil.transToDate(result.get("update_dt"));
                    String theme = sqlUtil.transToString(result.get("theme"));
                    String flowTypeStr = sqlUtil.transToString(result.get("FlowType"));
                    RestFlowType flowType = RestFlowType.valueOf(flowTypeStr);
                    SignType signType = null;
                    try {
                        signType = SignType.valueOf(signTypeStr);
                    } catch (Exception e) {
                        log.warn("SignType.valueOf(signTypeStr:{})", signTypeStr);
                        log.warn("SignType.valueOf(signTypeStr)", e);
                    }
                    Integer wc_create_usr = sqlUtil.transToInteger(result.get("wc_create_usr"));
                    Date wc_create_dt = sqlUtil.transToDate(result.get("wc_create_dt"));
                    String wc_statusStr = sqlUtil.transToString(result.get("wc_status"));
                    WCStatus wcStatus = null;
                    try {
                        wcStatus = WCStatus.valueOf(wc_statusStr);
                    } catch (Exception e) {
                        log.warn("WCStatus.valueOf(wc_statusStr:{})", wc_statusStr);
                        log.warn("WCStatus.valueOf(wc_statusStr)", e);
                    }
                    String statusCodeStr = sqlUtil.transToString(result.get("status_code"));
                    BpmStatus bpmStatus = null;
                    try {
                        bpmStatus = BpmStatus.valueOf(statusCodeStr);
                    } catch (Exception e) {
                        log.warn("BpmStatus.valueOf(statusCodeStr:{})", statusCodeStr);
                        log.warn("BpmStatus.valueOf(statusCodeStr)", e);
                    }

                    List<String> canSignSids = bpmHelper.findTaskCanSignUserIds(bpm_instance_id);
                    Integer compSid = sqlUtil.transToInteger(result.get("comp_sid"));
                    sd = new SignDataVO(
                            wc_sid,
                            wc_no,
                            bpm_instance_id,
                            signType,
                            create_dt,
                            update_dt,
                            theme,
                            flowType,
                            null,
                            wc_create_usr,
                            wc_create_dt,
                            wcStatus,
                            canSignSids,
                            compSid,
                            bpmStatus);
                    if (result.get("category_tag") != null
                            && !Strings.isNullOrEmpty(
                                    sqlUtil.transToString(result.get("category_tag")))) {
                        try {
                            String categoryTagStr = sqlUtil.transToString(
                                    result.get("category_tag"));
                            CategoryTagTo ct = WkJsonUtils.getInstance()
                                    .fromJson(categoryTagStr, CategoryTagTo.class);
                            this.settingCategoryTagTo(sd, ct, tagMaps);
                        } catch (Exception e) {
                            log.warn("settingObject ERROR", e);
                        }
                    }
                } else {
                    sd = allDatas.get(wc_sid);
                }
                allDatas.put(wc_sid, sd);
            }
        } catch (Exception e) {
            log.warn(
                    "settingReqUnSignDatas (allDatas:{}, instanceIDs():{}, tagMaps:{})",
                    new Gson().toJson(allDatas),
                    new Gson().toJson(instanceIDs),
                    new Gson().toJson(tagMaps));
            log.warn("settingReqUnSignDatas", e);
        }
    }

    private List<SignDataVO> getUnSignData(String userID) {
        Map<String, SignDataVO> allDatas = Maps.newConcurrentMap();
        try {
            Map<String, String> tagMaps = wcTagService.getTagMaps();
            List<String> definitionList = Stream.of(WcBpmFlowType.values())
                    .map(WcBpmFlowType::getDefinition)
                    .collect(Collectors.toList());
            List<ProcessTask> tasks = BpmService.getInstance().findTaskByUserIdAndDefinitions(userID, definitionList);
            List<String> instanceIDList = tasks.stream().map(ProcessTask::getInstanceID).collect(Collectors.toList());
            this.settingReqUnSignDatas(allDatas, instanceIDList, tagMaps);
            this.settingExecUnSignDatas(allDatas, instanceIDList, tagMaps);
        } catch (Exception e) {
            log.warn("getUnSignData(userID:{})", userID);
            log.warn("getUnSignData", e);
        }
        List<SignDataVO> unsignDatas = Lists.newArrayList();
        allDatas
                .values()
                .forEach(
                        item -> {
                            unsignDatas.add(item);
                        });
        Collections.sort(
                unsignDatas,
                new Comparator<SignDataVO>() {
                    @Override
                    public int compare(SignDataVO o1, SignDataVO o2) {
                        Date d1 = (o1.getUpdate_dt() != null) ? o1.getUpdate_dt() : o1.getCreate_dt();
                        Date d2 = (o2.getUpdate_dt() != null) ? o2.getUpdate_dt() : o2.getCreate_dt();
                        return d1.compareTo(d2);
                    }
                });
        return unsignDatas;
    }

    /**
     * 取得待簽筆數
     *
     * @param userID 登入者ID
     * @return
     */
    public Map<String, Map<QueryType, List<TodoSummary>>> getUnSignCount(String userID) {
        Map<String, Map<QueryType, List<TodoSummary>>> result = Maps.newConcurrentMap();
        List<SignDataVO> unSignData = getUnSignData(userID);
        unSignData.forEach(
                item -> {
                    try {
                        Org comp = orgManager.findBySid(item.getCompSid());
                        Map<QueryType, List<TodoSummary>> qDetail = result.get(comp.getId());
                        if (qDetail == null) {
                            qDetail = Maps.newConcurrentMap();
                        }
                        QueryType qtype = null;

                        qtype = QueryType.NOT_EMERGENCY;
                        List<TodoSummary> todoSummary = qDetail.get(qtype);
                        List<String> userIds = Lists.newArrayList();
                        try {
                            userIds.add(item.getCanSignSids().get(0));
                        } catch (Exception e1) {
                            log.warn("取得預設簽核人員列表失敗, 錯誤原因: " + e1.getMessage(), e1);
                        }
                        if (!CollectionUtils.isEmpty(userIds)) {
                            if (todoSummary == null || todoSummary.isEmpty()) {
                                todoSummary = Lists.newArrayList();
                                Map<String, Integer> userIdCount = Maps.newHashMap();
                                String userId = userIds.get(0);
                                if (userIdCount.containsKey(userId)) {
                                    int value = userIdCount.get(userId);
                                    userIdCount.put(userId, ++value);
                                } else {
                                    userIdCount.put(userId, 1);
                                }
                                TodoSummary ts = new TodoSummary(
                                        FormType.WORK_CONNENT_SUMMARY, comp.getId(), qtype,
                                        userIdCount);
                                todoSummary.add(ts);
                            } else {
                                TodoSummary ts = todoSummary.get(0);
                                String userId = userIds.get(0);
                                Map<String, Integer> userIdCount = ts.getUserIdCount();
                                if (userIdCount.containsKey(userId)) {
                                    int value = userIdCount.get(userId);
                                    userIdCount.put(userId, ++value);
                                } else {
                                    userIdCount.put(userId, 1);
                                }
                                TodoSummary updateTs = new TodoSummary(
                                        FormType.WORK_CONNENT_SUMMARY, comp.getId(), qtype,
                                        userIdCount);
                                todoSummary = Lists.newArrayList();
                                todoSummary.add(updateTs);
                            }
                            qDetail.put(qtype, todoSummary);
                            result.put(comp.getId(), qDetail);
                        }
                    } catch (Exception e) {
                        log.warn("getUnSignCount (userID:{})", userID);
                        log.warn("getUnSignCount", e);
                    }
                });
        return result;
    }

    private void doAction(
            ActionType actType,
            String wcSid,
            String wcNo,
            User executor,
            Map<String, Object> inputData) throws UserMessageException {
        
        //防呆
        if(inputData==null) {
            inputData = Maps.newHashMap();
        }

        // ====================================
        // log
        // ====================================
        RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
        String session = "NO_SESSION";
        if (attrs != null) {
            session = attrs.getSessionId().hashCode() + "";
        }

        String execInfo = "[%s]執行者:[%s (%s)]，單據:[%s]，行為:[%s]-";
        execInfo = String.format(execInfo,
                session,
                executor.getName(),
                executor.getSid(),
                wcNo,
                actType.getDescr());

        log.debug(execInfo + "START");

        // ====================================
        // 取得其他傳入參數 (不一定有)
        // ====================================
        // comment (原因、理由、說明)
        String comment = inputData.containsKey(ButtonInfoWithOneTextarea.INPUT_TEXTAREA)
                ? Strings.nullToEmpty(
                        (String) inputData.get(ButtonInfoWithOneTextarea.INPUT_TEXTAREA))
                : "";

        // 退回節點 SID
        String rollBackTaskSid = inputData.containsKey(ButtonInfoWithOneSelectMenuAndOneTextarea.SELECT_ITEMS)
                ? Strings.nullToEmpty(
                        (String) inputData.get(ButtonInfoWithOneSelectMenuAndOneTextarea.SELECT_ITEMS))
                : "";

        // ====================================
        // 執行簽核動作
        // ====================================
        switch (actType) {
        // -------------------
        // 需求單位-簽名、會簽
        // -------------------
        case REQ_FLOW_SIGN:
            this.doReqflowSign(wcSid, executor);
            break;

        // -------------------
        // 需求單位-復原
        // -------------------
        case REQ_FLOW_RECOVERY:
            this.reqFlowActionHelper.doReqFlowRecovery(wcSid, executor.getSid());
            break;

        // -------------------
        // 需求單位-退回
        // -------------------
        case REQ_FLOW_ROLLBACK:
            // 退回
            this.reqFlowActionHelper.doReqFlowRollBack(wcSid, executor.getSid(), rollBackTaskSid, comment);
            break;

        // -------------------
        // 需求單位-作廢
        // -------------------
        case REQ_FLOW_INVAILD:
            this.reqFlowActionHelper.doReqFlowInvalid(wcSid, executor.getSid(), comment);
            break;

        // -------------------
        // 需求單位會簽-復原
        // -------------------
        case REQ_FLOW_COUNTER_RECOVERY:
            this.reqFlowActionHelper.doReqflowCounterSignRecovery(wcSid, executor.getSid());
            break;

        // -------------------
        // 執行單位-簽名
        // -------------------
        case EXEC_FLOW_SIGN:
            this.execFlowActionHelper.doExecFlowSign(wcSid, executor.getSid());
            break;

        // -------------------
        // 執行單位-復原
        // -------------------
        case EXEC_FLOW_RECOVERY:
            this.execFlowActionHelper.doExecFlowRecovery(wcSid, executor.getSid());
            break;

        // -------------------
        // 執行方-簽核者退回
        // -------------------
        case EXEC_FLOW_SIGNER_ROLLBACK:
            // 執行
            this.execFlowActionHelper.doExecFlowRollBack(wcSid, executor.getSid(), rollBackTaskSid, comment);
            break;

        // -------------------
        // 執行方-單位成員退回 (formSign 未開放執行此項目)
        // -------------------
        case EXEC_FLOW_MEMBER_ROLLBACK:
            // 執行
            this.execFlowActionHelper.doExecFlowMemberRollBack(wcSid, executor.getSid(), rollBackTaskSid, comment);
            break;

        // -------------------
        // 執行方-簽核者不執行
        // -------------------
        case EXEC_FLOW_SIGNER_STOP:
            // 執行
            execFlowActionHelper.doExecFlowSignerStop(wcSid, executor.getSid(), comment);
            break;

        // -------------------
        // 執行方-單位成員不執行 (formSign 未開放執行此項目)
        // -------------------
        case EXEC_FLOW_MEMBER_STOP:
            // 執行
            execFlowActionHelper.doExecFlowMemberStop(wcSid, executor.getSid(), comment);
            break;

        default:
            String errorMessage = WkMessage.EXECTION + "(未定義的操作類型:[" + actType + "])";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage);
        }

        log.debug(execInfo + "FINISH");
    }

    /**
     * 執行需求流程簽核(需求流程 & 會簽)
     *
     * @param wcSid    工作聯絡單Sid
     * @param signUser 執行者
     * @throws UserMessageException
     */
    public void doReqflowSign(String wcSid, User signUser) throws UserMessageException {

        // 為什麼 form sign 主流程和會簽要合併一個方法.. 搞誰阿..
        // TODO 待拆開

        // ====================================
        // 取得主檔資料
        // ====================================
        WCMaster wcMaster = wcMasterManager.findBySid(wcSid);

        if (wcMaster == null || wcMaster.getWc_status() == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(主檔資料不存在 sid:[" + wcSid + "])";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 狀態檢查
        // ====================================
        this.wcMasterHelper.checkReqFlowSignWCMasterStatus(wcMaster, false);

        // ====================================
        // 查詢『待簽』的需求方流程 （主要簽核者、代理人）
        // ====================================
        // 查詢所有會簽
        List<WCManagerSignInfo> activeSignInfos = this.wcManagerSignInfoManager.findActiveReqFlowSignInfos(wcSid);
        // 比對傳入使用者待簽的流程
        List<WCManagerSignInfo> canSignInfos = Lists.newArrayList();
        for (WCManagerSignInfo reqSignInfo : activeSignInfos) {
            try {
                if (this.bpmManager.isSignUserWithException(signUser.getId(), reqSignInfo.getBpmId())) {
                    canSignInfos.add(reqSignInfo);
                }
            } catch (SystemOperationException e) {
                throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
            }
        }

        if (WkStringUtils.isEmpty(canSignInfos)) {
            String errorMessage = WkMessage.NEED_RELOAD + "(您已不在流程可簽核節點)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 逐筆執行
        // ====================================
        for (WCManagerSignInfo canSignInfo : canSignInfos) {
            try {
                if (SignType.SIGN.equals(canSignInfo.getSignType())) {
                    // 主流程簽核
                    this.reqFlowActionHelper.doReqflowSign(wcSid, signUser.getSid());
                } else {
                    // 會簽
                    this.reqFlowActionHelper.doReqflowCounterSign(wcSid, signUser.getSid());
                }
            } catch (UserMessageException e) {
                throw e;
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "(需求單位簽核失敗)";
                log.error(errorMessage + " [" + e.getMessage() + "] ,[" + canSignInfo.getBpmId() + "]", e);
                throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
            }
        }
    }
}
