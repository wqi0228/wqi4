/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.config;

import com.antkorwin.xsync.XSync;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jimmy_chou
 */
@Configuration
public class XSyncConfig {

    @Bean
    public XSync<Integer> intXSync() {
        return new XSync<>();
    }

    @Bean
    public XSync<String> xSync() {
        return new XSync<>();
    }
}
