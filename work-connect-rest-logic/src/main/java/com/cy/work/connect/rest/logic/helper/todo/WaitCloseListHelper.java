/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper.todo;

import com.cy.work.connect.rest.logic.common.Constants;
import com.cy.work.connect.rest.logic.to.WCTodoTo;
import com.cy.work.connect.rest.logic.to.enums.WCToDoType;
import com.cy.work.connect.vo.enums.WCStatus;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 待結案
 *
 * @author kasim
 */
@Component
@Slf4j
public class WaitCloseListHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -212801656991211128L;

    @Autowired
    @Qualifier(Constants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    /**
     * 查詢 待結案 數量
     *
     * @param loginUserSid
     * @return
     */
    public WCTodoTo findByWaitCloseList(Integer loginUserSid) {
        try {
            int count = jdbc.queryForObject(this.bulidSqlByWaitCloseList(loginUserSid),
                Integer.class);
            if (count > 0) {
                return new WCTodoTo(WCToDoType.WAIT_CLOSE_LIST, count);
            }
        } catch (Exception e) {
            log.warn("查詢 待閱得 數量 ERROR!!", e);
        }
        return null;
    }

    /**
     * 建立主要 Sql
     *
     * @param depSids
     * @return
     */
    private String bulidSqlByWaitCloseList(Integer loginUserSid) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ").append("\r\n");
        sql.append("        count(distinct wc.wc_sid) ").append("\r\n");
        sql.append("FROM wc_master wc ").append("\r\n");
        sql.append("WHERE wc.create_usr in (").append(loginUserSid).append(") ").append("\r\n");
        sql.append("AND wc.wc_status in (")
            .append("'" + WCStatus.EXEC_FINISH.name() + "'")
            .append(") ")
            .append("\r\n");

        return sql.toString();
    }
}
