/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.commons.vo.User;
import com.cy.work.connect.rest.logic.service.BpmService;
import com.cy.work.connect.rest.logic.service.WCManagerSignInfoService;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WCManagerSignInfoRestCheckHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6207668116978716952L;

    @Autowired
    private WCManagerSignInfoService wcManagerSignInfoService;
    @Autowired
    private BPMHelper bpmHelper;

    /**
     * 檢測需求單位流程是否已核准,並且回傳需求單位流程物件
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public WCManagerSignInfo getApprovedReqFlow(String wcSid) {
        WCManagerSignInfo reqFlow = this.getReqFlow(wcSid);
        Preconditions.checkState(reqFlow.getStatus().equals(BpmStatus.APPROVED), "需求單位非核准狀態，請確認！！");
        return reqFlow;
    }

    /**
     * 取得需求流程物件
     *
     * @param wcSid
     * @return
     */
    public WCManagerSignInfo getReqFlow(String wcSid) {
        List<WCManagerSignInfo> flowReqs = wcManagerSignInfoService.getFlowSignInfos(wcSid);
        WCManagerSignInfo reqFlow = flowReqs.get(0);
        Preconditions.checkState(!BpmStatus.DELETE.equals(reqFlow.getStatus()), "聯絡單已被作廢 請確認!!");
        return reqFlow;
    }

    /**
     * 取得可簽核需求流程
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 執行者
     * @return
     */
    public List<WCManagerSignInfo> getSignManagerSignInfos(String wcSid, User loginUser) {
        List<WCManagerSignInfo> allWCManagerSignInfo =
            wcManagerSignInfoService.getAllFlowSignInfos(wcSid);
        List<WCManagerSignInfo> activeWCManagerSignInfos =
            allWCManagerSignInfo.stream()
                .filter(
                    each ->
                        !each.getStatus().equals(BpmStatus.DELETE)
                            && !each.getStatus().equals(BpmStatus.WAIT_CREATE))
                .collect(Collectors.toList());
        List<WCManagerSignInfo> result = Lists.newArrayList();
        activeWCManagerSignInfos.forEach(
            item -> {
                if (bpmHelper.isShowSign(loginUser.getId(), item.getBpmId())) {
                    result.add(item);
                }
            });
        Preconditions.checkState(result.size() > 0, "執行BPM簽名，比對簽核人員不符，請確認！！");
        return result;
    }

    public List<WCManagerSignInfo> getContinueRecoverySignInfo(String wcSid, User loginUser) {
        List<WCManagerSignInfo> allWCManagerSignInfo =
            wcManagerSignInfoService.getContinueSignInfos(wcSid);
        List<WCManagerSignInfo> activeWCManagerSignInfos =
            allWCManagerSignInfo.stream()
                .filter(each -> !each.getStatus().equals(BpmStatus.INVALID))
                .collect(Collectors.toList());
        List<WCManagerSignInfo> result = Lists.newArrayList();
        activeWCManagerSignInfos.forEach(
            item -> {
                List<ProcessTaskBase> tasks =
                    BpmService.getInstance()
                        .findSimulationChart(loginUser.getId(), item.getBpmId());
                if (wcManagerSignInfoService.isShowContinueRecovery(loginUser, tasks, wcSid,
                    null)) {
                    result.add(item);
                }
            });
        Preconditions.checkState(result.size() > 0, "無權限進行復原！！");
        return result;
    }

   
}
