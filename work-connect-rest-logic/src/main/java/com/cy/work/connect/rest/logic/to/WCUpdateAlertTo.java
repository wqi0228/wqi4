/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to;

import com.cy.work.connect.rest.logic.to.enums.WCReadAlertType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * rest / clent 使用 - 更新訊息
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WCUpdateAlertTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3595749215038744047L;

    @JsonProperty("sid")
    /** 工作聯絡單 Sid */
    private String sid;

    @JsonProperty("recipients")
    /** 收件人 */
    private List<Integer> recipients;

    @JsonProperty("type")
    /** 讀取訊息類型 */
    private WCReadAlertType type;

    public WCUpdateAlertTo(String sid, List<Integer> recipients, WCReadAlertType type) {
        this.sid = sid;
        this.recipients = recipients;
        this.type = type;
    }
}
