/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper.todo;

import com.cy.work.connect.rest.logic.common.Constants;
import com.cy.work.connect.rest.logic.to.WCTodoTo;
import com.cy.work.connect.rest.logic.to.enums.WCToDoType;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 處理清單 查詢邏輯
 *
 * @author kasim
 */
@Component
@Slf4j
public class ProcessListHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8868002901871487804L;

    @Autowired
    @Qualifier(Constants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    /**
     * 查詢 待處理 數量
     *
     * @param userSid
     * @return
     */
    public WCTodoTo findByProcessList(Integer userSid) {
        try {
            int count = jdbc.queryForObject(this.bulidSqlByProcessList(userSid), Integer.class);
            if (count > 0) {
                return new WCTodoTo(WCToDoType.PROCESS_LIST, count);
            }
        } catch (Exception e) {
            log.warn("查詢 待處理 數量 ERROR!!", e);
        }
        return null;
    }

    /**
     * 建立主要 Sql
     *
     * @param userSid
     * @return
     */
    private String bulidSqlByProcessList(Integer userSid) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        sql.append("        count(distinct wc.wc_sid) ");
        sql.append("FROM wc_master wc ");
        sql.append("INNER JOIN ( ");
        sql.append("    SELECT ");
        sql.append("        * ");
        sql.append("    FROM wc_exec_dep execDep ");
        sql.append("    WHERE execDep.status = 0 ");
        sql.append("    AND execDep.exec_user_sid = ").append(userSid).append(" ");
        sql.append("    AND execDep.exec_dep_status = 'PROCEDD'  ");
        sql.append(") execDep ON wc.wc_sid = execDep.wc_sid ");
        return sql.toString();
    }
}
