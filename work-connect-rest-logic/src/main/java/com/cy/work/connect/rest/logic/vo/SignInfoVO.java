/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.vo;

import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.RestFlowType;
import com.cy.work.connect.vo.enums.SignType;
import java.util.List;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class SignInfoVO {

    @Getter
    private final String wc_sid;
    @Getter
    private final SignType signType;
    @Getter
    private final BpmStatus bpmStatus;
    //    @Getter
    //    private List<ProcessTaskBase> pks;
    @Getter
    private final List<String> canSignSids;
    @Getter
    private final RestFlowType flowType;
    @Getter
    private final String instanceID;

    public SignInfoVO(
        String wc_sid,
        SignType signType,
        BpmStatus bpmStatus,
        // List<ProcessTaskBase> pks,
        List<String> canSignSids,
        RestFlowType flowType,
        String instanceID) {
        this.signType = signType;
        this.bpmStatus = bpmStatus;
        // this.pks = pks;
        this.canSignSids = canSignSids;
        this.flowType = flowType;
        this.wc_sid = wc_sid;
        this.instanceID = instanceID;
    }
}
