/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.repository.WCExecManagerSignInfoDetailRepository;
import com.cy.work.connect.repository.WCExecManagerSignInfoRepository;
import com.cy.work.connect.rest.logic.helper.BPMHelper;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCExecManagerSignInfoDetail;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignActionType;
import com.cy.work.connect.vo.enums.SignType;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 執行方簽核資訊 Service
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCExecManagerSignInfoService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4930866350367294584L;

    @Autowired
    private TaskClient taskClient;
    @Autowired
    private BPMHelper bpmHelper;
    @Autowired
    private WCExecManagerSignInfoRepository wcExecManagerSignInfoRepository;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCExecManagerSignInfoDetailRepository wcExecManagerSignInfoDetailRepository;
    @Autowired
    private WCExceDepService wcExceDepService;

    public WCExecManagerSignInfo getWCExecManagerSignInfoBySid(String sid) {
        return wcExecManagerSignInfoRepository.findOne(sid);
    }

    public int getMaxSeq(String wcSid, int groupSeq) {
        Integer seq = wcExecManagerSignInfoRepository.findMaxSeqByWcSidAndGroupSeq(wcSid, groupSeq);
        int maxSeq = ((seq == null) ? 0 : seq) + 1;
        return maxSeq;
    }

    /**
     * 取得所有執行單位簽核資訊 (執行單位流程&會簽)
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<WCExecManagerSignInfo> getAllFlowSignInfos(String wcSid) {
        List<WCExecManagerSignInfo> managerSignInfos = wcExecManagerSignInfoRepository.findByWcSid(wcSid);
        if (managerSignInfos == null || managerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<WCExecManagerSignInfo> activeWCManagerSignInfos = managerSignInfos.stream()
                .filter(each -> !each.getStatus().equals(BpmStatus.DELETE))
                .collect(Collectors.toList());
        return activeWCManagerSignInfos;
    }

    /**
     * 取得執行單位流程簽核資訊
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<WCExecManagerSignInfo> getFlowSignInfos(String wcSid) {
        List<WCExecManagerSignInfo> managerSignInfos = wcExecManagerSignInfoRepository.findByWcSid(wcSid);
        if (managerSignInfos == null || managerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<WCExecManagerSignInfo> activeWCManagerSignInfos = managerSignInfos.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                                && each.getSignType().equals(SignType.SIGN))
                .collect(Collectors.toList());
        return activeWCManagerSignInfos;
    }

    public void deleteExecSignInfo(String wc_ID, User loginUser) {
        List<WCExecManagerSignInfo> execFlows = this.getFlowSignInfos(wc_ID);
        for (WCExecManagerSignInfo item : execFlows) {
            try {
                WCExecManagerSignInfo wcExecManagerSignInfo = wcExecManagerSignInfoRepository.findOne(item.getSid());
                if (!Strings.isNullOrEmpty(item.getBpmId())) {
                    bpmHelper.invalid(loginUser.getId(), item.getBpmId());
                }
                this.updateBpmStatus(loginUser.getId(), wcExecManagerSignInfo, BpmStatus.DELETE);
            } catch (Exception e) {
                log.warn("deleteExecSignInfo ERROR", e);
            }
        }
    }

    /**
     * 檢測是否有(任何一個)再會簽簽核
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isCheckAnyContinue(String wcSid) {
        for (WCExecManagerSignInfo si : this.getContinueSignInfos(wcSid)) {
            if (!si.getStatus().equals(BpmStatus.DELETE)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 檢測是否(任何一個)核准者簽核節點已有簽名
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isCheckAnyFlowSign(String wcSid) {
        for (WCExecManagerSignInfo si : getFlowSignInfos(wcSid)) {
            if (si.getStatus().equals(BpmStatus.APPROVED) || si.getStatus()
                    .equals(BpmStatus.INVALID)) {
                return true;
            }
        }
        return false;
    }

    public void clearContinueSignInfo(String wc_ID, User loginUser) {
        this.getContinueSignInfos(wc_ID)
                .forEach(
                        item -> {
                            try {
                                this.doClearBpmContinueSign(
                                        loginUser.getId(), this.getWCExecManagerSignInfoBySid(item.getSid()));
                            } catch (Exception e) {
                                log.warn("doRollBackByExecDep ERROR", e);
                            }
                        });
    }

    /**
     * 檢測是否(任何一個)核准者簽核節點已有簽名
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isCheckAnyContinueSign(String wcSid) {
        for (WCExecManagerSignInfo si : this.getContinueSignInfos(wcSid)) {
            if (si.getStatus().equals(BpmStatus.APPROVED) || si.getStatus()
                    .equals(BpmStatus.INVALID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 此為清除會簽節點的BPM資訊, 會簽節點會根據需求流程節點簽名完畢,才會產生 若有人進行退回或復原 該BPM節點必須清除,但會簽人員資訊不需要再重新挑選
     *
     * @param userId   清除節點User ID
     * @param signInfo 簽核物件
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doClearBpmContinueSign(String userId, WCExecManagerSignInfo signInfo)
            throws ProcessRestException {
        bpmHelper.invalid(userId, signInfo.getBpmId());
        signInfo.setBpmId("");
        signInfo.setStatus(BpmStatus.WAIT_CREATE);
        signInfo.setUpdate_dt(new Date());
        signInfo.setBpmDefaultSignedName("");
        signInfo = wcExecManagerSignInfoRepository.save(signInfo);
    }

    public List<WCExecManagerSignInfo> getContinueSignInfos(String wc_ID) {
        List<WCExecManagerSignInfo> managerSignInfos = wcExecManagerSignInfoRepository.findByWcSid(wc_ID);
        if (managerSignInfos == null || managerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<WCExecManagerSignInfo> activeWCManagerSignInfos = managerSignInfos.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                                && each.getSignType().equals(SignType.COUNTERSIGN))
                .collect(Collectors.toList());
        return activeWCManagerSignInfos;
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateBpmStatus(String userId, WCExecManagerSignInfo signInfo, BpmStatus status)
            throws ProcessRestException {
        Preconditions.checkState(status != null, "BpmStatus 為NULL 無法更新簽核資訊，請確認！！");
        signInfo.setStatus(status);
        signInfo.setUpdate_dt(new Date());
        wcExecManagerSignInfoRepository.save(signInfo);
    }

    public void updateSignData(
            WCExecManagerSignInfo wcManagerSignInfo,
            List<ProcessTaskBase> tasks,
            SignActionType signActionType,
            Integer signUserSid) {
        try {
            // 將舊資料修改成無效資料
            wcExecManagerSignInfoDetailRepository.updateStatus(
                    Activation.INACTIVE, wcManagerSignInfo.getSid());
            // 由於復原及退回在單據歷史紀錄是無法查詢的,所以必須更新狀態
            if (signActionType != null
                    && (signActionType.equals(SignActionType.RECOVERY)
                            || signActionType.equals(SignActionType.ROLLBACK))
                    && signUserSid != null) {
                wcExecManagerSignInfoDetailRepository.updateSignActionType(
                        signActionType, wcManagerSignInfo.getSid(), signUserSid);
            }
            if (tasks != null && !tasks.isEmpty()) {
                tasks.forEach(
                        item -> {
                            try {
                                if (item instanceof ProcessTaskHistory) {
                                    ProcessTaskHistory ph = (ProcessTaskHistory) item;
                                    User user = userManager.findById(ph.getExecutorID());
                                    WCExecManagerSignInfoDetail wd = new WCExecManagerSignInfoDetail();
                                    wd.setSign_dt(ph.getTaskEndTime());
                                    wd.setStatus(Activation.ACTIVE);
                                    wd.setUser_sid(user.getSid());
                                    wd.setWc_exec_manager_sign_info_sid(wcManagerSignInfo.getSid());
                                    wcExecManagerSignInfoDetailRepository.save(wd);
                                }
                            } catch (Exception e) {
                                log.warn("updateSignData ERROR", e);
                            }
                        });
            }
        } catch (Exception e) {
            log.warn("updateSignData ERROR", e);
        }
    }

    @SuppressWarnings("deprecation")
    public WCExecManagerSignInfo doSyncBpmData(
            WCExecManagerSignInfo wcManagerSignInfo, SignActionType signActionType, Integer signUserSid)
            throws ProcessRestException {
        List<ProcessTaskBase> tasks = Lists.newArrayList();

        try {
            if (!Strings.isNullOrEmpty(wcManagerSignInfo.getBpmId())) {
                tasks = taskClient.findSimulationChart(wcManagerSignInfo.getBpmId());
            }
        } catch (Exception e) {
            log.warn("findSimulationChart ERROR", e);
        }

        updateSignData(wcManagerSignInfo, tasks, signActionType, signUserSid);
        wcManagerSignInfo.setBpmDefaultSignedName("");
        if (tasks != null && !tasks.isEmpty()) {
            wcManagerSignInfo.setBpmDefaultSignedName(
                    bpmHelper.findTaskDefaultUserName(tasks.get(tasks.size() - 1)));
        }
        return wcExecManagerSignInfoRepository.save(wcManagerSignInfo);
    }

    /**
     * 是否顯示復原按鈕
     *
     * @param userId
     * @param status
     * @param tasks
     * @return
     */
    public boolean isShowRecovery(String userId, BpmStatus status, List<ProcessTaskBase> tasks) {
        // 前一個簽核者為當前執行者才可復原
        ProcessTaskBase rTask = tasks.get(tasks.size() - 1);
        if (rTask instanceof ProcessTaskHistory) {
            return userId.equals(((ProcessTaskHistory) rTask).getExecutorID());
        }
        return false;
    }

    public boolean canRecoveryExecManagerInfo(String wc_ID, User loginUser) {
        boolean result = false;
        try {
            List<WCExecManagerSignInfo> wcExecManagerSignInfos = this.getAllFlowSignInfos(wc_ID);
            for (WCExecManagerSignInfo gi : wcExecManagerSignInfos) {
                if (gi.getStatus().equals(BpmStatus.INVALID)) {
                    continue;
                }
                List<ProcessTaskBase> tasks = BpmService.getInstance().findSimulationChart(loginUser.getId(), gi.getBpmId());
                if (this.isShowRecovery(loginUser.getId(), gi.getStatus(), tasks)
                        && !wcExceDepService.isExecWorking(wc_ID, gi)) {
                    result = true;
                }
            }
        } catch (Exception e) {
            log.warn("canSignExecManagerInfo ERROR", e);
        }
        return result;
    }
}
