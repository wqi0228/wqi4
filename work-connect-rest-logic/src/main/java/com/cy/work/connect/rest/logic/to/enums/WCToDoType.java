/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to.enums;

/**
 * 代辦事項型態
 *
 * @author kasim
 */
public enum WCToDoType {

    /**
     * 待派工
     */
    WAIT_WORK_LIST("待派工", "search1.xhtml"),
    /**
     * 待領單
     */
    WAIT_RECEIVE_WORK_LIST("待領單", "search2.xhtml"),
    /**
     * 待領單
     */
    WAIT_READ_LIST("待閱讀", "search5.xhtml?readType=UNREAD"),
    /**
     * 待結案
     */
    WAIT_CLOSE_LIST("待結案", "search11.xhtml"),
    /**
     * 待處理
     */
    PROCESS_LIST("待處理", "search3.xhtml");

    //    web / rest /client 必須同步
    private final String name;
    private final String url;

    WCToDoType(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
