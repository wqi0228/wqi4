package com.cy.work.connect.rest.logic.service;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.repository.WCAlertRepository;
import com.cy.work.connect.rest.logic.to.WCAlertTo;
import com.cy.work.connect.vo.WCAlert;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 最新消息 服務
 *
 * @author kasim
 */
@Component
@Slf4j
public class WCAlertService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3860347131517648919L;

    @Autowired
    private WCAlertRepository alertRepository;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WorkParamManager workParamManager;

    /**
     * 取回最新消息 (登入頁面)
     *
     * @param recipient 登入者
     * @param limit     取得數量
     * @return
     */
    public List<WCAlertTo> findByRecipientAndLimit(Integer recipient, Integer limit) {
        try {

            Long startTime = System.currentTimeMillis();
            List<WCAlertTo> result = alertRepository.findByRecipientAndLimit(recipient, limit).stream()
                    .map(each -> this.transTo(each))
                    .collect(Collectors.toList());

            // 顯示執行時間
            String logMessage = "取回最新消息 (登入頁面) findByRecipientAndLimit";
            this.showExecTimeLog(startTime, logMessage, recipient);

            return result;
        } catch (Exception e) {
            log.warn("取回最新消息失敗！！recipient：" + recipient + "，limit：" + limit, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取回最新消息 (訊息頁面)
     *
     * @param recipient 登入者
     * @return
     */
    public List<WCAlertTo> findByRecipient(Integer recipient) {
        try {

            Long startTime = System.currentTimeMillis();
            List<WCAlertTo> result = alertRepository.findByRecipient(recipient).stream()
                    .map(each -> this.transTo(each))
                    .collect(Collectors.toList());

            // 顯示執行時間
            String logMessage = "取回最新消息 (訊息頁面) findByRecipient";
            this.showExecTimeLog(startTime, logMessage, recipient);

            return result;
        } catch (Exception e) {
            log.warn("取回最新消息失敗！！recipient：" + recipient, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取回歷史消息 (訊息頁面)
     *
     * @param recipient 登入者
     * @param startDate 時間(起)
     * @param endDate   時間(訖)
     * @return
     */
    public List<WCAlertTo> findHistoryByRecipientAndBetweenCreateDate(
            Integer recipient, Date startDate, Date endDate) {
        try {

            Long startTime = System.currentTimeMillis();
            List<WCAlertTo> result = alertRepository
                    .findHistoryByRecipientAndBetweenCreateDate(recipient, startDate, endDate)
                    .stream()
                    .map(each -> this.transTo(each))
                    .collect(Collectors.toList());

            // 顯示執行時間
            String logMessage = "取回歷史消息 (訊息頁面) findHistoryByRecipientAndBetweenCreateDate startDate:["
                    + startDate
                    + "], endDate:["
                    + endDate
                    + "]";
            this.showExecTimeLog(startTime, logMessage, recipient);
            return result;
        } catch (Exception e) {
            log.warn(
                    "取回最新消息失敗！！recipient：" + recipient + "，startDate：" + startDate + "，endDate："
                            + endDate,
                    e);
            return Lists.newArrayList();
        }
    }

    /**
     * 讀取訊息
     *
     * @param alertSid
     */
    @Transactional
    public void readAlert(String alertSid) {
        try {
            alertRepository.readAlert(alertSid, new Date());
        } catch (Exception e) {
            log.warn("讀取訊息失敗！！sid：" + alertSid, e);
        }
    }

    /**
     * 轉換為自訂物件
     *
     * @param obj
     * @return
     */
    private WCAlertTo transTo(WCAlert obj) {
        WCAlertTo result = new WCAlertTo();
        result.setSid(obj.getSid());
        result.setUrlParam("wrcId=" + obj.getWcSid() + "&wrcType=news&detailSID=" + obj.getSid());
        result.setTheme(obj.getTheme());
        result.setCreateUser(obj.getCreateUser());
        User createUser = userManager.findBySid(obj.getCreateUser());
        if (createUser != null) {
            result.setCreateUserName(createUser.getName());
        }
        result.setCreateDate(obj.getCreateDate());
        return result;
    }

    private void showExecTimeLog(Long startTime, String apiname, Integer userSid) {

        Double responsetimeBound = this.workParamManager.findRestResponseTimeBound();
        if (responsetimeBound == null) {
            responsetimeBound = 5000.0;
        }
        String responseTime = (responsetimeBound / 1000) + "";

        // ====================================
        // 處理時間 (大於等於後臺設定「config.work-connect-rest.responsetimeBound」的耗時才輸出LOG紀錄)
        // ====================================
        String execueTime = WkCommonUtils.calcCostSec(startTime);
        if (WkStringUtils.notEmpty(execueTime) && WkStringUtils.notEmpty(responseTime)) {
            if ((new BigDecimal(responseTime)).compareTo(new BigDecimal(execueTime)) <= 0) {
                log.warn(
                        "[{}-{}]{}",
                        userSid,
                        WkUserUtils.findNameBySid(userSid),
                        WkCommonUtils.prepareCostMessage(startTime, apiname));
            }
        }
    }

    /**
     * 計算回應秒數
     *
     * @param responseTime
     * @return
     */
    public String calcSec(Double responseTime) {
        responseTime = responseTime / 1000;
        return responseTime + "";
    }
    //
    // /**
    // * 建立訊息
    // *
    // * @param createTo
    // */
    // @Transactional(rollbackForClassName = {"Exception"})
    // public void createAlerts(WCCreateAlertTo createTo) {
    // createTo.getRecipients().forEach(each -> {
    // this.create(createTo.getSid(), createTo.getNo(), each, createTo.getTheme(),
    // createTo.getCreateUser());
    // });
    // }
    //
    // /**
    // * 建立 訊息
    // *
    // * @param wcSid 工作聯絡單 Sid
    // * @param wcNo 工作聯絡單 單號
    // * @param recipient 收件人
    // * @param theme
    // * @param createUser
    // */
    // @Transactional(rollbackForClassName = {"Exception"})
    // private void create(String wcSid, String wcNo, Integer recipient, String theme, Integer
    // createUser) {
    // WCAlert obj = new WCAlert();
    // obj.setWcSid(wcSid);
    // obj.setWcNo(wcNo);
    // obj.setType(WRTraceType.WAIT_EXEC);
    // obj.setRecipient(recipient);
    // obj.setTheme(theme + "-待處理通知");
    // obj.setCreateUser(createUser);
    // obj.setCreateDate(new Date());
    // obj = wcAlertRepository.save(obj);
    // Preconditions.checkState(obj != null, "訊息建立失敗!!");
    // }
    //
    // /**
    // * 讀取訊息
    // *
    // * @param createTo
    // */
    // @Transactional(rollbackForClassName = {"Exception"})
    // public void readAlerts(WCUpdateAlertTo updateTo) {
    // if (WCReadAlertType.FINISH_SIGN.equals(updateTo.getType())) {
    // this.readAlerts(updateTo, Lists.newArrayList(WRTraceType.WAIT_EXEC));
    // } else if (WCReadAlertType.CLOSE.equals(updateTo.getType())) {
    // this.readAlerts(updateTo, Lists.newArrayList(WRTraceType.FINISH_REPLY,
    // WRTraceType.CORRECT_REPLY,
    // WRTraceType.WAIT_EXEC));
    // }
    // }
    //
    // /**
    // * 讀取訊息
    // *
    // * @param wcSid 工作聯絡單 Sid
    // * @param recipients 收件者
    // * @param Types 欲讀取型態
    // */
    // @Transactional
    // private void readAlerts(WCUpdateAlertTo updateTo, List<WRTraceType> Types) {
    // int count = wcAlertRepository.readAlertByWcSidAndTypesAndRecipients(updateTo.getSid(), Types,
    // updateTo.getRecipients(), new Date());
    // if (count == 0) {
    // log.error("批次讀取訊息失敗！！wcSid：" + updateTo.getSid());
    // }
    // }
}
