/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import com.cy.work.connect.repository.WCExecRollBackHistoryRepository;
import com.cy.work.connect.vo.WCExecRollBackHistory;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WCExecRollBackHistoryService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4580824216785949214L;

    @Autowired
    private WCExecRollBackHistoryRepository wcExecRollBackHistoryRepository;

    public void create(String wcSid, Integer rollBackUserSid, List<Integer> rollbackDepSids) {
        WCExecRollBackHistory wbh = new WCExecRollBackHistory();
        wbh.setRollBackDate(new Date());
        wbh.setRollBackSid(rollBackUserSid);
        wbh.setRollBackDep(transToUserDep(rollbackDepSids));
        wbh.setWcSid(wcSid);
        wcExecRollBackHistoryRepository.save(wbh);
    }

    private UserDep transToUserDep(List<Integer> rollbackDepSids) {
        UserDep ud = new UserDep();
        rollbackDepSids.forEach(
            item -> {
                UserDepTo udt = new UserDepTo();
                udt.setDepSid(String.valueOf(item));
                ud.getUserDepTos().add(udt);
            });
        return ud;
    }
}
