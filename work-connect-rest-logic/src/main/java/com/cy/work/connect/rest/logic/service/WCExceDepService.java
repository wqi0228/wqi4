/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.connect.repository.WCExceDepRepository;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.ExecDepType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * @author brain0925_liao
 */
@Component
public class WCExceDepService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7623236982252235542L;

    @Autowired
    private WCExceDepRepository wcExceDepRepository;
    @Autowired
    private WCExecDepSettingService execDepSettingService;
    @Autowired
    private WCMasterService wcMasterService;

    /**
     * 是否已有執行單位正在執行
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isAnyExecDepWorking(String wcSid) {
        List<WCExceDep> wcExceDeps = wcExceDepRepository.findByWcSid(wcSid);
        if (wcExceDeps == null) {
            return false;
        }
        List<WCExceDep> workingDeps = wcExceDeps.stream()
                .filter(
                        each -> Activation.ACTIVE.equals(each.getStatus())
                                && !(WCExceDepStatus.WAITRECEIVCE.equals(each.getExecDepStatus())
                                        || WCExceDepStatus.APPROVING.equals(each.getExecDepStatus())
                                        || WCExceDepStatus.WAITSEND.equals(each.getExecDepStatus())))
                .collect(Collectors.toList());
        return workingDeps != null && !workingDeps.isEmpty();
    }

    public void changeToExecFinish(String wcSid, Integer loginUserSid) {
        List<WCExceDep> wcExecDeps = this.getWCExceDepByWcSid(wcSid);
        if (wcExecDeps == null || wcExecDeps.isEmpty()) {
            return;
        }

        // ====================================
        // 判斷是否執行單位都已在完成狀態
        // ====================================
        // 檢查是不是全部執行單位都完成了
        boolean isAllFinish = wcExecDeps.stream()
                .allMatch(execDep -> execDep.getExecDepStatus() != null
                        && !execDep.getExecDepStatus().isFinish());

        // ====================================
        // 更新主單狀態
        // ====================================
        if (isAllFinish) {
            this.wcMasterService.updateToExecFinish(wcSid, loginUserSid);
        } else {
            WCMaster wcMaster = wcMasterService.getWCMasterBySid(wcSid);
            if (WCStatus.EXEC_FINISH.equals(wcMaster.getWc_status())) {
                this.wcMasterService.updateToExec(wcSid, loginUserSid);
            }
        }
    }

    public WCExceDep updateWCExceDep(WCExceDep wcExceDep) {
        return wcExceDepRepository.save(wcExceDep);
    }

    public List<WCExceDep> getWCExceDepByWcSid(String wcSid) {
        List<WCExceDep> wcExceDeps = wcExceDepRepository.findByWcSid(wcSid);
        if (wcExceDeps == null) {
            return Lists.newArrayList();
        }
        return wcExceDeps;
    }

    public List<WCExceDep> getByGroupSeqAndWcSId(Integer execManagerSignInfoGroupSeq,
            String wcSid) {
        List<WCExceDep> wcExceDeps = wcExceDepRepository.findOneByGroupSeqAndWcSId(execManagerSignInfoGroupSeq, wcSid);
        if (wcExceDeps == null) {
            return Lists.newArrayList();
        }
        return wcExceDeps;
    }

    public void updateExecWorkingApproving(
            String wcSid, WCExecManagerSignInfo wcExecManagerSignInfo) {
        List<WCExceDep> wcExecpDeps = getWCExceDepByWcSid(wcSid);
        if (wcExecpDeps == null) {
            return;
        }
        List<WCExceDep> workingExecDeps = wcExecpDeps.stream()
                .filter(
                        each -> each.getStatus().equals(Activation.ACTIVE)
                                && each.getExecManagerSignInfoGroupSeq() != null
                                && each.getExecManagerSignInfoGroupSeq()
                                        .equals(wcExecManagerSignInfo.getGroupSeq())
                                && (each.getExecDepStatus().equals(WCExceDepStatus.WAITSEND)
                                        || each.getExecDepStatus().equals(WCExceDepStatus.WAITRECEIVCE)))
                .collect(Collectors.toList());

        if (workingExecDeps == null || workingExecDeps.isEmpty()) {
            return;
        }
        workingExecDeps.forEach(
                item -> {
                    item.setExecDepStatus(WCExceDepStatus.APPROVING);
                    this.updateWCExceDep(item);
                });
    }

    public boolean isExecWorking(String wcSid, WCExecManagerSignInfo wcExecManagerSignInfo) {
        List<WCExceDep> wcExecpDeps = getWCExceDepByWcSid(wcSid);
        if (wcExecpDeps == null) {
            return false;
        }
        List<WCExceDep> workingExecDeps = wcExecpDeps.stream()
                .filter(
                        each -> each.getStatus().equals(Activation.ACTIVE)
                                && each.getExecManagerSignInfoGroupSeq() != null
                                && each.getExecManagerSignInfoGroupSeq()
                                        .equals(wcExecManagerSignInfo.getGroupSeq())
                                && !each.getExecDepStatus().equals(WCExceDepStatus.APPROVING)
                                && !each.getExecDepStatus().equals(WCExceDepStatus.WAITSEND)
                                && !each.getExecDepStatus().equals(WCExceDepStatus.WAITRECEIVCE))
                .collect(Collectors.toList());

        return workingExecDeps != null && !workingExecDeps.isEmpty();
    }

    public void deleteExecDep(String wcSid, User loginUser) {
        List<WCExceDep> wcExceDeps = wcExceDepRepository.findByWcSid(wcSid);
        wcExceDeps.forEach(
                subItem -> {
                    subItem.setExecDepStatus(WCExceDepStatus.STOP);
                    subItem.setStatus(Activation.INACTIVE);
                    this.updateWCExceDep(subItem);
                });
    }

    /**
     * 取得簽核中執行單位
     *
     * @param wcSid                  工作聯絡單Sid
     * @param execManagerSignInfoSid 執行單位簽核物件Sid
     * @return
     */
    public List<WCExceDep> getApprovingExecDeps(String wcSid, String execManagerSignInfoSid) {
        List<WCExceDep> wcExecpDeps = this.getWCExceDepByWcSid(wcSid);
        List<WCExceDep> approvingExecDeps = wcExecpDeps.stream()
                .filter(
                        each -> each.getExecDepStatus().equals(WCExceDepStatus.APPROVING)
                                && !Strings.isNullOrEmpty(each.getExecManagerSignInfoSid())
                                && each.getExecManagerSignInfoSid().equals(execManagerSignInfoSid))
                .collect(Collectors.toList());
        return approvingExecDeps;
    }

    /**
     * 根據類型進行更新執行單位執行狀態
     *
     * @param item 執行單位
     */
    public void updateWaitWorkStatus(WCExceDep item) {
        if (Strings.isNullOrEmpty(item.getExecDepSettingSid())) {
            if (ExecDepType.NEED_SIGN.equals(item.getExecDepType())) {
                item.setExecDepStatus(WCExceDepStatus.WAITSEND);
            } else {
                item.setExecDepStatus(WCExceDepStatus.WAITRECEIVCE);
            }
        } else {
            WCExecDepSetting execDepSetting = execDepSettingService.getWCExecDepSettingBySid(item.getExecDepSettingSid());
            if (execDepSetting.isNeedAssigned()) {
                item.setExecDepStatus(WCExceDepStatus.WAITSEND);
            } else {
                item.setExecDepStatus(WCExceDepStatus.WAITRECEIVCE);
            }
        }
        this.updateWCExceDep(item);
    }
}
