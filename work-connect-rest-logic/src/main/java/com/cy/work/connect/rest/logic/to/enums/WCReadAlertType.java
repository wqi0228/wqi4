/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to.enums;

/**
 * 讀取訊息類型
 *
 * @author kasim
 */
public enum WCReadAlertType {
    /**
     * 完成簽核
     */
    FINISH_SIGN,
    /**
     * 結案
     */
    CLOSE
}
