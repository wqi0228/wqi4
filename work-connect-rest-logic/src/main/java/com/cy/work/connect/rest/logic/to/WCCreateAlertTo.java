/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * rest / clent 使用 - 建立訊息
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WCCreateAlertTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 242221689008802354L;

    @JsonProperty("sid")
    /** 工作聯絡單 Sid */
    private String sid;

    @JsonProperty("no")
    /** 工作聯絡單 單號 */
    private String no;

    @JsonProperty("recipients")
    /** 收件人 */
    private List<Integer> recipients;

    @JsonProperty("theme")
    /** 主題 */
    private String theme;

    @JsonProperty("createUser")
    /** 建立者 */
    private Integer createUser;

    public WCCreateAlertTo(
        String sid, String no, List<Integer> recipients, String theme, Integer createUser) {
        this.sid = sid;
        this.no = no;
        this.recipients = recipients;
        this.theme = theme;
        this.createUser = createUser;
    }
}
