/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.repository.WCManagerSignInfoDetailRepository;
import com.cy.work.connect.repository.WCManagerSignInfoRepository;
import com.cy.work.connect.rest.logic.helper.BPMHelper;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCManagerSignInfoDetail;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.SignActionType;
import com.cy.work.connect.vo.enums.SignType;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCManagerSignInfoService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4986958747653866232L;

    @Autowired
    private WCManagerSignInfoRepository wcManagerSignInfoRepository;
    @Autowired
    private WCExecManagerSignInfoService wcExecManagerSignInfoService;
    @Autowired
    private WCManagerSignInfoDetailRepository wcManagerSignInfoDetailRepository;
    @Autowired
    private BPMHelper bpmHelper;
    @Autowired
    private WCExceDepService wcExceDepService;
    @Autowired
    private TaskClient taskClient;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCRollbackHistorySservice wcRollbackHistorySservice;

    @Autowired
    private OpinionDescripeSettingService opinionDescripeSettingService;

    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateBpmStatus(String userId, WCManagerSignInfo signInfo, BpmStatus status)
            throws ProcessRestException {
        Preconditions.checkState(status != null, "BpmStatus 為NULL 無法更新簽核資訊，請確認！！");
        signInfo.setStatus(status);
        signInfo.setUpdate_dt(new Date());
        wcManagerSignInfoRepository.save(signInfo);
    }

    public void doRollBack(
            String userId, WCManagerSignInfo signInfo, ProcessTaskHistory rollBackTask, String comment)
            throws ProcessRestException {
        User loginUser = userManager.findById(userId);
        bpmHelper.rollBack(userId, rollBackTask, comment);
        User backToUser = userManager.findById(rollBackTask.getExecutorID());
        signInfo = this.doSyncBpmData(signInfo, SignActionType.ROLLBACK, backToUser.getSid());

        try {
            // 寫退回記錄
            User rollbackUser = userManager.findById(userId);
            wcRollbackHistorySservice.insertRollBackHistory(rollbackUser.getSid(),
                    signInfo.getWcSid());

            // 將退回理由存至對應人員(被退的人員)
            opinionDescripeSettingService.saveRollbackReason(
                    ManagerSingInfoType.MANAGERSIGNINFO,
                    signInfo.getSid(),
                    backToUser.getSid(),
                    comment,
                    loginUser.getSid(),
                    signInfo.getWcSid());

        } catch (Exception e) {
            log.warn(
                    "doRollBack (userId:{}, signInfo.getWcSid():{}, signInfo.getSid():{}, backToUser.getSid():{}, comment:{})",
                    userId,
                    signInfo.getWcSid(),
                    signInfo.getSid(),
                    backToUser.getSid(),
                    comment);
            log.warn("doRollBack ERROR", e);
        }

        WCOpinionDescripeSetting wcOpinionDescripeSetting = opinionDescripeSettingService.getOpinionDescripeSetting(
                ManagerSingInfoType.MANAGERSIGNINFO, signInfo.getSid(), backToUser.getSid());
        List<ProcessTaskBase> tasks = BpmService.getInstance().findSimulationChart(userId, signInfo.getBpmId());
        BpmStatus bpmStatus = bpmHelper.getBpmStatus(userId, signInfo.getBpmId(), tasks, wcOpinionDescripeSetting);
        this.updateBpmStatus(userId, signInfo, bpmStatus);

        this.wcManagerSignInfoManager.updateStatusByBPM(signInfo.getWcSid(), loginUser.getSid(), bpmStatus);
    }

    /**
     * 執行BPM復原
     *
     * @param userId
     * @param signSid
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void recoveryBpm(String userId, String signSid, boolean isFromContinue)
            throws ProcessRestException {
        WCManagerSignInfo signInfo = this.findBySid(signSid);
        Preconditions.checkState(signInfo != null, "查無簽核資訊無法進行BPM復原");
        List<ProcessTaskBase> tasks = BpmService.getInstance().findSimulationChart(userId, signInfo.getBpmId());
        int recoverySub = BpmStatus.APPROVED.equals(signInfo.getStatus()) ? 1 : 2;
        bpmHelper.recovery(userId, (ProcessTaskHistory) tasks.get(tasks.size() - recoverySub));
        User user = userManager.findById(userId);
        // 即時更新流程資訊
        signInfo = this.doSyncBpmData(signInfo, SignActionType.RECOVERY, user.getSid());
        WCOpinionDescripeSetting wcOpinionDescripeSetting = opinionDescripeSettingService.getOpinionDescripeSetting(
                ManagerSingInfoType.MANAGERSIGNINFO, signInfo.getSid(), user.getSid());
        List<ProcessTaskBase> nTasks = BpmService.getInstance().findSimulationChart(userId, signInfo.getBpmId());
        BpmStatus bpmStatus = bpmHelper.getBpmStatus(userId, signInfo.getBpmId(), nTasks, wcOpinionDescripeSetting);
        this.updateBpmStatus(userId, signInfo, bpmStatus);

        if (isFromContinue) {
            this.wcManagerSignInfoManager.updateStatusByBPM(signInfo.getWcSid(), user.getSid(), null);
        } else {
            this.wcManagerSignInfoManager.updateStatusByBPM(signInfo.getWcSid(), user.getSid(), bpmStatus);
        }
    }

    /**
     * 此為清除會簽節點的BPM資訊, 會簽節點會根據需求流程節點簽名完畢,才會產生 若有人進行退回或復原 該BPM節點必須清除,但會簽人員資訊不需要再重新挑選
     *
     * @param userId   清除節點User ID
     * @param signInfo 簽核物件
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doClearBpmContinueSign(String userId, WCManagerSignInfo signInfo)
            throws ProcessRestException {
        bpmHelper.invalid(userId, signInfo.getBpmId());
        signInfo.setBpmId("");
        signInfo.setStatus(BpmStatus.WAIT_CREATE);
        signInfo.setUpdate_dt(new Date());
        signInfo.setBpmDefaultSignedName("");
        signInfo = wcManagerSignInfoRepository.save(signInfo);
    }

    /**
     * 查詢 by Sid
     *
     * @param sid
     * @return
     */
    public WCManagerSignInfo findBySid(String sid) {
        return wcManagerSignInfoRepository.findOne(sid);
    }

    public List<WCManagerSignInfo> getAllFlowSignInfos(String wc_ID) {
        List<WCManagerSignInfo> managerSignInfos = wcManagerSignInfoRepository.findOneByWCSid(
                wc_ID);
        if (managerSignInfos == null || managerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<WCManagerSignInfo> activeWCManagerSignInfos = managerSignInfos.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                                && !each.getStatus().equals(BpmStatus.INVALID))
                .collect(Collectors.toList());
        return activeWCManagerSignInfos;
    }

    public List<WCManagerSignInfo> getFlowSignInfos(String wc_ID) {
        List<WCManagerSignInfo> managerSignInfos = wcManagerSignInfoRepository.findOneByWCSid(
                wc_ID);
        if (managerSignInfos == null || managerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<WCManagerSignInfo> activeWCManagerSignInfos = managerSignInfos.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                                && each.getSignType().equals(SignType.SIGN))
                .collect(Collectors.toList());
        return activeWCManagerSignInfos;
    }

    public List<WCManagerSignInfo> getContinueSignInfos(String wc_ID) {
        List<WCManagerSignInfo> managerSignInfos = wcManagerSignInfoRepository.findOneByWCSid(
                wc_ID);
        if (managerSignInfos == null || managerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<WCManagerSignInfo> activeWCManagerSignInfos = managerSignInfos.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.DELETE)
                                && each.getSignType().equals(SignType.COUNTERSIGN))
                .collect(Collectors.toList());
        return activeWCManagerSignInfos;
    }

    /**
     * 檢測會簽是否已有人簽名
     *
     * @param wc_ID 工作聯絡單SID
     * @return
     */
    public boolean isCheckContinueSign(String wc_ID) {
        for (WCManagerSignInfo si : getContinueSignInfos(wc_ID)) {
            if (si.getStatus().equals(BpmStatus.APPROVED)) {
                return true;
            }
        }
        return false;
    }

    public void clearContinueSignInfo(String wc_ID, User loginUser) {
        this.getContinueSignInfos(wc_ID)
                .forEach(
                        item -> {
                            try {
                                this.doClearBpmContinueSign(loginUser.getId(),
                                        this.findBySid(item.getSid()));
                            } catch (Exception e) {
                                log.warn(
                                        "clearContinueSignInfo (wc_ID:{}, loginUser.getId():{}, WCManagerSignInfo.getSid():{})",
                                        wc_ID,
                                        loginUser.getId(),
                                        item.getSid());
                                log.warn("clearContinueSignInfo ERROR", e);
                            }
                        });
    }

    /**
     * 是否顯示復原按鈕(正常需求流程-核准)
     *
     * @param userId
     * @param tasks
     * @param wcSid
     * @return
     */
    public Boolean isShowApprovedRecovery(String userId, List<ProcessTaskBase> tasks,
            String wcSid) {
        try {
            if (!tasks.isEmpty()) {
                ProcessTaskBase rTask = tasks.get(tasks.size() - 1);
                if (rTask instanceof ProcessTaskHistory) {
                    if (userId.equals(((ProcessTaskHistory) rTask).getExecutorID())) {
                        // 檢測需求加簽是否已有簽名
                        boolean isSignCheckContinue = this.isCheckContinueSign(wcSid);
                        if (isSignCheckContinue) {
                            return false;
                        }
                        // 是否已有執行單位正在執行
                        boolean isSignCheckFlow = wcExceDepService.isAnyExecDepWorking(wcSid);
                        if (isSignCheckFlow) {
                            return false;
                        }
                        // 是否已有再會簽
                        boolean isExecSignCheckContinue = wcExecManagerSignInfoService.isCheckAnyContinue(wcSid);
                        if (isExecSignCheckContinue) {
                            return false;
                        }
                        // 檢測核准者流程是否已有簽名
                        boolean isExecSignCheckFlow = wcExecManagerSignInfoService.isCheckAnyFlowSign(
                                wcSid);
                        return !isExecSignCheckFlow;
                    }
                }
            }
        } catch (Exception e) {
            log.warn(
                    "isShowApprovedRecovery (userId:{}, tasks:{}, wcSid:{})",
                    userId,
                    new Gson().toJson(tasks),
                    wcSid);
            log.warn("isShowApprovedRecovery ERROR", e);
        }
        return false;
    }

    /**
     * 是否顯示復原按鈕(會簽)
     *
     * @param user          登入者
     * @param tasks         BPM水管資訊
     * @param wcSid         主檔Sid
     * @param createUserSid 建立該會簽的使用者Sid
     * @return
     */
    public Boolean isShowContinueRecovery(
            User user, List<ProcessTaskBase> tasks, String wcSid, Integer createUserSid) {
        try {
            if (!tasks.isEmpty()) {
                ProcessTaskBase rTask = tasks.get(tasks.size() - 1);
                if (rTask instanceof ProcessTaskHistory) {
                    if (user.getId().equals(((ProcessTaskHistory) rTask).getExecutorID())) {
                        // 是否已有執行單位正在執行
                        boolean isSignCheckFlow = wcExceDepService.isAnyExecDepWorking(wcSid);
                        if (isSignCheckFlow) {
                            return false;
                        }
                        // 是否已有再會簽
                        boolean isExecSignCheckContinue = wcExecManagerSignInfoService.isCheckAnyContinue(wcSid);
                        if (isExecSignCheckContinue) {
                            return false;
                        }
                        // 檢測核准者流程是否已有簽名
                        boolean isExecSignCheckFlow = wcExecManagerSignInfoService.isCheckAnyFlowSign(
                                wcSid);
                        return !isExecSignCheckFlow;
                    }
                }
            }
        } catch (Exception e) {
            log.warn(
                    "isShowContinueRecovery (user.getId():{}, wcSid:{}, tasks:{})",
                    user.getId(),
                    wcSid,
                    new Gson().toJson(tasks));
            log.warn("isShowContinueRecovery ERROR", e);
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public WCManagerSignInfo doSyncBpmData(
            WCManagerSignInfo wcManagerSignInfo, SignActionType signActionType, Integer signUserSid)
            throws ProcessRestException {
        List<ProcessTaskBase> tasks = Lists.newArrayList();

        try {
            if (!Strings.isNullOrEmpty(wcManagerSignInfo.getBpmId())) {
                tasks = taskClient.findSimulationChart(wcManagerSignInfo.getBpmId());
            }
        } catch (Exception e) {
            log.warn("findSimulationChart ERROR", e);
        }

        updateSignData(wcManagerSignInfo, tasks, signActionType, signUserSid);
        wcManagerSignInfo.setBpmDefaultSignedName("");
        if (tasks != null && !tasks.isEmpty()) {
            wcManagerSignInfo.setBpmDefaultSignedName(
                    bpmHelper.findTaskDefaultUserName(tasks.get(tasks.size() - 1)));
        }
        return wcManagerSignInfoRepository.save(wcManagerSignInfo);
    }

    public void updateSignData(
            WCManagerSignInfo wcManagerSignInfo,
            List<ProcessTaskBase> tasks,
            SignActionType signActionType,
            Integer signUserSid) {
        try {
            // 將舊資料修改成無效資料
            wcManagerSignInfoDetailRepository.updateStatus(
                    Activation.INACTIVE, wcManagerSignInfo.getSid());
            // 由於復原及退回在單據歷史紀錄是無法查詢的,所以必須更新狀態
            if (signActionType != null
                    && (signActionType.equals(SignActionType.RECOVERY)
                            || signActionType.equals(SignActionType.ROLLBACK))
                    && signUserSid != null) {
                wcManagerSignInfoDetailRepository.updateSignActionType(
                        signActionType, wcManagerSignInfo.getSid(), signUserSid);
            }
            if (tasks != null && !tasks.isEmpty()) {
                tasks.forEach(
                        item -> {
                            try {
                                if (item instanceof ProcessTaskHistory) {
                                    ProcessTaskHistory ph = (ProcessTaskHistory) item;
                                    User user = userManager.findById(ph.getExecutorID());
                                    WCManagerSignInfoDetail wd = new WCManagerSignInfoDetail();
                                    wd.setSign_dt(ph.getTaskEndTime());
                                    wd.setStatus(Activation.ACTIVE);
                                    wd.setUser_sid(user.getSid());
                                    wd.setWc_manager_sign_info_sid(wcManagerSignInfo.getSid());
                                    wcManagerSignInfoDetailRepository.save(wd);
                                }
                            } catch (Exception e) {
                                log.warn(
                                        "updateSignData (wcManagerSignInfo.getSid():{}, tasks:{})",
                                        wcManagerSignInfo.getSid(),
                                        new Gson().toJson(tasks));
                                log.warn("updateSignData ERROR", e);
                            }
                        });
            }
        } catch (Exception e) {
            log.warn(
                    "updateSignData (wcManagerSignInfo.getSid():{}, signActionType:{}, signUserSid:{}, tasks:{})",
                    wcManagerSignInfo.getSid(),
                    signActionType,
                    signUserSid,
                    new Gson().toJson(tasks));
            log.warn("updateSignData ERROR", e);
        }
    }
}
