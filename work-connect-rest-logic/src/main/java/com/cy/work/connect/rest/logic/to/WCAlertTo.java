/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * rest / clent 使用 - 訊息
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"sid"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WCAlertTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1221249095556364601L;

    @JsonProperty("sid")
    /** 訊息 Sid */
    private String sid;

    @JsonProperty("urlParam")
    /** 網址參數 */
    private String urlParam;

    @JsonProperty("theme")
    /** 主題 */
    private String theme;

    @JsonProperty("createUser")
    /** 建立者 */
    private Integer createUser;

    @JsonProperty("createUserName")
    /** 建立者 */
    private String createUserName;

    @JsonProperty("createDate")
    /** 建立日期 */
    private Date createDate;
}
